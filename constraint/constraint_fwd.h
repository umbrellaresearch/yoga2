#pragma once
#include "common/std_headers.h"
#include "../jit/value.h"
#include "numerical/polyfit.h"

struct ConstrainedVar;
struct Panel3dDrawList;

struct Constraint {

  Constraint();
  virtual ~Constraint();

  glm::vec4 relPos;

  
  virtual void run(R strength, R *forces, R *positions, R *stiffnesses);
  virtual void draw(Panel3dDrawList &dl, glm::mat4 const &m, R strength, R *forces, R *positions, R *stiffnesses);

};

struct ConstraintGroup : Constraint {

  ConstraintGroup();
  virtual ~ConstraintGroup();

  void run(R strength, R *forces, R *positions, R *stiffnesses) override;

  glm::vec4 relPos;
  vector<Constraint *> children; // not owned
};

struct ConstrainedVar {
  ConstrainedVar(string const &_name, int _index)
    :name(_name), index(_index)
  {
  }

  string name;
  int index;

};

struct ConstrainedSystemState;

struct ConstrainedSystemModel {
  ConstrainedSystemModel();
  ~ConstrainedSystemModel();

  ConstrainedVar *mkVar(string const &name);
  void addConstraint(Constraint *it);

  vector<Constraint *> constraints; // owned
  vector<ConstrainedVar *> vars; // owned
  map<string, ConstrainedVar *> varsByName; // not owned

  static shared_ptr<ConstrainedSystemState> getTestModelState();
  static map<string, shared_ptr<ConstrainedSystemModel>> modelsByName;
};

struct ConstrainedSystemState {
  ConstrainedSystemState(shared_ptr<ConstrainedSystemModel> _model);

  void addContents(Panel3dDrawList &dl);
  void runOde(R dt);
  void setPos(size_t index, R pos);

  shared_ptr<ConstrainedSystemModel> model;
  vector<R> positions;
  vector<R> velocities;
  vector<R> forces;
  vector<R> stiffnesses;
};


struct ConstraintLinear : Constraint {

  vector<tuple<R, R, ConstrainedVar *>> pts;
  void addPt(R attach, R stiffness, ConstrainedVar *var);

  Polyfit1 solve(R *positions);

  void run(R strength, R *forces, R *positions, R *stiffnesses) override;
  void draw(Panel3dDrawList &dl, glm::mat4 const &m, R strength, R *forces, R *positions, R *stiffnesses) override;
};
