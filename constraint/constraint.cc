#include "./constraint_fwd.h"
#include <Eigen/Dense>

Constraint::Constraint()
{
}

Constraint::~Constraint()
{
}

void Constraint::draw(Panel3dDrawList &dl, glm::mat4 const &m, R strength,
  R *forces, R *positions, R *stiffnesses)
{
}

void Constraint::run(R strength, R *forces, R *positions, R *stiffnesses)
{
}


ConstraintGroup::ConstraintGroup()
{
}

ConstraintGroup::~ConstraintGroup()
{
}

void ConstraintGroup::run(R strength, R *forces, R *positions, R *stiffnesses)
{
  for (auto &it : children) {
    it->run(strength, forces, positions, stiffnesses);
  }
}

ConstrainedSystemModel::ConstrainedSystemModel()
{
}

ConstrainedSystemModel::~ConstrainedSystemModel()
{
  for (auto &it : vars) {
    delete it;
    it = nullptr;
  }
  for (auto &it : constraints) {
    delete it;
    it = nullptr;
  }
}

ConstrainedVar *ConstrainedSystemModel::mkVar(string const &name)
{
  auto v = new ConstrainedVar(name, vars.size());
  vars.push_back(v);
  varsByName[name] = v;
  return v;
}

void ConstrainedSystemModel::addConstraint(Constraint *it)
{
  constraints.push_back(it);
}


map<string, shared_ptr<ConstrainedSystemModel>> ConstrainedSystemModel::modelsByName;

shared_ptr<ConstrainedSystemState> ConstrainedSystemModel::getTestModelState()
{
  auto &slot = modelsByName["test"];
  if (!slot) {
    slot = make_shared<ConstrainedSystemModel>();
    {
      auto v1 = slot->mkVar("foo");
      auto v2 = slot->mkVar("bar");
      auto v3 = slot->mkVar("buz");
      auto c1 = new ConstraintLinear();
      c1->addPt(-0.5, 1.0, v1);
      c1->addPt(+0.5, 1.0, v2);
      c1->addPt(+0.75, 1.0, v3);
      slot->addConstraint(c1);
    }
  }
  return make_shared<ConstrainedSystemState>(slot);
}

ConstrainedSystemState::ConstrainedSystemState(shared_ptr<ConstrainedSystemModel> _model)
  : model(_model),
    positions(_model->vars.size()),
    velocities(_model->vars.size()),
    forces(_model->vars.size()),
    stiffnesses(_model->vars.size())
{
}

void ConstrainedSystemState::runOde(R dt)
{
  auto n = model->vars.size();

  const int ITERS=16;
  for (int iter=0; iter<ITERS; iter++) {
    R subdt = dt / ITERS;
    for (size_t i=0; i < n; i++) {
      forces[i] = 0.0;
      stiffnesses[i] = 0.0;
    }
    for (auto &it : model->constraints) {
      it->run(500000.0, forces.data(), positions.data(), stiffnesses.data());
    }

    for (size_t i=0; i < n; i++) {
      // TODO: optimize
      R invmass = 10000.0 / (100.0 + stiffnesses[i]);
      R damping = 5000.0;
      R limPos = 2.5;
      positions[i] += subdt * velocities[i];
      velocities[i] += subdt * (forces[i] - velocities[i] * damping) * invmass;
      if (positions[i] > limPos) {
        positions[i] = limPos;
        if (velocities[i] > 0.0) velocities[i] = 0.0;
      }
      if (positions[i] < -limPos) {
        positions[i] = -limPos;
        if (velocities[i] < 0.0) velocities[i] = 0.0;
      }
    }
  }
}

void ConstraintLinear::addPt(R attach, R stiffness, ConstrainedVar *var)
{
  pts.emplace_back(attach, stiffness, var);
}

/*
  Find and b to minimize the sum of:
      ((var[i] - a + b*pos) * stiffness) ^ 2
  This is a weighted least squares problem.
*/
Polyfit1 ConstraintLinear::solve(R *positions)
{
  R sumxx = 0.0, sumxy = 0.0, sumyy = 0.0, sumx = 0.0, sumy = 0.0, sum1 = 0.0;

  for (auto &[attach, stiffness, var] : pts) {
    auto x = attach;
    auto y = positions[var->index];
    sumx += x * stiffness;
    sumy += y * stiffness;
    sumxx += x*x * stiffness;
    sumxy += x*y * stiffness;
    sumyy += y*y * stiffness;
    sum1 += stiffness;
  }
  if (sum1 < 1e-6) {
    return Polyfit1(0.0, 0.0);
  }

  R avgx = sumx / sum1;
  R avgy = sumy / sum1;

  R sxx = sumxx - sumx * sumx / sum1;
  R sxy = sumxy - sumx * sumy / sum1;

  R b = sxy / sxx; 
  R a = avgy - b * avgx;
  return Polyfit1(a, b);
}

void ConstraintLinear::run(R strength, R *forces, R *positions, R *stiffnesses)
{
  auto fit = solve(positions);

  for (auto &[attach, stiffness, var] : pts) {
    auto y = fit(attach);
    auto err = y - positions[var->index];
    auto force = err * stiffness * strength;
    forces[var->index] += force;
    stiffnesses[var->index] += stiffness * strength;
  }
}
