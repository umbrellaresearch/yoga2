#include "./constraint_fwd.h"
#include <Eigen/Dense>
#include "../studio/ysgui.h"
#include "../studio/panel_render.h"
#include "../studio/yogastudio_ui.h"
#include "../studio/scope_model.h"

void YogaStudioView::drawTestConstraints()
{
  //auto &m = ConstrainedSystemModel::getTestModel();
  drawHamburgerMenu();

  static Panel3dDrawList dl;
  static shared_ptr<ConstrainedSystemState> state;
  static R relTime = 0.0;
  if (!state) {
    dl.setHoverHome();
    state = ConstrainedSystemModel::getTestModelState();
  }

  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;
  auto const pos = window->DC.CursorPos;
  const ImVec2 size = CalcItemSize(ImVec2(-1, -1), 0, 0);
  ItemSize(size, g.Style.FramePadding.y);
  auto curbb = ImRect(pos, pos + size);

  dl.vpL = curbb.Min.x;
  dl.vpT = curbb.Min.y;
  dl.vpR = curbb.Max.x;
  dl.vpB = curbb.Max.y;
  dl.hoverControls();

  if (ImGui::IsKeyDown(SDL_SCANCODE_1)) {
    state->setPos(0, 0.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_2)) {
    state->setPos(0, 1.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_3)) {
    state->setPos(1, 0.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_4)) {
    state->setPos(1, 1.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_5)) {
    state->setPos(2, 0.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_6)) {
    state->setPos(2, 1.0);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_7)) {
    state->setPos(0, 0.5*sin(relTime));
    state->setPos(1, 0.5*cos(relTime));
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_8)) {
    state->setPos(1, 0.5*sin(relTime));
    state->setPos(2, 0.5*cos(relTime));
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_9)) {
    state->setPos(0, 0.5*sin(relTime));
    state->setPos(2, 0.5*cos(relTime));
  }

  R dt = 5.0/60.0;
  relTime += dt;
  state->runOde(dt);
  uvUiActive = true;

  GetWindowDrawList()->AddCallback([](const ImDrawList* parent_list, const ImDrawCmd* cmd) {
    //auto this1 = reinterpret_cast<YogaStudioView *>(cmd->UserCallbackData);
    dl.clear();
    state->addContents(dl);
    dl.glRender();
  }, reinterpret_cast<void *>(this));
  GetWindowDrawList()->AddCallback(ImDrawCallback_ResetRenderState, nullptr);
}

void ConstrainedSystemState::addContents(Panel3dDrawList &dl)
{
  glm::mat4 transform(1.0);

  for (auto &it : model->constraints) {
    it->draw(dl, transform, 1.0, forces.data(), positions.data(), stiffnesses.data());
  }
  dl.addArrowTriple(transform);
}

void ConstrainedSystemState::setPos(size_t index, R pos)
{
  assert(index < positions.size());
  positions[index] = pos;
  velocities[index] = 0.0;
}


void ConstraintLinear::draw(Panel3dDrawList &dl, glm::mat4 const &m, R strength, R *forces, R *positions, R *stiffnesses)
{
  auto fit = solve(positions);

  R maxPos = 1.0;
  R maxStiffness = 1.0;
  for (auto &[pos, stiffness, var] : pts) {
    maxPos = max(maxPos, abs(pos));
    maxStiffness = max(maxStiffness, stiffness);
  }

  R scaleX = 0.25;
  R scaleY = 0.40/maxPos;
  R centerX = 0.5;
  R centerY = 0.5;

  R linkPosX = centerX + scaleX * fit(+1.0);
  R linkNegX = centerX + scaleX * fit(-1.0);
  R linkPosY = centerY + scaleY * maxPos;
  R linkNegY = centerY - scaleY * maxPos;
  
  dl.addLine(
    m * vec4(linkPosX, linkPosY, 0, 1.0), vec4(0, 0, 0, 1),
    m * vec4(linkNegX, linkNegY, 0, 1.0), vec4(0, 0, 0, 1));

  dl.addLine(
    m * vec4(centerX - 0.05, 0.5, 0, 1.0), vec4(0.267, 0.267, 0.267, 0.8),
    m * vec4(centerX + 0.05, 0.5, 0, 1.0), vec4(0.267, 0.267, 0.267, 0.8));
  dl.addLine(
    m * vec4(0.5, centerY - 0.05, 0, 1.0), vec4(0.267, 0.267, 0.267, 0.8),
    m * vec4(0.5, centerY + 0.05, 0, 1.0), vec4(0.267, 0.267, 0.267, 0.8));

  for (auto &[attach, stiffness, var] : pts) {
    auto jointPosY = 0.5 + scaleY * attach;
    auto jointPosX = 0.5 + scaleX * fit(attach);
    auto jointRootX = 0.25 + scaleX * positions[var->index];

    dl.addLine(
      m * vec4(jointRootX, jointPosY, 0, 1.0), vec4(0.8, 0, 0, 1),
      m * vec4(jointPosX, jointPosY, 0, 1.0), vec4(0.8, 0, 0, 1));

    R ts = 0.01;
    if (1) dl.addTriangle(
      m * vec4(jointPosX-ts, jointPosY+0, 0, 1.0), vec4(0.8, 0, 0, 1),
      m * vec4(jointPosX+ts, jointPosY-ts, 0, 1.0), vec4(0.8, 0, 0, 1),
      m * vec4(jointPosX+ts, jointPosY+ts, 0, 1.0), vec4(0.8, 0, 0, 1));

    if (0) dl.addTriangle(
      m * vec4(jointRootX-ts, jointPosY+0, 0, 1.0), vec4(0.8, 0, 0, 1),
      m * vec4(jointRootX+ts, jointPosY-ts, 0, 1.0), vec4(0.8, 0, 0, 1),
      m * vec4(jointRootX+ts, jointPosY+ts, 0, 1.0), vec4(0.8, 0, 0, 1));

    float weightR = 0.02;
    if (1) dl.addCircle(
      m * vec4(jointRootX, jointPosY, 0, 1.0),
      m * vec4(weightR, 0, 0, 0),
      m * vec4(0, weightR, 0, 0),
      vec4(0, 0, 0.8, 0.8));

    // WRITEME: show stiffness

  }

}


