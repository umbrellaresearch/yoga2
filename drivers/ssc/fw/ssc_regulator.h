#pragma once
#include "common/std_headers.h"

typedef struct ssc_regulator_cmd_t {
  dsp824 setpoint;
} ssc_regulator_cmd_t;

typedef struct ssc_regulator_state_t {
  dsp824 sup_pressure;
  dsp824 exh_pressure;
} ssc_regulator_state_t;
