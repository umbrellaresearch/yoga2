#include "common/std_headers.h"

#include "hwdefs.h"

#include "dspcore/dspcore.h"
#include "./ssc_pneuservo.h"


void ssc_pneuservo_init(ssc_pneuservo_state_t *st)
{
  smoother2_dsp824_setup_butt_5(&st->valve_filter_coeffs);
  smoother2_dsp824_clear(&st->valve_filter_state, DSP824(0.0));

  smoother2_dsp824_setup_butt_5(&st->vel_filter_coeffs);
  smoother2_dsp824_clear(&st->vel_filter_state, DSP824(0.0));
}

void ssc_pneuservo_set_posvel(ssc_pneuservo_state_t *st, dsp824 pos, dsp824 vel)
{
  st->pos = pos;
  st->vel_prefilter = vel;
  st->vel = smoother2_dsp824_run(&st->vel_filter_coeffs, &st->vel_filter_state, dsplim(st->vel_prefilter/16, DSP824(-8.0), DSP824(+8.0)))*16;
}

dsp824 valve_comp(dsp824 v)
{
  if (v > DSP824(0.125)) return v + DSP824(0.125);
  if (v < DSP824(-0.125)) return v - DSP824(0.125);
  return v*2;
}

void ssc_pneuservo_run(ssc_pneuservo_cmd_t *cmd, ssc_pneuservo_state_t *st)
{
  // WRITEME: should be measured atmospheric pressure instead of standard
  st->torque = (mulsat_dsp824_dsp1616_dsp1616(st->ext_pressure - DSP824(0.101), st->ext_pressure_to_torque) +
                mulsat_dsp824_dsp1616_dsp1616(st->ret_pressure - DSP824(0.101), st->ret_pressure_to_torque));

  dsp824 vel_error = st->vel - cmd->desired_vel;
  st->vel_feedback = dsplim(mulsat_dsp824_dsp1616_dsp1616(vel_error, cmd->vel_feedback_coeff),
                            cmd->vel_feedback_lim_lo,
                            cmd->vel_feedback_lim_hi);

  st->vel_feedforward = dsplim(mulsat_dsp824_dsp824_dsp824(cmd->desired_vel, cmd->vel_feedforward_coeff),
                               DSP824(-1), DSP824(+1));

  dsp824 pos_error = st->pos - cmd->desired_pos;
  st->pos_feedback = dsplim(mul_dsp824_dsp824_dsp824(pos_error, cmd->pos_feedback_coeff),
                            cmd->pos_feedback_lim_lo,
                            cmd->pos_feedback_lim_hi);

  dsp1616 torque_error = st->torque - (cmd->desired_torque + st->pos_feedback + st->vel_feedback);
  st->torque_feedback = dsplim(mulsat_dsp1616_dsp824_dsp824(torque_error, cmd->torque_feedback_coeff),
                               cmd->torque_feedback_lim_lo,
                               cmd->torque_feedback_lim_hi);


  st->valve_prefilter = dsplim(cmd->base_valve + st->torque_feedback + st->vel_feedforward, DSP824(-1.25), DSP824(+1.25));
  st->valve = valve_comp(smoother2_dsp824_run(&st->valve_filter_coeffs, &st->valve_filter_state, st->valve_prefilter));
}
