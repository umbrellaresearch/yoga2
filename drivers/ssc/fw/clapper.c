#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_printbuf.h"
#include "embedded/embedded_timing.h"
#include "./clapper.h"

clapper_t clapper0;

void clapper_init(clapper_t *it)
{
  it->segments = 0;
}

void clapper_sync(clapper_t *it)
{
  if (it->segments&0x01) {
    gpio_set_gpio_pin(CLAPPER_SEG1_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG1_PIN);
  }
  if (it->segments&0x02) {
    gpio_set_gpio_pin(CLAPPER_SEG2_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG2_PIN);
  }
  if (it->segments&0x04) {
    gpio_set_gpio_pin(CLAPPER_SEG3_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG3_PIN);
  }
  if (it->segments&0x08) {
    gpio_set_gpio_pin(CLAPPER_SEG4_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG4_PIN);
  }
  if (it->segments&0x10) {
    gpio_set_gpio_pin(CLAPPER_SEG5_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG5_PIN);
  }
  if (it->segments&0x20) {
    gpio_set_gpio_pin(CLAPPER_SEG6_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG6_PIN);
  }
  if (it->segments&0x40) {
    gpio_set_gpio_pin(CLAPPER_SEG7_PIN);
  } else {
    gpio_clr_gpio_pin(CLAPPER_SEG7_PIN);
  }
}

/*
  The 7-segment LED display is mapped to bits as follows:
        01
    20      02
        40
    10      04
        08

 */

static uint8_t digit_table[10] = {
  0x3f,
  0x06,
  0x5b,
  0x4f,
  0x66,
  0x6d,
  0x7d,
  0x07,
  0x7f,
  0x6f
};

void clapper_set_digit(clapper_t *it, int digit)
{
  if (digit >= 0 && digit < 10) {
    it->segments = digit_table[digit];
  } else {
    it->segments = 0;
  }
}

void clapper_set_off(clapper_t *it)
{
  it->segments = 0;
}

void clapper_shutdown(clapper_t *it)
{
  it->segments = 0;
  clapper_sync(it);
}

void clapper_log_status(clapper_t *it, const char *name)
{
  log_printf("%s:  segments=%02x",
             name, it->segments);
}
