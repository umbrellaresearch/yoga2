#pragma once
#include "common/std_headers.h"

#include "hwdefs.h"

#include "../../../embedded/embedded_pktcom.h"

typedef struct ls7266_sample_t {
  U64 sample_ticks;
  S32 encs[LS7266_NCOUNTERS];
  S32 encvels[LS7266_NCOUNTERS];
} ls7266_sample_t;

typedef struct ls7266_t {
  ls7266_sample_t cur;
} ls7266_t;

extern ls7266_t ls72660;

/* internal */
void ls7266_write(ls7266_t *it, int which, bool is_ctl, U8 data);
U8 ls7266_read(ls7266_t *it, int which, bool is_ctl);
void ls7266_write_pr(ls7266_t *it, int which, U8 data);
void ls7266_write_rld(ls7266_t *it, int which, U8 data);
void ls7266_write_cmr(ls7266_t *it, int which, U8 data);
void ls7266_write_ior(ls7266_t *it, int which, U8 data);
void ls7266_write_idr(ls7266_t *it, int which, U8 data);
U8 ls7266_read_flag(ls7266_t *it, int which);
U8 ls7266_read_ol(ls7266_t *it, int which);
void ls7266_zero_counter(ls7266_t *it, int which);

/* Public */
void ls7266_configure(ls7266_t *it, int which);
void ls7266_update(ls7266_t *it, int which, S32 *ctr);

int ls7266_get_sample(ls7266_t *it, ls7266_sample_t *s);
void ls7266_poll(ls7266_t *it);
void ls7266_init(ls7266_t *it);
void ls7266_accept_message(ls7266_t *it, pkt_rx_buf *rx, pkt_tx_buf *tx);
void ls7266_log_status(ls7266_t *it, const char *name);

void pkt_tx_ls7266_sample(pkt_tx_buf *tx, ls7266_sample_t *s);
