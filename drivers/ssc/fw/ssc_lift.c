#include "common/std_headers.h"

#include "hwdefs.h"

#include "dspcore/dspcore.h"
#include "./ssc_lift.h"

void ssc_lift_init(ssc_lift_state_t *st)
{
  smoother2_dsp824_setup_butt_20(&st->valve_filter_coeffs);
  smoother2_dsp824_clear(&st->valve_filter_state, DSP824(0.0));
}


void ssc_lift_run(ssc_lift_cmd_t *cmd, ssc_lift_state_t *st)
{
  // subtract atmospheric from both sides
  st->force = (mulsat_dsp824_dsp1616_dsp1616(st->ext_pressure - DSP824(0.101), cmd->ext_pressure_to_force) +
               mulsat_dsp824_dsp1616_dsp1616(st->ret_pressure - DSP824(0.101), cmd->ret_pressure_to_force));

  dsp824 pos_error = st->pos - cmd->desired_pos;
  st->pos_feedback = dsplim(mulsat_dsp824_dsp824_dsp824(pos_error, cmd->pos_feedback_coeff),
                            cmd->pos_feedback_lim_lo,
                            cmd->pos_feedback_lim_hi);

  dsp1616 force_error = st->force - (cmd->desired_force + st->pos_feedback);
  st->force_feedback = dsplim(mulsat_dsp1616_dsp824_dsp824(force_error, cmd->force_feedback_coeff),
                              cmd->force_feedback_lim_lo,
                              cmd->force_feedback_lim_hi);
  st->valve_prefilter = cmd->base_valve + st->force_feedback;
  st->valve = smoother2_dsp824_run(&st->valve_filter_coeffs, &st->valve_filter_state, st->valve_prefilter);
}
