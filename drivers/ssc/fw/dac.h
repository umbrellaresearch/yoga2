#pragma once

typedef struct dac_t {
  int16_t out_values[N_SSC_DACS];
  bool vpwr_on;
} dac_t;

extern dac_t dac0;

void dac_sync(dac_t *it);
void dac_init(dac_t *it);
void dac_shutdown(dac_t *it);
void dac_log_status(dac_t *it, const char *name);
