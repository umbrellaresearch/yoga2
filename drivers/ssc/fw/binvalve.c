#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_printbuf.h"
#include "embedded/embedded_timing.h"
#include "./binvalve.h"

binvalve_t binvalve0;

void binvalve_init(binvalve_t *it)
{
  for (int i=0; i<N_SSC_BINVALVES; i++) {
    it->out_values[i] = false;
  }
}

void binvalve_sync(binvalve_t *it)
{
#if N_SSC_BINVALVES >= 1
  if (it->out_values[0]) {
    gpio_set_gpio_pin(BINVALVE1_EN_PIN);
  } else {
    gpio_clr_gpio_pin(BINVALVE1_EN_PIN);
  }
#endif
#if N_SSC_BINVALVES >= 2
  if (it->out_values[1]) {
    gpio_set_gpio_pin(BINVALVE2_EN_PIN);
  } else {
    gpio_clr_gpio_pin(BINVALVE2_EN_PIN);
  }
#endif
}

void binvalve_shutdown(binvalve_t *it)
{
  for (int i=0; i<N_SSC_BINVALVES; i++) {
    it->out_values[i] = false;
  }
  binvalve_sync(it);
}

void binvalve_log_status(binvalve_t *it, const char *name)
{
#if N_SSC_BINVALVES == 2
  log_printf("%s:  out=[%d %d]",
             name,
             it->out_values[0], it->out_values[2]);
#endif

}
