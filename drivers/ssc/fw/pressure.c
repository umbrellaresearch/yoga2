#include "common/std_headers.h"
#include "hwdefs.h"

#include "gpio.h"

#include "./pressure.h"
#include "embedded/embedded_debug.h"
#include "./dac.h"
/*
  SSCDANN150PG2A3 doc: http://sensing.honeywell.com/index.php?ci_id=151134

  The devices use I2C. They're connected with common clock pins, but separate data pins, so we can read them all simultaneously.

  Data sheet says max 400 kHz clk rate. It's not more precise about setup times and periods. So I try to keep 1.25 uS both high and low.
  The delays below are tuned by oscilloscope.

  1.25 uS * 2 [periods] * 9 [bits / byte] * 3 [bytes] = 67.5 uS for a complete read cycle

*/

pressure_sense_t pressure0;

#if N_SERCOMS >= 1
#define PRESSURE_GPIOA_MASK 0x0000fffd
#else
#define PRESSURE_GPIOA_MASK 0x0000ffff
#endif

static void ps_clk_1()
{
  gpio_set_gpio_pin(PS_CLK);
}

static void ps_clk_0()
{
  gpio_clr_gpio_pin(PS_CLK);
}

static void ps_sda_z()
{
  AVR32_GPIO.port[0].oderc = PRESSURE_GPIOA_MASK;
}

static void ps_sda_0()
{
  AVR32_GPIO.port[0].ovrc = PRESSURE_GPIOA_MASK;
  AVR32_GPIO.port[0].oders = PRESSURE_GPIOA_MASK;
}

// The nops are necessary with a small number of sensors, or else sensors can't keep up on I2C.
static void ps_sda_read(uint16_t *values)
{
  uint32_t pina = AVR32_GPIO.port[0].pvr;

#if N_SSC_PRESSURE > 0
  values[0 ] = (values[0 ]<<1) | ((pina>>0 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 1
  values[1 ] = (values[1 ]<<1) | ((pina>>1 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 2
  values[2 ] = (values[2 ]<<1) | ((pina>>2 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 3
  values[3 ] = (values[3 ]<<1) | ((pina>>3 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 4
  values[4 ] = (values[4 ]<<1) | ((pina>>4 ) & 1);
#endif
#if N_SSC_PRESSURE > 5
  values[5 ] = (values[5 ]<<1) | ((pina>>5 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 6
  values[6 ] = (values[6 ]<<1) | ((pina>>6 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 7
  values[7 ] = (values[7 ]<<1) | ((pina>>7 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 8
  values[8 ] = (values[8 ]<<1) | ((pina>>8 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 9
  values[9 ] = (values[9 ]<<1) | ((pina>>9 ) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 10
  values[10] = (values[10]<<1) | ((pina>>10) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 11
  values[11] = (values[11]<<1) | ((pina>>11) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 12
  values[12] = (values[12]<<1) | ((pina>>12) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 13
  values[13] = (values[13]<<1) | ((pina>>13) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 14
  values[14] = (values[14]<<1) | ((pina>>14) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
#if N_SSC_PRESSURE > 15
  values[15] = (values[15]<<1) | ((pina>>15) & 1);
#else
  asm("nop; nop; nop; nop; nop; nop");
#endif
}

static void ps_delay()
{
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
}

void pressure_poll(pressure_sense_t *it)
{
  uint16_t tmp_values[N_SSC_PRESSURE]; // 32 bytes on stack
  for (int vi=0; vi<N_SSC_PRESSURE; vi++) {
    tmp_values[vi] = 0;
  }

  ps_sda_0(); // initiate
  ps_delay();
  ps_clk_0();

  uint8_t tx_cmd = (0x28<<1) | 1; // all the sensors have this address

  for (int biti=0; biti<8; biti++) {
    if (tx_cmd&0x80) {
      ps_sda_z();
    } else {
      ps_sda_0();
    }
    ps_delay();
    ps_clk_1();
    ps_delay();
    ps_clk_0();
    tx_cmd <<= 1;
  }
  ps_sda_z();
  ps_delay();

  // secondary ack bit
  ps_clk_1();
  ps_delay();
  ps_clk_0();
  ps_delay();

  for (int biti=0; biti<8; biti++) {
    ps_clk_1();
    ps_sda_read(tmp_values);
    ps_clk_0();
    ps_delay();
  }

  // primary ack bit
  ps_sda_0();
  ps_delay();
  ps_clk_1();
  ps_delay();
  ps_clk_0();
  ps_sda_z();
  ps_delay();

  for (int biti=0; biti<8; biti++) {
    ps_clk_1();
    ps_sda_read(tmp_values);
    ps_clk_0();
    ps_delay();
  }

  // STOP bit
  ps_sda_0();
  ps_delay();
  ps_clk_1();
  ps_delay();
  ps_sda_z();

  for (int vi=0; vi<N_SSC_PRESSURE; vi++) {
    it->values[vi] = tmp_values[vi]; // WRITEME convert?
  }
}



// ----------------------------------------------------------------------

void pressure_init(pressure_sense_t *it)
{
  gpio_enable_gpio_pin(PS_CLK);
  gpio_set_gpio_pin(PS_CLK);

  // set all SDA pins to open drain (there are resistor pullups on them)
  AVR32_GPIO.port[0].ovrs = PRESSURE_GPIOA_MASK;
  AVR32_GPIO.port[0].oders = PRESSURE_GPIOA_MASK;
  AVR32_GPIO.port[0].gpers = PRESSURE_GPIOA_MASK;
}

void pressure_log_status(pressure_sense_t *it, const char *name)
{
#if N_SSC_PRESSURE == 16
  log_printf("%s:  [%04x %04x %04x %04x ...]",
             name,
             it->values[0],
             it->values[1],
             it->values[2],
             it->values[3]);
#elif N_SSC_PRESSURE == 4
  log_printf("%s:  [%04x %04x %04x %04x]",
             name,
             it->values[0],
             it->values[1],
             it->values[2],
             it->values[3]);
#endif
}
