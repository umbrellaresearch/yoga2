#include "common/std_headers.h"
#include "hwdefs.h"
#include "gpio.h"
#include "embedded/embedded_debug.h"
#include "embedded/embedded_printbuf.h"
#include "embedded/embedded_timing.h"
#include "./dac.h"


dac_t dac0;

/*
  The hardware has 47k:10k dividers, and a 2v reference, so full scale should be 11.4v
*/

static inline void ad5328_setup_delay()
{
  asm("nop; nop; nop; nop; nop; nop");
  asm("nop; nop; nop; nop; nop; nop");
}
static inline void ad5328_recovery_delay()
{
  asm("nop; nop; nop; nop;");
  asm("nop; nop; nop; nop;");
}

void dac_write(dac_t *it, uint16_t value)
{
  DAC_DATA_PORT.ovrs = DAC_CLK_MASK; //   gpio_set_gpio_pin(DAC_CLK_PIN);
  DAC_DATA_PORT.ovrc = DAC_NSYNC_MASK; // gpio_clr_gpio_pin(DAC_NSYNC_PIN);
  ad5328_setup_delay();

  for (int biti=0; biti<16; biti++) {

    if (value & 0x8000) {
      DAC_DATA_PORT.ovrs = DAC_DIN0_MASK; // gpio_set_gpio_pin(DAC_DIN0_PIN);
    } else {
      DAC_DATA_PORT.ovrc = DAC_DIN0_MASK; // gpio_clr_gpio_pin(DAC_DIN0_PIN);
    }

    ad5328_setup_delay();
    DAC_DATA_PORT.ovrc = DAC_CLK_MASK; //   gpio_clr_gpio_pin(DAC_CLK_PIN);
    ad5328_setup_delay();
    DAC_DATA_PORT.ovrs = DAC_CLK_MASK; //   gpio_set_gpio_pin(DAC_CLK_PIN);

    value <<= 1;
  }
  DAC_DATA_PORT.ovrs = DAC_NSYNC_MASK; // gpio_set_gpio_pin(DAC_NSYNC_PIN);
}

void dac_init(dac_t *it)
{
  it->vpwr_on = false;

  for (int i=0; i<N_SSC_DACS; i++) {
    it->out_values[i] = 0; // midpoint, 5.0v
  }

  if (0) {
    gpio_enable_gpio_pin(DAC_CLK_PIN);
    gpio_enable_gpio_pin(DAC_NSYNC_PIN);
    gpio_enable_gpio_pin(DAC_DIN0_PIN);
    gpio_enable_gpio_pin(DAC_NLDAC_PIN);
  }

  gpio_set_gpio_pin(DAC_CLK_PIN);
  gpio_set_gpio_pin(DAC_NSYNC_PIN);
  gpio_set_gpio_pin(DAC_DIN0_PIN);
  gpio_set_gpio_pin(DAC_NLDAC_PIN);

  for (int i=0; i<2; i++) {
    dac_write(it,
              (1<<15) |   // cmd mode
              (0<<13) |   // gain/reference
              (0<<4) |    // gain=00
              (0<<2) |    // buf=11
              (0<<0));    // vdd=00

    dac_write(it,
              (1<<15)|
              (1<<13)|  // ldac mode
              (0<<0));  // ldac permanently active

    dac_write(it,
              (1<<15) |
              (2<<13) |   // power down
              (0<<0));

    dac_write(it,
              (1<<15) |
              (3<<13) |   // reset
              (0<<12));   // reset data only
  }
}

void dac_sync(dac_t *it)
{
  for (int daci=0; daci<N_SSC_DACS; daci++) {
    int16_t ov = it->out_values[daci];
    if (ov < 0) ov = 0;
    if (ov > 4095) ov = 4095;
    dac_write(it, (daci<<12) | ov);
  }
  DAC_DATA_PORT.ovrc = DAC_NLDAC_MASK; // gpio_clr_gpio_pin(DAC_NLDAC_PIN);
  ad5328_setup_delay();
  DAC_DATA_PORT.ovrs = DAC_NLDAC_MASK; // gpio_set_gpio_pin(DAC_NLDAC_PIN);

  if (it->vpwr_on) {
    gpio_set_gpio_pin(VPWR_EN_PIN);
  } else {
    gpio_clr_gpio_pin(VPWR_EN_PIN);
  }

}

void dac_shutdown(dac_t *it)
{
  it->vpwr_on = false;
  gpio_clr_gpio_pin(VPWR_EN_PIN);
}

void dac_log_status(dac_t *it, const char *name)
{
#if N_SSC_DACS >= 8
  log_printf("%s:  vpwr=%d out=[%d %d %d %d %d %d %d %d]",
             name,
             (int)it->vpwr_on,
             it->out_values[0],
             it->out_values[1],
             it->out_values[2],
             it->out_values[3],
             it->out_values[4],
             it->out_values[5],
             it->out_values[6],
             it->out_values[7]);
#elif N_SSC_DACS == 1
  log_printf("%s:  vpwr=%d out=[%d]",
             name,
             (int)it->vpwr_on,
             it->out_values[0]);
#endif
}
