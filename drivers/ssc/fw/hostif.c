#include "common/std_headers.h"

#include "hwdefs.h"

#include "embedded/embedded_hostif.h"
#include "embedded/embedded_debug.h"
#include "./hostif.h"
#include "./conf_eth.h"
#include "embedded/avr32/avrhw.h"
#include "embedded/avr32/ethcom.h"
#include "embedded/avr32/test_memory.h"


host_t host0;

// ----------------------------------------------------------------------

void host_handle_pkt_rx(host_t *it, pkt_rx_buf *rx);
void host_send_update(host_t *it);
void host_handle_rx(host_t *it);

void host_fastpoll(host_t *it)
{
  pkt_rx_buf *rx = ethcom_rx_pkt(&ethcom0);
  if (rx) {
    host_handle_pkt_rx(it, rx);
    free_rx_buf(rx);
  }

  if (rate_generator_update(&it->update_rg)) {
    host_send_update(it);
    peer_send_update(it);
    rate_generator_done(&it->update_rg);
  }
}

void host_send_update(host_t *it)
{
  pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &it->update_ep);
  if (!tx) {
    if (1) log_printf("host_send_update: no tx buf");
    return;
  }

  pkt_tx_u8(tx, 'u'); // reply to update
  pkt_tx_u64(tx, get_ticks64()); // precise timestamp. It'd be better to put this in later
  int orig_tx_len = tx->len;

  host_fill_update(it, tx);

  // If this starts failing, reduce the maximum sample count per packet.
  assert (tx->len < 1450);

  if (tx->len > orig_tx_len) {
    ethcom_tx_pkt(&ethcom0, tx);
    ethcom_poll(&ethcom0);
  }
  else {
    free_tx_buf(tx);
  }
}


void host_handle_pkt_rx(host_t *it, pkt_rx_buf *rx)
{
  int orig_rdpos = rx->rdpos;
  while (!rx->overrun && pkt_rx_remaining(rx)>0) {
    U8 cmd = pkt_rx_u8(rx);

    if (0) debug_printf("host_handle_pkt_rx cmd=%02x", cmd);

    if (cmd == '\n') {
      // ignore
    }
    else if (cmd == '|') { // Echo
      pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &rx->src);
      if (!tx) {
        log_printf("reply to |: no tx buf");
        return;
      }
      pkt_tx_u8(tx, '|');
      while (pkt_rx_remaining(rx) > 0) {
        pkt_tx_u8(tx, pkt_rx_u8(rx));
      }
      ethcom_tx_pkt(&ethcom0, tx);
    }

    else if (cmd == '?') {
      test_memory();
    }

    else if (cmd == 'U') {
      rate_generator_enable(&it->update_rg, SECONDSTOTICKS(0.001), 4);
      rate_generator_set_budget(&it->update_rg, 1000);
      it->update_ep = rx->src;
    }

    else if (host_handle_cmd(it, cmd, rx)) {
    }

    else if (cmd=='Q') {
      if (pkt_rx_u8(rx) != '3') return;
      if (pkt_rx_u8(rx) != 'b') return;
      if (pkt_rx_u8(rx) != '0') return;
      if (pkt_rx_u8(rx) != '0') return;
      if (pkt_rx_u8(rx) != 't') return;

      ethcom_shutdown(&ethcom0);

      host_shutdown(it);
      avrhw_reset();
    }

    else {
      goto bad_request;
    }
  }
  return;

 bad_request:
  log_printf("hostif: Unknown packet starting %02x %02x (%d/%d/%d) [byte was %02x]", rx->buf[orig_rdpos], rx->buf[orig_rdpos+1], orig_rdpos, rx->rdpos, rx->len, rx->buf[rx->rdpos-1]);
}

void host_log_status(host_t *it, char const *name)
{

}

// ----------------------------------------------------------------------

#ifdef notyet
void handle_coap(pkt_rx_buf *rx)
{
  pkt_tx_buf *tx = NULL;
  if (pkt_rx_remaining(rx) < 4) {
    log_printf("coap: too short %d", (int)pkt_rx_remaining(rx));
    goto reply_rst;
  }
  coap_hdr_t hdr;
  hdr.b0 = pkt_rx_u8(rx);
  hdr.verType = (hdr.b0 >> 4) & 0x0f;
  hdr.tokenLen = (hdr.b0 >> 0) & 0x0f;
  if (hdr.tokenLen > 8) {
    log_printf("coap: Bad token length %d", (int)hdr.tokenLen);
    goto reply_rst;
  }
  hdr.code = pkt_rx_u8(rx);
  hdr.codeClass = (hdr.code>>5)&0x07;
  hdr.codeDetail = (hdr.code>>0)&0x1f;
  hdr.msgId = pkt_rx_u16(rx);

  for (int i=0; i<hdr.tokenLen; i++) {
    hdr.token[i] = pkt_rx_u8(rx);
  }
  hdr.payloadStart = pkt_rx_u8(rx);

  if (verType == 0x04) {
    if (1) log_printf("CON [0x%04x] %d.%d",
                      (int)msgId,
                      (int)codeClass, (int)codeDetail);
    tx = ethcom_start_tx_udp(&ethcom0, &rx->src);
    if (!tx) {
      log_printf("coap CON: Out of tx buffers");
      return;
    }
    pkt_tx_u8(tx, 0x60); // ACK
    pkt_tx_u8(tx, (2<<5) | 0); // 200 OK
    pkt_tx_u16(tx, msgId);
    for (int i=0; i<tokenLen; i++) {
      pkt_tx_u8(tx, token[i]);
    }
    pkt_tx_u8(0xff);
    handle_coap_msg(codeClass, codeDetail, rx, tx);
    ethcom_tx_pkt(&ethcom0, tx);
  }
  else if (verType == 0x05) {
    if (1) log_printf("NON [0x%04x] %d.%d",
                      (int)msgId,
                      (int)codeClass, (int)codeDetail);

  }
  else if (verType == 0x06) {
    if (1) log_printf("ACK [0x%04x] %d.%d",
                      (int)msgId,
                      (int)codeClass, (int)codeDetail);

  }
  else if (verType == 0x07) {
    if (1) log_printf("RST [0x%04x] %d.%d",
                      (int)msgId,
                      (int)codeClass, (int)codeDetail);

  }


 reply_rst:
  if (!tx) {
    tx = ethcom_start_tx_udp(&ethcom0, &rx->src);
    if (!tx) {
      log_printf("coap CON: Out of tx buffers");
      return;
    }
    pkt_tx_u8(tx, 0x70); // RST

  }

}
#endif
