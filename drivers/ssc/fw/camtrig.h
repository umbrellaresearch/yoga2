#pragma once

typedef struct camtrig_t {
} camtrig_t;

extern camtrig_t camtrig0;

void camtrig_init(camtrig_t *it);
void camtrig_trigger(camtrig_t *it, uint8_t trigi, uint8_t active);
void camtrig_clear(camtrig_t *it);
void camtrig_shutdown(camtrig_t *it);
