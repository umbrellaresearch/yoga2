#pragma once

typedef struct beeper_t {
  U16 beeper_mask, rled_mask, gled_mask, bled_mask;
} beeper_t;

extern beeper_t beeper0;

void beeper_set(beeper_t *it, U16 mask);
void led_set(beeper_t *it, U16 rmask, U16 gmask, U16 bmask);

void beeper_poll(beeper_t *it);
void beeper_setup();
