
typedef struct coap_hdr_s {

  U8 b0;
  U8 verType;
  U8 tokenLen;
  U8 code;
  U8 codeClass;
  U8 codeDetail;
  U16 msgId;
  U8 token[8];
  U8 payloadStart;

} coap_hdr_t;
