#pragma once
#include "common/std_headers.h"

#include "dspcore/smootherdsp.h"

typedef struct ssc_pneuservo_cmd_t {
  dsp824 base_valve;
  dsp1616 desired_torque;
  dsp824 desired_pos;
  dsp824 desired_vel;
  dsp824 torque_feedback_coeff;
  dsp824 torque_feedback_lim_lo, torque_feedback_lim_hi;
  dsp1616 pos_feedback_coeff;
  dsp1616 pos_feedback_lim_lo, pos_feedback_lim_hi;
  dsp1616 vel_feedback_coeff;
  dsp1616 vel_feedback_lim_lo, vel_feedback_lim_hi;
  dsp824 vel_feedforward_coeff;
} ssc_pneuservo_cmd_t;

typedef struct ssc_pneuservo_state_t {
  dsp824 ext_pressure, ret_pressure;
  dsp1616 ext_pressure_to_torque, ret_pressure_to_torque;
  dsp1616 torque;
  dsp824 pos;

  dsp824 vel_prefilter;
  smoother2_state_dsp824 vel_filter_state;
  smoother2_coeffs_dsp824 vel_filter_coeffs;
  dsp824 vel;

  dsp824 torque_feedback;
  dsp1616 pos_feedback;
  dsp1616 vel_feedback;
  dsp824 vel_feedforward;

  dsp824 valve_prefilter;
  smoother2_state_dsp824 valve_filter_state;
  smoother2_coeffs_dsp824 valve_filter_coeffs;
  dsp824 valve;
} ssc_pneuservo_state_t;

void ssc_pneuservo_init(ssc_pneuservo_state_t *st);
void ssc_pneuservo_run(ssc_pneuservo_cmd_t *cmd, ssc_pneuservo_state_t *st);
void ssc_pneuservo_set_posvel(ssc_pneuservo_state_t *st, dsp824 pos, dsp824 vel);
