#include "common/std_headers.h"

#include "gpio.h"
#include "hwdefs.h"

#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"
#include "./ls7266.h"

ls7266_t ls72660;

/*
  My distillation of the complicated timing constraints in the LS7266 spec sheet:
  write:
    It latches data on the rising edge of /WR. Everything must be stable 45 ns before and 10 ns after that.
    WR pulse width must be 45 ns.
    We must hold the address and data for 10 ns after we've deasserted /WR

    6 clocks should be enough for everything

  read:
    We must latch data at least 80 ns after we've asserted /RD
    We must hold the address for 10 ns after we've deasserted /RD

   The AVR32 inserts its own 4 clock pipeline delay between writes & read availability. I think those
   are PBA clocks in this case, which are 1/4 the frequency of the CPU clock. So we need 16 more cycles.

   In any case 24 cycles is enough for all reads


  either:
    We must wait 90 ns before beginning another cycle

*/

inline void ls7266_write_delay()
{
  asm("nop; nop; nop; nop; nop; nop");
}
inline void ls7266_read_delay()
{
  asm("nop; nop; nop; nop");
  asm("nop; nop; nop; nop");
  asm("nop; nop; nop; nop");
  asm("nop; nop; nop; nop");
  asm("nop; nop; nop; nop");
}
inline void ls7266_recovery_delay()
{
  asm("nop; nop; nop; nop;");
}

static void ls7266_select(ls7266_t *it, int which, bool is_ctl, bool is_write)
{
  if (is_ctl) {
    AVR32_GPIO.port[LS7266_CND_PIN>>5].ovrs = (1<<(LS7266_CND_PIN&0x1f));
  } else {
    AVR32_GPIO.port[LS7266_CND_PIN>>5].ovrc = (1<<(LS7266_CND_PIN&0x1f));
  }

  if (which&1) {
    AVR32_GPIO.port[LS7266_YNX_PIN>>5].ovrs = (1<<(LS7266_YNX_PIN&0x1f));
  } else {
    AVR32_GPIO.port[LS7266_YNX_PIN>>5].ovrc = (1<<(LS7266_YNX_PIN&0x1f));
  }

  switch(which>>1) {
  case 0:
    AVR32_GPIO.port[LS7266_NCS0_PIN>>5].ovrc = (1<<(LS7266_NCS0_PIN&0x1f));
    break;
#if LS7266_NCOUNTERS > 2
  case 1:
    AVR32_GPIO.port[LS7266_NCS1_PIN>>5].ovrc = (1<<(LS7266_NCS1_PIN&0x1f));
    break;
#endif
#if LS7266_NCOUNTERS > 4
  case 2:
    AVR32_GPIO.port[LS7266_NCS2_PIN>>5].ovrc = (1<<(LS7266_NCS2_PIN&0x1f));
    break;
#endif
  }

  if (is_write) {
    AVR32_GPIO.port[LS7266_NWR_PIN>>5].ovrc = (1<<(LS7266_NWR_PIN&0x1f));
  } else {
    AVR32_GPIO.port[LS7266_NRD_PIN>>5].ovrc = (1<<(LS7266_NRD_PIN&0x1f));
  }
}

static void ls7266_unselect(ls7266_t *it, int which)
{
  switch(which>>1) {
  case 0:
    AVR32_GPIO.port[LS7266_NCS0_PIN>>5].ovrs = (1<<(LS7266_NCS0_PIN&0x1f));
    break;
#if LS7266_NCOUNTERS > 2
  case 1:
    AVR32_GPIO.port[LS7266_NCS1_PIN>>5].ovrs = (1<<(LS7266_NCS1_PIN&0x1f));
    break;
#endif
#if LS7266_NCOUNTERS > 4
  case 2:
    AVR32_GPIO.port[LS7266_NCS2_PIN>>5].ovrs = (1<<(LS7266_NCS2_PIN&0x1f));
    break;
#endif
  }

  AVR32_GPIO.port[LS7266_NWR_PIN>>5].ovrs = (1<<(LS7266_NWR_PIN&0x1f));
  AVR32_GPIO.port[LS7266_NRD_PIN>>5].ovrs = (1<<(LS7266_NRD_PIN&0x1f));
}


void ls7266_write(ls7266_t *it, int which, bool is_ctl, U8 data)
{
  if (0) profts("+ls7266_write", which);

  volatile avr32_gpio_port_t *data_port = LS7266_DATA_PORT;

  data_port->ovrc = ((U32)~data) << LS7266_DATA_SHIFT; // clear zero bits
  data_port->ovrs = ((U32)data) << LS7266_DATA_SHIFT; // set one bits
  data_port->oders = ((U32)0xff) << LS7266_DATA_SHIFT; // set output mode

  ls7266_select(it, which, is_ctl, true);

  ls7266_write_delay();

  ls7266_unselect(it, which);

  ls7266_recovery_delay();

  data_port->oderc = ((U32)0xff) << LS7266_DATA_SHIFT; // clear output mode
  if (0) profts("-ls7266_write", 0);
}

U8 ls7266_read(ls7266_t *it, int which, bool is_ctl)
{
  if (0) profts("+ls7266_read", which);

  volatile avr32_gpio_port_t *data_port = LS7266_DATA_PORT;

  ls7266_select(it, which, is_ctl, false);

  ls7266_read_delay();

  U8 ret = (U8)(data_port->pvr >> LS7266_DATA_SHIFT);

  ls7266_unselect(it, which);

  ls7266_recovery_delay();

  if (0) profts("-ls7266_read", 0);
  return ret;
}

void ls7266_write_pr(ls7266_t *it, int which, U8 data)
{
  ls7266_write(it, which, 0, data);
}
void ls7266_write_rld(ls7266_t *it, int which, U8 data)
{
  ls7266_write(it, which, 1, (data&0x1f)|0x00);
}
void ls7266_write_cmr(ls7266_t *it, int which, U8 data)
{
  ls7266_write(it, which, 1, (data&0x1f)|0x20);
}
void ls7266_write_ior(ls7266_t *it, int which, U8 data)
{
  ls7266_write(it, which, 1, (data&0x1f)|0x40);
}
void ls7266_write_idr(ls7266_t *it, int which, U8 data)
{
  ls7266_write(it, which, 1, (data&0x1f)|0x60);
}
U8 ls7266_read_flag(ls7266_t *it, int which)
{
  return ls7266_read(it, which, 1);
}
U8 ls7266_read_ol(ls7266_t *it, int which)
{
  return ls7266_read(it, which, 0);
}

// ----------------------------------------------------------------------


void ls7266_zero_counter(ls7266_t *it, int which)
{
  ls7266_write_rld(it, which, 0x01); // reset bp
  ls7266_write_pr(it, which, 0);
  ls7266_write_pr(it, which, 0);
  ls7266_write_pr(it, which, 0);
  ls7266_write_rld(it, which, 0x08); // transfer PR to CNTR
}

void ls7266_configure(ls7266_t *it, int which)
{
  if (0) ls7266_write_rld(it, which, 0x02); // reset cntr
  ls7266_write_rld(it, which, 0x04); // reset BT, CT, CPT, S
  ls7266_write_rld(it, which, 0x06); // reset E

  ls7266_write_rld(it, which, 0x01); // reset bp
  /*
    I don't know how to choose a prescale value. Presumably if the
    frequency is too low, it'll screw up when the wheel velocity gets
    too high. But higher frequencies must be more susceptible to
    noise. The encs are clocked at 10 MHz, so with a prescale of 2
    we should have a max count rate of 5.0 MHz. 8000 RPM of the motor
    corresponds to 1.1 million counts/sec (or 270 kHz signals) with an
    8192-count wheel. It says you want fFCKn >= 8*fQA, and I think we
    have fFCKn > 18 fQA.

    It would majorly suck if we hit some upper frequency limit, either
    because of the counters or because of some limit in the optical
    encoders. We'd probably detect a large velocity and yaw error and
    wipe out at high speed. The HEDS module has HCMOS-type drivers
    (sink or source 8 mA) so 270 kHz shouldn't be a problem.

    I should probably change to lower-resolution encoders anyway. And
    make sure to use LS series for maximum noise immunity (the HEDS
    outputs TTL logic levels.)

    The ENC already has appropriate (TTL) input thresholds for the
    encoders.
  */
  ls7266_write_pr(it, which, 1);     // prescale by 2.
  ls7266_write_rld(it, which, 0x18); // transfer PR0 to PSC

  ls7266_write_cmr(it, which, 0x18);  // quadrature x4, normal, binary

  ls7266_write_ior(it, which, 0x01);  // enable A&B, LCTNR, RCNTR, /carry & /borrow

  ls7266_write_idr(it, which, 0x00);  // disable index

  if (0) ls7266_zero_counter(it, which);
}


void ls7266_init(ls7266_t *it)
{
  gpio_clr_gpio_pin(LS7266_YNX_PIN);
  gpio_set_gpio_pin(LS7266_NRD_PIN);
  gpio_set_gpio_pin(LS7266_NWR_PIN);
  gpio_clr_gpio_pin(LS7266_CND_PIN);

  gpio_set_gpio_pin(LS7266_NCS0_PIN);
#if LS7266_NCOUNTERS > 2
  gpio_set_gpio_pin(LS7266_NCS1_PIN);
#endif
#if LS7266_NCOUNTERS > 4
  gpio_set_gpio_pin(LS7266_NCS2_PIN);
#endif

  for (int i=0; i<8; i++) {
    gpio_enable_gpio_pin(LS7266_D0_PIN + i);
    gpio_enable_pin_pull_up(LS7266_D0_PIN + i);
  }

  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    ls7266_configure(it, i);
  }

}

void ls7266_update(ls7266_t *it, int which, S32 *ctr)
{
  /*
    You read it by setting a couple bits in the RLD register to transfer the counters to
    the output latch, then you read 3 bytes from the data port.
  */
  ls7266_write_rld(it, which, 0x01 | 0x10); // reset bp, transfer counters to OL
  // LSB first
  U8 newb0 = ls7266_read(it, which, 0);
  U8 newb1 = ls7266_read(it, which, 0);
  U8 newb2 = ls7266_read(it, which, 0);

  // Logic to extend the 24-bit counter into 32 bits
  U8 oldb2 = ((*ctr)>>16) & 0xff;
  U8 oldb3 = ((*ctr)>>24) & 0xff;

  S8 dir = (S8)(newb2 - oldb2);
  U8 newb3;
  if (dir<0  && !(oldb2 & 0x80) && (newb2 & 0x80)) {
    newb3 = oldb3 - 1;
  }
  else if (dir>0 && (oldb2 & 0x80) && !(newb2 & 0x80)) {
    newb3 = oldb3 + 1;
  }
  else {
    newb3 = oldb3;
  }

  *ctr = (((U32)newb0 << 0) |
          ((U32)newb1 << 8) |
          ((U32)newb2 << 16) |
          ((U32)newb3 << 24));
}

void ls7266_poll(ls7266_t *it)
{
  ls7266_sample_t new_sample = it->cur;
  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    ls7266_update(it, i, &new_sample.encs[i]);
  }
  new_sample.sample_ticks = get_ticks64();

  U32 interval = (U32)(new_sample.sample_ticks - it->cur.sample_ticks);
  if (interval < SECONDSTOTICKS(0.0001)) interval = SECONDSTOTICKS(0.0001);
  if (interval > SECONDSTOTICKS(0.01)) interval = SECONDSTOTICKS(0.01);
  S32 inv_interval = SECONDSTOTICKS(32.0) / interval;

  // ensure calcs below don't overflow
  S32 maxdelta = 1000000000 / inv_interval;

  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    S32 delta = new_sample.encs[i] - it->cur.encs[i];
    if (delta > maxdelta) delta = maxdelta;
    if (delta < -maxdelta) delta = -maxdelta;

    new_sample.encvels[i] = ((new_sample.encs[i] - it->cur.encs[i]) * inv_interval) / 32;
  }

  it->cur = new_sample;
}

// ======================================================================

void ls7266_log_status(ls7266_t *it, const char *name)
{
  log_printf("%s: encs=["
             "%d(d%+d) %d(d%+d)"
#if LS7266_NCOUNTERS > 2
             " %d(d%+d) %d(d%+d)"
#endif
#if LS7266_NCOUNTERS > 4
             " %d(d%+d) %d(d%+d)"
#endif
             "]",
             name,
             it->cur.encs[0], it->cur.encvels[0],
             it->cur.encs[1], it->cur.encvels[1]
#if LS7266_NCOUNTERS > 2
             ,
             it->cur.encs[2], it->cur.encvels[2],
             it->cur.encs[3], it->cur.encvels[3]
#endif
#if LS7266_NCOUNTERS > 4
             ,
             it->cur.encs[4], it->cur.encvels[4],
             it->cur.encs[5], it->cur.encvels[5]
#endif
             );
}

void pkt_tx_ls7266_sample(pkt_tx_buf *tx, ls7266_sample_t *s)
{
  pkt_tx_u64p(tx, &s->sample_ticks);
  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    pkt_tx_s32p(tx, &s->encs[i]);
  }
  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    pkt_tx_s32p(tx, &s->encvels[i]);
  }
}
