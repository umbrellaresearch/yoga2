#pragma once
#include "../../../embedded/embedded_pktcom.h"

typedef struct host_t {
  rate_generator update_rg;
  pkt_endpoint update_ep;
  U64 host_timestamp;
  U64 host_timestamp_ticks;
  U64 last_ls72660_sample_ticks;
  U64 last_ads83440_sample_ticks;
  U64 last_leg0_update_ticks;
  U64 last_foot0_update_ticks;
  U64 last_harness0_update_ticks;

  pkt_endpoint peer_leg_ep; // Foot has sensors that leg needs
  U64 peer_leg_last_foot0_update_ticks;

} host_t;

extern host_t host0;

void host_fastpoll(host_t *it);
void host_init(host_t *it);
void host_log_status(host_t *it, char const *name);

// User should implement
void host_fill_update(host_t *it, pkt_tx_buf *tx);
void peer_send_update(host_t *it);
U8 host_handle_cmd(host_t *it, U8 cmd, pkt_rx_buf *rx);
void host_shutdown(host_t *it);
