# SSC Communication system

SSC is the embedded hardware & communication architecture around the robot. Embedded boards
communicate with the host and with each other over UDP / IP/ Ethernet.

The boards (like ssc_leg, ssc_foot, ssc_gyro) each have an AVR32 microcontroller with an ethernet
interface. See ssc_gyro/hw/ssc_gyro.sch for a schematic. The ethernet hardware is managed by
avr32stuff/ethcom.c and avr32stuff/macb.c.

On the host side, a subclass of GenericEngine (such as SscLegNetworkEngine) opens a UDP port to one
of the ssc_leg boards and requests updates by sending a 'U' packet.

Boards can also communicate with each other. Currently, each ssc_foot board sends the ankle position
sensors to the corresponding ssc_leg board for use in servo feedback loops.

Boards can log to the syslogd on the host. Calling log_printf is the most convenient interface. When
a trace is running, a thread watches /var/log/syslog and adds anything that appears there to the
console channel of the trace.

When boards power up or reset, they go to a bootloader (code is in ssc/bootloader and
avr32stuff/bootloader.c). This code requests the real firmware from the host over BOOTP. The
firmware is installed where BOOTP can find it (in /tftpboot/) by running 'make install' in the
ssc_*/fw/ directory. They can be asked to reboot so they'll notice the new firmware by sending a
special packet by running 'make reboot'.

Boards have a deterministic MAC and IP address. MACs are 02:69:11:04:00:&lt;netid>, and IPs
are 172.17.10.&lt;netid>. The deterministic MAC address is used when communicating peer-peer, to avoid
having to ARP. The netids are defined in ssc/fw/ssc_makefile.

There are multiple instances of some boards, like there are 2 ssc_leg and ssc_foot boards (for left
and right). These are compiled separately, with -DBOARD_INSTANCE=0 or =1, respectively.

I haven't fully explored the limits of how often boards can update each other and the host. Most
updates now happen at 1000 Hz. 

In a previous robot codebase, I made all the peripheral stuff very generic and configurable at
runtime. Now the structure of the robot and names of joints, are all baked into the code. It seems
easier to understand and modify this way.

