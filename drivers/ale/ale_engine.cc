#include "../../../comm/engine.h"
#include "../../../timeseq/trace_blobs.h"
#include "./ale_types.h"
#include "../jit/runtime.h"
#ifdef USE_ALE
#include <ale/ale_interface.hpp>
using namespace ale;

struct AleReusable {
  string key;
  shared_ptr<ALEInterface> ale;
  ALEState initState;
};

struct AleEngine : GenericEngine {
  void update(EngineUpdateContext &ctx) override;
  void beforeSetRunning() override;
  void afterSetStopped() override;

  string romName;
  int mode{0};
  int difficulty{0};

  R totReward{0.0};
  U64 curChunkId{0};

  shared_ptr<YogaTimeseq> videoOut;
  shared_ptr<YogaTimeseq> statusOut;
  shared_ptr<YogaTimeseq> actionIn;

  shared_ptr<AleReusable> env;
};

// Must match types in ./ale_types.h
static YogaCodeRegister registerAleTypes("ale_engine.cc", R"(
  struct AleStatus {
    R reward;
    R score;
    R lives;
    R gameover;
  };

  struct AleAction {
    R fire;
    R[2] joy;
  };

  struct AleImage {
    U8[160, 210] pixels; // 160 * 210
  };
)");

Action toAction(YogaValue const &yv)
{
  auto cv = reinterpret_cast<YogaAleAction *>(yv.buf);
  bool f = cv->fire > 0.5;
  R x = cv->joy[0];
  R y = cv->joy[1];
  if (abs(x) < 0.5) x = 0.0;
  if (abs(y) < 0.5) y = 0.0;

  // Hard to fathom the numerical action encoding ALE uses. So just write it out.
  if (!f) {
    if (x==0 && y==0) return PLAYER_A_NOOP;

    if (x>0 && y==0) return PLAYER_A_RIGHT;
    if (x<0 && y==0) return PLAYER_A_LEFT;
    if (x==0 && y>0) return PLAYER_A_UP;
    if (x==0 && y<0) return PLAYER_A_DOWN;

    if (x>0 && y>0) return PLAYER_A_UPRIGHT;
    if (x>0 && y<0) return PLAYER_A_DOWNRIGHT;
    if (x<0 && y>0) return PLAYER_A_UPLEFT;
    if (x<0 && y<0) return PLAYER_A_DOWNLEFT;
  }
  else {
    if (x==0 && y==0) return PLAYER_A_FIRE;

    if (x>0 && y==0) return PLAYER_A_RIGHTFIRE;
    if (x<0 && y==0) return PLAYER_A_LEFTFIRE;
    if (x==0 && y>0) return PLAYER_A_UPFIRE;
    if (x==0 && y<0) return PLAYER_A_DOWNFIRE;

    if (x>0 && y>0) return PLAYER_A_UPRIGHTFIRE;
    if (x>0 && y<0) return PLAYER_A_DOWNRIGHTFIRE;
    if (x<0 && y>0) return PLAYER_A_UPLEFTFIRE;
    if (x<0 && y<0) return PLAYER_A_DOWNLEFTFIRE;
  }
  throw runtime_error("Invalid action " + repr(yv));
}

std::mutex aleReusableMutex;
map<string, std::vector<shared_ptr<AleReusable>>> aleReusables;

static shared_ptr<AleReusable> getReuseAle(string const &key)
{
  unique_lock<std::mutex> l1(aleReusableMutex);

  auto &v = aleReusables[key];
  if (v.empty()) return nullptr;
  auto ret = v.back();
  v.pop_back();
  ret->ale->restoreSystemState(ret->initState);
  return ret;
}

static void putReuseAle(shared_ptr<AleReusable> p)
{
  unique_lock<std::mutex> l1(aleReusableMutex);

  auto &v = aleReusables[p->key];
  v.push_back(std::move(p));
  if (0) cerr << "aleReusables count: " + repr(v.size()) + "\n";
}

void AleEngine::beforeSetRunning()
{
  auto t0 = clock();

  auto aleReuseKey = romName + "|" + repr(mode) + "|" + repr(difficulty);
  env = getReuseAle(aleReuseKey);
  if (!env) {
    if (verbose >= 2) {    
      cerr << "Initializing ALEINterface\n";
      Logger::setMode(Logger::Info); // only warnings and errors
    }
    else {
      Logger::setMode(Logger::Warning); // only warnings and errors
    }
    env = make_shared<AleReusable>();
    env->key = aleReuseKey;
    env->ale = make_shared<ALEInterface>();
    env->ale->loadROM(romName);

    if (verbose >= 2) cerr << "Game modes: " + repr(env->ale->getAvailableModes()) + "\n";

    env->ale->setMode(game_mode_t(mode));
    env->ale->setDifficulty(difficulty);
    env->ale->act(PLAYER_A_FIRE); // start
    env->initState = env->ale->cloneSystemState();
  }

  auto t1 = clock();
  if (verbose >= 2) cerr << "Start AleEngine: " + repr_clock(t1-t0) + "\n";
}

void AleEngine::afterSetStopped()
{
  if (env) {
    putReuseAle(std::move(env));
    env = nullptr;
  }
}

void AleEngine::update(EngineUpdateContext &ctx)
{
  if (actionIn && statusOut && env && !env->ale->game_over()) {
    auto action0 = actionIn->getLast();
    auto status0 = statusOut->getLast();
    if (action0.ts >= status0.ts) {

      auto ts1 = action0.ts;

      auto reward = 0.0;
      for (int i=0; i < 2; i++) {
        reward += env->ale->act(toAction(action0));
        ts1 += 1.0/60.0;
      }
      totReward += reward;
      
      auto status = statusOut->addNew(ts1);
      auto cstatus = reinterpret_cast<YogaAleStatus *>(status.buf);

      cstatus->reward = reward;
      cstatus->score = totReward;
      cstatus->lives = env->ale->lives();
      cstatus->gameover = env->ale->game_over();
      //cstatus->frameTime = env->getEpisodeFrameNumber() / 60.0;

      auto &aleScreen = env->ale->getScreen();
      assert(aleScreen.width() == 160);
      assert(aleScreen.height() == 210);

      auto video = videoOut->addNew(ts1);
      if (video.type->llAllocSize != 210*160) throw runtime_error("Wrong-sized AleImage");
      auto cvideo = reinterpret_cast<YogaAleImage *>(video.buf);
      memcpy(cvideo->pixels, aleScreen.getArray(), video.type->llAllocSize);

      if (cstatus->gameover) {
        rti->hardHalt = true;
      }
      ctx.updateCnt ++;
    }
  }

}


static EngineRegister AleEngineReg(
  "ale",
  {
    "AleStatus",
    "AleImage",
    "AleAction"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 3) {
      return ctx.logError("ale: expected 3 arguments (action, status, video)");
    }

    auto e = make_shared<AleEngine>();
    if (args.size() > 0) {
      e->statusOut = args[0];
    }
    if (args.size() > 1) {
      e->videoOut = args[1];
    }
    if (args.size() > 2) {
      e->actionIn = args[2];
    }

    if (!e->setEngineConfig(ctx, spec)) return false;

    if (!spec->options->getValueForKey("rom", e->romName)) {
      return ctx.logError("Bad rom");
    }
    if (!spec->options->getValueForKey("mode", e->mode)) {
      return ctx.logError("Bad mode");
    }
    if (!spec->options->getValueForKey("difficulty", e->difficulty)) {
      return ctx.logError("Bad mode");
    }

    trace->addEngine(engineName, e, true);

    return true;
  },
  false
);

void findPixel(U8  * const *outArgs, U8 const * const *inArgs, R const *params, R *astonishment, R dt, apr_pool_t *mem)
{
  auto &image = *reinterpret_cast<YogaAleImage const *>(inArgs[0]);
  auto &desiredColor = *reinterpret_cast<R const *>(inArgs[1]);
  auto &minCoord = *reinterpret_cast<glm::dvec2 const *>(inArgs[2]);
  auto &maxCoord = *reinterpret_cast<glm::dvec2 const *>(inArgs[3]);
  auto &coords = *reinterpret_cast<glm::dvec2 *>(outArgs[0]);

  int minY = max(int(minCoord.y), 0), maxY = min(int(maxCoord.y), 210);
  int minX = max(int(minCoord.x), 0), maxX = min(int(maxCoord.x), 160);
  U8 searchpix = int(desiredColor);

  for (int y = minY; y < maxY; y++) {
    auto rowpixels = &image.pixels[y][0];
    for (int x = minX; x < maxX; x++) {
      if (rowpixels[x] == searchpix) {
        coords.y = y;
        coords.x = x;
        return;
      }
    }
  }
}

void findPixelDebug(U8  * const *outArgs, U8 const * const *inArgs, R const *params, R *astonishment, R dt, U8 *debugLocals, apr_pool_t *mem)
{
  findPixel(outArgs, inArgs, params, astonishment, dt, mem);
}


static YogaForeignRegister findPixelReg("ale_engine.cc", R"(
  function findPixel(
    out R[2] coords,
    in AleImage image,
    in R desiredColor,
    in R[2] minCoord,
    in R[2] maxCoord)
)", findPixel, findPixelDebug);



#else

static EngineRegister AleEngineReg(
  "ale",
  {},
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args) {
    return ctx(spec).logError("ALE (Arcade Learning Environment) wasn't compiled in. See mk/makedefs.inc");
  },
  false
);

#endif
