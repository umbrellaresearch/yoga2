#pragma once
#include "../../jit/jit_utils.h"

// These must match the yoga types in ./ale_engine.cc

struct YogaAleStatus {
  R reward;
  R score;
  R lives;
  R gameover;
};

struct YogaAleAction {
  R fire;
  R joy[2];
};

struct YogaAleImage {
  U8 pixels[210][160];
};
