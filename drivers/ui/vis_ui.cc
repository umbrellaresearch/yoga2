#include "../studio/trace_render.h"
#include "../studio/panel_render.h"
#include "./ui.h"
#include "./ui_types.h"

struct ScopeTraceRendererKeyboard : ScopeTraceRendererStruct {
  ScopeTraceRendererKeyboard(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr)
    : ScopeTraceRendererStruct(_m, _tr)
  {
  }

  void drawValues(RenderQueue &q) override
  {
    R textY = lo.plotT + 2;
    for (auto &alt : m.alts(tr.spec.ref)) {
      if (alt.isSecondary) continue;
      auto yv = alt.getValue(tr.spec.ref, m.visTime);
      if (!yv) continue;

      auto cppKbd = reinterpret_cast<YogaKeyboard *>(yv.buf);

      string valstr;
      for (int i=0; i<yogaKeyboardScanCodes.size(); i++) {
        auto [scanCode, keyName] = yogaKeyboardScanCodes[i];
        if (cppKbd->down[i] >= 0.5) valstr += keyName;
      }

      drawText(lo.infoX, textY,
        IM_COL32(0x00, 0x00, 0x00, 0xff),
        "keys = ["s + valstr + "]");
    }
  }

  void doAutoScale() override
  {
    tr.scale = 1.0;
  }
  bool positiveOnly() override
  {
    return true;
  }

};

static ScopeTraceRendererRegister regKeyboard("YogaKeyboard", 
  [](ScopeModel &m, ScopeModelVisTrace &tr)
  {
    tr.renderer = make_shared<ScopeTraceRendererKeyboard>(m, tr);
  }
);





struct ScopePanelRendererKeyboard : ScopePanelRenderer {
  ScopePanelRendererKeyboard(
    ScopeModel &_m, ScopeModelVisPanel &_tr, string const &_keys)
    : ScopePanelRenderer(_m, _tr),
      seqPtr(tr.spec.refs[0]),
      keys(_keys)
  {
    assert(tr.spec.refs.size() == 1);
  }

  YogaPtrAccessor seqPtr;
  string keys;

  float convValueToX(double v)
  {
    return curbb.Min.x + (0.5 + 0.5 * v) * (curbb.Max.x - curbb.Min.x);
  }
  float convValueToY(double v)
  {
    return curbb.Min.y + (0.5 + 0.5 * v) * (curbb.Max.y - curbb.Min.y);
  }

  float preferredAspect() override
  {
    return 0.5;
  }

  void drawKey(string const &label, R value, R posX, R posY, R scale=0.15)
  {
    auto cX = convValueToX(posX);
    auto cY = convValueToY(posY);
    auto scaleX = min(curbb.Max.x - curbb.Min.x, curbb.Max.y - curbb.Min.y) * scale;
    auto scaleY = scaleX;

    auto drawList = GetWindowDrawList();

    array pts {
      ImVec2(cX - 1.0*scaleX, cY - 1.0*scaleY),
      ImVec2(cX + 1.0*scaleX, cY - 1.0*scaleY),      
      ImVec2(cX + 1.0*scaleX, cY + 1.0*scaleY),      
      ImVec2(cX - 1.0*scaleX, cY + 1.0*scaleY),      
    };

    if (value >= 0.0) {
      drawList->AddConvexPolyFilled(pts.data(), pts.size(), ImColor(0.4f, 0.4f, 0.4f, float(value)));
    }
    drawList->AddPolyline(pts.data(), pts.size(), IM_COL32(0x66, 0x66, 0x66, 0xff), true, 1.0);
    drawText(cX, cY, IM_COL32(0x00, 0x00, 0x00, 0xff), label, 0.5f, 0.5f);
  }

  void drawContents() override
  {
    setBoundingBox();

    for (auto &alt : m.alts(tr.spec.refs[0])) {
      vector<ImVec2> pts;

      auto v = alt.seq->getEqualBefore(m.visTime);
      if (!v) return;
      auto v1 = seqPtr.ptr(v);
      if (!v1) return;
      assert(v1.type->clsName == "YogaKeyboard");

      auto cppKbdState = reinterpret_cast<R *>(v1.buf);

      if (keys == "WASD") {
        drawKey("W", cppKbdState['W'-'A'], 0.0, -0.33);
        drawKey("A", cppKbdState['A'-'A'], -0.33, +0.33);
        drawKey("S", cppKbdState['S'-'A'], 0.0, +0.33);
        drawKey("D", cppKbdState['D'-'A'], +0.33, +0.33);
      }
    }
  }
};



static ScopePanelRendererRegister regScopePanelRendererKeyboard("YogaKeyboard", "WASD",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    tr.renderer = make_shared<ScopePanelRendererKeyboard>(m, tr, "WASD");
  }
);

