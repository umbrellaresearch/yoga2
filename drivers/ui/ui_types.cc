#include "./ui.h"
#include "./ui_types.h"
#include "../jit/compilation.h"

// These must match the shadow types in ./ui_types.h

static YogaCodeRegister registerCoreTypes("ui_types.cc", R"(

struct YogaKeyboard {
  R a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;
  R n1, n2, n3, n4, n5, n6, n7, n8, n9, n0;
  {scopeNoMembers: true}
};

struct YogaPuck {
  R active;
  R[3] force;
  R[3] torque;
  R[8] buttons;
};

struct RenderCircle {
  R order;
  R[2] pos;
  R radius;
  R[4] color;
  R thickness;
};

struct RenderCircleFilled {
  R order;
  R[2] pos;
  R radius;
  R[4] color;
};

struct RenderLine {
  R order;
  R[2] pos1, pos2;
  R[4] color;
  R thickness;
};

struct RenderArcArrow {
  R order;
  R[2] pos;
  R radius;
  R arcFrom, arcTo;
  R[4] color;
  R thickness;
};

struct RenderSolid {
  R order;
  string name;
  R[4,4] pos;
  R[4] color;
};

)");
