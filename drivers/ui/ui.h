#pragma once
#include "../../jit/jit_utils.h"
#include "../../jit/value.h"

#if defined(SDL_scancode_h_)
extern array<tuple<SDL_Scancode, char const *>, 36> yogaKeyboardScanCodes;
#endif

extern YogaValue yogaKbdState;
extern YogaValue yogaPuckState;

void initYogaKbdState(YogaContext const &ctx);
void syncYogaKbdState();

void initYogaPuckState(YogaContext const &ctx);
void endYogaPuckState();
