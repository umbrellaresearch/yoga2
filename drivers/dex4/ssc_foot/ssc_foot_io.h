#include "../../../comm/ssc_io.h"
#include "../../../jit/runtime.h"

#include "./ssc_foot_defs.h"

struct FootStateAccessorSuite {
  FootStateAccessorSuite(YogaType *t)
    : fti(t, "fti"),
      fto(t, "fto"),
      fh(t, "fh"),
      ai(t, "ai"),
      ao(t, "ao")
  {
  }
  YogaValueAccessor<R> fti;
  YogaValueAccessor<R> fto;
  YogaValueAccessor<R> fh;
  YogaValueAccessor<R> ai;
  YogaValueAccessor<R> ao;

  void rxFoot(packet &rx, YogaValue const &yv)
  {
    rxDsp1616(rx, fti, yv);
    rxDsp1616(rx, fto, yv);
    rxDsp1616(rx, fh, yv);
    rxDsp824(rx, ai, yv);
    rxDsp824(rx, ao, yv);
  }
};
