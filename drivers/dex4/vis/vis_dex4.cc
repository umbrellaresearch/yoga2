#include "../../studio/trace_render.h"
#include "../../studio/panel_render.h"

struct ScopeTraceRendererDex4FootState : ScopeTraceRenderer {
  ScopeTraceRendererDex4FootState(
    ScopeModel &_m,
    ScopeModelVisTrace &_tr)
    : ScopeTraceRenderer(_m, _tr),
      fhGetter(tr.spec.ref.member("fh")),
      ftiGetter(tr.spec.ref.member("fti")),
      ftoGetter(tr.spec.ref.member("fto"))
  {
  }

  YogaValueAccessor<R> fhGetter;
  YogaValueAccessor<R> ftiGetter;
  YogaValueAccessor<R> ftoGetter;

  void drawData(RenderQueue &q) override
  {
    for (auto &alt : m.alts(tr.spec.ref)) {
      
      PolylineBuf fhLine;
      PolylineBuf ftiLine;
      PolylineBuf ftoLine;
      PolybandBuf fhBand;
      PolybandBuf ftiBand;
      PolybandBuf ftoBand;

      for (auto &yv : alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs())) {
        R x = lo.convTimeToX(yv.ts);

        auto fh = fhGetter.rd(yv);
        auto fti = ftiGetter.rd(yv);
        auto fto = ftoGetter.rd(yv);

        fhLine(x, convDataToY(fh));
        ftiLine(x, convDataToY(fh + fti));
        ftoLine(x, convDataToY(fh + fti + fto));

        fhBand(x, convDataToY(0.0), convDataToY(fh));
        ftiBand(x, convDataToY(fh), convDataToY(fh + fti));
        ftoBand(x, convDataToY(fh + fti), convDataToY(fh + fti + fto));
      }

      fhBand.fill(IM_COL32(0x99, 0x99, 0xff, 0xff));
      ftiBand.fill(IM_COL32(0xff, 0x99, 0x99, 0xff));
      ftoBand.fill(IM_COL32(0x99, 0xcc, 0x99, 0xff));
      fhLine.draw(IM_COL32(0x00, 0x00, 0xff, 0xff), false, 1.0);
      ftiLine.draw(IM_COL32(0xff, 0x00, 0x00, 0xff), false, 1.0);
      ftoLine.draw(IM_COL32(0x00, 0x00, 0x00, 0xff), false, 1.0);
    }
  }

  void drawValues(RenderQueue &q) override
  {
    R textY = lo.plotT + 2;
    for (auto &alt : m.alts(tr.spec.ref)) {
      if (alt.isSecondary) continue;
      auto yv = alt.seq->getEqualBefore(m.visTime);

      R elHeight = GetTextLineHeight();

      R fh = fhGetter.rd(yv);
      R fti = ftiGetter.rd(yv);
      R fto = ftoGetter.rd(yv);

      drawText(lo.infoX, textY,
        IM_COL32(0x00, 0x00, 0xff, 0xff),
        "fh = " + fmteng(fh, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0xff, 0x00, 0x00, 0xff),
        "fti = " + fmteng(fti, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0x00, 0xcc, 0x00, 0xff),
        "fto = " + fmteng(fto, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0x88, 0x88, 0x88, 0xff),
        "tot = " + fmteng(fh + fti + fto, 3));
      textY += elHeight;
    }
  }

  void doAutoScale() override
  {
    tr.scale = 1.5;
  }

};

static ScopeTraceRendererRegister regPneuservo("Dex4FootState", 
  [](ScopeModel &m, ScopeModelVisTrace &tr)
  {
    tr.renderer = make_shared<ScopeTraceRendererDex4FootState>(m, tr);
  }
);



