#include "../../studio/trace_render.h"
#include "../../studio/panel_render.h"

struct ScopeTraceRendererSscPneuservoState : ScopeTraceRenderer {
  ScopeTraceRendererSscPneuservoState(ScopeModel &_m, ScopeModelVisTrace &_tr)
    : ScopeTraceRenderer(_m, _tr),
      desiredPosGetter(tr.spec.ref.member("desiredPos")),
      posGetter(tr.spec.ref.member("pos")),
      valveGetter(tr.spec.ref.member("valve")),
      torqueGetter(tr.spec.ref.member("torque"))
  {
  }

  YogaValueAccessor<R> desiredPosGetter;
  YogaValueAccessor<R> posGetter;
  YogaValueAccessor<R> valveGetter;
  YogaValueAccessor<R> torqueGetter;

  void drawData(RenderQueue &q) override
  {
    for (auto &alt : m.alts(tr.spec.ref)) {
      
      PolylineBuf desiredPosLine;
      PolylineBuf posLine;
      PolylineBuf valveLine;
      PolylineBuf torqueLine;

      for (auto &yv : alt.seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs())) {
        R x = lo.convTimeToX(yv.ts);

        if (desiredPosGetter.isValid()) desiredPosLine(x, convDataToY(desiredPosGetter.rd(yv)));
        if (posGetter.isValid()) posLine(x, convDataToY(posGetter.rd(yv)));
        if (valveGetter.isValid()) valveLine(x, convDataToY(valveGetter.rd(yv)));
        if (torqueGetter.isValid()) torqueLine(x, convDataToY(torqueGetter.rd(yv) * 0.02));
      }

      desiredPosLine.draw(IM_COL32(0x00, 0x00, 0xff, 0xff), false, 1.0);
      posLine.draw(IM_COL32(0xff, 0x00, 0x00, 0xff), false, 1.0);
      valveLine.draw(IM_COL32(0x00, 0xcc, 0x00, 0xff), false, 1.0);
      torqueLine.draw(IM_COL32(0x88, 0x88, 0x88, 0xff), false, 1.0);
    }
  }

  void drawValues(RenderQueue &q) override
  {
    R textY = lo.plotT + 2;
    for (auto &alt : m.alts(tr.spec.ref)) {
      if (alt.isSecondary) return;
      auto yv = alt.seq->getEqualBefore(m.visTime);

      R elHeight = GetTextLineHeight();

      R desiredPos = desiredPosGetter.rd(yv);
      R pos = posGetter.rd(yv);
      R valve = valveGetter.rd(yv);
      R torque = torqueGetter.rd(yv);

      drawText(lo.infoX, textY,
        IM_COL32(0x00, 0x00, 0xff, 0xff),
        "desiredPos = " + fmteng(desiredPos, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0xff, 0x00, 0x00, 0xff),
        "pos = " + fmteng(pos, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0x00, 0xcc, 0x00, 0xff),
        "valve = " + fmteng(valve, 3));
      textY += elHeight;
      drawText(lo.infoX, textY,
        IM_COL32(0x88, 0x88, 0x88, 0xff),
        "torque = " + fmteng(torque, 1));
      textY += elHeight;
    }
  }

  void doAutoScale() override
  {
    tr.scale = 1.5;
  }

};

static ScopeTraceRendererRegister regPneuservo("SscPneuservoState", 
  [](ScopeModel &m, ScopeModelVisTrace &tr)
  {
    tr.renderer = make_shared<ScopeTraceRendererSscPneuservoState>(m, tr);
  }
);



#if 0

// WRITEME: use the yoga phaseplot system instead.

static ScopePanelRendererRegister regPneuservoTorqueVsValve("SscPneuservoState", "torque vs valve",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    tr.renderer = make_shared<ScopePanelRendererXY>(m, {
      tr.member("torque"),
      tr.member{"valve"}, 50.0, 1.0);
  }
);

static ScopePanelRendererRegister regPneuservoTorqueVsPos("SscPneuservoState", "torque vs pos",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    tr.renderer = make_shared<ScopePanelRendererXY>(m, tr, "torque", 50.0, "pos", 1.5);
  }
);

static ScopePanelRendererRegister regPneuservoPosVsValve("SscPneuservoState", "pos vs valve",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    tr.renderer = make_shared<ScopePanelRendererXY>(m, tr, "pos", 1.5, "valve", 1.0);
  }
);

#endif
