#include "../../../jit/runtime.h"
#include "../../../comm/ssc_io.h"
#include "./ssc_leg_defs.h"

struct LegStateAccessorSuite {
  LegStateAccessorSuite(YogaType *t)
    : reg0(t, "reg0"),
      ht(t, "ht"),
      hf(t, "hf"),
      hs(t, "hs"),
      ki(t, "ki"),
      ko(t, "ko"),
      ai(t, "ai"),
      ao(t, "ao"),

      supPressure(reg0.ptrType, "supPressure"),
      exhPressure(reg0.ptrType, "exhPressure"),

      extPressure(hs.ptrType, "extPressure"),
      retPressure(hs.ptrType, "retPressure"),
      torque(hs.ptrType, "torque"),
      pos(hs.ptrType, "pos"),
      velPrefilter(hs.ptrType, "velPrefilter"),
      vel(hs.ptrType, "vel"),
      torqueFeedback(hs.ptrType, "torqueFeedback"),
      posFeedback(hs.ptrType, "posFeedback"),
      velFeedback(hs.ptrType, "velFeedback"),
      velFeedforward(hs.ptrType, "velFeedforward"),
      valvePrefilter(hs.ptrType, "valvePrefilter"),
      valve(hs.ptrType, "valve"),
      baseValve(hs.ptrType, "baseValve"),
      desiredTorque(hs.ptrType, "desiredTorque"),
      desiredPos(hs.ptrType, "desiredPos"),
      desiredVel(hs.ptrType, "desiredVel")
  {
  }

  YogaPtrAccessor reg0;
  YogaPtrAccessor ht;
  YogaPtrAccessor hf;
  YogaPtrAccessor hs;
  YogaPtrAccessor ki;
  YogaPtrAccessor ko;
  YogaPtrAccessor ai;
  YogaPtrAccessor ao;

  YogaValueAccessor<R> supPressure;
  YogaValueAccessor<R> exhPressure;

  YogaValueAccessor<R> extPressure;
  YogaValueAccessor<R> retPressure;
  YogaValueAccessor<R> torque;
  YogaValueAccessor<R> pos;
  YogaValueAccessor<R> velPrefilter;
  YogaValueAccessor<R> vel;
  YogaValueAccessor<R> torqueFeedback;
  YogaValueAccessor<R> posFeedback;
  YogaValueAccessor<R> velFeedback;
  YogaValueAccessor<R> velFeedforward;
  YogaValueAccessor<R> valvePrefilter;
  YogaValueAccessor<R> valve;
  YogaValueAccessor<R> baseValve;
  YogaValueAccessor<R> desiredTorque;
  YogaValueAccessor<R> desiredPos;
  YogaValueAccessor<R> desiredVel;

  // Must match drivers/dex4/fw/ssc_leg.c:ssc_leg_emit_state(), and also the ordering of valves and hoses in the robot

  void rxSscRegulatorState(packet &rx, YogaValue const &yv)
  {
    rxDsp824(rx, supPressure, yv);
    rxDsp824(rx, exhPressure, yv);
  }

  void rxSscPneuservoState(packet &rx, YogaValue const &yv)
  {
    rxDsp824(rx, extPressure, yv);
    rxDsp824(rx, retPressure, yv);
    rxDsp1616(rx, torque, yv);
    rxDsp824(rx, pos, yv);
    rxDsp824(rx, velPrefilter, yv);
    rxDsp824(rx, vel, yv);
    rxDsp824(rx, torqueFeedback, yv);
    rxDsp1616(rx, posFeedback, yv);
    rxDsp1616(rx, velFeedback, yv);
    rxDsp824(rx, velFeedforward, yv);
    rxDsp824(rx, valvePrefilter, yv);
    rxDsp824(rx, valve, yv);
    rxDsp824(rx, baseValve, yv);
    rxDsp1616(rx, desiredTorque, yv);
    rxDsp824(rx, desiredPos, yv);
    rxDsp824(rx, desiredVel, yv);
  }

  void rxState0(packet &rx, YogaValue const &yv)
  {
    rxSscRegulatorState(rx, reg0.ptr(yv));
    rxSscPneuservoState(rx, hs.ptr(yv));
    rxSscPneuservoState(rx, ki.ptr(yv));
    rxSscPneuservoState(rx, hf.ptr(yv));
    rxSscPneuservoState(rx, ht.ptr(yv));
    rxSscPneuservoState(rx, ko.ptr(yv));
    rxSscPneuservoState(rx, ai.ptr(yv));
    rxSscPneuservoState(rx, ao.ptr(yv));
  }
  void rxState1(packet &rx, YogaValue const &yv)
  {
    rxSscPneuservoState(rx, ao.ptr(yv));
    rxSscPneuservoState(rx, ai.ptr(yv));
    rxSscPneuservoState(rx, ko.ptr(yv));
    rxSscPneuservoState(rx, ht.ptr(yv));
    rxSscPneuservoState(rx, hf.ptr(yv));
    rxSscPneuservoState(rx, ki.ptr(yv));
    rxSscPneuservoState(rx, hs.ptr(yv));
  }

};

struct LegCmdAccessorSuite {
  LegCmdAccessorSuite(YogaType *t)
    : reg0(t, "reg0"),
      ht(t, "ht"),
      hf(t, "hf"),
      hs(t, "hs"),
      ki(t, "ki"),
      ko(t, "ko"),
      ai(t, "ai"),
      ao(t, "ao"),

      setpoint(reg0.ptrType, "setpoint"),

      baseValve(ht.ptrType, "baseValve"),
      desiredTorque(ht.ptrType, "desiredTorque"),
      desiredPos(ht.ptrType, "desiredPos"),
      desiredVel(ht.ptrType, "desiredVel"),
      torqueFeedbackCoeff(ht.ptrType, "torqueFeedbackCoeff"),
      torqueFeedbackLimLo(ht.ptrType, "torqueFeedbackLimLo"),
      torqueFeedbackLimHi(ht.ptrType, "torqueFeedbackLimHi"),
      posFeedbackCoeff(ht.ptrType, "posFeedbackCoeff"),
      posFeedbackLimLo(ht.ptrType, "posFeedbackLimLo"),
      posFeedbackLimHi(ht.ptrType, "posFeedbackLimHi"),
      velFeedbackCoeff(ht.ptrType, "velFeedbackCoeff"),
      velFeedbackLimLo(ht.ptrType, "velFeedbackLimLo"),
      velFeedbackLimHi(ht.ptrType, "velFeedbackLimHi"),
      velFeedforwardCoeff(ht.ptrType, "velFeedforwardCoeff")
  {
  }

  YogaValueAccessor<bool> valveOpen;
  YogaPtrAccessor reg0;
  YogaPtrAccessor ht;
  YogaPtrAccessor hf;
  YogaPtrAccessor hs;
  YogaPtrAccessor ki;
  YogaPtrAccessor ko;
  YogaPtrAccessor ai;
  YogaPtrAccessor ao;

  YogaValueAccessor<R> setpoint;

  YogaValueAccessor<R> baseValve;
  YogaValueAccessor<R> desiredTorque;
  YogaValueAccessor<R> desiredPos;
  YogaValueAccessor<R> desiredVel;
  YogaValueAccessor<R> torqueFeedbackCoeff;
  YogaValueAccessor<R> torqueFeedbackLimLo;
  YogaValueAccessor<R> torqueFeedbackLimHi;
  YogaValueAccessor<R> posFeedbackCoeff;
  YogaValueAccessor<R> posFeedbackLimLo;
  YogaValueAccessor<R> posFeedbackLimHi;
  YogaValueAccessor<R> velFeedbackCoeff;
  YogaValueAccessor<R> velFeedbackLimLo;
  YogaValueAccessor<R> velFeedbackLimHi;
  YogaValueAccessor<R> velFeedforwardCoeff;

  void txRegulatorCmd(packet &tx, YogaValue const &yv)
  {
    txDsp824(tx, setpoint, yv);
  }

  void txPneuservoCmd(packet &tx, YogaValue const &yv)
  {
    txDsp824(tx, baseValve, yv);
    txDsp1616(tx, desiredTorque, yv);
    txDsp824(tx, desiredPos, yv);
    txDsp824(tx, desiredVel, yv);

    txDsp824(tx, torqueFeedbackCoeff, yv);
    txDsp824(tx, torqueFeedbackLimLo, yv);
    txDsp824(tx, torqueFeedbackLimHi, yv);

    txDsp1616(tx, posFeedbackCoeff, yv);
    txDsp1616(tx, posFeedbackLimLo, yv);
    txDsp1616(tx, posFeedbackLimHi, yv);

    txDsp1616(tx, velFeedbackCoeff, yv);
    txDsp1616(tx, velFeedbackLimLo, yv);
    txDsp1616(tx, velFeedbackLimHi, yv);

    txDsp824(tx, velFeedforwardCoeff, yv);
  }

  void txCmd0(packet &tx, YogaValue const &yv)
  {
    txRegulatorCmd(tx, reg0.ptr(yv));
    txPneuservoCmd(tx, hs.ptr(yv));
    txPneuservoCmd(tx, ki.ptr(yv));
    txPneuservoCmd(tx, hf.ptr(yv));
    txPneuservoCmd(tx, ht.ptr(yv));
    txPneuservoCmd(tx, ko.ptr(yv));
    txPneuservoCmd(tx, ai.ptr(yv));
    txPneuservoCmd(tx, ao.ptr(yv));
  }
  void txCmd1(packet &tx, YogaValue const &yv)
  {
    txPneuservoCmd(tx, ao.ptr(yv));
    txPneuservoCmd(tx, ai.ptr(yv));
    txPneuservoCmd(tx, ko.ptr(yv));
    txPneuservoCmd(tx, ht.ptr(yv));
    txPneuservoCmd(tx, hf.ptr(yv));
    txPneuservoCmd(tx, ki.ptr(yv));
    txPneuservoCmd(tx, hs.ptr(yv));
  }
};

