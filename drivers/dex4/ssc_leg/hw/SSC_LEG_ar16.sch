<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="11" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="3" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="2" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="14" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="panelcutout" color="10" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Frames" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tRes" color="12" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bRes" color="1" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic>
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="VCC" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCCINT">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-0.889" y="0.254" size="0.8128" layer="94">INT</text>
<pin name="VCCINT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+24V">
<wire x1="1.27" y1="-0.635" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="VCC" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCCINT" prefix="VCC">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCCINT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<packages>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1005">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.435" y="0.235" size="0.254" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="0.6096" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.381" y="0.254" size="0.4064" layer="25">&gt;NAME</text>
<text x="-0.381" y="-0.635" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1005" package="C1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1005" package="R1005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="honeywell-ssc">
<packages>
<package name="DIL08WIDE">
<wire x1="5.08" y1="5.461" x2="-5.08" y2="5.461" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.461" x2="5.08" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="5.08" y1="5.461" x2="5.08" y2="-5.461" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="5.461" x2="-5.08" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-5.461" x2="-5.08" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="2.286" x2="-5.08" y2="-2.286" width="0.1524" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="2" x="-1.27" y="-6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="7" x="-1.27" y="6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="8" x="-3.81" y="6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="3" x="1.27" y="-6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="4" x="3.81" y="-6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="6" x="1.27" y="6.604" drill="0.8128" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="6.604" drill="0.8128" shape="long" rot="R90"/>
<text x="-5.334" y="-5.461" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-3.556" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="SSC">
<description>Honeywell pressure sensor</description>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<pin name="GND" x="0" y="-20.32" length="middle" direction="pwr" rot="R90"/>
<pin name="VSUP" x="0" y="20.32" length="middle" direction="pwr" rot="R270"/>
<pin name="SCL" x="-15.24" y="7.62" length="middle"/>
<pin name="SDA" x="-15.24" y="-5.08" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SSCDANN150PG2A3">
<gates>
<gate name="G$1" symbol="SSC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DIL08WIDE">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="SCL" pad="4"/>
<connect gate="G$1" pin="SDA" pad="3"/>
<connect gate="G$1" pin="VSUP" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<packages>
<package name="MA05-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-5.588" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-6.35" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="2.921" size="1.27" layer="21" ratio="10">10</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA05-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA05-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<packages>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49GW">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-6.35" x2="5.08" y2="-6.35" width="0.8128" layer="21"/>
<wire x1="4.445" y1="6.731" x2="1.016" y2="6.731" width="0.1524" layer="21"/>
<wire x1="1.016" y1="6.731" x2="-1.016" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.731" x2="-4.445" y2="6.731" width="0.1524" layer="21"/>
<wire x1="4.445" y1="6.731" x2="5.08" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.096" x2="-4.445" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.08" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="2.54" x2="0.3048" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="0.3048" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.08" x2="-0.3302" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.08" x2="0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.08" x2="-0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-6.604" x2="-2.413" y2="-8.255" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-6.477" x2="2.413" y2="-8.382" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-6.35" x2="5.08" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-5.08" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="0" y1="8.382" x2="0" y2="6.985" width="0.6096" layer="51"/>
<smd name="1" x="-2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="0" y="8.001" dx="1.27" dy="2.794" layer="1"/>
<text x="-5.588" y="-5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-8.255" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49TL-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.334" y1="-5.715" x2="-5.461" y2="-5.715" width="0.8128" layer="21"/>
<wire x1="4.445" y1="7.366" x2="1.143" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="7.366" x2="-4.445" y2="7.366" width="0.1524" layer="21"/>
<wire x1="4.445" y1="7.366" x2="5.08" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.731" x2="-4.445" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.715" x2="-0.3302" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="3.175" x2="0.3048" y2="3.175" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="3.175" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.715" x2="0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.715" x2="-0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-5.842" x2="-2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-5.842" x2="2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.731" x2="-5.08" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="1.143" y1="7.366" x2="-1.143" y2="7.366" width="0.1524" layer="51"/>
<wire x1="0" y1="7.874" x2="0" y2="7.62" width="0.6096" layer="51"/>
<pad name="1" x="-2.413" y="-7.62" drill="0.8128"/>
<pad name="2" x="2.413" y="-7.62" drill="0.8128"/>
<pad name="3" x="0" y="7.874" drill="0.8128"/>
<text x="-5.461" y="-4.445" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-4.699" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="7.366" x2="0.3048" y2="7.5692" layer="51"/>
<rectangle x1="-6.35" y1="-6.985" x2="6.35" y2="-4.445" layer="43"/>
<rectangle x1="-5.715" y1="-4.445" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="9.271" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<text x="-5.461" y="-1.397" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-4.445" x2="6.35" y2="-1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.905" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-LM">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.414" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-5.08" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="HC49U70">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="3.048" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.016" x2="-0.3302" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="-1.016" x2="0.3048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="-1.016" x2="0.3048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.016" x2="-0.3302" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="1.016" x2="0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="1.016" x2="-0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="-2.54" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21" curve="-180"/>
<wire x1="-3.048" y1="-2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.572" y1="-2.794" x2="-4.064" y2="2.794" layer="43"/>
<rectangle x1="-5.08" y1="-2.54" x2="-4.572" y2="2.54" layer="43"/>
<rectangle x1="-5.588" y1="-2.032" x2="-5.08" y2="2.032" layer="43"/>
<rectangle x1="-5.842" y1="-1.27" x2="-5.588" y2="1.27" layer="43"/>
<rectangle x1="-4.064" y1="-3.048" x2="4.064" y2="3.048" layer="43"/>
<rectangle x1="4.064" y1="-2.794" x2="4.572" y2="2.794" layer="43"/>
<rectangle x1="4.572" y1="-2.54" x2="5.08" y2="2.54" layer="43"/>
<rectangle x1="5.08" y1="-2.032" x2="5.588" y2="2.032" layer="43"/>
<rectangle x1="5.588" y1="-1.27" x2="5.842" y2="1.27" layer="43"/>
</package>
<package name="HC13U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="33.401" x2="-8.636" y2="33.401" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="32.766" x2="-8.636" y2="33.401" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="33.401" x2="9.271" y2="32.766" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="15.24" x2="9.017" y2="15.24" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="21.59" x2="-0.3302" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="19.05" x2="0.3048" y2="19.05" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="19.05" x2="0.3048" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="21.59" x2="-0.3302" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="21.59" x2="0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="21.59" x2="-0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="9.144" y1="15.24" x2="10.16" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="15.24" x2="-9.144" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="9.271" y1="14.732" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="14.732" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="14.732" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="14.732" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="15.24" drill="0.8128"/>
<pad name="M1" x="10.16" y="15.24" drill="0.8128"/>
<text x="-10.16" y="0" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-5.08" x2="10.795" y2="34.925" layer="43"/>
</package>
<package name="HC18U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.461" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.461" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="10.16" x2="-4.445" y2="10.16" width="0.1524" layer="21"/>
<wire x1="4.445" y1="10.16" x2="5.08" y2="9.525" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.525" x2="-4.445" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.668" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.889" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-5.08" x2="6.35" y2="10.795" layer="43"/>
</package>
<package name="HC18U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="1.905" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="4.445" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="4.445" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.4064" layer="21" curve="90"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.4064" layer="21" curve="90"/>
<wire x1="-4.318" y1="-1.905" x2="4.318" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.905" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.905" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.27" x2="-0.3302" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="-1.27" x2="0.3048" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="-1.27" x2="0.3048" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="1.27" x2="-0.3302" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="1.27" x2="0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="1.27" x2="-0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="0" x2="0.9398" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-0.9398" y2="-1.27" width="0.3048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128"/>
<pad name="2" x="2.54" y="0" drill="0.8128"/>
<text x="-5.0546" y="3.2766" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.6228" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-3.175" x2="5.715" y2="3.175" layer="43"/>
</package>
<package name="HC33U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="16.51" x2="-8.636" y2="16.51" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.875" x2="-8.636" y2="16.51" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="16.51" x2="9.271" y2="15.875" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="7.62" x2="9.017" y2="7.62" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="13.97" x2="-0.3302" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="11.43" x2="0.3048" y2="11.43" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="11.43" x2="0.3048" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="13.97" x2="-0.3302" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="13.97" x2="0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="13.97" x2="-0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="9.144" y1="7.62" x2="10.16" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="7.62" x2="-9.144" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="9.271" y1="7.112" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="7.112" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="7.112" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="7.62" drill="0.8128"/>
<pad name="M1" x="10.16" y="7.62" drill="0.8128"/>
<text x="-7.62" y="17.272" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.62" y1="-3.175" x2="-6.985" y2="16.51" layer="21"/>
<rectangle x1="6.985" y1="-3.175" x2="7.62" y2="16.51" layer="21"/>
<rectangle x1="-10.795" y1="-5.715" x2="10.795" y2="17.145" layer="43"/>
</package>
<package name="HC33U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.3302" y1="2.54" x2="-0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="0" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="2.54" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="2.54" x2="0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="2.54" x2="-0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-5.207" y1="4.064" x2="5.207" y2="4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-4.064" x2="5.207" y2="-4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="3.683" x2="5.207" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="-5.207" y2="3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="4.064" x2="5.207" y2="-4.064" width="0.254" layer="21" curve="-180"/>
<wire x1="-5.207" y1="4.064" x2="-5.207" y2="-4.064" width="0.254" layer="21" curve="180"/>
<pad name="1" x="-6.223" y="0" drill="1.016"/>
<pad name="2" x="6.223" y="0" drill="1.016"/>
<text x="-5.08" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.255" y1="-3.81" x2="-6.985" y2="3.81" layer="43"/>
<rectangle x1="-8.89" y1="-3.175" x2="-8.255" y2="3.175" layer="43"/>
<rectangle x1="-9.525" y1="-2.54" x2="-8.89" y2="2.54" layer="43"/>
<rectangle x1="-6.985" y1="-4.445" x2="6.985" y2="4.445" layer="43"/>
<rectangle x1="6.985" y1="-3.81" x2="8.255" y2="3.81" layer="43"/>
<rectangle x1="8.255" y1="-3.175" x2="8.89" y2="3.175" layer="43"/>
<rectangle x1="8.89" y1="-2.54" x2="9.525" y2="2.54" layer="43"/>
</package>
<package name="TC26H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.397" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
</package>
<package name="TC38H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-1.397" y1="1.651" x2="1.397" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.524" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="9.652" x2="-1.27" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="9.906" x2="1.27" y2="9.906" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.651" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="0.7112" x2="0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.016" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="0.7112" x2="-0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="5.588" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.588" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="0" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.826" x2="0" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0.762" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.905" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.175" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="1.016" x2="1.778" y2="10.414" layer="43"/>
</package>
<package name="86SMX">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-3.81" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="2.413" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="2.413" y2="-1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="-2.286" x2="2.413" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="1.905" x2="-5.334" y2="1.016" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-3.81" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-2.286" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.286" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-2.286" x2="-3.81" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.54" x2="-4.191" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-2.2098" x2="-6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.35" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.54" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.016" x2="-5.334" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="-5.334" y1="-1.016" x2="-5.334" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-6.35" y2="-2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="-2.54" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.54" x2="-4.191" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-3.81" y2="1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="2.286" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.286" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="2.286" x2="-3.81" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="2.286" x2="-6.35" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.2098" x2="-6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-5.969" y1="2.54" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-6.35" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="2.54" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="6.604" y1="2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.223" y2="1.905" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.54" x2="5.842" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-2.286" x2="2.794" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="6.223" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="6.223" y1="1.905" x2="6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="2.286" x2="6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="5.842" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="2.286" x2="2.794" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="1.905" x2="6.223" y2="1.905" width="0.0508" layer="51"/>
<wire x1="2.413" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.651" x2="-0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.651" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.651" x2="0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="1.016" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-0.635" y1="1.651" x2="-0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-1.016" y2="1.016" width="0.0508" layer="21"/>
<smd name="2" x="4.318" y="-2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="3" x="4.318" y="2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="1" x="-5.08" y="-2.286" dx="2.286" dy="2.1844" layer="1"/>
<smd name="4" x="-5.08" y="2.286" dx="2.286" dy="2.1844" layer="1"/>
<text x="-3.683" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.683" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM20SS">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.032" y1="-1.27" x2="2.032" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-1.778" x2="2.032" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.27" x2="-2.032" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="0.381" x2="-2.921" y2="-0.381" width="0.0508" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-3.556" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.552" x2="-4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.921" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="1.27" x2="-2.921" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-0.381" x2="-2.921" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-2.032" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-3.556" y1="-1.778" x2="-2.032" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-3.556" y2="-1.552" width="0.0508" layer="51"/>
<wire x1="-4.064" y1="1.778" x2="-3.556" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.552" x2="-4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="1.27" x2="-3.556" y2="1.552" width="0.0508" layer="51"/>
<wire x1="-3.048" y1="1.778" x2="-3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="1.778" x2="-2.032" y2="1.778" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.556" y2="1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.27" x2="2.032" y2="1.27" width="0.0508" layer="51"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="3.048" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.27" x2="3.556" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="2.032" y1="-1.778" x2="3.556" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.81" y1="1.27" x2="4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.778" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.778" x2="3.556" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-0.254" x2="-0.508" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-1.778" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.254" x2="-1.778" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-1.143" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="-0.635" x2="-1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.016" width="0.0508" layer="21"/>
<circle x="3.048" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="2" x="2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="3" x="2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="4" x="-2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM39SL">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.683" y1="-1.651" x2="3.683" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-3.683" y1="-2.286" x2="3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="1.651" x2="-3.683" y2="1.651" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-4.826" y2="0.762" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-3.683" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.055" x2="-6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.715" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-4.826" y2="-0.762" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-5.715" y2="-2.055" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="2.286" x2="-3.683" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.683" y1="1.651" x2="-4.826" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-6.223" y1="2.286" x2="-5.715" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.055" x2="-6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-5.715" y2="2.055" width="0.0508" layer="51"/>
<wire x1="6.223" y1="-2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="5.842" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="5.715" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="5.715" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.842" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="3.683" y1="-1.651" x2="5.715" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="5.842" y1="1.651" x2="6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="5.842" y1="1.651" x2="5.715" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.715" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.286" x2="5.715" y2="2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.651" x2="3.683" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.254" x2="-3.81" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.254" x2="-3.81" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.016" width="0.1016" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.016" width="0.1016" layer="21"/>
<circle x="5.08" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="2" x="4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="3" x="4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="4" x="-4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="SM49">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="2.413" x2="5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-3.9826" y1="1.143" x2="-3.9826" y2="-1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="-5.1091" y1="1.143" x2="-5.1091" y2="-1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="3.9826" y1="1.143" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="6.477" y1="0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.1091" y1="1.143" x2="5.1091" y2="-1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="-2.413" x2="5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<text x="-5.715" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-2.54" x2="6.604" y2="2.794" layer="43"/>
</package>
<package name="TC26V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.127" y1="-0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.508" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0" y1="1.016" x2="0.7184" y2="0.7184" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="0.7184" x2="0" y2="1.016" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="-0.7184" x2="0" y2="-1.016" width="0.1524" layer="21" curve="44.999323"/>
<wire x1="0" y1="-1.016" x2="0.7184" y2="-0.7184" width="0.1524" layer="21" curve="44.999323"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-2.032" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CTS406">
<description>&lt;b&gt;Model 406 6.0x3.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: 008-0260-0_E.pdf</description>
<wire x1="-2.475" y1="1.65" x2="-1.05" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1.05" y1="1.65" x2="1.05" y2="1.65" width="0.2032" layer="21"/>
<wire x1="1.05" y1="1.65" x2="2.475" y2="1.65" width="0.2032" layer="51"/>
<wire x1="2.9" y1="1.225" x2="2.9" y2="0.3" width="0.2032" layer="51"/>
<wire x1="2.9" y1="0.3" x2="2.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-0.3" x2="2.9" y2="-1.225" width="0.2032" layer="51"/>
<wire x1="2.475" y1="-1.65" x2="1.05" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="1.05" y1="-1.65" x2="-1.05" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.05" y1="-1.65" x2="-2.475" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-1.225" x2="-2.9" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-0.3" x2="-2.9" y2="0.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="0.3" x2="-2.9" y2="1.225" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="1.225" x2="-2.475" y2="1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.475" y1="1.65" x2="2.9" y2="1.225" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.9" y1="-1.225" x2="2.475" y2="-1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-2.475" y1="-1.65" x2="-2.9" y2="-1.225" width="0.2032" layer="51" curve="89.516721"/>
<circle x="-2.05" y="-0.2" radius="0.182" width="0" layer="21"/>
<smd name="1" x="-2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="2" x="2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="3" x="2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1667008" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49GW" package="HC49GW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49TL-H" package="HC49TL-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-H" package="HC49U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666973" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-LM" package="HC49U-LM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666956" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-V" package="HC49U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666969" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U70" package="HC49U70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC13U-H" package="HC13U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-H" package="HC18U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-V" package="HC18U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-H" package="HC33U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-V" package="HC33U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SM49" package="SM49">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26H" package="TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26V" package="TC26V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC38H" package="TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="68SMX" package="86SMX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="6344860" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM20SS" package="MM20SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM39SL" package="MM39SL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTS406" package="CTS406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="tlb">
<packages>
<package name="SO14">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="-4.267" y1="-1.791" x2="4.267" y2="-1.791" width="0.1524" layer="51"/>
<wire x1="4.267" y1="-1.791" x2="4.267" y2="1.791" width="0.1524" layer="21"/>
<wire x1="4.267" y1="1.791" x2="-4.267" y2="1.791" width="0.1524" layer="51"/>
<wire x1="-4.267" y1="1.791" x2="-4.267" y2="-1.791" width="0.1524" layer="21"/>
<circle x="-3.5052" y="-0.7747" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-3.81" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="14" x="-3.81" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="2" x="-2.54" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="3" x="-1.27" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="13" x="-2.54" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="12" x="-1.27" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="4" x="0" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="11" x="0" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="5" x="1.27" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="6" x="2.54" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="10" x="1.27" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="9" x="2.54" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="7" x="3.81" y="-2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<smd name="8" x="3.81" y="2.6035" dx="0.6096" dy="2.2098" layer="1"/>
<text x="-4.5085" y="-2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-0.6985" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.9878" y1="1.8415" x2="-3.6322" y2="2.8575" layer="51"/>
<rectangle x1="-2.7178" y1="1.8415" x2="-2.3622" y2="2.8575" layer="51"/>
<rectangle x1="-1.4478" y1="1.8415" x2="-1.0922" y2="2.8575" layer="51"/>
<rectangle x1="-0.1778" y1="1.8415" x2="0.1778" y2="2.8575" layer="51"/>
<rectangle x1="1.0922" y1="1.8415" x2="1.4478" y2="2.8575" layer="51"/>
<rectangle x1="2.3622" y1="1.8415" x2="2.7178" y2="2.8575" layer="51"/>
<rectangle x1="3.6322" y1="1.8415" x2="3.9878" y2="2.8575" layer="51"/>
<rectangle x1="3.6322" y1="-2.8575" x2="3.9878" y2="-1.8415" layer="51"/>
<rectangle x1="2.3622" y1="-2.8575" x2="2.7178" y2="-1.8415" layer="51"/>
<rectangle x1="1.0922" y1="-2.8575" x2="1.4478" y2="-1.8415" layer="51"/>
<rectangle x1="-0.1778" y1="-2.8575" x2="0.1778" y2="-1.8415" layer="51"/>
<rectangle x1="-1.4478" y1="-2.8575" x2="-1.0922" y2="-1.8415" layer="51"/>
<rectangle x1="-2.7178" y1="-2.8575" x2="-2.3622" y2="-1.8415" layer="51"/>
<rectangle x1="-3.9878" y1="-2.8575" x2="-3.6322" y2="-1.8415" layer="51"/>
</package>
<package name="TSSOP16">
<description>&lt;b&gt;Thin Shrink Small Outline Plastic 16&lt;/b&gt;&lt;p&gt;
MAX3223-MAX3243.pdf</description>
<wire x1="-2.5146" y1="-2.2828" x2="2.5146" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="2.5146" y2="-2.2828" width="0.1524" layer="21"/>
<wire x1="2.5146" y1="2.2828" x2="-2.5146" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-2.5146" y1="-2.2828" x2="-2.5146" y2="2.2828" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-2.0542" x2="2.286" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.286" y1="2.0542" x2="2.286" y2="-2.0542" width="0.0508" layer="21"/>
<wire x1="2.286" y1="2.0542" x2="-2.286" y2="2.0542" width="0.0508" layer="21"/>
<wire x1="-2.286" y1="-2.0542" x2="-2.286" y2="2.0542" width="0.0508" layer="21"/>
<circle x="-1.6256" y="-1.2192" radius="0.4572" width="0.1524" layer="21"/>
<smd name="1" x="-2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="2" x="-1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="3" x="-0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="4" x="-0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="5" x="0.325" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="6" x="0.975" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="7" x="1.625" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="8" x="2.275" y="-2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="9" x="2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="10" x="1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="11" x="0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="12" x="0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="13" x="-0.325" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="14" x="-0.975" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="15" x="-1.625" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<smd name="16" x="-2.275" y="2.9178" dx="0.3048" dy="0.9906" layer="1"/>
<text x="-2.8956" y="-2.0828" size="1.016" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.8862" y="-2.0828" size="1.016" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.3766" y1="-3.121" x2="-2.1734" y2="-2.2828" layer="51"/>
<rectangle x1="-1.7266" y1="-3.121" x2="-1.5234" y2="-2.2828" layer="51"/>
<rectangle x1="-1.0766" y1="-3.121" x2="-0.8734" y2="-2.2828" layer="51"/>
<rectangle x1="-0.4266" y1="-3.121" x2="-0.2234" y2="-2.2828" layer="51"/>
<rectangle x1="0.2234" y1="-3.121" x2="0.4266" y2="-2.2828" layer="51"/>
<rectangle x1="0.8734" y1="-3.121" x2="1.0766" y2="-2.2828" layer="51"/>
<rectangle x1="1.5234" y1="-3.121" x2="1.7266" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="-3.121" x2="2.3766" y2="-2.2828" layer="51"/>
<rectangle x1="2.1734" y1="2.2828" x2="2.3766" y2="3.121" layer="51"/>
<rectangle x1="1.5234" y1="2.2828" x2="1.7266" y2="3.121" layer="51"/>
<rectangle x1="0.8734" y1="2.2828" x2="1.0766" y2="3.121" layer="51"/>
<rectangle x1="0.2234" y1="2.2828" x2="0.4266" y2="3.121" layer="51"/>
<rectangle x1="-0.4266" y1="2.2828" x2="-0.2234" y2="3.121" layer="51"/>
<rectangle x1="-1.0766" y1="2.2828" x2="-0.8734" y2="3.121" layer="51"/>
<rectangle x1="-1.7266" y1="2.2828" x2="-1.5234" y2="3.121" layer="51"/>
<rectangle x1="-2.3766" y1="2.2828" x2="-2.1734" y2="3.121" layer="51"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-1.27" y="0.635" size="0.6096" layer="25">&gt;NAME</text>
<text x="-1.27" y="-1.27" size="0.6096" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="43045-0412">
<wire x1="-5" y1="3.8" x2="5" y2="3.8" width="0.254" layer="21"/>
<wire x1="5" y1="3.8" x2="5" y2="-3.7" width="0.254" layer="21"/>
<wire x1="5" y1="-3.7" x2="-5" y2="-3.7" width="0.254" layer="21"/>
<wire x1="-5" y1="-3.7" x2="-5" y2="3.8" width="0.254" layer="21"/>
<wire x1="-2" y1="4.8" x2="2" y2="4.8" width="0.254" layer="27"/>
<wire x1="-2" y1="4.8" x2="-2" y2="3.8" width="0.254" layer="27"/>
<wire x1="2" y1="4.8" x2="2" y2="3.8" width="0.254" layer="27"/>
<pad name="1" x="1.5" y="-1.5" drill="1.1"/>
<pad name="2" x="-1.5" y="-1.5" drill="1.1"/>
<pad name="3" x="1.5" y="1.5" drill="1.1"/>
<pad name="4" x="-1.5" y="1.5" drill="1.1"/>
<text x="-4.055" y="-5.35" size="1.27" layer="25">&gt;NAME</text>
<text x="5.5" y="-2" size="1.27" layer="21">1</text>
<hole x="4.5" y="2.5" drill="1"/>
<hole x="-4.5" y="2.5" drill="1"/>
</package>
<package name="SOT23-5L">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.422" y1="0.81" x2="1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="51"/>
<wire x1="-1.422" y1="-0.81" x2="-1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="51"/>
<wire x1="-0.522" y1="0.81" x2="0.522" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-0.428" y1="-0.81" x2="-0.522" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="0.522" y1="-0.81" x2="0.428" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="-1.328" y1="-0.81" x2="-1.422" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.422" y1="-0.81" x2="1.328" y2="-0.81" width="0.1524" layer="21"/>
<wire x1="1.328" y1="0.81" x2="1.422" y2="0.81" width="0.1524" layer="21"/>
<wire x1="-1.422" y1="0.81" x2="-1.328" y2="0.81" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.905" y="1.905" size="0.8128" layer="25">&gt;NAME</text>
<text x="-1.3208" y="-0.4064" size="0.8128" layer="27">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="SO28">
<description>&lt;b&gt;Small Outline Package&lt;/b&gt;</description>
<wire x1="-8.814" y1="-4.248" x2="8.839" y2="-4.248" width="0.1524" layer="51"/>
<wire x1="8.839" y1="-4.248" x2="8.839" y2="4.248" width="0.1524" layer="21"/>
<wire x1="8.839" y1="4.248" x2="-8.814" y2="4.248" width="0.1524" layer="51"/>
<wire x1="-8.814" y1="4.248" x2="-8.814" y2="-4.248" width="0.1524" layer="21"/>
<circle x="-7.874" y="-3.0988" radius="0.5334" width="0.1524" layer="21"/>
<smd name="1" x="-8.255" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="2" x="-6.985" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="3" x="-5.715" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="4" x="-4.445" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="5" x="-3.175" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="6" x="-1.905" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="7" x="-0.635" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="8" x="0.635" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="9" x="1.905" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="10" x="3.175" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="20" x="1.905" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="19" x="3.175" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="18" x="4.445" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="17" x="5.715" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="16" x="6.985" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="15" x="8.255" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="14" x="8.255" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="13" x="6.985" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="12" x="5.715" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="11" x="4.445" y="-5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="21" x="0.635" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="22" x="-0.635" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="23" x="-1.905" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="24" x="-3.175" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="25" x="-4.445" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="26" x="-5.715" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="27" x="-6.985" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<smd name="28" x="-8.255" y="5.315" dx="0.61" dy="2.21" layer="1"/>
<text x="-9.652" y="-3.81" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-6.985" y="-1.27" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.509" y1="-5.7976" x2="-8.001" y2="-4.2736" layer="51"/>
<rectangle x1="-7.239" y1="-5.7976" x2="-6.731" y2="-4.2736" layer="51"/>
<rectangle x1="-5.969" y1="-5.7976" x2="-5.461" y2="-4.2736" layer="51"/>
<rectangle x1="-4.699" y1="-5.7976" x2="-4.191" y2="-4.2736" layer="51"/>
<rectangle x1="-3.429" y1="-5.7976" x2="-2.921" y2="-4.2736" layer="51"/>
<rectangle x1="-2.159" y1="-5.7976" x2="-1.651" y2="-4.2736" layer="51"/>
<rectangle x1="-0.889" y1="-5.7976" x2="-0.381" y2="-4.2736" layer="51"/>
<rectangle x1="0.381" y1="-5.7976" x2="0.889" y2="-4.2736" layer="51"/>
<rectangle x1="1.651" y1="-5.7976" x2="2.159" y2="-4.2736" layer="51"/>
<rectangle x1="2.921" y1="-5.7976" x2="3.429" y2="-4.2736" layer="51"/>
<rectangle x1="4.191" y1="-5.7976" x2="4.699" y2="-4.2736" layer="51"/>
<rectangle x1="5.461" y1="-5.7976" x2="5.969" y2="-4.2736" layer="51"/>
<rectangle x1="6.731" y1="-5.7976" x2="7.239" y2="-4.2736" layer="51"/>
<rectangle x1="8.001" y1="-5.7976" x2="8.509" y2="-4.2736" layer="51"/>
<rectangle x1="8.001" y1="4.2736" x2="8.509" y2="5.7976" layer="51"/>
<rectangle x1="6.731" y1="4.2736" x2="7.239" y2="5.7976" layer="51"/>
<rectangle x1="5.461" y1="4.2735" x2="5.969" y2="5.7975" layer="51"/>
<rectangle x1="4.191" y1="4.2735" x2="4.699" y2="5.7975" layer="51"/>
<rectangle x1="2.921" y1="4.2735" x2="3.429" y2="5.7975" layer="51"/>
<rectangle x1="1.651" y1="4.2735" x2="2.159" y2="5.7975" layer="51"/>
<rectangle x1="0.381" y1="4.2735" x2="0.889" y2="5.7975" layer="51"/>
<rectangle x1="-0.889" y1="4.2735" x2="-0.381" y2="5.7975" layer="51"/>
<rectangle x1="-2.159" y1="4.2735" x2="-1.651" y2="5.7975" layer="51"/>
<rectangle x1="-3.429" y1="4.2735" x2="-2.921" y2="5.7975" layer="51"/>
<rectangle x1="-4.699" y1="4.2735" x2="-4.191" y2="5.7975" layer="51"/>
<rectangle x1="-5.969" y1="4.2735" x2="-5.461" y2="5.7975" layer="51"/>
<rectangle x1="-7.239" y1="4.2736" x2="-6.731" y2="5.7976" layer="51"/>
<rectangle x1="-8.509" y1="4.2736" x2="-8.001" y2="5.7976" layer="51"/>
</package>
<package name="43045-0612">
<wire x1="-6.5" y1="3.8" x2="6.5" y2="3.8" width="0.254" layer="21"/>
<wire x1="6.5" y1="3.8" x2="6.5" y2="-3.7" width="0.254" layer="21"/>
<wire x1="6.5" y1="-3.7" x2="-6.5" y2="-3.7" width="0.254" layer="21"/>
<wire x1="-6.5" y1="-3.7" x2="-6.5" y2="3.8" width="0.254" layer="21"/>
<wire x1="-2" y1="4.8" x2="2" y2="4.8" width="0.254" layer="27"/>
<wire x1="-2" y1="4.8" x2="-2" y2="3.8" width="0.254" layer="27"/>
<wire x1="2" y1="4.8" x2="2" y2="3.8" width="0.254" layer="27"/>
<pad name="1" x="3" y="-1.5" drill="1.1"/>
<pad name="2" x="0" y="-1.5" drill="1.1"/>
<pad name="3" x="-3" y="-1.5" drill="1.1"/>
<pad name="4" x="3" y="1.5" drill="1.1"/>
<pad name="5" x="0" y="1.5" drill="1.1"/>
<pad name="6" x="-3" y="1.5" drill="1.1"/>
<text x="-5.555" y="-5.35" size="1.27" layer="25">&gt;NAME</text>
<text x="7" y="-2" size="1.27" layer="21">1</text>
<hole x="6" y="2.5" drill="1"/>
<hole x="-6" y="2.5" drill="1"/>
</package>
<package name="SSOP20BU">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;</description>
<wire x1="-3.9" y1="2.925" x2="3.9" y2="2.925" width="0.1524" layer="21"/>
<wire x1="3.9" y1="2.925" x2="3.9" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="3.9" y1="-2.925" x2="-3.9" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="-3.9" y1="-2.925" x2="-3.9" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-3.738" y1="2.763" x2="3.738" y2="2.763" width="0.0508" layer="27"/>
<wire x1="3.738" y1="2.763" x2="3.738" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="3.738" y1="-2.763" x2="-3.738" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="-3.738" y1="-2.763" x2="-3.738" y2="2.763" width="0.0508" layer="27"/>
<circle x="-2.925" y="-1.95" radius="0.4596" width="0.1524" layer="21"/>
<smd name="20" x="-2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="19" x="-2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="18" x="-1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="17" x="-0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="16" x="-0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="15" x="0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="14" x="0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="12" x="2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="13" x="1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="11" x="2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="1" x="-2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="2" x="-2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="3" x="-1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="4" x="-0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="5" x="-0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="6" x="0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="7" x="0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="8" x="1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="9" x="2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="10" x="2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<text x="-4.445" y="-2.73" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.73" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
</package>
<package name="QSOP20">
<description>&lt;b&gt;Quarter-Size Small Outline Package&lt;/b&gt;</description>
<wire x1="-4.3" y1="1.9" x2="4.3" y2="1.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.9" x2="4.3" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.9" x2="-4.3" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.9" x2="-4.3" y2="1.9" width="0.2032" layer="21"/>
<smd name="1" x="-2.858" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="2" x="-2.223" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="3" x="-1.588" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="4" x="-0.953" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="5" x="-0.318" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="6" x="0.318" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="7" x="0.953" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="8" x="1.588" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="9" x="2.223" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="10" x="2.858" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="11" x="2.858" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="12" x="2.223" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="13" x="1.588" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="14" x="0.953" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="15" x="0.318" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="16" x="-0.318" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="17" x="-0.953" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="18" x="-1.588" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="19" x="-2.223" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="20" x="-2.858" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<text x="-4.7625" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.0325" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.0075" y1="-3.1" x2="-2.7075" y2="-1.95" layer="51"/>
<rectangle x1="-2.3725" y1="-3.1" x2="-2.0725" y2="-1.95" layer="51"/>
<rectangle x1="-1.7375" y1="-3.1" x2="-1.4375" y2="-1.95" layer="51"/>
<rectangle x1="-1.1025" y1="-3.1" x2="-0.8025" y2="-1.95" layer="51"/>
<rectangle x1="-0.4675" y1="-3.1" x2="-0.1675" y2="-1.95" layer="51"/>
<rectangle x1="0.1675" y1="-3.1" x2="0.4675" y2="-1.95" layer="51"/>
<rectangle x1="0.8025" y1="-3.1" x2="1.1025" y2="-1.95" layer="51"/>
<rectangle x1="1.4375" y1="-3.1" x2="1.7375" y2="-1.95" layer="51"/>
<rectangle x1="2.0725" y1="-3.1" x2="2.3725" y2="-1.95" layer="51"/>
<rectangle x1="2.7075" y1="-3.1" x2="3.0075" y2="-1.95" layer="51"/>
<rectangle x1="2.7075" y1="1.95" x2="3.0075" y2="3.1" layer="51"/>
<rectangle x1="2.0725" y1="1.95" x2="2.3725" y2="3.1" layer="51"/>
<rectangle x1="1.4375" y1="1.95" x2="1.7375" y2="3.1" layer="51"/>
<rectangle x1="0.8025" y1="1.95" x2="1.1025" y2="3.1" layer="51"/>
<rectangle x1="0.1675" y1="1.95" x2="0.4675" y2="3.1" layer="51"/>
<rectangle x1="-0.4675" y1="1.95" x2="-0.1675" y2="3.1" layer="51"/>
<rectangle x1="-1.1025" y1="1.95" x2="-0.8025" y2="3.1" layer="51"/>
<rectangle x1="-1.7375" y1="1.95" x2="-1.4375" y2="3.1" layer="51"/>
<rectangle x1="-2.3725" y1="1.95" x2="-2.0725" y2="3.1" layer="51"/>
<rectangle x1="-3.0075" y1="1.95" x2="-2.7075" y2="3.1" layer="51"/>
<rectangle x1="-4.2545" y1="-1.905" x2="-2.9845" y2="-0.635" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="OP-AMP+-">
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.8862" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-3.9116" x2="-2.54" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<text x="2.54" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="4.445" size="0.8128" layer="93" rot="R90">V+</text>
<text x="-1.27" y="-5.715" size="0.8128" layer="93" rot="R90">V-</text>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
<pin name="V+" x="-2.54" y="7.62" visible="pad" length="short" direction="pwr" rot="R270"/>
<pin name="V-" x="-2.54" y="-7.62" visible="pad" length="short" direction="pwr" rot="R90"/>
</symbol>
<symbol name="OP-AMP">
<wire x1="-3.81" y1="3.175" x2="-3.81" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="-5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="0" width="0.4064" layer="94"/>
<text x="2.54" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="-IN" x="-7.62" y="-2.54" visible="pad" length="short" direction="in"/>
<pin name="+IN" x="-7.62" y="2.54" visible="pad" length="short" direction="in"/>
<pin name="OUT" x="7.62" y="0" visible="pad" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="AD5308">
<wire x1="-15.24" y1="17.78" x2="-15.24" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-17.78" x2="27.94" y2="-17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="-17.78" x2="27.94" y2="17.78" width="0.254" layer="94"/>
<wire x1="27.94" y1="17.78" x2="-15.24" y2="17.78" width="0.254" layer="94"/>
<pin name="SCLK" x="-20.32" y="7.62" length="middle" direction="in" swaplevel="1"/>
<pin name="/SYNC" x="-20.32" y="2.54" length="middle" direction="in" function="dot" swaplevel="1"/>
<pin name="DIN" x="-20.32" y="-2.54" length="middle" direction="in" swaplevel="1"/>
<pin name="/LDAC" x="-20.32" y="-10.16" length="middle" direction="in" function="dot" swaplevel="1"/>
<pin name="VREF0" x="10.16" y="22.86" length="middle" direction="in" swaplevel="1" rot="R270"/>
<pin name="VREF4" x="15.24" y="22.86" length="middle" direction="in" swaplevel="1" rot="R270"/>
<pin name="VCC" x="0" y="22.86" length="middle" direction="pwr" swaplevel="1" rot="R270"/>
<pin name="GND" x="0" y="-22.86" length="middle" direction="pwr" swaplevel="1" rot="R90"/>
<pin name="VO0" x="33.02" y="10.16" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO1" x="33.02" y="7.62" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO2" x="33.02" y="5.08" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO3" x="33.02" y="2.54" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO4" x="33.02" y="0" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO5" x="33.02" y="-2.54" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO6" x="33.02" y="-5.08" length="middle" direction="out" swaplevel="1" rot="R180"/>
<pin name="VO7" x="33.02" y="-7.62" length="middle" direction="out" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="TVS_BIDIR">
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="1.905" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-0.508" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="1.905" x2="-1.778" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.524" y1="-1.905" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.905" x2="1.524" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<text x="-5.4864" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.0104" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="LTC1799">
<wire x1="-10.16" y1="20.32" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="20.32" width="0.254" layer="94"/>
<wire x1="7.62" y1="20.32" x2="-10.16" y2="20.32" width="0.254" layer="94"/>
<text x="-10.16" y="20.32" size="1.778" layer="94">&gt;NAME</text>
<text x="-10.16" y="-7.62" size="1.778" layer="94">&gt;VALUE</text>
<pin name="SET" x="-15.24" y="0" length="middle" direction="in"/>
<pin name="DIV" x="12.7" y="0" length="middle" direction="in" rot="R180"/>
<pin name="OUT" x="12.7" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="VDD" x="-15.24" y="15.24" length="middle" direction="pwr"/>
<pin name="GND" x="-15.24" y="7.62" length="middle" direction="pwr"/>
</symbol>
<symbol name="LS7266">
<wire x1="-12.7" y1="17.78" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="12.7" y2="-35.56" width="0.254" layer="94"/>
<wire x1="12.7" y1="-35.56" x2="-12.7" y2="-35.56" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-35.56" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<text x="5.08" y="20.32" size="1.27" layer="95">&gt;NAME</text>
<text x="5.08" y="17.78" size="1.27" layer="96">&gt;VALUE</text>
<pin name="D0" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="D1" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="D2" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="D3" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="D4" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="D5" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="D6" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="D7" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="CTL/DATA" x="17.78" y="-27.94" length="middle" direction="in" rot="R180"/>
<pin name="CLK" x="-17.78" y="12.7" length="middle" direction="in"/>
<pin name="/YLOAD" x="17.78" y="-7.62" length="middle" direction="in" rot="R180"/>
<pin name="/XLOAD" x="17.78" y="-10.16" length="middle" direction="in" rot="R180"/>
<pin name="/XRST" x="17.78" y="-12.7" length="middle" direction="in" rot="R180"/>
<pin name="Y/X" x="17.78" y="-17.78" length="middle" direction="in" rot="R180"/>
<pin name="/RD" x="17.78" y="-20.32" length="middle" direction="in" rot="R180"/>
<pin name="/CS" x="17.78" y="-22.86" length="middle" direction="in" rot="R180"/>
<pin name="/WR" x="17.78" y="-25.4" length="middle" direction="in" rot="R180"/>
<pin name="XA" x="-17.78" y="2.54" length="middle" direction="in"/>
<pin name="XB" x="-17.78" y="0" length="middle" direction="in"/>
<pin name="YA" x="-17.78" y="-5.08" length="middle" direction="in"/>
<pin name="YB" x="-17.78" y="-7.62" length="middle" direction="in"/>
<pin name="YFLG1" x="-17.78" y="-12.7" length="middle" direction="out"/>
<pin name="YFLG2" x="-17.78" y="-15.24" length="middle" direction="out"/>
<pin name="XFLG1" x="-17.78" y="-20.32" length="middle" direction="out"/>
<pin name="XFLG2" x="-17.78" y="-22.86" length="middle" direction="out"/>
<pin name="VCC" x="0" y="22.86" length="middle" direction="pwr" rot="R270"/>
<pin name="GND" x="0" y="-40.64" length="middle" direction="pwr" rot="R90"/>
<pin name="/YRST" x="17.78" y="-15.24" length="middle" direction="in" rot="R180"/>
</symbol>
<symbol name="ADS8344">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CH0" x="-12.7" y="12.7" length="short" direction="in"/>
<pin name="CH1" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="CH2" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="CH3" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="CH4" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="CH5" x="-12.7" y="0" length="short" direction="in"/>
<pin name="CH6" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="CH7" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="COM" x="-12.7" y="-15.24" length="short" direction="in"/>
<pin name="!SHDN" x="12.7" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="VREF" x="-12.7" y="-12.7" length="short" direction="in"/>
<pin name="VCC" x="12.7" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="12.7" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="DOUT" x="12.7" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="DI" x="12.7" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="!CS" x="12.7" y="0" length="short" direction="in" rot="R180"/>
<pin name="DCLK" x="12.7" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="VCC@1" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="BUSY" x="12.7" y="-5.08" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AD824" prefix="IC">
<description>&lt;b&gt;Operational Amplifiers&lt;/b&gt;&lt;p&gt;
Source: http://focus.ti.com/lit/ds/sbos073/sbos073.pdf</description>
<gates>
<gate name="A" symbol="OP-AMP+-" x="-17.78" y="12.7"/>
<gate name="B" symbol="OP-AMP" x="10.16" y="12.7"/>
<gate name="C" symbol="OP-AMP" x="-17.78" y="-7.62"/>
<gate name="D" symbol="OP-AMP" x="10.16" y="-7.62"/>
</gates>
<devices>
<device name="R" package="SO14">
<connects>
<connect gate="A" pin="+IN" pad="3"/>
<connect gate="A" pin="-IN" pad="2"/>
<connect gate="A" pin="OUT" pad="1"/>
<connect gate="A" pin="V+" pad="4"/>
<connect gate="A" pin="V-" pad="11"/>
<connect gate="B" pin="+IN" pad="5"/>
<connect gate="B" pin="-IN" pad="6"/>
<connect gate="B" pin="OUT" pad="7"/>
<connect gate="C" pin="+IN" pad="10"/>
<connect gate="C" pin="-IN" pad="9"/>
<connect gate="C" pin="OUT" pad="8"/>
<connect gate="D" pin="+IN" pad="12"/>
<connect gate="D" pin="-IN" pad="13"/>
<connect gate="D" pin="OUT" pad="14"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AD5308">
<gates>
<gate name="G$1" symbol="AD5308" x="0" y="0" swaplevel="1"/>
</gates>
<devices>
<device name="" package="TSSOP16">
<connects>
<connect gate="G$1" pin="/LDAC" pad="1"/>
<connect gate="G$1" pin="/SYNC" pad="2"/>
<connect gate="G$1" pin="DIN" pad="15"/>
<connect gate="G$1" pin="GND" pad="14"/>
<connect gate="G$1" pin="SCLK" pad="16"/>
<connect gate="G$1" pin="VCC" pad="3"/>
<connect gate="G$1" pin="VO0" pad="4"/>
<connect gate="G$1" pin="VO1" pad="5"/>
<connect gate="G$1" pin="VO2" pad="6"/>
<connect gate="G$1" pin="VO3" pad="7"/>
<connect gate="G$1" pin="VO4" pad="10"/>
<connect gate="G$1" pin="VO5" pad="11"/>
<connect gate="G$1" pin="VO6" pad="12"/>
<connect gate="G$1" pin="VO7" pad="13"/>
<connect gate="G$1" pin="VREF0" pad="8"/>
<connect gate="G$1" pin="VREF4" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B725" prefix="90">
<gates>
<gate name="G$1" symbol="TVS_BIDIR" x="0" y="0"/>
</gates>
<devices>
<device name="90" package="R0402">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="00" package="R0603">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="43045-0412">
<gates>
<gate name="1" symbol="MV" x="0" y="10.16"/>
<gate name="2" symbol="MV" x="0" y="2.54"/>
<gate name="3" symbol="MV" x="0" y="-5.08"/>
<gate name="4" symbol="MV" x="0" y="-12.7"/>
</gates>
<devices>
<device name="" package="43045-0412">
<connects>
<connect gate="1" pin="S" pad="1"/>
<connect gate="2" pin="S" pad="2"/>
<connect gate="3" pin="S" pad="3"/>
<connect gate="4" pin="S" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LTC1799">
<gates>
<gate name="G$1" symbol="LTC1799" x="2.54" y="-7.62"/>
</gates>
<devices>
<device name="S5" package="SOT23-5L">
<connects>
<connect gate="G$1" pin="DIV" pad="4"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="SET" pad="3"/>
<connect gate="G$1" pin="VDD" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LS7266R1-S">
<gates>
<gate name="A" symbol="LS7266" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SO28">
<connects>
<connect gate="A" pin="/CS" pad="15"/>
<connect gate="A" pin="/RD" pad="16"/>
<connect gate="A" pin="/WR" pad="14"/>
<connect gate="A" pin="/XLOAD" pad="19"/>
<connect gate="A" pin="/XRST" pad="18"/>
<connect gate="A" pin="/YLOAD" pad="1"/>
<connect gate="A" pin="/YRST" pad="28"/>
<connect gate="A" pin="CLK" pad="2"/>
<connect gate="A" pin="CTL/DATA" pad="13"/>
<connect gate="A" pin="D0" pad="4"/>
<connect gate="A" pin="D1" pad="5"/>
<connect gate="A" pin="D2" pad="6"/>
<connect gate="A" pin="D3" pad="7"/>
<connect gate="A" pin="D4" pad="8"/>
<connect gate="A" pin="D5" pad="9"/>
<connect gate="A" pin="D6" pad="10"/>
<connect gate="A" pin="D7" pad="11"/>
<connect gate="A" pin="GND" pad="12"/>
<connect gate="A" pin="VCC" pad="3"/>
<connect gate="A" pin="XA" pad="20"/>
<connect gate="A" pin="XB" pad="21"/>
<connect gate="A" pin="XFLG1" pad="22"/>
<connect gate="A" pin="XFLG2" pad="23"/>
<connect gate="A" pin="Y/X" pad="17"/>
<connect gate="A" pin="YA" pad="25"/>
<connect gate="A" pin="YB" pad="24"/>
<connect gate="A" pin="YFLG1" pad="27"/>
<connect gate="A" pin="YFLG2" pad="26"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="43045-0612">
<gates>
<gate name="1" symbol="MV" x="0" y="20.32"/>
<gate name="2" symbol="MV" x="0" y="15.24"/>
<gate name="3" symbol="MV" x="0" y="10.16"/>
<gate name="4" symbol="MV" x="0" y="5.08"/>
<gate name="5" symbol="MV" x="0" y="0"/>
<gate name="6" symbol="MV" x="0" y="-5.08"/>
</gates>
<devices>
<device name="" package="43045-0612">
<connects>
<connect gate="1" pin="S" pad="1"/>
<connect gate="2" pin="S" pad="2"/>
<connect gate="3" pin="S" pad="3"/>
<connect gate="4" pin="S" pad="4"/>
<connect gate="5" pin="S" pad="5"/>
<connect gate="6" pin="S" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ADS8344" prefix="IC">
<description>&lt;b&gt;16-Bit, 8-Channel Serial Output Sampling ADC&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="ADS8344" x="0" y="0"/>
</gates>
<devices>
<device name="U" package="SSOP20BU">
<connects>
<connect gate="G$1" pin="!CS" pad="18"/>
<connect gate="G$1" pin="!SHDN" pad="10"/>
<connect gate="G$1" pin="BUSY" pad="16"/>
<connect gate="G$1" pin="CH0" pad="1"/>
<connect gate="G$1" pin="CH1" pad="2"/>
<connect gate="G$1" pin="CH2" pad="3"/>
<connect gate="G$1" pin="CH3" pad="4"/>
<connect gate="G$1" pin="CH4" pad="5"/>
<connect gate="G$1" pin="CH5" pad="6"/>
<connect gate="G$1" pin="CH6" pad="7"/>
<connect gate="G$1" pin="CH7" pad="8"/>
<connect gate="G$1" pin="COM" pad="9"/>
<connect gate="G$1" pin="DCLK" pad="19"/>
<connect gate="G$1" pin="DI" pad="17"/>
<connect gate="G$1" pin="DOUT" pad="15"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="14"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCC@1" pad="20"/>
<connect gate="G$1" pin="VREF" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="QSOP20">
<connects>
<connect gate="G$1" pin="!CS" pad="18"/>
<connect gate="G$1" pin="!SHDN" pad="10"/>
<connect gate="G$1" pin="BUSY" pad="16"/>
<connect gate="G$1" pin="CH0" pad="1"/>
<connect gate="G$1" pin="CH1" pad="2"/>
<connect gate="G$1" pin="CH2" pad="3"/>
<connect gate="G$1" pin="CH3" pad="4"/>
<connect gate="G$1" pin="CH4" pad="5"/>
<connect gate="G$1" pin="CH5" pad="6"/>
<connect gate="G$1" pin="CH6" pad="7"/>
<connect gate="G$1" pin="CH7" pad="8"/>
<connect gate="G$1" pin="COM" pad="9"/>
<connect gate="G$1" pin="DCLK" pad="19"/>
<connect gate="G$1" pin="DI" pad="17"/>
<connect gate="G$1" pin="DOUT" pad="15"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="14"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCC@1" pad="20"/>
<connect gate="G$1" pin="VREF" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="infineon">
<packages>
<package name="TO252-5-11">
<description>&lt;b&gt;Dpak-5 Pin&lt;/b&gt; P-TO252-5-11 &lt;/b&gt;&lt;p&gt;
Source: www.infineon.com .. BTS428L2_20030912.pdf</description>
<wire x1="-3.15" y1="3" x2="-2.75" y2="3" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="3" x2="2.75" y2="3" width="0.2032" layer="51"/>
<wire x1="2.75" y1="3" x2="3.15" y2="3" width="0.2032" layer="51"/>
<wire x1="3.15" y1="3" x2="3.15" y2="-3" width="0.2032" layer="21"/>
<wire x1="3.15" y1="-3" x2="-3.15" y2="-3" width="0.2032" layer="21"/>
<wire x1="-3.15" y1="-3" x2="-3.15" y2="3" width="0.2032" layer="21"/>
<wire x1="-2.75" y1="3" x2="-2.75" y2="4" width="0.2032" layer="51"/>
<wire x1="-2.75" y1="4" x2="-2" y2="4" width="0.2032" layer="51"/>
<wire x1="-2" y1="4" x2="-2" y2="3.85" width="0.2032" layer="51"/>
<wire x1="-2" y1="3.85" x2="2" y2="3.85" width="0.2032" layer="51"/>
<wire x1="2" y1="3.85" x2="2" y2="4" width="0.2032" layer="51"/>
<wire x1="2" y1="4" x2="2.75" y2="4" width="0.2032" layer="51"/>
<wire x1="2.75" y1="4" x2="2.75" y2="3" width="0.2032" layer="51"/>
<wire x1="0.6351" y1="1.1472" x2="0.6815" y2="1.0719" width="0.0926" layer="51" curve="50.5163"/>
<wire x1="0.6815" y1="1.0719" x2="0.7568" y2="1.0603" width="0.0926" layer="51" curve="48.799462"/>
<wire x1="0.7568" y1="1.0603" x2="0.8147" y2="1.1124" width="0.0926" layer="51" curve="52.776062"/>
<wire x1="0.8147" y1="1.1124" x2="0.8321" y2="1.182" width="0.0926" layer="51" curve="15.189287"/>
<wire x1="0.8321" y1="1.182" x2="0.8321" y2="1.3442" width="0.0926" layer="51" curve="12.862147"/>
<wire x1="0.8321" y1="1.3442" x2="0.8031" y2="1.4253" width="0.0926" layer="51" curve="26.516294"/>
<wire x1="0.8031" y1="1.4253" x2="0.751" y2="1.4601" width="0.0926" layer="51" curve="46.660241"/>
<wire x1="0.751" y1="1.4601" x2="0.7046" y2="1.4601" width="0.0926" layer="51" curve="20.946986"/>
<wire x1="0.7046" y1="1.4601" x2="0.6467" y2="1.4079" width="0.0926" layer="51" curve="63.371643"/>
<wire x1="0.6467" y1="1.4079" x2="0.6293" y2="1.3152" width="0.0926" layer="51" curve="11.340973"/>
<wire x1="0.6293" y1="1.3152" x2="0.6351" y2="1.153" width="0.0926" layer="51" curve="14.04857"/>
<wire x1="0.4439" y1="1.2921" x2="0.4439" y2="1.3152" width="0.0926" layer="51"/>
<wire x1="0.4439" y1="1.3152" x2="0.4207" y2="1.4137" width="0.0926" layer="51" curve="26.531483"/>
<wire x1="0.4207" y1="1.4137" x2="0.4091" y2="1.4311" width="0.0926" layer="51" curve="14.250033"/>
<wire x1="0.4091" y1="1.4311" x2="0.386" y2="1.4543" width="0.0926" layer="51" curve="8.016267"/>
<wire x1="0.386" y1="1.4543" x2="0.2759" y2="1.4311" width="0.0926" layer="51" curve="105.712638"/>
<wire x1="0.2759" y1="1.4311" x2="0.2585" y2="1.3848" width="0.0926" layer="51" curve="9.226419"/>
<wire x1="0.2585" y1="1.3848" x2="0.2469" y2="1.3326" width="0.0926" layer="51" curve="6.956601"/>
<wire x1="0.2469" y1="1.3326" x2="0.2527" y2="1.1588" width="0.0926" layer="51" curve="21.945486"/>
<wire x1="0.2527" y1="1.1588" x2="0.2759" y2="1.1009" width="0.0926" layer="51" curve="17.892453"/>
<wire x1="0.2759" y1="1.1009" x2="0.2991" y2="1.0719" width="0.0926" layer="51" curve="15.844294"/>
<wire x1="0.2991" y1="1.0719" x2="0.3686" y2="1.0545" width="0.0926" layer="51" curve="51.403204"/>
<wire x1="0.3686" y1="1.0545" x2="0.4193" y2="1.0805" width="0.0926" layer="51" curve="44.347321"/>
<wire x1="0.4193" y1="1.0805" x2="0.4411" y2="1.1381" width="0.0926" layer="51" curve="32.502594"/>
<wire x1="0.4411" y1="1.1381" x2="0.4439" y2="1.1936" width="0.0926" layer="51" curve="8.167043"/>
<wire x1="0.4963" y1="0.6453" x2="0.5024" y2="0.6177" width="0.0346" layer="51" curve="25.274365"/>
<wire x1="0.5024" y1="0.6177" x2="0.5192" y2="0.5968" width="0.0346" layer="51" curve="28.732379"/>
<wire x1="0.5192" y1="0.5968" x2="0.55" y2="0.5834" width="0.0346" layer="51" curve="29.115849"/>
<wire x1="0.55" y1="0.5834" x2="0.5888" y2="0.591" width="0.0346" layer="51" curve="42.029398"/>
<wire x1="0.5888" y1="0.591" x2="0.6177" y2="0.6258" width="0.0346" layer="51" curve="42.408509"/>
<wire x1="0.6177" y1="0.6258" x2="0.6235" y2="0.6605" width="0.0346" layer="51" curve="17.830845"/>
<wire x1="0.6235" y1="0.6605" x2="0.6235" y2="0.7532" width="0.0346" layer="51" curve="1.010691"/>
<wire x1="0.6235" y1="0.7532" x2="0.6061" y2="0.788" width="0.0346" layer="51" curve="52.095444"/>
<wire x1="0.6061" y1="0.788" x2="0.5888" y2="0.7996" width="0.0346" layer="51" curve="7.066098"/>
<wire x1="0.5888" y1="0.7996" x2="0.525" y2="0.7996" width="0.0346" layer="51" curve="60.682678"/>
<wire x1="0.525" y1="0.7996" x2="0.5019" y2="0.7764" width="0.0346" layer="51" curve="29.506332"/>
<wire x1="0.5019" y1="0.7764" x2="0.5019" y2="0.7011" width="0.0346" layer="51" curve="60.391666"/>
<wire x1="0.5019" y1="0.7011" x2="0.525" y2="0.6779" width="0.0346" layer="51" curve="29.506332"/>
<wire x1="0.525" y1="0.6779" x2="0.5772" y2="0.6721" width="0.0346" layer="51" curve="48.088447"/>
<wire x1="0.5772" y1="0.6721" x2="0.6119" y2="0.7011" width="0.0346" layer="51" curve="44.50857"/>
<wire x1="0.6119" y1="0.7011" x2="0.6235" y2="0.7358" width="0.0346" layer="51" curve="18.76533"/>
<wire x1="0.6235" y1="0.7358" x2="0.6235" y2="0.8227" width="0.0346" layer="51" curve="1.67181"/>
<wire x1="-1.0336" y1="0.7532" x2="-1.0336" y2="0.759" width="0.0346" layer="51"/>
<wire x1="-1.0336" y1="0.759" x2="-1.0625" y2="0.8054" width="0.0346" layer="51" curve="46.770442"/>
<wire x1="-1.0625" y1="0.8054" x2="-1.1147" y2="0.8112" width="0.0346" layer="51" curve="55.602918"/>
<wire x1="-1.1147" y1="0.8112" x2="-1.1552" y2="0.759" width="0.0346" layer="51" curve="59.408808"/>
<wire x1="-1.1552" y1="0.759" x2="-1.1552" y2="0.73" width="0.0346" layer="51" curve="0.473516"/>
<wire x1="-1.1552" y1="0.73" x2="-1.1321" y2="0.6837" width="0.0346" layer="51" curve="45.840301"/>
<wire x1="-1.1321" y1="0.6837" x2="-1.0567" y2="0.6779" width="0.0346" layer="51" curve="69.24831"/>
<wire x1="-1.0567" y1="0.6779" x2="-1.0336" y2="0.7069" width="0.0346" layer="51" curve="23.22831"/>
<wire x1="1.0464" y1="0.7532" x2="1.0464" y2="0.759" width="0.0346" layer="51"/>
<wire x1="1.0464" y1="0.759" x2="1.0175" y2="0.8054" width="0.0346" layer="51" curve="46.770442"/>
<wire x1="1.0175" y1="0.8054" x2="0.9653" y2="0.8112" width="0.0346" layer="51" curve="55.602918"/>
<wire x1="0.9653" y1="0.8112" x2="0.9248" y2="0.759" width="0.0346" layer="51" curve="59.516792"/>
<wire x1="0.9248" y1="0.759" x2="0.9248" y2="0.73" width="0.0346" layer="51" curve="0.473516"/>
<wire x1="0.9248" y1="0.73" x2="0.9479" y2="0.6837" width="0.0346" layer="51" curve="45.840301"/>
<wire x1="0.9479" y1="0.6837" x2="1.0233" y2="0.6779" width="0.0346" layer="51" curve="69.24831"/>
<wire x1="1.0233" y1="0.6779" x2="1.0464" y2="0.7069" width="0.0346" layer="51" curve="23.100883"/>
<wire x1="1.174" y1="0.6895" x2="1.1971" y2="0.6721" width="0.0346" layer="51" curve="16.054475"/>
<wire x1="1.1971" y1="0.6721" x2="1.2377" y2="0.6779" width="0.0346" layer="51" curve="74.698698"/>
<wire x1="1.2377" y1="0.6779" x2="1.2435" y2="0.7185" width="0.0346" layer="51" curve="73.268228"/>
<wire x1="1.2435" y1="0.7185" x2="1.2203" y2="0.7416" width="0.0346" layer="51" curve="33.330651"/>
<wire x1="1.2203" y1="0.7416" x2="1.1913" y2="0.759" width="0.0346" layer="51" curve="-5.57137"/>
<wire x1="1.1913" y1="0.759" x2="1.1797" y2="0.7822" width="0.0346" layer="51" curve="-59.489763"/>
<wire x1="1.1797" y1="0.7822" x2="1.1913" y2="0.8054" width="0.0346" layer="51" curve="-46.499586"/>
<wire x1="1.1913" y1="0.8054" x2="1.2145" y2="0.8112" width="0.0346" layer="51" curve="-52.35058"/>
<wire x1="1.2145" y1="0.8112" x2="1.2377" y2="0.7996" width="0.0346" layer="51" curve="-28.90978"/>
<circle x="-1.2595" y="2.0337" radius="0.197" width="0" layer="51"/>
<circle x="-0.3499" y="1.6397" radius="0.058" width="0" layer="51"/>
<circle x="-0.1065" y="0.7358" radius="0.0695" width="0.0116" layer="51"/>
<circle x="0.2991" y="0.7358" radius="0.0697" width="0.0346" layer="51"/>
<circle x="-0.1123" y="0.7358" radius="0.0697" width="0.0346" layer="51"/>
<circle x="0.78" y="0.8807" radius="0.0258" width="0" layer="51"/>
<smd name="1" x="-2.28" y="-5.41" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="-1.14" y="-5.41" dx="0.8" dy="1.6" layer="1"/>
<smd name="4" x="1.14" y="-5.41" dx="0.8" dy="1.6" layer="1"/>
<smd name="5" x="2.28" y="-5.41" dx="0.8" dy="1.6" layer="1"/>
<smd name="TAB" x="0" y="1.3" dx="6" dy="6" layer="1"/>
<text x="-3.81" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-3.175" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.58" y1="-3.85" x2="-1.98" y2="-3.05" layer="21"/>
<rectangle x1="-2.73" y1="-4.5" x2="-1.83" y2="-3.55" layer="21"/>
<rectangle x1="-1.44" y1="-3.85" x2="-0.84" y2="-3.05" layer="21"/>
<rectangle x1="-1.59" y1="-4.5" x2="-0.69" y2="-3.55" layer="21"/>
<rectangle x1="-0.3" y1="-3.85" x2="0.3" y2="-3.05" layer="21"/>
<rectangle x1="-0.45" y1="-3.9" x2="0.45" y2="-3.55" layer="21"/>
<rectangle x1="0.84" y1="-3.85" x2="1.44" y2="-3.05" layer="21"/>
<rectangle x1="0.69" y1="-4.5" x2="1.59" y2="-3.55" layer="21"/>
<rectangle x1="1.98" y1="-3.85" x2="2.58" y2="-3.05" layer="21"/>
<rectangle x1="1.83" y1="-4.5" x2="2.73" y2="-3.55" layer="21"/>
<rectangle x1="-2.58" y1="-5.85" x2="-1.98" y2="-4.45" layer="51"/>
<rectangle x1="-1.44" y1="-5.85" x2="-0.84" y2="-4.45" layer="51"/>
<rectangle x1="0.84" y1="-5.85" x2="1.44" y2="-4.45" layer="51"/>
<rectangle x1="1.98" y1="-5.85" x2="2.58" y2="-4.45" layer="51"/>
<rectangle x1="-1.3059" y1="1.0197" x2="-1.2074" y2="1.7266" layer="51"/>
<rectangle x1="-1.0741" y1="1.0197" x2="-0.9814" y2="1.4949" layer="51"/>
<rectangle x1="-0.8829" y1="1.0197" x2="-0.7902" y2="1.379" layer="51"/>
<rectangle x1="-0.6917" y1="1.4195" x2="-0.4831" y2="1.4949" layer="51"/>
<rectangle x1="-0.6454" y1="1.0197" x2="-0.5527" y2="1.6861" layer="51"/>
<rectangle x1="-0.3962" y1="1.0197" x2="-0.3035" y2="1.5006" layer="51"/>
<rectangle x1="0.0094" y1="1.0197" x2="0.1021" y2="1.3732" layer="51"/>
<rectangle x1="0.2064" y1="1.2399" x2="0.4903" y2="1.3094" layer="51"/>
<rectangle x1="0.9885" y1="1.0197" x2="1.0813" y2="1.4949" layer="51"/>
<rectangle x1="1.1797" y1="1.0197" x2="1.2725" y2="1.379" layer="51"/>
<rectangle x1="-0.1819" y1="1.0197" x2="-0.0891" y2="1.4949" layer="51"/>
<rectangle x1="-1.3117" y1="0.6547" x2="-1.2769" y2="0.8807" layer="51"/>
<rectangle x1="-1.3233" y1="0.788" x2="-1.2479" y2="0.817" layer="51"/>
<rectangle x1="-1.1668" y1="0.73" x2="-1.0162" y2="0.759" layer="51"/>
<rectangle x1="-0.6801" y1="0.6547" x2="-0.6454" y2="0.9444" layer="51"/>
<rectangle x1="-0.5816" y1="0.6547" x2="-0.5469" y2="0.7764" layer="51"/>
<rectangle x1="-0.431" y1="0.6547" x2="-0.3963" y2="0.817" layer="51"/>
<rectangle x1="-0.3325" y1="0.6547" x2="-0.2978" y2="0.7764" layer="51"/>
<rectangle x1="0.079" y1="0.6547" x2="0.1136" y2="0.9443" layer="51"/>
<rectangle x1="0.7626" y1="0.6547" x2="0.7973" y2="0.8227" layer="51"/>
<rectangle x1="0.9132" y1="0.73" x2="1.0638" y2="0.759" layer="51"/>
<polygon width="0.0102" layer="51">
<vertex x="-1.4507" y="1.9062" curve="-31.391799"/>
<vertex x="-1.4913" y="2.0453"/>
<vertex x="-1.6593" y="1.9873" curve="6.588458"/>
<vertex x="-1.7578" y="1.9468" curve="1.838803"/>
<vertex x="-1.9548" y="1.8483" curve="8.257493"/>
<vertex x="-2.1054" y="1.7556" curve="3.883128"/>
<vertex x="-2.2329" y="1.6571" curve="10.709256"/>
<vertex x="-2.3024" y="1.5876" curve="-3.615587"/>
<vertex x="-2.3894" y="1.4891" curve="21.558374"/>
<vertex x="-2.4705" y="1.3616" curve="1.368686"/>
<vertex x="-2.4994" y="1.2863" curve="21.04052"/>
<vertex x="-2.5284" y="1.1182" curve="14.83372"/>
<vertex x="-2.5052" y="0.956" curve="20.434382"/>
<vertex x="-2.4241" y="0.788" curve="11.786339"/>
<vertex x="-2.2735" y="0.62" curve="10.152173"/>
<vertex x="-2.0823" y="0.4751" curve="8.28339"/>
<vertex x="-1.7984" y="0.3245" curve="7.802828"/>
<vertex x="-1.4623" y="0.2028" curve="3.951224"/>
<vertex x="-1.161" y="0.1275" curve="2.412405"/>
<vertex x="-0.8887" y="0.0753" curve="5.073858"/>
<vertex x="-0.6106" y="0.0406" curve="4.25527"/>
<vertex x="0.0673" y="0.0116" curve="13.441337"/>
<vertex x="1.2029" y="0.1391" curve="10.613893"/>
<vertex x="1.7939" y="0.3361" curve="8.146352"/>
<vertex x="2.2111" y="0.5562" curve="15.823896"/>
<vertex x="2.4197" y="0.73" curve="172.109628"/>
<vertex x="2.4139" y="0.7358" curve="-5.433492"/>
<vertex x="2.2748" y="0.6258" curve="-13.999292"/>
<vertex x="2.0199" y="0.4867" curve="-0.116677"/>
<vertex x="1.7707" y="0.3882" curve="-13.042388"/>
<vertex x="1.3594" y="0.2781" curve="1.960346"/>
<vertex x="1.116" y="0.2376" curve="-8.871401"/>
<vertex x="0.3976" y="0.1622" curve="-3.114208"/>
<vertex x="-0.176" y="0.1622" curve="-5.185695"/>
<vertex x="-0.5758" y="0.1912" curve="-5.19899"/>
<vertex x="-1.0336" y="0.2665" curve="-5.873861"/>
<vertex x="-1.4044" y="0.365" curve="-8.501763"/>
<vertex x="-1.6188" y="0.4519" curve="-3.187269"/>
<vertex x="-1.8157" y="0.5562" curve="-13.025524"/>
<vertex x="-2.007" y="0.6953" curve="-11.411402"/>
<vertex x="-2.1518" y="0.8575" curve="-22.053503"/>
<vertex x="-2.2329" y="1.0313" curve="-6.690142"/>
<vertex x="-2.2503" y="1.124" curve="-26.38927"/>
<vertex x="-2.2329" y="1.2921" curve="-19.878963"/>
<vertex x="-2.146" y="1.4485" curve="-13.873453"/>
<vertex x="-1.9664" y="1.6223" curve="-7.551782"/>
<vertex x="-1.7462" y="1.7672" curve="-6.010589"/>
<vertex x="-1.5376" y="1.8715" curve="-3.595975"/>
</polygon>
<polygon width="0.0102" layer="51">
<vertex x="-1.0394" y="2.0453"/>
<vertex x="-0.9524" y="2.0743" curve="-10.251571"/>
<vertex x="-0.7323" y="2.1264" curve="0.331965"/>
<vertex x="-0.3383" y="2.1843" curve="-3.88686"/>
<vertex x="-0.1876" y="2.2017" curve="-4.876977"/>
<vertex x="0.1137" y="2.2133"/>
<vertex x="-0.0833" y="2.2133" curve="1.332929"/>
<vertex x="-0.5816" y="2.2075" curve="6.144407"/>
<vertex x="-0.8076" y="2.1901" curve="2.712231"/>
<vertex x="-1.0683" y="2.1496" curve="-35.452386"/>
</polygon>
<polygon width="0.0102" layer="51">
<vertex x="1.2667" y="1.3732"/>
<vertex x="1.2667" y="1.4021" curve="34.212031"/>
<vertex x="1.2435" y="1.4775" curve="47.803061"/>
<vertex x="1.1971" y="1.5064" curve="25.543907"/>
<vertex x="1.1276" y="1.5006" curve="47.389743"/>
<vertex x="1.0813" y="1.4601" curve="26.063264"/>
<vertex x="1.0639" y="1.379" curve="178.024479"/>
<vertex x="1.0755" y="1.379" curve="-57.579798"/>
<vertex x="1.0986" y="1.4195" curve="-62.869986"/>
<vertex x="1.1508" y="1.4195" curve="-59.127037"/>
<vertex x="1.174" y="1.3964" curve="-20.212776"/>
<vertex x="1.1855" y="1.3674"/>
</polygon>
<polygon width="0.0116" layer="51">
<vertex x="-0.5585" y="1.6803"/>
<vertex x="-0.5585" y="1.6861" curve="-77.825279"/>
<vertex x="-0.5353" y="1.715" curve="-24.601078"/>
<vertex x="-0.5121" y="1.715" curve="-18.988937"/>
<vertex x="-0.4831" y="1.7034"/>
<vertex x="-0.4831" y="1.7961"/>
<vertex x="-0.5005" y="1.8019" curve="36.751586"/>
<vertex x="-0.5874" y="1.8019" curve="45.408376"/>
<vertex x="-0.6338" y="1.7614" curve="28.072487"/>
<vertex x="-0.6396" y="1.7208" curve="-19.854893"/>
<vertex x="-0.6396" y="1.6803"/>
</polygon>
<polygon width="0.0102" layer="51">
<vertex x="-0.796" y="1.3732"/>
<vertex x="-0.796" y="1.4021" curve="34.212031"/>
<vertex x="-0.8192" y="1.4775" curve="47.803061"/>
<vertex x="-0.8656" y="1.5064" curve="25.543907"/>
<vertex x="-0.9351" y="1.5006" curve="47.30993"/>
<vertex x="-0.9814" y="1.4601" curve="26.063264"/>
<vertex x="-0.9988" y="1.379" curve="178.024479"/>
<vertex x="-0.9872" y="1.379" curve="-57.579798"/>
<vertex x="-0.9641" y="1.4195" curve="-62.869986"/>
<vertex x="-0.9119" y="1.4195" curve="-59.368405"/>
<vertex x="-0.8887" y="1.3964" curve="-20.261561"/>
<vertex x="-0.8772" y="1.3674"/>
</polygon>
<polygon width="0.0102" layer="51">
<vertex x="0.0963" y="1.3732"/>
<vertex x="0.0963" y="1.4021" curve="34.212031"/>
<vertex x="0.0731" y="1.4775" curve="47.803061"/>
<vertex x="0.0267" y="1.5064" curve="25.543907"/>
<vertex x="-0.0428" y="1.5006" curve="47.30993"/>
<vertex x="-0.0891" y="1.4601" curve="26.063264"/>
<vertex x="-0.1065" y="1.379" curve="178.024479"/>
<vertex x="-0.0949" y="1.379" curve="-57.579798"/>
<vertex x="-0.0718" y="1.4195" curve="-62.869986"/>
<vertex x="-0.0196" y="1.4195" curve="-59.368405"/>
<vertex x="0.0036" y="1.3964" curve="-20.212776"/>
<vertex x="0.0151" y="1.3674"/>
</polygon>
<polygon width="0.0058" layer="51">
<vertex x="-0.796" y="0.7706"/>
<vertex x="-0.796" y="0.8112"/>
<vertex x="-0.8076" y="0.817" curve="67.399158"/>
<vertex x="-0.9003" y="0.8054" curve="49.560797"/>
<vertex x="-0.9293" y="0.7416" curve="56.073444"/>
<vertex x="-0.8887" y="0.6663" curve="67.475342"/>
<vertex x="-0.796" y="0.6663"/>
<vertex x="-0.796" y="0.7069"/>
<vertex x="-0.8018" y="0.7011" curve="-80.981953"/>
<vertex x="-0.8713" y="0.6953" curve="-41.829572"/>
<vertex x="-0.8945" y="0.73" curve="-25.492604"/>
<vertex x="-0.8945" y="0.759" curve="-51.892995"/>
<vertex x="-0.8713" y="0.788" curve="-34.657774"/>
<vertex x="-0.8308" y="0.7938" curve="-48.640994"/>
</polygon>
<polygon width="0.0116" layer="51">
<vertex x="-0.5527" y="0.759"/>
<vertex x="-0.5527" y="0.7764" curve="59.178608"/>
<vertex x="-0.5758" y="0.817" curve="81.84062"/>
<vertex x="-0.6396" y="0.8054" curve="24.203979"/>
<vertex x="-0.6512" y="0.7822" curve="29.0334"/>
<vertex x="-0.6512" y="0.759"/>
<vertex x="-0.6454" y="0.7764" curve="-76.177546"/>
<vertex x="-0.6048" y="0.7996" curve="-80.099456"/>
<vertex x="-0.5758" y="0.7648"/>
</polygon>
<polygon width="0.0116" layer="51">
<vertex x="-0.3036" y="0.759"/>
<vertex x="-0.3036" y="0.7764" curve="59.178608"/>
<vertex x="-0.3267" y="0.817" curve="81.84062"/>
<vertex x="-0.3905" y="0.8054" curve="24.203979"/>
<vertex x="-0.4021" y="0.7822" curve="29.0334"/>
<vertex x="-0.4021" y="0.759"/>
<vertex x="-0.3963" y="0.7764" curve="-76.177546"/>
<vertex x="-0.3557" y="0.7996" curve="-80.099456"/>
<vertex x="-0.3267" y="0.7648"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="BTS428L2">
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<text x="2.54" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-8.89" size="1.778" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="short" direction="in"/>
<pin name="ST" x="-7.62" y="-2.54" length="short" direction="in"/>
<pin name="GND" x="0" y="-7.62" visible="pad" length="short" direction="pwr" rot="R90"/>
<pin name="VBB" x="0" y="7.62" visible="off" length="short" direction="pwr" rot="R270"/>
<pin name="OUT" x="7.62" y="0" length="short" direction="out" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="BTS428L2" prefix="IC">
<description>&lt;b&gt;Smart High-Side Power Switch&lt;/b&gt;&lt;p&gt;
One Channel: 60mOhm Status Feedback&lt;br&gt;
Source: Source: www.infineon.com .. BTS428L2_20030912.pdf</description>
<gates>
<gate name="G$1" symbol="BTS428L2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO252-5-11">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="5"/>
<connect gate="G$1" pin="ST" pad="4"/>
<connect gate="G$1" pin="VBB" pad="TAB"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="qualified_1">
<packages>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
 chip</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.873" y1="0.483" x2="0.873" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="0.483" x2="0.873" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="-0.483" x2="-0.873" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.873" y1="-0.483" x2="-0.873" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-0.95" y1="0.5" x2="0.95" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.95" y1="0.5" x2="0.95" y2="-0.5" width="0.127" layer="21"/>
<wire x1="0.95" y1="-0.5" x2="-0.95" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-0.95" y1="-0.5" x2="-0.95" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.65" dy="0.65" layer="1"/>
<smd name="2" x="0.45" y="0" dx="0.65" dy="0.65" layer="1"/>
<text x="-1" y="0.6" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="-1.2" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
 chip</description>
<wire x1="-1.673" y1="0.883" x2="1.673" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.673" y1="-0.883" x2="-1.673" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.673" y1="-0.883" x2="-1.673" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.673" y1="0.883" x2="1.673" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.68" y1="0.9" x2="1.66" y2="0.9" width="0.08" layer="21"/>
<wire x1="1.66" y1="0.9" x2="1.66" y2="-0.91" width="0.08" layer="21"/>
<wire x1="1.66" y1="-0.91" x2="-1.69" y2="-0.91" width="0.08" layer="21"/>
<wire x1="-1.69" y1="-0.91" x2="-1.69" y2="0.89" width="0.08" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.6" y="1" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-1.6" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
 chip</description>
<wire x1="-2.373" y1="1.083" x2="2.373" y2="1.083" width="0.0508" layer="39"/>
<wire x1="2.373" y1="-1.083" x2="-2.373" y2="-1.083" width="0.0508" layer="39"/>
<wire x1="-2.373" y1="-1.083" x2="-2.373" y2="1.083" width="0.0508" layer="39"/>
<wire x1="2.373" y1="1.083" x2="2.373" y2="-1.083" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.3" y="1.2" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.3" y="-1.7" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="CBZ02">
<wire x1="-1.016" y1="14.986" x2="1.016" y2="14.986" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="0.255" x2="0" y2="0.255" width="0.254" layer="21"/>
<wire x1="0" y1="-7.92" x2="0" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="0" y1="0.255" x2="0" y2="9.16" width="0.1524" layer="21"/>
<wire x1="0" y1="0.255" x2="2.54" y2="0.255" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-0.745" x2="2.54" y2="-0.745" width="0.254" layer="21"/>
<wire x1="-9" y1="14" x2="9" y2="14" width="0.4064" layer="21"/>
<wire x1="9" y1="14" x2="9" y2="-14" width="0.4064" layer="21"/>
<wire x1="9" y1="-14" x2="-9" y2="-14" width="0.4064" layer="21"/>
<wire x1="-9" y1="-14" x2="-9" y2="14" width="0.4064" layer="21"/>
<smd name="1" x="0" y="17.05" dx="6" dy="4.5" layer="1"/>
<smd name="G@2" x="-6.5" y="-17.05" dx="3.5" dy="5.8" layer="1"/>
<smd name="G@3" x="6.5" y="-17.05" dx="3.5" dy="5.8" layer="1"/>
<text x="-9.16" y="15.7" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="3.84" y="15.7" size="1.27" layer="25" ratio="10">&gt;VALUE</text>
</package>
<package name="CBZ05">
<wire x1="-2.54" y1="0.255" x2="0" y2="0.255" width="0.254" layer="21"/>
<wire x1="0" y1="-4.92" x2="0" y2="-1.015" width="0.1524" layer="21"/>
<wire x1="0" y1="0.255" x2="0" y2="5.16" width="0.1524" layer="21"/>
<wire x1="0" y1="0.255" x2="2.54" y2="0.255" width="0.254" layer="21"/>
<wire x1="-2.54" y1="-0.745" x2="2.54" y2="-0.745" width="0.254" layer="21"/>
<wire x1="-7.5" y1="10" x2="7.5" y2="10" width="0.4064" layer="21"/>
<wire x1="7.5" y1="10" x2="7.5" y2="-9" width="0.4064" layer="21"/>
<wire x1="7.5" y1="-9" x2="-7.5" y2="-9" width="0.4064" layer="21"/>
<wire x1="-7.5" y1="-9" x2="-7.5" y2="10" width="0.4064" layer="21"/>
<smd name="1" x="0" y="12.5" dx="4.5" dy="3" layer="1"/>
<smd name="G@2" x="-5" y="-12.5" dx="2.9" dy="4.5" layer="1"/>
<smd name="G@3" x="5" y="-12.5" dx="2.9" dy="4.5" layer="1"/>
<text x="-7.66" y="11.2" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="2.84" y="11.2" size="1.27" layer="25" ratio="10">&gt;VALUE</text>
</package>
<package name="SOT-23-5">
<description>&lt;b&gt;Small Outline Transistor&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.8104" x2="1.4224" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.8104" x2="-1.4224" y2="-0.8104" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.8104" x2="-1.4224" y2="0.8104" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.8104" x2="1.4224" y2="0.8104" width="0.1524" layer="51"/>
<wire x1="-0.5224" y1="0.8104" x2="0.5224" y2="0.8104" width="0.1524" layer="21"/>
<wire x1="-0.4276" y1="-0.8104" x2="-0.5224" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="0.5224" y1="-0.8104" x2="0.4276" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="-1.3276" y1="-0.8104" x2="-1.4224" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="-0.8104" x2="1.3276" y2="-0.8104" width="0.1524" layer="21"/>
<wire x1="1.3276" y1="0.8104" x2="1.4224" y2="0.8104" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.8104" x2="-1.3276" y2="0.8104" width="0.1524" layer="21"/>
<smd name="1" x="-0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="2" x="0" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="3" x="0.95" y="-1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="4" x="0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<smd name="5" x="-0.95" y="1.3001" dx="0.55" dy="1.2" layer="1"/>
<text x="-1.6" y="-2" size="0.8128" layer="25" rot="R90">&gt;NAME</text>
<text x="2.4" y="-2" size="0.8128" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.2" y1="-1.5" x2="-0.7" y2="-0.85" layer="51"/>
<rectangle x1="-0.25" y1="-1.5" x2="0.25" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="-1.5" x2="1.2" y2="-0.85" layer="51"/>
<rectangle x1="0.7" y1="0.85" x2="1.2" y2="1.5" layer="51"/>
<rectangle x1="-1.2" y1="0.85" x2="-0.7" y2="1.5" layer="51"/>
</package>
<package name="C1210">
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="-2.4" y1="1.55" x2="2.4" y2="1.55" width="0.127" layer="21"/>
<wire x1="2.4" y1="1.55" x2="2.4" y2="-1.55" width="0.127" layer="21"/>
<wire x1="2.4" y1="-1.55" x2="-2.4" y2="-1.55" width="0.127" layer="21"/>
<wire x1="-2.4" y1="-1.55" x2="-2.4" y2="1.55" width="0.127" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.6" y="-1.3" size="0.6096" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="3.2" y="-1.5" size="0.6096" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
 chip</description>
<wire x1="-1.473" y1="0.583" x2="1.473" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.583" x2="1.473" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.583" x2="-1.473" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.583" x2="-1.473" y2="0.583" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.6" y="0.8" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="-1.4" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2" y1="-0.5" x2="0.2" y2="0.5" layer="41"/>
</package>
<package name="C0402R">
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-0.923" y1="0.483" x2="0.873" y2="0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="0.483" x2="0.873" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="0.873" y1="-0.483" x2="-0.923" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-0.923" y1="-0.483" x2="-0.923" y2="0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="0.5" x2="1" y2="0.5" width="0.127" layer="21"/>
<wire x1="1" y1="0.5" x2="1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="1" y1="-0.5" x2="-1" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-1" y1="-0.5" x2="-1" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.45" y="0" dx="0.65" dy="0.65" layer="1"/>
<smd name="2" x="0.45" y="0" dx="0.65" dy="0.65" layer="1"/>
<text x="-1" y="-1.2" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1" y="0.6" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805R">
<wire x1="-1.673" y1="0.883" x2="1.673" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.673" y1="-0.883" x2="-1.673" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.673" y1="-0.883" x2="-1.673" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.673" y1="0.883" x2="1.673" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.67" y1="0.9" x2="1.66" y2="0.9" width="0.08" layer="21"/>
<wire x1="1.66" y1="0.9" x2="1.66" y2="-0.91" width="0.08" layer="21"/>
<wire x1="1.66" y1="-0.91" x2="-1.69" y2="-0.91" width="0.08" layer="21"/>
<wire x1="-1.69" y1="-0.91" x2="-1.69" y2="0.87" width="0.08" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.689" y="-1.612" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.709" y="0.964" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603R">
<wire x1="-1.473" y1="0.583" x2="1.473" y2="0.583" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.583" x2="1.473" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.583" x2="-1.473" y2="-0.583" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.583" x2="-1.473" y2="0.583" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<wire x1="-1.6" y1="0.7" x2="1.6" y2="0.7" width="0.127" layer="21"/>
<wire x1="1.6" y1="0.7" x2="1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="1.6" y1="-0.7" x2="-1.6" y2="-0.7" width="0.127" layer="21"/>
<wire x1="-1.6" y1="-0.7" x2="-1.6" y2="0.7" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-1.6" y="-1.4" size="0.6096" layer="25" font="vector">&gt;NAME</text>
<text x="-1.6" y="0.8" size="0.6096" layer="27" font="vector">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<rectangle x1="-0.2" y1="-0.5" x2="0.2" y2="0.5" layer="41"/>
</package>
<package name="C1206R">
<wire x1="-2.373" y1="1.083" x2="2.373" y2="1.083" width="0.0508" layer="39"/>
<wire x1="2.373" y1="-1.083" x2="-2.373" y2="-1.083" width="0.0508" layer="39"/>
<wire x1="-2.373" y1="-1.083" x2="-2.373" y2="1.083" width="0.0508" layer="39"/>
<wire x1="2.373" y1="1.083" x2="2.373" y2="-1.083" width="0.0508" layer="39"/>
<wire x1="-2.365" y1="1.087" x2="2.315" y2="1.087" width="0.1016" layer="51"/>
<wire x1="-2.365" y1="-1.087" x2="2.315" y2="-1.087" width="0.1016" layer="51"/>
<wire x1="2.4" y1="1.05" x2="2.4" y2="-1.05" width="0.127" layer="21"/>
<wire x1="-2.4" y1="1.05" x2="-2.4" y2="-1.05" width="0.127" layer="21"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="3.2" y="-1.1" size="0.6096" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="-2.5" y="-1.1" size="0.6096" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C0201">
<wire x1="-0.673" y1="0.383" x2="0.673" y2="0.383" width="0.0508" layer="39"/>
<wire x1="0.673" y1="0.383" x2="0.673" y2="-0.383" width="0.0508" layer="39"/>
<wire x1="0.673" y1="-0.383" x2="-0.673" y2="-0.383" width="0.0508" layer="39"/>
<wire x1="-0.673" y1="-0.383" x2="-0.673" y2="0.383" width="0.0508" layer="39"/>
<wire x1="-0.65" y1="0.5" x2="0.65" y2="0.5" width="0.127" layer="21"/>
<wire x1="0.65" y1="0.5" x2="0.65" y2="-0.4" width="0.127" layer="21"/>
<wire x1="0.65" y1="-0.4" x2="-0.65" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-0.65" y1="-0.4" x2="-0.65" y2="0.5" width="0.127" layer="21"/>
<smd name="1" x="-0.265" y="0" dx="0.35" dy="0.45" layer="1"/>
<smd name="2" x="0.265" y="0" dx="0.35" dy="0.45" layer="1"/>
<text x="-0.6" y="0.4" size="0.4064" layer="25" font="vector" ratio="15">&gt;NAME</text>
<text x="-0.6" y="-1" size="0.4064" layer="27" font="vector" ratio="15">&gt;VALUE</text>
</package>
<package name="L4035M">
<description>&lt;b&gt;INDUCTOR&lt;/b&gt;&lt;p&gt;
 molded</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.083" y1="0.686" x2="-2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="2.083" y1="0.686" x2="2.083" y2="-0.686" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="1.524" x2="-1.981" y2="-1.524" width="0.1524" layer="51"/>
<wire x1="-1.981" y1="-1.524" x2="1.981" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="1.981" y1="-1.524" x2="1.981" y2="1.524" width="0.1524" layer="51"/>
<wire x1="1.981" y1="1.524" x2="-1.981" y2="1.524" width="0.1524" layer="21"/>
<smd name="1" x="-1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<smd name="2" x="1.6" y="0" dx="2.2" dy="1.4" layer="1"/>
<text x="-1.651" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.651" y="-3.048" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
 chip</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.673" y1="0.883" x2="1.773" y2="0.883" width="0.0508" layer="39"/>
<wire x1="1.773" y1="0.883" x2="1.773" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="1.773" y1="-0.883" x2="-1.673" y2="-0.883" width="0.0508" layer="39"/>
<wire x1="-1.673" y1="-0.883" x2="-1.673" y2="0.883" width="0.0508" layer="39"/>
<wire x1="-1.7" y1="0.9" x2="-1.7" y2="-0.9" width="0.127" layer="21"/>
<wire x1="-1.7" y1="-0.9" x2="1.8" y2="-0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="-0.9" x2="1.8" y2="0.9" width="0.127" layer="21"/>
<wire x1="1.8" y1="0.9" x2="-1.7" y2="0.9" width="0.127" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="1.2192" dy="1.27" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.2192" dy="1.27" layer="1"/>
<text x="-1.7" y="1" size="0.8128" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.8" y="-1.9" size="0.8128" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35" rot="R270"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 Reflow solder&lt;/b&gt;&lt;p&gt;
 Metric Code Size 1608</description>
<wire x1="-0.35" y1="-0.725" x2="-0.35" y2="0.725" width="0.1016" layer="51"/>
<wire x1="0.35" y1="0.715" x2="0.35" y2="-0.735" width="0.1016" layer="51"/>
<wire x1="-0.75" y1="1.65" x2="0.8" y2="1.65" width="0.1778" layer="21"/>
<wire x1="0.8" y1="1.65" x2="0.8" y2="-1.65" width="0.1778" layer="21"/>
<wire x1="0.8" y1="-1.65" x2="-0.75" y2="-1.65" width="0.1778" layer="21"/>
<wire x1="-0.75" y1="-1.65" x2="-0.75" y2="1.65" width="0.1778" layer="21"/>
<smd name="1" x="0" y="-0.875" dx="1.05" dy="1.08" layer="1" rot="R90"/>
<smd name="2" x="0" y="0.875" dx="1.05" dy="1.08" layer="1" rot="R90"/>
<text x="-1" y="-1.7" size="0.6096" layer="25" font="vector" rot="R90">&gt;NAME</text>
<text x="1.9" y="-1.7" size="0.6096" layer="27" font="vector" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.175" y1="-1.025" x2="0.175" y2="-0.225" layer="51" rot="R90"/>
<rectangle x1="-0.175" y1="0.225" x2="0.175" y2="1.025" layer="51" rot="R90"/>
<rectangle x1="-0.3957" y1="-0.5867" x2="0.4093" y2="0.5867" layer="43"/>
<rectangle x1="-0.54" y1="-0.25" x2="0.54" y2="0.25" layer="41"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-1.524" y1="0" x2="1.524" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-1.0161" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.0161" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="1.4732" y2="-1.6002" width="0.254" layer="94" curve="-37.882317" cap="flat"/>
<wire x1="-1.4508" y1="-1.5964" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373258" cap="flat"/>
<text x="0.856" y="1.175" size="1.016" layer="95">&gt;NAME</text>
<text x="0.936" y="-4.191" size="1.016" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="MIC5207">
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94"/>
<text x="-7.62" y="8.89" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="V_IN" x="-10.16" y="5.08" length="short" direction="in"/>
<pin name="EN" x="-10.16" y="-5.08" length="short" direction="in"/>
<pin name="VCC" x="10.16" y="2.54" length="short" direction="sup" rot="R180"/>
<pin name="ADJ" x="10.16" y="-5.08" length="short" direction="pas" rot="R180"/>
<pin name="GND" x="-10.16" y="0" length="short" direction="sup"/>
</symbol>
<symbol name="FB">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C" prefix="C" uservalue="yes">
<gates>
<gate name="G$1" symbol="C-US" x="-2.54" y="30.48"/>
</gates>
<devices>
<device name="1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-R" package="C0402R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805-R" package="C0805R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-R" package="C0603R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206-R" package="C1206R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BZ02" package="CBZ02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="G@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="BZ05" package="CBZ05">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="G@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MIC5219" uservalue="yes">
<gates>
<gate name="G$1" symbol="MIC5207" x="-5.08" y="43.18"/>
</gates>
<devices>
<device name="BM5" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="ADJ" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="V_IN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="YM5" package="SOT-23-5">
<connects>
<connect gate="G$1" pin="ADJ" pad="4"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VCC" pad="5"/>
<connect gate="G$1" pin="V_IN" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FB" prefix="FB" uservalue="yes">
<gates>
<gate name="G$1" symbol="FB" x="-15.24" y="38.1"/>
</gates>
<devices>
<device name="4035" package="L4035M">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0603-R" package="C0603R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0402-R" package="C0402R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<text x="-1.905" y="-3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="linear-technology">
<description>&lt;b&gt;Linear Technology Devices&lt;/b&gt;&lt;p&gt;
http://www.linear-tech.com&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="DFN14-4X3DE">
<description>&lt;b&gt;14-LEAD (4mm x 2mm) PALSTIC DFN&lt;/b&gt; DE14MA PACKAGE&lt;p&gt;
Source: http://www.linear.com/pc/downloadDocument.do?navId=H0,C1,C1003,C1042,C1032,C1064,P38040,D25144</description>
<wire x1="-1.9" y1="1.4" x2="1.9" y2="1.4" width="0.2032" layer="51"/>
<wire x1="1.9" y1="1.4" x2="1.9" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="1.9" y1="-1.4" x2="-1.9" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-1.9" y1="-1.4" x2="-1.9" y2="1.4" width="0.2032" layer="21"/>
<circle x="-1.45" y="-0.9125" radius="0.1754" width="0" layer="51"/>
<smd name="1" x="-1.5" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="2" x="-1" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="3" x="-0.5" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="4" x="0" y="-0.75" dx="0.25" dy="2" layer="1" stop="no" cream="no"/>
<smd name="5" x="0.5" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="6" x="1" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="7" x="1.5" y="-1.4" dx="0.25" dy="0.7" layer="1" stop="no" cream="no"/>
<smd name="8" x="1.5" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="9" x="1" y="0.75" dx="0.25" dy="2" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="10" x="0.5" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="11" x="0" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="12" x="-0.5" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="13" x="-1" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="14" x="-1.5" y="1.4" dx="0.25" dy="0.7" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="4@1" x="-0.75" y="0" dx="1.75" dy="1.65" layer="1" roundness="25" stop="no" cream="no"/>
<smd name="9@2" x="1" y="0" dx="1.07" dy="1.65" layer="1" roundness="25" stop="no" cream="no"/>
<text x="-2" y="2" size="1.27" layer="25">&gt;NAME</text>
<text x="-2" y="-3.25" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.65" y1="-1.775" x2="-1.35" y2="-1.025" layer="29"/>
<rectangle x1="-1.625" y1="-0.825" x2="0.125" y2="0.825" layer="29"/>
<rectangle x1="0.45" y1="-0.825" x2="1.55" y2="0.825" layer="29"/>
<rectangle x1="-1.55" y1="-0.75" x2="0.05" y2="0.75" layer="31"/>
<rectangle x1="0.525" y1="-0.75" x2="1.475" y2="0.75" layer="31"/>
<rectangle x1="-1.6" y1="-1.725" x2="-1.4" y2="-1.075" layer="31"/>
<rectangle x1="-1.1" y1="-1.725" x2="-0.9" y2="-1.075" layer="31"/>
<rectangle x1="-0.6" y1="-1.725" x2="-0.4" y2="-1.075" layer="31"/>
<rectangle x1="-0.1" y1="-1.725" x2="0.1" y2="-1.075" layer="31"/>
<rectangle x1="0.4" y1="-1.725" x2="0.6" y2="-1.075" layer="31"/>
<rectangle x1="0.9" y1="-1.725" x2="1.1" y2="-1.075" layer="31"/>
<rectangle x1="1.4" y1="-1.725" x2="1.6" y2="-1.075" layer="31"/>
<rectangle x1="1.4" y1="1.075" x2="1.6" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="0.9" y1="1.075" x2="1.1" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="0.4" y1="1.075" x2="0.6" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-0.1" y1="1.075" x2="0.1" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-0.6" y1="1.075" x2="-0.4" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-1.1" y1="1.075" x2="-0.9" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-1.6" y1="1.075" x2="-1.4" y2="1.725" layer="31" rot="R180"/>
<rectangle x1="-1.15" y1="-1.775" x2="-0.85" y2="-1.025" layer="29"/>
<rectangle x1="-0.65" y1="-1.775" x2="-0.35" y2="-1.025" layer="29"/>
<rectangle x1="-0.15" y1="-1.775" x2="0.15" y2="-1.025" layer="29"/>
<rectangle x1="0.35" y1="-1.775" x2="0.65" y2="-1.025" layer="29"/>
<rectangle x1="0.85" y1="-1.775" x2="1.15" y2="-1.025" layer="29"/>
<rectangle x1="1.35" y1="-1.775" x2="1.65" y2="-1.025" layer="29"/>
<rectangle x1="1.35" y1="1.025" x2="1.65" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="0.85" y1="1.025" x2="1.15" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="0.35" y1="1.025" x2="0.65" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="-0.15" y1="1.025" x2="0.15" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="-0.65" y1="1.025" x2="-0.35" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="-1.15" y1="1.025" x2="-0.85" y2="1.775" layer="29" rot="R180"/>
<rectangle x1="-1.65" y1="1.025" x2="-1.35" y2="1.775" layer="29" rot="R180"/>
</package>
</packages>
<symbols>
<symbol name="LT3681">
<wire x1="-12.7" y1="15.24" x2="12.7" y2="15.24" width="0.254" layer="94"/>
<wire x1="12.7" y1="15.24" x2="12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="12.7" y1="-20.32" x2="-12.7" y2="-20.32" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-20.32" x2="-12.7" y2="15.24" width="0.254" layer="94"/>
<text x="-12.7" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="16.51" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PG" x="-15.24" y="-10.16" length="short" direction="oc"/>
<pin name="BIAS" x="15.24" y="-12.7" length="short" direction="in" rot="R180"/>
<pin name="FB" x="15.24" y="-17.78" length="short" direction="in" rot="R180"/>
<pin name="GND" x="-2.54" y="-22.86" length="short" direction="in" rot="R90"/>
<pin name="VC" x="-15.24" y="0" length="short" direction="out"/>
<pin name="RT" x="-15.24" y="-5.08" length="short" direction="in"/>
<pin name="GND@1" x="0" y="-22.86" length="short" direction="in" rot="R90"/>
<pin name="DA" x="15.24" y="-7.62" length="short" direction="pas" rot="R180"/>
<pin name="DC" x="15.24" y="0" length="short" direction="pas" rot="R180"/>
<pin name="BD" x="15.24" y="12.7" length="short" direction="in" rot="R180"/>
<pin name="BOOST" x="15.24" y="7.62" length="short" direction="out" rot="R180"/>
<pin name="SW" x="15.24" y="2.54" length="short" direction="out" rot="R180"/>
<pin name="VIN" x="-15.24" y="12.7" length="short" direction="in"/>
<pin name="RUN/SS" x="-15.24" y="5.08" length="short" direction="in"/>
<pin name="GND@2" x="2.54" y="-22.86" length="short" direction="in" rot="R90"/>
<pin name="DC@1" x="15.24" y="-2.54" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LT3681" prefix="IC">
<description>36V, 2A, 2.8MHz &lt;b&gt;Step-Down Switching Regulator&lt;/b&gt; with Integrated Power Schottky Diode&lt;p&gt;
Source: http://www.linear.com/pc/downloadDocument.do?navId=H0,C1,C1003,C1042,C1032,C1064,P38040,D25144</description>
<gates>
<gate name="G$1" symbol="LT3681" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DFN14-4X3DE">
<connects>
<connect gate="G$1" pin="BD" pad="10"/>
<connect gate="G$1" pin="BIAS" pad="2"/>
<connect gate="G$1" pin="BOOST" pad="11"/>
<connect gate="G$1" pin="DA" pad="8"/>
<connect gate="G$1" pin="DC" pad="9"/>
<connect gate="G$1" pin="DC@1" pad="9@2"/>
<connect gate="G$1" pin="FB" pad="3"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="GND@1" pad="4@1"/>
<connect gate="G$1" pin="GND@2" pad="7"/>
<connect gate="G$1" pin="PG" pad="1"/>
<connect gate="G$1" pin="RT" pad="6"/>
<connect gate="G$1" pin="RUN/SS" pad="14"/>
<connect gate="G$1" pin="SW" pad="12"/>
<connect gate="G$1" pin="VC" pad="5"/>
<connect gate="G$1" pin="VIN" pad="13"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="LT3681EDE#PBF" constant="no"/>
<attribute name="OC_FARNELL" value="1627603" constant="no"/>
<attribute name="OC_NEWARK" value="57M8550" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="inductors">
<description>&lt;b&gt;Inductors and Filters&lt;/b&gt;&lt;p&gt;
Based on the previous library ind-a.lbr&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SLF7045">
<description>&lt;b&gt;SMD Inductors(Coils)&lt;/b&gt; For Power Line (Wound, Magnetic Shielded)&lt;p&gt;
Source: TDK, e531_slf7045.pdf</description>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="2.35" width="0.2032" layer="51"/>
<wire x1="-2.35" y1="3.4" x2="3.4" y2="3.4" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="-3.4" width="0.2032" layer="51"/>
<wire x1="3.4" y1="-3.4" x2="-3.4" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="3.4" x2="-2.35" y2="2.35" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="2.35" x2="-3.4" y2="2.35" width="0.2032" layer="21"/>
<wire x1="0" y1="-3.125" x2="-2.775" y2="-1.425" width="0.2032" layer="21" curve="-62.757204"/>
<wire x1="0" y1="-3.125" x2="2.825" y2="-1.35" width="0.2032" layer="21" curve="64.5272"/>
<wire x1="0" y1="3.125" x2="-2.775" y2="1.425" width="0.2032" layer="21" curve="62.757204"/>
<wire x1="0" y1="3.125" x2="2.775" y2="1.425" width="0.2032" layer="21" curve="-62.757204"/>
<wire x1="-3.4" y1="-3.4" x2="-3.4" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="3.4" y1="-1.6" x2="3.4" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="3.4" y1="3.4" x2="3.4" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-3.4" y1="1.6" x2="-3.4" y2="2.35" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.125" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="2" width="0.2032" layer="21"/>
<smd name="1" x="-3.2" y="0" dx="2.2" dy="1.5" layer="1" rot="R90"/>
<smd name="2" x="3.2" y="0" dx="2.2" dy="1.5" layer="1" rot="R90"/>
<text x="-3.5" y="4" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.5" y="-5" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="DRK">
<wire x1="-3.81" y1="1.651" x2="3.81" y2="1.651" width="0.254" layer="94"/>
<text x="-3.81" y="2.286" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.937" y="-3.048" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-0.889" x2="3.81" y2="0.889" layer="94"/>
<pin name="2" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SLF7045" prefix="L">
<description>&lt;b&gt;SMD Inductors(Coils)&lt;/b&gt; For Power Line (Wound, Magnetic Shielded)&lt;p&gt;
Source: TDK, e531_slf7045.pdf</description>
<gates>
<gate name="G$1" symbol="DRK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SLF7045">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Atmel_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 27/07/2012 14:02:49</description>
<packages>
<package name="QFP50P2200X2200X160-144N">
<smd name="1" x="-10.668" y="8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-10.668" y="8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-10.668" y="7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-10.668" y="7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-10.668" y="6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-10.668" y="6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-10.668" y="5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-10.668" y="5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-10.668" y="4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-10.668" y="4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-10.668" y="3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-10.668" y="3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-10.668" y="2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="14" x="-10.668" y="2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="15" x="-10.668" y="1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="16" x="-10.668" y="1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="17" x="-10.668" y="0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="18" x="-10.668" y="0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="19" x="-10.668" y="-0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="20" x="-10.668" y="-0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="21" x="-10.668" y="-1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="22" x="-10.668" y="-1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="23" x="-10.668" y="-2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="24" x="-10.668" y="-2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="25" x="-10.668" y="-3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="-10.668" y="-3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="27" x="-10.668" y="-4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="28" x="-10.668" y="-4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="29" x="-10.668" y="-5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="30" x="-10.668" y="-5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="31" x="-10.668" y="-6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="32" x="-10.668" y="-6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="33" x="-10.668" y="-7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="-10.668" y="-7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="-10.668" y="-8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="-10.668" y="-8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="-8.7376" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="-8.255" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="-7.747" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="-7.239" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="-6.7564" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="-6.2484" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="-5.7404" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="-5.2578" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="-4.7498" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="-4.2418" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="-3.7592" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="-3.2512" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="-2.7432" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="50" x="-2.2606" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="51" x="-1.7526" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="52" x="-1.2446" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="53" x="-0.762" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="54" x="-0.254" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="55" x="0.254" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="56" x="0.762" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="57" x="1.2446" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="58" x="1.7526" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="59" x="2.2606" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="60" x="2.7432" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="61" x="3.2512" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="62" x="3.7592" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="63" x="4.2418" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="64" x="4.7498" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="65" x="5.2578" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="66" x="5.7404" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="67" x="6.2484" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="68" x="6.7564" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="69" x="7.239" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="70" x="7.747" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="71" x="8.255" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="72" x="8.7376" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="73" x="10.668" y="-8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="74" x="10.668" y="-8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="75" x="10.668" y="-7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="76" x="10.668" y="-7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="77" x="10.668" y="-6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="78" x="10.668" y="-6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="79" x="10.668" y="-5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="80" x="10.668" y="-5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="81" x="10.668" y="-4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="82" x="10.668" y="-4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="83" x="10.668" y="-3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="84" x="10.668" y="-3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="85" x="10.668" y="-2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="86" x="10.668" y="-2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="87" x="10.668" y="-1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="88" x="10.668" y="-1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="89" x="10.668" y="-0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="90" x="10.668" y="-0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="91" x="10.668" y="0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="92" x="10.668" y="0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="93" x="10.668" y="1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="94" x="10.668" y="1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="95" x="10.668" y="2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="96" x="10.668" y="2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="97" x="10.668" y="3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="98" x="10.668" y="3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="99" x="10.668" y="4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="100" x="10.668" y="4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="101" x="10.668" y="5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="102" x="10.668" y="5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="103" x="10.668" y="6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="104" x="10.668" y="6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="105" x="10.668" y="7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="106" x="10.668" y="7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="107" x="10.668" y="8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="108" x="10.668" y="8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="109" x="8.7376" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="110" x="8.255" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="111" x="7.747" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="112" x="7.239" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="113" x="6.7564" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="114" x="6.2484" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="115" x="5.7404" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="116" x="5.2578" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="117" x="4.7498" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="118" x="4.2418" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="119" x="3.7592" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="120" x="3.2512" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="121" x="2.7432" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="122" x="2.2606" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="123" x="1.7526" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="124" x="1.2446" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="125" x="0.762" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="126" x="0.254" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="127" x="-0.254" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="128" x="-0.762" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="129" x="-1.2446" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="130" x="-1.7526" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="131" x="-2.2606" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="132" x="-2.7432" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="133" x="-3.2512" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="134" x="-3.7592" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="135" x="-4.2418" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="136" x="-4.7498" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="137" x="-5.2578" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="138" x="-5.7404" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="139" x="-6.2484" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="140" x="-6.7564" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="141" x="-7.239" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="142" x="-7.747" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="143" x="-8.255" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="144" x="-8.7376" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<wire x1="-11.7348" y1="-5.7658" x2="-12.7508" y2="-5.7658" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="-5.2578" x2="12.7508" y2="-5.2578" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="-0.2032" x2="12.7508" y2="-0.2032" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="4.8006" x2="12.7508" y2="4.8006" width="0.1524" layer="21"/>
<wire x1="8.255" y1="11.7348" x2="8.255" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="3.2512" y1="11.7348" x2="3.2512" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="11.7602" x2="-1.778" y2="12.7762" width="0.1524" layer="21"/>
<wire x1="-6.7564" y1="11.7348" x2="-6.7564" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="-11.7348" y1="4.2672" x2="-12.7508" y2="4.2672" width="0.1524" layer="21"/>
<wire x1="-11.7094" y1="-0.762" x2="-12.7254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="7.7724" y1="-11.7348" x2="7.7724" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="2.7686" y1="-11.7348" x2="2.7686" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-11.7348" x2="-2.2352" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-7.2898" y1="-11.7348" x2="-7.2898" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-9.1948" y1="10.0584" x2="-10.0584" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="9.1948" x2="10.0584" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="9.1948" y1="-10.0584" x2="10.0584" y2="-10.0584" width="0.1524" layer="21"/>
<wire x1="-9.7282" y1="9.1186" x2="-9.1186" y2="9.7282" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="-10.0584" x2="-9.1948" y2="-10.0584" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="-10.0584" x2="10.0584" y2="-9.1948" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="10.0584" x2="9.1948" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="10.0584" x2="-10.0584" y2="9.1948" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="-9.1948" x2="-10.0584" y2="-10.0584" width="0.1524" layer="21"/>
<text x="-12.6238" y="8.7376" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="8.636" y1="10.0584" x2="8.8646" y2="10.0584" width="0" layer="51"/>
<wire x1="8.8646" y1="10.0584" x2="8.8646" y2="11.049" width="0" layer="51"/>
<wire x1="8.8646" y1="11.049" x2="8.636" y2="11.049" width="0" layer="51"/>
<wire x1="8.636" y1="11.049" x2="8.636" y2="10.0584" width="0" layer="51"/>
<wire x1="8.128" y1="10.0584" x2="8.3566" y2="10.0584" width="0" layer="51"/>
<wire x1="8.3566" y1="10.0584" x2="8.3566" y2="11.049" width="0" layer="51"/>
<wire x1="8.3566" y1="11.049" x2="8.128" y2="11.049" width="0" layer="51"/>
<wire x1="8.128" y1="11.049" x2="8.128" y2="10.0584" width="0" layer="51"/>
<wire x1="7.6454" y1="10.0584" x2="7.8486" y2="10.0584" width="0" layer="51"/>
<wire x1="7.8486" y1="10.0584" x2="7.8486" y2="11.049" width="0" layer="51"/>
<wire x1="7.8486" y1="11.049" x2="7.6454" y2="11.049" width="0" layer="51"/>
<wire x1="7.6454" y1="11.049" x2="7.6454" y2="10.0584" width="0" layer="51"/>
<wire x1="7.1374" y1="10.0584" x2="7.366" y2="10.0584" width="0" layer="51"/>
<wire x1="7.366" y1="10.0584" x2="7.366" y2="11.049" width="0" layer="51"/>
<wire x1="7.366" y1="11.049" x2="7.1374" y2="11.049" width="0" layer="51"/>
<wire x1="7.1374" y1="11.049" x2="7.1374" y2="10.0584" width="0" layer="51"/>
<wire x1="6.6294" y1="10.0584" x2="6.858" y2="10.0584" width="0" layer="51"/>
<wire x1="6.858" y1="10.0584" x2="6.858" y2="11.049" width="0" layer="51"/>
<wire x1="6.858" y1="11.049" x2="6.6294" y2="11.049" width="0" layer="51"/>
<wire x1="6.6294" y1="11.049" x2="6.6294" y2="10.0584" width="0" layer="51"/>
<wire x1="6.1468" y1="10.0584" x2="6.35" y2="10.0584" width="0" layer="51"/>
<wire x1="6.35" y1="10.0584" x2="6.35" y2="11.049" width="0" layer="51"/>
<wire x1="6.35" y1="11.049" x2="6.1468" y2="11.049" width="0" layer="51"/>
<wire x1="6.1468" y1="11.049" x2="6.1468" y2="10.0584" width="0" layer="51"/>
<wire x1="5.6388" y1="10.0584" x2="5.8674" y2="10.0584" width="0" layer="51"/>
<wire x1="5.8674" y1="10.0584" x2="5.8674" y2="11.049" width="0" layer="51"/>
<wire x1="5.8674" y1="11.049" x2="5.6388" y2="11.049" width="0" layer="51"/>
<wire x1="5.6388" y1="11.049" x2="5.6388" y2="10.0584" width="0" layer="51"/>
<wire x1="5.1308" y1="10.0584" x2="5.3594" y2="10.0584" width="0" layer="51"/>
<wire x1="5.3594" y1="10.0584" x2="5.3594" y2="11.049" width="0" layer="51"/>
<wire x1="5.3594" y1="11.049" x2="5.1308" y2="11.049" width="0" layer="51"/>
<wire x1="5.1308" y1="11.049" x2="5.1308" y2="10.0584" width="0" layer="51"/>
<wire x1="4.6482" y1="10.0584" x2="4.8514" y2="10.0584" width="0" layer="51"/>
<wire x1="4.8514" y1="10.0584" x2="4.8514" y2="11.049" width="0" layer="51"/>
<wire x1="4.8514" y1="11.049" x2="4.6482" y2="11.049" width="0" layer="51"/>
<wire x1="4.6482" y1="11.049" x2="4.6482" y2="10.0584" width="0" layer="51"/>
<wire x1="4.1402" y1="10.0584" x2="4.3688" y2="10.0584" width="0" layer="51"/>
<wire x1="4.3688" y1="10.0584" x2="4.3688" y2="11.049" width="0" layer="51"/>
<wire x1="4.3688" y1="11.049" x2="4.1402" y2="11.049" width="0" layer="51"/>
<wire x1="4.1402" y1="11.049" x2="4.1402" y2="10.0584" width="0" layer="51"/>
<wire x1="3.6322" y1="10.0584" x2="3.8608" y2="10.0584" width="0" layer="51"/>
<wire x1="3.8608" y1="10.0584" x2="3.8608" y2="11.049" width="0" layer="51"/>
<wire x1="3.8608" y1="11.049" x2="3.6322" y2="11.049" width="0" layer="51"/>
<wire x1="3.6322" y1="11.049" x2="3.6322" y2="10.0584" width="0" layer="51"/>
<wire x1="3.1496" y1="10.0584" x2="3.3528" y2="10.0584" width="0" layer="51"/>
<wire x1="3.3528" y1="10.0584" x2="3.3528" y2="11.049" width="0" layer="51"/>
<wire x1="3.3528" y1="11.049" x2="3.1496" y2="11.049" width="0" layer="51"/>
<wire x1="3.1496" y1="11.049" x2="3.1496" y2="10.0584" width="0" layer="51"/>
<wire x1="2.6416" y1="10.0584" x2="2.8702" y2="10.0584" width="0" layer="51"/>
<wire x1="2.8702" y1="10.0584" x2="2.8702" y2="11.049" width="0" layer="51"/>
<wire x1="2.8702" y1="11.049" x2="2.6416" y2="11.049" width="0" layer="51"/>
<wire x1="2.6416" y1="11.049" x2="2.6416" y2="10.0584" width="0" layer="51"/>
<wire x1="2.1336" y1="10.0584" x2="2.3622" y2="10.0584" width="0" layer="51"/>
<wire x1="2.3622" y1="10.0584" x2="2.3622" y2="11.049" width="0" layer="51"/>
<wire x1="2.3622" y1="11.049" x2="2.1336" y2="11.049" width="0" layer="51"/>
<wire x1="2.1336" y1="11.049" x2="2.1336" y2="10.0584" width="0" layer="51"/>
<wire x1="1.651" y1="10.0584" x2="1.8542" y2="10.0584" width="0" layer="51"/>
<wire x1="1.8542" y1="10.0584" x2="1.8542" y2="11.049" width="0" layer="51"/>
<wire x1="1.8542" y1="11.049" x2="1.651" y2="11.049" width="0" layer="51"/>
<wire x1="1.651" y1="11.049" x2="1.651" y2="10.0584" width="0" layer="51"/>
<wire x1="1.143" y1="10.0584" x2="1.3716" y2="10.0584" width="0" layer="51"/>
<wire x1="1.3716" y1="10.0584" x2="1.3716" y2="11.049" width="0" layer="51"/>
<wire x1="1.3716" y1="11.049" x2="1.143" y2="11.049" width="0" layer="51"/>
<wire x1="1.143" y1="11.049" x2="1.143" y2="10.0584" width="0" layer="51"/>
<wire x1="0.635" y1="10.0584" x2="0.8636" y2="10.0584" width="0" layer="51"/>
<wire x1="0.8636" y1="10.0584" x2="0.8636" y2="11.049" width="0" layer="51"/>
<wire x1="0.8636" y1="11.049" x2="0.635" y2="11.049" width="0" layer="51"/>
<wire x1="0.635" y1="11.049" x2="0.635" y2="10.0584" width="0" layer="51"/>
<wire x1="0.1524" y1="10.0584" x2="0.3556" y2="10.0584" width="0" layer="51"/>
<wire x1="0.3556" y1="10.0584" x2="0.3556" y2="11.049" width="0" layer="51"/>
<wire x1="0.3556" y1="11.049" x2="0.1524" y2="11.049" width="0" layer="51"/>
<wire x1="0.1524" y1="11.049" x2="0.1524" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.3556" y1="10.0584" x2="-0.1524" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.1524" y1="10.0584" x2="-0.1524" y2="11.049" width="0" layer="51"/>
<wire x1="-0.1524" y1="11.049" x2="-0.3556" y2="11.049" width="0" layer="51"/>
<wire x1="-0.3556" y1="11.049" x2="-0.3556" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.8636" y1="10.0584" x2="-0.635" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.635" y1="10.0584" x2="-0.635" y2="11.049" width="0" layer="51"/>
<wire x1="-0.635" y1="11.049" x2="-0.8636" y2="11.049" width="0" layer="51"/>
<wire x1="-0.8636" y1="11.049" x2="-0.8636" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.3716" y1="10.0584" x2="-1.143" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.143" y1="10.0584" x2="-1.143" y2="11.049" width="0" layer="51"/>
<wire x1="-1.143" y1="11.049" x2="-1.3716" y2="11.049" width="0" layer="51"/>
<wire x1="-1.3716" y1="11.049" x2="-1.3716" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.8542" y1="10.0584" x2="-1.651" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.651" y1="10.0584" x2="-1.651" y2="11.049" width="0" layer="51"/>
<wire x1="-1.651" y1="11.049" x2="-1.8542" y2="11.049" width="0" layer="51"/>
<wire x1="-1.8542" y1="11.049" x2="-1.8542" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.3622" y1="10.0584" x2="-2.1336" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.1336" y1="10.0584" x2="-2.1336" y2="11.049" width="0" layer="51"/>
<wire x1="-2.1336" y1="11.049" x2="-2.3622" y2="11.049" width="0" layer="51"/>
<wire x1="-2.3622" y1="11.049" x2="-2.3622" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.8702" y1="10.0584" x2="-2.6416" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.6416" y1="10.0584" x2="-2.6416" y2="11.049" width="0" layer="51"/>
<wire x1="-2.6416" y1="11.049" x2="-2.8702" y2="11.049" width="0" layer="51"/>
<wire x1="-2.8702" y1="11.049" x2="-2.8702" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.3528" y1="10.0584" x2="-3.1496" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.1496" y1="10.0584" x2="-3.1496" y2="11.049" width="0" layer="51"/>
<wire x1="-3.1496" y1="11.049" x2="-3.3528" y2="11.049" width="0" layer="51"/>
<wire x1="-3.3528" y1="11.049" x2="-3.3528" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.8608" y1="10.0584" x2="-3.6322" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.6322" y1="10.0584" x2="-3.6322" y2="11.049" width="0" layer="51"/>
<wire x1="-3.6322" y1="11.049" x2="-3.8608" y2="11.049" width="0" layer="51"/>
<wire x1="-3.8608" y1="11.049" x2="-3.8608" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.3688" y1="10.0584" x2="-4.1402" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.1402" y1="10.0584" x2="-4.1402" y2="11.049" width="0" layer="51"/>
<wire x1="-4.1402" y1="11.049" x2="-4.3688" y2="11.049" width="0" layer="51"/>
<wire x1="-4.3688" y1="11.049" x2="-4.3688" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.8514" y1="10.0584" x2="-4.6482" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.6482" y1="10.0584" x2="-4.6482" y2="11.049" width="0" layer="51"/>
<wire x1="-4.6482" y1="11.049" x2="-4.8514" y2="11.049" width="0" layer="51"/>
<wire x1="-4.8514" y1="11.049" x2="-4.8514" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.3594" y1="10.0584" x2="-5.1308" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.1308" y1="10.0584" x2="-5.1308" y2="11.049" width="0" layer="51"/>
<wire x1="-5.1308" y1="11.049" x2="-5.3594" y2="11.049" width="0" layer="51"/>
<wire x1="-5.3594" y1="11.049" x2="-5.3594" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.8674" y1="10.0584" x2="-5.6388" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.6388" y1="10.0584" x2="-5.6388" y2="11.049" width="0" layer="51"/>
<wire x1="-5.6388" y1="11.049" x2="-5.8674" y2="11.049" width="0" layer="51"/>
<wire x1="-5.8674" y1="11.049" x2="-5.8674" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.35" y1="10.0584" x2="-6.1468" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.1468" y1="10.0584" x2="-6.1468" y2="11.049" width="0" layer="51"/>
<wire x1="-6.1468" y1="11.049" x2="-6.35" y2="11.049" width="0" layer="51"/>
<wire x1="-6.35" y1="11.049" x2="-6.35" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.858" y1="10.0584" x2="-6.6294" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.6294" y1="10.0584" x2="-6.6294" y2="11.049" width="0" layer="51"/>
<wire x1="-6.6294" y1="11.049" x2="-6.858" y2="11.049" width="0" layer="51"/>
<wire x1="-6.858" y1="11.049" x2="-6.858" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.366" y1="10.0584" x2="-7.1374" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.1374" y1="10.0584" x2="-7.1374" y2="11.049" width="0" layer="51"/>
<wire x1="-7.1374" y1="11.049" x2="-7.366" y2="11.049" width="0" layer="51"/>
<wire x1="-7.366" y1="11.049" x2="-7.366" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.8486" y1="10.0584" x2="-7.6454" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.6454" y1="10.0584" x2="-7.6454" y2="11.049" width="0" layer="51"/>
<wire x1="-7.6454" y1="11.049" x2="-7.8486" y2="11.049" width="0" layer="51"/>
<wire x1="-7.8486" y1="11.049" x2="-7.8486" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.3566" y1="10.0584" x2="-8.128" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.128" y1="10.0584" x2="-8.128" y2="11.049" width="0" layer="51"/>
<wire x1="-8.128" y1="11.049" x2="-8.3566" y2="11.049" width="0" layer="51"/>
<wire x1="-8.3566" y1="11.049" x2="-8.3566" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.8646" y1="10.0584" x2="-8.7884" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.7884" y1="10.0584" x2="-8.636" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.636" y1="10.0584" x2="-8.636" y2="11.049" width="0" layer="51"/>
<wire x1="-8.636" y1="11.049" x2="-8.8646" y2="11.049" width="0" layer="51"/>
<wire x1="-8.8646" y1="11.049" x2="-8.8646" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.636" x2="-10.0584" y2="8.7884" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.7884" x2="-10.0584" y2="8.8646" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.8646" x2="-11.049" y2="8.8646" width="0" layer="51"/>
<wire x1="-11.049" y1="8.8646" x2="-11.049" y2="8.636" width="0" layer="51"/>
<wire x1="-11.049" y1="8.636" x2="-10.0584" y2="8.636" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.128" x2="-10.0584" y2="8.3566" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.3566" x2="-11.049" y2="8.3566" width="0" layer="51"/>
<wire x1="-11.049" y1="8.3566" x2="-11.049" y2="8.128" width="0" layer="51"/>
<wire x1="-11.049" y1="8.128" x2="-10.0584" y2="8.128" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.6454" x2="-10.0584" y2="7.8486" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.8486" x2="-11.049" y2="7.8486" width="0" layer="51"/>
<wire x1="-11.049" y1="7.8486" x2="-11.049" y2="7.6454" width="0" layer="51"/>
<wire x1="-11.049" y1="7.6454" x2="-10.0584" y2="7.6454" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.1374" x2="-10.0584" y2="7.366" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.366" x2="-11.049" y2="7.366" width="0" layer="51"/>
<wire x1="-11.049" y1="7.366" x2="-11.049" y2="7.1374" width="0" layer="51"/>
<wire x1="-11.049" y1="7.1374" x2="-10.0584" y2="7.1374" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.6294" x2="-10.0584" y2="6.858" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.858" x2="-11.049" y2="6.858" width="0" layer="51"/>
<wire x1="-11.049" y1="6.858" x2="-11.049" y2="6.6294" width="0" layer="51"/>
<wire x1="-11.049" y1="6.6294" x2="-10.0584" y2="6.6294" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.1468" x2="-10.0584" y2="6.35" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.35" x2="-11.049" y2="6.35" width="0" layer="51"/>
<wire x1="-11.049" y1="6.35" x2="-11.049" y2="6.1468" width="0" layer="51"/>
<wire x1="-11.049" y1="6.1468" x2="-10.0584" y2="6.1468" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.6388" x2="-10.0584" y2="5.8674" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.8674" x2="-11.049" y2="5.8674" width="0" layer="51"/>
<wire x1="-11.049" y1="5.8674" x2="-11.049" y2="5.6388" width="0" layer="51"/>
<wire x1="-11.049" y1="5.6388" x2="-10.0584" y2="5.6388" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.1308" x2="-10.0584" y2="5.3594" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.3594" x2="-11.049" y2="5.3594" width="0" layer="51"/>
<wire x1="-11.049" y1="5.3594" x2="-11.049" y2="5.1308" width="0" layer="51"/>
<wire x1="-11.049" y1="5.1308" x2="-10.0584" y2="5.1308" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.6482" x2="-10.0584" y2="4.8514" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.8514" x2="-11.049" y2="4.8514" width="0" layer="51"/>
<wire x1="-11.049" y1="4.8514" x2="-11.049" y2="4.6482" width="0" layer="51"/>
<wire x1="-11.049" y1="4.6482" x2="-10.0584" y2="4.6482" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.1402" x2="-10.0584" y2="4.3688" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.3688" x2="-11.049" y2="4.3688" width="0" layer="51"/>
<wire x1="-11.049" y1="4.3688" x2="-11.049" y2="4.1402" width="0" layer="51"/>
<wire x1="-11.049" y1="4.1402" x2="-10.0584" y2="4.1402" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.6322" x2="-10.0584" y2="3.8608" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.8608" x2="-11.049" y2="3.8608" width="0" layer="51"/>
<wire x1="-11.049" y1="3.8608" x2="-11.049" y2="3.6322" width="0" layer="51"/>
<wire x1="-11.049" y1="3.6322" x2="-10.0584" y2="3.6322" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.1496" x2="-10.0584" y2="3.3528" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.3528" x2="-11.049" y2="3.3528" width="0" layer="51"/>
<wire x1="-11.049" y1="3.3528" x2="-11.049" y2="3.1496" width="0" layer="51"/>
<wire x1="-11.049" y1="3.1496" x2="-10.0584" y2="3.1496" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.6416" x2="-10.0584" y2="2.8702" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.8702" x2="-11.049" y2="2.8702" width="0" layer="51"/>
<wire x1="-11.049" y1="2.8702" x2="-11.049" y2="2.6416" width="0" layer="51"/>
<wire x1="-11.049" y1="2.6416" x2="-10.0584" y2="2.6416" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.1336" x2="-10.0584" y2="2.3622" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.3622" x2="-11.049" y2="2.3622" width="0" layer="51"/>
<wire x1="-11.049" y1="2.3622" x2="-11.049" y2="2.1336" width="0" layer="51"/>
<wire x1="-11.049" y1="2.1336" x2="-10.0584" y2="2.1336" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.651" x2="-10.0584" y2="1.8542" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.8542" x2="-11.049" y2="1.8542" width="0" layer="51"/>
<wire x1="-11.049" y1="1.8542" x2="-11.049" y2="1.651" width="0" layer="51"/>
<wire x1="-11.049" y1="1.651" x2="-10.0584" y2="1.651" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.143" x2="-10.0584" y2="1.3716" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.3716" x2="-11.049" y2="1.3716" width="0" layer="51"/>
<wire x1="-11.049" y1="1.3716" x2="-11.049" y2="1.143" width="0" layer="51"/>
<wire x1="-11.049" y1="1.143" x2="-10.0584" y2="1.143" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.635" x2="-10.0584" y2="0.8636" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.8636" x2="-11.049" y2="0.8636" width="0" layer="51"/>
<wire x1="-11.049" y1="0.8636" x2="-11.049" y2="0.635" width="0" layer="51"/>
<wire x1="-11.049" y1="0.635" x2="-10.0584" y2="0.635" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.1524" x2="-10.0584" y2="0.3556" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.3556" x2="-11.049" y2="0.3556" width="0" layer="51"/>
<wire x1="-11.049" y1="0.3556" x2="-11.049" y2="0.1524" width="0" layer="51"/>
<wire x1="-11.049" y1="0.1524" x2="-10.0584" y2="0.1524" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.3556" x2="-10.0584" y2="-0.1524" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.1524" x2="-11.049" y2="-0.1524" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.1524" x2="-11.049" y2="-0.3556" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.3556" x2="-10.0584" y2="-0.3556" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.8636" x2="-10.0584" y2="-0.635" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.635" x2="-11.049" y2="-0.635" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.635" x2="-11.049" y2="-0.8636" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.8636" x2="-10.0584" y2="-0.8636" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.3716" x2="-10.0584" y2="-1.143" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.143" x2="-11.049" y2="-1.143" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.143" x2="-11.049" y2="-1.3716" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.3716" x2="-10.0584" y2="-1.3716" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.8542" x2="-10.0584" y2="-1.651" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.651" x2="-11.049" y2="-1.651" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.651" x2="-11.049" y2="-1.8542" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.8542" x2="-10.0584" y2="-1.8542" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.3622" x2="-10.0584" y2="-2.1336" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.1336" x2="-11.049" y2="-2.1336" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.1336" x2="-11.049" y2="-2.3622" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.3622" x2="-10.0584" y2="-2.3622" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.8702" x2="-10.0584" y2="-2.6416" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.6416" x2="-11.049" y2="-2.6416" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.6416" x2="-11.049" y2="-2.8702" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.8702" x2="-10.0584" y2="-2.8702" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.3528" x2="-10.0584" y2="-3.1496" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.1496" x2="-11.049" y2="-3.1496" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.1496" x2="-11.049" y2="-3.3528" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.3528" x2="-10.0584" y2="-3.3528" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.8608" x2="-10.0584" y2="-3.6322" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.6322" x2="-11.049" y2="-3.6322" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.6322" x2="-11.049" y2="-3.8608" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.8608" x2="-10.0584" y2="-3.8608" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.3688" x2="-10.0584" y2="-4.1402" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.1402" x2="-11.049" y2="-4.1402" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.1402" x2="-11.049" y2="-4.3688" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.3688" x2="-10.0584" y2="-4.3688" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.8514" x2="-10.0584" y2="-4.6482" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.6482" x2="-11.049" y2="-4.6482" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.6482" x2="-11.049" y2="-4.8514" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.8514" x2="-10.0584" y2="-4.8514" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.3594" x2="-10.0584" y2="-5.1308" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.1308" x2="-11.049" y2="-5.1308" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.1308" x2="-11.049" y2="-5.3594" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.3594" x2="-10.0584" y2="-5.3594" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.8674" x2="-10.0584" y2="-5.6388" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.6388" x2="-11.049" y2="-5.6388" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.6388" x2="-11.049" y2="-5.8674" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.8674" x2="-10.0584" y2="-5.8674" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.35" x2="-10.0584" y2="-6.1468" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.1468" x2="-11.049" y2="-6.1468" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.1468" x2="-11.049" y2="-6.35" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.35" x2="-10.0584" y2="-6.35" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.858" x2="-10.0584" y2="-6.6294" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.6294" x2="-11.049" y2="-6.6294" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.6294" x2="-11.049" y2="-6.858" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.858" x2="-10.0584" y2="-6.858" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.366" x2="-10.0584" y2="-7.1374" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.1374" x2="-11.049" y2="-7.1374" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.1374" x2="-11.049" y2="-7.366" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.366" x2="-10.0584" y2="-7.366" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.8486" x2="-10.0584" y2="-7.6454" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.6454" x2="-11.049" y2="-7.6454" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.6454" x2="-11.049" y2="-7.8486" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.8486" x2="-10.0584" y2="-7.8486" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.3566" x2="-10.0584" y2="-8.128" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.128" x2="-11.049" y2="-8.128" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.128" x2="-11.049" y2="-8.3566" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.3566" x2="-10.0584" y2="-8.3566" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.8646" x2="-10.0584" y2="-8.636" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.636" x2="-11.049" y2="-8.636" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.636" x2="-11.049" y2="-8.8646" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.8646" x2="-10.0584" y2="-8.8646" width="0" layer="51"/>
<wire x1="-8.636" y1="-10.0584" x2="-8.8646" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.8646" y1="-10.0584" x2="-8.8646" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.8646" y1="-11.049" x2="-8.636" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.636" y1="-11.049" x2="-8.636" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.128" y1="-10.0584" x2="-8.3566" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.3566" y1="-10.0584" x2="-8.3566" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.3566" y1="-11.049" x2="-8.128" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.128" y1="-11.049" x2="-8.128" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.6454" y1="-10.0584" x2="-7.8486" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.8486" y1="-10.0584" x2="-7.8486" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.8486" y1="-11.049" x2="-7.6454" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.6454" y1="-11.049" x2="-7.6454" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.1374" y1="-10.0584" x2="-7.366" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.366" y1="-10.0584" x2="-7.366" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.366" y1="-11.049" x2="-7.1374" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.1374" y1="-11.049" x2="-7.1374" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.6294" y1="-10.0584" x2="-6.858" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.858" y1="-10.0584" x2="-6.858" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.858" y1="-11.049" x2="-6.6294" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.6294" y1="-11.049" x2="-6.6294" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.1468" y1="-10.0584" x2="-6.35" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.35" y1="-10.0584" x2="-6.35" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.35" y1="-11.049" x2="-6.1468" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.1468" y1="-11.049" x2="-6.1468" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.6388" y1="-10.0584" x2="-5.8674" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.8674" y1="-10.0584" x2="-5.8674" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.8674" y1="-11.049" x2="-5.6388" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.6388" y1="-11.049" x2="-5.6388" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.1308" y1="-10.0584" x2="-5.3594" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.3594" y1="-10.0584" x2="-5.3594" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.3594" y1="-11.049" x2="-5.1308" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.1308" y1="-11.049" x2="-5.1308" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.6482" y1="-10.0584" x2="-4.8514" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.8514" y1="-10.0584" x2="-4.8514" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.8514" y1="-11.049" x2="-4.6482" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.6482" y1="-11.049" x2="-4.6482" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.1402" y1="-10.0584" x2="-4.3688" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.3688" y1="-10.0584" x2="-4.3688" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.3688" y1="-11.049" x2="-4.1402" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.1402" y1="-11.049" x2="-4.1402" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.6322" y1="-10.0584" x2="-3.8608" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.8608" y1="-10.0584" x2="-3.8608" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.8608" y1="-11.049" x2="-3.6322" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.6322" y1="-11.049" x2="-3.6322" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.1496" y1="-10.0584" x2="-3.3528" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.3528" y1="-10.0584" x2="-3.3528" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.3528" y1="-11.049" x2="-3.1496" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.1496" y1="-11.049" x2="-3.1496" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.6416" y1="-10.0584" x2="-2.8702" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.8702" y1="-10.0584" x2="-2.8702" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.8702" y1="-11.049" x2="-2.6416" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.6416" y1="-11.049" x2="-2.6416" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.1336" y1="-10.0584" x2="-2.3622" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.3622" y1="-10.0584" x2="-2.3622" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.3622" y1="-11.049" x2="-2.1336" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.1336" y1="-11.049" x2="-2.1336" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.651" y1="-10.0584" x2="-1.8542" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.8542" y1="-10.0584" x2="-1.8542" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.8542" y1="-11.049" x2="-1.651" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.651" y1="-11.049" x2="-1.651" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.143" y1="-10.0584" x2="-1.3716" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.3716" y1="-10.0584" x2="-1.3716" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.3716" y1="-11.049" x2="-1.143" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.143" y1="-11.049" x2="-1.143" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.635" y1="-10.0584" x2="-0.8636" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.8636" y1="-10.0584" x2="-0.8636" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.8636" y1="-11.049" x2="-0.635" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.635" y1="-11.049" x2="-0.635" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.1524" y1="-10.0584" x2="-0.3556" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.3556" y1="-10.0584" x2="-0.3556" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.3556" y1="-11.049" x2="-0.1524" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.1524" y1="-11.049" x2="-0.1524" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.3556" y1="-10.0584" x2="0.1524" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.1524" y1="-10.0584" x2="0.1524" y2="-11.049" width="0" layer="51"/>
<wire x1="0.1524" y1="-11.049" x2="0.3556" y2="-11.049" width="0" layer="51"/>
<wire x1="0.3556" y1="-11.049" x2="0.3556" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.8636" y1="-10.0584" x2="0.635" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.635" y1="-10.0584" x2="0.635" y2="-11.049" width="0" layer="51"/>
<wire x1="0.635" y1="-11.049" x2="0.8636" y2="-11.049" width="0" layer="51"/>
<wire x1="0.8636" y1="-11.049" x2="0.8636" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.3716" y1="-10.0584" x2="1.143" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.143" y1="-10.0584" x2="1.143" y2="-11.049" width="0" layer="51"/>
<wire x1="1.143" y1="-11.049" x2="1.3716" y2="-11.049" width="0" layer="51"/>
<wire x1="1.3716" y1="-11.049" x2="1.3716" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.8542" y1="-10.0584" x2="1.651" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.651" y1="-10.0584" x2="1.651" y2="-11.049" width="0" layer="51"/>
<wire x1="1.651" y1="-11.049" x2="1.8542" y2="-11.049" width="0" layer="51"/>
<wire x1="1.8542" y1="-11.049" x2="1.8542" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.3622" y1="-10.0584" x2="2.1336" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.1336" y1="-10.0584" x2="2.1336" y2="-11.049" width="0" layer="51"/>
<wire x1="2.1336" y1="-11.049" x2="2.3622" y2="-11.049" width="0" layer="51"/>
<wire x1="2.3622" y1="-11.049" x2="2.3622" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.8702" y1="-10.0584" x2="2.6416" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.6416" y1="-10.0584" x2="2.6416" y2="-11.049" width="0" layer="51"/>
<wire x1="2.6416" y1="-11.049" x2="2.8702" y2="-11.049" width="0" layer="51"/>
<wire x1="2.8702" y1="-11.049" x2="2.8702" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.3528" y1="-10.0584" x2="3.1496" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.1496" y1="-10.0584" x2="3.1496" y2="-11.049" width="0" layer="51"/>
<wire x1="3.1496" y1="-11.049" x2="3.3528" y2="-11.049" width="0" layer="51"/>
<wire x1="3.3528" y1="-11.049" x2="3.3528" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.8608" y1="-10.0584" x2="3.6322" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.6322" y1="-10.0584" x2="3.6322" y2="-11.049" width="0" layer="51"/>
<wire x1="3.6322" y1="-11.049" x2="3.8608" y2="-11.049" width="0" layer="51"/>
<wire x1="3.8608" y1="-11.049" x2="3.8608" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.3688" y1="-10.0584" x2="4.1402" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.1402" y1="-10.0584" x2="4.1402" y2="-11.049" width="0" layer="51"/>
<wire x1="4.1402" y1="-11.049" x2="4.3688" y2="-11.049" width="0" layer="51"/>
<wire x1="4.3688" y1="-11.049" x2="4.3688" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.8514" y1="-10.0584" x2="4.6482" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.6482" y1="-10.0584" x2="4.6482" y2="-11.049" width="0" layer="51"/>
<wire x1="4.6482" y1="-11.049" x2="4.8514" y2="-11.049" width="0" layer="51"/>
<wire x1="4.8514" y1="-11.049" x2="4.8514" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.3594" y1="-10.0584" x2="5.1308" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.1308" y1="-10.0584" x2="5.1308" y2="-11.049" width="0" layer="51"/>
<wire x1="5.1308" y1="-11.049" x2="5.3594" y2="-11.049" width="0" layer="51"/>
<wire x1="5.3594" y1="-11.049" x2="5.3594" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.8674" y1="-10.0584" x2="5.6388" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.6388" y1="-10.0584" x2="5.6388" y2="-11.049" width="0" layer="51"/>
<wire x1="5.6388" y1="-11.049" x2="5.8674" y2="-11.049" width="0" layer="51"/>
<wire x1="5.8674" y1="-11.049" x2="5.8674" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.35" y1="-10.0584" x2="6.1468" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.1468" y1="-10.0584" x2="6.1468" y2="-11.049" width="0" layer="51"/>
<wire x1="6.1468" y1="-11.049" x2="6.35" y2="-11.049" width="0" layer="51"/>
<wire x1="6.35" y1="-11.049" x2="6.35" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.858" y1="-10.0584" x2="6.6294" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.6294" y1="-10.0584" x2="6.6294" y2="-11.049" width="0" layer="51"/>
<wire x1="6.6294" y1="-11.049" x2="6.858" y2="-11.049" width="0" layer="51"/>
<wire x1="6.858" y1="-11.049" x2="6.858" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.366" y1="-10.0584" x2="7.1374" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.1374" y1="-10.0584" x2="7.1374" y2="-11.049" width="0" layer="51"/>
<wire x1="7.1374" y1="-11.049" x2="7.366" y2="-11.049" width="0" layer="51"/>
<wire x1="7.366" y1="-11.049" x2="7.366" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.8486" y1="-10.0584" x2="7.6454" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.6454" y1="-10.0584" x2="7.6454" y2="-11.049" width="0" layer="51"/>
<wire x1="7.6454" y1="-11.049" x2="7.8486" y2="-11.049" width="0" layer="51"/>
<wire x1="7.8486" y1="-11.049" x2="7.8486" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.3566" y1="-10.0584" x2="8.128" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.128" y1="-10.0584" x2="8.128" y2="-11.049" width="0" layer="51"/>
<wire x1="8.128" y1="-11.049" x2="8.3566" y2="-11.049" width="0" layer="51"/>
<wire x1="8.3566" y1="-11.049" x2="8.3566" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.8646" y1="-10.0584" x2="8.636" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.636" y1="-10.0584" x2="8.636" y2="-11.049" width="0" layer="51"/>
<wire x1="8.636" y1="-11.049" x2="8.8646" y2="-11.049" width="0" layer="51"/>
<wire x1="8.8646" y1="-11.049" x2="8.8646" y2="-10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.636" x2="10.0584" y2="-8.8646" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.8646" x2="11.049" y2="-8.8646" width="0" layer="51"/>
<wire x1="11.049" y1="-8.8646" x2="11.049" y2="-8.636" width="0" layer="51"/>
<wire x1="11.049" y1="-8.636" x2="10.0584" y2="-8.636" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.128" x2="10.0584" y2="-8.3566" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.3566" x2="11.049" y2="-8.3566" width="0" layer="51"/>
<wire x1="11.049" y1="-8.3566" x2="11.049" y2="-8.128" width="0" layer="51"/>
<wire x1="11.049" y1="-8.128" x2="10.0584" y2="-8.128" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.6454" x2="10.0584" y2="-7.8486" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.8486" x2="11.049" y2="-7.8486" width="0" layer="51"/>
<wire x1="11.049" y1="-7.8486" x2="11.049" y2="-7.6454" width="0" layer="51"/>
<wire x1="11.049" y1="-7.6454" x2="10.0584" y2="-7.6454" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.1374" x2="10.0584" y2="-7.366" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.366" x2="11.049" y2="-7.366" width="0" layer="51"/>
<wire x1="11.049" y1="-7.366" x2="11.049" y2="-7.1374" width="0" layer="51"/>
<wire x1="11.049" y1="-7.1374" x2="10.0584" y2="-7.1374" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.6294" x2="10.0584" y2="-6.858" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.858" x2="11.049" y2="-6.858" width="0" layer="51"/>
<wire x1="11.049" y1="-6.858" x2="11.049" y2="-6.6294" width="0" layer="51"/>
<wire x1="11.049" y1="-6.6294" x2="10.0584" y2="-6.6294" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.1468" x2="10.0584" y2="-6.35" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.35" x2="11.049" y2="-6.35" width="0" layer="51"/>
<wire x1="11.049" y1="-6.35" x2="11.049" y2="-6.1468" width="0" layer="51"/>
<wire x1="11.049" y1="-6.1468" x2="10.0584" y2="-6.1468" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.6388" x2="10.0584" y2="-5.8674" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.8674" x2="11.049" y2="-5.8674" width="0" layer="51"/>
<wire x1="11.049" y1="-5.8674" x2="11.049" y2="-5.6388" width="0" layer="51"/>
<wire x1="11.049" y1="-5.6388" x2="10.0584" y2="-5.6388" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.1308" x2="10.0584" y2="-5.3594" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.3594" x2="11.049" y2="-5.3594" width="0" layer="51"/>
<wire x1="11.049" y1="-5.3594" x2="11.049" y2="-5.1308" width="0" layer="51"/>
<wire x1="11.049" y1="-5.1308" x2="10.0584" y2="-5.1308" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.6482" x2="10.0584" y2="-4.8514" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.8514" x2="11.049" y2="-4.8514" width="0" layer="51"/>
<wire x1="11.049" y1="-4.8514" x2="11.049" y2="-4.6482" width="0" layer="51"/>
<wire x1="11.049" y1="-4.6482" x2="10.0584" y2="-4.6482" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.1402" x2="10.0584" y2="-4.3688" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.3688" x2="11.049" y2="-4.3688" width="0" layer="51"/>
<wire x1="11.049" y1="-4.3688" x2="11.049" y2="-4.1402" width="0" layer="51"/>
<wire x1="11.049" y1="-4.1402" x2="10.0584" y2="-4.1402" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.6322" x2="10.0584" y2="-3.8608" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.8608" x2="11.049" y2="-3.8608" width="0" layer="51"/>
<wire x1="11.049" y1="-3.8608" x2="11.049" y2="-3.6322" width="0" layer="51"/>
<wire x1="11.049" y1="-3.6322" x2="10.0584" y2="-3.6322" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.1496" x2="10.0584" y2="-3.3528" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.3528" x2="11.049" y2="-3.3528" width="0" layer="51"/>
<wire x1="11.049" y1="-3.3528" x2="11.049" y2="-3.1496" width="0" layer="51"/>
<wire x1="11.049" y1="-3.1496" x2="10.0584" y2="-3.1496" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.6416" x2="10.0584" y2="-2.8702" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.8702" x2="11.049" y2="-2.8702" width="0" layer="51"/>
<wire x1="11.049" y1="-2.8702" x2="11.049" y2="-2.6416" width="0" layer="51"/>
<wire x1="11.049" y1="-2.6416" x2="10.0584" y2="-2.6416" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.1336" x2="10.0584" y2="-2.3622" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.3622" x2="11.049" y2="-2.3622" width="0" layer="51"/>
<wire x1="11.049" y1="-2.3622" x2="11.049" y2="-2.1336" width="0" layer="51"/>
<wire x1="11.049" y1="-2.1336" x2="10.0584" y2="-2.1336" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.651" x2="10.0584" y2="-1.8542" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.8542" x2="11.049" y2="-1.8542" width="0" layer="51"/>
<wire x1="11.049" y1="-1.8542" x2="11.049" y2="-1.651" width="0" layer="51"/>
<wire x1="11.049" y1="-1.651" x2="10.0584" y2="-1.651" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.143" x2="10.0584" y2="-1.3716" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.3716" x2="11.049" y2="-1.3716" width="0" layer="51"/>
<wire x1="11.049" y1="-1.3716" x2="11.049" y2="-1.143" width="0" layer="51"/>
<wire x1="11.049" y1="-1.143" x2="10.0584" y2="-1.143" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.635" x2="10.0584" y2="-0.8636" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.8636" x2="11.049" y2="-0.8636" width="0" layer="51"/>
<wire x1="11.049" y1="-0.8636" x2="11.049" y2="-0.635" width="0" layer="51"/>
<wire x1="11.049" y1="-0.635" x2="10.0584" y2="-0.635" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.1524" x2="10.0584" y2="-0.3556" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.3556" x2="11.049" y2="-0.3556" width="0" layer="51"/>
<wire x1="11.049" y1="-0.3556" x2="11.049" y2="-0.1524" width="0" layer="51"/>
<wire x1="11.049" y1="-0.1524" x2="10.0584" y2="-0.1524" width="0" layer="51"/>
<wire x1="10.0584" y1="0.3556" x2="10.0584" y2="0.1524" width="0" layer="51"/>
<wire x1="10.0584" y1="0.1524" x2="11.049" y2="0.1524" width="0" layer="51"/>
<wire x1="11.049" y1="0.1524" x2="11.049" y2="0.3556" width="0" layer="51"/>
<wire x1="11.049" y1="0.3556" x2="10.0584" y2="0.3556" width="0" layer="51"/>
<wire x1="10.0584" y1="0.8636" x2="10.0584" y2="0.635" width="0" layer="51"/>
<wire x1="10.0584" y1="0.635" x2="11.049" y2="0.635" width="0" layer="51"/>
<wire x1="11.049" y1="0.635" x2="11.049" y2="0.8636" width="0" layer="51"/>
<wire x1="11.049" y1="0.8636" x2="10.0584" y2="0.8636" width="0" layer="51"/>
<wire x1="10.0584" y1="1.3716" x2="10.0584" y2="1.143" width="0" layer="51"/>
<wire x1="10.0584" y1="1.143" x2="11.049" y2="1.143" width="0" layer="51"/>
<wire x1="11.049" y1="1.143" x2="11.049" y2="1.3716" width="0" layer="51"/>
<wire x1="11.049" y1="1.3716" x2="10.0584" y2="1.3716" width="0" layer="51"/>
<wire x1="10.0584" y1="1.8542" x2="10.0584" y2="1.651" width="0" layer="51"/>
<wire x1="10.0584" y1="1.651" x2="11.049" y2="1.651" width="0" layer="51"/>
<wire x1="11.049" y1="1.651" x2="11.049" y2="1.8542" width="0" layer="51"/>
<wire x1="11.049" y1="1.8542" x2="10.0584" y2="1.8542" width="0" layer="51"/>
<wire x1="10.0584" y1="2.3622" x2="10.0584" y2="2.1336" width="0" layer="51"/>
<wire x1="10.0584" y1="2.1336" x2="11.049" y2="2.1336" width="0" layer="51"/>
<wire x1="11.049" y1="2.1336" x2="11.049" y2="2.3622" width="0" layer="51"/>
<wire x1="11.049" y1="2.3622" x2="10.0584" y2="2.3622" width="0" layer="51"/>
<wire x1="10.0584" y1="2.8702" x2="10.0584" y2="2.6416" width="0" layer="51"/>
<wire x1="10.0584" y1="2.6416" x2="11.049" y2="2.6416" width="0" layer="51"/>
<wire x1="11.049" y1="2.6416" x2="11.049" y2="2.8702" width="0" layer="51"/>
<wire x1="11.049" y1="2.8702" x2="10.0584" y2="2.8702" width="0" layer="51"/>
<wire x1="10.0584" y1="3.3528" x2="10.0584" y2="3.1496" width="0" layer="51"/>
<wire x1="10.0584" y1="3.1496" x2="11.049" y2="3.1496" width="0" layer="51"/>
<wire x1="11.049" y1="3.1496" x2="11.049" y2="3.3528" width="0" layer="51"/>
<wire x1="11.049" y1="3.3528" x2="10.0584" y2="3.3528" width="0" layer="51"/>
<wire x1="10.0584" y1="3.8608" x2="10.0584" y2="3.6322" width="0" layer="51"/>
<wire x1="10.0584" y1="3.6322" x2="11.049" y2="3.6322" width="0" layer="51"/>
<wire x1="11.049" y1="3.6322" x2="11.049" y2="3.8608" width="0" layer="51"/>
<wire x1="11.049" y1="3.8608" x2="10.0584" y2="3.8608" width="0" layer="51"/>
<wire x1="10.0584" y1="4.3688" x2="10.0584" y2="4.1402" width="0" layer="51"/>
<wire x1="10.0584" y1="4.1402" x2="11.049" y2="4.1402" width="0" layer="51"/>
<wire x1="11.049" y1="4.1402" x2="11.049" y2="4.3688" width="0" layer="51"/>
<wire x1="11.049" y1="4.3688" x2="10.0584" y2="4.3688" width="0" layer="51"/>
<wire x1="10.0584" y1="4.8514" x2="10.0584" y2="4.6482" width="0" layer="51"/>
<wire x1="10.0584" y1="4.6482" x2="11.049" y2="4.6482" width="0" layer="51"/>
<wire x1="11.049" y1="4.6482" x2="11.049" y2="4.8514" width="0" layer="51"/>
<wire x1="11.049" y1="4.8514" x2="10.0584" y2="4.8514" width="0" layer="51"/>
<wire x1="10.0584" y1="5.3594" x2="10.0584" y2="5.1308" width="0" layer="51"/>
<wire x1="10.0584" y1="5.1308" x2="11.049" y2="5.1308" width="0" layer="51"/>
<wire x1="11.049" y1="5.1308" x2="11.049" y2="5.3594" width="0" layer="51"/>
<wire x1="11.049" y1="5.3594" x2="10.0584" y2="5.3594" width="0" layer="51"/>
<wire x1="10.0584" y1="5.8674" x2="10.0584" y2="5.6388" width="0" layer="51"/>
<wire x1="10.0584" y1="5.6388" x2="11.049" y2="5.6388" width="0" layer="51"/>
<wire x1="11.049" y1="5.6388" x2="11.049" y2="5.8674" width="0" layer="51"/>
<wire x1="11.049" y1="5.8674" x2="10.0584" y2="5.8674" width="0" layer="51"/>
<wire x1="10.0584" y1="6.35" x2="10.0584" y2="6.1468" width="0" layer="51"/>
<wire x1="10.0584" y1="6.1468" x2="11.049" y2="6.1468" width="0" layer="51"/>
<wire x1="11.049" y1="6.1468" x2="11.049" y2="6.35" width="0" layer="51"/>
<wire x1="11.049" y1="6.35" x2="10.0584" y2="6.35" width="0" layer="51"/>
<wire x1="10.0584" y1="6.858" x2="10.0584" y2="6.6294" width="0" layer="51"/>
<wire x1="10.0584" y1="6.6294" x2="11.049" y2="6.6294" width="0" layer="51"/>
<wire x1="11.049" y1="6.6294" x2="11.049" y2="6.858" width="0" layer="51"/>
<wire x1="11.049" y1="6.858" x2="10.0584" y2="6.858" width="0" layer="51"/>
<wire x1="10.0584" y1="7.366" x2="10.0584" y2="7.1374" width="0" layer="51"/>
<wire x1="10.0584" y1="7.1374" x2="11.049" y2="7.1374" width="0" layer="51"/>
<wire x1="11.049" y1="7.1374" x2="11.049" y2="7.366" width="0" layer="51"/>
<wire x1="11.049" y1="7.366" x2="10.0584" y2="7.366" width="0" layer="51"/>
<wire x1="10.0584" y1="7.8486" x2="10.0584" y2="7.6454" width="0" layer="51"/>
<wire x1="10.0584" y1="7.6454" x2="11.049" y2="7.6454" width="0" layer="51"/>
<wire x1="11.049" y1="7.6454" x2="11.049" y2="7.8486" width="0" layer="51"/>
<wire x1="11.049" y1="7.8486" x2="10.0584" y2="7.8486" width="0" layer="51"/>
<wire x1="10.0584" y1="8.3566" x2="10.0584" y2="8.128" width="0" layer="51"/>
<wire x1="10.0584" y1="8.128" x2="11.049" y2="8.128" width="0" layer="51"/>
<wire x1="11.049" y1="8.128" x2="11.049" y2="8.3566" width="0" layer="51"/>
<wire x1="11.049" y1="8.3566" x2="10.0584" y2="8.3566" width="0" layer="51"/>
<wire x1="10.0584" y1="8.8646" x2="10.0584" y2="8.636" width="0" layer="51"/>
<wire x1="10.0584" y1="8.636" x2="11.049" y2="8.636" width="0" layer="51"/>
<wire x1="11.049" y1="8.636" x2="11.049" y2="8.8646" width="0" layer="51"/>
<wire x1="11.049" y1="8.8646" x2="10.0584" y2="8.8646" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.7884" x2="-8.7884" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="-10.0584" x2="10.0584" y2="-10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="-10.0584" x2="10.0584" y2="10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="10.0584" x2="-10.0584" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="10.0584" x2="-10.0584" y2="-10.0584" width="0" layer="51"/>
<text x="-12.6238" y="8.7376" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-4.6736" y="-15.0368" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="15.0368" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="AT32UC3A0256-ALUT_A">
<pin name="VDDIO_2" x="-22.86" y="50.8" length="middle" direction="pwr"/>
<pin name="VDDOUT" x="-22.86" y="48.26" length="middle" direction="pwr"/>
<pin name="VDDIN" x="-22.86" y="45.72" length="middle" direction="pwr"/>
<pin name="VDDCORE_2" x="-22.86" y="43.18" length="middle" direction="pwr"/>
<pin name="VDDIO_3" x="-22.86" y="40.64" length="middle" direction="pwr"/>
<pin name="VDDCORE_3" x="-22.86" y="38.1" length="middle" direction="pwr"/>
<pin name="VDDCORE" x="-22.86" y="35.56" length="middle" direction="pwr"/>
<pin name="VDDIO" x="-22.86" y="33.02" length="middle" direction="pwr"/>
<pin name="RESET_N" x="-22.86" y="27.94" length="middle" direction="in"/>
<pin name="VBUS" x="-22.86" y="25.4" length="middle" direction="in"/>
<pin name="DM" x="-22.86" y="22.86" length="middle" direction="in"/>
<pin name="DP" x="-22.86" y="20.32" length="middle" direction="in"/>
<pin name="PA00" x="-22.86" y="15.24" length="middle"/>
<pin name="PA01" x="-22.86" y="12.7" length="middle"/>
<pin name="PA02" x="-22.86" y="10.16" length="middle"/>
<pin name="PA03" x="-22.86" y="7.62" length="middle"/>
<pin name="PA04" x="-22.86" y="5.08" length="middle"/>
<pin name="PA05" x="-22.86" y="2.54" length="middle"/>
<pin name="PA06" x="-22.86" y="0" length="middle"/>
<pin name="PA07" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA08" x="-22.86" y="-5.08" length="middle"/>
<pin name="PA09" x="-22.86" y="-7.62" length="middle"/>
<pin name="PA10" x="-22.86" y="-10.16" length="middle"/>
<pin name="PA11" x="-22.86" y="-12.7" length="middle"/>
<pin name="PA12" x="-22.86" y="-15.24" length="middle"/>
<pin name="PA13" x="-22.86" y="-17.78" length="middle"/>
<pin name="PA14" x="-22.86" y="-20.32" length="middle"/>
<pin name="PA15" x="-22.86" y="-22.86" length="middle"/>
<pin name="PA16" x="-22.86" y="-25.4" length="middle"/>
<pin name="PA17" x="-22.86" y="-27.94" length="middle"/>
<pin name="PA18" x="-22.86" y="-30.48" length="middle"/>
<pin name="PA19" x="-22.86" y="-33.02" length="middle"/>
<pin name="PA20" x="-22.86" y="-35.56" length="middle"/>
<pin name="N/C" x="-22.86" y="-40.64" length="middle" direction="nc"/>
<pin name="GND_2" x="-22.86" y="-45.72" length="middle" direction="pas"/>
<pin name="GND_3" x="-22.86" y="-48.26" length="middle" direction="pas"/>
<pin name="GND_4" x="-22.86" y="-50.8" length="middle" direction="pas"/>
<pin name="GND_5" x="-22.86" y="-53.34" length="middle" direction="pas"/>
<pin name="GND_6" x="-22.86" y="-55.88" length="middle" direction="pas"/>
<pin name="GND" x="-22.86" y="-58.42" length="middle" direction="pas"/>
<pin name="PB20" x="22.86" y="50.8" length="middle" rot="R180"/>
<pin name="PB21" x="22.86" y="48.26" length="middle" rot="R180"/>
<pin name="PB22" x="22.86" y="45.72" length="middle" rot="R180"/>
<pin name="PB23" x="22.86" y="43.18" length="middle" rot="R180"/>
<pin name="PB24" x="22.86" y="40.64" length="middle" rot="R180"/>
<pin name="PB25" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="PB26" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="PB27" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="PB28" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PB29" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PB30" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PB31" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PX00" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PX01" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PX02" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="PX03" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PX04" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PX05" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PX06" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PX07" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PX08" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PX09" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PX10" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PX11" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PX12" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="PX13" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PX14" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PX15" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PX16" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PX17" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PX18" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PX19" x="22.86" y="-30.48" length="middle" rot="R180"/>
<wire x1="-17.78" y1="55.88" x2="-17.78" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="-17.78" y1="-63.5" x2="17.78" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="17.78" y1="-63.5" x2="17.78" y2="55.88" width="0.4064" layer="94"/>
<wire x1="17.78" y1="55.88" x2="-17.78" y2="55.88" width="0.4064" layer="94"/>
<text x="-6.0198" y="58.4708" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.08" y="-67.0814" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="AT32UC3A0256-ALUT_B">
<pin name="VDDIO_2" x="-17.78" y="48.26" length="middle" direction="pwr"/>
<pin name="VDDIO_3" x="-17.78" y="45.72" length="middle" direction="pwr"/>
<pin name="VDDIO_4" x="-17.78" y="43.18" length="middle" direction="pwr"/>
<pin name="VDDIO" x="-17.78" y="40.64" length="middle" direction="pwr"/>
<pin name="VDDANA" x="-17.78" y="38.1" length="middle" direction="pwr"/>
<pin name="VDDPLL" x="-17.78" y="35.56" length="middle" direction="pwr"/>
<pin name="VDDCORE" x="-17.78" y="33.02" length="middle" direction="pwr"/>
<pin name="TMS" x="-17.78" y="27.94" length="middle" direction="in"/>
<pin name="TCK" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="TDI" x="-17.78" y="22.86" length="middle" direction="in"/>
<pin name="PA21" x="-17.78" y="17.78" length="middle"/>
<pin name="PA22" x="-17.78" y="15.24" length="middle"/>
<pin name="PA23" x="-17.78" y="12.7" length="middle"/>
<pin name="PA24" x="-17.78" y="10.16" length="middle"/>
<pin name="PA25" x="-17.78" y="7.62" length="middle"/>
<pin name="PA26" x="-17.78" y="5.08" length="middle"/>
<pin name="PA27" x="-17.78" y="2.54" length="middle"/>
<pin name="PA28" x="-17.78" y="0" length="middle"/>
<pin name="PA29" x="-17.78" y="-2.54" length="middle"/>
<pin name="PA30" x="-17.78" y="-5.08" length="middle"/>
<pin name="PB00" x="-17.78" y="-10.16" length="middle"/>
<pin name="PB01" x="-17.78" y="-12.7" length="middle"/>
<pin name="PB02" x="-17.78" y="-15.24" length="middle"/>
<pin name="PB03" x="-17.78" y="-17.78" length="middle"/>
<pin name="PB04" x="-17.78" y="-20.32" length="middle"/>
<pin name="PB05" x="-17.78" y="-22.86" length="middle"/>
<pin name="PB06" x="-17.78" y="-25.4" length="middle"/>
<pin name="PB07" x="-17.78" y="-27.94" length="middle"/>
<pin name="PB08" x="-17.78" y="-30.48" length="middle"/>
<pin name="PB09" x="-17.78" y="-33.02" length="middle"/>
<pin name="PB10" x="-17.78" y="-35.56" length="middle"/>
<pin name="ADVREF" x="-17.78" y="-40.64" length="middle" direction="pas"/>
<pin name="GNDANA" x="-17.78" y="-45.72" length="middle" direction="pas"/>
<pin name="GND_2" x="-17.78" y="-48.26" length="middle" direction="pas"/>
<pin name="GND_3" x="-17.78" y="-50.8" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-53.34" length="middle" direction="pas"/>
<pin name="TDO" x="17.78" y="48.26" length="middle" direction="out" rot="R180"/>
<pin name="PB11" x="17.78" y="43.18" length="middle" rot="R180"/>
<pin name="PB12" x="17.78" y="40.64" length="middle" rot="R180"/>
<pin name="PB13" x="17.78" y="38.1" length="middle" rot="R180"/>
<pin name="PB14" x="17.78" y="35.56" length="middle" rot="R180"/>
<pin name="PB15" x="17.78" y="33.02" length="middle" rot="R180"/>
<pin name="PB16" x="17.78" y="30.48" length="middle" rot="R180"/>
<pin name="PB17" x="17.78" y="27.94" length="middle" rot="R180"/>
<pin name="PB18" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="PB19" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="PC00" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="PC01" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="PC02" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="PC03" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PC04" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PC05" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PX20" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PX21" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="PX22" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PX23" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PX24" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PX25" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="PX26" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="PX27" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="PX28" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="PX29" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="PX30" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="PX31" x="17.78" y="-27.94" length="middle" rot="R180"/>
<pin name="PX32" x="17.78" y="-30.48" length="middle" rot="R180"/>
<pin name="PX33" x="17.78" y="-33.02" length="middle" rot="R180"/>
<pin name="PX34" x="17.78" y="-35.56" length="middle" rot="R180"/>
<pin name="PX35" x="17.78" y="-38.1" length="middle" rot="R180"/>
<pin name="PX36" x="17.78" y="-40.64" length="middle" rot="R180"/>
<pin name="PX37" x="17.78" y="-43.18" length="middle" rot="R180"/>
<pin name="PX38" x="17.78" y="-45.72" length="middle" rot="R180"/>
<pin name="PX39" x="17.78" y="-48.26" length="middle" rot="R180"/>
<wire x1="-12.7" y1="53.34" x2="-12.7" y2="-58.42" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-58.42" x2="12.7" y2="-58.42" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-58.42" x2="12.7" y2="53.34" width="0.4064" layer="94"/>
<wire x1="12.7" y1="53.34" x2="-12.7" y2="53.34" width="0.4064" layer="94"/>
<text x="-5.1562" y="55.2704" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.4356" y="-61.849" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT32UC3A0256-ALUT" prefix="U">
<description>32-Bit Microcontroller</description>
<gates>
<gate name="A" symbol="AT32UC3A0256-ALUT_A" x="0" y="0"/>
<gate name="B" symbol="AT32UC3A0256-ALUT_B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P2200X2200X160-144N">
<connects>
<connect gate="A" pin="DM" pad="70"/>
<connect gate="A" pin="DP" pad="71"/>
<connect gate="A" pin="GND" pad="72"/>
<connect gate="A" pin="GND_2" pad="8"/>
<connect gate="A" pin="GND_3" pad="18"/>
<connect gate="A" pin="GND_4" pad="28"/>
<connect gate="A" pin="GND_5" pad="37"/>
<connect gate="A" pin="GND_6" pad="52"/>
<connect gate="A" pin="N/C" pad="49"/>
<connect gate="A" pin="PA00" pad="25"/>
<connect gate="A" pin="PA01" pad="27"/>
<connect gate="A" pin="PA02" pad="30"/>
<connect gate="A" pin="PA03" pad="32"/>
<connect gate="A" pin="PA04" pad="34"/>
<connect gate="A" pin="PA05" pad="39"/>
<connect gate="A" pin="PA06" pad="41"/>
<connect gate="A" pin="PA07" pad="43"/>
<connect gate="A" pin="PA08" pad="45"/>
<connect gate="A" pin="PA09" pad="47"/>
<connect gate="A" pin="PA10" pad="48"/>
<connect gate="A" pin="PA11" pad="50"/>
<connect gate="A" pin="PA12" pad="53"/>
<connect gate="A" pin="PA13" pad="54"/>
<connect gate="A" pin="PA14" pad="56"/>
<connect gate="A" pin="PA15" pad="57"/>
<connect gate="A" pin="PA16" pad="58"/>
<connect gate="A" pin="PA17" pad="60"/>
<connect gate="A" pin="PA18" pad="62"/>
<connect gate="A" pin="PA19" pad="64"/>
<connect gate="A" pin="PA20" pad="66"/>
<connect gate="A" pin="PB20" pad="3"/>
<connect gate="A" pin="PB21" pad="5"/>
<connect gate="A" pin="PB22" pad="6"/>
<connect gate="A" pin="PB23" pad="9"/>
<connect gate="A" pin="PB24" pad="11"/>
<connect gate="A" pin="PB25" pad="13"/>
<connect gate="A" pin="PB26" pad="14"/>
<connect gate="A" pin="PB27" pad="15"/>
<connect gate="A" pin="PB28" pad="19"/>
<connect gate="A" pin="PB29" pad="20"/>
<connect gate="A" pin="PB30" pad="21"/>
<connect gate="A" pin="PB31" pad="22"/>
<connect gate="A" pin="PX00" pad="1"/>
<connect gate="A" pin="PX01" pad="2"/>
<connect gate="A" pin="PX02" pad="4"/>
<connect gate="A" pin="PX03" pad="10"/>
<connect gate="A" pin="PX04" pad="12"/>
<connect gate="A" pin="PX05" pad="24"/>
<connect gate="A" pin="PX06" pad="26"/>
<connect gate="A" pin="PX07" pad="31"/>
<connect gate="A" pin="PX08" pad="33"/>
<connect gate="A" pin="PX09" pad="35"/>
<connect gate="A" pin="PX10" pad="38"/>
<connect gate="A" pin="PX11" pad="40"/>
<connect gate="A" pin="PX12" pad="42"/>
<connect gate="A" pin="PX13" pad="44"/>
<connect gate="A" pin="PX14" pad="46"/>
<connect gate="A" pin="PX15" pad="59"/>
<connect gate="A" pin="PX16" pad="61"/>
<connect gate="A" pin="PX17" pad="63"/>
<connect gate="A" pin="PX18" pad="65"/>
<connect gate="A" pin="PX19" pad="67"/>
<connect gate="A" pin="RESET_N" pad="23"/>
<connect gate="A" pin="VBUS" pad="68"/>
<connect gate="A" pin="VDDCORE" pad="55"/>
<connect gate="A" pin="VDDCORE_2" pad="29"/>
<connect gate="A" pin="VDDCORE_3" pad="51"/>
<connect gate="A" pin="VDDIN" pad="17"/>
<connect gate="A" pin="VDDIO" pad="69"/>
<connect gate="A" pin="VDDIO_2" pad="7"/>
<connect gate="A" pin="VDDIO_3" pad="36"/>
<connect gate="A" pin="VDDOUT" pad="16"/>
<connect gate="B" pin="ADVREF" pad="82"/>
<connect gate="B" pin="GND" pad="117"/>
<connect gate="B" pin="GNDANA" pad="83"/>
<connect gate="B" pin="GND_2" pad="94"/>
<connect gate="B" pin="GND_3" pad="109"/>
<connect gate="B" pin="PA21" pad="73"/>
<connect gate="B" pin="PA22" pad="74"/>
<connect gate="B" pin="PA23" pad="75"/>
<connect gate="B" pin="PA24" pad="76"/>
<connect gate="B" pin="PA25" pad="77"/>
<connect gate="B" pin="PA26" pad="78"/>
<connect gate="B" pin="PA27" pad="79"/>
<connect gate="B" pin="PA28" pad="80"/>
<connect gate="B" pin="PA29" pad="122"/>
<connect gate="B" pin="PA30" pad="123"/>
<connect gate="B" pin="PB00" pad="88"/>
<connect gate="B" pin="PB01" pad="90"/>
<connect gate="B" pin="PB02" pad="96"/>
<connect gate="B" pin="PB03" pad="98"/>
<connect gate="B" pin="PB04" pad="100"/>
<connect gate="B" pin="PB05" pad="102"/>
<connect gate="B" pin="PB06" pad="104"/>
<connect gate="B" pin="PB07" pad="106"/>
<connect gate="B" pin="PB08" pad="111"/>
<connect gate="B" pin="PB09" pad="113"/>
<connect gate="B" pin="PB10" pad="115"/>
<connect gate="B" pin="PB11" pad="119"/>
<connect gate="B" pin="PB12" pad="121"/>
<connect gate="B" pin="PB13" pad="126"/>
<connect gate="B" pin="PB14" pad="127"/>
<connect gate="B" pin="PB15" pad="134"/>
<connect gate="B" pin="PB16" pad="136"/>
<connect gate="B" pin="PB17" pad="139"/>
<connect gate="B" pin="PB18" pad="141"/>
<connect gate="B" pin="PB19" pad="143"/>
<connect gate="B" pin="PC00" pad="85"/>
<connect gate="B" pin="PC01" pad="86"/>
<connect gate="B" pin="PC02" pad="124"/>
<connect gate="B" pin="PC03" pad="125"/>
<connect gate="B" pin="PC04" pad="132"/>
<connect gate="B" pin="PC05" pad="133"/>
<connect gate="B" pin="PX20" pad="87"/>
<connect gate="B" pin="PX21" pad="89"/>
<connect gate="B" pin="PX22" pad="91"/>
<connect gate="B" pin="PX23" pad="95"/>
<connect gate="B" pin="PX24" pad="97"/>
<connect gate="B" pin="PX25" pad="99"/>
<connect gate="B" pin="PX26" pad="101"/>
<connect gate="B" pin="PX27" pad="103"/>
<connect gate="B" pin="PX28" pad="105"/>
<connect gate="B" pin="PX29" pad="107"/>
<connect gate="B" pin="PX30" pad="110"/>
<connect gate="B" pin="PX31" pad="112"/>
<connect gate="B" pin="PX32" pad="114"/>
<connect gate="B" pin="PX33" pad="118"/>
<connect gate="B" pin="PX34" pad="120"/>
<connect gate="B" pin="PX35" pad="135"/>
<connect gate="B" pin="PX36" pad="137"/>
<connect gate="B" pin="PX37" pad="140"/>
<connect gate="B" pin="PX38" pad="142"/>
<connect gate="B" pin="PX39" pad="144"/>
<connect gate="B" pin="TCK" pad="129"/>
<connect gate="B" pin="TDI" pad="131"/>
<connect gate="B" pin="TDO" pad="130"/>
<connect gate="B" pin="TMS" pad="128"/>
<connect gate="B" pin="VDDANA" pad="81"/>
<connect gate="B" pin="VDDCORE" pad="138"/>
<connect gate="B" pin="VDDIO" pad="116"/>
<connect gate="B" pin="VDDIO_2" pad="92"/>
<connect gate="B" pin="VDDIO_3" pad="93"/>
<connect gate="B" pin="VDDIO_4" pad="108"/>
<connect gate="B" pin="VDDPLL" pad="84"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="32-Bit Microcontroller" constant="no"/>
<attribute name="MPN" value="AT32UC3A0256-ALUT" constant="no"/>
<attribute name="OC_FARNELL" value="1841635" constant="no"/>
<attribute name="OC_NEWARK" value="12T1369" constant="no"/>
<attribute name="PACKAGE" value="LQFP-144" constant="no"/>
<attribute name="SUPPLIER" value="Atmel Corporation" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Texas Instruments_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 30/07/2012 17:45:58</description>
<packages>
<package name="QFP50P900X900X120-49N">
<smd name="1" x="-4.2164" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-4.2164" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-4.2164" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-4.2164" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-4.2164" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-4.2164" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-4.2164" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-4.2164" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-4.2164" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-4.2164" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-4.2164" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-4.2164" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-2.7432" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="14" x="-2.2606" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="15" x="-1.7526" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="16" x="-1.2446" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="17" x="-0.762" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="18" x="-0.254" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="19" x="0.254" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="20" x="0.762" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="21" x="1.2446" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="22" x="1.7526" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="23" x="2.2606" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="24" x="2.7432" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="25" x="4.2164" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="4.2164" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="27" x="4.2164" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="28" x="4.2164" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="29" x="4.2164" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="30" x="4.2164" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="31" x="4.2164" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="32" x="4.2164" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="33" x="4.2164" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="4.2164" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="4.2164" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="4.2164" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="2.7432" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="2.2606" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="1.7526" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="1.2446" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="0.762" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="0.254" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="-0.254" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="-0.762" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="-1.2446" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="-1.7526" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="-2.2606" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="-2.7432" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="0" y="0" dx="4.5974" dy="4.5974" layer="1"/>
<wire x1="0.762" y1="-5.2832" x2="0.762" y2="-6.3246" width="0.1524" layer="21"/>
<wire x1="-6.2738" y1="-1.778" x2="-5.2578" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="1.27" y1="5.2832" x2="1.27" y2="6.2992" width="0.1524" layer="21"/>
<wire x1="5.2578" y1="-0.254" x2="6.2738" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.2258" y1="3.6068" x2="-3.6068" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="2.7686" x2="-2.7686" y2="3.1496" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.6068" x2="-3.2258" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="-3.6068" x2="3.6068" y2="-3.2258" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.6068" x2="3.2258" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="3.6068" x2="-3.6068" y2="3.2258" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.2258" x2="-3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.2258" y1="-3.6068" x2="3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.2258" x2="3.6068" y2="3.6068" width="0.1524" layer="21"/>
<text x="-6.1722" y="2.7432" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="2.6162" y1="3.6068" x2="2.8956" y2="3.6068" width="0" layer="51"/>
<wire x1="2.8956" y1="3.6068" x2="2.8956" y2="4.5974" width="0" layer="51"/>
<wire x1="2.8956" y1="4.5974" x2="2.6162" y2="4.5974" width="0" layer="51"/>
<wire x1="2.6162" y1="4.5974" x2="2.6162" y2="3.6068" width="0" layer="51"/>
<wire x1="2.1082" y1="3.6068" x2="2.3876" y2="3.6068" width="0" layer="51"/>
<wire x1="2.3876" y1="3.6068" x2="2.3876" y2="4.5974" width="0" layer="51"/>
<wire x1="2.3876" y1="4.5974" x2="2.1082" y2="4.5974" width="0" layer="51"/>
<wire x1="2.1082" y1="4.5974" x2="2.1082" y2="3.6068" width="0" layer="51"/>
<wire x1="1.6256" y1="3.6068" x2="1.8796" y2="3.6068" width="0" layer="51"/>
<wire x1="1.8796" y1="3.6068" x2="1.8796" y2="4.5974" width="0" layer="51"/>
<wire x1="1.8796" y1="4.5974" x2="1.6256" y2="4.5974" width="0" layer="51"/>
<wire x1="1.6256" y1="4.5974" x2="1.6256" y2="3.6068" width="0" layer="51"/>
<wire x1="1.1176" y1="3.6068" x2="1.397" y2="3.6068" width="0" layer="51"/>
<wire x1="1.397" y1="3.6068" x2="1.397" y2="4.5974" width="0" layer="51"/>
<wire x1="1.397" y1="4.5974" x2="1.1176" y2="4.5974" width="0" layer="51"/>
<wire x1="1.1176" y1="4.5974" x2="1.1176" y2="3.6068" width="0" layer="51"/>
<wire x1="0.6096" y1="3.6068" x2="0.889" y2="3.6068" width="0" layer="51"/>
<wire x1="0.889" y1="3.6068" x2="0.889" y2="4.5974" width="0" layer="51"/>
<wire x1="0.889" y1="4.5974" x2="0.6096" y2="4.5974" width="0" layer="51"/>
<wire x1="0.6096" y1="4.5974" x2="0.6096" y2="3.6068" width="0" layer="51"/>
<wire x1="0.127" y1="3.6068" x2="0.381" y2="3.6068" width="0" layer="51"/>
<wire x1="0.381" y1="3.6068" x2="0.381" y2="4.5974" width="0" layer="51"/>
<wire x1="0.381" y1="4.5974" x2="0.127" y2="4.5974" width="0" layer="51"/>
<wire x1="0.127" y1="4.5974" x2="0.127" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.381" y1="3.6068" x2="-0.127" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.127" y1="3.6068" x2="-0.127" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.127" y1="4.5974" x2="-0.381" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.381" y1="4.5974" x2="-0.381" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.889" y1="3.6068" x2="-0.6096" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.6096" y1="3.6068" x2="-0.6096" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.6096" y1="4.5974" x2="-0.889" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.889" y1="4.5974" x2="-0.889" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.397" y1="3.6068" x2="-1.1176" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.1176" y1="3.6068" x2="-1.1176" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.1176" y1="4.5974" x2="-1.397" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.397" y1="4.5974" x2="-1.397" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.8796" y1="3.6068" x2="-1.6256" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.6256" y1="3.6068" x2="-1.6256" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.6256" y1="4.5974" x2="-1.8796" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.8796" y1="4.5974" x2="-1.8796" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.3876" y1="3.6068" x2="-2.3368" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.3368" y1="3.6068" x2="-2.1082" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.1082" y1="3.6068" x2="-2.1082" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.1082" y1="4.5974" x2="-2.3876" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.3876" y1="4.5974" x2="-2.3876" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.8956" y1="3.6068" x2="-2.6162" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.6162" y1="3.6068" x2="-2.6162" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.6162" y1="4.5974" x2="-2.8956" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.8956" y1="4.5974" x2="-2.8956" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.6162" x2="-3.6068" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.8956" x2="-4.5974" y2="2.8956" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.8956" x2="-4.5974" y2="2.6162" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.6162" x2="-3.6068" y2="2.6162" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.1082" x2="-3.6068" y2="2.3368" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3368" x2="-3.6068" y2="2.3876" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3876" x2="-4.5974" y2="2.3876" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.3876" x2="-4.5974" y2="2.1082" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.1082" x2="-3.6068" y2="2.1082" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.6256" x2="-3.6068" y2="1.8796" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.8796" x2="-4.5974" y2="1.8796" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.8796" x2="-4.5974" y2="1.6256" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.6256" x2="-3.6068" y2="1.6256" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.1176" x2="-3.6068" y2="1.397" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.397" x2="-4.5974" y2="1.397" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.397" x2="-4.5974" y2="1.1176" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.1176" x2="-3.6068" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.6096" x2="-3.6068" y2="0.889" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.889" x2="-4.5974" y2="0.889" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.889" x2="-4.5974" y2="0.6096" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.6096" x2="-3.6068" y2="0.6096" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.127" x2="-3.6068" y2="0.381" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.381" x2="-4.5974" y2="0.381" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.381" x2="-4.5974" y2="0.127" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.127" x2="-3.6068" y2="0.127" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.381" x2="-3.6068" y2="-0.127" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.127" x2="-4.5974" y2="-0.127" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.127" x2="-4.5974" y2="-0.381" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.381" x2="-3.6068" y2="-0.381" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.889" x2="-3.6068" y2="-0.6096" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.6096" x2="-4.5974" y2="-0.6096" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.6096" x2="-4.5974" y2="-0.889" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.889" x2="-3.6068" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.397" x2="-3.6068" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.1176" x2="-4.5974" y2="-1.1176" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.1176" x2="-4.5974" y2="-1.397" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.397" x2="-3.6068" y2="-1.397" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.8796" x2="-3.6068" y2="-1.6256" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.6256" x2="-4.5974" y2="-1.6256" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.6256" x2="-4.5974" y2="-1.8796" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.8796" x2="-3.6068" y2="-1.8796" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.3876" x2="-3.6068" y2="-2.1082" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.1082" x2="-4.5974" y2="-2.1082" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.1082" x2="-4.5974" y2="-2.3876" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.3876" x2="-3.6068" y2="-2.3876" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.8956" x2="-3.6068" y2="-2.6162" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.6162" x2="-4.5974" y2="-2.6162" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.6162" x2="-4.5974" y2="-2.8956" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.8956" x2="-3.6068" y2="-2.8956" width="0" layer="51"/>
<wire x1="-2.6162" y1="-3.6068" x2="-2.8956" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.8956" y1="-3.6068" x2="-2.8956" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.8956" y1="-4.5974" x2="-2.6162" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.6162" y1="-4.5974" x2="-2.6162" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.1082" y1="-3.6068" x2="-2.3876" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.3876" y1="-3.6068" x2="-2.3876" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.3876" y1="-4.5974" x2="-2.1082" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.1082" y1="-4.5974" x2="-2.1082" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.6256" y1="-3.6068" x2="-1.8796" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.8796" y1="-3.6068" x2="-1.8796" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.8796" y1="-4.5974" x2="-1.6256" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.6256" y1="-4.5974" x2="-1.6256" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.1176" y1="-3.6068" x2="-1.397" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.397" y1="-3.6068" x2="-1.397" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.397" y1="-4.5974" x2="-1.1176" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.1176" y1="-4.5974" x2="-1.1176" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.6096" y1="-3.6068" x2="-0.889" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.889" y1="-3.6068" x2="-0.889" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.889" y1="-4.5974" x2="-0.6096" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.6096" y1="-4.5974" x2="-0.6096" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.127" y1="-3.6068" x2="-0.381" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.381" y1="-3.6068" x2="-0.381" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.381" y1="-4.5974" x2="-0.127" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.127" y1="-4.5974" x2="-0.127" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.381" y1="-3.6068" x2="0.127" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.127" y1="-3.6068" x2="0.127" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.127" y1="-4.5974" x2="0.381" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.381" y1="-4.5974" x2="0.381" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.889" y1="-3.6068" x2="0.6096" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.6096" y1="-3.6068" x2="0.6096" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.6096" y1="-4.5974" x2="0.889" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.889" y1="-4.5974" x2="0.889" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.397" y1="-3.6068" x2="1.1176" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.1176" y1="-3.6068" x2="1.1176" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.1176" y1="-4.5974" x2="1.397" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.397" y1="-4.5974" x2="1.397" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.8796" y1="-3.6068" x2="1.6256" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.6256" y1="-3.6068" x2="1.6256" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.6256" y1="-4.5974" x2="1.8796" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.8796" y1="-4.5974" x2="1.8796" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.3876" y1="-3.6068" x2="2.1082" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.1082" y1="-3.6068" x2="2.1082" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.1082" y1="-4.5974" x2="2.3876" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.3876" y1="-4.5974" x2="2.3876" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.8956" y1="-3.6068" x2="2.6162" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.6162" y1="-3.6068" x2="2.6162" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.6162" y1="-4.5974" x2="2.8956" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.8956" y1="-4.5974" x2="2.8956" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.6162" x2="3.6068" y2="-2.8956" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.8956" x2="4.5974" y2="-2.8956" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.8956" x2="4.5974" y2="-2.6162" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.6162" x2="3.6068" y2="-2.6162" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.1082" x2="3.6068" y2="-2.3876" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.3876" x2="4.5974" y2="-2.3876" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.3876" x2="4.5974" y2="-2.1082" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.1082" x2="3.6068" y2="-2.1082" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.6256" x2="3.6068" y2="-1.8796" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.8796" x2="4.5974" y2="-1.8796" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.8796" x2="4.5974" y2="-1.6256" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.6256" x2="3.6068" y2="-1.6256" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.1176" x2="3.6068" y2="-1.397" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.397" x2="4.5974" y2="-1.397" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.397" x2="4.5974" y2="-1.1176" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.1176" x2="3.6068" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.6096" x2="3.6068" y2="-0.889" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.889" x2="4.5974" y2="-0.889" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.889" x2="4.5974" y2="-0.6096" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.6096" x2="3.6068" y2="-0.6096" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.127" x2="3.6068" y2="-0.381" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.381" x2="4.5974" y2="-0.381" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.381" x2="4.5974" y2="-0.127" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.127" x2="3.6068" y2="-0.127" width="0" layer="51"/>
<wire x1="3.6068" y1="0.381" x2="3.6068" y2="0.127" width="0" layer="51"/>
<wire x1="3.6068" y1="0.127" x2="4.5974" y2="0.127" width="0" layer="51"/>
<wire x1="4.5974" y1="0.127" x2="4.5974" y2="0.381" width="0" layer="51"/>
<wire x1="4.5974" y1="0.381" x2="3.6068" y2="0.381" width="0" layer="51"/>
<wire x1="3.6068" y1="0.889" x2="3.6068" y2="0.6096" width="0" layer="51"/>
<wire x1="3.6068" y1="0.6096" x2="4.5974" y2="0.6096" width="0" layer="51"/>
<wire x1="4.5974" y1="0.6096" x2="4.5974" y2="0.889" width="0" layer="51"/>
<wire x1="4.5974" y1="0.889" x2="3.6068" y2="0.889" width="0" layer="51"/>
<wire x1="3.6068" y1="1.397" x2="3.6068" y2="1.1176" width="0" layer="51"/>
<wire x1="3.6068" y1="1.1176" x2="4.5974" y2="1.1176" width="0" layer="51"/>
<wire x1="4.5974" y1="1.1176" x2="4.5974" y2="1.397" width="0" layer="51"/>
<wire x1="4.5974" y1="1.397" x2="3.6068" y2="1.397" width="0" layer="51"/>
<wire x1="3.6068" y1="1.8796" x2="3.6068" y2="1.6256" width="0" layer="51"/>
<wire x1="3.6068" y1="1.6256" x2="4.5974" y2="1.6256" width="0" layer="51"/>
<wire x1="4.5974" y1="1.6256" x2="4.5974" y2="1.8796" width="0" layer="51"/>
<wire x1="4.5974" y1="1.8796" x2="3.6068" y2="1.8796" width="0" layer="51"/>
<wire x1="3.6068" y1="2.3876" x2="3.6068" y2="2.1082" width="0" layer="51"/>
<wire x1="3.6068" y1="2.1082" x2="4.5974" y2="2.1082" width="0" layer="51"/>
<wire x1="4.5974" y1="2.1082" x2="4.5974" y2="2.3876" width="0" layer="51"/>
<wire x1="4.5974" y1="2.3876" x2="3.6068" y2="2.3876" width="0" layer="51"/>
<wire x1="3.6068" y1="2.8956" x2="3.6068" y2="2.6162" width="0" layer="51"/>
<wire x1="3.6068" y1="2.6162" x2="4.5974" y2="2.6162" width="0" layer="51"/>
<wire x1="4.5974" y1="2.6162" x2="4.5974" y2="2.8956" width="0" layer="51"/>
<wire x1="4.5974" y1="2.8956" x2="3.6068" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3368" x2="-2.3368" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="-3.6068" x2="3.6068" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="-3.6068" x2="3.6068" y2="3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="3.6068" x2="-3.6068" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="3.6068" x2="-3.6068" y2="-3.6068" width="0" layer="51"/>
<text x="-6.1722" y="2.7432" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-9.525" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TLK100PHP">
<pin name="VDD33_VA11" x="-38.1" y="33.02" length="middle" direction="pwr"/>
<pin name="VDD33_IO_2" x="-38.1" y="30.48" length="middle" direction="pwr"/>
<pin name="VDD33_VD11" x="-38.1" y="27.94" length="middle" direction="pwr"/>
<pin name="VDD33_IO" x="-38.1" y="25.4" length="middle" direction="pwr"/>
<pin name="VDD33_V18" x="-38.1" y="22.86" length="middle" direction="pwr"/>
<pin name="VA11_PFBIN1" x="-38.1" y="17.78" length="middle" direction="in"/>
<pin name="VA11_PFBIN2" x="-38.1" y="15.24" length="middle" direction="in"/>
<pin name="MII_TXD_0" x="-38.1" y="10.16" length="middle" direction="in"/>
<pin name="MII_TXD_1" x="-38.1" y="7.62" length="middle" direction="in"/>
<pin name="MII_TXD_2" x="-38.1" y="5.08" length="middle" direction="in"/>
<pin name="MII_TXD_3" x="-38.1" y="2.54" length="middle" direction="in"/>
<pin name="MII_TX_EN" x="-38.1" y="0" length="middle" direction="in"/>
<pin name="PWRDNN/INT" x="-38.1" y="-2.54" length="middle" direction="in"/>
<pin name="RESETN" x="-38.1" y="-5.08" length="middle" direction="in"/>
<pin name="JTAG_TCK" x="-38.1" y="-7.62" length="middle" direction="in"/>
<pin name="JTAG_TDI" x="-38.1" y="-10.16" length="middle" direction="in"/>
<pin name="JTAG_TMS" x="-38.1" y="-12.7" length="middle" direction="in"/>
<pin name="JTAG_TRSTN" x="-38.1" y="-15.24" length="middle" direction="in"/>
<pin name="RBIAS" x="-38.1" y="-17.78" length="middle" direction="in"/>
<pin name="MDC" x="-38.1" y="-20.32" length="middle" direction="in"/>
<pin name="XI" x="-38.1" y="-22.86" length="middle" direction="in"/>
<pin name="V18_PFBIN1" x="-38.1" y="-27.94" length="middle" direction="in"/>
<pin name="V18_PFBIN2" x="-38.1" y="-30.48" length="middle" direction="in"/>
<pin name="VSS" x="-38.1" y="-35.56" length="middle" direction="pwr"/>
<pin name="EP" x="-38.1" y="-38.1" length="middle" direction="pas"/>
<pin name="VA11_PFBOUT" x="38.1" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="CLK25OUT" x="38.1" y="30.48" length="middle" direction="out" rot="R180"/>
<pin name="MII_TX_CLK" x="38.1" y="27.94" length="middle" direction="out" rot="R180"/>
<pin name="MII_COL/PHYAD0" x="38.1" y="22.86" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_0/PHYAD1" x="38.1" y="20.32" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_1/PHYAD2" x="38.1" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_2/PHYAD3" x="38.1" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_3/PHYAD4" x="38.1" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="LED_LINK/AN_0" x="38.1" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="LED_SPEED/AN_1" x="38.1" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="XO" x="38.1" y="0" length="middle" direction="out" rot="R180"/>
<pin name="V18_PFBOUT" x="38.1" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="JTAG_TDO" x="38.1" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDD11" x="38.1" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="MII_CRS/LED_CFG" x="38.1" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_CLK" x="38.1" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_DV" x="38.1" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_ERR/MDIX_EN" x="38.1" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="LED_ACT/AN_EN" x="38.1" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="RD-" x="38.1" y="-25.4" length="middle" rot="R180"/>
<pin name="RD+" x="38.1" y="-27.94" length="middle" rot="R180"/>
<pin name="TD-" x="38.1" y="-30.48" length="middle" rot="R180"/>
<pin name="TD+" x="38.1" y="-33.02" length="middle" rot="R180"/>
<pin name="MDIO" x="38.1" y="-35.56" length="middle" rot="R180"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="-33.02" y1="-43.18" x2="33.02" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="33.02" y1="-43.18" x2="33.02" y2="38.1" width="0.4064" layer="94"/>
<wire x1="33.02" y1="38.1" x2="-33.02" y2="38.1" width="0.4064" layer="94"/>
<text x="-7.6454" y="39.7764" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.9182" y="-46.5074" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TLK100PHP" prefix="U">
<description>Ethernet Physical Layer Transceiver</description>
<gates>
<gate name="A" symbol="TLK100PHP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P900X900X120-49N">
<connects>
<connect gate="A" pin="CLK25OUT" pad="12"/>
<connect gate="A" pin="EP" pad="49"/>
<connect gate="A" pin="JTAG_TCK" pad="44"/>
<connect gate="A" pin="JTAG_TDI" pad="45"/>
<connect gate="A" pin="JTAG_TDO" pad="47"/>
<connect gate="A" pin="JTAG_TMS" pad="46"/>
<connect gate="A" pin="JTAG_TRSTN" pad="48"/>
<connect gate="A" pin="LED_ACT/AN_EN" pad="34"/>
<connect gate="A" pin="LED_LINK/AN_0" pad="36"/>
<connect gate="A" pin="LED_SPEED/AN_1" pad="35"/>
<connect gate="A" pin="MDC" pad="32"/>
<connect gate="A" pin="MDIO" pad="33"/>
<connect gate="A" pin="MII_COL/PHYAD0" pad="24"/>
<connect gate="A" pin="MII_CRS/LED_CFG" pad="22"/>
<connect gate="A" pin="MII_RXD_0/PHYAD1" pad="25"/>
<connect gate="A" pin="MII_RXD_1/PHYAD2" pad="26"/>
<connect gate="A" pin="MII_RXD_2/PHYAD3" pad="27"/>
<connect gate="A" pin="MII_RXD_3/PHYAD4" pad="28"/>
<connect gate="A" pin="MII_RX_CLK" pad="23"/>
<connect gate="A" pin="MII_RX_DV" pad="30"/>
<connect gate="A" pin="MII_RX_ERR/MDIX_EN" pad="31"/>
<connect gate="A" pin="MII_TXD_0" pad="13"/>
<connect gate="A" pin="MII_TXD_1" pad="14"/>
<connect gate="A" pin="MII_TXD_2" pad="15"/>
<connect gate="A" pin="MII_TXD_3" pad="16"/>
<connect gate="A" pin="MII_TX_CLK" pad="19"/>
<connect gate="A" pin="MII_TX_EN" pad="18"/>
<connect gate="A" pin="PWRDNN/INT" pad="42"/>
<connect gate="A" pin="RBIAS" pad="3"/>
<connect gate="A" pin="RD+" pad="6"/>
<connect gate="A" pin="RD-" pad="5"/>
<connect gate="A" pin="RESETN" pad="43"/>
<connect gate="A" pin="TD+" pad="9"/>
<connect gate="A" pin="TD-" pad="8"/>
<connect gate="A" pin="V18_PFBIN1" pad="2"/>
<connect gate="A" pin="V18_PFBIN2" pad="4"/>
<connect gate="A" pin="V18_PFBOUT" pad="40"/>
<connect gate="A" pin="VA11_PFBIN1" pad="1"/>
<connect gate="A" pin="VA11_PFBIN2" pad="7"/>
<connect gate="A" pin="VA11_PFBOUT" pad="10"/>
<connect gate="A" pin="VDD11" pad="20"/>
<connect gate="A" pin="VDD33_IO" pad="29"/>
<connect gate="A" pin="VDD33_IO_2" pad="17"/>
<connect gate="A" pin="VDD33_V18" pad="41"/>
<connect gate="A" pin="VDD33_VA11" pad="11"/>
<connect gate="A" pin="VDD33_VD11" pad="21"/>
<connect gate="A" pin="VSS" pad="38"/>
<connect gate="A" pin="XI" pad="39"/>
<connect gate="A" pin="XO" pad="37"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="TLK100PHP" constant="no"/>
<attribute name="OC_FARNELL" value="-" constant="no"/>
<attribute name="OC_NEWARK" value="26R5216" constant="no"/>
<attribute name="PACKAGE" value="HTQFP-48" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-pulse">
<description>&lt;b&gt;Pulse Engineering, Inc.&lt;/b&gt;&lt;p&gt;
www.pulseeng.com
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="J1">
<description>&lt;b&gt;PULSEJACK (TM) 1x1 Tab-UP RJ45&lt;/b&gt;&lt;p&gt;
10/100 Base-TX RJ45 1x1 Tab-UP with LEDs 8-pin (J1 series) and 6-pin (JP series) integrated magnetics connector,&lt;br&gt;
designed to support applications, such as ADSL modems, LAN-on-Motherboard, and Hub and Switches.&lt;br&gt;
Source: www.pulseeng.com .. PulseJack-J402.pdf</description>
<wire x1="-8.405" y1="-17.05" x2="8.405" y2="-17.05" width="0.2032" layer="21"/>
<wire x1="8.405" y1="-17.05" x2="8.405" y2="8.15" width="0.2032" layer="51"/>
<wire x1="-8.405" y1="8.15" x2="-8.405" y2="-17.05" width="0.2032" layer="51"/>
<wire x1="-8.405" y1="8.15" x2="8.405" y2="8.15" width="0.2032" layer="21"/>
<wire x1="-8.3851" y1="-16.9799" x2="-8.8044" y2="-16.316" width="0.2032" layer="21"/>
<wire x1="-8.8044" y1="-16.316" x2="-10.2718" y2="-10.3067" width="0.2032" layer="21" curve="-37.105316"/>
<wire x1="-10.2718" y1="-10.3067" x2="-10.5862" y2="-9.2586" width="0.2032" layer="21" curve="43.052296"/>
<wire x1="-10.5862" y1="-9.2586" x2="-10.4814" y2="-9.1538" width="0.2032" layer="21" curve="-166.504203"/>
<wire x1="-10.4814" y1="-9.1538" x2="-10.3766" y2="-9.2935" width="0.2032" layer="21"/>
<wire x1="-10.3766" y1="-9.2935" x2="-10.132" y2="-9.9923" width="0.2032" layer="21" curve="-35.168494"/>
<wire x1="-10.132" y1="-9.9923" x2="-10.132" y2="-10.726" width="0.2032" layer="21" curve="-3.414373"/>
<wire x1="-10.132" y1="-10.726" x2="-8.455" y2="-16.5606" width="0.2032" layer="21" curve="35.485558"/>
<wire x1="8.3851" y1="-16.9799" x2="8.8044" y2="-16.316" width="0.2032" layer="21"/>
<wire x1="8.8044" y1="-16.316" x2="10.2718" y2="-10.3067" width="0.2032" layer="21" curve="37.105316"/>
<wire x1="10.2718" y1="-10.3067" x2="10.5862" y2="-9.2586" width="0.2032" layer="21" curve="-43.052296"/>
<wire x1="10.5862" y1="-9.2586" x2="10.4814" y2="-9.1538" width="0.2032" layer="21" curve="166.504203"/>
<wire x1="10.4814" y1="-9.1538" x2="10.3766" y2="-9.2935" width="0.2032" layer="21"/>
<wire x1="10.3766" y1="-9.2935" x2="10.132" y2="-9.9923" width="0.2032" layer="21" curve="35.168494"/>
<wire x1="10.132" y1="-9.9923" x2="10.132" y2="-10.726" width="0.2032" layer="21" curve="3.414373"/>
<wire x1="10.132" y1="-10.726" x2="8.455" y2="-16.5606" width="0.2032" layer="21" curve="-35.485558"/>
<wire x1="-8.405" y1="8.15" x2="-8.405" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-8.405" y1="-5.08" x2="-8.405" y2="-17.05" width="0.2032" layer="21"/>
<wire x1="8.405" y1="-17.05" x2="8.405" y2="-5.08" width="0.2032" layer="21"/>
<wire x1="8.405" y1="-1.27" x2="8.405" y2="8.15" width="0.2032" layer="21"/>
<wire x1="-6.8922" y1="-16.9332" x2="-6.5774" y2="-8.8515" width="0.2032" layer="21"/>
<wire x1="-6.5774" y1="-8.8515" x2="-6.2975" y2="-8.5716" width="0.2032" layer="21" curve="-85.555511"/>
<wire x1="-6.3675" y1="-16.9332" x2="-6.1575" y2="-9.8661" width="0.2032" layer="21"/>
<wire x1="-6.1575" y1="-9.8661" x2="-4.6531" y2="-9.8661" width="0.2032" layer="21" curve="-176.58855"/>
<wire x1="-4.6531" y1="-9.8661" x2="-4.4082" y2="-16.9682" width="0.2032" layer="21"/>
<wire x1="-3.8834" y1="-16.9332" x2="-4.1982" y2="-8.8515" width="0.2032" layer="21"/>
<wire x1="-4.1982" y1="-8.8515" x2="-4.4781" y2="-8.5716" width="0.2032" layer="21" curve="85.555511"/>
<wire x1="-6.2975" y1="-8.5716" x2="-4.4781" y2="-8.5716" width="0.2032" layer="21"/>
<wire x1="6.8922" y1="-16.9332" x2="6.5774" y2="-8.8515" width="0.2032" layer="21"/>
<wire x1="6.5774" y1="-8.8515" x2="6.2975" y2="-8.5716" width="0.2032" layer="21" curve="85.555511"/>
<wire x1="6.3675" y1="-16.9332" x2="6.1575" y2="-9.8661" width="0.2032" layer="21"/>
<wire x1="6.1575" y1="-9.8661" x2="4.6531" y2="-9.8661" width="0.2032" layer="21" curve="176.58855"/>
<wire x1="4.6531" y1="-9.8661" x2="4.4082" y2="-16.9682" width="0.2032" layer="21"/>
<wire x1="3.8834" y1="-16.9332" x2="4.1982" y2="-8.8515" width="0.2032" layer="21"/>
<wire x1="4.1982" y1="-8.8515" x2="4.4781" y2="-8.5716" width="0.2032" layer="21" curve="-85.555511"/>
<wire x1="6.2975" y1="-8.5716" x2="4.4781" y2="-8.5716" width="0.2032" layer="21"/>
<pad name="1" x="4.445" y="2.54" drill="0.9" diameter="1.3"/>
<pad name="2" x="3.175" y="0" drill="0.9" diameter="1.3"/>
<pad name="3" x="1.905" y="2.54" drill="0.9" diameter="1.3"/>
<pad name="4" x="0.635" y="0" drill="0.9" diameter="1.3"/>
<pad name="5" x="-0.635" y="2.54" drill="0.9" diameter="1.3"/>
<pad name="6" x="-1.905" y="0" drill="0.9" diameter="1.3"/>
<pad name="7" x="-3.175" y="2.54" drill="0.9" diameter="1.3"/>
<pad name="8" x="-4.445" y="0" drill="0.9" diameter="1.3"/>
<pad name="M1" x="-7.875" y="-3.3" drill="1.6" diameter="2.1844"/>
<pad name="M2" x="7.875" y="-3.3" drill="1.6" diameter="2.1844"/>
<pad name="9" x="5.465" y="7.37" drill="1" diameter="1.4224"/>
<pad name="12" x="-5.465" y="7.37" drill="1" diameter="1.4224"/>
<pad name="10" x="2.925" y="7.37" drill="1" diameter="1.4224"/>
<pad name="11" x="-2.925" y="7.37" drill="1" diameter="1.4224"/>
<text x="-7.62" y="8.89" size="1.27" layer="25">&gt;NAME</text>
<text x="-6.35" y="4.445" size="1.27" layer="27">&gt;VALUE</text>
<hole x="5.715" y="-6.35" drill="3.3"/>
<hole x="-5.715" y="-6.35" drill="3.3"/>
</package>
</packages>
<symbols>
<symbol name="J1011F">
<wire x1="21.59" y1="-39.37" x2="20.32" y2="-41.91" width="0.254" layer="94"/>
<wire x1="20.32" y1="-41.91" x2="19.05" y2="-39.37" width="0.254" layer="94"/>
<wire x1="21.59" y1="-41.91" x2="20.32" y2="-41.91" width="0.254" layer="94"/>
<wire x1="20.32" y1="-41.91" x2="19.05" y2="-41.91" width="0.254" layer="94"/>
<wire x1="21.59" y1="-39.37" x2="20.32" y2="-39.37" width="0.254" layer="94"/>
<wire x1="20.32" y1="-39.37" x2="19.05" y2="-39.37" width="0.254" layer="94"/>
<wire x1="20.32" y1="-39.37" x2="20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="18.288" y1="-40.132" x2="16.891" y2="-41.529" width="0.1524" layer="94"/>
<wire x1="18.415" y1="-41.275" x2="17.018" y2="-42.672" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-43.18" x2="20.32" y2="-41.91" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-38.1" x2="20.32" y2="-38.1" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-38.1" x2="20.32" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-43.18" x2="20.32" y2="-43.18" width="0.1524" layer="94"/>
<wire x1="21.59" y1="-49.53" x2="20.32" y2="-52.07" width="0.254" layer="94"/>
<wire x1="20.32" y1="-52.07" x2="19.05" y2="-49.53" width="0.254" layer="94"/>
<wire x1="21.59" y1="-52.07" x2="20.32" y2="-52.07" width="0.254" layer="94"/>
<wire x1="20.32" y1="-52.07" x2="19.05" y2="-52.07" width="0.254" layer="94"/>
<wire x1="21.59" y1="-49.53" x2="20.32" y2="-49.53" width="0.254" layer="94"/>
<wire x1="20.32" y1="-49.53" x2="19.05" y2="-49.53" width="0.254" layer="94"/>
<wire x1="20.32" y1="-49.53" x2="20.32" y2="-52.07" width="0.1524" layer="94"/>
<wire x1="18.288" y1="-50.292" x2="16.891" y2="-51.689" width="0.1524" layer="94"/>
<wire x1="18.415" y1="-51.435" x2="17.018" y2="-52.832" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-53.34" x2="20.32" y2="-52.07" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-48.26" x2="20.32" y2="-48.26" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-48.26" x2="20.32" y2="-49.53" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="-53.34" x2="20.32" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="10.16" x2="-3.175" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="17.78" x2="-3.175" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="10.16" x2="-3.175" y2="12.7" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="12.7" x2="-3.175" y2="15.24" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="15.24" x2="-3.175" y2="17.78" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="7.62" x2="-3.175" y2="10.16" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="5.08" x2="-3.175" y2="7.62" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="2.54" x2="-3.175" y2="5.08" width="0.1524" layer="94" curve="180"/>
<wire x1="-0.635" y1="16.51" x2="-0.635" y2="3.81" width="0.635" layer="94"/>
<wire x1="0.635" y1="16.51" x2="0.635" y2="3.81" width="0.635" layer="94"/>
<wire x1="3.175" y1="10.16" x2="3.175" y2="12.7" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="12.7" x2="3.175" y2="15.24" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="15.24" x2="3.175" y2="17.78" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="7.62" x2="3.175" y2="10.16" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="5.08" x2="3.175" y2="7.62" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="2.54" x2="3.175" y2="5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="19.05" y1="6.985" x2="16.51" y2="6.985" width="0.1524" layer="94" curve="180"/>
<wire x1="16.51" y1="6.985" x2="13.97" y2="6.985" width="0.1524" layer="94" curve="180"/>
<wire x1="13.97" y1="6.985" x2="11.43" y2="6.985" width="0.1524" layer="94" curve="180"/>
<wire x1="19.05" y1="13.335" x2="16.51" y2="13.335" width="0.1524" layer="94" curve="-180"/>
<wire x1="16.51" y1="13.335" x2="13.97" y2="13.335" width="0.1524" layer="94" curve="-180"/>
<wire x1="13.97" y1="13.335" x2="11.43" y2="13.335" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="17.78" x2="11.43" y2="17.78" width="0.1524" layer="94"/>
<wire x1="11.43" y1="17.78" x2="11.43" y2="13.335" width="0.1524" layer="94"/>
<wire x1="11.43" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="94"/>
<wire x1="11.43" y1="2.54" x2="11.43" y2="6.985" width="0.1524" layer="94"/>
<wire x1="19.05" y1="13.335" x2="19.05" y2="17.78" width="0.1524" layer="94"/>
<wire x1="19.05" y1="17.78" x2="21.59" y2="17.78" width="0.1524" layer="94"/>
<wire x1="19.05" y1="6.985" x2="19.05" y2="2.54" width="0.1524" layer="94"/>
<wire x1="19.05" y1="2.54" x2="21.59" y2="2.54" width="0.1524" layer="94"/>
<wire x1="11.43" y1="9.525" x2="19.05" y2="9.525" width="0.635" layer="94"/>
<wire x1="11.43" y1="10.795" x2="19.05" y2="10.795" width="0.635" layer="94"/>
<wire x1="-5.08" y1="-17.78" x2="-3.175" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-10.16" x2="-3.175" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-10.16" x2="-3.175" y2="-7.62" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="-7.62" x2="-3.175" y2="-5.08" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="-5.08" x2="-3.175" y2="-2.54" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="-12.7" x2="-3.175" y2="-10.16" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="-15.24" x2="-3.175" y2="-12.7" width="0.1524" layer="94" curve="180"/>
<wire x1="-3.175" y1="-17.78" x2="-3.175" y2="-15.24" width="0.1524" layer="94" curve="180"/>
<wire x1="-0.635" y1="-3.81" x2="-0.635" y2="-16.51" width="0.635" layer="94"/>
<wire x1="0.635" y1="-3.81" x2="0.635" y2="-16.51" width="0.635" layer="94"/>
<wire x1="3.175" y1="-10.16" x2="3.175" y2="-7.62" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-7.62" x2="3.175" y2="-5.08" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-5.08" x2="3.175" y2="-2.54" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-12.7" x2="3.175" y2="-10.16" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-15.24" x2="3.175" y2="-12.7" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-17.78" x2="3.175" y2="-15.24" width="0.1524" layer="94" curve="-180"/>
<wire x1="19.05" y1="-13.335" x2="16.51" y2="-13.335" width="0.1524" layer="94" curve="180"/>
<wire x1="16.51" y1="-13.335" x2="13.97" y2="-13.335" width="0.1524" layer="94" curve="180"/>
<wire x1="13.97" y1="-13.335" x2="11.43" y2="-13.335" width="0.1524" layer="94" curve="180"/>
<wire x1="19.05" y1="-6.985" x2="16.51" y2="-6.985" width="0.1524" layer="94" curve="-180"/>
<wire x1="16.51" y1="-6.985" x2="13.97" y2="-6.985" width="0.1524" layer="94" curve="-180"/>
<wire x1="13.97" y1="-6.985" x2="11.43" y2="-6.985" width="0.1524" layer="94" curve="-180"/>
<wire x1="3.175" y1="-2.54" x2="11.43" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-2.54" x2="11.43" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-17.78" x2="3.175" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-17.78" x2="11.43" y2="-13.335" width="0.1524" layer="94"/>
<wire x1="19.05" y1="-6.985" x2="19.05" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="19.05" y1="-2.54" x2="21.59" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="19.05" y1="-13.335" x2="19.05" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="19.05" y1="-17.78" x2="21.59" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-10.795" x2="19.05" y2="-10.795" width="0.635" layer="94"/>
<wire x1="11.43" y1="-9.525" x2="19.05" y2="-9.525" width="0.635" layer="94"/>
<wire x1="21.59" y1="-22.86" x2="20.32" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-22.86" x2="20.32" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="21.59" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="21.59" y1="-30.48" x2="20.32" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-30.48" x2="20.32" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-33.02" x2="21.59" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-25.4" x2="17.526" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="17.526" y1="-33.02" x2="20.32" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-12.7" y1="20.32" x2="-12.7" y2="-55.88" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-55.88" x2="22.86" y2="-55.88" width="0.254" layer="94"/>
<wire x1="-12.7" y1="20.32" x2="22.86" y2="20.32" width="0.254" layer="94"/>
<wire x1="22.86" y1="20.32" x2="22.86" y2="19.05" width="0.254" layer="94"/>
<wire x1="22.86" y1="-55.88" x2="22.86" y2="-34.29" width="0.254" layer="94"/>
<wire x1="22.86" y1="-29.21" x2="22.86" y2="-26.67" width="0.254" layer="94"/>
<wire x1="22.86" y1="-21.59" x2="22.86" y2="-19.05" width="0.254" layer="94"/>
<wire x1="22.86" y1="-16.51" x2="22.86" y2="-3.81" width="0.254" layer="94"/>
<wire x1="22.86" y1="-1.27" x2="22.86" y2="1.27" width="0.254" layer="94"/>
<wire x1="22.86" y1="3.81" x2="22.86" y2="16.51" width="0.254" layer="94"/>
<wire x1="3.175" y1="10.16" x2="8.89" y2="10.16" width="0.1524" layer="94"/>
<wire x1="8.89" y1="10.16" x2="8.89" y2="-20.574" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-10.16" x2="5.08" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-10.16" x2="5.08" y2="-20.574" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-20.574" x2="4.445" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-20.955" x2="5.715" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-21.59" x2="4.445" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-22.225" x2="5.715" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-22.86" x2="4.445" y2="-23.495" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-23.495" x2="5.715" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-24.13" x2="5.08" y2="-24.511" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-20.574" x2="8.255" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="8.255" y1="-20.955" x2="9.525" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="9.525" y1="-21.59" x2="8.255" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="8.255" y1="-22.225" x2="9.525" y2="-22.86" width="0.1524" layer="94"/>
<wire x1="9.525" y1="-22.86" x2="8.255" y2="-23.495" width="0.1524" layer="94"/>
<wire x1="8.255" y1="-23.495" x2="9.525" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="9.525" y1="-24.13" x2="8.89" y2="-24.511" width="0.1524" layer="94"/>
<wire x1="13.589" y1="-25.4" x2="13.97" y2="-26.035" width="0.1524" layer="94"/>
<wire x1="13.97" y1="-26.035" x2="14.605" y2="-24.765" width="0.1524" layer="94"/>
<wire x1="14.605" y1="-24.765" x2="15.24" y2="-26.035" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-26.035" x2="15.875" y2="-24.765" width="0.1524" layer="94"/>
<wire x1="15.875" y1="-24.765" x2="16.51" y2="-26.035" width="0.1524" layer="94"/>
<wire x1="16.51" y1="-26.035" x2="17.145" y2="-24.765" width="0.1524" layer="94"/>
<wire x1="17.145" y1="-24.765" x2="17.526" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="13.589" y1="-33.02" x2="13.97" y2="-33.655" width="0.1524" layer="94"/>
<wire x1="13.97" y1="-33.655" x2="14.605" y2="-32.385" width="0.1524" layer="94"/>
<wire x1="14.605" y1="-32.385" x2="15.24" y2="-33.655" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-33.655" x2="15.875" y2="-32.385" width="0.1524" layer="94"/>
<wire x1="15.875" y1="-32.385" x2="16.51" y2="-33.655" width="0.1524" layer="94"/>
<wire x1="16.51" y1="-33.655" x2="17.145" y2="-32.385" width="0.1524" layer="94"/>
<wire x1="17.145" y1="-32.385" x2="17.526" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-25.4" x2="-3.175" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-25.4" x2="-3.175" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-30.48" x2="-5.08" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-30.48" x2="-3.175" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-33.02" x2="0" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="1.016" y1="-33.02" x2="5.08" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-33.02" x2="8.89" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-33.02" x2="11.43" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-33.02" x2="13.589" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="13.589" y1="-25.4" x2="11.43" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="11.43" y1="-25.4" x2="11.43" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-24.511" x2="8.89" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-24.511" x2="5.08" y2="-33.02" width="0.1524" layer="94"/>
<circle x="-3.175" y="16.51" radius="0.381" width="0" layer="94"/>
<circle x="-3.175" y="8.89" radius="0.381" width="0" layer="94"/>
<circle x="3.175" y="16.51" radius="0.381" width="0" layer="94"/>
<circle x="3.175" y="8.89" radius="0.381" width="0" layer="94"/>
<circle x="12.7" y="6.985" radius="0.381" width="0" layer="94"/>
<circle x="12.7" y="13.335" radius="0.381" width="0" layer="94"/>
<circle x="-3.175" y="-3.81" radius="0.381" width="0" layer="94"/>
<circle x="-3.175" y="-11.43" radius="0.381" width="0" layer="94"/>
<circle x="3.175" y="-3.81" radius="0.381" width="0" layer="94"/>
<circle x="3.175" y="-11.43" radius="0.381" width="0" layer="94"/>
<circle x="12.7" y="-13.335" radius="0.381" width="0" layer="94"/>
<circle x="12.7" y="-6.985" radius="0.381" width="0" layer="94"/>
<circle x="22.225" y="17.78" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="2.54" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-2.54" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-17.78" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-22.86" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-25.4" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-30.48" radius="0.635" width="0.1524" layer="94"/>
<circle x="22.225" y="-33.02" radius="0.635" width="0.1524" layer="94"/>
<circle x="-3.175" y="-30.48" radius="0.381" width="0" layer="94"/>
<circle x="5.08" y="-33.02" radius="0.381" width="0" layer="94"/>
<circle x="8.89" y="-33.02" radius="0.381" width="0" layer="94"/>
<circle x="11.43" y="-33.02" radius="0.381" width="0" layer="94"/>
<circle x="20.32" y="-33.02" radius="0.381" width="0" layer="94"/>
<circle x="20.32" y="-25.4" radius="0.381" width="0" layer="94"/>
<text x="2.54" y="-40.64" size="1.778" layer="94">Right LED</text>
<text x="2.54" y="-50.8" size="1.778" layer="94">Left LED</text>
<text x="24.13" y="17.018" size="1.524" layer="94">1 TX+</text>
<text x="24.13" y="1.778" size="1.524" layer="94">2 TX-</text>
<text x="24.13" y="-3.302" size="1.524" layer="94">3 RX+</text>
<text x="24.13" y="-18.542" size="1.524" layer="94">6 RX-</text>
<text x="24.13" y="-23.622" size="1.524" layer="94">4</text>
<text x="24.13" y="-26.162" size="1.524" layer="94">5</text>
<text x="24.13" y="-31.242" size="1.524" layer="94">7</text>
<text x="24.13" y="-33.782" size="1.524" layer="94">8</text>
<text x="-12.7" y="21.59" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-58.42" size="1.778" layer="96">&gt;VALUE</text>
<text x="3.683" y="-24.003" size="1.016" layer="94" rot="R90">75R</text>
<text x="7.493" y="-24.13" size="1.016" layer="94" rot="R90">75R</text>
<text x="13.97" y="-24.003" size="1.016" layer="94">75R</text>
<text x="13.716" y="-31.75" size="1.016" layer="94">75R</text>
<text x="-0.635" y="-31.115" size="1.016" layer="94">1 nF</text>
<rectangle x1="0" y1="-34.29" x2="0.254" y2="-31.75" layer="94"/>
<rectangle x1="0.762" y1="-34.29" x2="1.016" y2="-31.75" layer="94"/>
<pin name="C-R" x="-15.24" y="-43.18" visible="pad" length="short" direction="pas"/>
<pin name="A-R" x="-15.24" y="-38.1" visible="pad" length="short" direction="pas"/>
<pin name="C-L" x="-15.24" y="-53.34" visible="pad" length="short" direction="pas"/>
<pin name="AL" x="-15.24" y="-48.26" visible="pad" length="short" direction="pas"/>
<pin name="TD+" x="-15.24" y="17.78" length="short" direction="in"/>
<pin name="CT" x="-15.24" y="10.16" length="short" direction="in"/>
<pin name="TD-" x="-15.24" y="2.54" length="short" direction="in"/>
<pin name="RD+" x="-15.24" y="-2.54" length="short" direction="in"/>
<pin name="CR" x="-15.24" y="-10.16" length="short" direction="in"/>
<pin name="RD-" x="-15.24" y="-17.78" length="short" direction="in"/>
<pin name="NC/4" x="-15.24" y="-25.4" length="short" direction="in"/>
<pin name="NC/5" x="-15.24" y="-30.48" length="short" direction="in"/>
<polygon width="0.1524" layer="94">
<vertex x="16.891" y="-41.529"/>
<vertex x="17.272" y="-40.64"/>
<vertex x="17.78" y="-41.148"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="17.018" y="-42.672"/>
<vertex x="17.399" y="-41.783"/>
<vertex x="17.907" y="-42.291"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="16.891" y="-51.689"/>
<vertex x="17.272" y="-50.8"/>
<vertex x="17.78" y="-51.308"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="17.018" y="-52.832"/>
<vertex x="17.399" y="-51.943"/>
<vertex x="17.907" y="-52.451"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="J1011F" prefix="X">
<description>&lt;b&gt;PULSEJACK (TM) 1x1 Tab-UP RJ45&lt;/b&gt;&lt;p&gt;
10/100 Base-TX RJ45 1x1 Tab-UP with LEDs 8-pin (J1 series) and 6-pin (JP series) integrated magnetics connector,&lt;br&gt;
designed to support applications, such as ADSL modems, LAN-on-Motherboard, and Hub and Switches.&lt;br&gt;
Source: www.pulseeng.com .. PulseJack-J402.pdf</description>
<gates>
<gate name="G$1" symbol="J1011F" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="J1">
<connects>
<connect gate="G$1" pin="A-R" pad="9"/>
<connect gate="G$1" pin="AL" pad="12"/>
<connect gate="G$1" pin="C-L" pad="11"/>
<connect gate="G$1" pin="C-R" pad="10"/>
<connect gate="G$1" pin="CR" pad="6"/>
<connect gate="G$1" pin="CT" pad="3"/>
<connect gate="G$1" pin="NC/4" pad="4"/>
<connect gate="G$1" pin="NC/5" pad="5"/>
<connect gate="G$1" pin="RD+" pad="7"/>
<connect gate="G$1" pin="RD-" pad="8"/>
<connect gate="G$1" pin="TD+" pad="1"/>
<connect gate="G$1" pin="TD-" pad="2"/>
</connects>
<technologies>
<technology name="01P">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
<technology name="21P">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="pwrx" width="0" drill="0">
</class>
<class number="2" name="highpwr" width="0" drill="0.8128">
</class>
<class number="3" name="highsig" width="0" drill="0.8128">
<clearance class="3" value="0.254"/>
</class>
<class number="7" name="hipwr" width="0" drill="0">
<clearance class="7" value="0.2032"/>
</class>
</classes>
<parts>
<part name="P+23" library="supply1" deviceset="VCC" device=""/>
<part name="C7" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C5" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C4" library="rcl" deviceset="C-US" device="C0805" value="10uF 6v"/>
<part name="GND40" library="supply1" deviceset="GND" device=""/>
<part name="C8" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C9" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="S0" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S1" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S2" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S3" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="P+1" library="supply1" deviceset="VCC" device=""/>
<part name="P+2" library="supply1" deviceset="VCC" device=""/>
<part name="P+3" library="supply1" deviceset="VCC" device=""/>
<part name="P+4" library="supply1" deviceset="VCC" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="S4" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S5" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S6" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="S7" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device="" value="SSCDANN150PG2A3"/>
<part name="P+5" library="supply1" deviceset="VCC" device=""/>
<part name="P+6" library="supply1" deviceset="VCC" device=""/>
<part name="P+7" library="supply1" deviceset="VCC" device=""/>
<part name="P+8" library="supply1" deviceset="VCC" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="S8" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S9" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S10" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S11" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="P+9" library="supply1" deviceset="VCC" device=""/>
<part name="P+10" library="supply1" deviceset="VCC" device=""/>
<part name="P+11" library="supply1" deviceset="VCC" device=""/>
<part name="P+12" library="supply1" deviceset="VCC" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="S12" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S13" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S14" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="S15" library="honeywell-ssc" deviceset="SSCDANN150PG2A3" device=""/>
<part name="P+13" library="supply1" deviceset="VCC" device=""/>
<part name="P+14" library="supply1" deviceset="VCC" device=""/>
<part name="P+15" library="supply1" deviceset="VCC" device=""/>
<part name="P+16" library="supply1" deviceset="VCC" device=""/>
<part name="GND13" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="GND16" library="supply1" deviceset="GND" device=""/>
<part name="C16" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C17" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C19" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C20" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C18" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="C15" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C2" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="C3" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="JTAG1" library="con-lstb" deviceset="MA05-2" device="" value="67996-110HLF"/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="+3V15" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V56" library="supply1" deviceset="+3V3" device=""/>
<part name="R1" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="C10" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C13" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C14" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="C21" library="rcl" deviceset="C-US" device="C0402" value="1uF 6v"/>
<part name="R5" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+22" library="supply1" deviceset="VCC" device=""/>
<part name="R6" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+24" library="supply1" deviceset="VCC" device=""/>
<part name="R7" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+25" library="supply1" deviceset="VCC" device=""/>
<part name="R8" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+26" library="supply1" deviceset="VCC" device=""/>
<part name="R9" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+27" library="supply1" deviceset="VCC" device=""/>
<part name="R10" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+28" library="supply1" deviceset="VCC" device=""/>
<part name="R11" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+29" library="supply1" deviceset="VCC" device=""/>
<part name="R12" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+30" library="supply1" deviceset="VCC" device=""/>
<part name="R13" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+31" library="supply1" deviceset="VCC" device=""/>
<part name="R14" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+32" library="supply1" deviceset="VCC" device=""/>
<part name="R15" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+33" library="supply1" deviceset="VCC" device=""/>
<part name="R16" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+34" library="supply1" deviceset="VCC" device=""/>
<part name="R17" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+35" library="supply1" deviceset="VCC" device=""/>
<part name="R18" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+36" library="supply1" deviceset="VCC" device=""/>
<part name="R19" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+37" library="supply1" deviceset="VCC" device=""/>
<part name="R20" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="P+38" library="supply1" deviceset="VCC" device=""/>
<part name="R21" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="P+39" library="supply1" deviceset="VCC" device=""/>
<part name="IC3" library="tlb" deviceset="AD824" device="R"/>
<part name="IC12" library="tlb" deviceset="AD5308" device="" value="AD5328ARUZ"/>
<part name="GND22" library="supply1" deviceset="GND" device=""/>
<part name="GND24" library="supply1" deviceset="GND" device=""/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="GND37" library="supply1" deviceset="GND" device=""/>
<part name="GND38" library="supply1" deviceset="GND" device=""/>
<part name="GND42" library="supply1" deviceset="GND" device=""/>
<part name="GND43" library="supply1" deviceset="GND" device=""/>
<part name="GND44" library="supply1" deviceset="GND" device=""/>
<part name="GND45" library="supply1" deviceset="GND" device=""/>
<part name="GND46" library="supply1" deviceset="GND" device=""/>
<part name="IC7" library="infineon" deviceset="BTS428L2" device=""/>
<part name="P+40" library="supply1" deviceset="+24V" device=""/>
<part name="GND47" library="supply1" deviceset="GND" device=""/>
<part name="GND48" library="supply1" deviceset="GND" device=""/>
<part name="GND49" library="supply1" deviceset="GND" device=""/>
<part name="GND50" library="supply1" deviceset="GND" device=""/>
<part name="GND51" library="supply1" deviceset="GND" device=""/>
<part name="Z0" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z1" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z2" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z3" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="RA0" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA1" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA2" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA3" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RB0" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RC0" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RD0" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RE0" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RC1" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RC2" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RB1" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD1" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RB2" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD2" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RE1" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RE2" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RB3" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD3" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RC3" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RE3" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="IC4" library="tlb" deviceset="AD824" device="R"/>
<part name="GND52" library="supply1" deviceset="GND" device=""/>
<part name="GND54" library="supply1" deviceset="GND" device=""/>
<part name="GND55" library="supply1" deviceset="GND" device=""/>
<part name="GND57" library="supply1" deviceset="GND" device=""/>
<part name="GND58" library="supply1" deviceset="GND" device=""/>
<part name="GND59" library="supply1" deviceset="GND" device=""/>
<part name="GND60" library="supply1" deviceset="GND" device=""/>
<part name="GND61" library="supply1" deviceset="GND" device=""/>
<part name="GND62" library="supply1" deviceset="GND" device=""/>
<part name="Z4" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z5" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z6" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="Z7" library="tlb" deviceset="B725" device="90" value="B72590D150H60 23v"/>
<part name="RA4" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA5" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA6" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RA7" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RB4" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RC4" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RD4" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RE4" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RC5" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RC6" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RB5" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD5" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RB6" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD6" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RE5" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RE6" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="RB7" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RD7" library="rcl" deviceset="R-US_" device="R0402" value="47k"/>
<part name="RC7" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="RE7" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND63" library="supply1" deviceset="GND" device=""/>
<part name="GND64" library="supply1" deviceset="GND" device=""/>
<part name="GND65" library="supply1" deviceset="GND" device=""/>
<part name="GND66" library="supply1" deviceset="GND" device=""/>
<part name="P+42" library="supply1" deviceset="+24V" device=""/>
<part name="P+43" library="supply1" deviceset="+24V" device=""/>
<part name="P+45" library="supply1" deviceset="+24V" device=""/>
<part name="C23" library="rcl" deviceset="C-US" device="C0402" value="0.1uF 35v"/>
<part name="GND69" library="supply1" deviceset="GND" device=""/>
<part name="P+49" library="supply1" deviceset="+24V" device=""/>
<part name="C24" library="rcl" deviceset="C-US" device="C0402" value="0.1uF 35v"/>
<part name="GND96" library="supply1" deviceset="GND" device=""/>
<part name="GND68" library="supply1" deviceset="GND" device=""/>
<part name="C27" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C42" library="qualified_1" deviceset="C" device="1206" value="4.7uF 35v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="270.000"/>
<attribute name="DX" value="78.200"/>
<attribute name="DY" value="21.800"/>
<attribute name="MANUF" value="C3216X5R1H475K160AB"/>
<attribute name="SHEETNO" value="1"/>
</part>
<part name="C44" library="qualified_1" deviceset="C" device="0805" value="22uF 6v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="270.000"/>
<attribute name="DX" value="71.300"/>
<attribute name="DY" value="21.800"/>
<attribute name="MANUF" value="C2012X5R0J226M125AC"/>
<attribute name="SHEETNO" value="1"/>
</part>
<part name="SUPPLY49" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY54" library="supply2" deviceset="GND" device=""/>
<part name="P+53" library="supply1" deviceset="+24V" device=""/>
<part name="IC21" library="linear-technology" deviceset="LT3681" device=""/>
<part name="C28" library="rcl" deviceset="C-US" device="C0402" value="0.47uF 25v"/>
<part name="GND70" library="supply1" deviceset="GND" device=""/>
<part name="GND71" library="supply1" deviceset="GND" device=""/>
<part name="GND72" library="supply1" deviceset="GND" device=""/>
<part name="GND73" library="supply1" deviceset="GND" device=""/>
<part name="R22" library="rcl" deviceset="R-US_" device="R0402" value="324k"/>
<part name="R23" library="rcl" deviceset="R-US_" device="R0402" value="200k"/>
<part name="GND88" library="supply1" deviceset="GND" device=""/>
<part name="R24" library="rcl" deviceset="R-US_" device="R0402" value="20k"/>
<part name="C29" library="rcl" deviceset="C-US" device="C0402" value="330pF"/>
<part name="GND89" library="supply1" deviceset="GND" device=""/>
<part name="R25" library="rcl" deviceset="R-US_" device="R0402" value="60k"/>
<part name="L3" library="inductors" deviceset="SLF7045" device="" value="SLF7045 100uH"/>
<part name="XPWR" library="tlb" deviceset="43045-0412" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V5" library="supply1" deviceset="+3V3" device=""/>
<part name="R26" library="rcl" deviceset="R-US_" device="R0402" value="0"/>
<part name="+3V7" library="supply1" deviceset="+3V3" device=""/>
<part name="P+17" library="supply1" deviceset="VCC" device=""/>
<part name="U1" library="Atmel_By_element14_Batch_1" deviceset="AT32UC3A0256-ALUT" device=""/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V16" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V17" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V18" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V19" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V20" library="supply1" deviceset="+3V3" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND77" library="supply1" deviceset="GND" device=""/>
<part name="GND78" library="supply1" deviceset="GND" device=""/>
<part name="GND79" library="supply1" deviceset="GND" device=""/>
<part name="GND80" library="supply1" deviceset="GND" device=""/>
<part name="GND81" library="supply1" deviceset="GND" device=""/>
<part name="GND82" library="supply1" deviceset="GND" device=""/>
<part name="GND83" library="supply1" deviceset="GND" device=""/>
<part name="+3V21" library="supply1" deviceset="+3V3" device=""/>
<part name="VCC4" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC5" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC6" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC7" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC8" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC1" library="supply1" deviceset="VCCINT" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="VCC2" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC3" library="supply1" deviceset="VCCINT" device=""/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="C11" library="rcl" deviceset="C-US" device="C0402" value="4.7uF"/>
<part name="C12" library="rcl" deviceset="C-US" device="C0402" value="1nF"/>
<part name="C31" library="rcl" deviceset="C-US" device="C0402" value="2.2uF"/>
<part name="C32" library="rcl" deviceset="C-US" device="C0402" value="470pF"/>
<part name="GND85" library="supply1" deviceset="GND" device=""/>
<part name="GND87" library="supply1" deviceset="GND" device=""/>
<part name="GND90" library="supply1" deviceset="GND" device=""/>
<part name="GND91" library="supply1" deviceset="GND" device=""/>
<part name="C33" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC9" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND92" library="supply1" deviceset="GND" device=""/>
<part name="C34" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC10" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND93" library="supply1" deviceset="GND" device=""/>
<part name="C35" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC11" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND94" library="supply1" deviceset="GND" device=""/>
<part name="C36" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC12" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND95" library="supply1" deviceset="GND" device=""/>
<part name="+3V12" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V13" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V14" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V22" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V23" library="supply1" deviceset="+3V3" device=""/>
<part name="GND99" library="supply1" deviceset="GND" device=""/>
<part name="GND100" library="supply1" deviceset="GND" device=""/>
<part name="GND101" library="supply1" deviceset="GND" device=""/>
<part name="GND102" library="supply1" deviceset="GND" device=""/>
<part name="GND103" library="supply1" deviceset="GND" device=""/>
<part name="GND104" library="supply1" deviceset="GND" device=""/>
<part name="C52" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC13" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND156" library="supply1" deviceset="GND" device=""/>
<part name="IC9" library="qualified_1" deviceset="MIC5219" device="YM5" value="MIC5247-2.0v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="180.000"/>
<attribute name="DX" value="70.050"/>
<attribute name="DY" value="14.150"/>
<attribute name="SHEETNO" value="5"/>
</part>
<part name="SUPPLY15" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY42" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY56" library="supply2" deviceset="GND" device=""/>
<part name="+3V27" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V28" library="supply1" deviceset="+3V3" device=""/>
<part name="C1" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V25" library="supply1" deviceset="+3V3" device=""/>
<part name="GND149" library="supply1" deviceset="GND" device=""/>
<part name="E0" library="tlb" deviceset="43045-0412" device=""/>
<part name="E1" library="tlb" deviceset="43045-0412" device=""/>
<part name="E2" library="tlb" deviceset="43045-0412" device=""/>
<part name="E3" library="tlb" deviceset="43045-0412" device=""/>
<part name="E4" library="tlb" deviceset="43045-0412" device=""/>
<part name="E5" library="tlb" deviceset="43045-0412" device=""/>
<part name="GND150" library="supply1" deviceset="GND" device=""/>
<part name="U19" library="tlb" deviceset="LTC1799" device="S5" value="LTC1799CS5"/>
<part name="GND151" library="supply1" deviceset="GND" device=""/>
<part name="RO1" library="rcl" deviceset="R-US_" device="R0402" value="10k 1%"/>
<part name="GND152" library="supply1" deviceset="GND" device=""/>
<part name="U6" library="tlb" deviceset="LS7266R1-S" device=""/>
<part name="GND153" library="supply1" deviceset="GND" device=""/>
<part name="+3V26" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V29" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V30" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V31" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V32" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V37" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V38" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V58" library="supply1" deviceset="+3V3" device=""/>
<part name="R43" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="U2" library="tlb" deviceset="LS7266R1-S" device=""/>
<part name="GND154" library="supply1" deviceset="GND" device=""/>
<part name="+3V33" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V34" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V35" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V36" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V39" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V40" library="supply1" deviceset="+3V3" device=""/>
<part name="R44" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="U3" library="tlb" deviceset="LS7266R1-S" device=""/>
<part name="GND157" library="supply1" deviceset="GND" device=""/>
<part name="+3V41" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V42" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V43" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V44" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V45" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V46" library="supply1" deviceset="+3V3" device=""/>
<part name="R45" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R49" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R50" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="P0" library="tlb" deviceset="43045-0612" device=""/>
<part name="P1" library="tlb" deviceset="43045-0612" device=""/>
<part name="P2" library="tlb" deviceset="43045-0612" device=""/>
<part name="P3" library="tlb" deviceset="43045-0612" device=""/>
<part name="GND161" library="supply1" deviceset="GND" device=""/>
<part name="R51" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R52" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND162" library="supply1" deviceset="GND" device=""/>
<part name="R53" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R54" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND163" library="supply1" deviceset="GND" device=""/>
<part name="R55" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R56" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND164" library="supply1" deviceset="GND" device=""/>
<part name="R57" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R58" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND165" library="supply1" deviceset="GND" device=""/>
<part name="R59" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R60" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="GND172" library="supply1" deviceset="GND" device=""/>
<part name="GND173" library="supply1" deviceset="GND" device=""/>
<part name="GND174" library="supply1" deviceset="GND" device=""/>
<part name="IC17" library="tlb" deviceset="ADS8344" device="E"/>
<part name="GND175" library="supply1" deviceset="GND" device=""/>
<part name="C39" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V67" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V68" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V69" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V70" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V71" library="supply1" deviceset="+3V3" device=""/>
<part name="R73" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R74" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R75" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R76" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R77" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R78" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R79" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="R80" library="rcl" deviceset="R-US_" device="R0402" value="220"/>
<part name="GND176" library="supply1" deviceset="GND" device=""/>
<part name="GND177" library="supply1" deviceset="GND" device=""/>
<part name="GND178" library="supply1" deviceset="GND" device=""/>
<part name="GND179" library="supply1" deviceset="GND" device=""/>
<part name="GND180" library="supply1" deviceset="GND" device=""/>
<part name="GND181" library="supply1" deviceset="GND" device=""/>
<part name="GND182" library="supply1" deviceset="GND" device=""/>
<part name="GND183" library="supply1" deviceset="GND" device=""/>
<part name="+3V72" library="supply1" deviceset="+3V3" device=""/>
<part name="R81" library="rcl" deviceset="R-US_" device="R0402" value="0"/>
<part name="C40" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND184" library="supply1" deviceset="GND" device=""/>
<part name="+3V75" library="supply1" deviceset="+3V3" device=""/>
<part name="C54" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND187" library="supply1" deviceset="GND" device=""/>
<part name="+3V76" library="supply1" deviceset="+3V3" device=""/>
<part name="C55" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND188" library="supply1" deviceset="GND" device=""/>
<part name="+3V77" library="supply1" deviceset="+3V3" device=""/>
<part name="C56" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND189" library="supply1" deviceset="GND" device=""/>
<part name="+3V78" library="supply1" deviceset="+3V3" device=""/>
<part name="C57" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND190" library="supply1" deviceset="GND" device=""/>
<part name="C58" library="qualified_1" deviceset="C" device="1206" value="4.7uF 35v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="270.000"/>
<attribute name="DX" value="78.200"/>
<attribute name="DY" value="21.800"/>
<attribute name="MANUF" value="C3216X5R1H475K160AB"/>
<attribute name="SHEETNO" value="1"/>
</part>
<part name="C59" library="qualified_1" deviceset="C" device="0805" value="22uF 6v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="270.000"/>
<attribute name="DX" value="71.300"/>
<attribute name="DY" value="21.800"/>
<attribute name="MANUF" value="C2012X5R0J226M125AC"/>
<attribute name="SHEETNO" value="1"/>
</part>
<part name="SUPPLY1" library="supply2" deviceset="GND" device=""/>
<part name="SUPPLY2" library="supply2" deviceset="GND" device=""/>
<part name="P+83" library="supply1" deviceset="+24V" device=""/>
<part name="IC10" library="linear-technology" deviceset="LT3681" device=""/>
<part name="C60" library="rcl" deviceset="C-US" device="C0402" value="0.47uF 25v"/>
<part name="GND191" library="supply1" deviceset="GND" device=""/>
<part name="GND192" library="supply1" deviceset="GND" device=""/>
<part name="GND193" library="supply1" deviceset="GND" device=""/>
<part name="GND194" library="supply1" deviceset="GND" device=""/>
<part name="R82" library="rcl" deviceset="R-US_" device="R0402" value="590k"/>
<part name="R83" library="rcl" deviceset="R-US_" device="R0402" value="200k"/>
<part name="GND195" library="supply1" deviceset="GND" device=""/>
<part name="R84" library="rcl" deviceset="R-US_" device="R0402" value="20k"/>
<part name="C61" library="rcl" deviceset="C-US" device="C0402" value="330pF"/>
<part name="GND196" library="supply1" deviceset="GND" device=""/>
<part name="R85" library="rcl" deviceset="R-US_" device="R0402" value="60k"/>
<part name="L1" library="inductors" deviceset="SLF7045" device="" value="SLF7045 100uH"/>
<part name="P+84" library="supply1" deviceset="+5V" device=""/>
<part name="P+85" library="supply1" deviceset="+5V" device=""/>
<part name="R86" library="rcl" deviceset="R-US_" device="R0402" value="0"/>
<part name="GND197" library="supply1" deviceset="GND" device=""/>
<part name="Z16" library="tlb" deviceset="B725" device="90"/>
<part name="GND198" library="supply1" deviceset="GND" device=""/>
<part name="Z17" library="tlb" deviceset="B725" device="90"/>
<part name="GND199" library="supply1" deviceset="GND" device=""/>
<part name="Z18" library="tlb" deviceset="B725" device="90"/>
<part name="GND200" library="supply1" deviceset="GND" device=""/>
<part name="Z19" library="tlb" deviceset="B725" device="90"/>
<part name="GND201" library="supply1" deviceset="GND" device=""/>
<part name="Z20" library="tlb" deviceset="B725" device="90"/>
<part name="GND202" library="supply1" deviceset="GND" device=""/>
<part name="Z21" library="tlb" deviceset="B725" device="90"/>
<part name="GND203" library="supply1" deviceset="GND" device=""/>
<part name="Z22" library="tlb" deviceset="B725" device="90"/>
<part name="GND204" library="supply1" deviceset="GND" device=""/>
<part name="Z23" library="tlb" deviceset="B725" device="90"/>
<part name="U8" library="Texas Instruments_By_element14_Batch_1" deviceset="TLK100PHP" device=""/>
<part name="+3V80" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V81" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V82" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V83" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V84" library="supply1" deviceset="+3V3" device=""/>
<part name="C62" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="C63" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C64" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND205" library="supply1" deviceset="GND" device=""/>
<part name="GND206" library="supply1" deviceset="GND" device=""/>
<part name="GND207" library="supply1" deviceset="GND" device=""/>
<part name="C65" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="C66" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C67" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND208" library="supply1" deviceset="GND" device=""/>
<part name="GND209" library="supply1" deviceset="GND" device=""/>
<part name="GND210" library="supply1" deviceset="GND" device=""/>
<part name="C68" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="GND211" library="supply1" deviceset="GND" device=""/>
<part name="X2" library="con-pulse" deviceset="J1011F" device="" technology="01P"/>
<part name="GND212" library="supply1" deviceset="GND" device=""/>
<part name="GND213" library="supply1" deviceset="GND" device=""/>
<part name="R88" library="rcl" deviceset="R-US_" device="R0402" value="4.99k 1%"/>
<part name="GND214" library="supply1" deviceset="GND" device=""/>
<part name="R89" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R90" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R91" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R92" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="+3V85" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V86" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V87" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V88" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V89" library="supply1" deviceset="+3V3" device=""/>
<part name="C69" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND215" library="supply1" deviceset="GND" device=""/>
<part name="+3V90" library="supply1" deviceset="+3V3" device=""/>
<part name="C70" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND216" library="supply1" deviceset="GND" device=""/>
<part name="+3V91" library="supply1" deviceset="+3V3" device=""/>
<part name="C71" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND217" library="supply1" deviceset="GND" device=""/>
<part name="+3V92" library="supply1" deviceset="+3V3" device=""/>
<part name="C72" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND218" library="supply1" deviceset="GND" device=""/>
<part name="GND219" library="supply1" deviceset="GND" device=""/>
<part name="GND220" library="supply1" deviceset="GND" device=""/>
<part name="+3V93" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V94" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V95" library="supply1" deviceset="+3V3" device=""/>
<part name="C73" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND221" library="supply1" deviceset="GND" device=""/>
<part name="+3V96" library="supply1" deviceset="+3V3" device=""/>
<part name="C74" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND222" library="supply1" deviceset="GND" device=""/>
<part name="+3V97" library="supply1" deviceset="+3V3" device=""/>
<part name="C75" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND223" library="supply1" deviceset="GND" device=""/>
<part name="+3V98" library="supply1" deviceset="+3V3" device=""/>
<part name="C76" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND224" library="supply1" deviceset="GND" device=""/>
<part name="+3V99" library="supply1" deviceset="+3V3" device=""/>
<part name="C77" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND225" library="supply1" deviceset="GND" device=""/>
<part name="R93" library="rcl" deviceset="R-US_" device="R0402" value="470"/>
<part name="R94" library="rcl" deviceset="R-US_" device="R0402" value="470"/>
<part name="+3V100" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V101" library="supply1" deviceset="+3V3" device=""/>
<part name="R95" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R96" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="GND227" library="supply1" deviceset="GND" device=""/>
<part name="R97" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="+3V102" library="supply1" deviceset="+3V3" device=""/>
<part name="R98" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R99" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R100" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="+3V103" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V104" library="supply1" deviceset="+3V3" device=""/>
<part name="GND228" library="supply1" deviceset="GND" device=""/>
<part name="+3V105" library="supply1" deviceset="+3V3" device=""/>
<part name="Q1" library="crystal" deviceset="CRYSTAL" device="CTS406" value="406C35D25M00000"/>
<part name="C78" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="C79" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="GND226" library="supply1" deviceset="GND" device=""/>
<part name="GND229" library="supply1" deviceset="GND" device=""/>
<part name="Q2" library="crystal" deviceset="CRYSTAL" device="CTS406" value="406C35D12M00000"/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="+3V110" library="supply1" deviceset="+3V3" device=""/>
<part name="R4" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="+3V112" library="supply1" deviceset="+3V3" device=""/>
<part name="R101" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R102" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="R103" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="+3V114" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V115" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V116" library="supply1" deviceset="+3V3" device=""/>
<part name="C81" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C82" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND84" library="supply1" deviceset="GND" device=""/>
<part name="GND233" library="supply1" deviceset="GND" device=""/>
<part name="FB25" library="qualified_1" deviceset="FB" device="0805" value="BKP1005HS121"/>
<part name="FB26" library="qualified_1" deviceset="FB" device="0805" value="BKP1005HS121"/>
<part name="C247" library="qualified_1" deviceset="C" device="1210" value="10uF 35v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="90.000"/>
<attribute name="DX" value="37.600"/>
<attribute name="DY" value="32.600"/>
<attribute name="SHEETNO" value="7"/>
</part>
<part name="C250" library="qualified_1" deviceset="C" device="1210" value="10uF 35v">
<attribute name="DM" value="0"/>
<attribute name="DR" value="90.000"/>
<attribute name="DX" value="37.600"/>
<attribute name="DY" value="32.600"/>
<attribute name="SHEETNO" value="7"/>
</part>
<part name="P+55" library="supply1" deviceset="+24V" device=""/>
<part name="GND234" library="supply1" deviceset="GND" device=""/>
<part name="XV0" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV1" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV2" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV3" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV4" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV5" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV6" library="tlb" deviceset="43045-0412" device=""/>
<part name="XV7" library="tlb" deviceset="43045-0412" device=""/>
<part name="C22" library="rcl" deviceset="C-US" device="C0402" value="1uF"/>
<part name="C25" library="rcl" deviceset="C-US" device="C0402" value="1uF"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-127" y="162.56" size="1.778" layer="97">at vregin</text>
<text x="-101.6" y="162.56" size="1.778" layer="97">at vregout</text>
<text x="-35.56" y="160.02" size="1.778" layer="97">around at32</text>
</plain>
<instances>
<instance part="C16" gate="G$1" x="-30.48" y="152.4"/>
<instance part="C17" gate="G$1" x="-17.78" y="152.4"/>
<instance part="C19" gate="G$1" x="7.62" y="152.4"/>
<instance part="C20" gate="G$1" x="20.32" y="152.4"/>
<instance part="C18" gate="G$1" x="-5.08" y="152.4"/>
<instance part="+3V6" gate="G$1" x="-43.18" y="157.48"/>
<instance part="C15" gate="G$1" x="-43.18" y="152.4"/>
<instance part="C2" gate="G$1" x="10.16" y="-93.98"/>
<instance part="C3" gate="G$1" x="25.4" y="-93.98"/>
<instance part="GND28" gate="1" x="17.78" y="-101.6"/>
<instance part="C6" gate="G$1" x="-152.4" y="86.36"/>
<instance part="GND53" gate="1" x="-152.4" y="76.2"/>
<instance part="JTAG1" gate="G$1" x="-175.26" y="-45.72"/>
<instance part="GND39" gate="1" x="-182.88" y="-55.88"/>
<instance part="GND41" gate="1" x="-182.88" y="-35.56" rot="R180"/>
<instance part="+3V15" gate="G$1" x="-198.12" y="-48.26" rot="R90"/>
<instance part="+3V56" gate="G$1" x="-152.4" y="106.68"/>
<instance part="R1" gate="G$1" x="-152.4" y="96.52" rot="R90"/>
<instance part="+3V8" gate="G$1" x="-93.98" y="114.3" rot="R90"/>
<instance part="+3V16" gate="G$1" x="-93.98" y="104.14" rot="R90"/>
<instance part="+3V17" gate="G$1" x="-93.98" y="96.52" rot="R90"/>
<instance part="+3V18" gate="G$1" x="-96.52" y="-25.4" rot="R90"/>
<instance part="+3V19" gate="G$1" x="-96.52" y="-27.94" rot="R90"/>
<instance part="+3V20" gate="G$1" x="-96.52" y="-30.48" rot="R90"/>
<instance part="GND17" gate="1" x="-73.66" y="-119.38" rot="R270"/>
<instance part="GND25" gate="1" x="-73.66" y="-121.92" rot="R270"/>
<instance part="GND26" gate="1" x="-73.66" y="-124.46" rot="R270"/>
<instance part="GND77" gate="1" x="-73.66" y="-127" rot="R270"/>
<instance part="GND78" gate="1" x="-78.74" y="5.08" rot="R270"/>
<instance part="GND79" gate="1" x="-78.74" y="7.62" rot="R270"/>
<instance part="GND80" gate="1" x="-78.74" y="10.16" rot="R270"/>
<instance part="GND81" gate="1" x="-78.74" y="12.7" rot="R270"/>
<instance part="GND82" gate="1" x="-78.74" y="15.24" rot="R270"/>
<instance part="GND83" gate="1" x="-78.74" y="17.78" rot="R270"/>
<instance part="+3V21" gate="G$1" x="-93.98" y="109.22" rot="R90"/>
<instance part="VCC4" gate="G$1" x="-78.74" y="111.76" rot="R90"/>
<instance part="VCC5" gate="G$1" x="-78.74" y="106.68" rot="R90"/>
<instance part="VCC6" gate="G$1" x="-78.74" y="101.6" rot="R90"/>
<instance part="VCC7" gate="G$1" x="-78.74" y="99.06" rot="R90"/>
<instance part="VCC8" gate="G$1" x="-88.9" y="-40.64" rot="R90"/>
<instance part="VCC1" gate="G$1" x="-88.9" y="-38.1" rot="R90"/>
<instance part="+3V1" gate="G$1" x="-96.52" y="-35.56" rot="R90"/>
<instance part="+3V9" gate="G$1" x="-96.52" y="-33.02" rot="R90"/>
<instance part="U1" gate="B" x="-53.34" y="-73.66"/>
<instance part="U1" gate="A" x="-53.34" y="63.5"/>
<instance part="+3V10" gate="G$1" x="-114.3" y="154.94"/>
<instance part="VCC2" gate="G$1" x="-99.06" y="154.94"/>
<instance part="VCC3" gate="G$1" x="-88.9" y="154.94"/>
<instance part="+3V11" gate="G$1" x="-127" y="154.94"/>
<instance part="C11" gate="G$1" x="-127" y="149.86"/>
<instance part="C12" gate="G$1" x="-114.3" y="149.86"/>
<instance part="C31" gate="G$1" x="-99.06" y="149.86"/>
<instance part="C32" gate="G$1" x="-88.9" y="149.86"/>
<instance part="GND85" gate="1" x="-127" y="142.24"/>
<instance part="GND87" gate="1" x="-114.3" y="142.24"/>
<instance part="GND90" gate="1" x="-99.06" y="142.24"/>
<instance part="GND91" gate="1" x="-88.9" y="142.24"/>
<instance part="C33" gate="G$1" x="71.12" y="149.86"/>
<instance part="VCC9" gate="G$1" x="71.12" y="154.94"/>
<instance part="GND92" gate="1" x="71.12" y="142.24"/>
<instance part="C34" gate="G$1" x="81.28" y="149.86"/>
<instance part="VCC10" gate="G$1" x="81.28" y="154.94"/>
<instance part="GND93" gate="1" x="81.28" y="142.24"/>
<instance part="C35" gate="G$1" x="91.44" y="149.86"/>
<instance part="VCC11" gate="G$1" x="91.44" y="154.94"/>
<instance part="GND94" gate="1" x="91.44" y="142.24"/>
<instance part="C36" gate="G$1" x="101.6" y="149.86"/>
<instance part="VCC12" gate="G$1" x="101.6" y="154.94"/>
<instance part="GND95" gate="1" x="101.6" y="142.24"/>
<instance part="+3V12" gate="G$1" x="-30.48" y="157.48"/>
<instance part="+3V13" gate="G$1" x="-17.78" y="157.48"/>
<instance part="+3V14" gate="G$1" x="-5.08" y="157.48"/>
<instance part="+3V22" gate="G$1" x="7.62" y="157.48"/>
<instance part="+3V23" gate="G$1" x="20.32" y="157.48"/>
<instance part="GND99" gate="1" x="-43.18" y="144.78"/>
<instance part="GND100" gate="1" x="-30.48" y="144.78"/>
<instance part="GND101" gate="1" x="-17.78" y="144.78"/>
<instance part="GND102" gate="1" x="-5.08" y="144.78"/>
<instance part="GND103" gate="1" x="7.62" y="144.78"/>
<instance part="GND104" gate="1" x="20.32" y="144.78"/>
<instance part="C52" gate="G$1" x="111.76" y="149.86"/>
<instance part="VCC13" gate="G$1" x="111.76" y="154.94"/>
<instance part="GND156" gate="1" x="111.76" y="142.24"/>
<instance part="C1" gate="G$1" x="33.02" y="152.4"/>
<instance part="+3V25" gate="G$1" x="33.02" y="157.48"/>
<instance part="GND149" gate="1" x="33.02" y="144.78"/>
<instance part="U8" gate="A" x="182.88" y="60.96"/>
<instance part="+3V80" gate="G$1" x="109.22" y="93.98" rot="R90"/>
<instance part="+3V81" gate="G$1" x="109.22" y="91.44" rot="R90"/>
<instance part="+3V82" gate="G$1" x="109.22" y="88.9" rot="R90"/>
<instance part="+3V83" gate="G$1" x="109.22" y="86.36" rot="R90"/>
<instance part="+3V84" gate="G$1" x="109.22" y="83.82" rot="R90"/>
<instance part="C62" gate="G$1" x="142.24" y="-12.7"/>
<instance part="C63" gate="G$1" x="152.4" y="-12.7"/>
<instance part="C64" gate="G$1" x="162.56" y="-12.7"/>
<instance part="GND205" gate="1" x="142.24" y="-20.32"/>
<instance part="GND206" gate="1" x="152.4" y="-20.32"/>
<instance part="GND207" gate="1" x="162.56" y="-20.32"/>
<instance part="C65" gate="G$1" x="190.5" y="-12.7"/>
<instance part="C66" gate="G$1" x="200.66" y="-12.7"/>
<instance part="C67" gate="G$1" x="210.82" y="-12.7"/>
<instance part="GND208" gate="1" x="190.5" y="-20.32"/>
<instance part="GND209" gate="1" x="200.66" y="-20.32"/>
<instance part="GND210" gate="1" x="210.82" y="-20.32"/>
<instance part="C68" gate="G$1" x="236.22" y="-12.7"/>
<instance part="GND211" gate="1" x="236.22" y="-20.32"/>
<instance part="X2" gate="G$1" x="363.22" y="43.18"/>
<instance part="GND212" gate="1" x="302.26" y="17.78" rot="R270"/>
<instance part="GND213" gate="1" x="302.26" y="12.7" rot="R270"/>
<instance part="R88" gate="G$1" x="111.76" y="43.18"/>
<instance part="GND214" gate="1" x="104.14" y="43.18" rot="R270"/>
<instance part="R89" gate="G$1" x="307.34" y="73.66" rot="R270"/>
<instance part="R90" gate="G$1" x="314.96" y="73.66" rot="R270"/>
<instance part="R91" gate="G$1" x="322.58" y="73.66" rot="R270"/>
<instance part="R92" gate="G$1" x="330.2" y="73.66" rot="R270"/>
<instance part="+3V85" gate="G$1" x="307.34" y="81.28"/>
<instance part="+3V86" gate="G$1" x="314.96" y="81.28"/>
<instance part="+3V87" gate="G$1" x="322.58" y="81.28"/>
<instance part="+3V88" gate="G$1" x="330.2" y="81.28"/>
<instance part="+3V89" gate="G$1" x="340.36" y="116.84"/>
<instance part="C69" gate="G$1" x="340.36" y="111.76"/>
<instance part="GND215" gate="1" x="340.36" y="104.14"/>
<instance part="+3V90" gate="G$1" x="353.06" y="116.84"/>
<instance part="C70" gate="G$1" x="353.06" y="111.76"/>
<instance part="GND216" gate="1" x="353.06" y="104.14"/>
<instance part="+3V91" gate="G$1" x="365.76" y="116.84"/>
<instance part="C71" gate="G$1" x="365.76" y="111.76"/>
<instance part="GND217" gate="1" x="365.76" y="104.14"/>
<instance part="+3V92" gate="G$1" x="378.46" y="116.84"/>
<instance part="C72" gate="G$1" x="378.46" y="111.76"/>
<instance part="GND218" gate="1" x="378.46" y="104.14"/>
<instance part="GND219" gate="1" x="142.24" y="25.4" rot="R270"/>
<instance part="GND220" gate="1" x="142.24" y="22.86" rot="R270"/>
<instance part="+3V93" gate="G$1" x="345.44" y="53.34" rot="R90"/>
<instance part="+3V94" gate="G$1" x="345.44" y="33.02" rot="R90"/>
<instance part="+3V95" gate="G$1" x="264.16" y="116.84"/>
<instance part="C73" gate="G$1" x="264.16" y="111.76"/>
<instance part="GND221" gate="1" x="264.16" y="104.14"/>
<instance part="+3V96" gate="G$1" x="276.86" y="116.84"/>
<instance part="C74" gate="G$1" x="276.86" y="111.76"/>
<instance part="GND222" gate="1" x="276.86" y="104.14"/>
<instance part="+3V97" gate="G$1" x="289.56" y="116.84"/>
<instance part="C75" gate="G$1" x="289.56" y="111.76"/>
<instance part="GND223" gate="1" x="289.56" y="104.14"/>
<instance part="+3V98" gate="G$1" x="302.26" y="116.84"/>
<instance part="C76" gate="G$1" x="302.26" y="111.76"/>
<instance part="GND224" gate="1" x="302.26" y="104.14"/>
<instance part="+3V99" gate="G$1" x="251.46" y="116.84"/>
<instance part="C77" gate="G$1" x="251.46" y="111.76"/>
<instance part="GND225" gate="1" x="251.46" y="104.14"/>
<instance part="R93" gate="G$1" x="327.66" y="0"/>
<instance part="R94" gate="G$1" x="327.66" y="-10.16"/>
<instance part="+3V100" gate="G$1" x="345.44" y="5.08" rot="R90"/>
<instance part="+3V101" gate="G$1" x="345.44" y="-5.08" rot="R90"/>
<instance part="R95" gate="G$1" x="114.3" y="58.42"/>
<instance part="R96" gate="G$1" x="114.3" y="55.88"/>
<instance part="GND227" gate="1" x="106.68" y="55.88" rot="R270"/>
<instance part="R97" gate="G$1" x="254" y="43.18"/>
<instance part="+3V102" gate="G$1" x="261.62" y="43.18" rot="R270"/>
<instance part="R98" gate="G$1" x="254" y="68.58"/>
<instance part="R99" gate="G$1" x="254" y="66.04"/>
<instance part="R100" gate="G$1" x="254" y="40.64"/>
<instance part="+3V103" gate="G$1" x="261.62" y="68.58" rot="R270"/>
<instance part="+3V104" gate="G$1" x="261.62" y="66.04" rot="R270"/>
<instance part="GND228" gate="1" x="261.62" y="40.64" rot="R90"/>
<instance part="+3V105" gate="G$1" x="106.68" y="58.42" rot="R90"/>
<instance part="Q1" gate="G$1" x="177.8" y="132.08"/>
<instance part="C78" gate="G$1" x="165.1" y="129.54"/>
<instance part="C79" gate="G$1" x="190.5" y="129.54"/>
<instance part="GND226" gate="1" x="165.1" y="121.92"/>
<instance part="GND229" gate="1" x="190.5" y="121.92"/>
<instance part="Q2" gate="G$1" x="17.78" y="-78.74"/>
<instance part="GND19" gate="1" x="-111.76" y="86.36" rot="R270"/>
<instance part="GND20" gate="1" x="-111.76" y="83.82" rot="R270"/>
<instance part="GND21" gate="1" x="-111.76" y="88.9" rot="R270"/>
<instance part="+3V110" gate="G$1" x="-73.66" y="-114.3" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="25.4" y1="-99.06" x2="17.78" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-99.06" x2="10.16" y2="-99.06" width="0.1524" layer="91"/>
<junction x="17.78" y="-99.06"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-152.4" y1="78.74" x2="-152.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JTAG1" gate="G$1" pin="2"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="-182.88" y1="-53.34" x2="-182.88" y2="-50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JTAG1" gate="G$1" pin="10"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="-182.88" y1="-38.1" x2="-182.88" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GNDANA"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND_2"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND_3"/>
</segment>
<segment>
<pinref part="GND77" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND"/>
</segment>
<segment>
<pinref part="GND78" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="GND79" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_6"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_5"/>
</segment>
<segment>
<pinref part="GND81" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_4"/>
</segment>
<segment>
<pinref part="GND82" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_3"/>
</segment>
<segment>
<pinref part="GND83" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_2"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND85" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND87" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="GND90" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND91" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="2"/>
<pinref part="GND92" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="GND93" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND94" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="GND95" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND99" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND100" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="2"/>
<pinref part="GND101" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND102" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND103" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND104" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="GND156" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND149" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C62" gate="G$1" pin="2"/>
<pinref part="GND205" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="GND206" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C64" gate="G$1" pin="2"/>
<pinref part="GND207" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="GND208" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C66" gate="G$1" pin="2"/>
<pinref part="GND209" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="2"/>
<pinref part="GND210" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="2"/>
<pinref part="GND211" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="NC/4"/>
<pinref part="GND212" gate="1" pin="GND"/>
<wire x1="347.98" y1="17.78" x2="304.8" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="NC/5"/>
<pinref part="GND213" gate="1" pin="GND"/>
<wire x1="347.98" y1="12.7" x2="304.8" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R88" gate="G$1" pin="1"/>
<pinref part="GND214" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="2"/>
<pinref part="GND215" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C70" gate="G$1" pin="2"/>
<pinref part="GND216" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="2"/>
<pinref part="GND217" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="2"/>
<pinref part="GND218" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VSS"/>
<pinref part="GND219" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="EP"/>
<pinref part="GND220" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C73" gate="G$1" pin="2"/>
<pinref part="GND221" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C74" gate="G$1" pin="2"/>
<pinref part="GND222" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C75" gate="G$1" pin="2"/>
<pinref part="GND223" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C76" gate="G$1" pin="2"/>
<pinref part="GND224" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C77" gate="G$1" pin="2"/>
<pinref part="GND225" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R96" gate="G$1" pin="1"/>
<pinref part="GND227" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<pinref part="GND228" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C78" gate="G$1" pin="2"/>
<pinref part="GND226" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C79" gate="G$1" pin="2"/>
<pinref part="GND229" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="86.36" x2="-76.2" y2="86.36" width="0.1524" layer="91"/>
<label x="-101.6" y="86.36" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="DM"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="83.82" x2="-76.2" y2="83.82" width="0.1524" layer="91"/>
<label x="-101.6" y="83.82" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="DP"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="88.9" x2="-76.2" y2="88.9" width="0.1524" layer="91"/>
<label x="-101.6" y="88.9" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="VBUS"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="-195.58" y1="-48.26" x2="-182.88" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="JTAG1" gate="G$1" pin="4"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="-152.4" y1="104.14" x2="-152.4" y2="101.6" width="0.1524" layer="91"/>
<pinref part="+3V56" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="114.3" x2="-76.2" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO_2"/>
</segment>
<segment>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="104.14" x2="-76.2" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO_3"/>
</segment>
<segment>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="96.52" x2="-76.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO"/>
</segment>
<segment>
<pinref part="+3V18" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-25.4" x2="-71.12" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_2"/>
</segment>
<segment>
<pinref part="+3V19" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-27.94" x2="-71.12" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_3"/>
</segment>
<segment>
<pinref part="+3V20" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-30.48" x2="-71.12" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_4"/>
</segment>
<segment>
<pinref part="+3V21" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="109.22" x2="-76.2" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIN"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-35.56" x2="-71.12" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDANA"/>
</segment>
<segment>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-33.02" x2="-71.12" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<pinref part="C11" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C17" gate="G$1" pin="1"/>
<pinref part="+3V13" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="+3V22" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="+3V23" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="+3V25" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_VA11"/>
<pinref part="+3V80" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="93.98" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_IO_2"/>
<pinref part="+3V81" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_VD11"/>
<pinref part="+3V82" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_IO"/>
<pinref part="+3V83" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="86.36" x2="144.78" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_V18"/>
<pinref part="+3V84" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R89" gate="G$1" pin="1"/>
<pinref part="+3V85" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R90" gate="G$1" pin="1"/>
<pinref part="+3V86" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R91" gate="G$1" pin="1"/>
<pinref part="+3V87" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R92" gate="G$1" pin="1"/>
<pinref part="+3V88" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V89" gate="G$1" pin="+3V3"/>
<pinref part="C69" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V90" gate="G$1" pin="+3V3"/>
<pinref part="C70" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V91" gate="G$1" pin="+3V3"/>
<pinref part="C71" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V92" gate="G$1" pin="+3V3"/>
<pinref part="C72" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="CT"/>
<pinref part="+3V93" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="CR"/>
<pinref part="+3V94" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V95" gate="G$1" pin="+3V3"/>
<pinref part="C73" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V96" gate="G$1" pin="+3V3"/>
<pinref part="C74" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V97" gate="G$1" pin="+3V3"/>
<pinref part="C75" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V98" gate="G$1" pin="+3V3"/>
<pinref part="C76" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V99" gate="G$1" pin="+3V3"/>
<pinref part="C77" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="A-R"/>
<pinref part="+3V100" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="X2" gate="G$1" pin="AL"/>
<pinref part="+3V101" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R97" gate="G$1" pin="2"/>
<pinref part="+3V102" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<pinref part="+3V103" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R99" gate="G$1" pin="2"/>
<pinref part="+3V104" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R95" gate="G$1" pin="1"/>
<pinref part="+3V105" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="ADVREF"/>
<pinref part="+3V110" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VCCINT" class="0">
<segment>
<pinref part="VCC8" gate="G$1" pin="VCCINT"/>
<wire x1="-86.36" y1="-40.64" x2="-71.12" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDCORE"/>
</segment>
<segment>
<pinref part="VCC1" gate="G$1" pin="VCCINT"/>
<wire x1="-86.36" y1="-38.1" x2="-71.12" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDPLL"/>
</segment>
<segment>
<pinref part="VCC4" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDOUT"/>
</segment>
<segment>
<pinref part="VCC5" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE_2"/>
</segment>
<segment>
<pinref part="VCC6" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE_3"/>
</segment>
<segment>
<pinref part="VCC7" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE"/>
</segment>
<segment>
<pinref part="VCC2" gate="G$1" pin="VCCINT"/>
<pinref part="C31" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="VCC3" gate="G$1" pin="VCCINT"/>
<pinref part="C32" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C33" gate="G$1" pin="1"/>
<pinref part="VCC9" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="VCC10" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="VCC11" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="VCC12" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="1"/>
<pinref part="VCC13" gate="G$1" pin="VCCINT"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<wire x1="-182.88" y1="-45.72" x2="-198.12" y2="-45.72" width="0.1524" layer="91"/>
<label x="-195.58" y="-45.72" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<junction x="-152.4" y="91.44"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="91.44" x2="-152.4" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="91.44" x2="-76.2" y2="91.44" width="0.1524" layer="91"/>
<label x="-101.6" y="91.44" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="RESET_N"/>
</segment>
</net>
<net name="PS_CLK" class="0">
<segment>
<wire x1="-30.48" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<label x="-27.94" y="38.1" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX17"/>
</segment>
</net>
<net name="PS_SDA1" class="0">
<segment>
<wire x1="-76.2" y1="76.2" x2="-101.6" y2="76.2" width="0.1524" layer="91"/>
<label x="-99.06" y="76.2" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA01"/>
</segment>
</net>
<net name="PS_SDA2" class="0">
<segment>
<wire x1="-76.2" y1="73.66" x2="-101.6" y2="73.66" width="0.1524" layer="91"/>
<label x="-99.06" y="73.66" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA02"/>
</segment>
</net>
<net name="PS_SDA3" class="0">
<segment>
<wire x1="-76.2" y1="71.12" x2="-101.6" y2="71.12" width="0.1524" layer="91"/>
<label x="-99.06" y="71.12" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA03"/>
</segment>
</net>
<net name="PS_SDA4" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA04"/>
<wire x1="-76.2" y1="68.58" x2="-101.6" y2="68.58" width="0.1524" layer="91"/>
<label x="-99.06" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA6" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA06"/>
<wire x1="-76.2" y1="63.5" x2="-101.6" y2="63.5" width="0.1524" layer="91"/>
<label x="-99.06" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA7" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA07"/>
<wire x1="-76.2" y1="60.96" x2="-101.6" y2="60.96" width="0.1524" layer="91"/>
<label x="-99.06" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA8" class="0">
<segment>
<wire x1="-76.2" y1="58.42" x2="-101.6" y2="58.42" width="0.1524" layer="91"/>
<label x="-99.06" y="58.42" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA08"/>
</segment>
</net>
<net name="PS_SDA9" class="0">
<segment>
<wire x1="-76.2" y1="55.88" x2="-101.6" y2="55.88" width="0.1524" layer="91"/>
<label x="-99.06" y="55.88" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA09"/>
</segment>
</net>
<net name="PS_SDA10" class="0">
<segment>
<wire x1="-76.2" y1="53.34" x2="-101.6" y2="53.34" width="0.1524" layer="91"/>
<label x="-99.06" y="53.34" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA10"/>
</segment>
</net>
<net name="PS_SDA11" class="0">
<segment>
<wire x1="-76.2" y1="50.8" x2="-101.6" y2="50.8" width="0.1524" layer="91"/>
<label x="-99.06" y="50.8" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA11"/>
</segment>
</net>
<net name="PS_SDA12" class="0">
<segment>
<wire x1="-76.2" y1="48.26" x2="-101.6" y2="48.26" width="0.1524" layer="91"/>
<label x="-99.06" y="48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA12"/>
</segment>
</net>
<net name="PS_SDA13" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA13"/>
<wire x1="-76.2" y1="45.72" x2="-101.6" y2="45.72" width="0.1524" layer="91"/>
<label x="-99.06" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA14" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA14"/>
<wire x1="-76.2" y1="43.18" x2="-101.6" y2="43.18" width="0.1524" layer="91"/>
<label x="-99.06" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA15" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA15"/>
<wire x1="-76.2" y1="40.64" x2="-101.6" y2="40.64" width="0.1524" layer="91"/>
<label x="-99.06" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCUXTAL3" class="0">
<segment>
<wire x1="20.32" y1="-78.74" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-91.44" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-60.96" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<junction x="25.4" y="-78.74"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-60.96" x2="-35.56" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="PC02"/>
<pinref part="Q2" gate="G$1" pin="2"/>
<label x="-33.02" y="-60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCUXTAL4" class="0">
<segment>
<wire x1="10.16" y1="-91.44" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-78.74" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-63.5" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="10.16" y="-78.74"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-63.5" x2="-35.56" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="PC03"/>
<pinref part="Q2" gate="G$1" pin="1"/>
<label x="-33.02" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<wire x1="-152.4" y1="-50.8" x2="-167.64" y2="-50.8" width="0.1524" layer="91"/>
<label x="-160.02" y="-50.8" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-71.12" y1="-48.26" x2="-86.36" y2="-48.26" width="0.1524" layer="91"/>
<label x="-86.36" y="-48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TCK"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<wire x1="-152.4" y1="-48.26" x2="-167.64" y2="-48.26" width="0.1524" layer="91"/>
<label x="-160.02" y="-48.26" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="-35.56" y1="-25.4" x2="-20.32" y2="-25.4" width="0.1524" layer="91"/>
<label x="-33.02" y="-25.4" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TDO"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<label x="-160.02" y="-45.72" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="5"/>
<wire x1="-152.4" y1="-45.72" x2="-167.64" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-71.12" y1="-45.72" x2="-86.36" y2="-45.72" width="0.1524" layer="91"/>
<label x="-86.36" y="-45.72" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TMS"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<wire x1="-152.4" y1="-40.64" x2="-167.64" y2="-40.64" width="0.1524" layer="91"/>
<label x="-160.02" y="-40.64" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="-71.12" y1="-50.8" x2="-86.36" y2="-50.8" width="0.1524" layer="91"/>
<label x="-86.36" y="-50.8" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TDI"/>
</segment>
</net>
<net name="EVTO" class="0">
<segment>
<wire x1="-167.64" y1="-43.18" x2="-152.4" y2="-43.18" width="0.1524" layer="91"/>
<label x="-160.02" y="-43.18" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="DAC0_DIN" class="0">
<segment>
<wire x1="2.54" y1="81.28" x2="-30.48" y2="81.28" width="0.1524" layer="91"/>
<label x="-27.94" y="81.28" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX00"/>
</segment>
</net>
<net name="DAC1_DIN" class="0">
<segment>
<wire x1="2.54" y1="78.74" x2="-30.48" y2="78.74" width="0.1524" layer="91"/>
<label x="-27.94" y="78.74" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX01"/>
</segment>
</net>
<net name="DAC_CLK" class="0">
<segment>
<wire x1="2.54" y1="76.2" x2="-30.48" y2="76.2" width="0.1524" layer="91"/>
<label x="-27.94" y="76.2" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX02"/>
</segment>
</net>
<net name="DAC_/SYNC" class="0">
<segment>
<wire x1="-30.48" y1="73.66" x2="2.54" y2="73.66" width="0.1524" layer="91"/>
<label x="-27.94" y="73.66" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX03"/>
</segment>
</net>
<net name="DAC_/LDAC" class="0">
<segment>
<wire x1="2.54" y1="71.12" x2="-30.48" y2="71.12" width="0.1524" layer="91"/>
<label x="-27.94" y="71.12" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX04"/>
</segment>
</net>
<net name="ADC_CLK" class="0">
<segment>
<wire x1="-30.48" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<label x="-27.94" y="60.96" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX08"/>
</segment>
</net>
<net name="PS_SDA16" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA16"/>
<wire x1="-76.2" y1="38.1" x2="-101.6" y2="38.1" width="0.1524" layer="91"/>
<label x="-99.06" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA17" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA17"/>
<wire x1="-76.2" y1="35.56" x2="-101.6" y2="35.56" width="0.1524" layer="91"/>
<label x="-99.06" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA18" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA18"/>
<wire x1="-76.2" y1="33.02" x2="-101.6" y2="33.02" width="0.1524" layer="91"/>
<label x="-99.06" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA19" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA19"/>
<wire x1="-76.2" y1="30.48" x2="-101.6" y2="30.48" width="0.1524" layer="91"/>
<label x="-99.06" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA20" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB20"/>
<wire x1="-30.48" y1="114.3" x2="2.54" y2="114.3" width="0.1524" layer="91"/>
<label x="-27.94" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA21" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB21"/>
<wire x1="-30.48" y1="111.76" x2="2.54" y2="111.76" width="0.1524" layer="91"/>
<label x="-27.94" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA22" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB22"/>
<wire x1="-30.48" y1="109.22" x2="2.54" y2="109.22" width="0.1524" layer="91"/>
<label x="-27.94" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA23" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB23"/>
<wire x1="-30.48" y1="106.68" x2="2.54" y2="106.68" width="0.1524" layer="91"/>
<label x="-27.94" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA24" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB24"/>
<wire x1="-30.48" y1="104.14" x2="2.54" y2="104.14" width="0.1524" layer="91"/>
<label x="-27.94" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA25" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB25"/>
<wire x1="-30.48" y1="101.6" x2="2.54" y2="101.6" width="0.1524" layer="91"/>
<label x="-27.94" y="101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA26" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB26"/>
<wire x1="-30.48" y1="99.06" x2="2.54" y2="99.06" width="0.1524" layer="91"/>
<label x="-27.94" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA27" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB27"/>
<wire x1="-30.48" y1="96.52" x2="2.54" y2="96.52" width="0.1524" layer="91"/>
<label x="-27.94" y="96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA28" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB28"/>
<wire x1="-30.48" y1="93.98" x2="2.54" y2="93.98" width="0.1524" layer="91"/>
<label x="-27.94" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA29" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB29"/>
<wire x1="-30.48" y1="91.44" x2="2.54" y2="91.44" width="0.1524" layer="91"/>
<label x="-27.94" y="91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA30" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB30"/>
<wire x1="-30.48" y1="88.9" x2="2.54" y2="88.9" width="0.1524" layer="91"/>
<label x="-27.94" y="88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA31" class="0">
<segment>
<pinref part="U1" gate="A" pin="PB31"/>
<wire x1="-30.48" y1="86.36" x2="2.54" y2="86.36" width="0.1524" layer="91"/>
<label x="-27.94" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_Y/X" class="0">
<segment>
<wire x1="2.54" y1="55.88" x2="-30.48" y2="55.88" width="0.1524" layer="91"/>
<label x="-27.94" y="55.88" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX10"/>
</segment>
</net>
<net name="ENC_C/D" class="0">
<segment>
<wire x1="2.54" y1="53.34" x2="-30.48" y2="53.34" width="0.1524" layer="91"/>
<label x="-27.94" y="53.34" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX11"/>
</segment>
</net>
<net name="ENC_/RD" class="0">
<segment>
<wire x1="2.54" y1="50.8" x2="-30.48" y2="50.8" width="0.1524" layer="91"/>
<label x="-27.94" y="50.8" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX12"/>
</segment>
</net>
<net name="ENC_/WR" class="0">
<segment>
<wire x1="2.54" y1="48.26" x2="-30.48" y2="48.26" width="0.1524" layer="91"/>
<label x="-27.94" y="48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX13"/>
</segment>
</net>
<net name="ENC0_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX32"/>
<wire x1="-35.56" y1="-104.14" x2="-10.16" y2="-104.14" width="0.1524" layer="91"/>
<label x="-33.02" y="-104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC1_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX33"/>
<wire x1="-35.56" y1="-106.68" x2="-10.16" y2="-106.68" width="0.1524" layer="91"/>
<label x="-33.02" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC2_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX34"/>
<wire x1="-35.56" y1="-109.22" x2="-10.16" y2="-109.22" width="0.1524" layer="91"/>
<label x="-33.02" y="-109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC3_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX35"/>
<wire x1="-35.56" y1="-111.76" x2="-10.16" y2="-111.76" width="0.1524" layer="91"/>
<label x="-33.02" y="-111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC4_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX36"/>
<wire x1="-35.56" y1="-114.3" x2="-10.16" y2="-114.3" width="0.1524" layer="91"/>
<label x="-33.02" y="-114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC5_/CS" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX37"/>
<wire x1="-35.56" y1="-116.84" x2="-10.16" y2="-116.84" width="0.1524" layer="91"/>
<label x="-33.02" y="-116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC0_DI" class="0">
<segment>
<wire x1="2.54" y1="68.58" x2="-30.48" y2="68.58" width="0.1524" layer="91"/>
<label x="-27.94" y="68.58" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX05"/>
</segment>
</net>
<net name="ADC0_DO" class="0">
<segment>
<wire x1="-30.48" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<label x="-27.94" y="66.04" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX06"/>
</segment>
</net>
<net name="ADC0_BUSY" class="0">
<segment>
<wire x1="2.54" y1="63.5" x2="-30.48" y2="63.5" width="0.1524" layer="91"/>
<label x="-27.94" y="63.5" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX07"/>
</segment>
</net>
<net name="MII_TXD_0" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_0"/>
<wire x1="144.78" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<label x="121.92" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB02"/>
<wire x1="-71.12" y1="-88.9" x2="-93.98" y2="-88.9" width="0.1524" layer="91"/>
<label x="-91.44" y="-88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_1" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_1"/>
<wire x1="144.78" y1="68.58" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<label x="121.92" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB03"/>
<wire x1="-71.12" y1="-91.44" x2="-93.98" y2="-91.44" width="0.1524" layer="91"/>
<label x="-91.44" y="-91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_2" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_2"/>
<wire x1="144.78" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<label x="121.92" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB10"/>
<wire x1="-71.12" y1="-109.22" x2="-93.98" y2="-109.22" width="0.1524" layer="91"/>
<label x="-91.44" y="-109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_3" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_3"/>
<wire x1="144.78" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<label x="121.92" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB11"/>
<wire x1="-35.56" y1="-30.48" x2="-10.16" y2="-30.48" width="0.1524" layer="91"/>
<label x="-33.02" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TX_EN" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TX_EN"/>
<wire x1="144.78" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<label x="121.92" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB01"/>
<wire x1="-71.12" y1="-86.36" x2="-93.98" y2="-86.36" width="0.1524" layer="91"/>
<label x="-91.44" y="-86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_/PWRDN" class="0">
<segment>
<pinref part="U8" gate="A" pin="PWRDNN/INT"/>
<wire x1="144.78" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<label x="121.92" y="58.42" size="1.778" layer="95"/>
<pinref part="R95" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ETHER_/RST" class="0">
<segment>
<pinref part="U8" gate="A" pin="RESETN"/>
<wire x1="144.78" y1="55.88" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<label x="121.92" y="55.88" size="1.778" layer="95"/>
<pinref part="R96" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PX38"/>
<wire x1="-35.56" y1="-119.38" x2="-10.16" y2="-119.38" width="0.1524" layer="91"/>
<label x="-33.02" y="-119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TX_CLK" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TX_CLK"/>
<wire x1="220.98" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<label x="226.06" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB00"/>
<wire x1="-71.12" y1="-83.82" x2="-93.98" y2="-83.82" width="0.1524" layer="91"/>
<label x="-91.44" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_COL" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_COL/PHYAD0"/>
<wire x1="220.98" y1="83.82" x2="248.92" y2="83.82" width="0.1524" layer="91"/>
<label x="226.06" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB16"/>
<wire x1="-35.56" y1="-43.18" x2="-10.16" y2="-43.18" width="0.1524" layer="91"/>
<label x="-33.02" y="-43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_0" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_0/PHYAD1"/>
<wire x1="220.98" y1="81.28" x2="248.92" y2="81.28" width="0.1524" layer="91"/>
<label x="226.06" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB05"/>
<wire x1="-71.12" y1="-96.52" x2="-93.98" y2="-96.52" width="0.1524" layer="91"/>
<label x="-91.44" y="-96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_1" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_1/PHYAD2"/>
<wire x1="220.98" y1="78.74" x2="248.92" y2="78.74" width="0.1524" layer="91"/>
<label x="226.06" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB06"/>
<wire x1="-71.12" y1="-99.06" x2="-93.98" y2="-99.06" width="0.1524" layer="91"/>
<label x="-91.44" y="-99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_2" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_2/PHYAD3"/>
<wire x1="220.98" y1="76.2" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<label x="226.06" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB13"/>
<wire x1="-35.56" y1="-35.56" x2="-10.16" y2="-35.56" width="0.1524" layer="91"/>
<label x="-33.02" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_3" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_3/PHYAD4"/>
<wire x1="220.98" y1="73.66" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<label x="226.06" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB14"/>
<wire x1="-35.56" y1="-38.1" x2="-10.16" y2="-38.1" width="0.1524" layer="91"/>
<label x="-33.02" y="-38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_LINK" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_LINK/AN_0"/>
<wire x1="220.98" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<label x="226.06" y="68.58" size="1.778" layer="95"/>
<pinref part="R98" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R93" gate="G$1" pin="1"/>
<wire x1="322.58" y1="0" x2="294.64" y2="0" width="0.1524" layer="91"/>
<label x="294.64" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_SPEED" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_SPEED/AN_1"/>
<wire x1="220.98" y1="66.04" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<label x="226.06" y="66.04" size="1.778" layer="95"/>
<pinref part="R99" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MII_CRS" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_CRS/LED_CFG"/>
<wire x1="220.98" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<label x="226.06" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB04"/>
<wire x1="-71.12" y1="-93.98" x2="-93.98" y2="-93.98" width="0.1524" layer="91"/>
<label x="-91.44" y="-93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_CLK" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_CLK"/>
<wire x1="220.98" y1="48.26" x2="248.92" y2="48.26" width="0.1524" layer="91"/>
<label x="226.06" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB17"/>
<wire x1="-35.56" y1="-45.72" x2="-10.16" y2="-45.72" width="0.1524" layer="91"/>
<label x="-33.02" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_DV" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_DV"/>
<wire x1="220.98" y1="45.72" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<label x="226.06" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB15"/>
<wire x1="-35.56" y1="-40.64" x2="-10.16" y2="-40.64" width="0.1524" layer="91"/>
<label x="-33.02" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_ERR" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_ERR/MDIX_EN"/>
<wire x1="220.98" y1="43.18" x2="248.92" y2="43.18" width="0.1524" layer="91"/>
<label x="226.06" y="43.18" size="1.778" layer="95"/>
<pinref part="R97" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB07"/>
<wire x1="-71.12" y1="-101.6" x2="-93.98" y2="-101.6" width="0.1524" layer="91"/>
<label x="-91.44" y="-101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_ACT" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_ACT/AN_EN"/>
<wire x1="220.98" y1="40.64" x2="248.92" y2="40.64" width="0.1524" layer="91"/>
<label x="226.06" y="40.64" size="1.778" layer="95"/>
<pinref part="R100" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R94" gate="G$1" pin="1"/>
<wire x1="322.58" y1="-10.16" x2="294.64" y2="-10.16" width="0.1524" layer="91"/>
<label x="294.64" y="-10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_V18" class="0">
<segment>
<pinref part="U8" gate="A" pin="V18_PFBIN1"/>
<wire x1="144.78" y1="33.02" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<label x="121.92" y="33.02" size="1.778" layer="95"/>
<wire x1="213.36" y1="-10.16" x2="210.82" y2="-10.16" width="0.1524" layer="91"/>
<label x="190.5" y="-10.16" size="1.778" layer="95"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-10.16" x2="200.66" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-10.16" x2="190.5" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-10.16" x2="187.96" y2="-10.16" width="0.1524" layer="91"/>
<junction x="190.5" y="-10.16"/>
<pinref part="C66" gate="G$1" pin="1"/>
<junction x="200.66" y="-10.16"/>
<pinref part="C67" gate="G$1" pin="1"/>
<junction x="210.82" y="-10.16"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="V18_PFBIN2"/>
<wire x1="144.78" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<label x="121.92" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="V18_PFBOUT"/>
<wire x1="220.98" y1="58.42" x2="248.92" y2="58.42" width="0.1524" layer="91"/>
<label x="226.06" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_VA11" class="0">
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBIN1"/>
<wire x1="119.38" y1="78.74" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<label x="121.92" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBIN2"/>
<wire x1="119.38" y1="76.2" x2="144.78" y2="76.2" width="0.1524" layer="91"/>
<label x="121.92" y="76.2" size="1.778" layer="95"/>
<wire x1="139.7" y1="-10.16" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
<label x="142.24" y="-10.16" size="1.778" layer="95"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="142.24" y1="-10.16" x2="152.4" y2="-10.16" width="0.1524" layer="91"/>
<junction x="142.24" y="-10.16"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="152.4" y1="-10.16" x2="162.56" y2="-10.16" width="0.1524" layer="91"/>
<junction x="152.4" y="-10.16"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="162.56" y1="-10.16" x2="165.1" y2="-10.16" width="0.1524" layer="91"/>
<junction x="162.56" y="-10.16"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBOUT"/>
<wire x1="220.98" y1="93.98" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<label x="226.06" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_VDD11" class="0">
<segment>
<pinref part="U8" gate="A" pin="VDD11"/>
<wire x1="220.98" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<label x="226.06" y="53.34" size="1.778" layer="95"/>
<wire x1="231.14" y1="-10.16" x2="236.22" y2="-10.16" width="0.1524" layer="91"/>
<label x="233.68" y="-10.16" size="1.778" layer="95"/>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="236.22" y1="-10.16" x2="259.08" y2="-10.16" width="0.1524" layer="91"/>
<junction x="236.22" y="-10.16"/>
</segment>
</net>
<net name="ETHER_XO" class="0">
<segment>
<pinref part="U8" gate="A" pin="XO"/>
<wire x1="220.98" y1="60.96" x2="248.92" y2="60.96" width="0.1524" layer="91"/>
<label x="226.06" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="1"/>
<wire x1="175.26" y1="132.08" x2="165.1" y2="132.08" width="0.1524" layer="91"/>
<label x="149.86" y="132.08" size="1.778" layer="95"/>
<pinref part="C78" gate="G$1" pin="1"/>
<wire x1="165.1" y1="132.08" x2="147.32" y2="132.08" width="0.1524" layer="91"/>
<junction x="165.1" y="132.08"/>
</segment>
</net>
<net name="ETHER_XI" class="0">
<segment>
<pinref part="U8" gate="A" pin="XI"/>
<wire x1="144.78" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<label x="121.92" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="2"/>
<wire x1="180.34" y1="132.08" x2="190.5" y2="132.08" width="0.1524" layer="91"/>
<label x="193.04" y="132.08" size="1.778" layer="95"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="190.5" y1="132.08" x2="210.82" y2="132.08" width="0.1524" layer="91"/>
<junction x="190.5" y="132.08"/>
</segment>
</net>
<net name="EXTRA_PB12" class="0">
<segment>
<pinref part="U1" gate="B" pin="PB12"/>
<wire x1="-35.56" y1="-33.02" x2="-10.16" y2="-33.02" width="0.1524" layer="91"/>
<label x="-33.02" y="-33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PB18" class="0">
<segment>
<pinref part="U1" gate="B" pin="PB18"/>
<wire x1="-35.56" y1="-48.26" x2="-10.16" y2="-48.26" width="0.1524" layer="91"/>
<label x="-33.02" y="-48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_MDC" class="0">
<segment>
<pinref part="U8" gate="A" pin="MDC"/>
<wire x1="144.78" y1="40.64" x2="119.38" y2="40.64" width="0.1524" layer="91"/>
<label x="121.92" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB08"/>
<wire x1="-71.12" y1="-104.14" x2="-93.98" y2="-104.14" width="0.1524" layer="91"/>
<label x="-91.44" y="-104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_MDIO" class="0">
<segment>
<pinref part="U8" gate="A" pin="MDIO"/>
<wire x1="220.98" y1="25.4" x2="248.92" y2="25.4" width="0.1524" layer="91"/>
<label x="226.06" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB09"/>
<wire x1="-71.12" y1="-106.68" x2="-93.98" y2="-106.68" width="0.1524" layer="91"/>
<label x="-91.44" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RBIAS" class="0">
<segment>
<pinref part="U8" gate="A" pin="RBIAS"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="116.84" y1="43.18" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
<label x="121.92" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RD-" class="0">
<segment>
<pinref part="U8" gate="A" pin="RD-"/>
<wire x1="220.98" y1="35.56" x2="246.38" y2="35.56" width="0.1524" layer="91"/>
<wire x1="246.38" y1="35.56" x2="256.54" y2="25.4" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="RD-"/>
<wire x1="256.54" y1="25.4" x2="330.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="R92" gate="G$1" pin="2"/>
<wire x1="330.2" y1="25.4" x2="347.98" y2="25.4" width="0.1524" layer="91"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="25.4" width="0.1524" layer="91"/>
<junction x="330.2" y="25.4"/>
<label x="226.06" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RD+" class="0">
<segment>
<pinref part="U8" gate="A" pin="RD+"/>
<wire x1="220.98" y1="33.02" x2="271.78" y2="33.02" width="0.1524" layer="91"/>
<wire x1="271.78" y1="33.02" x2="279.4" y2="40.64" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="RD+"/>
<wire x1="279.4" y1="40.64" x2="322.58" y2="40.64" width="0.1524" layer="91"/>
<pinref part="R91" gate="G$1" pin="2"/>
<wire x1="322.58" y1="40.64" x2="347.98" y2="40.64" width="0.1524" layer="91"/>
<wire x1="322.58" y1="68.58" x2="322.58" y2="40.64" width="0.1524" layer="91"/>
<junction x="322.58" y="40.64"/>
<label x="226.06" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_TD-" class="0">
<segment>
<pinref part="U8" gate="A" pin="TD-"/>
<wire x1="220.98" y1="30.48" x2="264.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="264.16" y1="30.48" x2="279.4" y2="45.72" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="TD-"/>
<wire x1="279.4" y1="45.72" x2="314.96" y2="45.72" width="0.1524" layer="91"/>
<pinref part="R90" gate="G$1" pin="2"/>
<wire x1="314.96" y1="45.72" x2="347.98" y2="45.72" width="0.1524" layer="91"/>
<wire x1="314.96" y1="68.58" x2="314.96" y2="45.72" width="0.1524" layer="91"/>
<junction x="314.96" y="45.72"/>
<label x="226.06" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_TD+" class="0">
<segment>
<pinref part="U8" gate="A" pin="TD+"/>
<wire x1="220.98" y1="27.94" x2="256.54" y2="27.94" width="0.1524" layer="91"/>
<wire x1="256.54" y1="27.94" x2="289.56" y2="60.96" width="0.1524" layer="91"/>
<pinref part="X2" gate="G$1" pin="TD+"/>
<wire x1="289.56" y1="60.96" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<pinref part="R89" gate="G$1" pin="2"/>
<wire x1="307.34" y1="60.96" x2="347.98" y2="60.96" width="0.1524" layer="91"/>
<wire x1="307.34" y1="68.58" x2="307.34" y2="60.96" width="0.1524" layer="91"/>
<junction x="307.34" y="60.96"/>
<label x="226.06" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$153" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="C-R"/>
<wire x1="347.98" y1="0" x2="332.74" y2="0" width="0.1524" layer="91"/>
<pinref part="R93" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$154" class="0">
<segment>
<pinref part="X2" gate="G$1" pin="C-L"/>
<wire x1="347.98" y1="-10.16" x2="332.74" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="R94" gate="G$1" pin="2"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<wire x1="-35.56" y1="-91.44" x2="-10.16" y2="-91.44" width="0.1524" layer="91"/>
<label x="-33.02" y="-91.44" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX27"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<wire x1="-35.56" y1="-96.52" x2="-10.16" y2="-96.52" width="0.1524" layer="91"/>
<label x="-33.02" y="-96.52" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX29"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<wire x1="-35.56" y1="-93.98" x2="-10.16" y2="-93.98" width="0.1524" layer="91"/>
<label x="-33.02" y="-93.98" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX28"/>
</segment>
</net>
<net name="GYRO_/CS" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX14"/>
<wire x1="-30.48" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<label x="-27.94" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_/RST" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX15"/>
<wire x1="-30.48" y1="43.18" x2="2.54" y2="43.18" width="0.1524" layer="91"/>
<label x="-27.94" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="PS_SDA0" class="0">
<segment>
<wire x1="-76.2" y1="78.74" x2="-101.6" y2="78.74" width="0.1524" layer="91"/>
<label x="-99.06" y="78.74" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA00"/>
</segment>
</net>
<net name="PS_SDA5" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA05"/>
<wire x1="-76.2" y1="66.04" x2="-101.6" y2="66.04" width="0.1524" layer="91"/>
<label x="-99.06" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D0" class="0">
<segment>
<pinref part="U1" gate="A" pin="PA20"/>
<wire x1="-76.2" y1="27.94" x2="-101.6" y2="27.94" width="0.1524" layer="91"/>
<label x="-99.06" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D1" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA21"/>
<wire x1="-71.12" y1="-55.88" x2="-93.98" y2="-55.88" width="0.1524" layer="91"/>
<label x="-91.44" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D2" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA22"/>
<wire x1="-71.12" y1="-58.42" x2="-93.98" y2="-58.42" width="0.1524" layer="91"/>
<label x="-91.44" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D3" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA23"/>
<wire x1="-71.12" y1="-60.96" x2="-93.98" y2="-60.96" width="0.1524" layer="91"/>
<label x="-91.44" y="-60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D4" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA24"/>
<wire x1="-71.12" y1="-63.5" x2="-93.98" y2="-63.5" width="0.1524" layer="91"/>
<label x="-91.44" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D5" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA25"/>
<wire x1="-71.12" y1="-66.04" x2="-93.98" y2="-66.04" width="0.1524" layer="91"/>
<label x="-91.44" y="-66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D6" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA26"/>
<wire x1="-71.12" y1="-68.58" x2="-93.98" y2="-68.58" width="0.1524" layer="91"/>
<label x="-91.44" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC_D7" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA27"/>
<wire x1="-71.12" y1="-71.12" x2="-93.98" y2="-71.12" width="0.1524" layer="91"/>
<label x="-91.44" y="-71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="ADC_/CS" class="0">
<segment>
<wire x1="2.54" y1="58.42" x2="-30.48" y2="58.42" width="0.1524" layer="91"/>
<label x="-27.94" y="58.42" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX09"/>
</segment>
</net>
<net name="VPWR_EN" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX18"/>
<wire x1="-30.48" y1="35.56" x2="2.54" y2="35.56" width="0.1524" layer="91"/>
<label x="-27.94" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="VPWR_ST" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX19"/>
<wire x1="-30.48" y1="33.02" x2="2.54" y2="33.02" width="0.1524" layer="91"/>
<label x="-27.94" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_SYNC" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX16"/>
<wire x1="-30.48" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<label x="-27.94" y="40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX20" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX20"/>
<wire x1="-35.56" y1="-73.66" x2="-10.16" y2="-73.66" width="0.1524" layer="91"/>
<label x="-33.02" y="-73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX21" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX21"/>
<wire x1="-35.56" y1="-76.2" x2="-10.16" y2="-76.2" width="0.1524" layer="91"/>
<label x="-33.02" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX22" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX22"/>
<wire x1="-35.56" y1="-78.74" x2="-10.16" y2="-78.74" width="0.1524" layer="91"/>
<label x="-33.02" y="-78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX23" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX23"/>
<wire x1="-35.56" y1="-81.28" x2="-10.16" y2="-81.28" width="0.1524" layer="91"/>
<label x="-33.02" y="-81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX24" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX24"/>
<wire x1="-35.56" y1="-83.82" x2="-10.16" y2="-83.82" width="0.1524" layer="91"/>
<label x="-33.02" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX25" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX25"/>
<wire x1="-35.56" y1="-86.36" x2="-10.16" y2="-86.36" width="0.1524" layer="91"/>
<label x="-33.02" y="-86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX26" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX26"/>
<wire x1="-35.56" y1="-88.9" x2="-10.16" y2="-88.9" width="0.1524" layer="91"/>
<label x="-33.02" y="-88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX30" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX30"/>
<wire x1="-35.56" y1="-99.06" x2="-10.16" y2="-99.06" width="0.1524" layer="91"/>
<label x="-33.02" y="-99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX31" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX31"/>
<wire x1="-35.56" y1="-101.6" x2="-10.16" y2="-101.6" width="0.1524" layer="91"/>
<label x="-33.02" y="-101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PX39" class="0">
<segment>
<pinref part="U1" gate="B" pin="PX39"/>
<wire x1="-35.56" y1="-121.92" x2="-10.16" y2="-121.92" width="0.1524" layer="91"/>
<label x="-33.02" y="-121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PA28" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA28"/>
<wire x1="-71.12" y1="-73.66" x2="-93.98" y2="-73.66" width="0.1524" layer="91"/>
<label x="-91.44" y="-73.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PA29" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA29"/>
<wire x1="-71.12" y1="-76.2" x2="-93.98" y2="-76.2" width="0.1524" layer="91"/>
<label x="-91.44" y="-76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PA30" class="0">
<segment>
<pinref part="U1" gate="B" pin="PA30"/>
<wire x1="-71.12" y1="-78.74" x2="-93.98" y2="-78.74" width="0.1524" layer="91"/>
<label x="-91.44" y="-78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PC00" class="0">
<segment>
<pinref part="U1" gate="B" pin="PC00"/>
<wire x1="-35.56" y1="-55.88" x2="-10.16" y2="-55.88" width="0.1524" layer="91"/>
<label x="-33.02" y="-55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PC01" class="0">
<segment>
<pinref part="U1" gate="B" pin="PC01"/>
<wire x1="-35.56" y1="-58.42" x2="-10.16" y2="-58.42" width="0.1524" layer="91"/>
<label x="-33.02" y="-58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PC04" class="0">
<segment>
<pinref part="U1" gate="B" pin="PC04"/>
<wire x1="-35.56" y1="-66.04" x2="-10.16" y2="-66.04" width="0.1524" layer="91"/>
<label x="-33.02" y="-66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PC05" class="0">
<segment>
<pinref part="U1" gate="B" pin="PC05"/>
<wire x1="-35.56" y1="-68.58" x2="-10.16" y2="-68.58" width="0.1524" layer="91"/>
<label x="-33.02" y="-68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="EXTRA_PB19" class="0">
<segment>
<pinref part="U1" gate="B" pin="PB19"/>
<wire x1="-35.56" y1="-50.8" x2="-10.16" y2="-50.8" width="0.1524" layer="91"/>
<label x="-33.02" y="-50.8" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC3" gate="A" x="78.74" y="121.92"/>
<instance part="IC3" gate="B" x="78.74" y="86.36"/>
<instance part="IC3" gate="C" x="78.74" y="45.72"/>
<instance part="IC3" gate="D" x="78.74" y="7.62"/>
<instance part="IC12" gate="G$1" x="-30.48" y="142.24"/>
<instance part="GND22" gate="1" x="132.08" y="111.76"/>
<instance part="GND24" gate="1" x="58.42" y="106.68"/>
<instance part="GND27" gate="1" x="132.08" y="76.2"/>
<instance part="GND37" gate="1" x="58.42" y="71.12"/>
<instance part="GND38" gate="1" x="132.08" y="35.56"/>
<instance part="GND42" gate="1" x="58.42" y="30.48"/>
<instance part="GND43" gate="1" x="132.08" y="-2.54"/>
<instance part="GND44" gate="1" x="58.42" y="-7.62"/>
<instance part="GND45" gate="1" x="78.74" y="114.3" rot="R90"/>
<instance part="GND46" gate="1" x="-30.48" y="116.84"/>
<instance part="IC7" gate="G$1" x="111.76" y="147.32"/>
<instance part="P+40" gate="1" x="111.76" y="160.02"/>
<instance part="GND47" gate="1" x="111.76" y="137.16"/>
<instance part="GND48" gate="1" x="109.22" y="109.22"/>
<instance part="GND49" gate="1" x="109.22" y="73.66"/>
<instance part="GND50" gate="1" x="109.22" y="33.02"/>
<instance part="GND51" gate="1" x="109.22" y="-5.08"/>
<instance part="Z0" gate="G$1" x="109.22" y="116.84" rot="R90"/>
<instance part="Z1" gate="G$1" x="109.22" y="81.28" rot="R90"/>
<instance part="Z2" gate="G$1" x="109.22" y="40.64" rot="R90"/>
<instance part="Z3" gate="G$1" x="109.22" y="2.54" rot="R90"/>
<instance part="RA0" gate="G$1" x="60.96" y="132.08"/>
<instance part="RA1" gate="G$1" x="60.96" y="96.52"/>
<instance part="RA2" gate="G$1" x="60.96" y="55.88"/>
<instance part="RA3" gate="G$1" x="60.96" y="17.78"/>
<instance part="RB0" gate="G$1" x="78.74" y="132.08"/>
<instance part="RC0" gate="G$1" x="63.5" y="109.22"/>
<instance part="RD0" gate="G$1" x="78.74" y="109.22"/>
<instance part="RE0" gate="G$1" x="99.06" y="119.38"/>
<instance part="RC1" gate="G$1" x="63.5" y="73.66"/>
<instance part="RC2" gate="G$1" x="63.5" y="33.02"/>
<instance part="RB1" gate="G$1" x="78.74" y="96.52"/>
<instance part="RD1" gate="G$1" x="78.74" y="73.66"/>
<instance part="RB2" gate="G$1" x="78.74" y="55.88"/>
<instance part="RD2" gate="G$1" x="78.74" y="33.02"/>
<instance part="RE1" gate="G$1" x="99.06" y="83.82"/>
<instance part="RE2" gate="G$1" x="99.06" y="43.18"/>
<instance part="RB3" gate="G$1" x="78.74" y="17.78"/>
<instance part="RD3" gate="G$1" x="78.74" y="-5.08"/>
<instance part="RC3" gate="G$1" x="63.5" y="-5.08"/>
<instance part="RE3" gate="G$1" x="99.06" y="5.08"/>
<instance part="IC4" gate="A" x="78.74" y="-45.72"/>
<instance part="IC4" gate="B" x="78.74" y="-81.28"/>
<instance part="IC4" gate="C" x="78.74" y="-121.92"/>
<instance part="IC4" gate="D" x="78.74" y="-160.02"/>
<instance part="GND52" gate="1" x="58.42" y="-60.96"/>
<instance part="GND54" gate="1" x="58.42" y="-96.52"/>
<instance part="GND55" gate="1" x="58.42" y="-137.16"/>
<instance part="GND57" gate="1" x="58.42" y="-175.26"/>
<instance part="GND58" gate="1" x="78.74" y="-53.34" rot="R90"/>
<instance part="GND59" gate="1" x="109.22" y="-58.42"/>
<instance part="GND60" gate="1" x="109.22" y="-93.98"/>
<instance part="GND61" gate="1" x="109.22" y="-134.62"/>
<instance part="GND62" gate="1" x="109.22" y="-172.72"/>
<instance part="Z4" gate="G$1" x="109.22" y="-50.8" rot="R90"/>
<instance part="Z5" gate="G$1" x="109.22" y="-86.36" rot="R90"/>
<instance part="Z6" gate="G$1" x="109.22" y="-127" rot="R90"/>
<instance part="Z7" gate="G$1" x="109.22" y="-165.1" rot="R90"/>
<instance part="RA4" gate="G$1" x="60.96" y="-35.56"/>
<instance part="RA5" gate="G$1" x="60.96" y="-71.12"/>
<instance part="RA6" gate="G$1" x="60.96" y="-111.76"/>
<instance part="RA7" gate="G$1" x="60.96" y="-149.86"/>
<instance part="RB4" gate="G$1" x="78.74" y="-35.56"/>
<instance part="RC4" gate="G$1" x="63.5" y="-58.42"/>
<instance part="RD4" gate="G$1" x="78.74" y="-58.42"/>
<instance part="RE4" gate="G$1" x="99.06" y="-48.26"/>
<instance part="RC5" gate="G$1" x="63.5" y="-93.98"/>
<instance part="RC6" gate="G$1" x="63.5" y="-134.62"/>
<instance part="RB5" gate="G$1" x="78.74" y="-71.12"/>
<instance part="RD5" gate="G$1" x="78.74" y="-93.98"/>
<instance part="RB6" gate="G$1" x="78.74" y="-111.76"/>
<instance part="RD6" gate="G$1" x="78.74" y="-134.62"/>
<instance part="RE5" gate="G$1" x="99.06" y="-83.82"/>
<instance part="RE6" gate="G$1" x="99.06" y="-124.46"/>
<instance part="RB7" gate="G$1" x="78.74" y="-149.86"/>
<instance part="RD7" gate="G$1" x="78.74" y="-172.72"/>
<instance part="RC7" gate="G$1" x="63.5" y="-172.72"/>
<instance part="RE7" gate="G$1" x="99.06" y="-162.56"/>
<instance part="GND63" gate="1" x="132.08" y="-55.88"/>
<instance part="GND64" gate="1" x="132.08" y="-91.44"/>
<instance part="GND65" gate="1" x="132.08" y="-132.08"/>
<instance part="GND66" gate="1" x="132.08" y="-170.18"/>
<instance part="P+42" gate="1" x="78.74" y="129.54" rot="R270"/>
<instance part="P+43" gate="1" x="78.74" y="-38.1" rot="R270"/>
<instance part="P+45" gate="1" x="-40.64" y="71.12" rot="R90"/>
<instance part="C23" gate="G$1" x="-30.48" y="71.12" rot="R90"/>
<instance part="GND69" gate="1" x="-15.24" y="71.12" rot="R90"/>
<instance part="P+49" gate="1" x="-40.64" y="58.42" rot="R90"/>
<instance part="C24" gate="G$1" x="-30.48" y="58.42" rot="R90"/>
<instance part="GND96" gate="1" x="-15.24" y="58.42" rot="R90"/>
<instance part="GND68" gate="1" x="-15.24" y="91.44" rot="R90"/>
<instance part="C27" gate="G$1" x="-30.48" y="91.44" rot="R90"/>
<instance part="+3V3" gate="G$1" x="-30.48" y="167.64"/>
<instance part="+3V5" gate="G$1" x="-40.64" y="91.44" rot="R90"/>
<instance part="IC9" gate="G$1" x="71.12" y="198.12">
<attribute name="SHEETNO" x="71.12" y="198.12" size="1.778" layer="96" display="off"/>
<attribute name="DX" x="71.12" y="198.12" size="1.778" layer="96" display="off"/>
<attribute name="DY" x="71.12" y="198.12" size="1.778" layer="96" display="off"/>
<attribute name="DR" x="71.12" y="198.12" size="1.778" layer="96" display="off"/>
<attribute name="DM" x="71.12" y="198.12" size="1.778" layer="96" display="off"/>
</instance>
<instance part="SUPPLY15" gate="GND" x="27.94" y="185.42"/>
<instance part="SUPPLY42" gate="GND" x="38.1" y="185.42"/>
<instance part="SUPPLY56" gate="GND" x="101.6" y="185.42"/>
<instance part="+3V27" gate="G$1" x="27.94" y="205.74"/>
<instance part="+3V28" gate="G$1" x="58.42" y="193.04" rot="R90"/>
<instance part="R101" gate="G$1" x="-78.74" y="149.86"/>
<instance part="R102" gate="G$1" x="-78.74" y="144.78"/>
<instance part="R103" gate="G$1" x="-78.74" y="132.08"/>
<instance part="+3V114" gate="G$1" x="-86.36" y="149.86" rot="R90"/>
<instance part="+3V115" gate="G$1" x="-86.36" y="144.78" rot="R90"/>
<instance part="+3V116" gate="G$1" x="-86.36" y="132.08" rot="R90"/>
<instance part="C81" gate="G$1" x="116.84" y="195.58"/>
<instance part="C82" gate="G$1" x="127" y="195.58"/>
<instance part="GND84" gate="1" x="116.84" y="187.96"/>
<instance part="GND233" gate="1" x="127" y="187.96"/>
<instance part="XV0" gate="1" x="134.62" y="121.92"/>
<instance part="XV0" gate="2" x="134.62" y="119.38"/>
<instance part="XV0" gate="3" x="134.62" y="116.84"/>
<instance part="XV0" gate="4" x="134.62" y="114.3"/>
<instance part="XV1" gate="1" x="134.62" y="86.36"/>
<instance part="XV1" gate="2" x="134.62" y="83.82"/>
<instance part="XV1" gate="3" x="134.62" y="81.28"/>
<instance part="XV1" gate="4" x="134.62" y="78.74"/>
<instance part="XV2" gate="1" x="134.62" y="45.72"/>
<instance part="XV2" gate="2" x="134.62" y="43.18"/>
<instance part="XV2" gate="3" x="134.62" y="40.64"/>
<instance part="XV2" gate="4" x="134.62" y="38.1"/>
<instance part="XV3" gate="1" x="134.62" y="7.62"/>
<instance part="XV3" gate="2" x="134.62" y="5.08"/>
<instance part="XV3" gate="3" x="134.62" y="2.54"/>
<instance part="XV3" gate="4" x="134.62" y="0"/>
<instance part="XV4" gate="1" x="134.62" y="-45.72"/>
<instance part="XV4" gate="2" x="134.62" y="-48.26"/>
<instance part="XV4" gate="3" x="134.62" y="-50.8"/>
<instance part="XV4" gate="4" x="134.62" y="-53.34"/>
<instance part="XV5" gate="1" x="134.62" y="-81.28"/>
<instance part="XV5" gate="2" x="134.62" y="-83.82"/>
<instance part="XV5" gate="3" x="134.62" y="-86.36"/>
<instance part="XV5" gate="4" x="134.62" y="-88.9"/>
<instance part="XV6" gate="1" x="134.62" y="-121.92"/>
<instance part="XV6" gate="2" x="134.62" y="-124.46"/>
<instance part="XV6" gate="3" x="134.62" y="-127"/>
<instance part="XV6" gate="4" x="134.62" y="-129.54"/>
<instance part="XV7" gate="1" x="134.62" y="-160.02"/>
<instance part="XV7" gate="2" x="134.62" y="-162.56"/>
<instance part="XV7" gate="3" x="134.62" y="-165.1"/>
<instance part="XV7" gate="4" x="134.62" y="-167.64"/>
<instance part="C22" gate="G$1" x="101.6" y="195.58"/>
<instance part="C25" gate="G$1" x="27.94" y="198.12"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<pinref part="IC3" gate="A" pin="V-"/>
<pinref part="GND45" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="GND"/>
<pinref part="GND46" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="IC7" gate="G$1" pin="GND"/>
<pinref part="GND47" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z0" gate="G$1" pin="A"/>
<pinref part="GND48" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND49" gate="1" pin="GND"/>
<pinref part="Z1" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND50" gate="1" pin="GND"/>
<pinref part="Z2" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND51" gate="1" pin="GND"/>
<pinref part="Z3" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND24" gate="1" pin="GND"/>
<pinref part="RC0" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND37" gate="1" pin="GND"/>
<pinref part="RC1" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND42" gate="1" pin="GND"/>
<pinref part="RC2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND44" gate="1" pin="GND"/>
<pinref part="RC3" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="V-"/>
<pinref part="GND58" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z4" gate="G$1" pin="A"/>
<pinref part="GND59" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND60" gate="1" pin="GND"/>
<pinref part="Z5" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND61" gate="1" pin="GND"/>
<pinref part="Z6" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND62" gate="1" pin="GND"/>
<pinref part="Z7" gate="G$1" pin="A"/>
</segment>
<segment>
<pinref part="GND52" gate="1" pin="GND"/>
<pinref part="RC4" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND54" gate="1" pin="GND"/>
<pinref part="RC5" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND55" gate="1" pin="GND"/>
<pinref part="RC6" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="GND57" gate="1" pin="GND"/>
<pinref part="RC7" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<pinref part="GND69" gate="1" pin="GND"/>
<wire x1="-25.4" y1="71.12" x2="-17.78" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C24" gate="G$1" pin="2"/>
<pinref part="GND96" gate="1" pin="GND"/>
<wire x1="-25.4" y1="58.42" x2="-17.78" y2="58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND68" gate="1" pin="GND"/>
<wire x1="-25.4" y1="91.44" x2="-17.78" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="101.6" y1="187.96" x2="101.6" y2="190.5" width="0.1524" layer="91"/>
<pinref part="SUPPLY56" gate="GND" pin="GND"/>
<pinref part="C22" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="60.96" y1="198.12" x2="38.1" y2="198.12" width="0.1524" layer="91"/>
<wire x1="38.1" y1="198.12" x2="38.1" y2="187.96" width="0.1524" layer="91"/>
<label x="43.18" y="198.12" size="1.778" layer="95"/>
<pinref part="IC9" gate="G$1" pin="GND"/>
<pinref part="SUPPLY42" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="27.94" y1="187.96" x2="27.94" y2="193.04" width="0.1524" layer="91"/>
<pinref part="SUPPLY15" gate="GND" pin="GND"/>
<pinref part="C25" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="C81" gate="G$1" pin="2"/>
<pinref part="GND84" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C82" gate="G$1" pin="2"/>
<pinref part="GND233" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND22" gate="1" pin="GND"/>
<pinref part="XV0" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND27" gate="1" pin="GND"/>
<pinref part="XV1" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND38" gate="1" pin="GND"/>
<pinref part="XV2" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND43" gate="1" pin="GND"/>
<pinref part="XV3" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND63" gate="1" pin="GND"/>
<pinref part="XV4" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND64" gate="1" pin="GND"/>
<pinref part="XV5" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND65" gate="1" pin="GND"/>
<pinref part="XV6" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND66" gate="1" pin="GND"/>
<pinref part="XV7" gate="4" pin="S"/>
</segment>
</net>
<net name="+24V" class="7">
<segment>
<pinref part="IC7" gate="G$1" pin="VBB"/>
<pinref part="P+40" gate="1" pin="+24V"/>
<wire x1="111.76" y1="157.48" x2="111.76" y2="154.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC3" gate="A" pin="V+"/>
<pinref part="P+42" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="IC4" gate="A" pin="V+"/>
<pinref part="P+43" gate="1" pin="+24V"/>
</segment>
<segment>
<pinref part="P+45" gate="1" pin="+24V"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="71.12" x2="-33.02" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+49" gate="1" pin="+24V"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="58.42" x2="-33.02" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="71.12" y1="119.38" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<wire x1="71.12" y1="109.22" x2="73.66" y2="109.22" width="0.1524" layer="91"/>
<wire x1="68.58" y1="109.22" x2="71.12" y2="109.22" width="0.1524" layer="91"/>
<junction x="71.12" y="109.22"/>
<pinref part="IC3" gate="A" pin="-IN"/>
<pinref part="RC0" gate="G$1" pin="2"/>
<pinref part="RD0" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="132.08" y1="119.38" x2="109.22" y2="119.38" width="0.1524" layer="91"/>
<wire x1="109.22" y1="119.38" x2="104.14" y2="119.38" width="0.1524" layer="91"/>
<junction x="109.22" y="119.38"/>
<pinref part="Z0" gate="G$1" pin="C"/>
<pinref part="RE0" gate="G$1" pin="2"/>
<pinref part="XV0" gate="2" pin="S"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="116.84" y1="116.84" x2="132.08" y2="116.84" width="0.1524" layer="91"/>
<wire x1="83.82" y1="132.08" x2="116.84" y2="132.08" width="0.1524" layer="91"/>
<wire x1="116.84" y1="132.08" x2="116.84" y2="116.84" width="0.1524" layer="91"/>
<pinref part="RB0" gate="G$1" pin="2"/>
<pinref part="XV0" gate="3" pin="S"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<wire x1="73.66" y1="132.08" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="132.08" x2="66.04" y2="132.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="124.46" x2="71.12" y2="132.08" width="0.1524" layer="91"/>
<junction x="71.12" y="132.08"/>
<pinref part="IC3" gate="A" pin="+IN"/>
<pinref part="RA0" gate="G$1" pin="2"/>
<pinref part="RB0" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="55.88" y1="132.08" x2="50.8" y2="132.08" width="0.1524" layer="91"/>
<wire x1="50.8" y1="132.08" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="152.4" x2="2.54" y2="152.4" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO0"/>
<pinref part="RA0" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<wire x1="71.12" y1="83.82" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<wire x1="71.12" y1="73.66" x2="73.66" y2="73.66" width="0.1524" layer="91"/>
<wire x1="68.58" y1="73.66" x2="71.12" y2="73.66" width="0.1524" layer="91"/>
<junction x="71.12" y="73.66"/>
<pinref part="IC3" gate="B" pin="-IN"/>
<pinref part="RC1" gate="G$1" pin="2"/>
<pinref part="RD1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<wire x1="132.08" y1="83.82" x2="109.22" y2="83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="83.82" x2="104.14" y2="83.82" width="0.1524" layer="91"/>
<junction x="109.22" y="83.82"/>
<pinref part="Z1" gate="G$1" pin="C"/>
<pinref part="RE1" gate="G$1" pin="2"/>
<pinref part="XV1" gate="2" pin="S"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="116.84" y1="81.28" x2="132.08" y2="81.28" width="0.1524" layer="91"/>
<wire x1="83.82" y1="96.52" x2="116.84" y2="96.52" width="0.1524" layer="91"/>
<wire x1="116.84" y1="96.52" x2="116.84" y2="81.28" width="0.1524" layer="91"/>
<pinref part="RB1" gate="G$1" pin="2"/>
<pinref part="XV1" gate="3" pin="S"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<wire x1="73.66" y1="96.52" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="96.52" x2="66.04" y2="96.52" width="0.1524" layer="91"/>
<wire x1="71.12" y1="88.9" x2="71.12" y2="96.52" width="0.1524" layer="91"/>
<junction x="71.12" y="96.52"/>
<pinref part="IC3" gate="B" pin="+IN"/>
<pinref part="RA1" gate="G$1" pin="2"/>
<pinref part="RB1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="55.88" y1="96.52" x2="48.26" y2="96.52" width="0.1524" layer="91"/>
<wire x1="48.26" y1="96.52" x2="48.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="48.26" y1="149.86" x2="2.54" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO1"/>
<pinref part="RA1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="71.12" y1="43.18" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<wire x1="71.12" y1="33.02" x2="73.66" y2="33.02" width="0.1524" layer="91"/>
<wire x1="68.58" y1="33.02" x2="71.12" y2="33.02" width="0.1524" layer="91"/>
<junction x="71.12" y="33.02"/>
<pinref part="IC3" gate="C" pin="-IN"/>
<pinref part="RC2" gate="G$1" pin="2"/>
<pinref part="RD2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="132.08" y1="43.18" x2="109.22" y2="43.18" width="0.1524" layer="91"/>
<wire x1="109.22" y1="43.18" x2="104.14" y2="43.18" width="0.1524" layer="91"/>
<junction x="109.22" y="43.18"/>
<pinref part="Z2" gate="G$1" pin="C"/>
<pinref part="RE2" gate="G$1" pin="2"/>
<pinref part="XV2" gate="2" pin="S"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="116.84" y1="40.64" x2="132.08" y2="40.64" width="0.1524" layer="91"/>
<wire x1="83.82" y1="55.88" x2="116.84" y2="55.88" width="0.1524" layer="91"/>
<wire x1="116.84" y1="55.88" x2="116.84" y2="40.64" width="0.1524" layer="91"/>
<pinref part="RB2" gate="G$1" pin="2"/>
<pinref part="XV2" gate="3" pin="S"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="73.66" y1="55.88" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="55.88" x2="66.04" y2="55.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="48.26" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="71.12" y="55.88"/>
<pinref part="IC3" gate="C" pin="+IN"/>
<pinref part="RA2" gate="G$1" pin="2"/>
<pinref part="RB2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<wire x1="55.88" y1="55.88" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="45.72" y1="55.88" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="147.32" x2="2.54" y2="147.32" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO2"/>
<pinref part="RA2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="71.12" y1="5.08" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-5.08" x2="73.66" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-5.08" x2="71.12" y2="-5.08" width="0.1524" layer="91"/>
<junction x="71.12" y="-5.08"/>
<pinref part="IC3" gate="D" pin="-IN"/>
<pinref part="RD3" gate="G$1" pin="1"/>
<pinref part="RC3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="86.36" y1="7.62" x2="91.44" y2="7.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="7.62" x2="91.44" y2="5.08" width="0.1524" layer="91"/>
<wire x1="93.98" y1="5.08" x2="91.44" y2="5.08" width="0.1524" layer="91"/>
<wire x1="91.44" y1="5.08" x2="91.44" y2="-5.08" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-5.08" x2="83.82" y2="-5.08" width="0.1524" layer="91"/>
<junction x="91.44" y="5.08"/>
<pinref part="IC3" gate="D" pin="OUT"/>
<pinref part="RD3" gate="G$1" pin="2"/>
<pinref part="RE3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<wire x1="116.84" y1="2.54" x2="132.08" y2="2.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="17.78" x2="116.84" y2="17.78" width="0.1524" layer="91"/>
<wire x1="116.84" y1="17.78" x2="116.84" y2="2.54" width="0.1524" layer="91"/>
<pinref part="RB3" gate="G$1" pin="2"/>
<pinref part="XV3" gate="3" pin="S"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="73.66" y1="17.78" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="17.78" x2="66.04" y2="17.78" width="0.1524" layer="91"/>
<wire x1="71.12" y1="10.16" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
<junction x="71.12" y="17.78"/>
<pinref part="IC3" gate="D" pin="+IN"/>
<pinref part="RA3" gate="G$1" pin="2"/>
<pinref part="RB3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="55.88" y1="17.78" x2="43.18" y2="17.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="17.78" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
<wire x1="43.18" y1="144.78" x2="2.54" y2="144.78" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO3"/>
<pinref part="RA3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="40.64" y1="-35.56" x2="40.64" y2="142.24" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO4"/>
<wire x1="40.64" y1="142.24" x2="2.54" y2="142.24" width="0.1524" layer="91"/>
<pinref part="RA4" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-35.56" x2="40.64" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<wire x1="38.1" y1="-71.12" x2="38.1" y2="139.7" width="0.1524" layer="91"/>
<wire x1="38.1" y1="139.7" x2="2.54" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO5"/>
<pinref part="RA5" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-71.12" x2="38.1" y2="-71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<wire x1="35.56" y1="-111.76" x2="35.56" y2="137.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="137.16" x2="2.54" y2="137.16" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO6"/>
<pinref part="RA6" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-111.76" x2="35.56" y2="-111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<wire x1="33.02" y1="-149.86" x2="33.02" y2="134.62" width="0.1524" layer="91"/>
<wire x1="33.02" y1="134.62" x2="2.54" y2="134.62" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="VO7"/>
<pinref part="RA7" gate="G$1" pin="1"/>
<wire x1="55.88" y1="-149.86" x2="33.02" y2="-149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC0_DIN" class="0">
<segment>
<wire x1="-73.66" y1="139.7" x2="-50.8" y2="139.7" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="DIN"/>
<label x="-73.66" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="DAC_/SYNC" class="0">
<segment>
<wire x1="-73.66" y1="144.78" x2="-50.8" y2="144.78" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="/SYNC"/>
<label x="-73.66" y="144.78" size="1.778" layer="95"/>
<pinref part="R102" gate="G$1" pin="2"/>
</segment>
</net>
<net name="DAC_CLK" class="0">
<segment>
<wire x1="-73.66" y1="149.86" x2="-50.8" y2="149.86" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="SCLK"/>
<label x="-73.66" y="149.86" size="1.778" layer="95"/>
<pinref part="R101" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VPWR1" class="7">
<segment>
<wire x1="119.38" y1="147.32" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
<wire x1="121.92" y1="147.32" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="132.08" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="121.92" y1="121.92" x2="121.92" y2="86.36" width="0.1524" layer="91"/>
<wire x1="132.08" y1="86.36" x2="121.92" y2="86.36" width="0.1524" layer="91"/>
<wire x1="121.92" y1="86.36" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="132.08" y1="45.72" x2="121.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="45.72" x2="121.92" y2="7.62" width="0.1524" layer="91"/>
<wire x1="121.92" y1="7.62" x2="132.08" y2="7.62" width="0.1524" layer="91"/>
<junction x="121.92" y="121.92"/>
<junction x="121.92" y="86.36"/>
<junction x="121.92" y="45.72"/>
<pinref part="IC7" gate="G$1" pin="OUT"/>
<label x="121.92" y="147.32" size="1.778" layer="95"/>
<pinref part="XV0" gate="1" pin="S"/>
<pinref part="XV1" gate="1" pin="S"/>
<pinref part="XV2" gate="1" pin="S"/>
<pinref part="XV3" gate="1" pin="S"/>
<wire x1="121.92" y1="-160.02" x2="132.08" y2="-160.02" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-45.72" x2="132.08" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-45.72" x2="121.92" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-81.28" x2="132.08" y2="-81.28" width="0.1524" layer="91"/>
<junction x="121.92" y="-81.28"/>
<wire x1="121.92" y1="-81.28" x2="121.92" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="121.92" y1="-121.92" x2="121.92" y2="-160.02" width="0.1524" layer="91"/>
<junction x="121.92" y="-121.92"/>
<wire x1="121.92" y1="-121.92" x2="132.08" y2="-121.92" width="0.1524" layer="91"/>
<junction x="121.92" y="-45.72"/>
<label x="121.92" y="-20.32" size="1.778" layer="95"/>
<pinref part="XV4" gate="1" pin="S"/>
<pinref part="XV5" gate="1" pin="S"/>
<pinref part="XV6" gate="1" pin="S"/>
<pinref part="XV7" gate="1" pin="S"/>
<wire x1="121.92" y1="7.62" x2="121.92" y2="-45.72" width="0.1524" layer="91"/>
<junction x="121.92" y="7.62"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="86.36" y1="121.92" x2="91.44" y2="121.92" width="0.1524" layer="91"/>
<wire x1="91.44" y1="121.92" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<wire x1="93.98" y1="119.38" x2="91.44" y2="119.38" width="0.1524" layer="91"/>
<wire x1="91.44" y1="119.38" x2="91.44" y2="109.22" width="0.1524" layer="91"/>
<wire x1="91.44" y1="109.22" x2="83.82" y2="109.22" width="0.1524" layer="91"/>
<junction x="91.44" y="119.38"/>
<pinref part="IC3" gate="A" pin="OUT"/>
<pinref part="RD0" gate="G$1" pin="2"/>
<pinref part="RE0" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<wire x1="132.08" y1="5.08" x2="109.22" y2="5.08" width="0.1524" layer="91"/>
<wire x1="109.22" y1="5.08" x2="104.14" y2="5.08" width="0.1524" layer="91"/>
<junction x="109.22" y="5.08"/>
<pinref part="Z3" gate="G$1" pin="C"/>
<pinref part="RE3" gate="G$1" pin="2"/>
<pinref part="XV3" gate="2" pin="S"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<wire x1="86.36" y1="45.72" x2="91.44" y2="45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="45.72" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<wire x1="93.98" y1="43.18" x2="91.44" y2="43.18" width="0.1524" layer="91"/>
<wire x1="91.44" y1="43.18" x2="91.44" y2="33.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="33.02" x2="83.82" y2="33.02" width="0.1524" layer="91"/>
<junction x="91.44" y="43.18"/>
<pinref part="IC3" gate="C" pin="OUT"/>
<pinref part="RD2" gate="G$1" pin="2"/>
<pinref part="RE2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<wire x1="86.36" y1="86.36" x2="91.44" y2="86.36" width="0.1524" layer="91"/>
<wire x1="91.44" y1="86.36" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="93.98" y1="83.82" x2="91.44" y2="83.82" width="0.1524" layer="91"/>
<wire x1="91.44" y1="83.82" x2="91.44" y2="73.66" width="0.1524" layer="91"/>
<wire x1="91.44" y1="73.66" x2="83.82" y2="73.66" width="0.1524" layer="91"/>
<junction x="91.44" y="83.82"/>
<pinref part="IC3" gate="B" pin="OUT"/>
<pinref part="RD1" gate="G$1" pin="2"/>
<pinref part="RE1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="71.12" y1="-48.26" x2="71.12" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-58.42" x2="73.66" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-58.42" x2="71.12" y2="-58.42" width="0.1524" layer="91"/>
<junction x="71.12" y="-58.42"/>
<pinref part="IC4" gate="A" pin="-IN"/>
<pinref part="RC4" gate="G$1" pin="2"/>
<pinref part="RD4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="132.08" y1="-48.26" x2="109.22" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-48.26" x2="104.14" y2="-48.26" width="0.1524" layer="91"/>
<junction x="109.22" y="-48.26"/>
<pinref part="Z4" gate="G$1" pin="C"/>
<pinref part="RE4" gate="G$1" pin="2"/>
<pinref part="XV4" gate="2" pin="S"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="116.84" y1="-50.8" x2="132.08" y2="-50.8" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-35.56" x2="116.84" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-35.56" x2="116.84" y2="-50.8" width="0.1524" layer="91"/>
<pinref part="RB4" gate="G$1" pin="2"/>
<pinref part="XV4" gate="3" pin="S"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="73.66" y1="-35.56" x2="71.12" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-35.56" x2="66.04" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-43.18" x2="71.12" y2="-35.56" width="0.1524" layer="91"/>
<junction x="71.12" y="-35.56"/>
<pinref part="IC4" gate="A" pin="+IN"/>
<pinref part="RA4" gate="G$1" pin="2"/>
<pinref part="RB4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<wire x1="71.12" y1="-83.82" x2="71.12" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-93.98" x2="73.66" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-93.98" x2="71.12" y2="-93.98" width="0.1524" layer="91"/>
<junction x="71.12" y="-93.98"/>
<pinref part="IC4" gate="B" pin="-IN"/>
<pinref part="RC5" gate="G$1" pin="2"/>
<pinref part="RD5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="132.08" y1="-83.82" x2="109.22" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-83.82" x2="104.14" y2="-83.82" width="0.1524" layer="91"/>
<junction x="109.22" y="-83.82"/>
<pinref part="Z5" gate="G$1" pin="C"/>
<pinref part="RE5" gate="G$1" pin="2"/>
<pinref part="XV5" gate="2" pin="S"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="116.84" y1="-86.36" x2="132.08" y2="-86.36" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-71.12" x2="116.84" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-71.12" x2="116.84" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="RB5" gate="G$1" pin="2"/>
<pinref part="XV5" gate="3" pin="S"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<wire x1="73.66" y1="-71.12" x2="71.12" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-71.12" x2="66.04" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-78.74" x2="71.12" y2="-71.12" width="0.1524" layer="91"/>
<junction x="71.12" y="-71.12"/>
<pinref part="IC4" gate="B" pin="+IN"/>
<pinref part="RA5" gate="G$1" pin="2"/>
<pinref part="RB5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<wire x1="71.12" y1="-124.46" x2="71.12" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-134.62" x2="73.66" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-134.62" x2="71.12" y2="-134.62" width="0.1524" layer="91"/>
<junction x="71.12" y="-134.62"/>
<pinref part="IC4" gate="C" pin="-IN"/>
<pinref part="RC6" gate="G$1" pin="2"/>
<pinref part="RD6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<wire x1="132.08" y1="-124.46" x2="109.22" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-124.46" x2="104.14" y2="-124.46" width="0.1524" layer="91"/>
<junction x="109.22" y="-124.46"/>
<pinref part="Z6" gate="G$1" pin="C"/>
<pinref part="RE6" gate="G$1" pin="2"/>
<pinref part="XV6" gate="2" pin="S"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<wire x1="116.84" y1="-127" x2="132.08" y2="-127" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-111.76" x2="116.84" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-111.76" x2="116.84" y2="-127" width="0.1524" layer="91"/>
<pinref part="RB6" gate="G$1" pin="2"/>
<pinref part="XV6" gate="3" pin="S"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<wire x1="73.66" y1="-111.76" x2="71.12" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-111.76" x2="66.04" y2="-111.76" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-119.38" x2="71.12" y2="-111.76" width="0.1524" layer="91"/>
<junction x="71.12" y="-111.76"/>
<pinref part="IC4" gate="C" pin="+IN"/>
<pinref part="RA6" gate="G$1" pin="2"/>
<pinref part="RB6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="71.12" y1="-162.56" x2="71.12" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-172.72" x2="73.66" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="68.58" y1="-172.72" x2="71.12" y2="-172.72" width="0.1524" layer="91"/>
<junction x="71.12" y="-172.72"/>
<pinref part="IC4" gate="D" pin="-IN"/>
<pinref part="RD7" gate="G$1" pin="1"/>
<pinref part="RC7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="86.36" y1="-160.02" x2="91.44" y2="-160.02" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-160.02" x2="91.44" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-162.56" x2="91.44" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-162.56" x2="91.44" y2="-172.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-172.72" x2="83.82" y2="-172.72" width="0.1524" layer="91"/>
<junction x="91.44" y="-162.56"/>
<pinref part="IC4" gate="D" pin="OUT"/>
<pinref part="RD7" gate="G$1" pin="2"/>
<pinref part="RE7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="116.84" y1="-165.1" x2="132.08" y2="-165.1" width="0.1524" layer="91"/>
<wire x1="83.82" y1="-149.86" x2="116.84" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="-149.86" x2="116.84" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="RB7" gate="G$1" pin="2"/>
<pinref part="XV7" gate="3" pin="S"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="73.66" y1="-149.86" x2="71.12" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-149.86" x2="66.04" y2="-149.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="-157.48" x2="71.12" y2="-149.86" width="0.1524" layer="91"/>
<junction x="71.12" y="-149.86"/>
<pinref part="IC4" gate="D" pin="+IN"/>
<pinref part="RA7" gate="G$1" pin="2"/>
<pinref part="RB7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="86.36" y1="-45.72" x2="91.44" y2="-45.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-45.72" x2="91.44" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-48.26" x2="91.44" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-48.26" x2="91.44" y2="-58.42" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-58.42" x2="83.82" y2="-58.42" width="0.1524" layer="91"/>
<junction x="91.44" y="-48.26"/>
<pinref part="IC4" gate="A" pin="OUT"/>
<pinref part="RD4" gate="G$1" pin="2"/>
<pinref part="RE4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="132.08" y1="-162.56" x2="109.22" y2="-162.56" width="0.1524" layer="91"/>
<wire x1="109.22" y1="-162.56" x2="104.14" y2="-162.56" width="0.1524" layer="91"/>
<junction x="109.22" y="-162.56"/>
<pinref part="Z7" gate="G$1" pin="C"/>
<pinref part="RE7" gate="G$1" pin="2"/>
<pinref part="XV7" gate="2" pin="S"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<wire x1="86.36" y1="-121.92" x2="91.44" y2="-121.92" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-121.92" x2="91.44" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-124.46" x2="91.44" y2="-124.46" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-124.46" x2="91.44" y2="-134.62" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-134.62" x2="83.82" y2="-134.62" width="0.1524" layer="91"/>
<junction x="91.44" y="-124.46"/>
<pinref part="IC4" gate="C" pin="OUT"/>
<pinref part="RD6" gate="G$1" pin="2"/>
<pinref part="RE6" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<wire x1="86.36" y1="-81.28" x2="91.44" y2="-81.28" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-81.28" x2="91.44" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="93.98" y1="-83.82" x2="91.44" y2="-83.82" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-83.82" x2="91.44" y2="-93.98" width="0.1524" layer="91"/>
<wire x1="91.44" y1="-93.98" x2="83.82" y2="-93.98" width="0.1524" layer="91"/>
<junction x="91.44" y="-83.82"/>
<pinref part="IC4" gate="B" pin="OUT"/>
<pinref part="RD5" gate="G$1" pin="2"/>
<pinref part="RE5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="DAC_/LDAC" class="0">
<segment>
<wire x1="-73.66" y1="132.08" x2="-50.8" y2="132.08" width="0.1524" layer="91"/>
<pinref part="IC12" gate="G$1" pin="/LDAC"/>
<label x="-73.66" y="132.08" size="1.778" layer="95"/>
<pinref part="R103" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VPWR_EN" class="0">
<segment>
<wire x1="104.14" y1="149.86" x2="81.28" y2="149.86" width="0.1524" layer="91"/>
<label x="81.28" y="149.86" size="1.778" layer="95"/>
<pinref part="IC7" gate="G$1" pin="IN"/>
</segment>
</net>
<net name="VPWR_ST" class="0">
<segment>
<wire x1="104.14" y1="144.78" x2="81.28" y2="144.78" width="0.1524" layer="91"/>
<label x="81.28" y="144.78" size="1.778" layer="95"/>
<pinref part="IC7" gate="G$1" pin="ST"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="VCC"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="-38.1" y1="91.44" x2="-33.02" y2="91.44" width="0.1524" layer="91"/>
<pinref part="+3V5" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="60.96" y1="203.2" x2="27.94" y2="203.2" width="0.1524" layer="91"/>
<wire x1="27.94" y1="203.2" x2="27.94" y2="200.66" width="0.1524" layer="91"/>
<label x="43.18" y="203.2" size="1.778" layer="95"/>
<pinref part="IC9" gate="G$1" pin="V_IN"/>
<pinref part="+3V27" gate="G$1" pin="+3V3"/>
<junction x="27.94" y="203.2"/>
<pinref part="C25" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="IC9" gate="G$1" pin="EN"/>
<pinref part="+3V28" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R101" gate="G$1" pin="1"/>
<pinref part="+3V114" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R102" gate="G$1" pin="1"/>
<pinref part="+3V115" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R103" gate="G$1" pin="1"/>
<pinref part="+3V116" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="DAC_VREF" class="0">
<segment>
<pinref part="IC12" gate="G$1" pin="VREF0"/>
<wire x1="-20.32" y1="165.1" x2="-20.32" y2="175.26" width="0.1524" layer="91"/>
<label x="-20.32" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="IC12" gate="G$1" pin="VREF4"/>
<wire x1="-15.24" y1="165.1" x2="-15.24" y2="175.26" width="0.1524" layer="91"/>
<label x="-15.24" y="167.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="101.6" y1="198.12" x2="101.6" y2="200.66" width="0.1524" layer="91"/>
<wire x1="101.6" y1="200.66" x2="81.28" y2="200.66" width="0.1524" layer="91"/>
<label x="88.9" y="200.66" size="1.778" layer="95"/>
<pinref part="IC9" gate="G$1" pin="VCC"/>
<pinref part="C81" gate="G$1" pin="1"/>
<wire x1="101.6" y1="200.66" x2="116.84" y2="200.66" width="0.1524" layer="91"/>
<wire x1="116.84" y1="200.66" x2="116.84" y2="198.12" width="0.1524" layer="91"/>
<pinref part="C82" gate="G$1" pin="1"/>
<wire x1="116.84" y1="200.66" x2="127" y2="200.66" width="0.1524" layer="91"/>
<wire x1="127" y1="200.66" x2="127" y2="198.12" width="0.1524" layer="91"/>
<junction x="101.6" y="200.66"/>
<junction x="116.84" y="200.66"/>
<pinref part="C22" gate="G$1" pin="1"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="C42" gate="G$1" x="27.94" y="63.5">
<attribute name="SHEETNO" x="27.94" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="DX" x="27.94" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="DY" x="27.94" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="DR" x="27.94" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="DM" x="27.94" y="63.5" size="1.778" layer="96" display="off"/>
<attribute name="MANUF" x="27.94" y="63.5" size="1.778" layer="96"/>
</instance>
<instance part="C44" gate="G$1" x="162.56" y="60.96">
<attribute name="SHEETNO" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="DX" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="DY" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="DR" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="DM" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
<attribute name="MANUF" x="162.56" y="60.96" size="1.778" layer="96" display="off"/>
</instance>
<instance part="SUPPLY49" gate="GND" x="27.94" y="53.34"/>
<instance part="SUPPLY54" gate="GND" x="162.56" y="50.8"/>
<instance part="P+53" gate="1" x="-25.4" y="68.58" rot="R90"/>
<instance part="IC21" gate="G$1" x="73.66" y="55.88"/>
<instance part="C28" gate="G$1" x="96.52" y="63.5" rot="R90"/>
<instance part="GND70" gate="1" x="71.12" y="25.4"/>
<instance part="GND71" gate="1" x="73.66" y="25.4"/>
<instance part="GND72" gate="1" x="76.2" y="25.4"/>
<instance part="GND73" gate="1" x="96.52" y="48.26" rot="R90"/>
<instance part="R22" gate="G$1" x="111.76" y="38.1"/>
<instance part="R23" gate="G$1" x="99.06" y="30.48" rot="R90"/>
<instance part="GND88" gate="1" x="99.06" y="20.32"/>
<instance part="R24" gate="G$1" x="40.64" y="30.48" rot="R270"/>
<instance part="C29" gate="G$1" x="40.64" y="15.24"/>
<instance part="GND89" gate="1" x="40.64" y="2.54"/>
<instance part="R25" gate="G$1" x="48.26" y="30.48" rot="R270"/>
<instance part="L3" gate="G$1" x="114.3" y="58.42"/>
<instance part="XPWR" gate="1" x="-12.7" y="127" rot="R180"/>
<instance part="XPWR" gate="2" x="-12.7" y="119.38" rot="R180"/>
<instance part="XPWR" gate="3" x="-12.7" y="114.3" rot="R180"/>
<instance part="XPWR" gate="4" x="-12.7" y="106.68" rot="R180"/>
<instance part="+3V2" gate="G$1" x="182.88" y="68.58" rot="R270"/>
<instance part="C58" gate="G$1" x="27.94" y="-22.86">
<attribute name="SHEETNO" x="27.94" y="-22.86" size="1.778" layer="96" display="off"/>
<attribute name="DX" x="27.94" y="-22.86" size="1.778" layer="96" display="off"/>
<attribute name="DY" x="27.94" y="-22.86" size="1.778" layer="96" display="off"/>
<attribute name="DR" x="27.94" y="-22.86" size="1.778" layer="96" display="off"/>
<attribute name="DM" x="27.94" y="-22.86" size="1.778" layer="96" display="off"/>
<attribute name="MANUF" x="27.94" y="-22.86" size="1.778" layer="96"/>
</instance>
<instance part="C59" gate="G$1" x="162.56" y="-25.4">
<attribute name="SHEETNO" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
<attribute name="DX" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
<attribute name="DY" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
<attribute name="DR" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
<attribute name="DM" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
<attribute name="MANUF" x="162.56" y="-25.4" size="1.778" layer="96" display="off"/>
</instance>
<instance part="SUPPLY1" gate="GND" x="27.94" y="-33.02"/>
<instance part="SUPPLY2" gate="GND" x="162.56" y="-35.56"/>
<instance part="P+83" gate="1" x="-25.4" y="-17.78" rot="R90"/>
<instance part="IC10" gate="G$1" x="73.66" y="-30.48"/>
<instance part="C60" gate="G$1" x="96.52" y="-22.86" rot="R90"/>
<instance part="GND191" gate="1" x="71.12" y="-60.96"/>
<instance part="GND192" gate="1" x="73.66" y="-60.96"/>
<instance part="GND193" gate="1" x="76.2" y="-60.96"/>
<instance part="GND194" gate="1" x="96.52" y="-38.1" rot="R90"/>
<instance part="R82" gate="G$1" x="111.76" y="-48.26"/>
<instance part="R83" gate="G$1" x="99.06" y="-55.88" rot="R90"/>
<instance part="GND195" gate="1" x="99.06" y="-66.04"/>
<instance part="R84" gate="G$1" x="40.64" y="-55.88" rot="R270"/>
<instance part="C61" gate="G$1" x="40.64" y="-71.12"/>
<instance part="GND196" gate="1" x="40.64" y="-83.82"/>
<instance part="R85" gate="G$1" x="48.26" y="-55.88" rot="R270"/>
<instance part="L1" gate="G$1" x="114.3" y="-27.94"/>
<instance part="P+84" gate="1" x="182.88" y="-17.78" rot="R270"/>
<instance part="FB25" gate="G$1" x="48.26" y="127"/>
<instance part="FB26" gate="G$1" x="48.26" y="114.3"/>
<instance part="C247" gate="G$1" x="38.1" y="121.92">
<attribute name="SHEETNO" x="38.1" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DX" x="38.1" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DY" x="38.1" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DR" x="38.1" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DM" x="38.1" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="C250" gate="G$1" x="60.96" y="121.92">
<attribute name="SHEETNO" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DX" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DY" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DR" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
<attribute name="DM" x="60.96" y="121.92" size="1.778" layer="96" rot="R90" display="off"/>
</instance>
<instance part="P+55" gate="1" x="81.28" y="127" rot="R270"/>
<instance part="GND234" gate="1" x="86.36" y="114.3" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="27.94" y1="55.88" x2="27.94" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="2"/>
<pinref part="SUPPLY49" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="162.56" y1="53.34" x2="162.56" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C44" gate="G$1" pin="2"/>
<pinref part="SUPPLY54" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC21" gate="G$1" pin="GND"/>
<pinref part="GND70" gate="1" pin="GND"/>
<wire x1="71.12" y1="33.02" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="G$1" pin="GND@1"/>
<pinref part="GND71" gate="1" pin="GND"/>
<wire x1="73.66" y1="33.02" x2="73.66" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="G$1" pin="GND@2"/>
<pinref part="GND72" gate="1" pin="GND"/>
<wire x1="76.2" y1="33.02" x2="76.2" y2="27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC21" gate="G$1" pin="DA"/>
<pinref part="GND73" gate="1" pin="GND"/>
<wire x1="88.9" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R23" gate="G$1" pin="1"/>
<pinref part="GND88" gate="1" pin="GND"/>
<wire x1="99.06" y1="25.4" x2="99.06" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND89" gate="1" pin="GND"/>
<pinref part="C29" gate="G$1" pin="2"/>
<wire x1="40.64" y1="5.08" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<pinref part="R25" gate="G$1" pin="2"/>
<wire x1="40.64" y1="7.62" x2="40.64" y2="10.16" width="0.1524" layer="91"/>
<wire x1="48.26" y1="25.4" x2="48.26" y2="7.62" width="0.1524" layer="91"/>
<wire x1="48.26" y1="7.62" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<junction x="40.64" y="7.62"/>
</segment>
<segment>
<wire x1="27.94" y1="-30.48" x2="27.94" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="2"/>
<pinref part="SUPPLY1" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="162.56" y1="-33.02" x2="162.56" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="C59" gate="G$1" pin="2"/>
<pinref part="SUPPLY2" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="GND"/>
<pinref part="GND191" gate="1" pin="GND"/>
<wire x1="71.12" y1="-53.34" x2="71.12" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="GND@1"/>
<pinref part="GND192" gate="1" pin="GND"/>
<wire x1="73.66" y1="-53.34" x2="73.66" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="GND@2"/>
<pinref part="GND193" gate="1" pin="GND"/>
<wire x1="76.2" y1="-53.34" x2="76.2" y2="-58.42" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC10" gate="G$1" pin="DA"/>
<pinref part="GND194" gate="1" pin="GND"/>
<wire x1="88.9" y1="-38.1" x2="93.98" y2="-38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R83" gate="G$1" pin="1"/>
<pinref part="GND195" gate="1" pin="GND"/>
<wire x1="99.06" y1="-60.96" x2="99.06" y2="-63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND196" gate="1" pin="GND"/>
<pinref part="C61" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-81.28" x2="40.64" y2="-78.74" width="0.1524" layer="91"/>
<pinref part="R85" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-78.74" x2="40.64" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-60.96" x2="48.26" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-78.74" x2="40.64" y2="-78.74" width="0.1524" layer="91"/>
<junction x="40.64" y="-78.74"/>
</segment>
<segment>
<wire x1="53.34" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<wire x1="60.96" y1="114.3" x2="83.82" y2="114.3" width="0.1524" layer="91"/>
<wire x1="60.96" y1="116.84" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<junction x="60.96" y="114.3"/>
<pinref part="FB26" gate="G$1" pin="2"/>
<pinref part="C250" gate="G$1" pin="2"/>
<pinref part="GND234" gate="1" pin="GND"/>
</segment>
</net>
<net name="+24V" class="7">
<segment>
<wire x1="27.94" y1="68.58" x2="-22.86" y2="68.58" width="0.1524" layer="91"/>
<wire x1="27.94" y1="66.04" x2="27.94" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C42" gate="G$1" pin="1"/>
<pinref part="P+53" gate="1" pin="+24V"/>
<pinref part="IC21" gate="G$1" pin="VIN"/>
<wire x1="27.94" y1="68.58" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<junction x="27.94" y="68.58"/>
<pinref part="IC21" gate="G$1" pin="RUN/SS"/>
<wire x1="48.26" y1="68.58" x2="58.42" y2="68.58" width="0.1524" layer="91"/>
<wire x1="58.42" y1="60.96" x2="48.26" y2="60.96" width="0.1524" layer="91"/>
<wire x1="48.26" y1="60.96" x2="48.26" y2="68.58" width="0.1524" layer="91"/>
<junction x="48.26" y="68.58"/>
</segment>
<segment>
<wire x1="27.94" y1="-17.78" x2="-22.86" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="27.94" y1="-20.32" x2="27.94" y2="-17.78" width="0.1524" layer="91"/>
<pinref part="C58" gate="G$1" pin="1"/>
<pinref part="P+83" gate="1" pin="+24V"/>
<pinref part="IC10" gate="G$1" pin="VIN"/>
<wire x1="27.94" y1="-17.78" x2="48.26" y2="-17.78" width="0.1524" layer="91"/>
<junction x="27.94" y="-17.78"/>
<pinref part="IC10" gate="G$1" pin="RUN/SS"/>
<wire x1="48.26" y1="-17.78" x2="58.42" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="58.42" y1="-25.4" x2="48.26" y2="-25.4" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-25.4" x2="48.26" y2="-17.78" width="0.1524" layer="91"/>
<junction x="48.26" y="-17.78"/>
</segment>
<segment>
<wire x1="53.34" y1="127" x2="60.96" y2="127" width="0.1524" layer="91"/>
<wire x1="60.96" y1="127" x2="78.74" y2="127" width="0.1524" layer="91"/>
<wire x1="60.96" y1="124.46" x2="60.96" y2="127" width="0.1524" layer="91"/>
<junction x="60.96" y="127"/>
<pinref part="FB25" gate="G$1" pin="2"/>
<pinref part="C250" gate="G$1" pin="1"/>
<pinref part="P+55" gate="1" pin="+24V"/>
</segment>
</net>
<net name="VSW3V3" class="0">
<segment>
<pinref part="IC21" gate="G$1" pin="SW"/>
<wire x1="88.9" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="C28" gate="G$1" pin="2"/>
<wire x1="99.06" y1="58.42" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<wire x1="104.14" y1="58.42" x2="106.68" y2="58.42" width="0.1524" layer="91"/>
<wire x1="101.6" y1="63.5" x2="104.14" y2="63.5" width="0.1524" layer="91"/>
<wire x1="104.14" y1="63.5" x2="104.14" y2="58.42" width="0.1524" layer="91"/>
<junction x="104.14" y="58.42"/>
<pinref part="IC21" gate="G$1" pin="DC"/>
<wire x1="88.9" y1="55.88" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<wire x1="99.06" y1="55.88" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC21" gate="G$1" pin="DC@1"/>
<wire x1="88.9" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="53.34" x2="99.06" y2="55.88" width="0.1524" layer="91"/>
<junction x="99.06" y="58.42"/>
<junction x="99.06" y="55.88"/>
<pinref part="L3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$98" class="0">
<segment>
<pinref part="IC21" gate="G$1" pin="BOOST"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="88.9" y1="63.5" x2="93.98" y2="63.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<pinref part="IC21" gate="G$1" pin="FB"/>
<pinref part="R22" gate="G$1" pin="1"/>
<wire x1="88.9" y1="38.1" x2="99.06" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R23" gate="G$1" pin="2"/>
<wire x1="99.06" y1="38.1" x2="106.68" y2="38.1" width="0.1524" layer="91"/>
<wire x1="99.06" y1="35.56" x2="99.06" y2="38.1" width="0.1524" layer="91"/>
<junction x="99.06" y="38.1"/>
</segment>
</net>
<net name="N$100" class="0">
<segment>
<pinref part="C29" gate="G$1" pin="1"/>
<pinref part="R24" gate="G$1" pin="2"/>
<wire x1="40.64" y1="17.78" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$101" class="0">
<segment>
<pinref part="R24" gate="G$1" pin="1"/>
<pinref part="IC21" gate="G$1" pin="VC"/>
<wire x1="40.64" y1="35.56" x2="40.64" y2="55.88" width="0.1524" layer="91"/>
<wire x1="40.64" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="R25" gate="G$1" pin="1"/>
<pinref part="IC21" gate="G$1" pin="RT"/>
<wire x1="48.26" y1="35.56" x2="48.26" y2="50.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="50.8" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="162.56" y1="68.58" x2="132.08" y2="68.58" width="0.1524" layer="91"/>
<wire x1="132.08" y1="68.58" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<wire x1="132.08" y1="58.42" x2="121.92" y2="58.42" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="180.34" y2="68.58" width="0.1524" layer="91"/>
<wire x1="162.56" y1="68.58" x2="162.56" y2="63.5" width="0.1524" layer="91"/>
<junction x="162.56" y="68.58"/>
<label x="167.64" y="68.58" size="1.778" layer="95"/>
<pinref part="C44" gate="G$1" pin="1"/>
<pinref part="IC21" gate="G$1" pin="BD"/>
<wire x1="132.08" y1="68.58" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<junction x="132.08" y="68.58"/>
<pinref part="IC21" gate="G$1" pin="BIAS"/>
<wire x1="88.9" y1="43.18" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="43.18" x2="132.08" y2="58.42" width="0.1524" layer="91"/>
<junction x="132.08" y="58.42"/>
<pinref part="R22" gate="G$1" pin="2"/>
<wire x1="116.84" y1="38.1" x2="132.08" y2="38.1" width="0.1524" layer="91"/>
<wire x1="132.08" y1="38.1" x2="132.08" y2="43.18" width="0.1524" layer="91"/>
<junction x="132.08" y="43.18"/>
<pinref part="L3" gate="G$1" pin="2"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VSW5V" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="SW"/>
<wire x1="88.9" y1="-27.94" x2="99.06" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="C60" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-27.94" x2="104.14" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-27.94" x2="106.68" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="-22.86" x2="104.14" y2="-22.86" width="0.1524" layer="91"/>
<wire x1="104.14" y1="-22.86" x2="104.14" y2="-27.94" width="0.1524" layer="91"/>
<junction x="104.14" y="-27.94"/>
<pinref part="IC10" gate="G$1" pin="DC"/>
<wire x1="88.9" y1="-30.48" x2="99.06" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-30.48" x2="99.06" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="IC10" gate="G$1" pin="DC@1"/>
<wire x1="88.9" y1="-33.02" x2="99.06" y2="-33.02" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-33.02" x2="99.06" y2="-30.48" width="0.1524" layer="91"/>
<junction x="99.06" y="-27.94"/>
<junction x="99.06" y="-30.48"/>
<pinref part="L1" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="BOOST"/>
<pinref part="C60" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-22.86" x2="93.98" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="IC10" gate="G$1" pin="FB"/>
<pinref part="R82" gate="G$1" pin="1"/>
<wire x1="88.9" y1="-48.26" x2="99.06" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="R83" gate="G$1" pin="2"/>
<wire x1="99.06" y1="-48.26" x2="106.68" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="99.06" y1="-50.8" x2="99.06" y2="-48.26" width="0.1524" layer="91"/>
<junction x="99.06" y="-48.26"/>
</segment>
</net>
<net name="N$145" class="0">
<segment>
<pinref part="C61" gate="G$1" pin="1"/>
<pinref part="R84" gate="G$1" pin="2"/>
<wire x1="40.64" y1="-68.58" x2="40.64" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$149" class="0">
<segment>
<pinref part="R84" gate="G$1" pin="1"/>
<pinref part="IC10" gate="G$1" pin="VC"/>
<wire x1="40.64" y1="-50.8" x2="40.64" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="40.64" y1="-30.48" x2="58.42" y2="-30.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$150" class="0">
<segment>
<pinref part="R85" gate="G$1" pin="1"/>
<pinref part="IC10" gate="G$1" pin="RT"/>
<wire x1="48.26" y1="-50.8" x2="48.26" y2="-35.56" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-35.56" x2="58.42" y2="-35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<wire x1="162.56" y1="-17.78" x2="132.08" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-17.78" x2="132.08" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-27.94" x2="121.92" y2="-27.94" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-17.78" x2="180.34" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="162.56" y1="-17.78" x2="162.56" y2="-22.86" width="0.1524" layer="91"/>
<junction x="162.56" y="-17.78"/>
<label x="167.64" y="-17.78" size="1.778" layer="95"/>
<pinref part="C59" gate="G$1" pin="1"/>
<pinref part="IC10" gate="G$1" pin="BD"/>
<wire x1="132.08" y1="-17.78" x2="88.9" y2="-17.78" width="0.1524" layer="91"/>
<junction x="132.08" y="-17.78"/>
<pinref part="IC10" gate="G$1" pin="BIAS"/>
<wire x1="88.9" y1="-43.18" x2="132.08" y2="-43.18" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-43.18" x2="132.08" y2="-27.94" width="0.1524" layer="91"/>
<junction x="132.08" y="-27.94"/>
<pinref part="R82" gate="G$1" pin="2"/>
<wire x1="116.84" y1="-48.26" x2="132.08" y2="-48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="-48.26" x2="132.08" y2="-43.18" width="0.1524" layer="91"/>
<junction x="132.08" y="-43.18"/>
<pinref part="L1" gate="G$1" pin="2"/>
<pinref part="P+84" gate="1" pin="+5V"/>
</segment>
</net>
<net name="GND_UF" class="0">
<segment>
<wire x1="43.18" y1="114.3" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="38.1" y1="116.84" x2="38.1" y2="114.3" width="0.1524" layer="91"/>
<junction x="38.1" y="114.3"/>
<pinref part="FB26" gate="G$1" pin="1"/>
<pinref part="C247" gate="G$1" pin="2"/>
<pinref part="XPWR" gate="3" pin="S"/>
<wire x1="38.1" y1="114.3" x2="-10.16" y2="114.3" width="0.1524" layer="91"/>
<pinref part="XPWR" gate="4" pin="S"/>
<wire x1="-10.16" y1="114.3" x2="-10.16" y2="106.68" width="0.1524" layer="91"/>
<junction x="-10.16" y="114.3"/>
</segment>
</net>
<net name="V_POWER_UF" class="0">
<segment>
<wire x1="43.18" y1="127" x2="38.1" y2="127" width="0.1524" layer="91"/>
<wire x1="38.1" y1="124.46" x2="38.1" y2="127" width="0.1524" layer="91"/>
<junction x="38.1" y="127"/>
<pinref part="FB25" gate="G$1" pin="1"/>
<pinref part="C247" gate="G$1" pin="1"/>
<pinref part="XPWR" gate="1" pin="S"/>
<pinref part="XPWR" gate="2" pin="S"/>
<wire x1="-10.16" y1="127" x2="-10.16" y2="119.38" width="0.1524" layer="91"/>
<junction x="-10.16" y="127"/>
<wire x1="-10.16" y1="127" x2="38.1" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="E0" gate="1" x="5.08" y="83.82"/>
<instance part="E0" gate="2" x="5.08" y="81.28"/>
<instance part="E0" gate="3" x="5.08" y="78.74"/>
<instance part="E0" gate="4" x="5.08" y="76.2"/>
<instance part="E1" gate="1" x="5.08" y="68.58"/>
<instance part="E1" gate="2" x="5.08" y="66.04"/>
<instance part="E1" gate="3" x="5.08" y="63.5"/>
<instance part="E1" gate="4" x="5.08" y="60.96"/>
<instance part="E2" gate="1" x="5.08" y="53.34"/>
<instance part="E2" gate="2" x="5.08" y="50.8"/>
<instance part="E2" gate="3" x="5.08" y="48.26"/>
<instance part="E2" gate="4" x="5.08" y="45.72"/>
<instance part="E3" gate="1" x="5.08" y="38.1"/>
<instance part="E3" gate="2" x="5.08" y="35.56"/>
<instance part="E3" gate="3" x="5.08" y="33.02"/>
<instance part="E3" gate="4" x="5.08" y="30.48"/>
<instance part="E4" gate="1" x="5.08" y="20.32"/>
<instance part="E4" gate="2" x="5.08" y="17.78"/>
<instance part="E4" gate="3" x="5.08" y="15.24"/>
<instance part="E4" gate="4" x="5.08" y="12.7"/>
<instance part="E5" gate="1" x="5.08" y="5.08"/>
<instance part="E5" gate="2" x="5.08" y="2.54"/>
<instance part="E5" gate="3" x="5.08" y="0"/>
<instance part="E5" gate="4" x="5.08" y="-2.54"/>
<instance part="GND150" gate="1" x="-2.54" y="76.2" rot="R270"/>
<instance part="U19" gate="G$1" x="96.52" y="139.7"/>
<instance part="GND151" gate="1" x="78.74" y="147.32" rot="R270"/>
<instance part="RO1" gate="G$1" x="76.2" y="139.7"/>
<instance part="GND152" gate="1" x="111.76" y="139.7" rot="R90"/>
<instance part="U6" gate="A" x="170.18" y="109.22"/>
<instance part="GND153" gate="1" x="170.18" y="66.04"/>
<instance part="+3V26" gate="G$1" x="170.18" y="134.62"/>
<instance part="+3V29" gate="G$1" x="195.58" y="101.6" rot="R270"/>
<instance part="+3V30" gate="G$1" x="195.58" y="99.06" rot="R270"/>
<instance part="+3V31" gate="G$1" x="195.58" y="96.52" rot="R270"/>
<instance part="+3V32" gate="G$1" x="195.58" y="93.98" rot="R270"/>
<instance part="+3V37" gate="G$1" x="78.74" y="154.94" rot="R90"/>
<instance part="+3V38" gate="G$1" x="68.58" y="139.7" rot="R90"/>
<instance part="+3V58" gate="G$1" x="220.98" y="86.36" rot="R270"/>
<instance part="R43" gate="G$1" x="213.36" y="86.36"/>
<instance part="U2" gate="A" x="170.18" y="30.48"/>
<instance part="GND154" gate="1" x="170.18" y="-12.7"/>
<instance part="+3V33" gate="G$1" x="170.18" y="55.88"/>
<instance part="+3V34" gate="G$1" x="190.5" y="22.86" rot="R270"/>
<instance part="+3V35" gate="G$1" x="190.5" y="20.32" rot="R270"/>
<instance part="+3V36" gate="G$1" x="190.5" y="17.78" rot="R270"/>
<instance part="+3V39" gate="G$1" x="190.5" y="15.24" rot="R270"/>
<instance part="+3V40" gate="G$1" x="220.98" y="7.62" rot="R270"/>
<instance part="R44" gate="G$1" x="213.36" y="7.62"/>
<instance part="U3" gate="A" x="170.18" y="-48.26"/>
<instance part="GND157" gate="1" x="170.18" y="-91.44"/>
<instance part="+3V41" gate="G$1" x="170.18" y="-22.86"/>
<instance part="+3V42" gate="G$1" x="190.5" y="-55.88" rot="R270"/>
<instance part="+3V43" gate="G$1" x="190.5" y="-58.42" rot="R270"/>
<instance part="+3V44" gate="G$1" x="190.5" y="-60.96" rot="R270"/>
<instance part="+3V45" gate="G$1" x="190.5" y="-63.5" rot="R270"/>
<instance part="+3V46" gate="G$1" x="220.98" y="-71.12" rot="R270"/>
<instance part="R45" gate="G$1" x="213.36" y="-71.12"/>
<instance part="R49" gate="G$1" x="-2.54" y="81.28"/>
<instance part="R50" gate="G$1" x="-2.54" y="78.74"/>
<instance part="GND161" gate="1" x="-2.54" y="60.96" rot="R270"/>
<instance part="R51" gate="G$1" x="-2.54" y="66.04"/>
<instance part="R52" gate="G$1" x="-2.54" y="63.5"/>
<instance part="GND162" gate="1" x="-2.54" y="45.72" rot="R270"/>
<instance part="R53" gate="G$1" x="-2.54" y="50.8"/>
<instance part="R54" gate="G$1" x="-2.54" y="48.26"/>
<instance part="GND163" gate="1" x="-2.54" y="30.48" rot="R270"/>
<instance part="R55" gate="G$1" x="-2.54" y="35.56"/>
<instance part="R56" gate="G$1" x="-2.54" y="33.02"/>
<instance part="GND164" gate="1" x="-2.54" y="12.7" rot="R270"/>
<instance part="R57" gate="G$1" x="-2.54" y="17.78"/>
<instance part="R58" gate="G$1" x="-2.54" y="15.24"/>
<instance part="GND165" gate="1" x="-2.54" y="-2.54" rot="R270"/>
<instance part="R59" gate="G$1" x="-2.54" y="2.54"/>
<instance part="R60" gate="G$1" x="-2.54" y="0"/>
<instance part="+3V75" gate="G$1" x="195.58" y="175.26"/>
<instance part="C54" gate="G$1" x="195.58" y="170.18"/>
<instance part="GND187" gate="1" x="195.58" y="162.56"/>
<instance part="+3V76" gate="G$1" x="208.28" y="175.26"/>
<instance part="C55" gate="G$1" x="208.28" y="170.18"/>
<instance part="GND188" gate="1" x="208.28" y="162.56"/>
<instance part="+3V77" gate="G$1" x="220.98" y="175.26"/>
<instance part="C56" gate="G$1" x="220.98" y="170.18"/>
<instance part="GND189" gate="1" x="220.98" y="162.56"/>
<instance part="+3V78" gate="G$1" x="233.68" y="175.26"/>
<instance part="C57" gate="G$1" x="233.68" y="170.18"/>
<instance part="GND190" gate="1" x="233.68" y="162.56"/>
<instance part="P+85" gate="1" x="17.78" y="119.38" rot="R90"/>
<instance part="R86" gate="G$1" x="30.48" y="119.38"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<pinref part="E0" gate="4" pin="S"/>
<pinref part="GND150" gate="1" pin="GND"/>
<wire x1="0" y1="76.2" x2="2.54" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U19" gate="G$1" pin="GND"/>
<pinref part="GND151" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U19" gate="G$1" pin="DIV"/>
<pinref part="GND152" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U6" gate="A" pin="GND"/>
<pinref part="GND153" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="GND"/>
<pinref part="GND154" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="GND"/>
<pinref part="GND157" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND161" gate="1" pin="GND"/>
<wire x1="0" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<pinref part="E1" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND162" gate="1" pin="GND"/>
<wire x1="0" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<pinref part="E2" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND163" gate="1" pin="GND"/>
<wire x1="0" y1="30.48" x2="2.54" y2="30.48" width="0.1524" layer="91"/>
<pinref part="E3" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND164" gate="1" pin="GND"/>
<wire x1="0" y1="12.7" x2="2.54" y2="12.7" width="0.1524" layer="91"/>
<pinref part="E4" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="GND165" gate="1" pin="GND"/>
<wire x1="0" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="91"/>
<pinref part="E5" gate="4" pin="S"/>
</segment>
<segment>
<pinref part="C54" gate="G$1" pin="2"/>
<pinref part="GND187" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C55" gate="G$1" pin="2"/>
<pinref part="GND188" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C56" gate="G$1" pin="2"/>
<pinref part="GND189" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C57" gate="G$1" pin="2"/>
<pinref part="GND190" gate="1" pin="GND"/>
</segment>
</net>
<net name="ENC_D0" class="0">
<segment>
<wire x1="187.96" y1="124.46" x2="215.9" y2="124.46" width="0.1524" layer="91"/>
<label x="195.58" y="124.46" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D0"/>
</segment>
<segment>
<wire x1="187.96" y1="45.72" x2="215.9" y2="45.72" width="0.1524" layer="91"/>
<label x="195.58" y="45.72" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D0"/>
</segment>
<segment>
<wire x1="187.96" y1="-33.02" x2="215.9" y2="-33.02" width="0.1524" layer="91"/>
<label x="195.58" y="-33.02" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D0"/>
</segment>
</net>
<net name="ENC_D1" class="0">
<segment>
<wire x1="187.96" y1="121.92" x2="215.9" y2="121.92" width="0.1524" layer="91"/>
<label x="195.58" y="121.92" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D1"/>
</segment>
<segment>
<wire x1="187.96" y1="43.18" x2="215.9" y2="43.18" width="0.1524" layer="91"/>
<label x="195.58" y="43.18" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D1"/>
</segment>
<segment>
<wire x1="187.96" y1="-35.56" x2="215.9" y2="-35.56" width="0.1524" layer="91"/>
<label x="195.58" y="-35.56" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D1"/>
</segment>
</net>
<net name="ENC_D2" class="0">
<segment>
<wire x1="187.96" y1="119.38" x2="215.9" y2="119.38" width="0.1524" layer="91"/>
<label x="195.58" y="119.38" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D2"/>
</segment>
<segment>
<wire x1="187.96" y1="40.64" x2="215.9" y2="40.64" width="0.1524" layer="91"/>
<label x="195.58" y="40.64" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D2"/>
</segment>
<segment>
<wire x1="187.96" y1="-38.1" x2="215.9" y2="-38.1" width="0.1524" layer="91"/>
<label x="195.58" y="-38.1" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D2"/>
</segment>
</net>
<net name="ENC_D3" class="0">
<segment>
<wire x1="187.96" y1="116.84" x2="215.9" y2="116.84" width="0.1524" layer="91"/>
<label x="195.58" y="116.84" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D3"/>
</segment>
<segment>
<wire x1="187.96" y1="38.1" x2="215.9" y2="38.1" width="0.1524" layer="91"/>
<label x="195.58" y="38.1" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D3"/>
</segment>
<segment>
<wire x1="187.96" y1="-40.64" x2="215.9" y2="-40.64" width="0.1524" layer="91"/>
<label x="195.58" y="-40.64" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D3"/>
</segment>
</net>
<net name="ENC_D4" class="0">
<segment>
<wire x1="187.96" y1="114.3" x2="215.9" y2="114.3" width="0.1524" layer="91"/>
<label x="195.58" y="114.3" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D4"/>
</segment>
<segment>
<wire x1="187.96" y1="35.56" x2="215.9" y2="35.56" width="0.1524" layer="91"/>
<label x="195.58" y="35.56" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D4"/>
</segment>
<segment>
<wire x1="187.96" y1="-43.18" x2="215.9" y2="-43.18" width="0.1524" layer="91"/>
<label x="195.58" y="-43.18" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D4"/>
</segment>
</net>
<net name="ENC_D5" class="0">
<segment>
<wire x1="187.96" y1="111.76" x2="215.9" y2="111.76" width="0.1524" layer="91"/>
<label x="195.58" y="111.76" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D5"/>
</segment>
<segment>
<wire x1="187.96" y1="33.02" x2="215.9" y2="33.02" width="0.1524" layer="91"/>
<label x="195.58" y="33.02" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D5"/>
</segment>
<segment>
<wire x1="187.96" y1="-45.72" x2="215.9" y2="-45.72" width="0.1524" layer="91"/>
<label x="195.58" y="-45.72" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D5"/>
</segment>
</net>
<net name="ENC_D6" class="0">
<segment>
<wire x1="187.96" y1="109.22" x2="215.9" y2="109.22" width="0.1524" layer="91"/>
<label x="195.58" y="109.22" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D6"/>
</segment>
<segment>
<wire x1="187.96" y1="30.48" x2="215.9" y2="30.48" width="0.1524" layer="91"/>
<label x="195.58" y="30.48" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D6"/>
</segment>
<segment>
<wire x1="187.96" y1="-48.26" x2="215.9" y2="-48.26" width="0.1524" layer="91"/>
<label x="195.58" y="-48.26" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D6"/>
</segment>
</net>
<net name="ENC_D7" class="0">
<segment>
<wire x1="187.96" y1="106.68" x2="215.9" y2="106.68" width="0.1524" layer="91"/>
<label x="195.58" y="106.68" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="D7"/>
</segment>
<segment>
<wire x1="187.96" y1="27.94" x2="215.9" y2="27.94" width="0.1524" layer="91"/>
<label x="195.58" y="27.94" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="D7"/>
</segment>
<segment>
<wire x1="187.96" y1="-50.8" x2="215.9" y2="-50.8" width="0.1524" layer="91"/>
<label x="195.58" y="-50.8" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="D7"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<pinref part="U19" gate="G$1" pin="SET"/>
<pinref part="RO1" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ENCB1" class="0">
<segment>
<wire x1="152.4" y1="101.6" x2="132.08" y2="101.6" width="0.1524" layer="91"/>
<label x="134.62" y="101.6" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="YB"/>
</segment>
<segment>
<pinref part="R52" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="63.5" x2="-22.86" y2="63.5" width="0.1524" layer="91"/>
<label x="-20.32" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCA1" class="0">
<segment>
<wire x1="152.4" y1="104.14" x2="132.08" y2="104.14" width="0.1524" layer="91"/>
<label x="134.62" y="104.14" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="YA"/>
</segment>
<segment>
<pinref part="R51" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="66.04" x2="-22.86" y2="66.04" width="0.1524" layer="91"/>
<label x="-20.32" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCA0" class="0">
<segment>
<wire x1="152.4" y1="111.76" x2="132.08" y2="111.76" width="0.1524" layer="91"/>
<label x="134.62" y="111.76" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="XA"/>
</segment>
<segment>
<pinref part="R49" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="81.28" x2="-22.86" y2="81.28" width="0.1524" layer="91"/>
<label x="-20.32" y="81.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCB0" class="0">
<segment>
<wire x1="152.4" y1="109.22" x2="132.08" y2="109.22" width="0.1524" layer="91"/>
<label x="134.62" y="109.22" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="XB"/>
</segment>
<segment>
<pinref part="R50" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="78.74" x2="-22.86" y2="78.74" width="0.1524" layer="91"/>
<label x="-20.32" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U6" gate="A" pin="VCC"/>
<pinref part="+3V26" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U6" gate="A" pin="/YLOAD"/>
<pinref part="+3V29" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="101.6" x2="187.96" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="A" pin="/XLOAD"/>
<pinref part="+3V30" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="99.06" x2="187.96" y2="99.06" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="A" pin="/XRST"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="96.52" x2="187.96" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U6" gate="A" pin="/YRST"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<wire x1="193.04" y1="93.98" x2="187.96" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U19" gate="G$1" pin="VDD"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="RO1" gate="G$1" pin="1"/>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V58" gate="G$1" pin="+3V3"/>
<pinref part="R43" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="VCC"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="/YLOAD"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="/XLOAD"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="/XRST"/>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="A" pin="/YRST"/>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V40" gate="G$1" pin="+3V3"/>
<pinref part="R44" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="VCC"/>
<pinref part="+3V41" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="/YLOAD"/>
<pinref part="+3V42" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="/XLOAD"/>
<pinref part="+3V43" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="/XRST"/>
<pinref part="+3V44" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U3" gate="A" pin="/YRST"/>
<pinref part="+3V45" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V46" gate="G$1" pin="+3V3"/>
<pinref part="R45" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V75" gate="G$1" pin="+3V3"/>
<pinref part="C54" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V76" gate="G$1" pin="+3V3"/>
<pinref part="C55" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V77" gate="G$1" pin="+3V3"/>
<pinref part="C56" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V78" gate="G$1" pin="+3V3"/>
<pinref part="C57" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ENC_Y/X" class="0">
<segment>
<wire x1="187.96" y1="91.44" x2="246.38" y2="91.44" width="0.1524" layer="91"/>
<label x="193.04" y="91.44" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="Y/X"/>
</segment>
<segment>
<wire x1="187.96" y1="12.7" x2="246.38" y2="12.7" width="0.1524" layer="91"/>
<label x="193.04" y="12.7" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="Y/X"/>
</segment>
<segment>
<wire x1="187.96" y1="-66.04" x2="246.38" y2="-66.04" width="0.1524" layer="91"/>
<label x="193.04" y="-66.04" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="Y/X"/>
</segment>
</net>
<net name="ENC_/RD" class="0">
<segment>
<wire x1="243.84" y1="88.9" x2="187.96" y2="88.9" width="0.1524" layer="91"/>
<label x="193.04" y="88.9" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="/RD"/>
</segment>
<segment>
<wire x1="243.84" y1="10.16" x2="187.96" y2="10.16" width="0.1524" layer="91"/>
<label x="193.04" y="10.16" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="/RD"/>
</segment>
<segment>
<wire x1="243.84" y1="-68.58" x2="187.96" y2="-68.58" width="0.1524" layer="91"/>
<label x="193.04" y="-68.58" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="/RD"/>
</segment>
</net>
<net name="ENC_/WR" class="0">
<segment>
<wire x1="236.22" y1="83.82" x2="187.96" y2="83.82" width="0.1524" layer="91"/>
<label x="193.04" y="83.82" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="/WR"/>
</segment>
<segment>
<wire x1="236.22" y1="5.08" x2="187.96" y2="5.08" width="0.1524" layer="91"/>
<label x="193.04" y="5.08" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="/WR"/>
</segment>
<segment>
<wire x1="236.22" y1="-73.66" x2="187.96" y2="-73.66" width="0.1524" layer="91"/>
<label x="193.04" y="-73.66" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="/WR"/>
</segment>
</net>
<net name="ENC_C/D" class="0">
<segment>
<wire x1="238.76" y1="81.28" x2="187.96" y2="81.28" width="0.1524" layer="91"/>
<label x="193.04" y="81.28" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="CTL/DATA"/>
</segment>
<segment>
<wire x1="238.76" y1="2.54" x2="187.96" y2="2.54" width="0.1524" layer="91"/>
<label x="193.04" y="2.54" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="CTL/DATA"/>
</segment>
<segment>
<wire x1="238.76" y1="-76.2" x2="187.96" y2="-76.2" width="0.1524" layer="91"/>
<label x="193.04" y="-76.2" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="CTL/DATA"/>
</segment>
</net>
<net name="ENC0_/CS" class="0">
<segment>
<wire x1="187.96" y1="86.36" x2="208.28" y2="86.36" width="0.1524" layer="91"/>
<label x="193.04" y="86.36" size="1.778" layer="95"/>
<pinref part="U6" gate="A" pin="/CS"/>
<pinref part="R43" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ENCCLK" class="0">
<segment>
<wire x1="127" y1="43.18" x2="152.4" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U2" gate="A" pin="CLK"/>
<wire x1="109.22" y1="154.94" x2="127" y2="154.94" width="0.1524" layer="91"/>
<wire x1="127" y1="154.94" x2="127" y2="121.92" width="0.1524" layer="91"/>
<wire x1="127" y1="121.92" x2="152.4" y2="121.92" width="0.1524" layer="91"/>
<pinref part="U19" gate="G$1" pin="OUT"/>
<pinref part="U6" gate="A" pin="CLK"/>
<wire x1="127" y1="43.18" x2="127" y2="121.92" width="0.1524" layer="91"/>
<junction x="127" y="121.92"/>
<wire x1="127" y1="-35.56" x2="152.4" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U3" gate="A" pin="CLK"/>
<wire x1="127" y1="-35.56" x2="127" y2="43.18" width="0.1524" layer="91"/>
<junction x="127" y="43.18"/>
</segment>
</net>
<net name="ENCA2" class="0">
<segment>
<wire x1="152.4" y1="33.02" x2="132.08" y2="33.02" width="0.1524" layer="91"/>
<label x="134.62" y="33.02" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="XA"/>
</segment>
<segment>
<pinref part="R53" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="50.8" x2="-22.86" y2="50.8" width="0.1524" layer="91"/>
<label x="-20.32" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCB2" class="0">
<segment>
<wire x1="152.4" y1="30.48" x2="132.08" y2="30.48" width="0.1524" layer="91"/>
<label x="134.62" y="30.48" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="XB"/>
</segment>
<segment>
<pinref part="R54" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="48.26" x2="-22.86" y2="48.26" width="0.1524" layer="91"/>
<label x="-20.32" y="48.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCA3" class="0">
<segment>
<wire x1="152.4" y1="25.4" x2="132.08" y2="25.4" width="0.1524" layer="91"/>
<label x="134.62" y="25.4" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="YA"/>
</segment>
<segment>
<pinref part="R55" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="35.56" x2="-22.86" y2="35.56" width="0.1524" layer="91"/>
<label x="-20.32" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCB3" class="0">
<segment>
<wire x1="152.4" y1="22.86" x2="132.08" y2="22.86" width="0.1524" layer="91"/>
<label x="134.62" y="22.86" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="YB"/>
</segment>
<segment>
<pinref part="R56" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="33.02" x2="-22.86" y2="33.02" width="0.1524" layer="91"/>
<label x="-20.32" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC1_/CS" class="0">
<segment>
<wire x1="187.96" y1="7.62" x2="208.28" y2="7.62" width="0.1524" layer="91"/>
<label x="193.04" y="7.62" size="1.778" layer="95"/>
<pinref part="U2" gate="A" pin="/CS"/>
<pinref part="R44" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ENCA4" class="0">
<segment>
<wire x1="152.4" y1="-45.72" x2="132.08" y2="-45.72" width="0.1524" layer="91"/>
<label x="134.62" y="-45.72" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="XA"/>
</segment>
<segment>
<pinref part="R57" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="17.78" x2="-22.86" y2="17.78" width="0.1524" layer="91"/>
<label x="-20.32" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCB4" class="0">
<segment>
<wire x1="152.4" y1="-48.26" x2="132.08" y2="-48.26" width="0.1524" layer="91"/>
<label x="134.62" y="-48.26" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="XB"/>
</segment>
<segment>
<pinref part="R58" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="15.24" x2="-22.86" y2="15.24" width="0.1524" layer="91"/>
<label x="-20.32" y="15.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCA5" class="0">
<segment>
<wire x1="152.4" y1="-53.34" x2="132.08" y2="-53.34" width="0.1524" layer="91"/>
<label x="134.62" y="-53.34" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="YA"/>
</segment>
<segment>
<pinref part="R59" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="2.54" x2="-22.86" y2="2.54" width="0.1524" layer="91"/>
<label x="-20.32" y="2.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENCB5" class="0">
<segment>
<wire x1="152.4" y1="-55.88" x2="132.08" y2="-55.88" width="0.1524" layer="91"/>
<label x="134.62" y="-55.88" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="YB"/>
</segment>
<segment>
<pinref part="R60" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="0" x2="-22.86" y2="0" width="0.1524" layer="91"/>
<label x="-20.32" y="0" size="1.778" layer="95"/>
</segment>
</net>
<net name="ENC2_/CS" class="0">
<segment>
<wire x1="187.96" y1="-71.12" x2="208.28" y2="-71.12" width="0.1524" layer="91"/>
<label x="193.04" y="-71.12" size="1.778" layer="95"/>
<pinref part="U3" gate="A" pin="/CS"/>
<pinref part="R45" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$108" class="0">
<segment>
<pinref part="E0" gate="2" pin="S"/>
<pinref part="R49" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<pinref part="E0" gate="3" pin="S"/>
<pinref part="R50" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$111" class="0">
<segment>
<pinref part="R51" gate="G$1" pin="2"/>
<pinref part="E1" gate="2" pin="S"/>
</segment>
</net>
<net name="N$112" class="0">
<segment>
<pinref part="R52" gate="G$1" pin="2"/>
<pinref part="E1" gate="3" pin="S"/>
</segment>
</net>
<net name="N$113" class="0">
<segment>
<pinref part="R53" gate="G$1" pin="2"/>
<pinref part="E2" gate="2" pin="S"/>
</segment>
</net>
<net name="N$114" class="0">
<segment>
<pinref part="R54" gate="G$1" pin="2"/>
<pinref part="E2" gate="3" pin="S"/>
</segment>
</net>
<net name="N$115" class="0">
<segment>
<pinref part="R55" gate="G$1" pin="2"/>
<pinref part="E3" gate="2" pin="S"/>
</segment>
</net>
<net name="N$116" class="0">
<segment>
<pinref part="R56" gate="G$1" pin="2"/>
<pinref part="E3" gate="3" pin="S"/>
</segment>
</net>
<net name="N$117" class="0">
<segment>
<pinref part="R57" gate="G$1" pin="2"/>
<pinref part="E4" gate="2" pin="S"/>
</segment>
</net>
<net name="N$118" class="0">
<segment>
<pinref part="R58" gate="G$1" pin="2"/>
<pinref part="E4" gate="3" pin="S"/>
</segment>
</net>
<net name="N$119" class="0">
<segment>
<pinref part="R59" gate="G$1" pin="2"/>
<pinref part="E5" gate="2" pin="S"/>
</segment>
</net>
<net name="N$120" class="0">
<segment>
<pinref part="R60" gate="G$1" pin="2"/>
<pinref part="E5" gate="3" pin="S"/>
</segment>
</net>
<net name="VENC" class="0">
<segment>
<pinref part="E0" gate="1" pin="S"/>
<wire x1="-20.32" y1="83.82" x2="2.54" y2="83.82" width="0.1524" layer="91"/>
<label x="-20.32" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-20.32" y1="68.58" x2="2.54" y2="68.58" width="0.1524" layer="91"/>
<pinref part="E1" gate="1" pin="S"/>
<label x="-20.32" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-20.32" y1="53.34" x2="2.54" y2="53.34" width="0.1524" layer="91"/>
<pinref part="E2" gate="1" pin="S"/>
<label x="-20.32" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-20.32" y1="38.1" x2="2.54" y2="38.1" width="0.1524" layer="91"/>
<pinref part="E3" gate="1" pin="S"/>
<label x="-20.32" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-20.32" y1="20.32" x2="2.54" y2="20.32" width="0.1524" layer="91"/>
<pinref part="E4" gate="1" pin="S"/>
<label x="-20.32" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-20.32" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="91"/>
<pinref part="E5" gate="1" pin="S"/>
<label x="-20.32" y="5.08" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R86" gate="G$1" pin="2"/>
<wire x1="35.56" y1="119.38" x2="63.5" y2="119.38" width="0.1524" layer="91"/>
<label x="40.64" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="P+85" gate="1" pin="+5V"/>
<pinref part="R86" gate="G$1" pin="1"/>
<wire x1="20.32" y1="119.38" x2="25.4" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="P0" gate="1" x="10.16" y="91.44"/>
<instance part="P0" gate="2" x="10.16" y="88.9"/>
<instance part="P0" gate="3" x="10.16" y="86.36"/>
<instance part="P0" gate="4" x="10.16" y="83.82"/>
<instance part="P0" gate="5" x="10.16" y="81.28"/>
<instance part="P0" gate="6" x="10.16" y="78.74"/>
<instance part="P1" gate="1" x="10.16" y="66.04"/>
<instance part="P1" gate="2" x="10.16" y="63.5"/>
<instance part="P1" gate="3" x="10.16" y="60.96"/>
<instance part="P1" gate="4" x="10.16" y="58.42"/>
<instance part="P1" gate="5" x="10.16" y="55.88"/>
<instance part="P1" gate="6" x="10.16" y="53.34"/>
<instance part="P2" gate="1" x="10.16" y="43.18"/>
<instance part="P2" gate="2" x="10.16" y="40.64"/>
<instance part="P2" gate="3" x="10.16" y="38.1"/>
<instance part="P2" gate="4" x="10.16" y="35.56"/>
<instance part="P2" gate="5" x="10.16" y="33.02"/>
<instance part="P2" gate="6" x="10.16" y="30.48"/>
<instance part="P3" gate="1" x="10.16" y="20.32"/>
<instance part="P3" gate="2" x="10.16" y="17.78"/>
<instance part="P3" gate="3" x="10.16" y="15.24"/>
<instance part="P3" gate="4" x="10.16" y="12.7"/>
<instance part="P3" gate="5" x="10.16" y="10.16"/>
<instance part="P3" gate="6" x="10.16" y="7.62"/>
<instance part="GND172" gate="1" x="187.96" y="33.02" rot="R90"/>
<instance part="GND173" gate="1" x="193.04" y="30.48" rot="R90"/>
<instance part="GND174" gate="1" x="134.62" y="30.48" rot="R270"/>
<instance part="IC17" gate="G$1" x="167.64" y="45.72"/>
<instance part="GND175" gate="1" x="114.3" y="33.02" rot="R90"/>
<instance part="C39" gate="G$1" x="99.06" y="33.02" rot="R90"/>
<instance part="+3V67" gate="G$1" x="193.04" y="58.42" rot="R270"/>
<instance part="+3V68" gate="G$1" x="198.12" y="55.88" rot="R270"/>
<instance part="+3V69" gate="G$1" x="226.06" y="50.8" rot="R270"/>
<instance part="+3V70" gate="G$1" x="124.46" y="33.02" rot="R90"/>
<instance part="+3V71" gate="G$1" x="88.9" y="33.02" rot="R90"/>
<instance part="R73" gate="G$1" x="2.54" y="88.9"/>
<instance part="R74" gate="G$1" x="2.54" y="81.28"/>
<instance part="R75" gate="G$1" x="2.54" y="63.5"/>
<instance part="R76" gate="G$1" x="2.54" y="55.88"/>
<instance part="R77" gate="G$1" x="2.54" y="40.64"/>
<instance part="R78" gate="G$1" x="2.54" y="33.02"/>
<instance part="R79" gate="G$1" x="2.54" y="17.78"/>
<instance part="R80" gate="G$1" x="2.54" y="10.16"/>
<instance part="GND176" gate="1" x="5.08" y="86.36" rot="R270"/>
<instance part="GND177" gate="1" x="5.08" y="78.74" rot="R270"/>
<instance part="GND178" gate="1" x="5.08" y="60.96" rot="R270"/>
<instance part="GND179" gate="1" x="5.08" y="53.34" rot="R270"/>
<instance part="GND180" gate="1" x="5.08" y="38.1" rot="R270"/>
<instance part="GND181" gate="1" x="5.08" y="30.48" rot="R270"/>
<instance part="GND182" gate="1" x="5.08" y="15.24" rot="R270"/>
<instance part="GND183" gate="1" x="5.08" y="7.62" rot="R270"/>
<instance part="+3V72" gate="G$1" x="66.04" y="81.28" rot="R90"/>
<instance part="R81" gate="G$1" x="73.66" y="81.28"/>
<instance part="C40" gate="G$1" x="93.98" y="78.74"/>
<instance part="GND184" gate="1" x="93.98" y="71.12"/>
<instance part="GND197" gate="1" x="-33.02" y="88.9" rot="R270"/>
<instance part="Z16" gate="G$1" x="-25.4" y="88.9"/>
<instance part="GND198" gate="1" x="-33.02" y="81.28" rot="R270"/>
<instance part="Z17" gate="G$1" x="-25.4" y="81.28"/>
<instance part="GND199" gate="1" x="-33.02" y="63.5" rot="R270"/>
<instance part="Z18" gate="G$1" x="-25.4" y="63.5"/>
<instance part="GND200" gate="1" x="-33.02" y="55.88" rot="R270"/>
<instance part="Z19" gate="G$1" x="-25.4" y="55.88"/>
<instance part="GND201" gate="1" x="-33.02" y="40.64" rot="R270"/>
<instance part="Z20" gate="G$1" x="-25.4" y="40.64"/>
<instance part="GND202" gate="1" x="-33.02" y="33.02" rot="R270"/>
<instance part="Z21" gate="G$1" x="-25.4" y="33.02"/>
<instance part="GND203" gate="1" x="-33.02" y="17.78" rot="R270"/>
<instance part="Z22" gate="G$1" x="-25.4" y="17.78"/>
<instance part="GND204" gate="1" x="-33.02" y="10.16" rot="R270"/>
<instance part="Z23" gate="G$1" x="-25.4" y="10.16"/>
<instance part="R4" gate="G$1" x="228.6" y="45.72"/>
<instance part="+3V112" gate="G$1" x="236.22" y="45.72" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="185.42" y1="33.02" x2="180.34" y2="33.02" width="0.1524" layer="91"/>
<pinref part="GND172" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="190.5" y1="30.48" x2="180.34" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND173" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<wire x1="137.16" y1="30.48" x2="154.94" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND174" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="GND175" gate="1" pin="GND"/>
<wire x1="104.14" y1="33.02" x2="111.76" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P0" gate="3" pin="S"/>
<pinref part="GND176" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P0" gate="6" pin="S"/>
<pinref part="GND177" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P1" gate="3" pin="S"/>
<pinref part="GND178" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P1" gate="6" pin="S"/>
<pinref part="GND179" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P2" gate="3" pin="S"/>
<pinref part="GND180" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P2" gate="6" pin="S"/>
<pinref part="GND181" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P3" gate="3" pin="S"/>
<pinref part="GND182" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="P3" gate="6" pin="S"/>
<pinref part="GND183" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="GND184" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z16" gate="G$1" pin="A"/>
<pinref part="GND197" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z17" gate="G$1" pin="A"/>
<pinref part="GND198" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z18" gate="G$1" pin="A"/>
<pinref part="GND199" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z19" gate="G$1" pin="A"/>
<pinref part="GND200" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z20" gate="G$1" pin="A"/>
<pinref part="GND201" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z21" gate="G$1" pin="A"/>
<pinref part="GND202" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z22" gate="G$1" pin="A"/>
<pinref part="GND203" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z23" gate="G$1" pin="A"/>
<pinref part="GND204" gate="1" pin="GND"/>
</segment>
</net>
<net name="POT0" class="0">
<segment>
<wire x1="104.14" y1="58.42" x2="154.94" y2="58.42" width="0.1524" layer="91"/>
<label x="119.38" y="58.42" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH0"/>
</segment>
<segment>
<pinref part="R73" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="88.9" x2="-22.86" y2="88.9" width="0.1524" layer="91"/>
<label x="-20.32" y="88.9" size="1.778" layer="95"/>
<pinref part="Z16" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT1" class="0">
<segment>
<wire x1="104.14" y1="55.88" x2="154.94" y2="55.88" width="0.1524" layer="91"/>
<label x="119.38" y="55.88" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH1"/>
</segment>
<segment>
<pinref part="R74" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="81.28" x2="-22.86" y2="81.28" width="0.1524" layer="91"/>
<label x="-20.32" y="81.28" size="1.778" layer="95"/>
<pinref part="Z17" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT2" class="0">
<segment>
<wire x1="104.14" y1="53.34" x2="154.94" y2="53.34" width="0.1524" layer="91"/>
<label x="119.38" y="53.34" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH2"/>
</segment>
<segment>
<pinref part="R75" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="63.5" x2="-22.86" y2="63.5" width="0.1524" layer="91"/>
<label x="-20.32" y="63.5" size="1.778" layer="95"/>
<pinref part="Z18" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT4" class="0">
<segment>
<wire x1="104.14" y1="48.26" x2="154.94" y2="48.26" width="0.1524" layer="91"/>
<label x="119.38" y="48.26" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH4"/>
</segment>
<segment>
<pinref part="R77" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="40.64" x2="-22.86" y2="40.64" width="0.1524" layer="91"/>
<label x="-20.32" y="40.64" size="1.778" layer="95"/>
<pinref part="Z20" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT5" class="0">
<segment>
<wire x1="104.14" y1="45.72" x2="154.94" y2="45.72" width="0.1524" layer="91"/>
<label x="119.38" y="45.72" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH5"/>
</segment>
<segment>
<pinref part="R78" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="33.02" x2="-22.86" y2="33.02" width="0.1524" layer="91"/>
<label x="-20.32" y="33.02" size="1.778" layer="95"/>
<pinref part="Z21" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT6" class="0">
<segment>
<wire x1="104.14" y1="43.18" x2="154.94" y2="43.18" width="0.1524" layer="91"/>
<label x="119.38" y="43.18" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH6"/>
</segment>
<segment>
<pinref part="R79" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="17.78" x2="-22.86" y2="17.78" width="0.1524" layer="91"/>
<label x="-20.32" y="17.78" size="1.778" layer="95"/>
<pinref part="Z22" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT7" class="0">
<segment>
<wire x1="104.14" y1="40.64" x2="154.94" y2="40.64" width="0.1524" layer="91"/>
<label x="119.38" y="40.64" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH7"/>
</segment>
<segment>
<pinref part="R80" gate="G$1" pin="1"/>
<wire x1="-2.54" y1="10.16" x2="-22.86" y2="10.16" width="0.1524" layer="91"/>
<label x="-20.32" y="10.16" size="1.778" layer="95"/>
<pinref part="Z23" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT3" class="0">
<segment>
<wire x1="104.14" y1="50.8" x2="154.94" y2="50.8" width="0.1524" layer="91"/>
<label x="119.38" y="50.8" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH3"/>
</segment>
<segment>
<wire x1="-2.54" y1="55.88" x2="-22.86" y2="55.88" width="0.1524" layer="91"/>
<pinref part="R76" gate="G$1" pin="1"/>
<label x="-20.32" y="55.88" size="1.778" layer="95"/>
<pinref part="Z19" gate="G$1" pin="C"/>
</segment>
</net>
<net name="ADC_/CS" class="0">
<segment>
<wire x1="180.34" y1="45.72" x2="223.52" y2="45.72" width="0.1524" layer="91"/>
<label x="185.42" y="45.72" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="!CS"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="ADC_CLK" class="0">
<segment>
<wire x1="180.34" y1="48.26" x2="223.52" y2="48.26" width="0.1524" layer="91"/>
<label x="185.42" y="48.26" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DCLK"/>
</segment>
</net>
<net name="ADC0_BUSY" class="0">
<segment>
<wire x1="223.52" y1="40.64" x2="180.34" y2="40.64" width="0.1524" layer="91"/>
<label x="185.42" y="40.64" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="BUSY"/>
</segment>
</net>
<net name="ADC0_DI" class="0">
<segment>
<wire x1="180.34" y1="43.18" x2="223.52" y2="43.18" width="0.1524" layer="91"/>
<label x="185.42" y="43.18" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="ADC0_DO" class="0">
<segment>
<wire x1="180.34" y1="38.1" x2="223.52" y2="38.1" width="0.1524" layer="91"/>
<label x="185.42" y="38.1" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DOUT"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="190.5" y1="58.42" x2="180.34" y2="58.42" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VCC@1"/>
<pinref part="+3V67" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="195.58" y1="55.88" x2="180.34" y2="55.88" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VCC"/>
<pinref part="+3V68" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="180.34" y1="50.8" x2="223.52" y2="50.8" width="0.1524" layer="91"/>
<label x="185.42" y="50.8" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="!SHDN"/>
<pinref part="+3V69" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="127" y1="33.02" x2="154.94" y2="33.02" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VREF"/>
<pinref part="+3V70" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="91.44" y1="33.02" x2="96.52" y2="33.02" width="0.1524" layer="91"/>
<pinref part="+3V71" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V72" gate="G$1" pin="+3V3"/>
<pinref part="R81" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="+3V112" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="P0" gate="2" pin="S"/>
<pinref part="R73" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$132" class="0">
<segment>
<pinref part="P0" gate="5" pin="S"/>
<pinref part="R74" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$133" class="0">
<segment>
<pinref part="P1" gate="2" pin="S"/>
<pinref part="R75" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$134" class="0">
<segment>
<pinref part="P1" gate="5" pin="S"/>
<pinref part="R76" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$135" class="0">
<segment>
<pinref part="P2" gate="2" pin="S"/>
<pinref part="R77" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$139" class="0">
<segment>
<pinref part="P2" gate="5" pin="S"/>
<pinref part="R78" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$140" class="0">
<segment>
<pinref part="P3" gate="2" pin="S"/>
<pinref part="R79" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$143" class="0">
<segment>
<pinref part="P3" gate="5" pin="S"/>
<pinref part="R80" gate="G$1" pin="2"/>
</segment>
</net>
<net name="VPOT" class="0">
<segment>
<pinref part="P0" gate="1" pin="S"/>
<wire x1="7.62" y1="91.44" x2="-12.7" y2="91.44" width="0.1524" layer="91"/>
<label x="-10.16" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P0" gate="4" pin="S"/>
<wire x1="7.62" y1="83.82" x2="-12.7" y2="83.82" width="0.1524" layer="91"/>
<label x="-12.7" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="1" pin="S"/>
<wire x1="7.62" y1="66.04" x2="-12.7" y2="66.04" width="0.1524" layer="91"/>
<label x="-12.7" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P1" gate="4" pin="S"/>
<wire x1="7.62" y1="58.42" x2="-12.7" y2="58.42" width="0.1524" layer="91"/>
<label x="-12.7" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="1" pin="S"/>
<wire x1="7.62" y1="43.18" x2="-12.7" y2="43.18" width="0.1524" layer="91"/>
<label x="-12.7" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P2" gate="4" pin="S"/>
<wire x1="7.62" y1="35.56" x2="-12.7" y2="35.56" width="0.1524" layer="91"/>
<label x="-12.7" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P3" gate="1" pin="S"/>
<wire x1="7.62" y1="20.32" x2="-12.7" y2="20.32" width="0.1524" layer="91"/>
<label x="-12.7" y="20.32" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="P3" gate="4" pin="S"/>
<wire x1="7.62" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
<label x="-12.7" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="78.74" y1="81.28" x2="93.98" y2="81.28" width="0.1524" layer="91"/>
<label x="83.82" y="81.28" size="1.778" layer="95"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="93.98" y1="81.28" x2="99.06" y2="81.28" width="0.1524" layer="91"/>
<junction x="93.98" y="81.28"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="P+23" gate="VCC" x="139.7" y="185.42" rot="R270"/>
<instance part="C7" gate="G$1" x="73.66" y="182.88"/>
<instance part="C5" gate="G$1" x="35.56" y="182.88"/>
<instance part="C4" gate="G$1" x="17.78" y="182.88"/>
<instance part="GND40" gate="1" x="144.78" y="177.8" rot="R90"/>
<instance part="C8" gate="G$1" x="99.06" y="182.88"/>
<instance part="C9" gate="G$1" x="111.76" y="182.88"/>
<instance part="S0" gate="G$1" x="-2.54" y="71.12"/>
<instance part="S1" gate="G$1" x="50.8" y="71.12"/>
<instance part="S2" gate="G$1" x="111.76" y="71.12"/>
<instance part="S3" gate="G$1" x="165.1" y="71.12"/>
<instance part="P+1" gate="VCC" x="-2.54" y="93.98"/>
<instance part="P+2" gate="VCC" x="50.8" y="93.98"/>
<instance part="P+3" gate="VCC" x="111.76" y="93.98"/>
<instance part="P+4" gate="VCC" x="165.1" y="93.98"/>
<instance part="GND1" gate="1" x="-2.54" y="48.26"/>
<instance part="GND2" gate="1" x="50.8" y="48.26"/>
<instance part="GND3" gate="1" x="111.76" y="48.26"/>
<instance part="GND4" gate="1" x="165.1" y="48.26"/>
<instance part="S4" gate="G$1" x="-2.54" y="12.7"/>
<instance part="S5" gate="G$1" x="50.8" y="12.7"/>
<instance part="S6" gate="G$1" x="111.76" y="12.7"/>
<instance part="S7" gate="G$1" x="165.1" y="12.7"/>
<instance part="P+5" gate="VCC" x="-2.54" y="35.56"/>
<instance part="P+6" gate="VCC" x="50.8" y="35.56"/>
<instance part="P+7" gate="VCC" x="111.76" y="35.56"/>
<instance part="P+8" gate="VCC" x="165.1" y="35.56"/>
<instance part="GND5" gate="1" x="-2.54" y="-10.16"/>
<instance part="GND6" gate="1" x="50.8" y="-10.16"/>
<instance part="GND7" gate="1" x="111.76" y="-10.16"/>
<instance part="GND8" gate="1" x="165.1" y="-10.16"/>
<instance part="S8" gate="G$1" x="-2.54" y="-45.72"/>
<instance part="S9" gate="G$1" x="50.8" y="-45.72"/>
<instance part="S10" gate="G$1" x="111.76" y="-45.72"/>
<instance part="S11" gate="G$1" x="165.1" y="-45.72"/>
<instance part="P+9" gate="VCC" x="-2.54" y="-22.86"/>
<instance part="P+10" gate="VCC" x="50.8" y="-22.86"/>
<instance part="P+11" gate="VCC" x="111.76" y="-22.86"/>
<instance part="P+12" gate="VCC" x="165.1" y="-22.86"/>
<instance part="GND9" gate="1" x="-2.54" y="-68.58"/>
<instance part="GND10" gate="1" x="50.8" y="-68.58"/>
<instance part="GND11" gate="1" x="111.76" y="-68.58"/>
<instance part="GND12" gate="1" x="165.1" y="-68.58"/>
<instance part="S12" gate="G$1" x="-2.54" y="-104.14"/>
<instance part="S13" gate="G$1" x="50.8" y="-104.14"/>
<instance part="S14" gate="G$1" x="111.76" y="-104.14"/>
<instance part="S15" gate="G$1" x="165.1" y="-104.14"/>
<instance part="P+13" gate="VCC" x="-2.54" y="-81.28"/>
<instance part="P+14" gate="VCC" x="50.8" y="-81.28"/>
<instance part="P+15" gate="VCC" x="111.76" y="-81.28"/>
<instance part="P+16" gate="VCC" x="165.1" y="-81.28"/>
<instance part="GND13" gate="1" x="-2.54" y="-127"/>
<instance part="GND14" gate="1" x="50.8" y="-127"/>
<instance part="GND15" gate="1" x="111.76" y="-127"/>
<instance part="GND16" gate="1" x="165.1" y="-127"/>
<instance part="C10" gate="G$1" x="48.26" y="182.88"/>
<instance part="C13" gate="G$1" x="58.42" y="182.88"/>
<instance part="C14" gate="G$1" x="86.36" y="182.88"/>
<instance part="C21" gate="G$1" x="121.92" y="182.88"/>
<instance part="R5" gate="G$1" x="-30.48" y="66.04"/>
<instance part="P+22" gate="VCC" x="-38.1" y="66.04" rot="R90"/>
<instance part="R6" gate="G$1" x="22.86" y="66.04"/>
<instance part="P+24" gate="VCC" x="15.24" y="66.04" rot="R90"/>
<instance part="R7" gate="G$1" x="83.82" y="66.04"/>
<instance part="P+25" gate="VCC" x="76.2" y="66.04" rot="R90"/>
<instance part="R8" gate="G$1" x="137.16" y="66.04"/>
<instance part="P+26" gate="VCC" x="129.54" y="66.04" rot="R90"/>
<instance part="R9" gate="G$1" x="-30.48" y="7.62"/>
<instance part="P+27" gate="VCC" x="-38.1" y="7.62" rot="R90"/>
<instance part="R10" gate="G$1" x="22.86" y="7.62"/>
<instance part="P+28" gate="VCC" x="15.24" y="7.62" rot="R90"/>
<instance part="R11" gate="G$1" x="83.82" y="7.62"/>
<instance part="P+29" gate="VCC" x="76.2" y="7.62" rot="R90"/>
<instance part="R12" gate="G$1" x="137.16" y="7.62"/>
<instance part="P+30" gate="VCC" x="129.54" y="7.62" rot="R90"/>
<instance part="R13" gate="G$1" x="-30.48" y="-50.8"/>
<instance part="P+31" gate="VCC" x="-38.1" y="-50.8" rot="R90"/>
<instance part="R14" gate="G$1" x="22.86" y="-50.8"/>
<instance part="P+32" gate="VCC" x="15.24" y="-50.8" rot="R90"/>
<instance part="R15" gate="G$1" x="83.82" y="-50.8"/>
<instance part="P+33" gate="VCC" x="76.2" y="-50.8" rot="R90"/>
<instance part="R16" gate="G$1" x="137.16" y="-50.8"/>
<instance part="P+34" gate="VCC" x="129.54" y="-50.8" rot="R90"/>
<instance part="R17" gate="G$1" x="-30.48" y="-109.22"/>
<instance part="P+35" gate="VCC" x="-38.1" y="-109.22" rot="R90"/>
<instance part="R18" gate="G$1" x="22.86" y="-109.22"/>
<instance part="P+36" gate="VCC" x="15.24" y="-109.22" rot="R90"/>
<instance part="R19" gate="G$1" x="83.82" y="-109.22"/>
<instance part="P+37" gate="VCC" x="76.2" y="-109.22" rot="R90"/>
<instance part="R20" gate="G$1" x="137.16" y="-109.22"/>
<instance part="P+38" gate="VCC" x="129.54" y="-109.22" rot="R90"/>
<instance part="R21" gate="G$1" x="-30.48" y="78.74"/>
<instance part="P+39" gate="VCC" x="-38.1" y="78.74" rot="R90"/>
<instance part="R26" gate="G$1" x="91.44" y="147.32"/>
<instance part="+3V7" gate="G$1" x="83.82" y="147.32" rot="R90"/>
<instance part="P+17" gate="VCC" x="99.06" y="147.32" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="17.78" y1="177.8" x2="35.56" y2="177.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="177.8" x2="48.26" y2="177.8" width="0.1524" layer="91"/>
<wire x1="48.26" y1="177.8" x2="58.42" y2="177.8" width="0.1524" layer="91"/>
<wire x1="58.42" y1="177.8" x2="73.66" y2="177.8" width="0.1524" layer="91"/>
<wire x1="73.66" y1="177.8" x2="86.36" y2="177.8" width="0.1524" layer="91"/>
<wire x1="86.36" y1="177.8" x2="99.06" y2="177.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="177.8" x2="111.76" y2="177.8" width="0.1524" layer="91"/>
<wire x1="111.76" y1="177.8" x2="121.92" y2="177.8" width="0.1524" layer="91"/>
<junction x="73.66" y="177.8"/>
<junction x="35.56" y="177.8"/>
<junction x="99.06" y="177.8"/>
<junction x="111.76" y="177.8"/>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="C7" gate="G$1" pin="2"/>
<pinref part="GND40" gate="1" pin="GND"/>
<pinref part="C8" gate="G$1" pin="2"/>
<pinref part="C9" gate="G$1" pin="2"/>
<pinref part="C10" gate="G$1" pin="2"/>
<junction x="48.26" y="177.8"/>
<pinref part="C13" gate="G$1" pin="2"/>
<junction x="58.42" y="177.8"/>
<pinref part="C14" gate="G$1" pin="2"/>
<junction x="86.36" y="177.8"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="121.92" y1="177.8" x2="142.24" y2="177.8" width="0.1524" layer="91"/>
<junction x="121.92" y="177.8"/>
</segment>
<segment>
<pinref part="S0" gate="G$1" pin="GND"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="GND"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="GND"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S3" gate="G$1" pin="GND"/>
<pinref part="GND4" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S4" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S5" gate="G$1" pin="GND"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S6" gate="G$1" pin="GND"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S7" gate="G$1" pin="GND"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S8" gate="G$1" pin="GND"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S9" gate="G$1" pin="GND"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S10" gate="G$1" pin="GND"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S11" gate="G$1" pin="GND"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S12" gate="G$1" pin="GND"/>
<pinref part="GND13" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S13" gate="G$1" pin="GND"/>
<pinref part="GND14" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S14" gate="G$1" pin="GND"/>
<pinref part="GND15" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="S15" gate="G$1" pin="GND"/>
<pinref part="GND16" gate="1" pin="GND"/>
</segment>
</net>
<net name="VCC" class="1">
<segment>
<wire x1="17.78" y1="185.42" x2="35.56" y2="185.42" width="0.1524" layer="91"/>
<wire x1="35.56" y1="185.42" x2="48.26" y2="185.42" width="0.1524" layer="91"/>
<wire x1="48.26" y1="185.42" x2="58.42" y2="185.42" width="0.1524" layer="91"/>
<wire x1="58.42" y1="185.42" x2="73.66" y2="185.42" width="0.1524" layer="91"/>
<wire x1="73.66" y1="185.42" x2="86.36" y2="185.42" width="0.1524" layer="91"/>
<wire x1="86.36" y1="185.42" x2="99.06" y2="185.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="185.42" x2="111.76" y2="185.42" width="0.1524" layer="91"/>
<wire x1="111.76" y1="185.42" x2="121.92" y2="185.42" width="0.1524" layer="91"/>
<junction x="73.66" y="185.42"/>
<junction x="35.56" y="185.42"/>
<junction x="99.06" y="185.42"/>
<junction x="111.76" y="185.42"/>
<pinref part="P+23" gate="VCC" pin="VCC"/>
<pinref part="C7" gate="G$1" pin="1"/>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="C8" gate="G$1" pin="1"/>
<pinref part="C9" gate="G$1" pin="1"/>
<pinref part="C10" gate="G$1" pin="1"/>
<junction x="48.26" y="185.42"/>
<pinref part="C13" gate="G$1" pin="1"/>
<junction x="58.42" y="185.42"/>
<pinref part="C14" gate="G$1" pin="1"/>
<junction x="86.36" y="185.42"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="121.92" y1="185.42" x2="137.16" y2="185.42" width="0.1524" layer="91"/>
<junction x="121.92" y="185.42"/>
</segment>
<segment>
<pinref part="S0" gate="G$1" pin="VSUP"/>
<pinref part="P+1" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S1" gate="G$1" pin="VSUP"/>
<pinref part="P+2" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S2" gate="G$1" pin="VSUP"/>
<pinref part="P+3" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S3" gate="G$1" pin="VSUP"/>
<pinref part="P+4" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S4" gate="G$1" pin="VSUP"/>
<pinref part="P+5" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S5" gate="G$1" pin="VSUP"/>
<pinref part="P+6" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S6" gate="G$1" pin="VSUP"/>
<pinref part="P+7" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S7" gate="G$1" pin="VSUP"/>
<pinref part="P+8" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S8" gate="G$1" pin="VSUP"/>
<pinref part="P+9" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S9" gate="G$1" pin="VSUP"/>
<pinref part="P+10" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S10" gate="G$1" pin="VSUP"/>
<pinref part="P+11" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S11" gate="G$1" pin="VSUP"/>
<pinref part="P+12" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S12" gate="G$1" pin="VSUP"/>
<pinref part="P+13" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S13" gate="G$1" pin="VSUP"/>
<pinref part="P+14" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S14" gate="G$1" pin="VSUP"/>
<pinref part="P+15" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="S15" gate="G$1" pin="VSUP"/>
<pinref part="P+16" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<pinref part="P+22" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<pinref part="P+24" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<pinref part="P+25" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="P+26" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R9" gate="G$1" pin="1"/>
<pinref part="P+27" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R10" gate="G$1" pin="1"/>
<pinref part="P+28" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R11" gate="G$1" pin="1"/>
<pinref part="P+29" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<pinref part="P+30" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<pinref part="P+31" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R14" gate="G$1" pin="1"/>
<pinref part="P+32" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R15" gate="G$1" pin="1"/>
<pinref part="P+33" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R16" gate="G$1" pin="1"/>
<pinref part="P+34" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R17" gate="G$1" pin="1"/>
<pinref part="P+35" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R18" gate="G$1" pin="1"/>
<pinref part="P+36" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R19" gate="G$1" pin="1"/>
<pinref part="P+37" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R20" gate="G$1" pin="1"/>
<pinref part="P+38" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R21" gate="G$1" pin="1"/>
<pinref part="P+39" gate="VCC" pin="VCC"/>
</segment>
<segment>
<pinref part="R26" gate="G$1" pin="2"/>
<pinref part="P+17" gate="VCC" pin="VCC"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="R26" gate="G$1" pin="1"/>
<pinref part="+3V7" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="PS_CLK" class="0">
<segment>
<pinref part="S0" gate="G$1" pin="SCL"/>
<wire x1="-17.78" y1="78.74" x2="-25.4" y2="78.74" width="0.1524" layer="91"/>
<label x="-25.4" y="78.74" size="1.778" layer="95"/>
<pinref part="R21" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="35.56" y1="78.74" x2="27.94" y2="78.74" width="0.1524" layer="91"/>
<label x="27.94" y="78.74" size="1.778" layer="95"/>
<pinref part="S1" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="96.52" y1="78.74" x2="88.9" y2="78.74" width="0.1524" layer="91"/>
<label x="88.9" y="78.74" size="1.778" layer="95"/>
<pinref part="S2" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="149.86" y1="78.74" x2="142.24" y2="78.74" width="0.1524" layer="91"/>
<label x="142.24" y="78.74" size="1.778" layer="95"/>
<pinref part="S3" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="-17.78" y1="20.32" x2="-25.4" y2="20.32" width="0.1524" layer="91"/>
<label x="-25.4" y="20.32" size="1.778" layer="95"/>
<pinref part="S4" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="35.56" y1="20.32" x2="27.94" y2="20.32" width="0.1524" layer="91"/>
<label x="27.94" y="20.32" size="1.778" layer="95"/>
<pinref part="S5" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="96.52" y1="20.32" x2="88.9" y2="20.32" width="0.1524" layer="91"/>
<label x="88.9" y="20.32" size="1.778" layer="95"/>
<pinref part="S6" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="149.86" y1="20.32" x2="142.24" y2="20.32" width="0.1524" layer="91"/>
<label x="142.24" y="20.32" size="1.778" layer="95"/>
<pinref part="S7" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="-17.78" y1="-38.1" x2="-25.4" y2="-38.1" width="0.1524" layer="91"/>
<label x="-25.4" y="-38.1" size="1.778" layer="95"/>
<pinref part="S8" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="35.56" y1="-38.1" x2="27.94" y2="-38.1" width="0.1524" layer="91"/>
<label x="27.94" y="-38.1" size="1.778" layer="95"/>
<pinref part="S9" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="96.52" y1="-38.1" x2="88.9" y2="-38.1" width="0.1524" layer="91"/>
<label x="88.9" y="-38.1" size="1.778" layer="95"/>
<pinref part="S10" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="149.86" y1="-38.1" x2="142.24" y2="-38.1" width="0.1524" layer="91"/>
<label x="142.24" y="-38.1" size="1.778" layer="95"/>
<pinref part="S11" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="-17.78" y1="-96.52" x2="-25.4" y2="-96.52" width="0.1524" layer="91"/>
<label x="-25.4" y="-96.52" size="1.778" layer="95"/>
<pinref part="S12" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="35.56" y1="-96.52" x2="27.94" y2="-96.52" width="0.1524" layer="91"/>
<label x="27.94" y="-96.52" size="1.778" layer="95"/>
<pinref part="S13" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="96.52" y1="-96.52" x2="88.9" y2="-96.52" width="0.1524" layer="91"/>
<label x="88.9" y="-96.52" size="1.778" layer="95"/>
<pinref part="S14" gate="G$1" pin="SCL"/>
</segment>
<segment>
<wire x1="149.86" y1="-96.52" x2="142.24" y2="-96.52" width="0.1524" layer="91"/>
<label x="142.24" y="-96.52" size="1.778" layer="95"/>
<pinref part="S15" gate="G$1" pin="SCL"/>
</segment>
</net>
<net name="PS_SDA1" class="0">
<segment>
<wire x1="35.56" y1="66.04" x2="27.94" y2="66.04" width="0.1524" layer="91"/>
<label x="27.94" y="66.04" size="1.778" layer="95"/>
<pinref part="S1" gate="G$1" pin="SDA"/>
<pinref part="R6" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA2" class="0">
<segment>
<wire x1="96.52" y1="66.04" x2="88.9" y2="66.04" width="0.1524" layer="91"/>
<label x="88.9" y="66.04" size="1.778" layer="95"/>
<pinref part="S2" gate="G$1" pin="SDA"/>
<pinref part="R7" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA3" class="0">
<segment>
<wire x1="149.86" y1="66.04" x2="142.24" y2="66.04" width="0.1524" layer="91"/>
<label x="142.24" y="66.04" size="1.778" layer="95"/>
<pinref part="S3" gate="G$1" pin="SDA"/>
<pinref part="R8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA4" class="0">
<segment>
<wire x1="-17.78" y1="7.62" x2="-25.4" y2="7.62" width="0.1524" layer="91"/>
<label x="-25.4" y="7.62" size="1.778" layer="95"/>
<pinref part="S4" gate="G$1" pin="SDA"/>
<pinref part="R9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA6" class="0">
<segment>
<wire x1="96.52" y1="7.62" x2="88.9" y2="7.62" width="0.1524" layer="91"/>
<label x="88.9" y="7.62" size="1.778" layer="95"/>
<pinref part="S6" gate="G$1" pin="SDA"/>
<pinref part="R11" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA7" class="0">
<segment>
<wire x1="149.86" y1="7.62" x2="142.24" y2="7.62" width="0.1524" layer="91"/>
<label x="142.24" y="7.62" size="1.778" layer="95"/>
<pinref part="S7" gate="G$1" pin="SDA"/>
<pinref part="R12" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA8" class="0">
<segment>
<wire x1="-17.78" y1="-50.8" x2="-25.4" y2="-50.8" width="0.1524" layer="91"/>
<label x="-25.4" y="-50.8" size="1.778" layer="95"/>
<pinref part="S8" gate="G$1" pin="SDA"/>
<pinref part="R13" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA9" class="0">
<segment>
<wire x1="35.56" y1="-50.8" x2="27.94" y2="-50.8" width="0.1524" layer="91"/>
<label x="27.94" y="-50.8" size="1.778" layer="95"/>
<pinref part="S9" gate="G$1" pin="SDA"/>
<pinref part="R14" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA10" class="0">
<segment>
<wire x1="96.52" y1="-50.8" x2="88.9" y2="-50.8" width="0.1524" layer="91"/>
<label x="88.9" y="-50.8" size="1.778" layer="95"/>
<pinref part="S10" gate="G$1" pin="SDA"/>
<pinref part="R15" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA11" class="0">
<segment>
<wire x1="149.86" y1="-50.8" x2="142.24" y2="-50.8" width="0.1524" layer="91"/>
<label x="142.24" y="-50.8" size="1.778" layer="95"/>
<pinref part="S11" gate="G$1" pin="SDA"/>
<pinref part="R16" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA12" class="0">
<segment>
<wire x1="-17.78" y1="-109.22" x2="-25.4" y2="-109.22" width="0.1524" layer="91"/>
<label x="-25.4" y="-109.22" size="1.778" layer="95"/>
<pinref part="S12" gate="G$1" pin="SDA"/>
<pinref part="R17" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA13" class="0">
<segment>
<wire x1="35.56" y1="-109.22" x2="27.94" y2="-109.22" width="0.1524" layer="91"/>
<label x="27.94" y="-109.22" size="1.778" layer="95"/>
<pinref part="S13" gate="G$1" pin="SDA"/>
<pinref part="R18" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA14" class="0">
<segment>
<wire x1="96.52" y1="-109.22" x2="88.9" y2="-109.22" width="0.1524" layer="91"/>
<label x="88.9" y="-109.22" size="1.778" layer="95"/>
<pinref part="S14" gate="G$1" pin="SDA"/>
<pinref part="R19" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA15" class="0">
<segment>
<wire x1="149.86" y1="-109.22" x2="142.24" y2="-109.22" width="0.1524" layer="91"/>
<label x="142.24" y="-109.22" size="1.778" layer="95"/>
<pinref part="S15" gate="G$1" pin="SDA"/>
<pinref part="R20" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA0" class="0">
<segment>
<pinref part="S0" gate="G$1" pin="SDA"/>
<wire x1="-17.78" y1="66.04" x2="-25.4" y2="66.04" width="0.1524" layer="91"/>
<label x="-25.4" y="66.04" size="1.778" layer="95"/>
<pinref part="R5" gate="G$1" pin="2"/>
</segment>
</net>
<net name="PS_SDA5" class="0">
<segment>
<wire x1="35.56" y1="7.62" x2="27.94" y2="7.62" width="0.1524" layer="91"/>
<label x="27.94" y="7.62" size="1.778" layer="95"/>
<pinref part="S5" gate="G$1" pin="SDA"/>
<pinref part="R10" gate="G$1" pin="2"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
