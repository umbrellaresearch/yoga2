#include "common/std_headers.h"

#include "./hwdefs.h"

#include "gpio.h"
#include "wdt.h"
#include "embedded/embedded_hostif.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"
#include "embedded/embedded_debug.h"
#include "dspcore/dspcore.h"
#include "drivers/ssc/fw/ls7266.h"
#include "drivers/ssc/fw/pressure.h"
#include "drivers/ssc/fw/dac.h"
#include "drivers/ssc/fw/ads8344.h"
#include "drivers/ssc/fw/ssc_pneuservo.h"
#include "drivers/ssc/fw/ssc_main.h"
#include "./cad42_dsp.h"
#include "drivers/ssc/fw/conf_eth.h"
#include "embedded/avr32/ethcom.h"

#include "drivers/ssc/fw/hostif.h"
#include "./ssc_leg.h"

ssc_leg_t leg0;
ssc_peer_foot_t peer_foot0;

static rate_generator ssc_leg_polling_rg;


void ssc_leg_setup(void)
{
  rate_generator_enable(&ssc_leg_polling_rg, SECONDSTOTICKS(0.001), 3);

  debug_printf("ls7266...");
  ls7266_init(&ls72660);
  wdt_clear();

  debug_printf("pressure...");
  pressure_init(&pressure0);
  wdt_clear();

  debug_printf("ads8344...");
  ads8344_init(&ads83440);
  wdt_clear();

  debug_printf("dac...");
  dac_init(&dac0);
  wdt_clear();

  for (int i=0; i<N_SSC_LEG_PNEUSERVOS; i++) {
    ssc_pneuservo_state_t *st = &leg0.pneuservo_states[i];
    ssc_pneuservo_init(st);
  }

}

static dsp824 conv_150pg_pressure(uint16_t raw)
{
  if (raw & (uint16_t)0xc000) {
    // error indication
    return DSP824(0.0);
  }
  /* We have the 150PG sensors, 150 psi gage.
     So 10% reading is 14.7 psi (at sea level) or 0.101 MPa
     and 90% reading is 164.7 psi or 1.136 MPa
     The reading is 14 bits, or 16384 counts.
     We want absolute MPa

     So the slope is:
       >>> (1.136-0.101) / (16384*0.8)
       7.896423339843748e-05
     Or as an 8.24 number:
       >>> (1.136-0.101) / (16384*0.8) * (1<<24)
       1324.7999999999997
     And the intercept is:
       >>> 0.101 - (16384*0.1) * 7.896423339843748e-05
       -0.028374999999999984
  */

  return raw * DSP824(7.896423339843748e-05) + DSP824(-0.028374999999999984);
}

static uint16_t conv_valve_dac(dsp824 value)
{
  /*
    Convert a valve command in (dsp824: -1 .. +1) to the DAC value required to get 0..10v.
    Due to a botch in the schematic in SSC_LEG1, the DAC value to get 5v is 2178 instead of 2048.
    Divide by 8192 to convert 1<<24 to 1<<11
  */
  return dsplim(2178 - dsplim(value, DSP824(-1.0), DSP824(+1.0)) / 8192, 0, 4095);
}

static uint16_t conv_regulator_dac(dsp824 value)
{
  // Divide by 4096 to convert 1<<24 to 1<<12
  return dsplim(dsplim(value, DSP824(0.0), DSP824(+1.0)) / 4096, 0, 4095);
}

void ssc_leg_core(void)
{
  leg0.update_ticks = get_ticks64();

  if (leg0.update_ticks - peer_foot0.update_ticks > (U64)SECONDSTOTICKS(1.0)) {
    // zero out any old crap
    for (int i=0; i<N_SSC_FOOT_ANGLE_SENSORS; i++) {
      peer_foot0.angle_sensors[i] = DSP824(0.0);
    }
    for (int i=0; i<N_SSC_FOOT_FORCE_SENSORS; i++) {
      peer_foot0.force_sensors[i] = DSP824(0.0);
    }
  }

  // Read sensors

#if BOARD_INSTANCE==0
  leg0.regulator_states[0].sup_pressure = conv_150pg_pressure(pressure0.values[14]);
  leg0.regulator_states[0].exh_pressure = conv_150pg_pressure(pressure0.values[15]);
#endif

  for (int i=0; i<N_SSC_LEG_PNEUSERVOS; i++) {
    leg0.pneuservo_states[i].ext_pressure = conv_150pg_pressure(pressure0.values[i*2 + 0]);
    leg0.pneuservo_states[i].ret_pressure = conv_150pg_pressure(pressure0.values[i*2 + 1]);
  }

#if BOARD_INSTANCE == 0 // left
  // hs
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[0],
                           ls72660.cur.encs[2] * DSP824(M_PI * 2.0 / (625.0*4.0)) + DSP824(0.0),
                           ls72660.cur.encvels[2] * DSP824(M_PI * 2.0 / (625.0*4.0)));
  calcTorques_lhs(&leg0.pneuservo_states[0]);
  // ki
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[1],
                           ls72660.cur.encs[3] * DSP824(M_PI * 2.0 / (-625.0*4.0)) + DSP824(0.0),
                           ls72660.cur.encvels[3] * DSP824(M_PI * 2.0 / (-625.0*4.0)));
  calcTorques_lki(&leg0.pneuservo_states[1]);
  // hf
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[2],
                           ls72660.cur.encs[1] * DSP824(M_PI * 2.0 / (625.0*4.0)) + DSP824(0.0),
                           ls72660.cur.encvels[1] * DSP824(M_PI * 2.0 / (625.0*4.0)));
  calcTorques_lhf(&leg0.pneuservo_states[2]);
  // ht
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[3],
                           ls72660.cur.encs[0] * DSP824(M_PI * 2.0 / (625.0*4.0)) + DSP824(0.0),
                           ls72660.cur.encvels[0] * DSP824(M_PI * 2.0 / (625.0*4.0)));
  calcTorques_lht(&leg0.pneuservo_states[3]);
  // ko
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[4],
                           ls72660.cur.encs[3] * DSP824(M_PI * 2.0 / (-625.0*4.0)) + DSP824(0.0),
                           ls72660.cur.encvels[3] * DSP824(M_PI * 2.0 / (-625.0*4.0)));
  calcTorques_lko(&leg0.pneuservo_states[4]);
  // ai
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[5],
                           peer_foot0.angle_sensors[0],
                           DSP824(0.0));
  calcTorques_lai(&leg0.pneuservo_states[5]);
  // ao
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[6],
                           peer_foot0.angle_sensors[1],
                           DSP824(0.0));
  calcTorques_lao(&leg0.pneuservo_states[6]);
#elif BOARD_INSTANCE == 1 // right
  // ao
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[0],
                           peer_foot0.angle_sensors[1],
                           DSP824(0.0));
  calcTorques_rao(&leg0.pneuservo_states[0]);
  // ai
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[1],
                           peer_foot0.angle_sensors[0],
                           DSP824(0.0));
  calcTorques_rai(&leg0.pneuservo_states[1]);
  // ko
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[2],
                           ls72660.cur.encs[3] * DSP824(M_PI * 2.0 / (625.0*4.0)),
                           ls72660.cur.encvels[3] * DSP824(M_PI * 2.0 / (625.0*4.0)));
  calcTorques_rko(&leg0.pneuservo_states[2]);
  // ht
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[3],
                           ls72660.cur.encs[0] * DSP824(M_PI * 2.0 / (-625.0*4.0)),
                           ls72660.cur.encvels[0] * DSP824(M_PI * 2.0 / (-625.0*4.0)));
  calcTorques_rht(&leg0.pneuservo_states[3]);
  // hf
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[4],
                           ls72660.cur.encs[1] * DSP824(M_PI * 2.0 / (-625.0*4.0)),
                           ls72660.cur.encvels[1] * DSP824(M_PI * 2.0 / (-625.0*4.0)));
  calcTorques_rhf(&leg0.pneuservo_states[4]);
  // ki
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[5],
                           ls72660.cur.encs[3] * DSP824(M_PI * 2.0 / (625.0*4.0)),
                           ls72660.cur.encvels[3] * DSP824(M_PI * 2.0 / (625.0*4.0)));
  calcTorques_rki(&leg0.pneuservo_states[5]);
  // hs
  ssc_pneuservo_set_posvel(&leg0.pneuservo_states[6],
                           ls72660.cur.encs[2] * DSP824(M_PI * 2.0 / (-625.0*4.0)),
                           ls72660.cur.encvels[2] * DSP824(M_PI * 2.0 / (-625.0*4.0)));
  calcTorques_rhs(&leg0.pneuservo_states[6]);
#else
#error "Unknown board instance"
#endif

  // Update servos
  for (int i=0; i<N_SSC_LEG_PNEUSERVOS; i++) {
    ssc_pneuservo_cmd_t *cmd = &leg0.pneuservo_cmds[i];
    ssc_pneuservo_state_t *st = &leg0.pneuservo_states[i];

    ssc_pneuservo_run(cmd, st);
  }

  // Special mode: valves open
  if (leg0.valve_open_mode) {
#if BOARD_INSTANCE==0
    dac0.out_values[7] = conv_regulator_dac(DSP824(0.0));
    for (int i=0; i < 7; i++) {
      dac0.out_values[i] = conv_valve_dac(DSP824(0.6));
    }
#elif BOARD_INSTANCE==1
    dac0.out_values[7] = 0;
    for (int i=0; i < 7; i++) {
      dac0.out_values[i] = conv_valve_dac(DSP824(0.6));
    }
#else
#error "Unknown board instance"
#endif
    dac0.vpwr_on = true;
  }
  else {
    // Normal mode
    int64_t cmd_age = leg0.cmd_rx_ticks ? get_ticks64() - leg0.cmd_rx_ticks : SECONDSTOTICKS(30.0);

    if (cmd_age < (int64_t)SECONDSTOTICKS(0.2)) {
#if BOARD_INSTANCE==0
      for (int i=0; i < 7; i++) {
        dac0.out_values[i] = conv_valve_dac(leg0.pneuservo_states[i].valve);
      }
      dac0.out_values[7] = conv_regulator_dac(leg0.regulator_cmds[0].setpoint);
#elif BOARD_INSTANCE==1
      for (int i=0; i < 7; i++) {
        dac0.out_values[i] = conv_valve_dac(leg0.pneuservo_states[i].valve);
      }
      dac0.out_values[7] = 0;
#else
#error "Unknown board instance"
#endif
      dac0.vpwr_on = true;
    }
    else {
      dsp824 all_valve;
      dsp824 pressure;
      bool vpwr;

      if (cmd_age < (int64_t)SECONDSTOTICKS(2.0)) {
        // From T=0,valve=0.0 to T=2,valve=0.0
        all_valve = DSP824(0.0);
        pressure = DSP824(0.0);
        vpwr = true;
      }
      else if (cmd_age < (int64_t)SECONDSTOTICKS(4.0)) {
        // From T=2,valve=0.0 to T=4,valve=0.6
        all_valve = (S32)(cmd_age - (int64_t)SECONDSTOTICKS(2.0)) / (SECONDSTOTICKS(2.0)/256) * DSP824(0.6 / 256);
        pressure = DSP824(0.0);
        vpwr = true;
      }
      else if (cmd_age < (int64_t)SECONDSTOTICKS(8.0)) {
        // From T=4,valve=0.6 to T=8,valve=0.6
        all_valve = DSP824(0.6);
        pressure = DSP824(0.0);
        vpwr = true;
      }
      else if (cmd_age < (int64_t)SECONDSTOTICKS(10.0)) {
        // From T=8,valve=0.6 to T=10,valve=0.0
        all_valve = (S32)((int64_t)SECONDSTOTICKS(10.0) - cmd_age) / (SECONDSTOTICKS(2.0)/256) * DSP824(0.6 / 256);
        pressure = DSP824(0.0);
        vpwr = true;
      }
      else {
        all_valve = DSP824(0.0);
        pressure = DSP824(0.0);
        vpwr = false;
      }

#if BOARD_INSTANCE==0
      dac0.out_values[0] = conv_valve_dac(-all_valve); // hs should go forward
      dac0.out_values[1] = conv_valve_dac(all_valve);
      dac0.out_values[2] = conv_valve_dac(-all_valve); // hf should go outward
      dac0.out_values[3] = conv_valve_dac(all_valve);
      dac0.out_values[4] = conv_valve_dac(all_valve);
      dac0.out_values[5] = conv_valve_dac(all_valve);
      dac0.out_values[6] = conv_valve_dac(all_valve);
      dac0.out_values[7] = conv_regulator_dac(pressure);
#elif BOARD_INSTANCE==1
      dac0.out_values[0] = conv_valve_dac(all_valve);
      dac0.out_values[1] = conv_valve_dac(all_valve);
      dac0.out_values[2] = conv_valve_dac(all_valve);
      dac0.out_values[3] = conv_valve_dac(all_valve);
      dac0.out_values[4] = conv_valve_dac(-all_valve); // hf should go outward
      dac0.out_values[5] = conv_valve_dac(all_valve);
      dac0.out_values[6] = conv_valve_dac(-all_valve); // hs should go forward
      dac0.out_values[7] = 0;
#else
#error "Unknown board instance"
#endif
      dac0.vpwr_on = vpwr;
    }
  }
}

void ssc_poll_work(void)
{
  rate_generator_set_budget(&ssc_leg_polling_rg, 10);
  if (rate_generator_update(&ssc_leg_polling_rg)) {
    ls7266_poll(&ls72660);
    pressure_poll(&pressure0);
    ads8344_poll(&ads83440);
    ssc_leg_core();

    dac_sync(&dac0);
    rate_generator_done(&ssc_leg_polling_rg);
  }
}

void ssc_poll_logging(void)
{
  ls7266_log_status(&ls72660, "ls72660");
  pressure_log_status(&pressure0, "pressure0");
  ads8344_log_status(&ads83440, "ads83440");
  dac_log_status(&dac0, "dac0");
  log_printf("peer_foot0: force=[%{dsp824} %{dsp824} %{dsp824}] angle=[%{dsp824} %{dsp824}] age=%d\n",
             peer_foot0.force_sensors[0], peer_foot0.force_sensors[1], peer_foot0.force_sensors[2],
             peer_foot0.angle_sensors[0], peer_foot0.angle_sensors[1],
             (int)((get_ticks64() - peer_foot0.update_ticks) / SECONDSTOTICKS(1.0)));
}

void ssc_leg_handle_cmd_V(pkt_rx_buf *rx)
{
  leg0.valve_open_mode = pkt_rx_u8(rx) == 'O';

  // Must match ssc_leg_io.h : txCmd0 or txCmd1
  for (int i=0; i<N_SSC_LEG_REGULATORS; i++) {
    pkt_rx_dsp824p(rx, &leg0.regulator_cmds[i].setpoint);
  }
  for (int i=0; i<N_SSC_LEG_PNEUSERVOS; i++) {
    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].base_valve);

    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].desired_torque);
    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].desired_pos);
    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].desired_vel);

    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].torque_feedback_coeff);
    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].torque_feedback_lim_lo);
    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].torque_feedback_lim_hi);

    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].pos_feedback_coeff);
    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].pos_feedback_lim_lo);
    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].pos_feedback_lim_hi);

    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].vel_feedback_coeff);
    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].vel_feedback_lim_lo);
    pkt_rx_dsp1616p(rx, &leg0.pneuservo_cmds[i].vel_feedback_lim_hi);

    pkt_rx_dsp824p(rx, &leg0.pneuservo_cmds[i].vel_feedforward_coeff);
  }

  leg0.cmd_rx_ticks = get_ticks64();
}

void ssc_leg_handle_cmd_M(pkt_rx_buf *rx, pkt_tx_buf *tx)
{
  U8 subtype = pkt_rx_u8(rx);
  if (subtype == 'O') {
    leg0.valve_open_mode = true;
    pkt_tx_str(tx, "Entered O mode, valves open");
  }
  else if (subtype == '0') {
    leg0.valve_open_mode = false;
    pkt_tx_str(tx, "Entered normal mode");
  }
  else if (subtype == 'R') {
    for (int i=0; i<LS7266_NCOUNTERS; i++) {
      ls7266_zero_counter(&ls72660, i);
    }
    pkt_tx_str(tx, "Zeroed encoders");
  }
  else {
    pkt_tx_str(tx, "Unknown message");
  }
}

void ssc_leg_handle_peer_msg(pkt_rx_buf *rx)
{
  U8 subtype = pkt_rx_u8(rx);
  if (subtype == 'f') {
    U64 dummy_ticks;
    pkt_rx_u64p(rx, &dummy_ticks);
    peer_foot0.update_ticks = get_ticks64();
    for (int i=0; i<N_SSC_FOOT_FORCE_SENSORS; i++) {
      pkt_rx_dsp1616p(rx, &peer_foot0.force_sensors[i]);
    }
    for (int i=0; i<N_SSC_FOOT_ANGLE_SENSORS; i++) {
      pkt_rx_dsp824p(rx, &peer_foot0.angle_sensors[i]);
    }
  }
  else {
    log_printf("ssc_leg_handle_peer_msg: unknown type %02x", subtype);
  }
}

void ssc_leg_emit_state(pkt_tx_buf *tx)
{
  pkt_tx_u64p(tx, &leg0.update_ticks);
  for (int i=0; i<N_SSC_LEG_REGULATORS; i++) {
    // Must match ssc/SscCommon.h:pktRxSscRegulatorState()
    pkt_tx_dsp824p(tx, &leg0.regulator_states[i].sup_pressure);
    pkt_tx_dsp824p(tx, &leg0.regulator_states[i].exh_pressure);
  }
  for (int i=0; i<N_SSC_LEG_PNEUSERVOS; i++) {
    // Must match ssc/SscCommon.h:pktRxSscPneuservoState()
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].ext_pressure);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].ret_pressure);
    pkt_tx_dsp1616p(tx, &leg0.pneuservo_states[i].torque);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].pos);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].vel_prefilter);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].vel);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].torque_feedback);
    pkt_tx_dsp1616p(tx, &leg0.pneuservo_states[i].pos_feedback);
    pkt_tx_dsp1616p(tx, &leg0.pneuservo_states[i].vel_feedback);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].vel_feedforward);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].valve_prefilter);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_states[i].valve);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_cmds[i].base_valve);
    pkt_tx_dsp1616p(tx, &leg0.pneuservo_cmds[i].desired_torque);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_cmds[i].desired_pos);
    pkt_tx_dsp824p(tx, &leg0.pneuservo_cmds[i].desired_vel);
  }
}

void ssc_leg_emit_dump(pkt_tx_buf *tx)
{
  pkt_tx_u64p(tx, &leg0.update_ticks);
  pkt_tx_u32(tx, 2*LS7266_NCOUNTERS + N_SSC_PRESSURE + ADS8344_N_RESULTS);
  for (int i=0; i<LS7266_NCOUNTERS; i++) {
    pkt_tx_s32p(tx, &ls72660.cur.encs[i]);
    pkt_tx_s32p(tx, &ls72660.cur.encvels[i]);
  }

  for (int i=0; i<N_SSC_PRESSURE; i++) {
    pkt_tx_s32(tx, (S32)(U32)pressure0.values[i]);
  }

  for (int i=0; i<ADS8344_N_RESULTS; i++) {
    pkt_tx_s32(tx, (S32)(U32)ads83440.cur.results[i]);
  }
}

void host_fill_update(host_t *it, pkt_tx_buf *tx)
{
  if (leg0.update_ticks != it->last_leg0_update_ticks) {
    pkt_tx_u8(tx, 'l');
    ssc_leg_emit_state(tx);
    it->last_leg0_update_ticks = leg0.update_ticks;
  }
}

U8 host_handle_cmd(host_t *it, U8 cmd, pkt_rx_buf *rx)
{
  if (cmd == 'V') {
    ssc_leg_handle_cmd_V(rx);
    return 1;
  }

  else if (cmd == 'M') {
    pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &rx->src);
    if (!tx) {
      log_printf("reply to M: no tx buf");
      return 0;
    }
    pkt_tx_u8(tx, 'm');
    int orig_len = tx->len;

    ssc_leg_handle_cmd_M(rx, tx);

    if (tx->len > orig_len) {
      pkt_tx_u8(tx, '\n');
      ethcom_tx_pkt(&ethcom0, tx);
    }
    else {
      free_tx_buf(tx);
    }
    return 1;
  }
  else if (cmd=='p') { // Peer message
    ssc_leg_handle_peer_msg(rx);
    return 1;
  }

  return 0;
}

void host_init(host_t *it)
{
}

void host_shutdown(host_t *it)
{
  dac_shutdown(&dac0);
}

void peer_send_update(host_t *it)
{
}

int main(void)
{
  ssc_basic_setup();
  ssc_leg_setup();

  debug_printf("host...");
  host_init(&host0);

  ssc_poll();
}
