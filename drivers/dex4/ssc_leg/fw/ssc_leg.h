#pragma once
#include "common/std_headers.h"

#include "../../ssc_foot/ssc_foot_defs.h"
#include "embedded/embedded_pktcom.h"

#include "drivers/ssc/fw/ssc_pneuservo.h"
#include "drivers/ssc/fw/ssc_regulator.h"

#if BOARD_INSTANCE == 0
#define N_SSC_LEG_REGULATORS 1
#define N_SSC_LEG_PNEUSERVOS 7
#elif BOARD_INSTANCE == 1
#define N_SSC_LEG_REGULATORS 0
#define N_SSC_LEG_PNEUSERVOS 7
#endif

typedef struct ssc_leg_t {
  uint64_t cmd_rx_ticks;
  uint64_t update_ticks;
  bool valve_open_mode;
  ssc_pneuservo_cmd_t pneuservo_cmds[N_SSC_LEG_PNEUSERVOS];
  ssc_regulator_cmd_t regulator_cmds[N_SSC_LEG_REGULATORS];
  ssc_pneuservo_state_t pneuservo_states[N_SSC_LEG_PNEUSERVOS];
  ssc_regulator_state_t regulator_states[N_SSC_LEG_REGULATORS];
} ssc_leg_t;

extern ssc_leg_t leg0;

typedef struct ssc_peer_foot_t {
  uint64_t update_ticks;
  dsp1616 force_sensors[N_SSC_FOOT_FORCE_SENSORS];
  dsp824 angle_sensors[N_SSC_FOOT_ANGLE_SENSORS];
} ssc_peer_foot_t;

void ssc_leg_setup(void);
void ssc_leg_work(void);
void ssc_leg_log_status(void);

void ssc_leg_handle_cmd_V(pkt_rx_buf *rx);
void ssc_leg_handle_cmd_M(pkt_rx_buf *rx, pkt_tx_buf *tx);
void ssc_leg_handle_peer_msg(pkt_rx_buf *rx);
void ssc_leg_emit_state(pkt_tx_buf *tx);
