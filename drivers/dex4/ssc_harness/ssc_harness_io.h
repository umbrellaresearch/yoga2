#pragma once
#include "./ssc_harness_defs.h"
#include "../../../comm/ssc_io.h"
#include "../../../jit/runtime.h"

struct HarnessStateAccessorSuite {
  HarnessStateAccessorSuite(YogaType *t)
    : lift(t, "lift"),
      extPressure(lift.ptrType, "extPressure"),
      retPressure(lift.ptrType, "retPressure"),
      pos(lift.ptrType, "pos"),
      force(lift.ptrType, "force"),
      posFeedback(lift.ptrType, "posFeedback"),
      forceFeedback(lift.ptrType, "forceFeedback"),
      valve(lift.ptrType, "valve")
  {
  }

  YogaPtrAccessor lift;
  YogaValueAccessor<R> extPressure;
  YogaValueAccessor<R> retPressure;
  YogaValueAccessor<R> pos;
  YogaValueAccessor<R> force;
  YogaValueAccessor<R> posFeedback;
  YogaValueAccessor<R> forceFeedback;
  YogaValueAccessor<R> valve;

  void rxLiftState(packet &rx, YogaValue const &yv)
  {
    rxDsp824(rx, extPressure, yv);
    rxDsp824(rx, retPressure, yv);
    rxDsp824(rx, pos, yv);
    rxDsp1616(rx, force, yv);
    rxDsp1616(rx, posFeedback, yv);
    rxDsp824(rx, forceFeedback, yv);
    rxDsp824(rx, valve, yv);
  }

  void rxState(packet &rx, YogaValue const &yv)
  {
    rxLiftState(rx, lift.ptr(yv));
  }
};

struct HarnessClapperAccessorSuite {
  HarnessClapperAccessorSuite(YogaType *t)
    :digit(t, "digit")
  {
  }

  YogaValueAccessor<S32> digit;

  void rxEvent(packet &rx, YogaValue const &yv)
  {
    uint8_t digitRaw;
    rx.get(digitRaw);
    digit.wr(yv, (S32)(U32)digitRaw);
  }
};

struct HarnessCamtrigAccessorSuite {
  HarnessCamtrigAccessorSuite(YogaType *t)
    :trigi(t, "trigi")
  {
  }

  YogaValueAccessor<S32> trigi;

  void rxEvent(packet &rx, YogaValue const &yv)
  {
    uint8_t trigiRaw;
    rx.get(trigiRaw);
    trigi.wr(yv, (S32)(U32)trigiRaw);
  }
};


struct HarnessCmdAccessorSuite {
  HarnessCmdAccessorSuite(YogaType *t)
    : lift(t, "lift"),
      baseValve(lift.ptrType, "baseValve"),
      desiredForce(lift.ptrType, "desiredForce"),
      desiredPos(lift.ptrType, "desiredPos"),
      extPressureToForce(lift.ptrType, "extPressureToForce"),
      retPressureToForce(lift.ptrType, "retPressureToForce"),
      forceFeedbackCoeff(lift.ptrType, "forceFeedbackCoeff"),
      forceFeedbackLimLo(lift.ptrType, "forceFeedbackLimLo"),
      forceFeedbackLimHi(lift.ptrType, "forceFeedbackLimHi"),
      posFeedbackCoeff(lift.ptrType, "posFeedbackCoeff"),
      posFeedbackLimLo(lift.ptrType, "posFeedbackLimLo"),
      posFeedbackLimHi(lift.ptrType, "posFeedbackLimHi")
  {
  }

  YogaPtrAccessor lift;

  YogaValueAccessor<R> baseValve;
  YogaValueAccessor<R> desiredForce;
  YogaValueAccessor<R> desiredPos;
  YogaValueAccessor<R> extPressureToForce;
  YogaValueAccessor<R> retPressureToForce;
  YogaValueAccessor<R> forceFeedbackCoeff;
  YogaValueAccessor<R> forceFeedbackLimLo;
  YogaValueAccessor<R> forceFeedbackLimHi;
  YogaValueAccessor<R> posFeedbackCoeff;
  YogaValueAccessor<R> posFeedbackLimLo;
  YogaValueAccessor<R> posFeedbackLimHi;

  void txLift(packet &tx, YogaValue const &yv)
  {
    txDsp824(tx, baseValve, yv);
    txDsp1616(tx, desiredForce, yv);
    txDsp824(tx, desiredPos, yv);
    txDsp1616(tx, extPressureToForce, yv);
    txDsp1616(tx, retPressureToForce, yv);
    txDsp824(tx, forceFeedbackCoeff, yv);
    txDsp824(tx, forceFeedbackLimLo, yv);
    txDsp824(tx, forceFeedbackLimHi, yv);
    txDsp1616(tx, posFeedbackCoeff, yv);
    txDsp1616(tx, posFeedbackLimLo, yv);
    txDsp1616(tx, posFeedbackLimHi, yv);
  }

  void txCmd(packet &tx, YogaValue const &yv)
  {
    txLift(tx, lift.ptr(yv));
    tx.add((U8)0); // was camtrigMask
  }
};
