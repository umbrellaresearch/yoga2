#include "../../../comm/ssc_network_engine.h"
#include "./ssc_harness_io.h"

struct SscHarnessNetworkEngine : SscNetworkEngine {
  void setupHandlers() override;
  void setupAccessors() override;
  void update(EngineUpdateContext &ctx) override;

  R lastCmdTs{0.0};

  shared_ptr<YogaTimeseq> stateOut;
  unique_ptr<HarnessStateAccessorSuite> stateAs;
  shared_ptr<YogaTimeseq> clapperOut;
  unique_ptr<HarnessClapperAccessorSuite> clapperAs;
  shared_ptr<YogaTimeseq> camtrigOut;
  unique_ptr<HarnessClapperAccessorSuite> camtrigAs;
  shared_ptr<YogaTimeseq> cmdIn;
  unique_ptr<HarnessCmdAccessorSuite> cmdAs;
};


void SscHarnessNetworkEngine::setupAccessors()
{
  SscNetworkEngine::setupAccessors();
  cmdAs = make_unique<HarnessCmdAccessorSuite>(cmdIn->type);
  stateAs = make_unique<HarnessStateAccessorSuite>(stateOut->type);
  if (clapperOut) clapperAs = make_unique<HarnessClapperAccessorSuite>(clapperOut->type);
}

void SscHarnessNetworkEngine::setupHandlers()
{
  SscNetworkEngine::setupHandlers();
  updateHandlers[(U8)'h'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    auto state0 = stateOut->addNew(ts);
    stateAs->rxState(rx, state0);
  };

  updateHandlers[(U8)'c'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    auto clapper0 = clapperOut->addNew(ts);
    clapperAs->rxEvent(rx, clapper0);
  };

  updateHandlers[(U8)'t'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    if (!camtrigOut) return;
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    auto camtrig0 = camtrigOut->addNew(ts);
    camtrigAs->rxEvent(rx, camtrig0);
  };
}

void SscHarnessNetworkEngine::update(EngineUpdateContext &ctx)
{
  if (cmdIn) {
    auto out0 = cmdIn->getLast();
    if (out0.ts > lastCmdTs + 0.0019) {
      packet tx(2048);
      tx.add((U8)'V');
      cmdAs->txCmd(tx, out0);
      tx.add((uint8_t)0); // was camtrig mask

      txPkt(tx);
      lastCmdTs = out0.ts;
    }
  }
  SscNetworkEngine::update(ctx);
}


static EngineRegister sscHarnessNetworkEngineReg("sscHarnessNetwork",
  {
    "Dex4HarnessCmd",
    "Dex4HarnessState",
    "SscClapperEvent",
    "SscCamtrigEvent"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() < 2 || args.size() > 4) {
      return ctx.logError("sscHarnessNetwork: expected 2-4 arguments (cmd, state, [clapper], [camtrig])");
    }

    auto e = make_shared<SscHarnessNetworkEngine>();
    if (args.size() > 0) {
      e->cmdIn = args[0];
    }
    if (args.size() > 1) {
      e->stateOut = args[1];
      e->stateOut->needBackdateInitial = true;
      e->stateOut->isExtIn = true;
    }
    if (args.size() > 2) {
      e->clapperOut = args[2];
      e->clapperOut->isExtIn = true;
    }
    if (args.size() > 3) {
      e->camtrigOut = args[3];
      e->camtrigOut->isExtIn = true;
    }
    
    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    return true;
  }
);

