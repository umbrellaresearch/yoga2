#pragma once
#include "common/std_headers.h"

#include "drivers/ssc/fw/ssc_lift.h"
#include "drivers/ssc/fw/ssc_pneuservo.h"
#include "drivers/ssc/fw/ssc_regulator.h"
#include "embedded/embedded_pktcom.h"

#define N_SSC_HARNESS_REGULATORS 0 // but maybe someday???
#define N_SSC_HARNESS_PNEUSERVOS 0
#define N_SSC_HARNESS_LIFTS 1
#define N_SSC_HARNESS_CAMTRIGS 4

typedef struct ssc_harness_t {
  uint64_t cmd_rx_ticks;
  uint64_t update_ticks;
  bool drop_mode;
  ssc_lift_cmd_t lift_cmds[N_SSC_HARNESS_LIFTS];
  ssc_lift_state_t lift_states[N_SSC_HARNESS_LIFTS];

  uint64_t last_clapper_ticks;
  uint8_t last_clapper_digit;

  uint8_t req_camtrig_mask;
  uint64_t last_camtrig_ticks[N_SSC_HARNESS_CAMTRIGS];
  uint64_t send_camtrig_ticks[N_SSC_HARNESS_CAMTRIGS];
} ssc_harness_t;

extern ssc_harness_t harness0;

void ssc_harness_setup(void);
void ssc_harness_work(void);
void ssc_harness_log_status(void);

void ssc_harness_handle_cmd_V(pkt_rx_buf *rx);
void ssc_harness_handle_cmd_M(pkt_rx_buf *rx, pkt_tx_buf *tx);
void ssc_harness_emit_state(pkt_tx_buf *tx);
