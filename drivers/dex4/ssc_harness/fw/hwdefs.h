#pragma once
#include "common/std_headers.h"

#include "../ssc_harness_defs.h"

#define N_SSC_PRESSURE N_SSC_HARNESS_PRESSURE
#define N_SSC_DACS N_SSC_HARNESS_DACS

#define N_SSC_BINVALVES N_SSC_HARNESS_BINVALVES

#define USE_DEBUG_PACKETS
#define USE_PRINTBUF_DSP

// CPU

/*
  FCPU, FPBA and FPBB are allowed to be up to 66 MHz according to table 38.6 in the AT32UC3A datasheet.
 */
#define FOSC0 12000000
#define FCPU 66000000
#define FPBA 66000000
#define OSC0_STARTUP AVR32_PM_OSCCTRL0_STARTUP_8192_RCOSC

// SERCOM

// Broken since I used the MCU pins for pressure sensors.
#define N_SERCOMS 0
#undef SERCOM_USE_PKT
#define SERCOM_USART0 (AVR32_USART0)

// PA00 and PA01 on UC3A0512
#define SERCOM_USART0_RXD_PIN AVR32_USART0_RXD_0_0_PIN
#define SERCOM_USART0_RXD_FUNCTION AVR32_USART0_RXD_0_0_FUNCTION

#define SERCOM_USART0_TXD_PIN AVR32_USART0_TXD_0_0_PIN
#define SERCOM_USART0_TXD_FUNCTION AVR32_USART0_TXD_0_0_FUNCTION

#define SERCOM_USART0_IRQ AVR32_USART0_IRQ

// ETHCOM

#define ETHCOM_SEND_ARP

// WDT
#undef SSC_USE_WDT

// ETHER

#if defined(__TARG_SSC_HARNESS1__)
#define ETHER_NRST_PIN AVR32_PIN_PX38
#endif

// LS7266

#if defined(__TARG_SSC_HARNESS1__)
#define LS7266_NCOUNTERS N_SSC_HARNESS_ENCODERS

#define LS7266_DATA_PORT (&AVR32_GPIO.port[0]) // Port A
#define LS7266_DATA_SHIFT (20)

#define LS7266_YNX_PIN AVR32_PIN_PX10
#define LS7266_NRD_PIN AVR32_PIN_PX12
#define LS7266_NWR_PIN AVR32_PIN_PX13
#define LS7266_CND_PIN AVR32_PIN_PX11

#define LS7266_NCS0_PIN AVR32_PIN_PX32

#define LS7266_D0_PIN AVR32_PIN_PA20
#endif

#if defined(__TARG_SSC_HARNESS1__)
#define DAC_DATA_PORT (AVR32_GPIO.port[AVR32_PIN_PX00 >> 5])
#define DAC_DIN0_PIN AVR32_PIN_PX00
#define DAC_DIN0_MASK (1 << (DAC_DIN0_PIN & 0x1f))
#define DAC_CLK_PIN AVR32_PIN_PX02
#define DAC_CLK_MASK (1 << (DAC_CLK_PIN & 0x1f))
#define DAC_NSYNC_PIN AVR32_PIN_PX03
#define DAC_NSYNC_MASK (1 << (DAC_NSYNC_PIN & 0x1f))
#define DAC_NLDAC_PIN AVR32_PIN_PX04
#define DAC_NLDAC_MASK (1 << (DAC_NLDAC_PIN & 0x1f))
#endif

#if defined(__TARG_SSC_HARNESS1__)
#define VPWR_EN_PIN AVR32_PIN_PX18
#define VPWR_ST_PIN AVR32_PIN_PX19

#define BINVALVE1_EN_PIN AVR32_PIN_PC04
#define BINVALVE2_EN_PIN AVR32_PIN_PC05
#endif

#if defined(__TARG_SSC_HARNESS1__)
#define PS_CLK AVR32_PIN_PX17
#define PS_SDA0_PIN AVR32_PIN_PA00
#define PS_SDA1_PIN AVR32_PIN_PA01
#define PS_SDA2_PIN AVR32_PIN_PA02
#define PS_SDA3_PIN AVR32_PIN_PA03
#define PS_SDA4_PIN AVR32_PIN_PA04
#define PS_SDA5_PIN AVR32_PIN_PA05
#define PS_SDA6_PIN AVR32_PIN_PA06
#define PS_SDA7_PIN AVR32_PIN_PA07
#define PS_SDA8_PIN AVR32_PIN_PA08
#define PS_SDA9_PIN AVR32_PIN_PA09
#define PS_SDA10_PIN AVR32_PIN_PA10
#define PS_SDA11_PIN AVR32_PIN_PA11
#define PS_SDA12_PIN AVR32_PIN_PA12
#define PS_SDA13_PIN AVR32_PIN_PA13
#define PS_SDA14_PIN AVR32_PIN_PA14
#define PS_SDA15_PIN AVR32_PIN_PA15
#endif

/*
  For outputting the clock to see what the PLL is doing.
  This is PA07, same as ENC_NWR, so it's available on the lower right
  pin of the LS7266s.
*/
#define AVRHW_TEST_GCLK_ID 0
#define AVRHW_TEST_GCLK_PIN AVR32_PM_GCLK_0_0_PIN
#define AVRHW_TEST_GCLK_FUNCTION AVR32_PM_GCLK_0_0_FUNCTION

#if defined(__TARG_SSC_HARNESS1__)
// ADC - a single ads8344
#define ADC_DI0_PIN AVR32_PIN_PX05
#define ADC_DO0_PIN AVR32_PIN_PX06
#define ADC_BUSY0_PIN AVR32_PIN_PX07
#define ADC_CLK_PIN AVR32_PIN_PX08
#define ADC_NCS_PIN AVR32_PIN_PX09
#define ADS8344_N_RESULTS 8
#endif

#if defined(__TARG_SSC_HARNESS1__)
#define CLAPPER_SEG1_PIN AVR32_PIN_PX20
#define CLAPPER_SEG2_PIN AVR32_PIN_PX21
#define CLAPPER_SEG3_PIN AVR32_PIN_PX22
#define CLAPPER_SEG4_PIN AVR32_PIN_PX23
#define CLAPPER_SEG5_PIN AVR32_PIN_PX24
#define CLAPPER_SEG6_PIN AVR32_PIN_PX25
#define CLAPPER_SEG7_PIN AVR32_PIN_PX26
#endif

#if defined(__TARG_SSC_HARNESS1__)
#define CAMTRIG0_PIN AVR32_PIN_PB20
#define CAMTRIG1_PIN AVR32_PIN_PB21
#define CAMTRIG2_PIN AVR32_PIN_PB22
#define CAMTRIG3_PIN AVR32_PIN_PB23
#endif
