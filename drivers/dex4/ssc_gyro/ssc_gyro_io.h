#pragma once
#include "./ssc_gyro_defs.h"
#include "../../../comm/ssc_io.h"
#include "../../../jit/runtime.h"

using glm::vec3;
using glm::mat3;

inline double sscConvGyro(S32 raw)
{
  // Convert to radians/sec, see table 10 & 14.
  return raw * (0.02 * (M_PI / 180.0) / 65536.0);
}

inline double sscConvAccel(S32 raw)
{
  // Convert to m/s/s, see table 17
  return raw * (0.0008 * 9.81 / 65536.0);
}

inline double sscConvDeltang(S32 raw)
{
  // Convert to radians
  return raw * (4.0 * M_PI / 32768.0 / 65536.0);
}

inline double sscConvDeltvel(S32 raw)
{
  // Conver to m/s, see table 31
  return raw * (200.0 / 32768.0 / 65536.0);
}

inline double sscConvBarom(S32 raw)
{
  // Convert to bar
  return raw * (0.000040 / 65536.0);
}

inline double sscConvOrient(S16 raw)
{
  // Convert to orientation matrix entries, see table 42
  return raw * (1.0 / 32768.0);
}

inline double sscConvMag(S16 raw)
{
  // Convert to gauss, see table 38
  // Earth's magnetic field in Northern California is about 0.5 gauss with an incliation of -52 degrees
  // https://en.wikipedia.org/wiki/Earth%27s_magnetic_field
  return raw * 0.0001;
}

inline double sscConvTemperature(S16 raw)
{
  // Convert to celcius
  return raw * 0.00565 + 25.0;
}

static inline string sscFmtSysEFlag(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 15)) ret += " watchdog_timer";
  if (v & (1 << 13)) ret += " ekf_divergence";
  if (v & (1 << 12)) ret += " gyro_sat";
  if (v & (1 << 11)) ret += " mag_disturb";
  if (v & (1 << 10)) ret += " linear_accel";
  if (v & (1 << 7)) ret += " proc_overrun";
  if (v & (1 << 6)) ret += " flash_update_fail";
  if (v & (1 << 5)) ret += " inertial_self_test_fail";
  if (v & (1 << 4)) ret += " sensor_overrange";
  if (v & (1 << 3)) ret += " spi_comm_error";
  if (v & (1 << 0)) ret += " alarm";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}

static inline string sscFmtDiagSts(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 11)) ret += " barom";
  if (v & (1 << 10)) ret += " mag_z";
  if (v & (1 << 9)) ret += " mag_y";
  if (v & (1 << 8)) ret += " mag_x";
  if (v & (1 << 5)) ret += " accel_z";
  if (v & (1 << 4)) ret += " accel_y";
  if (v & (1 << 3)) ret += " accel_x";
  if (v & (1 << 2)) ret += " gyro_z";
  if (v & (1 << 1)) ret += " gyro_y";
  if (v & (1 << 0)) ret += " gyro_x";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}

static inline string sscFmtAlmSts(U16 v)
{
  if (v == 0) return "";
  string ret;
  if (v & (1 << 11)) ret += " barom";
  if (v & (1 << 10)) ret += " mag_z";
  if (v & (1 << 9)) ret += " mag_y";
  if (v & (1 << 8)) ret += " mag_x";
  if (v & (1 << 5)) ret += " accel_z";
  if (v & (1 << 4)) ret += " accel_y";
  if (v & (1 << 3)) ret += " accel_x";
  if (v & (1 << 2)) ret += " gyro_z";
  if (v & (1 << 1)) ret += " gyro_y";
  if (v & (1 << 0)) ret += " gyro_x";
  if (!ret.empty()) return "("s + ret.substr(1) + ")"s;
  return "";
}



using glm::vec3;
using glm::mat3;

struct ImuAccessorSuite {
  ImuAccessorSuite(YogaType *t)
    : gyro(t, "gyro"),
      accel(t, "accel"),
      deltang(t, "deltang"),
      deltvel(t, "deltvel"),
      orient(t, "orient"),
      mag(t, "mag"),
      barom(t, "barom"),
      temperature(t, "temperature"),
      sysEFlag(t, "sysEFlag"),
      diagSts(t, "diagSts"),
      almSts(t, "almSts")
  {
  }
  YogaValueAccessor<vec3> gyro;
  YogaValueAccessor<vec3> accel;
  YogaValueAccessor<vec3> deltang;
  YogaValueAccessor<vec3> deltvel;
  YogaValueAccessor<mat3> orient;
  YogaValueAccessor<vec3> mag;
  YogaValueAccessor<R> barom;
  YogaValueAccessor<R> temperature;
  YogaValueAccessor<S32> sysEFlag;
  YogaValueAccessor<S32> diagSts;
  YogaValueAccessor<S32> almSts;

  void rxGyro(packet &rx, YogaValue const &yv)
  {
    S32 gyroX, gyroY, gyroZ;
    rx.get(gyroX);
    rx.get(gyroY);
    rx.get(gyroZ);

    gyro.wr(yv, vec3(
      sscConvGyro(gyroX),
      sscConvGyro(gyroY),
      sscConvGyro(gyroZ)));
  }

  void rxAccel(packet &rx, YogaValue const &yv)
  {
    S32 accelX, accelY, accelZ;
    rx.get(accelX);
    rx.get(accelY);
    rx.get(accelZ);

    accel.wr(yv, vec3(
      sscConvAccel(accelX),
      sscConvAccel(accelY),
      sscConvAccel(accelZ)));
  }

  void rxDeltang(packet &rx, YogaValue const &yv)
  {
    S32 deltangX, deltangY, deltangZ;
    rx.get(deltangX);
    rx.get(deltangY);
    rx.get(deltangZ);

    deltang.wr(yv, vec3(
      sscConvDeltang(deltangX),
      sscConvDeltang(deltangY),
      sscConvDeltang(deltangZ)));
  }

  void rxDeltvel(packet &rx, YogaValue const &yv)
  {
    S32 deltvelX, deltvelY, deltvelZ;
    rx.get(deltvelX);
    rx.get(deltvelY);
    rx.get(deltvelZ);

    deltvel.wr(yv, vec3(
      sscConvDeltvel(deltvelX),
      sscConvDeltvel(deltvelY),
      sscConvDeltvel(deltvelZ)));
  }

  void rxBarom(packet &rx, YogaValue const &yv)
  {
    S32 baromRaw;
    rx.get(baromRaw);
    barom.wr(yv, sscConvBarom(baromRaw));
  }

  void rxOrient(packet &rx, YogaValue const &yv)
  {
    S16 orient11, orient12, orient13;
    S16 orient21, orient22, orient23;
    S16 orient31, orient32, orient33;
    rx.get(orient11);
    rx.get(orient12);
    rx.get(orient13);
    rx.get(orient21);
    rx.get(orient22);
    rx.get(orient23);
    rx.get(orient31);
    rx.get(orient32);
    rx.get(orient33);

    mat3 orientMat(
      sscConvOrient(orient11),
      sscConvOrient(orient12),
      sscConvOrient(orient13),
      sscConvOrient(orient21),
      sscConvOrient(orient22),
      sscConvOrient(orient23),
      sscConvOrient(orient31),
      sscConvOrient(orient32),
      sscConvOrient(orient33));
    if (0) {
      cerr << "Orient "s + 
        to_string(orient11) + " " + to_string(orient12) + " " + to_string(orient13) + " / " +
        to_string(orient21) + " " + to_string(orient22) + " " + to_string(orient23) + " / " +
        to_string(orient31) + " " + to_string(orient32) + " " + to_string(orient33) + " = " +
        to_string(orientMat) + "\n";
    }
    orient.wr(yv, orientMat);
    if (0) {
      auto orientMatBack = orient.rd(yv);
      cerr << " back "s +
        to_string(orientMatBack) + "\n";
    }
  }

  void rxMag(packet &rx, YogaValue const &yv)
  {
    S16 magX, magY, magZ;
    rx.get(magX);
    rx.get(magY);
    rx.get(magZ);

    mag.wr(yv, vec3(
      sscConvMag(magX),
      sscConvMag(magY),
      sscConvMag(magZ)));
  }

  void rxTemperature(packet &rx, YogaValue const &yv)
  {
    S16 temperatureRaw;
    rx.get(temperatureRaw);
    temperature.wr(yv, sscConvTemperature(temperatureRaw));
  }

  void rxFlags(packet &rx, YogaValue const &yv)
  {
    U16 sysEFlagRaw;
    U16 diagStsRaw;
    U16 almStsRaw;
    rx.get(sysEFlagRaw);
    rx.get(diagStsRaw);
    rx.get(almStsRaw);

    sysEFlag.wr(yv, sysEFlagRaw);
    diagSts.wr(yv, diagStsRaw);
    almSts.wr(yv, almStsRaw);
  }

  void rxState(packet &rx, YogaValue const &yv)
  {
    rxGyro(rx, yv);
    rxAccel(rx, yv);
    rxDeltang(rx, yv);
    rxDeltvel(rx, yv);
    rxBarom(rx, yv);
    rxOrient(rx, yv);
    rxMag(rx, yv);
    rxTemperature(rx, yv);
    rxFlags(rx, yv);
  }

};
