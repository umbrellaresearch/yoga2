
# Chest board and firmware

## Decisions

- Don't do gyro processing on SSC board, just send raw samples to host
- Use non-floating-point (AT32UC3A) chip
- Use Ethernet communication
- Don't plan to tweak code very often, at least not for tuning walking
- AVRONE doesn't work with Linux avr32program (most things work, but flash programming fails after the first page.) So use STK600 with the cable.
- AVRONE might still work for debugging?

## Results
- MACB example code takes 0x37c0 bytes, so an ethernet bootloader could fit within the protected boot flash

## TODO
- Get main code running on EVK1100, verify ARP
- Populate & solder SSC2 board
- Get main code running on SSC2 board
- Figure out what emulator/debugger to use: AVRONE or STK600?
- Speed up internet_checksum
- Verify that internet_checksum works right with odd numbers of bytes
- Choose private addresses
- Connect DIO2 line of ADIS16480 to MCU to use as interrupt when new data ready]
  (Probably to PX16)

## Open issues

- Can I upgrade over Ethernet?
  - is there enough space in boot flash for a TFTP client?
  - Can I stash it in a serial eprom or RAM


## Testing
On paris:
```sh
$ sudo ifconfig eth0 add 172.17.10.1 netmask 0xffffff00
$ sudo tcpdump -i eth0 -v -e -n -x host 172.17.10.2
$ echo '|test' | nc -u -p 10001 -w 1 172.17.10.2 10000
|test
```

Console monitor:
```sh
$ cu -l /dev/ttyUSB0 -s 115200
```

## Documents

MCU: [AT32UC3A](file://../../atmel/doc/AT32UC3A.pdf)
Gyro: [ADIS16364](file://../doc/ADIS16364.pdf)
SPI flash: [M25PE40](file://../doc/M25PE40.pdf)


## Development enviroment
STK600 programming: 
[JTAG](http://www.atmel.com/webdoc/stk600/stk600.programming_jtag.html)
[External](http://www.atmel.com/webdoc/stk600/stk600.programming_ext.html)

