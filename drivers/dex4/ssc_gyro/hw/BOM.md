
### Where to buy

* LS7266
http://www.usdigital.com/products/interfaces/ics/LFLS7266R1-S

* J1011F01P
http://www.digikey.com/product-detail/en/J1011F01PNL/553-1350-ND/1036966


### Build errata SSC2 board 1
* R49-R72 (encoder input resistors) used 10K instead of 1K.



### Connector for gyro: SAM1129-12-ND
http://www.digikey.com/scripts/DkSearch/dksus.dll?Detail&itemSeq=170754125&uq=635673111084366285

### Mating connectors for pots:
DF13-3S-1.25C
H4BXG-10104-A8-ND
H4BXG-10104-R8-ND
H4BXG-10104-B8-ND

