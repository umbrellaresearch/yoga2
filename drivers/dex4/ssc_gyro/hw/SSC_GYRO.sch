<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.2.0">
<drawing>
<settings>
<setting alwaysvectorfont="yes"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="11" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="3" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="2" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="14" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="yes" active="yes"/>
<layer number="100" name="panelcutout" color="10" fill="1" visible="yes" active="yes"/>
<layer number="101" name="Frames" color="7" fill="1" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="16" fill="1" visible="yes" active="yes"/>
<layer number="105" name="tRes" color="12" fill="1" visible="yes" active="yes"/>
<layer number="106" name="bRes" color="1" fill="1" visible="yes" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="prix" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="test" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic>
<libraries>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="VCCINT">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<text x="-0.889" y="0.254" size="0.8128" layer="94">INT</text>
<pin name="VCCINT" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCCINT" prefix="VCC">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="VCCINT" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rcl">
<packages>
<package name="C025-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.778" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.778" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-025X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 2.5 x 5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-030X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 3 x 5 mm</description>
<wire x1="-2.159" y1="1.524" x2="2.159" y2="1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.524" x2="-2.159" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.27" x2="2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.524" x2="2.413" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.27" x2="-2.159" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.524" x2="2.413" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.27" x2="-2.159" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-040X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 4 x 5 mm</description>
<wire x1="-2.159" y1="1.905" x2="2.159" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.905" x2="-2.159" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.651" x2="2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.651" x2="-2.413" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.905" x2="2.413" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.651" x2="-2.159" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.905" x2="2.413" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.651" x2="-2.159" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025-060X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 6 x 5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.048" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.27" x2="2.159" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.27" x2="-2.159" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.27" x2="2.413" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.016" x2="-2.159" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.27" x2="2.413" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.016" x2="-2.159" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.016" x2="4.953" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.27" x2="4.953" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.27" x2="4.953" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.27" x2="4.699" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.27" x2="2.794" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.413" y2="0.762" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-0.762" x2="2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.254" x2="2.413" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-2.159" y1="1.778" x2="2.159" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-1.778" x2="-2.159" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.524" x2="-2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.159" y1="1.778" x2="2.413" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="1.524" x2="-2.159" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-1.778" x2="2.413" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-1.524" x2="-2.159" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="2.794" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.524" x2="2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.413" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.508" x2="2.413" y2="-0.508" width="0.1524" layer="21"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.302" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.032" x2="4.953" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.286" x2="4.953" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.286" x2="4.953" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.286" x2="4.699" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.286" x2="2.794" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="1.397" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.397" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="1.778" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 + 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-2.159" y1="2.794" x2="2.159" y2="2.794" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.794" x2="-2.159" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.54" x2="-2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.794" x2="2.413" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.54" x2="-2.159" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.794" x2="2.413" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.54" x2="-2.159" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="4.953" y1="2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.794" x2="4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-2.794" x2="4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="2.794" y1="2.794" x2="4.699" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.794" x2="2.794" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.54" x2="2.413" y2="2.032" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-2.032" x2="2.413" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.413" y1="0.762" x2="2.413" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0" x2="2.286" y2="0" width="0.1524" layer="51"/>
<wire x1="2.286" y1="0" x2="2.794" y2="0" width="0.1524" layer="21"/>
<wire x1="2.794" y1="0" x2="3.302" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-025X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.016" x2="-3.683" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.27" x2="3.429" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.016" x2="3.683" y2="1.016" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="-3.429" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.27" x2="3.683" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.27" x2="3.683" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.016" x2="-3.429" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.016" x2="-3.429" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-045X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 4.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.032" x2="-3.683" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.286" x2="3.429" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.032" x2="3.683" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="-3.429" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.286" x2="3.683" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.286" x2="3.683" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.032" x2="-3.429" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.032" x2="-3.429" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-030X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.27" x2="-3.683" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.27" x2="3.683" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.524" x2="3.683" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.524" x2="3.683" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.27" x2="-3.429" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.27" x2="-3.429" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-050X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.54" x2="3.429" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="-3.429" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.54" x2="3.683" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.54" x2="3.683" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.286" x2="-3.429" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.286" x2="-3.429" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-055X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 5.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.54" x2="-3.683" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-2.794" x2="3.429" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.54" x2="3.683" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="-3.429" y2="2.794" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.794" x2="3.683" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-2.794" x2="3.683" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-2.54" x2="-3.429" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="2.54" x2="-3.429" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.302" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050H075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Horizontal, grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-3.683" y1="7.112" x2="-3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="0.508" x2="-3.302" y2="0.508" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.508" x2="-1.778" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="0.508" x2="1.778" y2="0.508" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.508" x2="3.302" y2="0.508" width="0.1524" layer="51"/>
<wire x1="3.302" y1="0.508" x2="3.683" y2="0.508" width="0.1524" layer="21"/>
<wire x1="3.683" y1="0.508" x2="3.683" y2="7.112" width="0.1524" layer="21"/>
<wire x1="3.175" y1="7.62" x2="-3.175" y2="7.62" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="2.413" x2="-0.3048" y2="1.778" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-0.3048" y2="1.143" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="1.778" x2="-1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="2.413" x2="0.3302" y2="1.778" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="0.3302" y2="1.143" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="1.778" x2="1.651" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="7.112" x2="-3.175" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.175" y1="7.62" x2="3.683" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.254" width="0.508" layer="51"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.254" width="0.508" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.302" y="8.001" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="3.175" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="0.127" x2="-2.286" y2="0.508" layer="51"/>
<rectangle x1="2.286" y1="0.127" x2="2.794" y2="0.508" layer="51"/>
</package>
<package name="C075-032X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 3.2 x 10.3 mm</description>
<wire x1="4.826" y1="1.524" x2="-4.826" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-1.524" x2="4.826" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.826" y1="1.524" x2="5.08" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-1.524" x2="5.08" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.27" x2="-4.826" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.27" x2="-4.826" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="0.508" y1="0" x2="2.54" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0" x2="-0.508" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.889" x2="-0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0" x2="-0.508" y2="-0.889" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.889" x2="0.508" y2="0" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0" x2="0.508" y2="-0.889" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.826" y="-3.048" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-042X103">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 4.2 x 10.3 mm</description>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.778" x2="-5.08" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.778" x2="5.08" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="5.08" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.826" y1="-2.032" x2="5.08" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="-1.778" x2="-4.826" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.778" x2="-4.826" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.699" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-052X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 5.2 x 10.6 mm</description>
<wire x1="4.953" y1="2.54" x2="-4.953" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.286" x2="-5.207" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.54" x2="4.953" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.286" x2="5.207" y2="2.286" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.54" x2="5.207" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-2.54" x2="5.207" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.286" x2="-4.953" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.286" x2="-4.953" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-043X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 4.3 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.032" x2="6.096" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.604" y1="1.524" x2="6.604" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.032" x2="-6.096" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-1.524" x2="-6.604" y2="1.524" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.032" x2="6.604" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.032" x2="6.604" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-1.524" x2="-6.096" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="1.524" x2="-6.096" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.651" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-054X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 5.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="2.54" x2="6.096" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.032" x2="6.604" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-2.54" x2="-6.096" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.032" x2="-6.604" y2="2.032" width="0.1524" layer="21"/>
<wire x1="6.096" y1="2.54" x2="6.604" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-2.54" x2="6.604" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.032" x2="-6.096" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.032" x2="-6.096" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-1.905" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102-064X133">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm, outline 6.4 x 13.3 mm</description>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="3.81" y1="0" x2="-2.286" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.81" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.096" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.604" y1="2.54" x2="6.604" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="6.604" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="-3.048" x2="6.604" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C102_152-062X184">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 10.2 mm + 15.2 mm, outline 6.2 x 18.4 mm</description>
<wire x1="-2.286" y1="1.27" x2="-2.286" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-3.175" y2="0" width="0.4064" layer="21"/>
<wire x1="-3.175" y1="0" x2="-3.175" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="0" x2="-3.175" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="0" x2="3.683" y2="0" width="0.1524" layer="21"/>
<wire x1="6.477" y1="0" x2="8.636" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="3.048" x2="6.223" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-3.048" x2="-6.096" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.54" x2="-6.604" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.223" y1="3.048" x2="6.731" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.223" y1="-3.048" x2="6.731" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="-2.54" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-6.604" y1="2.54" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="2.54" x2="6.731" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="11.176" y1="3.048" x2="11.684" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="11.176" y1="-3.048" x2="11.684" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="11.176" y1="-3.048" x2="7.112" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="7.112" y1="3.048" x2="11.176" y2="3.048" width="0.1524" layer="21"/>
<wire x1="11.684" y1="2.54" x2="11.684" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="3" x="10.033" y="0" drill="1.016" shape="octagon"/>
<text x="-5.969" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.524" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-054X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 5.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.032" x2="9.017" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-2.54" x2="-8.509" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.032" x2="-9.017" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="2.54" x2="8.509" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="2.54" x2="9.017" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-2.54" x2="9.017" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.032" x2="-8.509" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.032" x2="-8.509" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-064X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 6.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="2.54" x2="9.017" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.048" x2="-8.509" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-2.54" x2="-9.017" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.048" x2="8.509" y2="3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.048" x2="9.017" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.048" x2="9.017" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-2.54" x2="-8.509" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="2.54" x2="-8.509" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.032" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-072X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 7.2 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.048" x2="9.017" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.556" x2="-8.509" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.048" x2="-9.017" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="3.556" x2="8.509" y2="3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="3.556" x2="9.017" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-3.556" x2="9.017" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.048" x2="-8.509" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.048" x2="-8.509" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="3.937" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.286" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-084X183">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 8.4 x 18.3 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.556" x2="9.017" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.064" x2="-8.509" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.556" x2="-9.017" y2="3.556" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.064" x2="8.509" y2="4.064" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.064" x2="9.017" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.064" x2="9.017" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.556" x2="-8.509" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.556" x2="-8.509" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.445" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C150-091X182">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 15 mm, outline 9.1 x 18.2 mm</description>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="1.27" x2="-4.191" y2="0" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="-4.191" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-4.191" y1="0" x2="6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0" x2="-6.096" y2="0" width="0.1524" layer="21"/>
<wire x1="9.017" y1="3.937" x2="9.017" y2="-3.937" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-4.445" x2="-8.509" y2="-4.445" width="0.1524" layer="21"/>
<wire x1="-9.017" y1="-3.937" x2="-9.017" y2="3.937" width="0.1524" layer="21"/>
<wire x1="-8.509" y1="4.445" x2="8.509" y2="4.445" width="0.1524" layer="21"/>
<wire x1="8.509" y1="4.445" x2="9.017" y2="3.937" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.509" y1="-4.445" x2="9.017" y2="-3.937" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="-3.937" x2="-8.509" y2="-4.445" width="0.1524" layer="21" curve="90"/>
<wire x1="-9.017" y1="3.937" x2="-8.509" y2="4.445" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-7.493" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.493" y="0" drill="1.016" shape="octagon"/>
<text x="-8.509" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.429" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-062X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 6.2 x 26.8 mm</description>
<wire x1="-12.827" y1="3.048" x2="12.827" y2="3.048" width="0.1524" layer="21"/>
<wire x1="13.335" y1="2.54" x2="13.335" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.048" x2="-12.827" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.335" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.048" x2="13.335" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.048" x2="13.335" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-2.54" x2="-12.827" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="2.54" x2="-12.827" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.7" y="3.429" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-074X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 7.4 x 26.8 mm</description>
<wire x1="-12.827" y1="3.556" x2="12.827" y2="3.556" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.048" x2="13.335" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-3.556" x2="-12.827" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.048" x2="-13.335" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="3.556" x2="13.335" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-3.556" x2="13.335" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.048" x2="-12.827" y2="-3.556" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.048" x2="-12.827" y2="3.556" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="3.937" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-087X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 8.7 x 26.8 mm</description>
<wire x1="-12.827" y1="4.318" x2="12.827" y2="4.318" width="0.1524" layer="21"/>
<wire x1="13.335" y1="3.81" x2="13.335" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-4.318" x2="-12.827" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-3.81" x2="-13.335" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="4.318" x2="13.335" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-4.318" x2="13.335" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-3.81" x2="-12.827" y2="-4.318" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="3.81" x2="-12.827" y2="4.318" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.827" y="4.699" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-108X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 10.8 x 26.8 mm</description>
<wire x1="-12.827" y1="5.334" x2="12.827" y2="5.334" width="0.1524" layer="21"/>
<wire x1="13.335" y1="4.826" x2="13.335" y2="-4.826" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.334" x2="-12.827" y2="-5.334" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-4.826" x2="-13.335" y2="4.826" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.334" x2="13.335" y2="4.826" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.334" x2="13.335" y2="-4.826" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-4.826" x2="-12.827" y2="-5.334" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="4.826" x2="-12.827" y2="5.334" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.715" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C225-113X268">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 22.5 mm, outline 11.3 x 26.8 mm</description>
<wire x1="-12.827" y1="5.588" x2="12.827" y2="5.588" width="0.1524" layer="21"/>
<wire x1="13.335" y1="5.08" x2="13.335" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="12.827" y1="-5.588" x2="-12.827" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-13.335" y1="-5.08" x2="-13.335" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="12.827" y1="5.588" x2="13.335" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="12.827" y1="-5.588" x2="13.335" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="-5.08" x2="-12.827" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-13.335" y1="5.08" x2="-12.827" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.652" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="9.652" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-11.303" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.303" y="0" drill="1.016" shape="octagon"/>
<text x="-12.954" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-093X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 9.3 x 31.6 mm</description>
<wire x1="-15.24" y1="4.572" x2="15.24" y2="4.572" width="0.1524" layer="21"/>
<wire x1="15.748" y1="4.064" x2="15.748" y2="-4.064" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-4.572" x2="-15.24" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-4.064" x2="-15.748" y2="4.064" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="4.572" x2="15.748" y2="4.064" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-4.572" x2="15.748" y2="-4.064" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-4.064" x2="-15.24" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="4.064" x2="-15.24" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="4.953" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-113X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 11.3 x 31.6 mm</description>
<wire x1="-15.24" y1="5.588" x2="15.24" y2="5.588" width="0.1524" layer="21"/>
<wire x1="15.748" y1="5.08" x2="15.748" y2="-5.08" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-5.588" x2="-15.24" y2="-5.588" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-5.08" x2="-15.748" y2="5.08" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="5.588" x2="15.748" y2="5.08" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-5.588" x2="15.748" y2="-5.08" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-5.08" x2="-15.24" y2="-5.588" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="5.08" x2="-15.24" y2="5.588" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="5.969" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-134X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 13.4 x 31.6 mm</description>
<wire x1="-15.24" y1="6.604" x2="15.24" y2="6.604" width="0.1524" layer="21"/>
<wire x1="15.748" y1="6.096" x2="15.748" y2="-6.096" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-6.604" x2="-15.24" y2="-6.604" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-6.096" x2="-15.748" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="6.604" x2="15.748" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-6.604" x2="15.748" y2="-6.096" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-6.096" x2="-15.24" y2="-6.604" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="6.096" x2="-15.24" y2="6.604" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="6.985" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-205X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 20.5 x 31.6 mm</description>
<wire x1="-15.24" y1="10.16" x2="15.24" y2="10.16" width="0.1524" layer="21"/>
<wire x1="15.748" y1="9.652" x2="15.748" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-10.16" x2="-15.24" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-9.652" x2="-15.748" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="10.16" x2="15.748" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-10.16" x2="15.748" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-9.652" x2="-15.24" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="9.652" x2="-15.24" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.318" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-137X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 13.7 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="6.731" x2="-18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-6.731" x2="18.542" y2="-6.731" width="0.1524" layer="21"/>
<wire x1="18.542" y1="6.731" x2="-18.542" y2="6.731" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.2372" y="7.0612" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-162X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 16.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="8.001" x2="-18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-8.001" x2="18.542" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="18.542" y1="8.001" x2="-18.542" y2="8.001" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="8.3312" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C325-182X374">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 32.5 mm, outline 18.2 x 37.4 mm</description>
<wire x1="-14.2748" y1="0" x2="-12.7" y2="0" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.7" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="1.905" x2="-11.811" y2="0" width="0.4064" layer="21"/>
<wire x1="-11.811" y1="0" x2="14.2748" y2="0" width="0.1524" layer="21"/>
<wire x1="-11.811" y1="0" x2="-11.811" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-12.7" y1="0" x2="-12.7" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="18.542" y1="9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="9.017" x2="-18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="-18.542" y1="-9.017" x2="18.542" y2="-9.017" width="0.1524" layer="21"/>
<wire x1="18.542" y1="9.017" x2="-18.542" y2="9.017" width="0.1524" layer="21"/>
<pad name="1" x="-16.256" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="16.256" y="0" drill="1.1938" shape="octagon"/>
<text x="-18.3642" y="9.3472" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-10.8458" y="-2.8702" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-192X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 19.2 x 41.8 mm</description>
<wire x1="-20.32" y1="8.509" x2="20.32" y2="8.509" width="0.1524" layer="21"/>
<wire x1="20.828" y1="8.001" x2="20.828" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-8.509" x2="-20.32" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-8.001" x2="-20.828" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="8.509" x2="20.828" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-8.509" x2="20.828" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-8.001" x2="-20.32" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="8.001" x2="-20.32" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-203X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 20.3 x 41.8 mm</description>
<wire x1="-20.32" y1="10.16" x2="20.32" y2="10.16" width="0.1524" layer="21"/>
<wire x1="20.828" y1="9.652" x2="20.828" y2="-9.652" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-10.16" x2="-20.32" y2="-10.16" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-9.652" x2="-20.828" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="10.16" x2="20.828" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-10.16" x2="20.828" y2="-9.652" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-9.652" x2="-20.32" y2="-10.16" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="9.652" x2="-20.32" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.32" y="10.541" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-035X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 3.5 x 7.5 mm</description>
<wire x1="-0.3048" y1="0.635" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.3302" y1="0.635" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="1.524" x2="-3.683" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.778" x2="3.429" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-1.524" x2="3.683" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="-3.429" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.429" y1="1.778" x2="3.683" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-1.778" x2="3.683" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-1.524" x2="-3.429" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="1.524" x2="-3.429" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.556" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.556" y="-3.429" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C375-155X418">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 37.5 mm, outline 15.5 x 41.8 mm</description>
<wire x1="-20.32" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="21"/>
<wire x1="20.828" y1="7.112" x2="20.828" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="20.32" y1="-7.62" x2="-20.32" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-20.828" y1="-7.112" x2="-20.828" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="20.32" y1="7.62" x2="20.828" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="20.32" y1="-7.62" x2="20.828" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="-7.112" x2="-20.32" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-20.828" y1="7.112" x2="-20.32" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-16.002" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="16.002" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-18.796" y="0" drill="1.3208" shape="octagon"/>
<pad name="2" x="18.796" y="0" drill="1.3208" shape="octagon"/>
<text x="-20.447" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C075-063X106">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 7.5 mm, outline 6.3 x 10.6 mm</description>
<wire x1="4.953" y1="3.048" x2="-4.953" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.794" x2="-5.207" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-3.048" x2="4.953" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.794" x2="5.207" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="3.048" x2="5.207" y2="2.794" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.953" y1="-3.048" x2="5.207" y2="-2.794" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="-2.794" x2="-4.953" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.207" y1="2.794" x2="-4.953" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="0" x2="2.667" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.667" y1="0" x2="-2.159" y2="0" width="0.1524" layer="21"/>
<wire x1="-2.159" y1="1.27" x2="-2.159" y2="0" width="0.4064" layer="21"/>
<wire x1="-2.159" y1="0" x2="-2.159" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.4064" layer="21"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.4064" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.9144" shape="octagon"/>
<text x="-4.826" y="3.429" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-154X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 15.4 x 31.6 mm</description>
<wire x1="-15.24" y1="7.62" x2="15.24" y2="7.62" width="0.1524" layer="21"/>
<wire x1="15.748" y1="7.112" x2="15.748" y2="-7.112" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-7.62" x2="-15.24" y2="-7.62" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-7.112" x2="-15.748" y2="7.112" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="7.62" x2="15.748" y2="7.112" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-7.62" x2="15.748" y2="-7.112" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-7.112" x2="-15.24" y2="-7.62" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="7.112" x2="-15.24" y2="7.62" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.001" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C275-173X316">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 27.5 mm, outline 17.3 x 31.6 mm</description>
<wire x1="-15.24" y1="8.509" x2="15.24" y2="8.509" width="0.1524" layer="21"/>
<wire x1="15.748" y1="8.001" x2="15.748" y2="-8.001" width="0.1524" layer="21"/>
<wire x1="15.24" y1="-8.509" x2="-15.24" y2="-8.509" width="0.1524" layer="21"/>
<wire x1="-15.748" y1="-8.001" x2="-15.748" y2="8.001" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="1.905" x2="-6.731" y2="0" width="0.4064" layer="21"/>
<wire x1="-6.731" y1="0" x2="-6.731" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="0" width="0.4064" layer="21"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="15.24" y1="8.509" x2="15.748" y2="8.001" width="0.1524" layer="21" curve="-90"/>
<wire x1="15.24" y1="-8.509" x2="15.748" y2="-8.001" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="-8.001" x2="-15.24" y2="-8.509" width="0.1524" layer="21" curve="90"/>
<wire x1="-15.748" y1="8.001" x2="-15.24" y2="8.509" width="0.1524" layer="21" curve="-90"/>
<wire x1="-11.557" y1="0" x2="-7.62" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="0" x2="11.557" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-13.716" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="13.716" y="0" drill="1.1938" shape="octagon"/>
<text x="-15.24" y="8.89" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C0805">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0504">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C0603">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1210">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1310">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.294" y1="0.559" x2="0.294" y2="0.559" width="0.1016" layer="51"/>
<wire x1="-0.294" y1="-0.559" x2="0.294" y2="-0.559" width="0.1016" layer="51"/>
<smd name="1" x="-0.7" y="0" dx="1" dy="1.3" layer="1"/>
<smd name="2" x="0.7" y="0" dx="1" dy="1.3" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.6604" y1="-0.6223" x2="-0.2804" y2="0.6276" layer="51"/>
<rectangle x1="0.2794" y1="-0.6223" x2="0.6594" y2="0.6276" layer="51"/>
<rectangle x1="-0.1001" y1="-0.3" x2="0.1001" y2="0.3" layer="35"/>
</package>
<package name="C1608">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.356" y1="0.432" x2="0.356" y2="0.432" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.419" x2="0.356" y2="-0.419" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.1" dy="1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.1" dy="1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8382" y1="-0.4699" x2="-0.3381" y2="0.4801" layer="51"/>
<rectangle x1="0.3302" y1="-0.4699" x2="0.8303" y2="0.4801" layer="51"/>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
</package>
<package name="C1812">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.3" y1="-0.4001" x2="0.3" y2="0.4001" layer="35"/>
</package>
<package name="C1825">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.7" y1="-0.7" x2="0.7" y2="0.7" layer="35"/>
</package>
<package name="C2012">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="-0.381" y1="0.66" x2="0.381" y2="0.66" width="0.1016" layer="51"/>
<wire x1="-0.356" y1="-0.66" x2="0.381" y2="-0.66" width="0.1016" layer="51"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0922" y1="-0.7239" x2="-0.3421" y2="0.7262" layer="51"/>
<rectangle x1="0.3556" y1="-0.7239" x2="1.1057" y2="0.7262" layer="51"/>
<rectangle x1="-0.1001" y1="-0.4001" x2="0.1001" y2="0.4001" layer="35"/>
</package>
<package name="C3216">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.3" y1="-0.5001" x2="0.3" y2="0.5001" layer="35"/>
</package>
<package name="C3225">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="-0.9652" y1="1.2446" x2="0.9652" y2="1.2446" width="0.1016" layer="51"/>
<wire x1="-0.9652" y1="-1.2446" x2="0.9652" y2="-1.2446" width="0.1016" layer="51"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-1.2954" x2="-0.9517" y2="1.3045" layer="51"/>
<rectangle x1="0.9517" y1="-1.3045" x2="1.7018" y2="1.2954" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="C4532">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="1.983" x2="2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-1.983" x2="-2.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-1.983" x2="-2.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="-1.4732" y1="1.6002" x2="1.4732" y2="1.6002" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-1.6002" x2="1.4732" y2="-1.6002" width="0.1016" layer="51"/>
<wire x1="2.973" y1="1.983" x2="2.973" y2="-1.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="3.4" layer="1"/>
<text x="-1.905" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.3876" y1="-1.651" x2="-1.4376" y2="1.649" layer="51"/>
<rectangle x1="1.4478" y1="-1.651" x2="2.3978" y2="1.649" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="C4564">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.973" y1="3.483" x2="2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-3.483" x2="-2.973" y2="-3.483" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-3.483" x2="-2.973" y2="3.483" width="0.0508" layer="39"/>
<wire x1="-1.4986" y1="3.2766" x2="1.4732" y2="3.2766" width="0.1016" layer="51"/>
<wire x1="-1.4732" y1="-3.2766" x2="1.4986" y2="-3.2766" width="0.1016" layer="51"/>
<wire x1="2.973" y1="3.483" x2="2.973" y2="-3.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.9" dy="6.8" layer="1"/>
<text x="-1.905" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.413" y1="-3.3528" x2="-1.463" y2="3.3472" layer="51"/>
<rectangle x1="1.4478" y1="-3.3528" x2="2.3978" y2="3.3472" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="C025-050X050">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm, outline 5 x 5 mm</description>
<wire x1="-2.159" y1="2.286" x2="2.159" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-2.286" x2="-2.159" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.413" y1="2.032" x2="2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="2.032" x2="-2.413" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="2.159" y1="2.286" x2="2.413" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.413" y1="2.032" x2="-2.159" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.159" y1="-2.286" x2="2.413" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.413" y1="-2.032" x2="-2.159" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="0.762" y1="0" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.254" y2="0" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="0.762" width="0.254" layer="21"/>
<wire x1="0.254" y1="0" x2="0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="-0.254" y2="0" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.254" y2="-0.762" width="0.254" layer="21"/>
<wire x1="-0.254" y1="0" x2="-0.381" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.286" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C025_050-024X070">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 2.5 mm + 5 mm, outline 2.4 x 7 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.191" y1="-1.143" x2="-3.9624" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-4.191" y1="1.143" x2="-3.9624" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-0.635" x2="-4.191" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="0.635" x2="-4.191" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.699" y1="-0.635" x2="-4.699" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.143" x2="-2.5654" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-1.143" x2="-2.5654" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="3" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.81" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C050-024X044">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 2.4 x 4.4 mm</description>
<wire x1="-2.159" y1="-0.635" x2="-2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="0.635" x2="-1.651" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.159" y1="-0.635" x2="-1.651" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="1.651" y1="1.143" x2="-1.651" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.159" y1="-0.635" x2="2.159" y2="0.635" width="0.1524" layer="51"/>
<wire x1="1.651" y1="-1.143" x2="-1.651" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="1.651" y1="1.143" x2="2.159" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="1.651" y1="-1.143" x2="2.159" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.3048" y1="0.762" x2="-0.3048" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3048" y1="0" x2="-0.3048" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0.762" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="-0.762" width="0.3048" layer="21"/>
<wire x1="1.27" y1="0" x2="0.3302" y2="0" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0" x2="-0.3048" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.159" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.159" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.159" y1="-0.381" x2="2.54" y2="0.381" layer="51"/>
<rectangle x1="-2.54" y1="-0.381" x2="-2.159" y2="0.381" layer="51"/>
</package>
<package name="C050-075X075">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
grid 5 mm, outline 7.5 x 7.5 mm</description>
<wire x1="-1.524" y1="0" x2="-0.4572" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="0.762" width="0.4064" layer="21"/>
<wire x1="-0.4572" y1="0" x2="-0.4572" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0.762" x2="0.4318" y2="0" width="0.4064" layer="21"/>
<wire x1="0.4318" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.4318" y1="0" x2="0.4318" y2="-0.762" width="0.4064" layer="21"/>
<wire x1="-3.683" y1="3.429" x2="-3.683" y2="-3.429" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="-3.683" x2="3.429" y2="-3.683" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-3.429" x2="3.683" y2="3.429" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="-3.429" y2="3.683" width="0.1524" layer="21"/>
<wire x1="3.429" y1="3.683" x2="3.683" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="3.429" y1="-3.683" x2="3.683" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="-3.429" x2="-3.429" y2="-3.683" width="0.1524" layer="21" curve="90"/>
<wire x1="-3.683" y1="3.429" x2="-3.429" y2="3.683" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.429" y="4.064" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="C0402K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0204 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1005</description>
<wire x1="-0.425" y1="0.2" x2="0.425" y2="0.2" width="0.1016" layer="51"/>
<wire x1="0.425" y1="-0.2" x2="-0.425" y2="-0.2" width="0.1016" layer="51"/>
<smd name="1" x="-0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<smd name="2" x="0.6" y="0" dx="0.925" dy="0.74" layer="1"/>
<text x="-0.5" y="0.425" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.5" y="-1.45" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.5" y1="-0.25" x2="-0.225" y2="0.25" layer="51"/>
<rectangle x1="0.225" y1="-0.25" x2="0.5" y2="0.25" layer="51"/>
</package>
<package name="C0603K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0603 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 1608</description>
<wire x1="-0.725" y1="0.35" x2="0.725" y2="0.35" width="0.1016" layer="51"/>
<wire x1="0.725" y1="-0.35" x2="-0.725" y2="-0.35" width="0.1016" layer="51"/>
<smd name="1" x="-0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<smd name="2" x="0.875" y="0" dx="1.05" dy="1.08" layer="1"/>
<text x="-0.8" y="0.65" size="1.016" layer="25">&gt;NAME</text>
<text x="-0.8" y="-1.65" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-0.4" x2="-0.45" y2="0.4" layer="51"/>
<rectangle x1="0.45" y1="-0.4" x2="0.8" y2="0.4" layer="51"/>
</package>
<package name="C0805K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 0805 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 2012</description>
<wire x1="-0.925" y1="0.6" x2="0.925" y2="0.6" width="0.1016" layer="51"/>
<wire x1="0.925" y1="-0.6" x2="-0.925" y2="-0.6" width="0.1016" layer="51"/>
<smd name="1" x="-1" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="1" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1" y="0.875" size="1.016" layer="25">&gt;NAME</text>
<text x="-1" y="-1.9" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1" y1="-0.65" x2="-0.5" y2="0.65" layer="51"/>
<rectangle x1="0.5" y1="-0.65" x2="1" y2="0.65" layer="51"/>
</package>
<package name="C1206K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1206 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3216</description>
<wire x1="-1.525" y1="0.75" x2="1.525" y2="0.75" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-0.75" x2="-1.525" y2="-0.75" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2" layer="1"/>
<text x="-1.6" y="1.1" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.1" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-0.8" x2="-1.1" y2="0.8" layer="51"/>
<rectangle x1="1.1" y1="-0.8" x2="1.6" y2="0.8" layer="51"/>
</package>
<package name="C1210K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1210 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 3225</description>
<wire x1="-1.525" y1="1.175" x2="1.525" y2="1.175" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-1.175" x2="-1.525" y2="-1.175" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.5" dy="2.9" layer="1"/>
<text x="-1.6" y="1.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-2.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-1.25" x2="-1.1" y2="1.25" layer="51"/>
<rectangle x1="1.1" y1="-1.25" x2="1.6" y2="1.25" layer="51"/>
</package>
<package name="C1812K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1812 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4532</description>
<wire x1="-2.175" y1="1.525" x2="2.175" y2="1.525" width="0.1016" layer="51"/>
<wire x1="2.175" y1="-1.525" x2="-2.175" y2="-1.525" width="0.1016" layer="51"/>
<smd name="1" x="-2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<smd name="2" x="2.05" y="0" dx="1.8" dy="3.7" layer="1"/>
<text x="-2.25" y="1.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.25" y="-2.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.25" y1="-1.6" x2="-1.65" y2="1.6" layer="51"/>
<rectangle x1="1.65" y1="-1.6" x2="2.25" y2="1.6" layer="51"/>
</package>
<package name="C1825K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 1825 reflow solder&lt;/b&gt;&lt;p&gt;
Metric Code Size 4564</description>
<wire x1="-1.525" y1="3.125" x2="1.525" y2="3.125" width="0.1016" layer="51"/>
<wire x1="1.525" y1="-3.125" x2="-1.525" y2="-3.125" width="0.1016" layer="51"/>
<smd name="1" x="-1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.8" dy="6.9" layer="1"/>
<text x="-1.6" y="3.55" size="1.016" layer="25">&gt;NAME</text>
<text x="-1.6" y="-4.625" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6" y1="-3.2" x2="-1.1" y2="3.2" layer="51"/>
<rectangle x1="1.1" y1="-3.2" x2="1.6" y2="3.2" layer="51"/>
</package>
<package name="C2220K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2220 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5650</description>
<wire x1="-2.725" y1="2.425" x2="2.725" y2="2.425" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-2.425" x2="-2.725" y2="-2.425" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="5.5" layer="1"/>
<text x="-2.8" y="2.95" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-3.975" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-2.5" x2="-2.2" y2="2.5" layer="51"/>
<rectangle x1="2.2" y1="-2.5" x2="2.8" y2="2.5" layer="51"/>
</package>
<package name="C2225K">
<description>&lt;b&gt;Ceramic Chip Capacitor KEMET 2225 reflow solder&lt;/b&gt;&lt;p&gt;Metric Code Size 5664</description>
<wire x1="-2.725" y1="3.075" x2="2.725" y2="3.075" width="0.1016" layer="51"/>
<wire x1="2.725" y1="-3.075" x2="-2.725" y2="-3.075" width="0.1016" layer="51"/>
<smd name="1" x="-2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<smd name="2" x="2.55" y="0" dx="1.85" dy="6.8" layer="1"/>
<text x="-2.8" y="3.6" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.8" y="-4.575" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-2.8" y1="-3.15" x2="-2.2" y2="3.15" layer="51"/>
<rectangle x1="2.2" y1="-3.15" x2="2.8" y2="3.15" layer="51"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="C1808">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-1.4732" y1="0.9502" x2="1.4732" y2="0.9502" width="0.1016" layer="51"/>
<wire x1="-1.4478" y1="-0.9502" x2="1.4732" y2="-0.9502" width="0.1016" layer="51"/>
<smd name="1" x="-1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="1.95" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-2.233" y="1.827" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.233" y="-2.842" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.275" y1="-1.015" x2="-1.225" y2="1.015" layer="51"/>
<rectangle x1="1.225" y1="-1.015" x2="2.275" y2="1.015" layer="51"/>
</package>
<package name="C3640">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;&lt;p&gt;
Source: AVX .. aphvc.pdf</description>
<wire x1="-3.8322" y1="5.0496" x2="3.8322" y2="5.0496" width="0.1016" layer="51"/>
<wire x1="-3.8322" y1="-5.0496" x2="3.8322" y2="-5.0496" width="0.1016" layer="51"/>
<smd name="1" x="-4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<smd name="2" x="4.267" y="0" dx="2.6" dy="10.7" layer="1"/>
<text x="-4.647" y="6.465" size="1.27" layer="25">&gt;NAME</text>
<text x="-4.647" y="-7.255" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-4.57" y1="-5.1" x2="-3.05" y2="5.1" layer="51"/>
<rectangle x1="3.05" y1="-5.1" x2="4.5688" y2="5.1" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
<package name="C0201">
<description>Source: http://www.avxcorp.com/docs/catalogs/cx5r.pdf</description>
<smd name="1" x="-0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<smd name="2" x="0.25" y="0" dx="0.25" dy="0.35" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="0.1" x2="0.15" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="-0.1" layer="51"/>
</package>
<package name="C01005">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<rectangle x1="-0.1999" y1="-0.3" x2="0.1999" y2="0.3" layer="35"/>
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="C-US">
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="-1.016" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-1" x2="2.4892" y2="-1.8542" width="0.254" layer="94" curve="-37.878202"/>
<wire x1="-2.4668" y1="-1.8504" x2="0" y2="-1.0161" width="0.254" layer="94" curve="-37.373024"/>
<text x="1.016" y="0.635" size="1.778" layer="95">&gt;NAME</text>
<text x="1.016" y="-4.191" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="C-US" prefix="C" uservalue="yes">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="C-US" x="0" y="0"/>
</gates>
<devices>
<device name="C0402" package="C0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0504" package="C0504">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603" package="C0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805" package="C0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210" package="C1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1310" package="C1310">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="C1608">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812" package="C1812">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825" package="C1825">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2012" package="C2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="C3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3225" package="C3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4532" package="C4532">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C4564" package="C4564">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-024X044" package="C025-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-025X050" package="C025-025X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-030X050" package="C025-030X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-040X050" package="C025-040X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-050X050" package="C025-050X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025-060X050" package="C025-060X050">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C025_050-024X070" package="C025_050-024X070">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-025X075" package="C025_050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-035X075" package="C025_050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-045X075" package="C025_050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="025_050-055X075" package="C025_050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-024X044" package="C050-024X044">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-025X075" package="C050-025X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-045X075" package="C050-045X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-030X075" package="C050-030X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-050X075" package="C050-050X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-055X075" package="C050-055X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-075X075" package="C050-075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050H075X075" package="C050H075X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-032X103" package="C075-032X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-042X103" package="C075-042X103">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-052X106" package="C075-052X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-043X133" package="C102-043X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-054X133" package="C102-054X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102-064X133" package="C102-064X133">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="102_152-062X184" package="C102_152-062X184">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-054X183" package="C150-054X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-064X183" package="C150-064X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-072X183" package="C150-072X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-084X183" package="C150-084X183">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="150-091X182" package="C150-091X182">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-062X268" package="C225-062X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-074X268" package="C225-074X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-087X268" package="C225-087X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-108X268" package="C225-108X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="225-113X268" package="C225-113X268">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-093X316" package="C275-093X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-113X316" package="C275-113X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-134X316" package="C275-134X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-205X316" package="C275-205X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-137X374" package="C325-137X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-162X374" package="C325-162X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="325-182X374" package="C325-182X374">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-192X418" package="C375-192X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-203X418" package="C375-203X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="050-035X075" package="C050-035X075">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="375-155X418" package="C375-155X418">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="075-063X106" package="C075-063X106">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-154X316" package="C275-154X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="275-173X316" package="C275-173X316">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0402K" package="C0402K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0603K" package="C0603K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0805K" package="C0805K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1206K" package="C1206K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1210K" package="C1210K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1812K" package="C1812K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1825K" package="C1825K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2220K" package="C2220K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C2225K" package="C2225K">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C0201" package="C0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1808" package="C1808">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3640" package="C3640">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="C01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-US_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, American symbol</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-lstb">
<packages>
<package name="MA05-2">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-5.715" y1="2.54" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="1.905" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="1.905" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="3.81" y2="1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.905" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="1.27" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="1.905" x2="-6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="-1.905" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="-1.27" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="3.81" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="3.81" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-1.905" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.35" y2="1.905" width="0.1524" layer="21"/>
<wire x1="6.35" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="3" x="-2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="5" x="0" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="7" x="2.54" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="9" x="5.08" y="-1.27" drill="1.016" shape="octagon"/>
<pad name="2" x="-5.08" y="1.27" drill="1.016" shape="octagon"/>
<pad name="4" x="-2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="6" x="0" y="1.27" drill="1.016" shape="octagon"/>
<pad name="8" x="2.54" y="1.27" drill="1.016" shape="octagon"/>
<pad name="10" x="5.08" y="1.27" drill="1.016" shape="octagon"/>
<text x="-5.588" y="-4.191" size="1.27" layer="21" ratio="10">1</text>
<text x="-6.35" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="2.921" size="1.27" layer="21" ratio="10">10</text>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="-1.524" x2="-4.826" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="4.826" y1="-1.524" x2="5.334" y2="-1.016" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-5.334" y1="1.016" x2="-4.826" y2="1.524" layer="51"/>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="4.826" y1="1.016" x2="5.334" y2="1.524" layer="51"/>
</package>
<package name="MA06-1">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-0.635" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" shape="long" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" shape="long" rot="R90"/>
<text x="-7.62" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.985" y="-2.921" size="1.27" layer="21" ratio="10">1</text>
<text x="5.715" y="1.651" size="1.27" layer="21" ratio="10">6</text>
<text x="-2.54" y="-2.921" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="MA05-2">
<wire x1="3.81" y1="-7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-1.27" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="-3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="2.54" width="0.6096" layer="94"/>
<text x="-3.81" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="-7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="4" x="-7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="6" x="-7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="7" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="9" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="8" x="-7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="10" x="-7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="MA06-1">
<wire x1="3.81" y1="-10.16" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.6096" layer="94"/>
<wire x1="1.27" y1="5.08" x2="2.54" y2="5.08" width="0.6096" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="-1.27" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="-1.27" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<text x="-1.27" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<text x="-1.27" y="8.382" size="1.778" layer="95">&gt;NAME</text>
<pin name="1" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="2" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="3" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="4" x="7.62" y="0" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="5" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
<pin name="6" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MA05-2" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="MA05-2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA05-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MA06-1" prefix="SV" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="MA06-1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MA06-1">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
<connect gate="1" pin="4" pad="4"/>
<connect gate="1" pin="5" pad="5"/>
<connect gate="1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="crystal">
<packages>
<package name="HC49/S">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.159" x2="3.048" y2="-2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="3.048" y2="2.159" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-1.651" x2="3.048" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.651" x2="-3.048" y2="1.651" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.762" x2="0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.762" x2="0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.762" x2="-0.254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.762" x2="-0.254" y2="0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.762" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.762" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.159" x2="-3.048" y2="-2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="-2.159" x2="3.048" y2="2.159" width="0.4064" layer="21" curve="180"/>
<wire x1="-3.048" y1="1.651" x2="-3.048" y2="-1.651" width="0.1524" layer="21" curve="180"/>
<wire x1="3.048" y1="-1.651" x2="3.048" y2="1.651" width="0.1524" layer="21" curve="180"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.937" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.445" y1="-2.54" x2="4.445" y2="2.54" layer="43"/>
<rectangle x1="-5.08" y1="-1.905" x2="-4.445" y2="1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.27" x2="-5.08" y2="1.27" layer="43"/>
<rectangle x1="4.445" y1="-1.905" x2="5.08" y2="1.905" layer="43"/>
<rectangle x1="5.08" y1="-1.27" x2="5.715" y2="1.27" layer="43"/>
</package>
<package name="HC49GW">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-6.35" x2="5.08" y2="-6.35" width="0.8128" layer="21"/>
<wire x1="4.445" y1="6.731" x2="1.016" y2="6.731" width="0.1524" layer="21"/>
<wire x1="1.016" y1="6.731" x2="-1.016" y2="6.731" width="0.1524" layer="51"/>
<wire x1="-1.016" y1="6.731" x2="-4.445" y2="6.731" width="0.1524" layer="21"/>
<wire x1="4.445" y1="6.731" x2="5.08" y2="6.096" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.096" x2="-4.445" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.08" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="2.54" x2="0.3048" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="2.54" x2="0.3048" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.08" x2="-0.3302" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.08" x2="0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.08" x2="-0.9398" y2="3.81" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="3.81" x2="0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-2.032" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="3.81" x2="-0.9398" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-6.604" x2="-2.413" y2="-8.255" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-6.477" x2="2.413" y2="-8.382" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-6.35" x2="5.08" y2="6.096" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.096" x2="-5.08" y2="-6.35" width="0.1524" layer="21"/>
<wire x1="0" y1="8.382" x2="0" y2="6.985" width="0.6096" layer="51"/>
<smd name="1" x="-2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="2" x="2.413" y="-8.001" dx="1.27" dy="2.54" layer="1"/>
<smd name="3" x="0" y="8.001" dx="1.27" dy="2.794" layer="1"/>
<text x="-5.588" y="-5.08" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-5.08" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-8.255" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49TL-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.334" y1="-5.715" x2="-5.461" y2="-5.715" width="0.8128" layer="21"/>
<wire x1="4.445" y1="7.366" x2="1.143" y2="7.366" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="7.366" x2="-4.445" y2="7.366" width="0.1524" layer="21"/>
<wire x1="4.445" y1="7.366" x2="5.08" y2="6.731" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="6.731" x2="-4.445" y2="7.366" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="5.715" x2="-0.3302" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="3.175" x2="0.3048" y2="3.175" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="3.175" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="5.715" x2="0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="5.715" x2="-0.9398" y2="4.445" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="4.445" x2="0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-2.032" y2="4.445" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="4.445" x2="-0.9398" y2="3.175" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-5.842" x2="-2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-5.842" x2="2.413" y2="-7.62" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-5.715" x2="5.08" y2="6.731" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="6.731" x2="-5.08" y2="-5.715" width="0.1524" layer="21"/>
<wire x1="1.143" y1="7.366" x2="-1.143" y2="7.366" width="0.1524" layer="51"/>
<wire x1="0" y1="7.874" x2="0" y2="7.62" width="0.6096" layer="51"/>
<pad name="1" x="-2.413" y="-7.62" drill="0.8128"/>
<pad name="2" x="2.413" y="-7.62" drill="0.8128"/>
<pad name="3" x="0" y="7.874" drill="0.8128"/>
<text x="-5.461" y="-4.445" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-4.699" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-0.3048" y1="7.366" x2="0.3048" y2="7.5692" layer="51"/>
<rectangle x1="-6.35" y1="-6.985" x2="6.35" y2="-4.445" layer="43"/>
<rectangle x1="-5.715" y1="-4.445" x2="5.715" y2="8.255" layer="43"/>
</package>
<package name="HC49U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-1.905" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="-3.175" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="9.271" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<text x="-5.461" y="-1.397" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.54" y="-2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-4.445" x2="6.35" y2="-1.905" layer="43"/>
<rectangle x1="-5.715" y1="-1.905" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-LM">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.08" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="9.906" x2="-4.445" y2="9.906" width="0.1524" layer="21"/>
<wire x1="4.445" y1="9.906" x2="5.08" y2="9.271" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.271" x2="-4.445" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.413" y1="-3.302" x2="-2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.413" y1="-3.302" x2="2.413" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.271" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.413" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.413" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.414" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.572" y="0" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-5.08" x2="5.715" y2="10.795" layer="43"/>
</package>
<package name="HC49U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.921" y1="-2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="2.286" x2="2.921" y2="2.286" width="0.4064" layer="21"/>
<wire x1="-2.921" y1="-1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="-2.921" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.778" x2="2.921" y2="-1.778" width="0.1524" layer="21" curve="-180"/>
<wire x1="2.921" y1="2.286" x2="2.921" y2="-2.286" width="0.4064" layer="21" curve="-180"/>
<wire x1="-2.921" y1="2.286" x2="-2.921" y2="-2.286" width="0.4064" layer="21" curve="180"/>
<wire x1="-2.921" y1="1.778" x2="-2.921" y2="-1.778" width="0.1524" layer="21" curve="180"/>
<wire x1="-0.254" y1="0.889" x2="0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.889" x2="0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.889" x2="-0.254" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.889" x2="-0.254" y2="0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0.889" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.889" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.27" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="2.921" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.81" y1="-2.794" x2="3.81" y2="2.794" layer="43"/>
<rectangle x1="-4.318" y1="-2.54" x2="-3.81" y2="2.54" layer="43"/>
<rectangle x1="-4.826" y1="-2.286" x2="-4.318" y2="2.286" layer="43"/>
<rectangle x1="-5.334" y1="-1.778" x2="-4.826" y2="1.778" layer="43"/>
<rectangle x1="-5.588" y1="-1.27" x2="-5.334" y2="1.016" layer="43"/>
<rectangle x1="3.81" y1="-2.54" x2="4.318" y2="2.54" layer="43"/>
<rectangle x1="4.318" y1="-2.286" x2="4.826" y2="2.286" layer="43"/>
<rectangle x1="4.826" y1="-1.778" x2="5.334" y2="1.778" layer="43"/>
<rectangle x1="5.334" y1="-1.016" x2="5.588" y2="1.016" layer="43"/>
</package>
<package name="HC49U70">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.048" y1="-2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="3.048" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-3.048" y1="-2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.016" x2="-0.3302" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="-1.016" x2="0.3048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="-1.016" x2="0.3048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.3048" y1="1.016" x2="-0.3302" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="1.016" x2="0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="1.016" x2="-0.6858" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="0.6858" y1="0" x2="0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-1.397" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.6858" y1="0" x2="-0.6858" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="-2.54" width="0.4064" layer="21" curve="180"/>
<wire x1="3.048" y1="2.54" x2="3.048" y2="-2.54" width="0.4064" layer="21" curve="-180"/>
<wire x1="-3.048" y1="-2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="3.048" y2="-2.032" width="0.1524" layer="21" curve="-180"/>
<wire x1="3.048" y1="2.032" x2="-3.048" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-2.413" y="0" drill="0.8128"/>
<pad name="2" x="2.413" y="0" drill="0.8128"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.572" y1="-2.794" x2="-4.064" y2="2.794" layer="43"/>
<rectangle x1="-5.08" y1="-2.54" x2="-4.572" y2="2.54" layer="43"/>
<rectangle x1="-5.588" y1="-2.032" x2="-5.08" y2="2.032" layer="43"/>
<rectangle x1="-5.842" y1="-1.27" x2="-5.588" y2="1.27" layer="43"/>
<rectangle x1="-4.064" y1="-3.048" x2="4.064" y2="3.048" layer="43"/>
<rectangle x1="4.064" y1="-2.794" x2="4.572" y2="2.794" layer="43"/>
<rectangle x1="4.572" y1="-2.54" x2="5.08" y2="2.54" layer="43"/>
<rectangle x1="5.08" y1="-2.032" x2="5.588" y2="2.032" layer="43"/>
<rectangle x1="5.588" y1="-1.27" x2="5.842" y2="1.27" layer="43"/>
</package>
<package name="HC13U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="33.401" x2="-8.636" y2="33.401" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="32.766" x2="-8.636" y2="33.401" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="33.401" x2="9.271" y2="32.766" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="15.24" x2="9.017" y2="15.24" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="21.59" x2="-0.3302" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="19.05" x2="0.3048" y2="19.05" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="19.05" x2="0.3048" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="21.59" x2="-0.3302" y2="21.59" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="21.59" x2="0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="21.59" x2="-0.9398" y2="20.32" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="20.32" x2="0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-1.905" y2="20.32" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="20.32" x2="-0.9398" y2="19.05" width="0.3048" layer="21"/>
<wire x1="9.144" y1="15.24" x2="10.16" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="15.24" x2="-9.144" y2="15.24" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="9.271" y1="14.732" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="15.748" x2="9.271" y2="14.732" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="14.732" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="32.766" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.748" x2="-9.271" y2="14.732" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="15.24" drill="0.8128"/>
<pad name="M1" x="10.16" y="15.24" drill="0.8128"/>
<text x="-10.16" y="0" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-5.08" y="-1.27" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-5.08" x2="10.795" y2="34.925" layer="43"/>
</package>
<package name="HC18U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.461" y1="-3.175" x2="5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="5.08" y1="-3.175" x2="-5.08" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="-5.08" y1="-3.175" x2="-5.461" y2="-3.175" width="0.8128" layer="21"/>
<wire x1="4.445" y1="10.16" x2="-4.445" y2="10.16" width="0.1524" layer="21"/>
<wire x1="4.445" y1="10.16" x2="5.08" y2="9.525" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="9.525" x2="-4.445" y2="10.16" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.572" y1="3.81" x2="4.572" y2="3.81" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="8.255" x2="-0.3302" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="5.715" x2="0.3048" y2="5.715" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="5.715" x2="0.3048" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="8.255" x2="-0.3302" y2="8.255" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="8.255" x2="0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="8.255" x2="-0.9398" y2="6.985" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="6.985" x2="0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-2.54" y2="6.985" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="6.985" x2="-0.9398" y2="5.715" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-3.302" x2="-2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="2.54" y1="-3.302" x2="2.54" y2="-5.08" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.048" x2="5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="5.08" y1="4.572" x2="5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="4.572" y1="3.81" x2="5.08" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="3.048" width="0.1524" layer="51"/>
<wire x1="5.08" y1="3.81" x2="5.08" y2="4.572" width="0.1524" layer="51"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="-3.175" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="9.525" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="3.81" x2="-5.715" y2="3.81" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="3.175" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="-5.08" drill="0.8128"/>
<pad name="2" x="2.54" y="-5.08" drill="0.8128"/>
<pad name="M" x="-5.715" y="3.81" drill="0.8128"/>
<pad name="M1" x="5.715" y="3.81" drill="0.8128"/>
<text x="-5.08" y="10.668" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.889" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.35" y1="-5.08" x2="6.35" y2="10.795" layer="43"/>
</package>
<package name="HC18U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="5.08" y1="-1.905" x2="5.08" y2="1.905" width="0.4064" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="4.445" y2="2.54" width="0.4064" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="4.445" y2="-2.54" width="0.4064" layer="21"/>
<wire x1="4.445" y1="-2.54" x2="5.08" y2="-1.905" width="0.4064" layer="21" curve="90"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.4064" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.4064" layer="21" curve="90"/>
<wire x1="-4.318" y1="-1.905" x2="4.318" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.905" x2="4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.445" y1="-1.778" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="4.445" y2="1.778" width="0.1524" layer="21"/>
<wire x1="4.318" y1="1.905" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.318" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="1.778" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-4.318" y1="-1.905" x2="-4.445" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-0.3302" y1="1.27" x2="-0.3302" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="-1.27" x2="0.3048" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="-1.27" x2="0.3048" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="1.27" x2="-0.3302" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="1.27" x2="0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="1.27" x2="-0.9398" y2="0" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="0" x2="1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="0" x2="0.9398" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-1.524" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="0" x2="-0.9398" y2="-1.27" width="0.3048" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="0.8128"/>
<pad name="2" x="2.54" y="0" drill="0.8128"/>
<text x="-5.0546" y="3.2766" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.6228" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.715" y1="-3.175" x2="5.715" y2="3.175" layer="43"/>
</package>
<package name="HC33U-H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-9.906" y1="-3.048" x2="-9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="-9.271" y1="-3.048" x2="9.271" y2="-3.048" width="1.27" layer="21"/>
<wire x1="9.271" y1="-3.048" x2="9.906" y2="-3.048" width="1.27" layer="21"/>
<wire x1="8.636" y1="16.51" x2="-8.636" y2="16.51" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="15.875" x2="-8.636" y2="16.51" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.636" y1="16.51" x2="9.271" y2="15.875" width="0.1524" layer="21" curve="-90"/>
<wire x1="-9.017" y1="7.62" x2="9.017" y2="7.62" width="0.6096" layer="21"/>
<wire x1="-0.3302" y1="13.97" x2="-0.3302" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="11.43" x2="0.3048" y2="11.43" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="11.43" x2="0.3048" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.3048" y1="13.97" x2="-0.3302" y2="13.97" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="13.97" x2="0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="13.97" x2="-0.9398" y2="12.7" width="0.3048" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="0.9398" y1="12.7" x2="0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-1.905" y2="12.7" width="0.1524" layer="21"/>
<wire x1="-0.9398" y1="12.7" x2="-0.9398" y2="11.43" width="0.3048" layer="21"/>
<wire x1="9.144" y1="7.62" x2="10.16" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-10.16" y1="7.62" x2="-9.144" y2="7.62" width="0.6096" layer="51"/>
<wire x1="-6.223" y1="-3.175" x2="-6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="6.223" y1="-3.175" x2="6.223" y2="-5.08" width="0.8128" layer="51"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="9.271" y1="7.112" x2="9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="9.271" y1="8.128" x2="9.271" y2="7.112" width="0.1524" layer="51"/>
<wire x1="-9.271" y1="7.112" x2="-9.271" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="15.875" width="0.1524" layer="21"/>
<wire x1="-9.271" y1="8.128" x2="-9.271" y2="7.112" width="0.1524" layer="51"/>
<pad name="1" x="-6.223" y="-5.08" drill="1.016"/>
<pad name="2" x="6.223" y="-5.08" drill="1.016"/>
<pad name="M" x="-10.16" y="7.62" drill="0.8128"/>
<pad name="M1" x="10.16" y="7.62" drill="0.8128"/>
<text x="-7.62" y="17.272" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.35" y="2.54" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.62" y1="-3.175" x2="-6.985" y2="16.51" layer="21"/>
<rectangle x1="6.985" y1="-3.175" x2="7.62" y2="16.51" layer="21"/>
<rectangle x1="-10.795" y1="-5.715" x2="10.795" y2="17.145" layer="43"/>
</package>
<package name="HC33U-V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.3302" y1="2.54" x2="-0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.3302" y1="0" x2="0.3302" y2="0" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="0" x2="0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.3302" y1="2.54" x2="-0.3302" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="2.54" x2="0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="2.54" x2="-0.9652" y2="1.27" width="0.3048" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.9652" y1="1.27" x2="0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.9652" y1="1.27" x2="-0.9652" y2="0" width="0.3048" layer="21"/>
<wire x1="-5.207" y1="4.064" x2="5.207" y2="4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-4.064" x2="5.207" y2="-4.064" width="0.254" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="3.683" x2="5.207" y2="3.683" width="0.0508" layer="21"/>
<wire x1="-5.207" y1="-3.683" x2="-5.207" y2="3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="3.683" x2="5.207" y2="-3.683" width="0.0508" layer="21" curve="-180"/>
<wire x1="5.207" y1="4.064" x2="5.207" y2="-4.064" width="0.254" layer="21" curve="-180"/>
<wire x1="-5.207" y1="4.064" x2="-5.207" y2="-4.064" width="0.254" layer="21" curve="180"/>
<pad name="1" x="-6.223" y="0" drill="1.016"/>
<pad name="2" x="6.223" y="0" drill="1.016"/>
<text x="-5.08" y="4.826" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.255" y1="-3.81" x2="-6.985" y2="3.81" layer="43"/>
<rectangle x1="-8.89" y1="-3.175" x2="-8.255" y2="3.175" layer="43"/>
<rectangle x1="-9.525" y1="-2.54" x2="-8.89" y2="2.54" layer="43"/>
<rectangle x1="-6.985" y1="-4.445" x2="6.985" y2="4.445" layer="43"/>
<rectangle x1="6.985" y1="-3.81" x2="8.255" y2="3.81" layer="43"/>
<rectangle x1="8.255" y1="-3.175" x2="8.89" y2="3.175" layer="43"/>
<rectangle x1="8.89" y1="-2.54" x2="9.525" y2="2.54" layer="43"/>
</package>
<package name="TC26H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.889" y1="1.651" x2="0.889" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.762" y1="7.747" x2="1.016" y2="7.493" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.016" y1="7.493" x2="-0.762" y2="7.747" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.762" y1="7.747" x2="0.762" y2="7.747" width="0.1524" layer="21"/>
<wire x1="0.889" y1="1.651" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="-0.889" y1="1.651" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.016" y1="2.032" x2="-1.016" y2="7.493" width="0.1524" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.27" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.4064" layer="51"/>
<wire x1="-0.508" y1="4.953" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="-0.508" y2="4.572" width="0.1524" layer="21"/>
<wire x1="0.508" y1="4.572" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.953" x2="0.508" y2="4.953" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="5.334" x2="0" y2="5.334" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="4.191" x2="0" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0" y2="3.683" width="0.1524" layer="21"/>
<wire x1="0" y1="4.191" x2="0.508" y2="4.191" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0" y2="5.842" width="0.1524" layer="21"/>
<wire x1="0" y1="5.334" x2="0.508" y2="5.334" width="0.1524" layer="21"/>
<wire x1="1.016" y1="2.032" x2="0.889" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.889" y1="2.032" x2="-0.889" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.397" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="2.667" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="0.762" x2="1.778" y2="8.382" layer="43"/>
</package>
<package name="TC38H">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-1.397" y1="1.651" x2="1.397" y2="1.651" width="0.1524" layer="21"/>
<wire x1="1.27" y1="9.906" x2="1.524" y2="9.652" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.524" y1="9.652" x2="-1.27" y2="9.906" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.27" y1="9.906" x2="1.27" y2="9.906" width="0.1524" layer="21"/>
<wire x1="1.397" y1="1.651" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="1.524" y1="2.032" x2="1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="-1.397" y1="1.651" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="2.032" x2="-1.524" y2="9.652" width="0.1524" layer="21"/>
<wire x1="1.397" y1="2.032" x2="-1.397" y2="2.032" width="0.1524" layer="21"/>
<wire x1="0.5588" y1="0.7112" x2="0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.508" y1="0.762" x2="0.508" y2="1.143" width="0.4064" layer="21"/>
<wire x1="-0.508" y1="0.762" x2="-0.508" y2="1.016" width="0.4064" layer="21"/>
<wire x1="-0.5588" y1="0.7112" x2="-0.508" y2="0.762" width="0.4064" layer="21"/>
<wire x1="0.635" y1="0.635" x2="1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.635" y1="0.635" x2="-1.27" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.762" y1="5.588" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="-0.762" y2="5.207" width="0.1524" layer="21"/>
<wire x1="0.762" y1="5.207" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.588" x2="0.762" y2="5.588" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="5.969" x2="0" y2="5.969" width="0.1524" layer="21"/>
<wire x1="-0.762" y1="4.826" x2="0" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0" y2="4.318" width="0.1524" layer="21"/>
<wire x1="0" y1="4.826" x2="0.762" y2="4.826" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0" y2="6.477" width="0.1524" layer="21"/>
<wire x1="0" y1="5.969" x2="0.762" y2="5.969" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-1.905" y="2.032" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.175" y="2.032" size="1.27" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
<rectangle x1="0.3048" y1="1.016" x2="0.7112" y2="1.6002" layer="21"/>
<rectangle x1="-0.7112" y1="1.016" x2="-0.3048" y2="1.6002" layer="21"/>
<rectangle x1="-1.778" y1="1.016" x2="1.778" y2="10.414" layer="43"/>
</package>
<package name="86SMX">
<description>&lt;b&gt;CRYSTAL RESONATOR&lt;/b&gt;</description>
<wire x1="-3.81" y1="1.905" x2="2.413" y2="1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="2.286" x2="2.413" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="-1.905" x2="2.413" y2="-1.905" width="0.0508" layer="21"/>
<wire x1="-3.81" y1="-2.286" x2="2.413" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.334" y1="1.905" x2="-5.334" y2="1.016" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-3.81" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="-2.286" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.286" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="-2.286" x2="-3.81" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="-2.54" x2="-4.191" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-6.35" y1="-2.2098" x2="-6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.604" y1="-2.286" x2="-6.35" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.969" y1="-2.54" x2="-5.969" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.016" x2="-5.334" y2="-1.016" width="0.0508" layer="21"/>
<wire x1="-5.334" y1="-1.016" x2="-5.334" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="-5.334" y1="-1.905" x2="-6.35" y2="-2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="-2.54" x2="-4.191" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.54" x2="-4.191" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-3.81" y2="1.905" width="0.0508" layer="51"/>
<wire x1="-6.35" y1="2.286" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.969" y1="2.286" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-4.191" y1="2.286" x2="-3.81" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-6.604" y1="2.286" x2="-6.35" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.35" y1="2.2098" x2="-6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-5.969" y1="2.54" x2="-5.969" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-5.334" y1="1.905" x2="-6.35" y2="2.2098" width="0.0508" layer="51"/>
<wire x1="-4.191" y1="2.54" x2="-4.191" y2="2.286" width="0.1524" layer="51"/>
<wire x1="6.604" y1="2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.223" y2="1.905" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-1.905" x2="6.604" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="6.604" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="-2.54" x2="5.842" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="-2.286" x2="2.794" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="-2.54" x2="5.842" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.905" x2="6.223" y2="-1.905" width="0.0508" layer="51"/>
<wire x1="6.223" y1="1.905" x2="6.604" y2="2.286" width="0.0508" layer="21"/>
<wire x1="6.223" y1="2.286" x2="6.604" y2="2.286" width="0.1524" layer="21"/>
<wire x1="2.794" y1="2.54" x2="5.842" y2="2.54" width="0.1524" layer="51"/>
<wire x1="2.794" y1="2.286" x2="2.794" y2="2.54" width="0.1524" layer="51"/>
<wire x1="5.842" y1="2.54" x2="5.842" y2="2.286" width="0.1524" layer="51"/>
<wire x1="2.413" y1="1.905" x2="6.223" y2="1.905" width="0.0508" layer="51"/>
<wire x1="2.413" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-0.254" y1="1.651" x2="-0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="0.381" x2="0.254" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.381" x2="0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.254" y1="1.651" x2="-0.254" y2="1.651" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.651" x2="0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.016" x2="1.016" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-0.635" y1="1.651" x2="-0.635" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-0.635" y2="0.381" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.016" x2="-1.016" y2="1.016" width="0.0508" layer="21"/>
<smd name="2" x="4.318" y="-2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="3" x="4.318" y="2.286" dx="3.556" dy="2.1844" layer="1"/>
<smd name="1" x="-5.08" y="-2.286" dx="2.286" dy="2.1844" layer="1"/>
<smd name="4" x="-5.08" y="2.286" dx="2.286" dy="2.1844" layer="1"/>
<text x="-3.683" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.683" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM20SS">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-2.032" y1="-1.27" x2="2.032" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="-1.778" x2="2.032" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="2.032" y1="1.27" x2="-2.032" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.778" x2="2.032" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="0.381" x2="-2.921" y2="-0.381" width="0.0508" layer="21"/>
<wire x1="-4.064" y1="-1.778" x2="-3.556" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="-1.552" x2="-4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.921" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="1.27" x2="-2.921" y2="0.381" width="0.0508" layer="51"/>
<wire x1="-2.54" y1="-1.905" x2="-2.54" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-0.381" x2="-2.921" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-2.032" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="-3.556" y1="-1.778" x2="-2.032" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="-1.27" x2="-3.556" y2="-1.552" width="0.0508" layer="51"/>
<wire x1="-4.064" y1="1.778" x2="-3.556" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-3.556" y1="1.552" x2="-4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="1.27" x2="-3.556" y2="1.552" width="0.0508" layer="51"/>
<wire x1="-3.048" y1="1.778" x2="-3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="51"/>
<wire x1="-3.556" y1="1.778" x2="-2.032" y2="1.778" width="0.1524" layer="51"/>
<wire x1="4.064" y1="-1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="4.064" y2="-1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.27" x2="3.81" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="-1.778" x2="4.064" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="3.81" y1="1.27" x2="3.556" y2="1.27" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.27" x2="2.032" y2="1.27" width="0.0508" layer="51"/>
<wire x1="3.048" y1="-1.905" x2="3.048" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.778" x2="2.54" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-1.905" x2="3.048" y2="-1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="-1.27" x2="3.556" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="2.032" y1="-1.778" x2="3.556" y2="-1.778" width="0.1524" layer="51"/>
<wire x1="3.81" y1="1.27" x2="4.064" y2="1.778" width="0.0508" layer="21"/>
<wire x1="3.556" y1="1.778" x2="4.064" y2="1.778" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="1.778" width="0.1524" layer="51"/>
<wire x1="3.048" y1="1.778" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.54" y1="1.905" x2="3.048" y2="1.905" width="0.1524" layer="51"/>
<wire x1="2.032" y1="1.778" x2="3.556" y2="1.778" width="0.1524" layer="51"/>
<wire x1="-1.778" y1="-0.254" x2="-0.508" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="-0.254" x2="-0.508" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-0.508" y1="0.254" x2="-1.778" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.254" x2="-1.778" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.635" x2="-1.143" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-0.508" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.635" x2="-1.143" y2="1.016" width="0.0508" layer="21"/>
<wire x1="-1.778" y1="-0.635" x2="-1.143" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-0.508" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.635" x2="-1.143" y2="-1.016" width="0.0508" layer="21"/>
<circle x="3.048" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="2" x="2.794" y="-1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="3" x="2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<smd name="4" x="-2.794" y="1.524" dx="1.27" dy="1.8796" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MM39SL">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-3.683" y1="-1.651" x2="3.683" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="-3.683" y1="-2.286" x2="3.683" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-3.683" y1="2.286" x2="3.683" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="1.651" x2="-3.683" y2="1.651" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-4.826" y2="0.762" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.286" x2="-3.683" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-3.683" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="-2.055" x2="-6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.715" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="0.762" x2="-4.826" y2="-0.762" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="-0.762" x2="-4.826" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="-4.826" y1="-1.651" x2="-5.715" y2="-2.055" width="0.0508" layer="51"/>
<wire x1="-5.715" y1="2.286" x2="-3.683" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-3.683" y1="1.651" x2="-4.826" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-6.223" y1="2.286" x2="-5.715" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.055" x2="-6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="-4.826" y1="1.651" x2="-5.715" y2="2.055" width="0.0508" layer="51"/>
<wire x1="6.223" y1="-2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="5.842" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.842" y1="-1.651" x2="6.223" y2="-2.286" width="0.0508" layer="21"/>
<wire x1="5.715" y1="-2.286" x2="6.223" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="-2.286" x2="5.715" y2="-2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-1.651" x2="5.842" y2="-1.651" width="0.0508" layer="21"/>
<wire x1="3.683" y1="-1.651" x2="5.715" y2="-1.651" width="0.0508" layer="51"/>
<wire x1="5.842" y1="1.651" x2="6.223" y2="2.286" width="0.0508" layer="21"/>
<wire x1="5.842" y1="1.651" x2="5.715" y2="1.651" width="0.0508" layer="21"/>
<wire x1="5.715" y1="2.286" x2="6.223" y2="2.286" width="0.1524" layer="21"/>
<wire x1="3.683" y1="2.286" x2="5.715" y2="2.286" width="0.1524" layer="51"/>
<wire x1="5.715" y1="1.651" x2="3.683" y2="1.651" width="0.0508" layer="51"/>
<wire x1="-3.81" y1="-0.254" x2="-2.54" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.254" x2="-2.54" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.254" x2="-3.81" y2="0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.254" x2="-3.81" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.81" y1="0.635" x2="-3.175" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="1.016" width="0.1016" layer="21"/>
<wire x1="-3.81" y1="-0.635" x2="-3.175" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.016" width="0.1016" layer="21"/>
<circle x="5.08" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="2" x="4.699" y="-1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="3" x="4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<smd name="4" x="-4.699" y="1.778" dx="1.778" dy="1.778" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="HC49UP">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="2.159" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="5.461" y1="-2.413" x2="-5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="6.477" y1="-0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-2.159" x2="5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="3.9826" y1="-1.143" x2="3.9826" y2="1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="5.1091" y1="-1.143" x2="5.1091" y2="1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.9826" y2="1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-6.477" y1="-0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.1091" y1="-1.143" x2="-5.1091" y2="1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-5.715" y2="1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-2.159" x2="-5.715" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="5.461" y1="2.413" x2="-5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.016" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.016" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.334" dy="1.9304" layer="1"/>
<text x="-5.715" y="2.794" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.191" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-3.048" x2="6.604" y2="3.048" layer="43"/>
</package>
<package name="SM49">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="3.429" y1="-2.032" x2="5.109" y2="-1.1429" width="0.0508" layer="21" curve="55.771157"/>
<wire x1="5.715" y1="-1.143" x2="5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-5.1091" y1="-1.143" x2="-3.429" y2="-2.032" width="0.0508" layer="21" curve="55.772485"/>
<wire x1="-5.715" y1="-1.143" x2="-5.715" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-3.429" y1="1.27" x2="3.429" y2="1.27" width="0.0508" layer="21"/>
<wire x1="-3.429" y1="2.032" x2="3.429" y2="2.032" width="0.0508" layer="21"/>
<wire x1="3.429" y1="-1.27" x2="-3.429" y2="-1.27" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="2.413" x2="5.461" y2="2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="0.381" x2="-6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="-0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.381" x2="-6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.461" y2="2.413" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.715" y1="1.143" x2="-5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="-5.715" y1="2.159" x2="-5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-3.9826" y1="1.143" x2="-3.429" y2="1.27" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="-3.9826" y1="-1.143" x2="-3.429" y2="-1.27" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="-5.1091" y1="1.143" x2="-3.429" y2="2.0321" width="0.0508" layer="21" curve="-55.770993"/>
<wire x1="-3.9826" y1="1.143" x2="-3.9826" y2="-1.143" width="0.0508" layer="51" curve="128.314524"/>
<wire x1="-5.1091" y1="1.143" x2="-5.1091" y2="-1.143" width="0.0508" layer="51" curve="68.456213"/>
<wire x1="3.429" y1="2.032" x2="5.1091" y2="1.143" width="0.0508" layer="21" curve="-55.772485"/>
<wire x1="3.9826" y1="1.143" x2="3.9826" y2="-1.143" width="0.0508" layer="51" curve="-128.314524"/>
<wire x1="3.429" y1="1.27" x2="3.9826" y2="1.143" width="0.0508" layer="21" curve="-25.842828"/>
<wire x1="3.429" y1="-1.27" x2="3.9826" y2="-1.143" width="0.0508" layer="21" curve="25.842828"/>
<wire x1="6.477" y1="0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.1091" y1="1.143" x2="5.1091" y2="-1.143" width="0.0508" layer="51" curve="-68.456213"/>
<wire x1="5.715" y1="1.143" x2="5.715" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="0.381" x2="5.715" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="5.715" y2="-1.143" width="0.1524" layer="51"/>
<wire x1="5.715" y1="2.159" x2="5.715" y2="1.143" width="0.1524" layer="21"/>
<wire x1="5.461" y1="2.413" x2="5.715" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="5.715" y1="0.381" x2="6.477" y2="0.381" width="0.1524" layer="51"/>
<wire x1="5.715" y1="-0.381" x2="6.477" y2="-0.381" width="0.1524" layer="51"/>
<wire x1="3.429" y1="-2.032" x2="-3.429" y2="-2.032" width="0.0508" layer="21"/>
<wire x1="-5.461" y1="-2.413" x2="5.461" y2="-2.413" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.159" x2="-5.461" y2="-2.413" width="0.1524" layer="21" curve="90"/>
<wire x1="5.461" y1="-2.413" x2="5.715" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="-0.254" y1="0.635" x2="-0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.254" y1="-0.635" x2="0.254" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="-0.635" x2="0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0.254" y1="0.635" x2="-0.254" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="0" x2="-1.143" y2="0" width="0.0508" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0.635" y1="0" x2="1.143" y2="0" width="0.0508" layer="21"/>
<smd name="1" x="-4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<smd name="2" x="4.826" y="0" dx="5.08" dy="1.778" layer="1"/>
<text x="-5.715" y="2.667" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.715" y="-4.064" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.604" y1="-2.54" x2="6.604" y2="2.794" layer="43"/>
</package>
<package name="TC26V">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<wire x1="-0.127" y1="-0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="0.127" y2="-0.508" width="0.1524" layer="51"/>
<wire x1="0.127" y1="0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.127" y1="-0.508" x2="-0.127" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="-0.508" x2="-0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="-0.508" x2="0.381" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="0.381" y1="0" x2="0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.762" y2="0" width="0.1524" layer="51"/>
<wire x1="-0.381" y1="0" x2="-0.381" y2="0.508" width="0.1524" layer="51"/>
<wire x1="0" y1="1.016" x2="0.7184" y2="0.7184" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="0.7184" x2="0" y2="1.016" width="0.1524" layer="21" curve="-44.999323"/>
<wire x1="-0.7184" y1="-0.7184" x2="0" y2="-1.016" width="0.1524" layer="21" curve="44.999323"/>
<wire x1="0" y1="-1.016" x2="0.7184" y2="-0.7184" width="0.1524" layer="21" curve="44.999323"/>
<circle x="0" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128"/>
<pad name="2" x="1.27" y="0" drill="0.8128"/>
<text x="-2.032" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.032" y="-2.667" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="CTS406">
<description>&lt;b&gt;Model 406 6.0x3.5mm Low Cost Surface Mount Crystal&lt;/b&gt;&lt;p&gt;
Source: 008-0260-0_E.pdf</description>
<wire x1="-2.475" y1="1.65" x2="-1.05" y2="1.65" width="0.2032" layer="51"/>
<wire x1="-1.05" y1="1.65" x2="1.05" y2="1.65" width="0.2032" layer="21"/>
<wire x1="1.05" y1="1.65" x2="2.475" y2="1.65" width="0.2032" layer="51"/>
<wire x1="2.9" y1="1.225" x2="2.9" y2="0.3" width="0.2032" layer="51"/>
<wire x1="2.9" y1="0.3" x2="2.9" y2="-0.3" width="0.2032" layer="21"/>
<wire x1="2.9" y1="-0.3" x2="2.9" y2="-1.225" width="0.2032" layer="51"/>
<wire x1="2.475" y1="-1.65" x2="1.05" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="1.05" y1="-1.65" x2="-1.05" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.05" y1="-1.65" x2="-2.475" y2="-1.65" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-1.225" x2="-2.9" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="-0.3" x2="-2.9" y2="0.3" width="0.2032" layer="21"/>
<wire x1="-2.9" y1="0.3" x2="-2.9" y2="1.225" width="0.2032" layer="51"/>
<wire x1="-2.9" y1="1.225" x2="-2.475" y2="1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.475" y1="1.65" x2="2.9" y2="1.225" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="2.9" y1="-1.225" x2="2.475" y2="-1.65" width="0.2032" layer="51" curve="89.516721"/>
<wire x1="-2.475" y1="-1.65" x2="-2.9" y2="-1.225" width="0.2032" layer="51" curve="89.516721"/>
<circle x="-2.05" y="-0.2" radius="0.182" width="0" layer="21"/>
<smd name="1" x="-2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="2" x="2.2" y="-1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="3" x="2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<smd name="4" x="-2.2" y="1.2" dx="1.9" dy="1.4" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="Q">
<wire x1="1.016" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.381" y1="1.524" x2="-0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.381" y1="-1.524" x2="0.381" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="-1.524" x2="0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="0.381" y1="1.524" x2="-0.381" y2="1.524" width="0.254" layer="94"/>
<wire x1="1.016" y1="1.778" x2="1.016" y2="-1.778" width="0.254" layer="94"/>
<wire x1="-1.016" y1="1.778" x2="-1.016" y2="-1.778" width="0.254" layer="94"/>
<text x="2.54" y="1.016" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<text x="-2.159" y="-1.143" size="0.8636" layer="93">1</text>
<text x="1.524" y="-1.143" size="0.8636" layer="93">2</text>
<pin name="2" x="2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-2.54" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CRYSTAL" prefix="Q" uservalue="yes">
<description>&lt;b&gt;CRYSTAL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="Q" x="0" y="0"/>
</gates>
<devices>
<device name="HC49S" package="HC49/S">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1667008" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49GW" package="HC49GW">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49TL-H" package="HC49TL-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-H" package="HC49U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666973" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-LM" package="HC49U-LM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666956" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U-V" package="HC49U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="1666969" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49U70" package="HC49U70">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC49UP" package="HC49UP">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC13U-H" package="HC13U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-H" package="HC18U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC18U-V" package="HC18U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-H" package="HC33U-H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="HC33U-V" package="HC33U-V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="SM49" package="SM49">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26H" package="TC26H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC26V" package="TC26V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="TC38H" package="TC38H">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="68SMX" package="86SMX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="6344860" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM20SS" package="MM20SS">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="MM39SL" package="MM39SL">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
<device name="CTS406" package="CTS406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="tlb">
<packages>
<package name="SSOP20BU">
<description>&lt;b&gt;Small Shrink Outline Package&lt;/b&gt;</description>
<wire x1="-3.9" y1="2.925" x2="3.9" y2="2.925" width="0.1524" layer="21"/>
<wire x1="3.9" y1="2.925" x2="3.9" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="3.9" y1="-2.925" x2="-3.9" y2="-2.925" width="0.1524" layer="21"/>
<wire x1="-3.9" y1="-2.925" x2="-3.9" y2="2.925" width="0.1524" layer="21"/>
<wire x1="-3.738" y1="2.763" x2="3.738" y2="2.763" width="0.0508" layer="27"/>
<wire x1="3.738" y1="2.763" x2="3.738" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="3.738" y1="-2.763" x2="-3.738" y2="-2.763" width="0.0508" layer="27"/>
<wire x1="-3.738" y1="-2.763" x2="-3.738" y2="2.763" width="0.0508" layer="27"/>
<circle x="-2.925" y="-1.95" radius="0.4596" width="0.1524" layer="21"/>
<smd name="20" x="-2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="19" x="-2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="18" x="-1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="17" x="-0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="16" x="-0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="15" x="0.325" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="14" x="0.975" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="12" x="2.275" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="13" x="1.625" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="11" x="2.925" y="3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="1" x="-2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="2" x="-2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="3" x="-1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="4" x="-0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="5" x="-0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="6" x="0.325" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="7" x="0.975" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="8" x="1.625" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="9" x="2.275" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<smd name="10" x="2.925" y="-3.656" dx="0.348" dy="1.397" layer="1"/>
<text x="-4.445" y="-2.73" size="1.27" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="-2.73" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.0875" y1="2.9656" x2="-2.7625" y2="3.9" layer="51"/>
<rectangle x1="-3.0875" y1="-3.9" x2="-2.7625" y2="-2.9656" layer="51"/>
<rectangle x1="-2.4375" y1="-3.9" x2="-2.1125" y2="-2.9656" layer="51"/>
<rectangle x1="-1.7875" y1="-3.9" x2="-1.4625" y2="-2.9656" layer="51"/>
<rectangle x1="-2.4375" y1="2.9656" x2="-2.1125" y2="3.9" layer="51"/>
<rectangle x1="-1.7875" y1="2.9656" x2="-1.4625" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="2.9656" x2="-0.8125" y2="3.9" layer="51"/>
<rectangle x1="-0.4875" y1="2.9656" x2="-0.1625" y2="3.9" layer="51"/>
<rectangle x1="0.1625" y1="2.9656" x2="0.4875" y2="3.9" layer="51"/>
<rectangle x1="0.8125" y1="2.9656" x2="1.1375" y2="3.9" layer="51"/>
<rectangle x1="1.4625" y1="2.9656" x2="1.7875" y2="3.9" layer="51"/>
<rectangle x1="2.1125" y1="2.9656" x2="2.4375" y2="3.9" layer="51"/>
<rectangle x1="2.7625" y1="2.9656" x2="3.0875" y2="3.9" layer="51"/>
<rectangle x1="-1.1375" y1="-3.9" x2="-0.8125" y2="-2.9656" layer="51"/>
<rectangle x1="-0.4875" y1="-3.9" x2="-0.1625" y2="-2.9656" layer="51"/>
<rectangle x1="0.1625" y1="-3.9" x2="0.4875" y2="-2.9656" layer="51"/>
<rectangle x1="0.8125" y1="-3.9" x2="1.1375" y2="-2.9656" layer="51"/>
<rectangle x1="1.4625" y1="-3.9" x2="1.7875" y2="-2.9656" layer="51"/>
<rectangle x1="2.1125" y1="-3.9" x2="2.4375" y2="-2.9656" layer="51"/>
<rectangle x1="2.7625" y1="-3.9" x2="3.0875" y2="-2.9656" layer="51"/>
</package>
<package name="QSOP20">
<description>&lt;b&gt;Quarter-Size Small Outline Package&lt;/b&gt;</description>
<wire x1="-4.3" y1="1.9" x2="4.3" y2="1.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="1.9" x2="4.3" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="4.3" y1="-1.9" x2="-4.3" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-4.3" y1="-1.9" x2="-4.3" y2="1.9" width="0.2032" layer="21"/>
<smd name="1" x="-2.858" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="2" x="-2.223" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="3" x="-1.588" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="4" x="-0.953" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="5" x="-0.318" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="6" x="0.318" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="7" x="0.953" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="8" x="1.588" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="9" x="2.223" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="10" x="2.858" y="-2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="11" x="2.858" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="12" x="2.223" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="13" x="1.588" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="14" x="0.953" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="15" x="0.318" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="16" x="-0.318" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="17" x="-0.953" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="18" x="-1.588" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="19" x="-2.223" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<smd name="20" x="-2.858" y="2.794" dx="0.4" dy="1.4" layer="1"/>
<text x="-4.7625" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="6.0325" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-3.0075" y1="-3.1" x2="-2.7075" y2="-1.95" layer="51"/>
<rectangle x1="-2.3725" y1="-3.1" x2="-2.0725" y2="-1.95" layer="51"/>
<rectangle x1="-1.7375" y1="-3.1" x2="-1.4375" y2="-1.95" layer="51"/>
<rectangle x1="-1.1025" y1="-3.1" x2="-0.8025" y2="-1.95" layer="51"/>
<rectangle x1="-0.4675" y1="-3.1" x2="-0.1675" y2="-1.95" layer="51"/>
<rectangle x1="0.1675" y1="-3.1" x2="0.4675" y2="-1.95" layer="51"/>
<rectangle x1="0.8025" y1="-3.1" x2="1.1025" y2="-1.95" layer="51"/>
<rectangle x1="1.4375" y1="-3.1" x2="1.7375" y2="-1.95" layer="51"/>
<rectangle x1="2.0725" y1="-3.1" x2="2.3725" y2="-1.95" layer="51"/>
<rectangle x1="2.7075" y1="-3.1" x2="3.0075" y2="-1.95" layer="51"/>
<rectangle x1="2.7075" y1="1.95" x2="3.0075" y2="3.1" layer="51"/>
<rectangle x1="2.0725" y1="1.95" x2="2.3725" y2="3.1" layer="51"/>
<rectangle x1="1.4375" y1="1.95" x2="1.7375" y2="3.1" layer="51"/>
<rectangle x1="0.8025" y1="1.95" x2="1.1025" y2="3.1" layer="51"/>
<rectangle x1="0.1675" y1="1.95" x2="0.4675" y2="3.1" layer="51"/>
<rectangle x1="-0.4675" y1="1.95" x2="-0.1675" y2="3.1" layer="51"/>
<rectangle x1="-1.1025" y1="1.95" x2="-0.8025" y2="3.1" layer="51"/>
<rectangle x1="-1.7375" y1="1.95" x2="-1.4375" y2="3.1" layer="51"/>
<rectangle x1="-2.3725" y1="1.95" x2="-2.0725" y2="3.1" layer="51"/>
<rectangle x1="-3.0075" y1="1.95" x2="-2.7075" y2="3.1" layer="51"/>
<rectangle x1="-4.2545" y1="-1.905" x2="-2.9845" y2="-0.635" layer="21"/>
</package>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1" y1="0.483" x2="1" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="0.483" x2="1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1" y1="-0.483" x2="-1" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1" y1="-0.483" x2="-1" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<smd name="2" x="0.5" y="0" dx="0.6" dy="0.7" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
chip</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.889" y="0.889" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.889" y="-2.032" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="SMAJ">
<wire x1="-2.3" y1="1.5" x2="1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="2.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="2.3" y1="1.5" x2="2.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="2.3" y1="-1.5" x2="1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="-2.3" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-2.3" y1="-1.5" x2="-2.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="1.5" y2="-1.5" width="0.254" layer="21"/>
<smd name="A" x="-2" y="0" dx="2.5" dy="1.7" layer="1"/>
<smd name="C" x="2" y="0" dx="2.5" dy="1.7" layer="1"/>
</package>
<package name="43045-1212">
<wire x1="-11" y1="3.8" x2="11" y2="3.8" width="0.254" layer="21"/>
<wire x1="11" y1="3.8" x2="11" y2="-3.7" width="0.254" layer="21"/>
<wire x1="11" y1="-3.7" x2="-11" y2="-3.7" width="0.254" layer="21"/>
<wire x1="-11" y1="-3.7" x2="-11" y2="3.8" width="0.254" layer="21"/>
<wire x1="-2" y1="4.8" x2="2" y2="4.8" width="0.254" layer="21"/>
<wire x1="-2" y1="4.8" x2="-2" y2="3.8" width="0.254" layer="21"/>
<wire x1="2" y1="4.8" x2="2" y2="3.8" width="0.254" layer="21"/>
<pad name="1" x="7.5" y="-1.5" drill="1.1"/>
<pad name="2" x="4.5" y="-1.5" drill="1.1"/>
<pad name="3" x="1.5" y="-1.5" drill="1.1"/>
<pad name="4" x="-1.5" y="-1.5" drill="1.1"/>
<pad name="5" x="-4.5" y="-1.5" drill="1.1"/>
<pad name="6" x="-7.5" y="-1.5" drill="1.1"/>
<pad name="7" x="7.5" y="1.5" drill="1.1"/>
<pad name="8" x="4.5" y="1.5" drill="1.1"/>
<pad name="9" x="1.5" y="1.5" drill="1.1"/>
<pad name="10" x="-1.5" y="1.5" drill="1.1"/>
<pad name="11" x="-4.5" y="1.5" drill="1.1"/>
<pad name="12" x="-7.5" y="1.5" drill="1.1"/>
<text x="-10.73" y="-5.35" size="1.27" layer="25">&gt;NAME</text>
<text x="11.5" y="-3" size="1.27" layer="21">1</text>
<hole x="10.5" y="2.5" drill="1"/>
<hole x="-10.5" y="2.5" drill="1"/>
</package>
</packages>
<symbols>
<symbol name="ADS8344">
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<text x="-10.16" y="16.51" size="1.778" layer="95">&gt;NAME</text>
<text x="-10.16" y="-20.32" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CH0" x="-12.7" y="12.7" length="short" direction="in"/>
<pin name="CH1" x="-12.7" y="10.16" length="short" direction="in"/>
<pin name="CH2" x="-12.7" y="7.62" length="short" direction="in"/>
<pin name="CH3" x="-12.7" y="5.08" length="short" direction="in"/>
<pin name="CH4" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="CH5" x="-12.7" y="0" length="short" direction="in"/>
<pin name="CH6" x="-12.7" y="-2.54" length="short" direction="in"/>
<pin name="CH7" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="COM" x="-12.7" y="-15.24" length="short" direction="in"/>
<pin name="!SHDN" x="12.7" y="5.08" length="short" direction="in" rot="R180"/>
<pin name="VREF" x="-12.7" y="-12.7" length="short" direction="in"/>
<pin name="VCC" x="12.7" y="10.16" length="short" direction="pwr" rot="R180"/>
<pin name="GND" x="12.7" y="-12.7" length="short" direction="pwr" rot="R180"/>
<pin name="GND@1" x="12.7" y="-15.24" length="short" direction="pwr" rot="R180"/>
<pin name="DOUT" x="12.7" y="-7.62" length="short" direction="out" rot="R180"/>
<pin name="DI" x="12.7" y="-2.54" length="short" direction="in" rot="R180"/>
<pin name="!CS" x="12.7" y="0" length="short" direction="in" rot="R180"/>
<pin name="DCLK" x="12.7" y="2.54" length="short" direction="in" rot="R180"/>
<pin name="VCC@1" x="12.7" y="12.7" length="short" direction="pwr" rot="R180"/>
<pin name="BUSY" x="12.7" y="-5.08" length="short" direction="out" rot="R180"/>
</symbol>
<symbol name="TVS_BIDIR">
<wire x1="-3.81" y1="-1.905" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="1.905" x2="-1.143" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="-1.905" x2="-0.508" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.143" y1="1.905" x2="-1.778" y2="1.905" width="0.254" layer="94"/>
<wire x1="1.524" y1="-1.905" x2="-1.016" y2="0" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.905" x2="1.524" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-1.016" y1="0" x2="1.524" y2="1.905" width="0.254" layer="94"/>
<text x="-5.4864" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.0104" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="ZENER">
<wire x1="-1.27" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.905" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="-1.905" x2="2.032" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.397" y1="1.905" x2="0.762" y2="1.905" width="0.254" layer="94"/>
<text x="-2.9464" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-4.4704" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="MV">
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.524" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADS8344" prefix="IC">
<description>&lt;b&gt;16-Bit, 8-Channel Serial Output Sampling ADC&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="ADS8344" x="0" y="0"/>
</gates>
<devices>
<device name="U" package="SSOP20BU">
<connects>
<connect gate="G$1" pin="!CS" pad="18"/>
<connect gate="G$1" pin="!SHDN" pad="10"/>
<connect gate="G$1" pin="BUSY" pad="16"/>
<connect gate="G$1" pin="CH0" pad="1"/>
<connect gate="G$1" pin="CH1" pad="2"/>
<connect gate="G$1" pin="CH2" pad="3"/>
<connect gate="G$1" pin="CH3" pad="4"/>
<connect gate="G$1" pin="CH4" pad="5"/>
<connect gate="G$1" pin="CH5" pad="6"/>
<connect gate="G$1" pin="CH6" pad="7"/>
<connect gate="G$1" pin="CH7" pad="8"/>
<connect gate="G$1" pin="COM" pad="9"/>
<connect gate="G$1" pin="DCLK" pad="19"/>
<connect gate="G$1" pin="DI" pad="17"/>
<connect gate="G$1" pin="DOUT" pad="15"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="14"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCC@1" pad="20"/>
<connect gate="G$1" pin="VREF" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="E" package="QSOP20">
<connects>
<connect gate="G$1" pin="!CS" pad="18"/>
<connect gate="G$1" pin="!SHDN" pad="10"/>
<connect gate="G$1" pin="BUSY" pad="16"/>
<connect gate="G$1" pin="CH0" pad="1"/>
<connect gate="G$1" pin="CH1" pad="2"/>
<connect gate="G$1" pin="CH2" pad="3"/>
<connect gate="G$1" pin="CH3" pad="4"/>
<connect gate="G$1" pin="CH4" pad="5"/>
<connect gate="G$1" pin="CH5" pad="6"/>
<connect gate="G$1" pin="CH6" pad="7"/>
<connect gate="G$1" pin="CH7" pad="8"/>
<connect gate="G$1" pin="COM" pad="9"/>
<connect gate="G$1" pin="DCLK" pad="19"/>
<connect gate="G$1" pin="DI" pad="17"/>
<connect gate="G$1" pin="DOUT" pad="15"/>
<connect gate="G$1" pin="GND" pad="13"/>
<connect gate="G$1" pin="GND@1" pad="14"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCC@1" pad="20"/>
<connect gate="G$1" pin="VREF" pad="11"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="B725" prefix="90">
<gates>
<gate name="G$1" symbol="TVS_BIDIR" x="0" y="0"/>
</gates>
<devices>
<device name="90" package="R0402">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="C" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="00" package="R0603">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SMAJ*ADI" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZENER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMAJ">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="43045-1212">
<gates>
<gate name="1" symbol="MV" x="-2.54" y="27.94"/>
<gate name="2" symbol="MV" x="-2.54" y="22.86"/>
<gate name="3" symbol="MV" x="-2.54" y="17.78"/>
<gate name="4" symbol="MV" x="-2.54" y="12.7"/>
<gate name="5" symbol="MV" x="-2.54" y="7.62"/>
<gate name="6" symbol="MV" x="-2.54" y="2.54"/>
<gate name="7" symbol="MV" x="-2.54" y="-2.54"/>
<gate name="8" symbol="MV" x="-2.54" y="-7.62"/>
<gate name="9" symbol="MV" x="-2.54" y="-12.7"/>
<gate name="10" symbol="MV" x="-2.54" y="-17.78"/>
<gate name="11" symbol="MV" x="-2.54" y="-22.86"/>
<gate name="12" symbol="MV" x="-2.54" y="-27.94"/>
</gates>
<devices>
<device name="" package="43045-1212">
<connects>
<connect gate="1" pin="S" pad="1"/>
<connect gate="10" pin="S" pad="10"/>
<connect gate="11" pin="S" pad="11"/>
<connect gate="12" pin="S" pad="12"/>
<connect gate="2" pin="S" pad="2"/>
<connect gate="3" pin="S" pad="3"/>
<connect gate="4" pin="S" pad="4"/>
<connect gate="5" pin="S" pad="5"/>
<connect gate="6" pin="S" pad="6"/>
<connect gate="7" pin="S" pad="7"/>
<connect gate="8" pin="S" pad="8"/>
<connect gate="9" pin="S" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Atmel_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 27/07/2012 14:02:49</description>
<packages>
<package name="QFP50P2200X2200X160-144N">
<smd name="1" x="-10.668" y="8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-10.668" y="8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-10.668" y="7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-10.668" y="7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-10.668" y="6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-10.668" y="6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-10.668" y="5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-10.668" y="5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-10.668" y="4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-10.668" y="4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-10.668" y="3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-10.668" y="3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-10.668" y="2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="14" x="-10.668" y="2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="15" x="-10.668" y="1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="16" x="-10.668" y="1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="17" x="-10.668" y="0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="18" x="-10.668" y="0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="19" x="-10.668" y="-0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="20" x="-10.668" y="-0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="21" x="-10.668" y="-1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="22" x="-10.668" y="-1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="23" x="-10.668" y="-2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="24" x="-10.668" y="-2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="25" x="-10.668" y="-3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="-10.668" y="-3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="27" x="-10.668" y="-4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="28" x="-10.668" y="-4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="29" x="-10.668" y="-5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="30" x="-10.668" y="-5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="31" x="-10.668" y="-6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="32" x="-10.668" y="-6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="33" x="-10.668" y="-7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="-10.668" y="-7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="-10.668" y="-8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="-10.668" y="-8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="-8.7376" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="-8.255" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="-7.747" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="-7.239" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="-6.7564" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="-6.2484" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="-5.7404" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="-5.2578" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="-4.7498" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="-4.2418" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="-3.7592" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="-3.2512" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="-2.7432" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="50" x="-2.2606" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="51" x="-1.7526" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="52" x="-1.2446" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="53" x="-0.762" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="54" x="-0.254" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="55" x="0.254" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="56" x="0.762" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="57" x="1.2446" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="58" x="1.7526" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="59" x="2.2606" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="60" x="2.7432" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="61" x="3.2512" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="62" x="3.7592" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="63" x="4.2418" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="64" x="4.7498" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="65" x="5.2578" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="66" x="5.7404" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="67" x="6.2484" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="68" x="6.7564" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="69" x="7.239" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="70" x="7.747" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="71" x="8.255" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="72" x="8.7376" y="-10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="73" x="10.668" y="-8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="74" x="10.668" y="-8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="75" x="10.668" y="-7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="76" x="10.668" y="-7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="77" x="10.668" y="-6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="78" x="10.668" y="-6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="79" x="10.668" y="-5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="80" x="10.668" y="-5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="81" x="10.668" y="-4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="82" x="10.668" y="-4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="83" x="10.668" y="-3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="84" x="10.668" y="-3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="85" x="10.668" y="-2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="86" x="10.668" y="-2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="87" x="10.668" y="-1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="88" x="10.668" y="-1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="89" x="10.668" y="-0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="90" x="10.668" y="-0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="91" x="10.668" y="0.254" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="92" x="10.668" y="0.762" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="93" x="10.668" y="1.2446" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="94" x="10.668" y="1.7526" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="95" x="10.668" y="2.2606" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="96" x="10.668" y="2.7432" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="97" x="10.668" y="3.2512" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="98" x="10.668" y="3.7592" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="99" x="10.668" y="4.2418" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="100" x="10.668" y="4.7498" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="101" x="10.668" y="5.2578" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="102" x="10.668" y="5.7404" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="103" x="10.668" y="6.2484" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="104" x="10.668" y="6.7564" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="105" x="10.668" y="7.239" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="106" x="10.668" y="7.747" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="107" x="10.668" y="8.255" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="108" x="10.668" y="8.7376" dx="0.2286" dy="1.4732" layer="1" rot="R270"/>
<smd name="109" x="8.7376" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="110" x="8.255" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="111" x="7.747" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="112" x="7.239" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="113" x="6.7564" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="114" x="6.2484" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="115" x="5.7404" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="116" x="5.2578" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="117" x="4.7498" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="118" x="4.2418" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="119" x="3.7592" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="120" x="3.2512" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="121" x="2.7432" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="122" x="2.2606" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="123" x="1.7526" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="124" x="1.2446" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="125" x="0.762" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="126" x="0.254" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="127" x="-0.254" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="128" x="-0.762" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="129" x="-1.2446" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="130" x="-1.7526" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="131" x="-2.2606" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="132" x="-2.7432" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="133" x="-3.2512" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="134" x="-3.7592" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="135" x="-4.2418" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="136" x="-4.7498" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="137" x="-5.2578" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="138" x="-5.7404" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="139" x="-6.2484" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="140" x="-6.7564" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="141" x="-7.239" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="142" x="-7.747" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="143" x="-8.255" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<smd name="144" x="-8.7376" y="10.668" dx="0.2286" dy="1.4732" layer="1" rot="R180"/>
<wire x1="-11.7348" y1="-5.7658" x2="-12.7508" y2="-5.7658" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="-5.2578" x2="12.7508" y2="-5.2578" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="-0.2032" x2="12.7508" y2="-0.2032" width="0.1524" layer="21"/>
<wire x1="11.7348" y1="4.8006" x2="12.7508" y2="4.8006" width="0.1524" layer="21"/>
<wire x1="8.255" y1="11.7348" x2="8.255" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="3.2512" y1="11.7348" x2="3.2512" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="11.7602" x2="-1.778" y2="12.7762" width="0.1524" layer="21"/>
<wire x1="-6.7564" y1="11.7348" x2="-6.7564" y2="12.7508" width="0.1524" layer="21"/>
<wire x1="-11.7348" y1="4.2672" x2="-12.7508" y2="4.2672" width="0.1524" layer="21"/>
<wire x1="-11.7094" y1="-0.762" x2="-12.7254" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="7.7724" y1="-11.7348" x2="7.7724" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="2.7686" y1="-11.7348" x2="2.7686" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-2.2352" y1="-11.7348" x2="-2.2352" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-7.2898" y1="-11.7348" x2="-7.2898" y2="-12.7508" width="0.1524" layer="21"/>
<wire x1="-9.1948" y1="10.0584" x2="-10.0584" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="9.1948" x2="10.0584" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="9.1948" y1="-10.0584" x2="10.0584" y2="-10.0584" width="0.1524" layer="21"/>
<wire x1="-9.7282" y1="9.1186" x2="-9.1186" y2="9.7282" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="-10.0584" x2="-9.1948" y2="-10.0584" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="-10.0584" x2="10.0584" y2="-9.1948" width="0.1524" layer="21"/>
<wire x1="10.0584" y1="10.0584" x2="9.1948" y2="10.0584" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="10.0584" x2="-10.0584" y2="9.1948" width="0.1524" layer="21"/>
<wire x1="-10.0584" y1="-9.1948" x2="-10.0584" y2="-10.0584" width="0.1524" layer="21"/>
<text x="-12.6238" y="8.7376" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="8.636" y1="10.0584" x2="8.8646" y2="10.0584" width="0" layer="51"/>
<wire x1="8.8646" y1="10.0584" x2="8.8646" y2="11.049" width="0" layer="51"/>
<wire x1="8.8646" y1="11.049" x2="8.636" y2="11.049" width="0" layer="51"/>
<wire x1="8.636" y1="11.049" x2="8.636" y2="10.0584" width="0" layer="51"/>
<wire x1="8.128" y1="10.0584" x2="8.3566" y2="10.0584" width="0" layer="51"/>
<wire x1="8.3566" y1="10.0584" x2="8.3566" y2="11.049" width="0" layer="51"/>
<wire x1="8.3566" y1="11.049" x2="8.128" y2="11.049" width="0" layer="51"/>
<wire x1="8.128" y1="11.049" x2="8.128" y2="10.0584" width="0" layer="51"/>
<wire x1="7.6454" y1="10.0584" x2="7.8486" y2="10.0584" width="0" layer="51"/>
<wire x1="7.8486" y1="10.0584" x2="7.8486" y2="11.049" width="0" layer="51"/>
<wire x1="7.8486" y1="11.049" x2="7.6454" y2="11.049" width="0" layer="51"/>
<wire x1="7.6454" y1="11.049" x2="7.6454" y2="10.0584" width="0" layer="51"/>
<wire x1="7.1374" y1="10.0584" x2="7.366" y2="10.0584" width="0" layer="51"/>
<wire x1="7.366" y1="10.0584" x2="7.366" y2="11.049" width="0" layer="51"/>
<wire x1="7.366" y1="11.049" x2="7.1374" y2="11.049" width="0" layer="51"/>
<wire x1="7.1374" y1="11.049" x2="7.1374" y2="10.0584" width="0" layer="51"/>
<wire x1="6.6294" y1="10.0584" x2="6.858" y2="10.0584" width="0" layer="51"/>
<wire x1="6.858" y1="10.0584" x2="6.858" y2="11.049" width="0" layer="51"/>
<wire x1="6.858" y1="11.049" x2="6.6294" y2="11.049" width="0" layer="51"/>
<wire x1="6.6294" y1="11.049" x2="6.6294" y2="10.0584" width="0" layer="51"/>
<wire x1="6.1468" y1="10.0584" x2="6.35" y2="10.0584" width="0" layer="51"/>
<wire x1="6.35" y1="10.0584" x2="6.35" y2="11.049" width="0" layer="51"/>
<wire x1="6.35" y1="11.049" x2="6.1468" y2="11.049" width="0" layer="51"/>
<wire x1="6.1468" y1="11.049" x2="6.1468" y2="10.0584" width="0" layer="51"/>
<wire x1="5.6388" y1="10.0584" x2="5.8674" y2="10.0584" width="0" layer="51"/>
<wire x1="5.8674" y1="10.0584" x2="5.8674" y2="11.049" width="0" layer="51"/>
<wire x1="5.8674" y1="11.049" x2="5.6388" y2="11.049" width="0" layer="51"/>
<wire x1="5.6388" y1="11.049" x2="5.6388" y2="10.0584" width="0" layer="51"/>
<wire x1="5.1308" y1="10.0584" x2="5.3594" y2="10.0584" width="0" layer="51"/>
<wire x1="5.3594" y1="10.0584" x2="5.3594" y2="11.049" width="0" layer="51"/>
<wire x1="5.3594" y1="11.049" x2="5.1308" y2="11.049" width="0" layer="51"/>
<wire x1="5.1308" y1="11.049" x2="5.1308" y2="10.0584" width="0" layer="51"/>
<wire x1="4.6482" y1="10.0584" x2="4.8514" y2="10.0584" width="0" layer="51"/>
<wire x1="4.8514" y1="10.0584" x2="4.8514" y2="11.049" width="0" layer="51"/>
<wire x1="4.8514" y1="11.049" x2="4.6482" y2="11.049" width="0" layer="51"/>
<wire x1="4.6482" y1="11.049" x2="4.6482" y2="10.0584" width="0" layer="51"/>
<wire x1="4.1402" y1="10.0584" x2="4.3688" y2="10.0584" width="0" layer="51"/>
<wire x1="4.3688" y1="10.0584" x2="4.3688" y2="11.049" width="0" layer="51"/>
<wire x1="4.3688" y1="11.049" x2="4.1402" y2="11.049" width="0" layer="51"/>
<wire x1="4.1402" y1="11.049" x2="4.1402" y2="10.0584" width="0" layer="51"/>
<wire x1="3.6322" y1="10.0584" x2="3.8608" y2="10.0584" width="0" layer="51"/>
<wire x1="3.8608" y1="10.0584" x2="3.8608" y2="11.049" width="0" layer="51"/>
<wire x1="3.8608" y1="11.049" x2="3.6322" y2="11.049" width="0" layer="51"/>
<wire x1="3.6322" y1="11.049" x2="3.6322" y2="10.0584" width="0" layer="51"/>
<wire x1="3.1496" y1="10.0584" x2="3.3528" y2="10.0584" width="0" layer="51"/>
<wire x1="3.3528" y1="10.0584" x2="3.3528" y2="11.049" width="0" layer="51"/>
<wire x1="3.3528" y1="11.049" x2="3.1496" y2="11.049" width="0" layer="51"/>
<wire x1="3.1496" y1="11.049" x2="3.1496" y2="10.0584" width="0" layer="51"/>
<wire x1="2.6416" y1="10.0584" x2="2.8702" y2="10.0584" width="0" layer="51"/>
<wire x1="2.8702" y1="10.0584" x2="2.8702" y2="11.049" width="0" layer="51"/>
<wire x1="2.8702" y1="11.049" x2="2.6416" y2="11.049" width="0" layer="51"/>
<wire x1="2.6416" y1="11.049" x2="2.6416" y2="10.0584" width="0" layer="51"/>
<wire x1="2.1336" y1="10.0584" x2="2.3622" y2="10.0584" width="0" layer="51"/>
<wire x1="2.3622" y1="10.0584" x2="2.3622" y2="11.049" width="0" layer="51"/>
<wire x1="2.3622" y1="11.049" x2="2.1336" y2="11.049" width="0" layer="51"/>
<wire x1="2.1336" y1="11.049" x2="2.1336" y2="10.0584" width="0" layer="51"/>
<wire x1="1.651" y1="10.0584" x2="1.8542" y2="10.0584" width="0" layer="51"/>
<wire x1="1.8542" y1="10.0584" x2="1.8542" y2="11.049" width="0" layer="51"/>
<wire x1="1.8542" y1="11.049" x2="1.651" y2="11.049" width="0" layer="51"/>
<wire x1="1.651" y1="11.049" x2="1.651" y2="10.0584" width="0" layer="51"/>
<wire x1="1.143" y1="10.0584" x2="1.3716" y2="10.0584" width="0" layer="51"/>
<wire x1="1.3716" y1="10.0584" x2="1.3716" y2="11.049" width="0" layer="51"/>
<wire x1="1.3716" y1="11.049" x2="1.143" y2="11.049" width="0" layer="51"/>
<wire x1="1.143" y1="11.049" x2="1.143" y2="10.0584" width="0" layer="51"/>
<wire x1="0.635" y1="10.0584" x2="0.8636" y2="10.0584" width="0" layer="51"/>
<wire x1="0.8636" y1="10.0584" x2="0.8636" y2="11.049" width="0" layer="51"/>
<wire x1="0.8636" y1="11.049" x2="0.635" y2="11.049" width="0" layer="51"/>
<wire x1="0.635" y1="11.049" x2="0.635" y2="10.0584" width="0" layer="51"/>
<wire x1="0.1524" y1="10.0584" x2="0.3556" y2="10.0584" width="0" layer="51"/>
<wire x1="0.3556" y1="10.0584" x2="0.3556" y2="11.049" width="0" layer="51"/>
<wire x1="0.3556" y1="11.049" x2="0.1524" y2="11.049" width="0" layer="51"/>
<wire x1="0.1524" y1="11.049" x2="0.1524" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.3556" y1="10.0584" x2="-0.1524" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.1524" y1="10.0584" x2="-0.1524" y2="11.049" width="0" layer="51"/>
<wire x1="-0.1524" y1="11.049" x2="-0.3556" y2="11.049" width="0" layer="51"/>
<wire x1="-0.3556" y1="11.049" x2="-0.3556" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.8636" y1="10.0584" x2="-0.635" y2="10.0584" width="0" layer="51"/>
<wire x1="-0.635" y1="10.0584" x2="-0.635" y2="11.049" width="0" layer="51"/>
<wire x1="-0.635" y1="11.049" x2="-0.8636" y2="11.049" width="0" layer="51"/>
<wire x1="-0.8636" y1="11.049" x2="-0.8636" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.3716" y1="10.0584" x2="-1.143" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.143" y1="10.0584" x2="-1.143" y2="11.049" width="0" layer="51"/>
<wire x1="-1.143" y1="11.049" x2="-1.3716" y2="11.049" width="0" layer="51"/>
<wire x1="-1.3716" y1="11.049" x2="-1.3716" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.8542" y1="10.0584" x2="-1.651" y2="10.0584" width="0" layer="51"/>
<wire x1="-1.651" y1="10.0584" x2="-1.651" y2="11.049" width="0" layer="51"/>
<wire x1="-1.651" y1="11.049" x2="-1.8542" y2="11.049" width="0" layer="51"/>
<wire x1="-1.8542" y1="11.049" x2="-1.8542" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.3622" y1="10.0584" x2="-2.1336" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.1336" y1="10.0584" x2="-2.1336" y2="11.049" width="0" layer="51"/>
<wire x1="-2.1336" y1="11.049" x2="-2.3622" y2="11.049" width="0" layer="51"/>
<wire x1="-2.3622" y1="11.049" x2="-2.3622" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.8702" y1="10.0584" x2="-2.6416" y2="10.0584" width="0" layer="51"/>
<wire x1="-2.6416" y1="10.0584" x2="-2.6416" y2="11.049" width="0" layer="51"/>
<wire x1="-2.6416" y1="11.049" x2="-2.8702" y2="11.049" width="0" layer="51"/>
<wire x1="-2.8702" y1="11.049" x2="-2.8702" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.3528" y1="10.0584" x2="-3.1496" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.1496" y1="10.0584" x2="-3.1496" y2="11.049" width="0" layer="51"/>
<wire x1="-3.1496" y1="11.049" x2="-3.3528" y2="11.049" width="0" layer="51"/>
<wire x1="-3.3528" y1="11.049" x2="-3.3528" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.8608" y1="10.0584" x2="-3.6322" y2="10.0584" width="0" layer="51"/>
<wire x1="-3.6322" y1="10.0584" x2="-3.6322" y2="11.049" width="0" layer="51"/>
<wire x1="-3.6322" y1="11.049" x2="-3.8608" y2="11.049" width="0" layer="51"/>
<wire x1="-3.8608" y1="11.049" x2="-3.8608" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.3688" y1="10.0584" x2="-4.1402" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.1402" y1="10.0584" x2="-4.1402" y2="11.049" width="0" layer="51"/>
<wire x1="-4.1402" y1="11.049" x2="-4.3688" y2="11.049" width="0" layer="51"/>
<wire x1="-4.3688" y1="11.049" x2="-4.3688" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.8514" y1="10.0584" x2="-4.6482" y2="10.0584" width="0" layer="51"/>
<wire x1="-4.6482" y1="10.0584" x2="-4.6482" y2="11.049" width="0" layer="51"/>
<wire x1="-4.6482" y1="11.049" x2="-4.8514" y2="11.049" width="0" layer="51"/>
<wire x1="-4.8514" y1="11.049" x2="-4.8514" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.3594" y1="10.0584" x2="-5.1308" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.1308" y1="10.0584" x2="-5.1308" y2="11.049" width="0" layer="51"/>
<wire x1="-5.1308" y1="11.049" x2="-5.3594" y2="11.049" width="0" layer="51"/>
<wire x1="-5.3594" y1="11.049" x2="-5.3594" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.8674" y1="10.0584" x2="-5.6388" y2="10.0584" width="0" layer="51"/>
<wire x1="-5.6388" y1="10.0584" x2="-5.6388" y2="11.049" width="0" layer="51"/>
<wire x1="-5.6388" y1="11.049" x2="-5.8674" y2="11.049" width="0" layer="51"/>
<wire x1="-5.8674" y1="11.049" x2="-5.8674" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.35" y1="10.0584" x2="-6.1468" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.1468" y1="10.0584" x2="-6.1468" y2="11.049" width="0" layer="51"/>
<wire x1="-6.1468" y1="11.049" x2="-6.35" y2="11.049" width="0" layer="51"/>
<wire x1="-6.35" y1="11.049" x2="-6.35" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.858" y1="10.0584" x2="-6.6294" y2="10.0584" width="0" layer="51"/>
<wire x1="-6.6294" y1="10.0584" x2="-6.6294" y2="11.049" width="0" layer="51"/>
<wire x1="-6.6294" y1="11.049" x2="-6.858" y2="11.049" width="0" layer="51"/>
<wire x1="-6.858" y1="11.049" x2="-6.858" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.366" y1="10.0584" x2="-7.1374" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.1374" y1="10.0584" x2="-7.1374" y2="11.049" width="0" layer="51"/>
<wire x1="-7.1374" y1="11.049" x2="-7.366" y2="11.049" width="0" layer="51"/>
<wire x1="-7.366" y1="11.049" x2="-7.366" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.8486" y1="10.0584" x2="-7.6454" y2="10.0584" width="0" layer="51"/>
<wire x1="-7.6454" y1="10.0584" x2="-7.6454" y2="11.049" width="0" layer="51"/>
<wire x1="-7.6454" y1="11.049" x2="-7.8486" y2="11.049" width="0" layer="51"/>
<wire x1="-7.8486" y1="11.049" x2="-7.8486" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.3566" y1="10.0584" x2="-8.128" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.128" y1="10.0584" x2="-8.128" y2="11.049" width="0" layer="51"/>
<wire x1="-8.128" y1="11.049" x2="-8.3566" y2="11.049" width="0" layer="51"/>
<wire x1="-8.3566" y1="11.049" x2="-8.3566" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.8646" y1="10.0584" x2="-8.7884" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.7884" y1="10.0584" x2="-8.636" y2="10.0584" width="0" layer="51"/>
<wire x1="-8.636" y1="10.0584" x2="-8.636" y2="11.049" width="0" layer="51"/>
<wire x1="-8.636" y1="11.049" x2="-8.8646" y2="11.049" width="0" layer="51"/>
<wire x1="-8.8646" y1="11.049" x2="-8.8646" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.636" x2="-10.0584" y2="8.7884" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.7884" x2="-10.0584" y2="8.8646" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.8646" x2="-11.049" y2="8.8646" width="0" layer="51"/>
<wire x1="-11.049" y1="8.8646" x2="-11.049" y2="8.636" width="0" layer="51"/>
<wire x1="-11.049" y1="8.636" x2="-10.0584" y2="8.636" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.128" x2="-10.0584" y2="8.3566" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.3566" x2="-11.049" y2="8.3566" width="0" layer="51"/>
<wire x1="-11.049" y1="8.3566" x2="-11.049" y2="8.128" width="0" layer="51"/>
<wire x1="-11.049" y1="8.128" x2="-10.0584" y2="8.128" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.6454" x2="-10.0584" y2="7.8486" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.8486" x2="-11.049" y2="7.8486" width="0" layer="51"/>
<wire x1="-11.049" y1="7.8486" x2="-11.049" y2="7.6454" width="0" layer="51"/>
<wire x1="-11.049" y1="7.6454" x2="-10.0584" y2="7.6454" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.1374" x2="-10.0584" y2="7.366" width="0" layer="51"/>
<wire x1="-10.0584" y1="7.366" x2="-11.049" y2="7.366" width="0" layer="51"/>
<wire x1="-11.049" y1="7.366" x2="-11.049" y2="7.1374" width="0" layer="51"/>
<wire x1="-11.049" y1="7.1374" x2="-10.0584" y2="7.1374" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.6294" x2="-10.0584" y2="6.858" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.858" x2="-11.049" y2="6.858" width="0" layer="51"/>
<wire x1="-11.049" y1="6.858" x2="-11.049" y2="6.6294" width="0" layer="51"/>
<wire x1="-11.049" y1="6.6294" x2="-10.0584" y2="6.6294" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.1468" x2="-10.0584" y2="6.35" width="0" layer="51"/>
<wire x1="-10.0584" y1="6.35" x2="-11.049" y2="6.35" width="0" layer="51"/>
<wire x1="-11.049" y1="6.35" x2="-11.049" y2="6.1468" width="0" layer="51"/>
<wire x1="-11.049" y1="6.1468" x2="-10.0584" y2="6.1468" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.6388" x2="-10.0584" y2="5.8674" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.8674" x2="-11.049" y2="5.8674" width="0" layer="51"/>
<wire x1="-11.049" y1="5.8674" x2="-11.049" y2="5.6388" width="0" layer="51"/>
<wire x1="-11.049" y1="5.6388" x2="-10.0584" y2="5.6388" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.1308" x2="-10.0584" y2="5.3594" width="0" layer="51"/>
<wire x1="-10.0584" y1="5.3594" x2="-11.049" y2="5.3594" width="0" layer="51"/>
<wire x1="-11.049" y1="5.3594" x2="-11.049" y2="5.1308" width="0" layer="51"/>
<wire x1="-11.049" y1="5.1308" x2="-10.0584" y2="5.1308" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.6482" x2="-10.0584" y2="4.8514" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.8514" x2="-11.049" y2="4.8514" width="0" layer="51"/>
<wire x1="-11.049" y1="4.8514" x2="-11.049" y2="4.6482" width="0" layer="51"/>
<wire x1="-11.049" y1="4.6482" x2="-10.0584" y2="4.6482" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.1402" x2="-10.0584" y2="4.3688" width="0" layer="51"/>
<wire x1="-10.0584" y1="4.3688" x2="-11.049" y2="4.3688" width="0" layer="51"/>
<wire x1="-11.049" y1="4.3688" x2="-11.049" y2="4.1402" width="0" layer="51"/>
<wire x1="-11.049" y1="4.1402" x2="-10.0584" y2="4.1402" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.6322" x2="-10.0584" y2="3.8608" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.8608" x2="-11.049" y2="3.8608" width="0" layer="51"/>
<wire x1="-11.049" y1="3.8608" x2="-11.049" y2="3.6322" width="0" layer="51"/>
<wire x1="-11.049" y1="3.6322" x2="-10.0584" y2="3.6322" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.1496" x2="-10.0584" y2="3.3528" width="0" layer="51"/>
<wire x1="-10.0584" y1="3.3528" x2="-11.049" y2="3.3528" width="0" layer="51"/>
<wire x1="-11.049" y1="3.3528" x2="-11.049" y2="3.1496" width="0" layer="51"/>
<wire x1="-11.049" y1="3.1496" x2="-10.0584" y2="3.1496" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.6416" x2="-10.0584" y2="2.8702" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.8702" x2="-11.049" y2="2.8702" width="0" layer="51"/>
<wire x1="-11.049" y1="2.8702" x2="-11.049" y2="2.6416" width="0" layer="51"/>
<wire x1="-11.049" y1="2.6416" x2="-10.0584" y2="2.6416" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.1336" x2="-10.0584" y2="2.3622" width="0" layer="51"/>
<wire x1="-10.0584" y1="2.3622" x2="-11.049" y2="2.3622" width="0" layer="51"/>
<wire x1="-11.049" y1="2.3622" x2="-11.049" y2="2.1336" width="0" layer="51"/>
<wire x1="-11.049" y1="2.1336" x2="-10.0584" y2="2.1336" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.651" x2="-10.0584" y2="1.8542" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.8542" x2="-11.049" y2="1.8542" width="0" layer="51"/>
<wire x1="-11.049" y1="1.8542" x2="-11.049" y2="1.651" width="0" layer="51"/>
<wire x1="-11.049" y1="1.651" x2="-10.0584" y2="1.651" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.143" x2="-10.0584" y2="1.3716" width="0" layer="51"/>
<wire x1="-10.0584" y1="1.3716" x2="-11.049" y2="1.3716" width="0" layer="51"/>
<wire x1="-11.049" y1="1.3716" x2="-11.049" y2="1.143" width="0" layer="51"/>
<wire x1="-11.049" y1="1.143" x2="-10.0584" y2="1.143" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.635" x2="-10.0584" y2="0.8636" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.8636" x2="-11.049" y2="0.8636" width="0" layer="51"/>
<wire x1="-11.049" y1="0.8636" x2="-11.049" y2="0.635" width="0" layer="51"/>
<wire x1="-11.049" y1="0.635" x2="-10.0584" y2="0.635" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.1524" x2="-10.0584" y2="0.3556" width="0" layer="51"/>
<wire x1="-10.0584" y1="0.3556" x2="-11.049" y2="0.3556" width="0" layer="51"/>
<wire x1="-11.049" y1="0.3556" x2="-11.049" y2="0.1524" width="0" layer="51"/>
<wire x1="-11.049" y1="0.1524" x2="-10.0584" y2="0.1524" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.3556" x2="-10.0584" y2="-0.1524" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.1524" x2="-11.049" y2="-0.1524" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.1524" x2="-11.049" y2="-0.3556" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.3556" x2="-10.0584" y2="-0.3556" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.8636" x2="-10.0584" y2="-0.635" width="0" layer="51"/>
<wire x1="-10.0584" y1="-0.635" x2="-11.049" y2="-0.635" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.635" x2="-11.049" y2="-0.8636" width="0" layer="51"/>
<wire x1="-11.049" y1="-0.8636" x2="-10.0584" y2="-0.8636" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.3716" x2="-10.0584" y2="-1.143" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.143" x2="-11.049" y2="-1.143" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.143" x2="-11.049" y2="-1.3716" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.3716" x2="-10.0584" y2="-1.3716" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.8542" x2="-10.0584" y2="-1.651" width="0" layer="51"/>
<wire x1="-10.0584" y1="-1.651" x2="-11.049" y2="-1.651" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.651" x2="-11.049" y2="-1.8542" width="0" layer="51"/>
<wire x1="-11.049" y1="-1.8542" x2="-10.0584" y2="-1.8542" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.3622" x2="-10.0584" y2="-2.1336" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.1336" x2="-11.049" y2="-2.1336" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.1336" x2="-11.049" y2="-2.3622" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.3622" x2="-10.0584" y2="-2.3622" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.8702" x2="-10.0584" y2="-2.6416" width="0" layer="51"/>
<wire x1="-10.0584" y1="-2.6416" x2="-11.049" y2="-2.6416" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.6416" x2="-11.049" y2="-2.8702" width="0" layer="51"/>
<wire x1="-11.049" y1="-2.8702" x2="-10.0584" y2="-2.8702" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.3528" x2="-10.0584" y2="-3.1496" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.1496" x2="-11.049" y2="-3.1496" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.1496" x2="-11.049" y2="-3.3528" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.3528" x2="-10.0584" y2="-3.3528" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.8608" x2="-10.0584" y2="-3.6322" width="0" layer="51"/>
<wire x1="-10.0584" y1="-3.6322" x2="-11.049" y2="-3.6322" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.6322" x2="-11.049" y2="-3.8608" width="0" layer="51"/>
<wire x1="-11.049" y1="-3.8608" x2="-10.0584" y2="-3.8608" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.3688" x2="-10.0584" y2="-4.1402" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.1402" x2="-11.049" y2="-4.1402" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.1402" x2="-11.049" y2="-4.3688" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.3688" x2="-10.0584" y2="-4.3688" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.8514" x2="-10.0584" y2="-4.6482" width="0" layer="51"/>
<wire x1="-10.0584" y1="-4.6482" x2="-11.049" y2="-4.6482" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.6482" x2="-11.049" y2="-4.8514" width="0" layer="51"/>
<wire x1="-11.049" y1="-4.8514" x2="-10.0584" y2="-4.8514" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.3594" x2="-10.0584" y2="-5.1308" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.1308" x2="-11.049" y2="-5.1308" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.1308" x2="-11.049" y2="-5.3594" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.3594" x2="-10.0584" y2="-5.3594" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.8674" x2="-10.0584" y2="-5.6388" width="0" layer="51"/>
<wire x1="-10.0584" y1="-5.6388" x2="-11.049" y2="-5.6388" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.6388" x2="-11.049" y2="-5.8674" width="0" layer="51"/>
<wire x1="-11.049" y1="-5.8674" x2="-10.0584" y2="-5.8674" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.35" x2="-10.0584" y2="-6.1468" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.1468" x2="-11.049" y2="-6.1468" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.1468" x2="-11.049" y2="-6.35" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.35" x2="-10.0584" y2="-6.35" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.858" x2="-10.0584" y2="-6.6294" width="0" layer="51"/>
<wire x1="-10.0584" y1="-6.6294" x2="-11.049" y2="-6.6294" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.6294" x2="-11.049" y2="-6.858" width="0" layer="51"/>
<wire x1="-11.049" y1="-6.858" x2="-10.0584" y2="-6.858" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.366" x2="-10.0584" y2="-7.1374" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.1374" x2="-11.049" y2="-7.1374" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.1374" x2="-11.049" y2="-7.366" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.366" x2="-10.0584" y2="-7.366" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.8486" x2="-10.0584" y2="-7.6454" width="0" layer="51"/>
<wire x1="-10.0584" y1="-7.6454" x2="-11.049" y2="-7.6454" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.6454" x2="-11.049" y2="-7.8486" width="0" layer="51"/>
<wire x1="-11.049" y1="-7.8486" x2="-10.0584" y2="-7.8486" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.3566" x2="-10.0584" y2="-8.128" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.128" x2="-11.049" y2="-8.128" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.128" x2="-11.049" y2="-8.3566" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.3566" x2="-10.0584" y2="-8.3566" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.8646" x2="-10.0584" y2="-8.636" width="0" layer="51"/>
<wire x1="-10.0584" y1="-8.636" x2="-11.049" y2="-8.636" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.636" x2="-11.049" y2="-8.8646" width="0" layer="51"/>
<wire x1="-11.049" y1="-8.8646" x2="-10.0584" y2="-8.8646" width="0" layer="51"/>
<wire x1="-8.636" y1="-10.0584" x2="-8.8646" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.8646" y1="-10.0584" x2="-8.8646" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.8646" y1="-11.049" x2="-8.636" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.636" y1="-11.049" x2="-8.636" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.128" y1="-10.0584" x2="-8.3566" y2="-10.0584" width="0" layer="51"/>
<wire x1="-8.3566" y1="-10.0584" x2="-8.3566" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.3566" y1="-11.049" x2="-8.128" y2="-11.049" width="0" layer="51"/>
<wire x1="-8.128" y1="-11.049" x2="-8.128" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.6454" y1="-10.0584" x2="-7.8486" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.8486" y1="-10.0584" x2="-7.8486" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.8486" y1="-11.049" x2="-7.6454" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.6454" y1="-11.049" x2="-7.6454" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.1374" y1="-10.0584" x2="-7.366" y2="-10.0584" width="0" layer="51"/>
<wire x1="-7.366" y1="-10.0584" x2="-7.366" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.366" y1="-11.049" x2="-7.1374" y2="-11.049" width="0" layer="51"/>
<wire x1="-7.1374" y1="-11.049" x2="-7.1374" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.6294" y1="-10.0584" x2="-6.858" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.858" y1="-10.0584" x2="-6.858" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.858" y1="-11.049" x2="-6.6294" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.6294" y1="-11.049" x2="-6.6294" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.1468" y1="-10.0584" x2="-6.35" y2="-10.0584" width="0" layer="51"/>
<wire x1="-6.35" y1="-10.0584" x2="-6.35" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.35" y1="-11.049" x2="-6.1468" y2="-11.049" width="0" layer="51"/>
<wire x1="-6.1468" y1="-11.049" x2="-6.1468" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.6388" y1="-10.0584" x2="-5.8674" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.8674" y1="-10.0584" x2="-5.8674" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.8674" y1="-11.049" x2="-5.6388" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.6388" y1="-11.049" x2="-5.6388" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.1308" y1="-10.0584" x2="-5.3594" y2="-10.0584" width="0" layer="51"/>
<wire x1="-5.3594" y1="-10.0584" x2="-5.3594" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.3594" y1="-11.049" x2="-5.1308" y2="-11.049" width="0" layer="51"/>
<wire x1="-5.1308" y1="-11.049" x2="-5.1308" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.6482" y1="-10.0584" x2="-4.8514" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.8514" y1="-10.0584" x2="-4.8514" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.8514" y1="-11.049" x2="-4.6482" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.6482" y1="-11.049" x2="-4.6482" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.1402" y1="-10.0584" x2="-4.3688" y2="-10.0584" width="0" layer="51"/>
<wire x1="-4.3688" y1="-10.0584" x2="-4.3688" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.3688" y1="-11.049" x2="-4.1402" y2="-11.049" width="0" layer="51"/>
<wire x1="-4.1402" y1="-11.049" x2="-4.1402" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.6322" y1="-10.0584" x2="-3.8608" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.8608" y1="-10.0584" x2="-3.8608" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.8608" y1="-11.049" x2="-3.6322" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.6322" y1="-11.049" x2="-3.6322" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.1496" y1="-10.0584" x2="-3.3528" y2="-10.0584" width="0" layer="51"/>
<wire x1="-3.3528" y1="-10.0584" x2="-3.3528" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.3528" y1="-11.049" x2="-3.1496" y2="-11.049" width="0" layer="51"/>
<wire x1="-3.1496" y1="-11.049" x2="-3.1496" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.6416" y1="-10.0584" x2="-2.8702" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.8702" y1="-10.0584" x2="-2.8702" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.8702" y1="-11.049" x2="-2.6416" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.6416" y1="-11.049" x2="-2.6416" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.1336" y1="-10.0584" x2="-2.3622" y2="-10.0584" width="0" layer="51"/>
<wire x1="-2.3622" y1="-10.0584" x2="-2.3622" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.3622" y1="-11.049" x2="-2.1336" y2="-11.049" width="0" layer="51"/>
<wire x1="-2.1336" y1="-11.049" x2="-2.1336" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.651" y1="-10.0584" x2="-1.8542" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.8542" y1="-10.0584" x2="-1.8542" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.8542" y1="-11.049" x2="-1.651" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.651" y1="-11.049" x2="-1.651" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.143" y1="-10.0584" x2="-1.3716" y2="-10.0584" width="0" layer="51"/>
<wire x1="-1.3716" y1="-10.0584" x2="-1.3716" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.3716" y1="-11.049" x2="-1.143" y2="-11.049" width="0" layer="51"/>
<wire x1="-1.143" y1="-11.049" x2="-1.143" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.635" y1="-10.0584" x2="-0.8636" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.8636" y1="-10.0584" x2="-0.8636" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.8636" y1="-11.049" x2="-0.635" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.635" y1="-11.049" x2="-0.635" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.1524" y1="-10.0584" x2="-0.3556" y2="-10.0584" width="0" layer="51"/>
<wire x1="-0.3556" y1="-10.0584" x2="-0.3556" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.3556" y1="-11.049" x2="-0.1524" y2="-11.049" width="0" layer="51"/>
<wire x1="-0.1524" y1="-11.049" x2="-0.1524" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.3556" y1="-10.0584" x2="0.1524" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.1524" y1="-10.0584" x2="0.1524" y2="-11.049" width="0" layer="51"/>
<wire x1="0.1524" y1="-11.049" x2="0.3556" y2="-11.049" width="0" layer="51"/>
<wire x1="0.3556" y1="-11.049" x2="0.3556" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.8636" y1="-10.0584" x2="0.635" y2="-10.0584" width="0" layer="51"/>
<wire x1="0.635" y1="-10.0584" x2="0.635" y2="-11.049" width="0" layer="51"/>
<wire x1="0.635" y1="-11.049" x2="0.8636" y2="-11.049" width="0" layer="51"/>
<wire x1="0.8636" y1="-11.049" x2="0.8636" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.3716" y1="-10.0584" x2="1.143" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.143" y1="-10.0584" x2="1.143" y2="-11.049" width="0" layer="51"/>
<wire x1="1.143" y1="-11.049" x2="1.3716" y2="-11.049" width="0" layer="51"/>
<wire x1="1.3716" y1="-11.049" x2="1.3716" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.8542" y1="-10.0584" x2="1.651" y2="-10.0584" width="0" layer="51"/>
<wire x1="1.651" y1="-10.0584" x2="1.651" y2="-11.049" width="0" layer="51"/>
<wire x1="1.651" y1="-11.049" x2="1.8542" y2="-11.049" width="0" layer="51"/>
<wire x1="1.8542" y1="-11.049" x2="1.8542" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.3622" y1="-10.0584" x2="2.1336" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.1336" y1="-10.0584" x2="2.1336" y2="-11.049" width="0" layer="51"/>
<wire x1="2.1336" y1="-11.049" x2="2.3622" y2="-11.049" width="0" layer="51"/>
<wire x1="2.3622" y1="-11.049" x2="2.3622" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.8702" y1="-10.0584" x2="2.6416" y2="-10.0584" width="0" layer="51"/>
<wire x1="2.6416" y1="-10.0584" x2="2.6416" y2="-11.049" width="0" layer="51"/>
<wire x1="2.6416" y1="-11.049" x2="2.8702" y2="-11.049" width="0" layer="51"/>
<wire x1="2.8702" y1="-11.049" x2="2.8702" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.3528" y1="-10.0584" x2="3.1496" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.1496" y1="-10.0584" x2="3.1496" y2="-11.049" width="0" layer="51"/>
<wire x1="3.1496" y1="-11.049" x2="3.3528" y2="-11.049" width="0" layer="51"/>
<wire x1="3.3528" y1="-11.049" x2="3.3528" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.8608" y1="-10.0584" x2="3.6322" y2="-10.0584" width="0" layer="51"/>
<wire x1="3.6322" y1="-10.0584" x2="3.6322" y2="-11.049" width="0" layer="51"/>
<wire x1="3.6322" y1="-11.049" x2="3.8608" y2="-11.049" width="0" layer="51"/>
<wire x1="3.8608" y1="-11.049" x2="3.8608" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.3688" y1="-10.0584" x2="4.1402" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.1402" y1="-10.0584" x2="4.1402" y2="-11.049" width="0" layer="51"/>
<wire x1="4.1402" y1="-11.049" x2="4.3688" y2="-11.049" width="0" layer="51"/>
<wire x1="4.3688" y1="-11.049" x2="4.3688" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.8514" y1="-10.0584" x2="4.6482" y2="-10.0584" width="0" layer="51"/>
<wire x1="4.6482" y1="-10.0584" x2="4.6482" y2="-11.049" width="0" layer="51"/>
<wire x1="4.6482" y1="-11.049" x2="4.8514" y2="-11.049" width="0" layer="51"/>
<wire x1="4.8514" y1="-11.049" x2="4.8514" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.3594" y1="-10.0584" x2="5.1308" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.1308" y1="-10.0584" x2="5.1308" y2="-11.049" width="0" layer="51"/>
<wire x1="5.1308" y1="-11.049" x2="5.3594" y2="-11.049" width="0" layer="51"/>
<wire x1="5.3594" y1="-11.049" x2="5.3594" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.8674" y1="-10.0584" x2="5.6388" y2="-10.0584" width="0" layer="51"/>
<wire x1="5.6388" y1="-10.0584" x2="5.6388" y2="-11.049" width="0" layer="51"/>
<wire x1="5.6388" y1="-11.049" x2="5.8674" y2="-11.049" width="0" layer="51"/>
<wire x1="5.8674" y1="-11.049" x2="5.8674" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.35" y1="-10.0584" x2="6.1468" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.1468" y1="-10.0584" x2="6.1468" y2="-11.049" width="0" layer="51"/>
<wire x1="6.1468" y1="-11.049" x2="6.35" y2="-11.049" width="0" layer="51"/>
<wire x1="6.35" y1="-11.049" x2="6.35" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.858" y1="-10.0584" x2="6.6294" y2="-10.0584" width="0" layer="51"/>
<wire x1="6.6294" y1="-10.0584" x2="6.6294" y2="-11.049" width="0" layer="51"/>
<wire x1="6.6294" y1="-11.049" x2="6.858" y2="-11.049" width="0" layer="51"/>
<wire x1="6.858" y1="-11.049" x2="6.858" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.366" y1="-10.0584" x2="7.1374" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.1374" y1="-10.0584" x2="7.1374" y2="-11.049" width="0" layer="51"/>
<wire x1="7.1374" y1="-11.049" x2="7.366" y2="-11.049" width="0" layer="51"/>
<wire x1="7.366" y1="-11.049" x2="7.366" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.8486" y1="-10.0584" x2="7.6454" y2="-10.0584" width="0" layer="51"/>
<wire x1="7.6454" y1="-10.0584" x2="7.6454" y2="-11.049" width="0" layer="51"/>
<wire x1="7.6454" y1="-11.049" x2="7.8486" y2="-11.049" width="0" layer="51"/>
<wire x1="7.8486" y1="-11.049" x2="7.8486" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.3566" y1="-10.0584" x2="8.128" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.128" y1="-10.0584" x2="8.128" y2="-11.049" width="0" layer="51"/>
<wire x1="8.128" y1="-11.049" x2="8.3566" y2="-11.049" width="0" layer="51"/>
<wire x1="8.3566" y1="-11.049" x2="8.3566" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.8646" y1="-10.0584" x2="8.636" y2="-10.0584" width="0" layer="51"/>
<wire x1="8.636" y1="-10.0584" x2="8.636" y2="-11.049" width="0" layer="51"/>
<wire x1="8.636" y1="-11.049" x2="8.8646" y2="-11.049" width="0" layer="51"/>
<wire x1="8.8646" y1="-11.049" x2="8.8646" y2="-10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.636" x2="10.0584" y2="-8.8646" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.8646" x2="11.049" y2="-8.8646" width="0" layer="51"/>
<wire x1="11.049" y1="-8.8646" x2="11.049" y2="-8.636" width="0" layer="51"/>
<wire x1="11.049" y1="-8.636" x2="10.0584" y2="-8.636" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.128" x2="10.0584" y2="-8.3566" width="0" layer="51"/>
<wire x1="10.0584" y1="-8.3566" x2="11.049" y2="-8.3566" width="0" layer="51"/>
<wire x1="11.049" y1="-8.3566" x2="11.049" y2="-8.128" width="0" layer="51"/>
<wire x1="11.049" y1="-8.128" x2="10.0584" y2="-8.128" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.6454" x2="10.0584" y2="-7.8486" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.8486" x2="11.049" y2="-7.8486" width="0" layer="51"/>
<wire x1="11.049" y1="-7.8486" x2="11.049" y2="-7.6454" width="0" layer="51"/>
<wire x1="11.049" y1="-7.6454" x2="10.0584" y2="-7.6454" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.1374" x2="10.0584" y2="-7.366" width="0" layer="51"/>
<wire x1="10.0584" y1="-7.366" x2="11.049" y2="-7.366" width="0" layer="51"/>
<wire x1="11.049" y1="-7.366" x2="11.049" y2="-7.1374" width="0" layer="51"/>
<wire x1="11.049" y1="-7.1374" x2="10.0584" y2="-7.1374" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.6294" x2="10.0584" y2="-6.858" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.858" x2="11.049" y2="-6.858" width="0" layer="51"/>
<wire x1="11.049" y1="-6.858" x2="11.049" y2="-6.6294" width="0" layer="51"/>
<wire x1="11.049" y1="-6.6294" x2="10.0584" y2="-6.6294" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.1468" x2="10.0584" y2="-6.35" width="0" layer="51"/>
<wire x1="10.0584" y1="-6.35" x2="11.049" y2="-6.35" width="0" layer="51"/>
<wire x1="11.049" y1="-6.35" x2="11.049" y2="-6.1468" width="0" layer="51"/>
<wire x1="11.049" y1="-6.1468" x2="10.0584" y2="-6.1468" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.6388" x2="10.0584" y2="-5.8674" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.8674" x2="11.049" y2="-5.8674" width="0" layer="51"/>
<wire x1="11.049" y1="-5.8674" x2="11.049" y2="-5.6388" width="0" layer="51"/>
<wire x1="11.049" y1="-5.6388" x2="10.0584" y2="-5.6388" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.1308" x2="10.0584" y2="-5.3594" width="0" layer="51"/>
<wire x1="10.0584" y1="-5.3594" x2="11.049" y2="-5.3594" width="0" layer="51"/>
<wire x1="11.049" y1="-5.3594" x2="11.049" y2="-5.1308" width="0" layer="51"/>
<wire x1="11.049" y1="-5.1308" x2="10.0584" y2="-5.1308" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.6482" x2="10.0584" y2="-4.8514" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.8514" x2="11.049" y2="-4.8514" width="0" layer="51"/>
<wire x1="11.049" y1="-4.8514" x2="11.049" y2="-4.6482" width="0" layer="51"/>
<wire x1="11.049" y1="-4.6482" x2="10.0584" y2="-4.6482" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.1402" x2="10.0584" y2="-4.3688" width="0" layer="51"/>
<wire x1="10.0584" y1="-4.3688" x2="11.049" y2="-4.3688" width="0" layer="51"/>
<wire x1="11.049" y1="-4.3688" x2="11.049" y2="-4.1402" width="0" layer="51"/>
<wire x1="11.049" y1="-4.1402" x2="10.0584" y2="-4.1402" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.6322" x2="10.0584" y2="-3.8608" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.8608" x2="11.049" y2="-3.8608" width="0" layer="51"/>
<wire x1="11.049" y1="-3.8608" x2="11.049" y2="-3.6322" width="0" layer="51"/>
<wire x1="11.049" y1="-3.6322" x2="10.0584" y2="-3.6322" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.1496" x2="10.0584" y2="-3.3528" width="0" layer="51"/>
<wire x1="10.0584" y1="-3.3528" x2="11.049" y2="-3.3528" width="0" layer="51"/>
<wire x1="11.049" y1="-3.3528" x2="11.049" y2="-3.1496" width="0" layer="51"/>
<wire x1="11.049" y1="-3.1496" x2="10.0584" y2="-3.1496" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.6416" x2="10.0584" y2="-2.8702" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.8702" x2="11.049" y2="-2.8702" width="0" layer="51"/>
<wire x1="11.049" y1="-2.8702" x2="11.049" y2="-2.6416" width="0" layer="51"/>
<wire x1="11.049" y1="-2.6416" x2="10.0584" y2="-2.6416" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.1336" x2="10.0584" y2="-2.3622" width="0" layer="51"/>
<wire x1="10.0584" y1="-2.3622" x2="11.049" y2="-2.3622" width="0" layer="51"/>
<wire x1="11.049" y1="-2.3622" x2="11.049" y2="-2.1336" width="0" layer="51"/>
<wire x1="11.049" y1="-2.1336" x2="10.0584" y2="-2.1336" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.651" x2="10.0584" y2="-1.8542" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.8542" x2="11.049" y2="-1.8542" width="0" layer="51"/>
<wire x1="11.049" y1="-1.8542" x2="11.049" y2="-1.651" width="0" layer="51"/>
<wire x1="11.049" y1="-1.651" x2="10.0584" y2="-1.651" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.143" x2="10.0584" y2="-1.3716" width="0" layer="51"/>
<wire x1="10.0584" y1="-1.3716" x2="11.049" y2="-1.3716" width="0" layer="51"/>
<wire x1="11.049" y1="-1.3716" x2="11.049" y2="-1.143" width="0" layer="51"/>
<wire x1="11.049" y1="-1.143" x2="10.0584" y2="-1.143" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.635" x2="10.0584" y2="-0.8636" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.8636" x2="11.049" y2="-0.8636" width="0" layer="51"/>
<wire x1="11.049" y1="-0.8636" x2="11.049" y2="-0.635" width="0" layer="51"/>
<wire x1="11.049" y1="-0.635" x2="10.0584" y2="-0.635" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.1524" x2="10.0584" y2="-0.3556" width="0" layer="51"/>
<wire x1="10.0584" y1="-0.3556" x2="11.049" y2="-0.3556" width="0" layer="51"/>
<wire x1="11.049" y1="-0.3556" x2="11.049" y2="-0.1524" width="0" layer="51"/>
<wire x1="11.049" y1="-0.1524" x2="10.0584" y2="-0.1524" width="0" layer="51"/>
<wire x1="10.0584" y1="0.3556" x2="10.0584" y2="0.1524" width="0" layer="51"/>
<wire x1="10.0584" y1="0.1524" x2="11.049" y2="0.1524" width="0" layer="51"/>
<wire x1="11.049" y1="0.1524" x2="11.049" y2="0.3556" width="0" layer="51"/>
<wire x1="11.049" y1="0.3556" x2="10.0584" y2="0.3556" width="0" layer="51"/>
<wire x1="10.0584" y1="0.8636" x2="10.0584" y2="0.635" width="0" layer="51"/>
<wire x1="10.0584" y1="0.635" x2="11.049" y2="0.635" width="0" layer="51"/>
<wire x1="11.049" y1="0.635" x2="11.049" y2="0.8636" width="0" layer="51"/>
<wire x1="11.049" y1="0.8636" x2="10.0584" y2="0.8636" width="0" layer="51"/>
<wire x1="10.0584" y1="1.3716" x2="10.0584" y2="1.143" width="0" layer="51"/>
<wire x1="10.0584" y1="1.143" x2="11.049" y2="1.143" width="0" layer="51"/>
<wire x1="11.049" y1="1.143" x2="11.049" y2="1.3716" width="0" layer="51"/>
<wire x1="11.049" y1="1.3716" x2="10.0584" y2="1.3716" width="0" layer="51"/>
<wire x1="10.0584" y1="1.8542" x2="10.0584" y2="1.651" width="0" layer="51"/>
<wire x1="10.0584" y1="1.651" x2="11.049" y2="1.651" width="0" layer="51"/>
<wire x1="11.049" y1="1.651" x2="11.049" y2="1.8542" width="0" layer="51"/>
<wire x1="11.049" y1="1.8542" x2="10.0584" y2="1.8542" width="0" layer="51"/>
<wire x1="10.0584" y1="2.3622" x2="10.0584" y2="2.1336" width="0" layer="51"/>
<wire x1="10.0584" y1="2.1336" x2="11.049" y2="2.1336" width="0" layer="51"/>
<wire x1="11.049" y1="2.1336" x2="11.049" y2="2.3622" width="0" layer="51"/>
<wire x1="11.049" y1="2.3622" x2="10.0584" y2="2.3622" width="0" layer="51"/>
<wire x1="10.0584" y1="2.8702" x2="10.0584" y2="2.6416" width="0" layer="51"/>
<wire x1="10.0584" y1="2.6416" x2="11.049" y2="2.6416" width="0" layer="51"/>
<wire x1="11.049" y1="2.6416" x2="11.049" y2="2.8702" width="0" layer="51"/>
<wire x1="11.049" y1="2.8702" x2="10.0584" y2="2.8702" width="0" layer="51"/>
<wire x1="10.0584" y1="3.3528" x2="10.0584" y2="3.1496" width="0" layer="51"/>
<wire x1="10.0584" y1="3.1496" x2="11.049" y2="3.1496" width="0" layer="51"/>
<wire x1="11.049" y1="3.1496" x2="11.049" y2="3.3528" width="0" layer="51"/>
<wire x1="11.049" y1="3.3528" x2="10.0584" y2="3.3528" width="0" layer="51"/>
<wire x1="10.0584" y1="3.8608" x2="10.0584" y2="3.6322" width="0" layer="51"/>
<wire x1="10.0584" y1="3.6322" x2="11.049" y2="3.6322" width="0" layer="51"/>
<wire x1="11.049" y1="3.6322" x2="11.049" y2="3.8608" width="0" layer="51"/>
<wire x1="11.049" y1="3.8608" x2="10.0584" y2="3.8608" width="0" layer="51"/>
<wire x1="10.0584" y1="4.3688" x2="10.0584" y2="4.1402" width="0" layer="51"/>
<wire x1="10.0584" y1="4.1402" x2="11.049" y2="4.1402" width="0" layer="51"/>
<wire x1="11.049" y1="4.1402" x2="11.049" y2="4.3688" width="0" layer="51"/>
<wire x1="11.049" y1="4.3688" x2="10.0584" y2="4.3688" width="0" layer="51"/>
<wire x1="10.0584" y1="4.8514" x2="10.0584" y2="4.6482" width="0" layer="51"/>
<wire x1="10.0584" y1="4.6482" x2="11.049" y2="4.6482" width="0" layer="51"/>
<wire x1="11.049" y1="4.6482" x2="11.049" y2="4.8514" width="0" layer="51"/>
<wire x1="11.049" y1="4.8514" x2="10.0584" y2="4.8514" width="0" layer="51"/>
<wire x1="10.0584" y1="5.3594" x2="10.0584" y2="5.1308" width="0" layer="51"/>
<wire x1="10.0584" y1="5.1308" x2="11.049" y2="5.1308" width="0" layer="51"/>
<wire x1="11.049" y1="5.1308" x2="11.049" y2="5.3594" width="0" layer="51"/>
<wire x1="11.049" y1="5.3594" x2="10.0584" y2="5.3594" width="0" layer="51"/>
<wire x1="10.0584" y1="5.8674" x2="10.0584" y2="5.6388" width="0" layer="51"/>
<wire x1="10.0584" y1="5.6388" x2="11.049" y2="5.6388" width="0" layer="51"/>
<wire x1="11.049" y1="5.6388" x2="11.049" y2="5.8674" width="0" layer="51"/>
<wire x1="11.049" y1="5.8674" x2="10.0584" y2="5.8674" width="0" layer="51"/>
<wire x1="10.0584" y1="6.35" x2="10.0584" y2="6.1468" width="0" layer="51"/>
<wire x1="10.0584" y1="6.1468" x2="11.049" y2="6.1468" width="0" layer="51"/>
<wire x1="11.049" y1="6.1468" x2="11.049" y2="6.35" width="0" layer="51"/>
<wire x1="11.049" y1="6.35" x2="10.0584" y2="6.35" width="0" layer="51"/>
<wire x1="10.0584" y1="6.858" x2="10.0584" y2="6.6294" width="0" layer="51"/>
<wire x1="10.0584" y1="6.6294" x2="11.049" y2="6.6294" width="0" layer="51"/>
<wire x1="11.049" y1="6.6294" x2="11.049" y2="6.858" width="0" layer="51"/>
<wire x1="11.049" y1="6.858" x2="10.0584" y2="6.858" width="0" layer="51"/>
<wire x1="10.0584" y1="7.366" x2="10.0584" y2="7.1374" width="0" layer="51"/>
<wire x1="10.0584" y1="7.1374" x2="11.049" y2="7.1374" width="0" layer="51"/>
<wire x1="11.049" y1="7.1374" x2="11.049" y2="7.366" width="0" layer="51"/>
<wire x1="11.049" y1="7.366" x2="10.0584" y2="7.366" width="0" layer="51"/>
<wire x1="10.0584" y1="7.8486" x2="10.0584" y2="7.6454" width="0" layer="51"/>
<wire x1="10.0584" y1="7.6454" x2="11.049" y2="7.6454" width="0" layer="51"/>
<wire x1="11.049" y1="7.6454" x2="11.049" y2="7.8486" width="0" layer="51"/>
<wire x1="11.049" y1="7.8486" x2="10.0584" y2="7.8486" width="0" layer="51"/>
<wire x1="10.0584" y1="8.3566" x2="10.0584" y2="8.128" width="0" layer="51"/>
<wire x1="10.0584" y1="8.128" x2="11.049" y2="8.128" width="0" layer="51"/>
<wire x1="11.049" y1="8.128" x2="11.049" y2="8.3566" width="0" layer="51"/>
<wire x1="11.049" y1="8.3566" x2="10.0584" y2="8.3566" width="0" layer="51"/>
<wire x1="10.0584" y1="8.8646" x2="10.0584" y2="8.636" width="0" layer="51"/>
<wire x1="10.0584" y1="8.636" x2="11.049" y2="8.636" width="0" layer="51"/>
<wire x1="11.049" y1="8.636" x2="11.049" y2="8.8646" width="0" layer="51"/>
<wire x1="11.049" y1="8.8646" x2="10.0584" y2="8.8646" width="0" layer="51"/>
<wire x1="-10.0584" y1="8.7884" x2="-8.7884" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="-10.0584" x2="10.0584" y2="-10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="-10.0584" x2="10.0584" y2="10.0584" width="0" layer="51"/>
<wire x1="10.0584" y1="10.0584" x2="-10.0584" y2="10.0584" width="0" layer="51"/>
<wire x1="-10.0584" y1="10.0584" x2="-10.0584" y2="-10.0584" width="0" layer="51"/>
<text x="-12.6238" y="8.7376" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-4.6736" y="-15.0368" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="15.0368" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="AT32UC3A0256-ALUT_A">
<pin name="VDDIO_2" x="-22.86" y="50.8" length="middle" direction="pwr"/>
<pin name="VDDOUT" x="-22.86" y="48.26" length="middle" direction="pwr"/>
<pin name="VDDIN" x="-22.86" y="45.72" length="middle" direction="pwr"/>
<pin name="VDDCORE_2" x="-22.86" y="43.18" length="middle" direction="pwr"/>
<pin name="VDDIO_3" x="-22.86" y="40.64" length="middle" direction="pwr"/>
<pin name="VDDCORE_3" x="-22.86" y="38.1" length="middle" direction="pwr"/>
<pin name="VDDCORE" x="-22.86" y="35.56" length="middle" direction="pwr"/>
<pin name="VDDIO" x="-22.86" y="33.02" length="middle" direction="pwr"/>
<pin name="RESET_N" x="-22.86" y="27.94" length="middle" direction="in"/>
<pin name="VBUS" x="-22.86" y="25.4" length="middle" direction="in"/>
<pin name="DM" x="-22.86" y="22.86" length="middle" direction="in"/>
<pin name="DP" x="-22.86" y="20.32" length="middle" direction="in"/>
<pin name="PA00" x="-22.86" y="15.24" length="middle"/>
<pin name="PA01" x="-22.86" y="12.7" length="middle"/>
<pin name="PA02" x="-22.86" y="10.16" length="middle"/>
<pin name="PA03" x="-22.86" y="7.62" length="middle"/>
<pin name="PA04" x="-22.86" y="5.08" length="middle"/>
<pin name="PA05" x="-22.86" y="2.54" length="middle"/>
<pin name="PA06" x="-22.86" y="0" length="middle"/>
<pin name="PA07" x="-22.86" y="-2.54" length="middle"/>
<pin name="PA08" x="-22.86" y="-5.08" length="middle"/>
<pin name="PA09" x="-22.86" y="-7.62" length="middle"/>
<pin name="PA10" x="-22.86" y="-10.16" length="middle"/>
<pin name="PA11" x="-22.86" y="-12.7" length="middle"/>
<pin name="PA12" x="-22.86" y="-15.24" length="middle"/>
<pin name="PA13" x="-22.86" y="-17.78" length="middle"/>
<pin name="PA14" x="-22.86" y="-20.32" length="middle"/>
<pin name="PA15" x="-22.86" y="-22.86" length="middle"/>
<pin name="PA16" x="-22.86" y="-25.4" length="middle"/>
<pin name="PA17" x="-22.86" y="-27.94" length="middle"/>
<pin name="PA18" x="-22.86" y="-30.48" length="middle"/>
<pin name="PA19" x="-22.86" y="-33.02" length="middle"/>
<pin name="PA20" x="-22.86" y="-35.56" length="middle"/>
<pin name="N/C" x="-22.86" y="-40.64" length="middle" direction="nc"/>
<pin name="GND_2" x="-22.86" y="-45.72" length="middle" direction="pas"/>
<pin name="GND_3" x="-22.86" y="-48.26" length="middle" direction="pas"/>
<pin name="GND_4" x="-22.86" y="-50.8" length="middle" direction="pas"/>
<pin name="GND_5" x="-22.86" y="-53.34" length="middle" direction="pas"/>
<pin name="GND_6" x="-22.86" y="-55.88" length="middle" direction="pas"/>
<pin name="GND" x="-22.86" y="-58.42" length="middle" direction="pas"/>
<pin name="PB20" x="22.86" y="50.8" length="middle" rot="R180"/>
<pin name="PB21" x="22.86" y="48.26" length="middle" rot="R180"/>
<pin name="PB22" x="22.86" y="45.72" length="middle" rot="R180"/>
<pin name="PB23" x="22.86" y="43.18" length="middle" rot="R180"/>
<pin name="PB24" x="22.86" y="40.64" length="middle" rot="R180"/>
<pin name="PB25" x="22.86" y="38.1" length="middle" rot="R180"/>
<pin name="PB26" x="22.86" y="35.56" length="middle" rot="R180"/>
<pin name="PB27" x="22.86" y="33.02" length="middle" rot="R180"/>
<pin name="PB28" x="22.86" y="30.48" length="middle" rot="R180"/>
<pin name="PB29" x="22.86" y="27.94" length="middle" rot="R180"/>
<pin name="PB30" x="22.86" y="25.4" length="middle" rot="R180"/>
<pin name="PB31" x="22.86" y="22.86" length="middle" rot="R180"/>
<pin name="PX00" x="22.86" y="17.78" length="middle" rot="R180"/>
<pin name="PX01" x="22.86" y="15.24" length="middle" rot="R180"/>
<pin name="PX02" x="22.86" y="12.7" length="middle" rot="R180"/>
<pin name="PX03" x="22.86" y="10.16" length="middle" rot="R180"/>
<pin name="PX04" x="22.86" y="7.62" length="middle" rot="R180"/>
<pin name="PX05" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="PX06" x="22.86" y="2.54" length="middle" rot="R180"/>
<pin name="PX07" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="PX08" x="22.86" y="-2.54" length="middle" rot="R180"/>
<pin name="PX09" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="PX10" x="22.86" y="-7.62" length="middle" rot="R180"/>
<pin name="PX11" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="PX12" x="22.86" y="-12.7" length="middle" rot="R180"/>
<pin name="PX13" x="22.86" y="-15.24" length="middle" rot="R180"/>
<pin name="PX14" x="22.86" y="-17.78" length="middle" rot="R180"/>
<pin name="PX15" x="22.86" y="-20.32" length="middle" rot="R180"/>
<pin name="PX16" x="22.86" y="-22.86" length="middle" rot="R180"/>
<pin name="PX17" x="22.86" y="-25.4" length="middle" rot="R180"/>
<pin name="PX18" x="22.86" y="-27.94" length="middle" rot="R180"/>
<pin name="PX19" x="22.86" y="-30.48" length="middle" rot="R180"/>
<wire x1="-17.78" y1="55.88" x2="-17.78" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="-17.78" y1="-63.5" x2="17.78" y2="-63.5" width="0.4064" layer="94"/>
<wire x1="17.78" y1="-63.5" x2="17.78" y2="55.88" width="0.4064" layer="94"/>
<wire x1="17.78" y1="55.88" x2="-17.78" y2="55.88" width="0.4064" layer="94"/>
<text x="-6.0198" y="58.4708" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.08" y="-67.0814" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
<symbol name="AT32UC3A0256-ALUT_B">
<pin name="VDDIO_2" x="-17.78" y="48.26" length="middle" direction="pwr"/>
<pin name="VDDIO_3" x="-17.78" y="45.72" length="middle" direction="pwr"/>
<pin name="VDDIO_4" x="-17.78" y="43.18" length="middle" direction="pwr"/>
<pin name="VDDIO" x="-17.78" y="40.64" length="middle" direction="pwr"/>
<pin name="VDDANA" x="-17.78" y="38.1" length="middle" direction="pwr"/>
<pin name="VDDPLL" x="-17.78" y="35.56" length="middle" direction="pwr"/>
<pin name="VDDCORE" x="-17.78" y="33.02" length="middle" direction="pwr"/>
<pin name="TMS" x="-17.78" y="27.94" length="middle" direction="in"/>
<pin name="TCK" x="-17.78" y="25.4" length="middle" direction="in"/>
<pin name="TDI" x="-17.78" y="22.86" length="middle" direction="in"/>
<pin name="PA21" x="-17.78" y="17.78" length="middle"/>
<pin name="PA22" x="-17.78" y="15.24" length="middle"/>
<pin name="PA23" x="-17.78" y="12.7" length="middle"/>
<pin name="PA24" x="-17.78" y="10.16" length="middle"/>
<pin name="PA25" x="-17.78" y="7.62" length="middle"/>
<pin name="PA26" x="-17.78" y="5.08" length="middle"/>
<pin name="PA27" x="-17.78" y="2.54" length="middle"/>
<pin name="PA28" x="-17.78" y="0" length="middle"/>
<pin name="PA29" x="-17.78" y="-2.54" length="middle"/>
<pin name="PA30" x="-17.78" y="-5.08" length="middle"/>
<pin name="PB00" x="-17.78" y="-10.16" length="middle"/>
<pin name="PB01" x="-17.78" y="-12.7" length="middle"/>
<pin name="PB02" x="-17.78" y="-15.24" length="middle"/>
<pin name="PB03" x="-17.78" y="-17.78" length="middle"/>
<pin name="PB04" x="-17.78" y="-20.32" length="middle"/>
<pin name="PB05" x="-17.78" y="-22.86" length="middle"/>
<pin name="PB06" x="-17.78" y="-25.4" length="middle"/>
<pin name="PB07" x="-17.78" y="-27.94" length="middle"/>
<pin name="PB08" x="-17.78" y="-30.48" length="middle"/>
<pin name="PB09" x="-17.78" y="-33.02" length="middle"/>
<pin name="PB10" x="-17.78" y="-35.56" length="middle"/>
<pin name="ADVREF" x="-17.78" y="-40.64" length="middle" direction="pas"/>
<pin name="GNDANA" x="-17.78" y="-45.72" length="middle" direction="pas"/>
<pin name="GND_2" x="-17.78" y="-48.26" length="middle" direction="pas"/>
<pin name="GND_3" x="-17.78" y="-50.8" length="middle" direction="pas"/>
<pin name="GND" x="-17.78" y="-53.34" length="middle" direction="pas"/>
<pin name="TDO" x="17.78" y="48.26" length="middle" direction="out" rot="R180"/>
<pin name="PB11" x="17.78" y="43.18" length="middle" rot="R180"/>
<pin name="PB12" x="17.78" y="40.64" length="middle" rot="R180"/>
<pin name="PB13" x="17.78" y="38.1" length="middle" rot="R180"/>
<pin name="PB14" x="17.78" y="35.56" length="middle" rot="R180"/>
<pin name="PB15" x="17.78" y="33.02" length="middle" rot="R180"/>
<pin name="PB16" x="17.78" y="30.48" length="middle" rot="R180"/>
<pin name="PB17" x="17.78" y="27.94" length="middle" rot="R180"/>
<pin name="PB18" x="17.78" y="25.4" length="middle" rot="R180"/>
<pin name="PB19" x="17.78" y="22.86" length="middle" rot="R180"/>
<pin name="PC00" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="PC01" x="17.78" y="15.24" length="middle" rot="R180"/>
<pin name="PC02" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="PC03" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PC04" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PC05" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PX20" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PX21" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="PX22" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PX23" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PX24" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PX25" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="PX26" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="PX27" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="PX28" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="PX29" x="17.78" y="-22.86" length="middle" rot="R180"/>
<pin name="PX30" x="17.78" y="-25.4" length="middle" rot="R180"/>
<pin name="PX31" x="17.78" y="-27.94" length="middle" rot="R180"/>
<pin name="PX32" x="17.78" y="-30.48" length="middle" rot="R180"/>
<pin name="PX33" x="17.78" y="-33.02" length="middle" rot="R180"/>
<pin name="PX34" x="17.78" y="-35.56" length="middle" rot="R180"/>
<pin name="PX35" x="17.78" y="-38.1" length="middle" rot="R180"/>
<pin name="PX36" x="17.78" y="-40.64" length="middle" rot="R180"/>
<pin name="PX37" x="17.78" y="-43.18" length="middle" rot="R180"/>
<pin name="PX38" x="17.78" y="-45.72" length="middle" rot="R180"/>
<pin name="PX39" x="17.78" y="-48.26" length="middle" rot="R180"/>
<wire x1="-12.7" y1="53.34" x2="-12.7" y2="-58.42" width="0.4064" layer="94"/>
<wire x1="-12.7" y1="-58.42" x2="12.7" y2="-58.42" width="0.4064" layer="94"/>
<wire x1="12.7" y1="-58.42" x2="12.7" y2="53.34" width="0.4064" layer="94"/>
<wire x1="12.7" y1="53.34" x2="-12.7" y2="53.34" width="0.4064" layer="94"/>
<text x="-5.1562" y="55.2704" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.4356" y="-61.849" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="AT32UC3A0256-ALUT" prefix="U">
<description>32-Bit Microcontroller</description>
<gates>
<gate name="A" symbol="AT32UC3A0256-ALUT_A" x="0" y="0"/>
<gate name="B" symbol="AT32UC3A0256-ALUT_B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P2200X2200X160-144N">
<connects>
<connect gate="A" pin="DM" pad="70"/>
<connect gate="A" pin="DP" pad="71"/>
<connect gate="A" pin="GND" pad="72"/>
<connect gate="A" pin="GND_2" pad="8"/>
<connect gate="A" pin="GND_3" pad="18"/>
<connect gate="A" pin="GND_4" pad="28"/>
<connect gate="A" pin="GND_5" pad="37"/>
<connect gate="A" pin="GND_6" pad="52"/>
<connect gate="A" pin="N/C" pad="49"/>
<connect gate="A" pin="PA00" pad="25"/>
<connect gate="A" pin="PA01" pad="27"/>
<connect gate="A" pin="PA02" pad="30"/>
<connect gate="A" pin="PA03" pad="32"/>
<connect gate="A" pin="PA04" pad="34"/>
<connect gate="A" pin="PA05" pad="39"/>
<connect gate="A" pin="PA06" pad="41"/>
<connect gate="A" pin="PA07" pad="43"/>
<connect gate="A" pin="PA08" pad="45"/>
<connect gate="A" pin="PA09" pad="47"/>
<connect gate="A" pin="PA10" pad="48"/>
<connect gate="A" pin="PA11" pad="50"/>
<connect gate="A" pin="PA12" pad="53"/>
<connect gate="A" pin="PA13" pad="54"/>
<connect gate="A" pin="PA14" pad="56"/>
<connect gate="A" pin="PA15" pad="57"/>
<connect gate="A" pin="PA16" pad="58"/>
<connect gate="A" pin="PA17" pad="60"/>
<connect gate="A" pin="PA18" pad="62"/>
<connect gate="A" pin="PA19" pad="64"/>
<connect gate="A" pin="PA20" pad="66"/>
<connect gate="A" pin="PB20" pad="3"/>
<connect gate="A" pin="PB21" pad="5"/>
<connect gate="A" pin="PB22" pad="6"/>
<connect gate="A" pin="PB23" pad="9"/>
<connect gate="A" pin="PB24" pad="11"/>
<connect gate="A" pin="PB25" pad="13"/>
<connect gate="A" pin="PB26" pad="14"/>
<connect gate="A" pin="PB27" pad="15"/>
<connect gate="A" pin="PB28" pad="19"/>
<connect gate="A" pin="PB29" pad="20"/>
<connect gate="A" pin="PB30" pad="21"/>
<connect gate="A" pin="PB31" pad="22"/>
<connect gate="A" pin="PX00" pad="1"/>
<connect gate="A" pin="PX01" pad="2"/>
<connect gate="A" pin="PX02" pad="4"/>
<connect gate="A" pin="PX03" pad="10"/>
<connect gate="A" pin="PX04" pad="12"/>
<connect gate="A" pin="PX05" pad="24"/>
<connect gate="A" pin="PX06" pad="26"/>
<connect gate="A" pin="PX07" pad="31"/>
<connect gate="A" pin="PX08" pad="33"/>
<connect gate="A" pin="PX09" pad="35"/>
<connect gate="A" pin="PX10" pad="38"/>
<connect gate="A" pin="PX11" pad="40"/>
<connect gate="A" pin="PX12" pad="42"/>
<connect gate="A" pin="PX13" pad="44"/>
<connect gate="A" pin="PX14" pad="46"/>
<connect gate="A" pin="PX15" pad="59"/>
<connect gate="A" pin="PX16" pad="61"/>
<connect gate="A" pin="PX17" pad="63"/>
<connect gate="A" pin="PX18" pad="65"/>
<connect gate="A" pin="PX19" pad="67"/>
<connect gate="A" pin="RESET_N" pad="23"/>
<connect gate="A" pin="VBUS" pad="68"/>
<connect gate="A" pin="VDDCORE" pad="55"/>
<connect gate="A" pin="VDDCORE_2" pad="29"/>
<connect gate="A" pin="VDDCORE_3" pad="51"/>
<connect gate="A" pin="VDDIN" pad="17"/>
<connect gate="A" pin="VDDIO" pad="69"/>
<connect gate="A" pin="VDDIO_2" pad="7"/>
<connect gate="A" pin="VDDIO_3" pad="36"/>
<connect gate="A" pin="VDDOUT" pad="16"/>
<connect gate="B" pin="ADVREF" pad="82"/>
<connect gate="B" pin="GND" pad="117"/>
<connect gate="B" pin="GNDANA" pad="83"/>
<connect gate="B" pin="GND_2" pad="94"/>
<connect gate="B" pin="GND_3" pad="109"/>
<connect gate="B" pin="PA21" pad="73"/>
<connect gate="B" pin="PA22" pad="74"/>
<connect gate="B" pin="PA23" pad="75"/>
<connect gate="B" pin="PA24" pad="76"/>
<connect gate="B" pin="PA25" pad="77"/>
<connect gate="B" pin="PA26" pad="78"/>
<connect gate="B" pin="PA27" pad="79"/>
<connect gate="B" pin="PA28" pad="80"/>
<connect gate="B" pin="PA29" pad="122"/>
<connect gate="B" pin="PA30" pad="123"/>
<connect gate="B" pin="PB00" pad="88"/>
<connect gate="B" pin="PB01" pad="90"/>
<connect gate="B" pin="PB02" pad="96"/>
<connect gate="B" pin="PB03" pad="98"/>
<connect gate="B" pin="PB04" pad="100"/>
<connect gate="B" pin="PB05" pad="102"/>
<connect gate="B" pin="PB06" pad="104"/>
<connect gate="B" pin="PB07" pad="106"/>
<connect gate="B" pin="PB08" pad="111"/>
<connect gate="B" pin="PB09" pad="113"/>
<connect gate="B" pin="PB10" pad="115"/>
<connect gate="B" pin="PB11" pad="119"/>
<connect gate="B" pin="PB12" pad="121"/>
<connect gate="B" pin="PB13" pad="126"/>
<connect gate="B" pin="PB14" pad="127"/>
<connect gate="B" pin="PB15" pad="134"/>
<connect gate="B" pin="PB16" pad="136"/>
<connect gate="B" pin="PB17" pad="139"/>
<connect gate="B" pin="PB18" pad="141"/>
<connect gate="B" pin="PB19" pad="143"/>
<connect gate="B" pin="PC00" pad="85"/>
<connect gate="B" pin="PC01" pad="86"/>
<connect gate="B" pin="PC02" pad="124"/>
<connect gate="B" pin="PC03" pad="125"/>
<connect gate="B" pin="PC04" pad="132"/>
<connect gate="B" pin="PC05" pad="133"/>
<connect gate="B" pin="PX20" pad="87"/>
<connect gate="B" pin="PX21" pad="89"/>
<connect gate="B" pin="PX22" pad="91"/>
<connect gate="B" pin="PX23" pad="95"/>
<connect gate="B" pin="PX24" pad="97"/>
<connect gate="B" pin="PX25" pad="99"/>
<connect gate="B" pin="PX26" pad="101"/>
<connect gate="B" pin="PX27" pad="103"/>
<connect gate="B" pin="PX28" pad="105"/>
<connect gate="B" pin="PX29" pad="107"/>
<connect gate="B" pin="PX30" pad="110"/>
<connect gate="B" pin="PX31" pad="112"/>
<connect gate="B" pin="PX32" pad="114"/>
<connect gate="B" pin="PX33" pad="118"/>
<connect gate="B" pin="PX34" pad="120"/>
<connect gate="B" pin="PX35" pad="135"/>
<connect gate="B" pin="PX36" pad="137"/>
<connect gate="B" pin="PX37" pad="140"/>
<connect gate="B" pin="PX38" pad="142"/>
<connect gate="B" pin="PX39" pad="144"/>
<connect gate="B" pin="TCK" pad="129"/>
<connect gate="B" pin="TDI" pad="131"/>
<connect gate="B" pin="TDO" pad="130"/>
<connect gate="B" pin="TMS" pad="128"/>
<connect gate="B" pin="VDDANA" pad="81"/>
<connect gate="B" pin="VDDCORE" pad="138"/>
<connect gate="B" pin="VDDIO" pad="116"/>
<connect gate="B" pin="VDDIO_2" pad="92"/>
<connect gate="B" pin="VDDIO_3" pad="93"/>
<connect gate="B" pin="VDDIO_4" pad="108"/>
<connect gate="B" pin="VDDPLL" pad="84"/>
</connects>
<technologies>
<technology name="">
<attribute name="DESCRIPTION" value="32-Bit Microcontroller" constant="no"/>
<attribute name="MPN" value="AT32UC3A0256-ALUT" constant="no"/>
<attribute name="OC_FARNELL" value="1841635" constant="no"/>
<attribute name="OC_NEWARK" value="12T1369" constant="no"/>
<attribute name="PACKAGE" value="LQFP-144" constant="no"/>
<attribute name="SUPPLIER" value="Atmel Corporation" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Texas Instruments_By_element14_Batch_1">
<description>Developed by element14 :&lt;br&gt;
element14 CAD Library consolidation.ulp
at 30/07/2012 17:45:58</description>
<packages>
<package name="QFP50P900X900X120-49N">
<smd name="1" x="-4.2164" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="2" x="-4.2164" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="3" x="-4.2164" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="4" x="-4.2164" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="5" x="-4.2164" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="6" x="-4.2164" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="7" x="-4.2164" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="8" x="-4.2164" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="9" x="-4.2164" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="10" x="-4.2164" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="11" x="-4.2164" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="12" x="-4.2164" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="13" x="-2.7432" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="14" x="-2.2606" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="15" x="-1.7526" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="16" x="-1.2446" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="17" x="-0.762" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="18" x="-0.254" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="19" x="0.254" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="20" x="0.762" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="21" x="1.2446" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="22" x="1.7526" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="23" x="2.2606" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="24" x="2.7432" y="-4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="25" x="4.2164" y="-2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="26" x="4.2164" y="-2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="27" x="4.2164" y="-1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="28" x="4.2164" y="-1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="29" x="4.2164" y="-0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="30" x="4.2164" y="-0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="31" x="4.2164" y="0.254" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="32" x="4.2164" y="0.762" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="33" x="4.2164" y="1.2446" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="34" x="4.2164" y="1.7526" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="35" x="4.2164" y="2.2606" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="36" x="4.2164" y="2.7432" dx="0.2794" dy="1.4732" layer="1" rot="R270"/>
<smd name="37" x="2.7432" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="38" x="2.2606" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="39" x="1.7526" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="40" x="1.2446" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="41" x="0.762" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="42" x="0.254" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="43" x="-0.254" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="44" x="-0.762" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="45" x="-1.2446" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="46" x="-1.7526" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="47" x="-2.2606" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="48" x="-2.7432" y="4.2164" dx="0.2794" dy="1.4732" layer="1" rot="R180"/>
<smd name="49" x="0" y="0" dx="4.5974" dy="4.5974" layer="1"/>
<wire x1="0.762" y1="-5.2832" x2="0.762" y2="-6.3246" width="0.1524" layer="21"/>
<wire x1="-6.2738" y1="-1.778" x2="-5.2578" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="1.27" y1="5.2832" x2="1.27" y2="6.2992" width="0.1524" layer="21"/>
<wire x1="5.2578" y1="-0.254" x2="6.2738" y2="-0.254" width="0.1524" layer="21"/>
<wire x1="-3.2258" y1="3.6068" x2="-3.6068" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-3.1496" y1="2.7686" x2="-2.7686" y2="3.1496" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.6068" x2="-3.2258" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="-3.6068" x2="3.6068" y2="-3.2258" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.6068" x2="3.2258" y2="3.6068" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="3.6068" x2="-3.6068" y2="3.2258" width="0.1524" layer="21"/>
<wire x1="-3.6068" y1="-3.2258" x2="-3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.2258" y1="-3.6068" x2="3.6068" y2="-3.6068" width="0.1524" layer="21"/>
<wire x1="3.6068" y1="3.2258" x2="3.6068" y2="3.6068" width="0.1524" layer="21"/>
<text x="-6.1722" y="2.7432" size="1.27" layer="21" ratio="6" rot="SR0">*</text>
<wire x1="2.6162" y1="3.6068" x2="2.8956" y2="3.6068" width="0" layer="51"/>
<wire x1="2.8956" y1="3.6068" x2="2.8956" y2="4.5974" width="0" layer="51"/>
<wire x1="2.8956" y1="4.5974" x2="2.6162" y2="4.5974" width="0" layer="51"/>
<wire x1="2.6162" y1="4.5974" x2="2.6162" y2="3.6068" width="0" layer="51"/>
<wire x1="2.1082" y1="3.6068" x2="2.3876" y2="3.6068" width="0" layer="51"/>
<wire x1="2.3876" y1="3.6068" x2="2.3876" y2="4.5974" width="0" layer="51"/>
<wire x1="2.3876" y1="4.5974" x2="2.1082" y2="4.5974" width="0" layer="51"/>
<wire x1="2.1082" y1="4.5974" x2="2.1082" y2="3.6068" width="0" layer="51"/>
<wire x1="1.6256" y1="3.6068" x2="1.8796" y2="3.6068" width="0" layer="51"/>
<wire x1="1.8796" y1="3.6068" x2="1.8796" y2="4.5974" width="0" layer="51"/>
<wire x1="1.8796" y1="4.5974" x2="1.6256" y2="4.5974" width="0" layer="51"/>
<wire x1="1.6256" y1="4.5974" x2="1.6256" y2="3.6068" width="0" layer="51"/>
<wire x1="1.1176" y1="3.6068" x2="1.397" y2="3.6068" width="0" layer="51"/>
<wire x1="1.397" y1="3.6068" x2="1.397" y2="4.5974" width="0" layer="51"/>
<wire x1="1.397" y1="4.5974" x2="1.1176" y2="4.5974" width="0" layer="51"/>
<wire x1="1.1176" y1="4.5974" x2="1.1176" y2="3.6068" width="0" layer="51"/>
<wire x1="0.6096" y1="3.6068" x2="0.889" y2="3.6068" width="0" layer="51"/>
<wire x1="0.889" y1="3.6068" x2="0.889" y2="4.5974" width="0" layer="51"/>
<wire x1="0.889" y1="4.5974" x2="0.6096" y2="4.5974" width="0" layer="51"/>
<wire x1="0.6096" y1="4.5974" x2="0.6096" y2="3.6068" width="0" layer="51"/>
<wire x1="0.127" y1="3.6068" x2="0.381" y2="3.6068" width="0" layer="51"/>
<wire x1="0.381" y1="3.6068" x2="0.381" y2="4.5974" width="0" layer="51"/>
<wire x1="0.381" y1="4.5974" x2="0.127" y2="4.5974" width="0" layer="51"/>
<wire x1="0.127" y1="4.5974" x2="0.127" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.381" y1="3.6068" x2="-0.127" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.127" y1="3.6068" x2="-0.127" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.127" y1="4.5974" x2="-0.381" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.381" y1="4.5974" x2="-0.381" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.889" y1="3.6068" x2="-0.6096" y2="3.6068" width="0" layer="51"/>
<wire x1="-0.6096" y1="3.6068" x2="-0.6096" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.6096" y1="4.5974" x2="-0.889" y2="4.5974" width="0" layer="51"/>
<wire x1="-0.889" y1="4.5974" x2="-0.889" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.397" y1="3.6068" x2="-1.1176" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.1176" y1="3.6068" x2="-1.1176" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.1176" y1="4.5974" x2="-1.397" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.397" y1="4.5974" x2="-1.397" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.8796" y1="3.6068" x2="-1.6256" y2="3.6068" width="0" layer="51"/>
<wire x1="-1.6256" y1="3.6068" x2="-1.6256" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.6256" y1="4.5974" x2="-1.8796" y2="4.5974" width="0" layer="51"/>
<wire x1="-1.8796" y1="4.5974" x2="-1.8796" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.3876" y1="3.6068" x2="-2.3368" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.3368" y1="3.6068" x2="-2.1082" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.1082" y1="3.6068" x2="-2.1082" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.1082" y1="4.5974" x2="-2.3876" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.3876" y1="4.5974" x2="-2.3876" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.8956" y1="3.6068" x2="-2.6162" y2="3.6068" width="0" layer="51"/>
<wire x1="-2.6162" y1="3.6068" x2="-2.6162" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.6162" y1="4.5974" x2="-2.8956" y2="4.5974" width="0" layer="51"/>
<wire x1="-2.8956" y1="4.5974" x2="-2.8956" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.6162" x2="-3.6068" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.8956" x2="-4.5974" y2="2.8956" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.8956" x2="-4.5974" y2="2.6162" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.6162" x2="-3.6068" y2="2.6162" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.1082" x2="-3.6068" y2="2.3368" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3368" x2="-3.6068" y2="2.3876" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3876" x2="-4.5974" y2="2.3876" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.3876" x2="-4.5974" y2="2.1082" width="0" layer="51"/>
<wire x1="-4.5974" y1="2.1082" x2="-3.6068" y2="2.1082" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.6256" x2="-3.6068" y2="1.8796" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.8796" x2="-4.5974" y2="1.8796" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.8796" x2="-4.5974" y2="1.6256" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.6256" x2="-3.6068" y2="1.6256" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.1176" x2="-3.6068" y2="1.397" width="0" layer="51"/>
<wire x1="-3.6068" y1="1.397" x2="-4.5974" y2="1.397" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.397" x2="-4.5974" y2="1.1176" width="0" layer="51"/>
<wire x1="-4.5974" y1="1.1176" x2="-3.6068" y2="1.1176" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.6096" x2="-3.6068" y2="0.889" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.889" x2="-4.5974" y2="0.889" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.889" x2="-4.5974" y2="0.6096" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.6096" x2="-3.6068" y2="0.6096" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.127" x2="-3.6068" y2="0.381" width="0" layer="51"/>
<wire x1="-3.6068" y1="0.381" x2="-4.5974" y2="0.381" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.381" x2="-4.5974" y2="0.127" width="0" layer="51"/>
<wire x1="-4.5974" y1="0.127" x2="-3.6068" y2="0.127" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.381" x2="-3.6068" y2="-0.127" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.127" x2="-4.5974" y2="-0.127" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.127" x2="-4.5974" y2="-0.381" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.381" x2="-3.6068" y2="-0.381" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.889" x2="-3.6068" y2="-0.6096" width="0" layer="51"/>
<wire x1="-3.6068" y1="-0.6096" x2="-4.5974" y2="-0.6096" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.6096" x2="-4.5974" y2="-0.889" width="0" layer="51"/>
<wire x1="-4.5974" y1="-0.889" x2="-3.6068" y2="-0.889" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.397" x2="-3.6068" y2="-1.1176" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.1176" x2="-4.5974" y2="-1.1176" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.1176" x2="-4.5974" y2="-1.397" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.397" x2="-3.6068" y2="-1.397" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.8796" x2="-3.6068" y2="-1.6256" width="0" layer="51"/>
<wire x1="-3.6068" y1="-1.6256" x2="-4.5974" y2="-1.6256" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.6256" x2="-4.5974" y2="-1.8796" width="0" layer="51"/>
<wire x1="-4.5974" y1="-1.8796" x2="-3.6068" y2="-1.8796" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.3876" x2="-3.6068" y2="-2.1082" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.1082" x2="-4.5974" y2="-2.1082" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.1082" x2="-4.5974" y2="-2.3876" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.3876" x2="-3.6068" y2="-2.3876" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.8956" x2="-3.6068" y2="-2.6162" width="0" layer="51"/>
<wire x1="-3.6068" y1="-2.6162" x2="-4.5974" y2="-2.6162" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.6162" x2="-4.5974" y2="-2.8956" width="0" layer="51"/>
<wire x1="-4.5974" y1="-2.8956" x2="-3.6068" y2="-2.8956" width="0" layer="51"/>
<wire x1="-2.6162" y1="-3.6068" x2="-2.8956" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.8956" y1="-3.6068" x2="-2.8956" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.8956" y1="-4.5974" x2="-2.6162" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.6162" y1="-4.5974" x2="-2.6162" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.1082" y1="-3.6068" x2="-2.3876" y2="-3.6068" width="0" layer="51"/>
<wire x1="-2.3876" y1="-3.6068" x2="-2.3876" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.3876" y1="-4.5974" x2="-2.1082" y2="-4.5974" width="0" layer="51"/>
<wire x1="-2.1082" y1="-4.5974" x2="-2.1082" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.6256" y1="-3.6068" x2="-1.8796" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.8796" y1="-3.6068" x2="-1.8796" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.8796" y1="-4.5974" x2="-1.6256" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.6256" y1="-4.5974" x2="-1.6256" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.1176" y1="-3.6068" x2="-1.397" y2="-3.6068" width="0" layer="51"/>
<wire x1="-1.397" y1="-3.6068" x2="-1.397" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.397" y1="-4.5974" x2="-1.1176" y2="-4.5974" width="0" layer="51"/>
<wire x1="-1.1176" y1="-4.5974" x2="-1.1176" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.6096" y1="-3.6068" x2="-0.889" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.889" y1="-3.6068" x2="-0.889" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.889" y1="-4.5974" x2="-0.6096" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.6096" y1="-4.5974" x2="-0.6096" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.127" y1="-3.6068" x2="-0.381" y2="-3.6068" width="0" layer="51"/>
<wire x1="-0.381" y1="-3.6068" x2="-0.381" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.381" y1="-4.5974" x2="-0.127" y2="-4.5974" width="0" layer="51"/>
<wire x1="-0.127" y1="-4.5974" x2="-0.127" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.381" y1="-3.6068" x2="0.127" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.127" y1="-3.6068" x2="0.127" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.127" y1="-4.5974" x2="0.381" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.381" y1="-4.5974" x2="0.381" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.889" y1="-3.6068" x2="0.6096" y2="-3.6068" width="0" layer="51"/>
<wire x1="0.6096" y1="-3.6068" x2="0.6096" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.6096" y1="-4.5974" x2="0.889" y2="-4.5974" width="0" layer="51"/>
<wire x1="0.889" y1="-4.5974" x2="0.889" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.397" y1="-3.6068" x2="1.1176" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.1176" y1="-3.6068" x2="1.1176" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.1176" y1="-4.5974" x2="1.397" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.397" y1="-4.5974" x2="1.397" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.8796" y1="-3.6068" x2="1.6256" y2="-3.6068" width="0" layer="51"/>
<wire x1="1.6256" y1="-3.6068" x2="1.6256" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.6256" y1="-4.5974" x2="1.8796" y2="-4.5974" width="0" layer="51"/>
<wire x1="1.8796" y1="-4.5974" x2="1.8796" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.3876" y1="-3.6068" x2="2.1082" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.1082" y1="-3.6068" x2="2.1082" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.1082" y1="-4.5974" x2="2.3876" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.3876" y1="-4.5974" x2="2.3876" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.8956" y1="-3.6068" x2="2.6162" y2="-3.6068" width="0" layer="51"/>
<wire x1="2.6162" y1="-3.6068" x2="2.6162" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.6162" y1="-4.5974" x2="2.8956" y2="-4.5974" width="0" layer="51"/>
<wire x1="2.8956" y1="-4.5974" x2="2.8956" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.6162" x2="3.6068" y2="-2.8956" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.8956" x2="4.5974" y2="-2.8956" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.8956" x2="4.5974" y2="-2.6162" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.6162" x2="3.6068" y2="-2.6162" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.1082" x2="3.6068" y2="-2.3876" width="0" layer="51"/>
<wire x1="3.6068" y1="-2.3876" x2="4.5974" y2="-2.3876" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.3876" x2="4.5974" y2="-2.1082" width="0" layer="51"/>
<wire x1="4.5974" y1="-2.1082" x2="3.6068" y2="-2.1082" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.6256" x2="3.6068" y2="-1.8796" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.8796" x2="4.5974" y2="-1.8796" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.8796" x2="4.5974" y2="-1.6256" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.6256" x2="3.6068" y2="-1.6256" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.1176" x2="3.6068" y2="-1.397" width="0" layer="51"/>
<wire x1="3.6068" y1="-1.397" x2="4.5974" y2="-1.397" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.397" x2="4.5974" y2="-1.1176" width="0" layer="51"/>
<wire x1="4.5974" y1="-1.1176" x2="3.6068" y2="-1.1176" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.6096" x2="3.6068" y2="-0.889" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.889" x2="4.5974" y2="-0.889" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.889" x2="4.5974" y2="-0.6096" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.6096" x2="3.6068" y2="-0.6096" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.127" x2="3.6068" y2="-0.381" width="0" layer="51"/>
<wire x1="3.6068" y1="-0.381" x2="4.5974" y2="-0.381" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.381" x2="4.5974" y2="-0.127" width="0" layer="51"/>
<wire x1="4.5974" y1="-0.127" x2="3.6068" y2="-0.127" width="0" layer="51"/>
<wire x1="3.6068" y1="0.381" x2="3.6068" y2="0.127" width="0" layer="51"/>
<wire x1="3.6068" y1="0.127" x2="4.5974" y2="0.127" width="0" layer="51"/>
<wire x1="4.5974" y1="0.127" x2="4.5974" y2="0.381" width="0" layer="51"/>
<wire x1="4.5974" y1="0.381" x2="3.6068" y2="0.381" width="0" layer="51"/>
<wire x1="3.6068" y1="0.889" x2="3.6068" y2="0.6096" width="0" layer="51"/>
<wire x1="3.6068" y1="0.6096" x2="4.5974" y2="0.6096" width="0" layer="51"/>
<wire x1="4.5974" y1="0.6096" x2="4.5974" y2="0.889" width="0" layer="51"/>
<wire x1="4.5974" y1="0.889" x2="3.6068" y2="0.889" width="0" layer="51"/>
<wire x1="3.6068" y1="1.397" x2="3.6068" y2="1.1176" width="0" layer="51"/>
<wire x1="3.6068" y1="1.1176" x2="4.5974" y2="1.1176" width="0" layer="51"/>
<wire x1="4.5974" y1="1.1176" x2="4.5974" y2="1.397" width="0" layer="51"/>
<wire x1="4.5974" y1="1.397" x2="3.6068" y2="1.397" width="0" layer="51"/>
<wire x1="3.6068" y1="1.8796" x2="3.6068" y2="1.6256" width="0" layer="51"/>
<wire x1="3.6068" y1="1.6256" x2="4.5974" y2="1.6256" width="0" layer="51"/>
<wire x1="4.5974" y1="1.6256" x2="4.5974" y2="1.8796" width="0" layer="51"/>
<wire x1="4.5974" y1="1.8796" x2="3.6068" y2="1.8796" width="0" layer="51"/>
<wire x1="3.6068" y1="2.3876" x2="3.6068" y2="2.1082" width="0" layer="51"/>
<wire x1="3.6068" y1="2.1082" x2="4.5974" y2="2.1082" width="0" layer="51"/>
<wire x1="4.5974" y1="2.1082" x2="4.5974" y2="2.3876" width="0" layer="51"/>
<wire x1="4.5974" y1="2.3876" x2="3.6068" y2="2.3876" width="0" layer="51"/>
<wire x1="3.6068" y1="2.8956" x2="3.6068" y2="2.6162" width="0" layer="51"/>
<wire x1="3.6068" y1="2.6162" x2="4.5974" y2="2.6162" width="0" layer="51"/>
<wire x1="4.5974" y1="2.6162" x2="4.5974" y2="2.8956" width="0" layer="51"/>
<wire x1="4.5974" y1="2.8956" x2="3.6068" y2="2.8956" width="0" layer="51"/>
<wire x1="-3.6068" y1="2.3368" x2="-2.3368" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="-3.6068" x2="3.6068" y2="-3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="-3.6068" x2="3.6068" y2="3.6068" width="0" layer="51"/>
<wire x1="3.6068" y1="3.6068" x2="-3.6068" y2="3.6068" width="0" layer="51"/>
<wire x1="-3.6068" y1="3.6068" x2="-3.6068" y2="-3.6068" width="0" layer="51"/>
<text x="-6.1722" y="2.7432" size="1.27" layer="51" ratio="6" rot="SR0">*</text>
<text x="-3.4544" y="6.985" size="2.0828" layer="25" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.4544" y="-9.525" size="2.0828" layer="27" ratio="10" rot="SR0">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="TLK100PHP">
<pin name="VDD33_VA11" x="-38.1" y="33.02" length="middle" direction="pwr"/>
<pin name="VDD33_IO_2" x="-38.1" y="30.48" length="middle" direction="pwr"/>
<pin name="VDD33_VD11" x="-38.1" y="27.94" length="middle" direction="pwr"/>
<pin name="VDD33_IO" x="-38.1" y="25.4" length="middle" direction="pwr"/>
<pin name="VDD33_V18" x="-38.1" y="22.86" length="middle" direction="pwr"/>
<pin name="VA11_PFBIN1" x="-38.1" y="17.78" length="middle" direction="in"/>
<pin name="VA11_PFBIN2" x="-38.1" y="15.24" length="middle" direction="in"/>
<pin name="MII_TXD_0" x="-38.1" y="10.16" length="middle" direction="in"/>
<pin name="MII_TXD_1" x="-38.1" y="7.62" length="middle" direction="in"/>
<pin name="MII_TXD_2" x="-38.1" y="5.08" length="middle" direction="in"/>
<pin name="MII_TXD_3" x="-38.1" y="2.54" length="middle" direction="in"/>
<pin name="MII_TX_EN" x="-38.1" y="0" length="middle" direction="in"/>
<pin name="PWRDNN/INT" x="-38.1" y="-2.54" length="middle" direction="in"/>
<pin name="RESETN" x="-38.1" y="-5.08" length="middle" direction="in"/>
<pin name="JTAG_TCK" x="-38.1" y="-7.62" length="middle" direction="in"/>
<pin name="JTAG_TDI" x="-38.1" y="-10.16" length="middle" direction="in"/>
<pin name="JTAG_TMS" x="-38.1" y="-12.7" length="middle" direction="in"/>
<pin name="JTAG_TRSTN" x="-38.1" y="-15.24" length="middle" direction="in"/>
<pin name="RBIAS" x="-38.1" y="-17.78" length="middle" direction="in"/>
<pin name="MDC" x="-38.1" y="-20.32" length="middle" direction="in"/>
<pin name="XI" x="-38.1" y="-22.86" length="middle" direction="in"/>
<pin name="V18_PFBIN1" x="-38.1" y="-27.94" length="middle" direction="in"/>
<pin name="V18_PFBIN2" x="-38.1" y="-30.48" length="middle" direction="in"/>
<pin name="VSS" x="-38.1" y="-35.56" length="middle" direction="pwr"/>
<pin name="EP" x="-38.1" y="-38.1" length="middle" direction="pas"/>
<pin name="VA11_PFBOUT" x="38.1" y="33.02" length="middle" direction="out" rot="R180"/>
<pin name="CLK25OUT" x="38.1" y="30.48" length="middle" direction="out" rot="R180"/>
<pin name="MII_TX_CLK" x="38.1" y="27.94" length="middle" direction="out" rot="R180"/>
<pin name="MII_COL/PHYAD0" x="38.1" y="22.86" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_0/PHYAD1" x="38.1" y="20.32" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_1/PHYAD2" x="38.1" y="17.78" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_2/PHYAD3" x="38.1" y="15.24" length="middle" direction="out" rot="R180"/>
<pin name="MII_RXD_3/PHYAD4" x="38.1" y="12.7" length="middle" direction="out" rot="R180"/>
<pin name="LED_LINK/AN_0" x="38.1" y="7.62" length="middle" direction="out" rot="R180"/>
<pin name="LED_SPEED/AN_1" x="38.1" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="XO" x="38.1" y="0" length="middle" direction="out" rot="R180"/>
<pin name="V18_PFBOUT" x="38.1" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="JTAG_TDO" x="38.1" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="VDD11" x="38.1" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="MII_CRS/LED_CFG" x="38.1" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_CLK" x="38.1" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_DV" x="38.1" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="MII_RX_ERR/MDIX_EN" x="38.1" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="LED_ACT/AN_EN" x="38.1" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="RD-" x="38.1" y="-25.4" length="middle" rot="R180"/>
<pin name="RD+" x="38.1" y="-27.94" length="middle" rot="R180"/>
<pin name="TD-" x="38.1" y="-30.48" length="middle" rot="R180"/>
<pin name="TD+" x="38.1" y="-33.02" length="middle" rot="R180"/>
<pin name="MDIO" x="38.1" y="-35.56" length="middle" rot="R180"/>
<wire x1="-33.02" y1="38.1" x2="-33.02" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="-33.02" y1="-43.18" x2="33.02" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="33.02" y1="-43.18" x2="33.02" y2="38.1" width="0.4064" layer="94"/>
<wire x1="33.02" y1="38.1" x2="-33.02" y2="38.1" width="0.4064" layer="94"/>
<text x="-7.6454" y="39.7764" size="2.0828" layer="95" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-5.9182" y="-46.5074" size="2.0828" layer="96" ratio="10" rot="SR0">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TLK100PHP" prefix="U">
<description>Ethernet Physical Layer Transceiver</description>
<gates>
<gate name="A" symbol="TLK100PHP" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP50P900X900X120-49N">
<connects>
<connect gate="A" pin="CLK25OUT" pad="12"/>
<connect gate="A" pin="EP" pad="49"/>
<connect gate="A" pin="JTAG_TCK" pad="44"/>
<connect gate="A" pin="JTAG_TDI" pad="45"/>
<connect gate="A" pin="JTAG_TDO" pad="47"/>
<connect gate="A" pin="JTAG_TMS" pad="46"/>
<connect gate="A" pin="JTAG_TRSTN" pad="48"/>
<connect gate="A" pin="LED_ACT/AN_EN" pad="34"/>
<connect gate="A" pin="LED_LINK/AN_0" pad="36"/>
<connect gate="A" pin="LED_SPEED/AN_1" pad="35"/>
<connect gate="A" pin="MDC" pad="32"/>
<connect gate="A" pin="MDIO" pad="33"/>
<connect gate="A" pin="MII_COL/PHYAD0" pad="24"/>
<connect gate="A" pin="MII_CRS/LED_CFG" pad="22"/>
<connect gate="A" pin="MII_RXD_0/PHYAD1" pad="25"/>
<connect gate="A" pin="MII_RXD_1/PHYAD2" pad="26"/>
<connect gate="A" pin="MII_RXD_2/PHYAD3" pad="27"/>
<connect gate="A" pin="MII_RXD_3/PHYAD4" pad="28"/>
<connect gate="A" pin="MII_RX_CLK" pad="23"/>
<connect gate="A" pin="MII_RX_DV" pad="30"/>
<connect gate="A" pin="MII_RX_ERR/MDIX_EN" pad="31"/>
<connect gate="A" pin="MII_TXD_0" pad="13"/>
<connect gate="A" pin="MII_TXD_1" pad="14"/>
<connect gate="A" pin="MII_TXD_2" pad="15"/>
<connect gate="A" pin="MII_TXD_3" pad="16"/>
<connect gate="A" pin="MII_TX_CLK" pad="19"/>
<connect gate="A" pin="MII_TX_EN" pad="18"/>
<connect gate="A" pin="PWRDNN/INT" pad="42"/>
<connect gate="A" pin="RBIAS" pad="3"/>
<connect gate="A" pin="RD+" pad="6"/>
<connect gate="A" pin="RD-" pad="5"/>
<connect gate="A" pin="RESETN" pad="43"/>
<connect gate="A" pin="TD+" pad="9"/>
<connect gate="A" pin="TD-" pad="8"/>
<connect gate="A" pin="V18_PFBIN1" pad="2"/>
<connect gate="A" pin="V18_PFBIN2" pad="4"/>
<connect gate="A" pin="V18_PFBOUT" pad="40"/>
<connect gate="A" pin="VA11_PFBIN1" pad="1"/>
<connect gate="A" pin="VA11_PFBIN2" pad="7"/>
<connect gate="A" pin="VA11_PFBOUT" pad="10"/>
<connect gate="A" pin="VDD11" pad="20"/>
<connect gate="A" pin="VDD33_IO" pad="29"/>
<connect gate="A" pin="VDD33_IO_2" pad="17"/>
<connect gate="A" pin="VDD33_V18" pad="41"/>
<connect gate="A" pin="VDD33_VA11" pad="11"/>
<connect gate="A" pin="VDD33_VD11" pad="21"/>
<connect gate="A" pin="VSS" pad="38"/>
<connect gate="A" pin="XI" pad="39"/>
<connect gate="A" pin="XO" pad="37"/>
</connects>
<technologies>
<technology name="">
<attribute name="MPN" value="TLK100PHP" constant="no"/>
<attribute name="OC_FARNELL" value="-" constant="no"/>
<attribute name="OC_NEWARK" value="26R5216" constant="no"/>
<attribute name="PACKAGE" value="HTQFP-48" constant="no"/>
<attribute name="SUPPLIER" value="Texas Instruments" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="analog-adis">
<packages>
<package name="ADIS16485">
<wire x1="-6.6" y1="-21.05" x2="-6.6" y2="-18.45" width="0.127" layer="21"/>
<wire x1="-6.6" y1="-18.45" x2="6.6" y2="-18.45" width="0.127" layer="21"/>
<wire x1="6.6" y1="-18.45" x2="6.6" y2="-21.05" width="0.127" layer="21"/>
<wire x1="6.6" y1="-21.05" x2="-6.6" y2="-21.05" width="0.127" layer="21"/>
<smd name="13" x="-0.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="15" x="-1.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="17" x="-2.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="19" x="-3.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="21" x="-4.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="23" x="-5.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="14" x="-0.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="16" x="-1.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="18" x="-2.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="20" x="-3.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="22" x="-4.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="24" x="-5.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="11" x="0.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="9" x="1.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="7" x="2.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="5" x="3.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="3" x="4.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="1" x="5.5" y="-21.35" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="2" x="5.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="4" x="4.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="6" x="3.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="8" x="2.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="10" x="1.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<smd name="12" x="0.5" y="-18.15" dx="1.37" dy="0.61" layer="1" rot="R270"/>
<wire x1="-22" y1="23.5" x2="22" y2="23.5" width="0.127" layer="21"/>
<wire x1="22" y1="23.5" x2="22" y2="-23.5" width="0.127" layer="21"/>
<wire x1="22" y1="-23.5" x2="-22" y2="-23.5" width="0.127" layer="21"/>
<wire x1="-22" y1="-23.5" x2="-22" y2="23.5" width="0.127" layer="21"/>
<hole x="19.8" y="-21.3" drill="3"/>
<hole x="-19.8" y="-21.3" drill="3"/>
<hole x="-19.8" y="21.3" drill="3"/>
<hole x="19.8" y="21.3" drill="3"/>
<wire x1="-22" y1="23.6" x2="22" y2="23.6" width="0.127" layer="39"/>
<wire x1="22" y1="23.6" x2="22" y2="-23.6" width="0.127" layer="39"/>
<wire x1="22" y1="-23.6" x2="-22" y2="-23.6" width="0.127" layer="39"/>
<wire x1="-22" y1="-23.6" x2="-22" y2="23.6" width="0.127" layer="39"/>
<rectangle x1="-22" y1="-24" x2="-17" y2="-19" layer="42"/>
<rectangle x1="17" y1="-24" x2="22" y2="-19" layer="42"/>
<rectangle x1="17" y1="19" x2="22" y2="24" layer="42"/>
<rectangle x1="-22" y1="19" x2="-17" y2="24" layer="42"/>
</package>
</packages>
<symbols>
<symbol name="ADIS16485">
<wire x1="-17.78" y1="25.4" x2="-17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="17.78" y1="-22.86" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="-17.78" y2="25.4" width="0.254" layer="94"/>
<pin name="SCLK" x="-22.86" y="12.7" length="middle" direction="in"/>
<pin name="DOUT" x="-22.86" y="7.62" length="middle" direction="out"/>
<pin name="DIN" x="-22.86" y="2.54" length="middle" direction="in"/>
<pin name="/CS" x="-22.86" y="-2.54" length="middle" direction="in"/>
<pin name="DIO1" x="22.86" y="5.08" length="middle" rot="R180"/>
<pin name="DIO2" x="22.86" y="0" length="middle" rot="R180"/>
<pin name="/RST" x="-22.86" y="-17.78" length="middle" direction="in"/>
<pin name="VCC1" x="0" y="30.48" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC2" x="2.54" y="30.48" length="middle" direction="pwr" rot="R270"/>
<pin name="VCC3" x="5.08" y="30.48" length="middle" direction="pwr" rot="R270"/>
<pin name="GND1" x="0" y="-27.94" length="middle" direction="pwr" rot="R90"/>
<pin name="GND2" x="2.54" y="-27.94" length="middle" direction="pwr" rot="R90"/>
<pin name="GND3" x="5.08" y="-27.94" length="middle" direction="pwr" rot="R90"/>
<pin name="DIO3" x="22.86" y="-5.08" length="middle" rot="R180"/>
<pin name="DIO4" x="22.86" y="-10.16" length="middle" rot="R180"/>
<pin name="VDDRTC" x="-7.62" y="30.48" length="middle" direction="pwr" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ADIS16485">
<gates>
<gate name="G$1" symbol="ADIS16485" x="0" y="0"/>
</gates>
<devices>
<device name="MODULE" package="ADIS16485">
<connects>
<connect gate="G$1" pin="/CS" pad="6"/>
<connect gate="G$1" pin="/RST" pad="8"/>
<connect gate="G$1" pin="DIN" pad="5"/>
<connect gate="G$1" pin="DIO1" pad="7"/>
<connect gate="G$1" pin="DIO2" pad="9"/>
<connect gate="G$1" pin="DIO3" pad="1"/>
<connect gate="G$1" pin="DIO4" pad="2"/>
<connect gate="G$1" pin="DOUT" pad="4"/>
<connect gate="G$1" pin="GND1" pad="13"/>
<connect gate="G$1" pin="GND2" pad="14"/>
<connect gate="G$1" pin="GND3" pad="15"/>
<connect gate="G$1" pin="SCLK" pad="3"/>
<connect gate="G$1" pin="VCC1" pad="10"/>
<connect gate="G$1" pin="VCC2" pad="11"/>
<connect gate="G$1" pin="VCC3" pad="12"/>
<connect gate="G$1" pin="VDDRTC" pad="23"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="MAGJACK-POE">
<packages>
<package name="MAGJACK-POE">
<description>&lt;h3&gt;PoE Compatible MagJack&lt;/h3&gt;
This RJ-45 jack with magnetics has 10 pins, gives you access to the unused twisted pair (10BASE-T, and 100BASE-T) which usually carries power in a power-over-ethernet system.
&lt;br&gt;
&lt;br&gt;
&lt;b&gt;Footprint is Proven&lt;/b&gt;</description>
<wire x1="-8.05" y1="-1.7" x2="-8.05" y2="10.8" width="0.1778" layer="21"/>
<wire x1="-8.05" y1="10.8" x2="8.05" y2="10.8" width="0.1778" layer="21"/>
<wire x1="8.05" y1="10.8" x2="8.05" y2="-1.7" width="0.1778" layer="21"/>
<wire x1="9.24" y1="3.98" x2="8.7868" y2="5.3192" width="0.1778" layer="51" curve="-59.7562"/>
<wire x1="8.7868" y1="5.3192" x2="9.34" y2="9.31" width="0.1778" layer="51"/>
<wire x1="9.34" y1="9.31" x2="8.05" y2="10.8" width="0.1778" layer="51" curve="89.9923"/>
<wire x1="-9.24" y1="3.98" x2="-8.7868" y2="5.3192" width="0.1778" layer="51" curve="59.7562"/>
<wire x1="-8.7868" y1="5.3192" x2="-9.34" y2="9.31" width="0.1778" layer="51"/>
<wire x1="-9.34" y1="9.31" x2="-8.05" y2="10.8" width="0.1778" layer="51" curve="-89.9923"/>
<wire x1="8.05" y1="-10.8" x2="-8.05" y2="-10.8" width="0.1778" layer="21"/>
<wire x1="-8.05" y1="-10.8" x2="-8.05" y2="-4.7" width="0.1778" layer="21"/>
<wire x1="8.05" y1="-10.8" x2="8.05" y2="-4.7" width="0.1778" layer="21"/>
<pad name="GND0" x="-7.745" y="-3.17" drill="1.63"/>
<pad name="GND1" x="7.745" y="-3.17" drill="1.63"/>
<pad name="5" x="0.64" y="-8.89" drill="0.89"/>
<pad name="4" x="1.91" y="-6.35" drill="0.89"/>
<pad name="2" x="4.45" y="-6.35" drill="0.89"/>
<pad name="6" x="-0.63" y="-6.35" drill="0.89"/>
<pad name="8" x="-3.17" y="-6.35" drill="0.89"/>
<pad name="10" x="-5.71" y="-6.35" drill="0.89"/>
<pad name="3" x="3.18" y="-8.89" drill="0.89"/>
<pad name="1" x="5.72" y="-8.89" drill="0.89"/>
<pad name="7" x="-1.9" y="-8.89" drill="0.89"/>
<pad name="9" x="-4.44" y="-8.89" drill="0.89"/>
<pad name="14" x="-6.625" y="4.08" drill="1.02"/>
<pad name="11" x="6.625" y="4.08" drill="1.02"/>
<pad name="12" x="4.085" y="4.08" drill="1.02"/>
<pad name="13" x="-4.085" y="4.08" drill="1.02"/>
<hole x="-5.715" y="-0.12" drill="3.25"/>
<hole x="5.715" y="-0.12" drill="3.25"/>
</package>
</packages>
<symbols>
<symbol name="MAGJACK-POE">
<wire x1="-12.7" y1="-17.78" x2="12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="-12.7" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="12.7" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="17.78" x2="-12.7" y2="17.78" width="0.254" layer="94"/>
<wire x1="-12.7" y1="17.78" x2="-12.7" y2="-17.78" width="0.254" layer="94"/>
<wire x1="12.7" y1="12.7" x2="10.668" y2="12.7" width="0.254" layer="94"/>
<wire x1="10.668" y1="12.7" x2="10.668" y2="9.652" width="0.254" layer="94"/>
<wire x1="10.668" y1="9.652" x2="9.398" y2="9.652" width="0.254" layer="94"/>
<wire x1="9.398" y1="9.652" x2="10.668" y2="8.382" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="11.938" y2="9.652" width="0.254" layer="94"/>
<wire x1="11.938" y1="9.652" x2="10.668" y2="9.652" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="11.938" y2="8.382" width="0.254" layer="94"/>
<wire x1="9.398" y1="8.382" x2="10.668" y2="8.382" width="0.254" layer="94"/>
<wire x1="10.668" y1="8.382" x2="10.668" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.668" y1="5.08" x2="12.7" y2="5.08" width="0.254" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="10.668" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.668" y1="-5.08" x2="10.668" y2="-8.128" width="0.254" layer="94"/>
<wire x1="10.668" y1="-8.128" x2="9.398" y2="-8.128" width="0.254" layer="94"/>
<wire x1="9.398" y1="-8.128" x2="10.668" y2="-9.398" width="0.254" layer="94"/>
<wire x1="10.668" y1="-9.398" x2="11.938" y2="-8.128" width="0.254" layer="94"/>
<wire x1="11.938" y1="-8.128" x2="10.668" y2="-8.128" width="0.254" layer="94"/>
<wire x1="10.668" y1="-9.398" x2="11.938" y2="-9.398" width="0.254" layer="94"/>
<wire x1="9.398" y1="-9.398" x2="10.668" y2="-9.398" width="0.254" layer="94"/>
<wire x1="10.668" y1="-9.398" x2="10.668" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.668" y1="-12.7" x2="12.7" y2="-12.7" width="0.254" layer="94"/>
<wire x1="8.89" y1="8.89" x2="8.128" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.128" y1="9.144" x2="8.128" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.128" y1="9.652" x2="8.636" y2="9.652" width="0.254" layer="94"/>
<wire x1="8.89" y1="7.874" x2="8.128" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.128" y1="8.128" x2="8.128" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.128" y1="8.636" x2="8.636" y2="8.636" width="0.254" layer="94"/>
<wire x1="8.89" y1="-8.89" x2="8.128" y2="-8.128" width="0.254" layer="94"/>
<wire x1="8.128" y1="-8.636" x2="8.128" y2="-8.128" width="0.254" layer="94"/>
<wire x1="8.128" y1="-8.128" x2="8.636" y2="-8.128" width="0.254" layer="94"/>
<wire x1="8.89" y1="-9.906" x2="8.128" y2="-9.144" width="0.254" layer="94"/>
<wire x1="8.128" y1="-9.652" x2="8.128" y2="-9.144" width="0.254" layer="94"/>
<wire x1="8.128" y1="-9.144" x2="8.636" y2="-9.144" width="0.254" layer="94"/>
<text x="-12.7" y="18.288" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-20.066" size="1.778" layer="96">&gt;VALUE</text>
<pin name="YLED-" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="YLED+" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="GLED-" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="GLED+" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="RD-" x="-17.78" y="-7.62" length="middle"/>
<pin name="RCT" x="-17.78" y="-5.08" length="middle"/>
<pin name="RD+" x="-17.78" y="-2.54" length="middle"/>
<pin name="TD-" x="-17.78" y="2.54" length="middle"/>
<pin name="TCT" x="-17.78" y="5.08" length="middle"/>
<pin name="TD+" x="-17.78" y="7.62" length="middle"/>
<pin name="SHIELD@0" x="-17.78" y="-12.7" length="middle"/>
<pin name="SHIELD@1" x="-17.78" y="-15.24" length="middle"/>
<pin name="V+" x="-17.78" y="15.24" length="middle"/>
<pin name="V-" x="-17.78" y="12.7" length="middle"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MAGJACK-POE" prefix="U">
<description>&lt;h3&gt;PoE compatible MagJack&lt;/h3&gt;</description>
<gates>
<gate name="G$1" symbol="MAGJACK-POE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MAGJACK-POE">
<connects>
<connect gate="G$1" pin="GLED+" pad="13"/>
<connect gate="G$1" pin="GLED-" pad="14"/>
<connect gate="G$1" pin="RCT" pad="3"/>
<connect gate="G$1" pin="RD+" pad="1"/>
<connect gate="G$1" pin="RD-" pad="2"/>
<connect gate="G$1" pin="SHIELD@0" pad="GND0"/>
<connect gate="G$1" pin="SHIELD@1" pad="GND1"/>
<connect gate="G$1" pin="TCT" pad="4"/>
<connect gate="G$1" pin="TD+" pad="5"/>
<connect gate="G$1" pin="TD-" pad="6"/>
<connect gate="G$1" pin="V+" pad="9"/>
<connect gate="G$1" pin="V-" pad="10"/>
<connect gate="G$1" pin="YLED+" pad="11"/>
<connect gate="G$1" pin="YLED-" pad="12"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="LTC4257_">
<packages>
<package name="SO08">
<description>&lt;b&gt;Small Outline IC&lt;/b&gt;</description>
<wire x1="2.4" y1="1.9" x2="2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.9" x2="-2.4" y2="-1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.9" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="-1.4" x2="-2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="-2.4" y1="1.9" x2="2.4" y2="1.9" width="0.2032" layer="51"/>
<wire x1="2.4" y1="-1.4" x2="-2.4" y2="-1.4" width="0.2032" layer="51"/>
<text x="-2.667" y="-1.905" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="3.937" y="-1.905" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.15" y1="-3.1" x2="-1.66" y2="-2" layer="51"/>
<rectangle x1="-0.88" y1="-3.1" x2="-0.39" y2="-2" layer="51"/>
<rectangle x1="0.39" y1="-3.1" x2="0.88" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="-3.1" x2="2.15" y2="-2" layer="51"/>
<rectangle x1="1.66" y1="2" x2="2.15" y2="3.1" layer="51"/>
<rectangle x1="0.39" y1="2" x2="0.88" y2="3.1" layer="51"/>
<rectangle x1="-0.88" y1="2" x2="-0.39" y2="3.1" layer="51"/>
<rectangle x1="-2.15" y1="2" x2="-1.66" y2="3.1" layer="51"/>
<smd name="2" x="-0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="7" x="-0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="1" x="-1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="3" x="0.635" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="4" x="1.905" y="-2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="8" x="-1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="6" x="0.635" y="2.6" dx="0.6" dy="2.2" layer="1"/>
<smd name="5" x="1.905" y="2.6" dx="0.6" dy="2.2" layer="1"/>
</package>
</packages>
<symbols>
<symbol name="LTC4257">
<wire x1="-7.62" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="-7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="-5.08" width="0.4064" layer="94"/>
<text x="-7.62" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="RC" x="-12.7" y="2.54" length="middle" direction="pas"/>
<pin name="VIN" x="-12.7" y="-2.54" length="middle" direction="in"/>
<pin name="VOUT" x="12.7" y="-2.54" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="12.7" y="2.54" length="middle" direction="pas" rot="R180"/>
<pin name="/PGD" x="12.7" y="0" length="middle" direction="oc" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LTC4257*" prefix="IC">
<description>IEEE 802.3af PD Power over Ethernet Interface Controller</description>
<gates>
<gate name="G$1" symbol="LTC4257" x="0" y="0"/>
</gates>
<devices>
<device name="S8" package="SO08">
<connects>
<connect gate="G$1" pin="/PGD" pad="6"/>
<connect gate="G$1" pin="GND" pad="8"/>
<connect gate="G$1" pin="RC" pad="2"/>
<connect gate="G$1" pin="VIN" pad="4"/>
<connect gate="G$1" pin="VOUT" pad="5"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="xppower">
<packages>
<package name="SIP8">
<pad name="P$1" x="-8.89" y="0" drill="1.07"/>
<pad name="P$2" x="-6.35" y="0" drill="1.07"/>
<pad name="P$3" x="-3.81" y="0" drill="1.07"/>
<pad name="P$5" x="1.27" y="0" drill="1.07"/>
<pad name="P$6" x="3.81" y="0" drill="1.07"/>
<pad name="P$7" x="6.35" y="0" drill="1.07"/>
<pad name="P$8" x="8.89" y="0" drill="1.07"/>
<wire x1="-10.922" y1="3.302" x2="10.922" y2="3.302" width="0.127" layer="21"/>
<wire x1="10.922" y1="3.302" x2="10.922" y2="-5.842" width="0.127" layer="21"/>
<wire x1="10.922" y1="-5.842" x2="-10.922" y2="-5.842" width="0.127" layer="21"/>
<wire x1="-10.922" y1="-5.842" x2="-10.922" y2="3.302" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="RS">
<pin name="VIN+" x="-15.24" y="5.08" length="middle" direction="pas"/>
<pin name="VIN-" x="-15.24" y="-5.08" length="middle" direction="pas"/>
<pin name="VOUT+" x="15.24" y="5.08" length="middle" direction="pas" rot="R180"/>
<pin name="VOUT-" x="15.24" y="-5.08" length="middle" direction="pas" rot="R180"/>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="10.16" y2="-15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="10.16" y1="10.16" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="12.7" size="1.27" layer="94">&gt;NAME</text>
<text x="-10.16" y="10.16" size="1.27" layer="94">&gt;VALUE</text>
<pin name="CTRL" x="-15.24" y="-12.7" length="middle" direction="in"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="IU4824SA">
<gates>
<gate name="G$1" symbol="RS" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SIP8">
<connects>
<connect gate="G$1" pin="CTRL" pad="P$3"/>
<connect gate="G$1" pin="VIN+" pad="P$2"/>
<connect gate="G$1" pin="VIN-" pad="P$1"/>
<connect gate="G$1" pin="VOUT+" pad="P$6"/>
<connect gate="G$1" pin="VOUT-" pad="P$7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="1" name="pwrx" width="0" drill="0">
</class>
<class number="2" name="highpwr" width="0" drill="0.8128">
</class>
<class number="3" name="highsig" width="0" drill="0.8128">
<clearance class="3" value="0.254"/>
</class>
<class number="7" name="hipwr" width="0" drill="0">
<clearance class="7" value="0.2032"/>
</class>
</classes>
<parts>
<part name="C16" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C19" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C20" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C18" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V6" library="supply1" deviceset="+3V3" device=""/>
<part name="C15" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C2" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="C3" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="GND28" library="supply1" deviceset="GND" device=""/>
<part name="GND29" library="supply1" deviceset="GND" device=""/>
<part name="GND30" library="supply1" deviceset="GND" device=""/>
<part name="GND31" library="supply1" deviceset="GND" device=""/>
<part name="GND32" library="supply1" deviceset="GND" device=""/>
<part name="GND33" library="supply1" deviceset="GND" device=""/>
<part name="GND34" library="supply1" deviceset="GND" device=""/>
<part name="GND35" library="supply1" deviceset="GND" device=""/>
<part name="GND36" library="supply1" deviceset="GND" device=""/>
<part name="C6" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND53" library="supply1" deviceset="GND" device=""/>
<part name="JTAG1" library="con-lstb" deviceset="MA05-2" device="" value="67996-110HLF"/>
<part name="GND39" library="supply1" deviceset="GND" device=""/>
<part name="GND41" library="supply1" deviceset="GND" device=""/>
<part name="+3V15" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V56" library="supply1" deviceset="+3V3" device=""/>
<part name="R1" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="U1" library="Atmel_By_element14_Batch_1" deviceset="AT32UC3A0256-ALUT" device="" value="AT32UC3A0512-ALUT"/>
<part name="+3V8" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V16" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V17" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V18" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V19" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V20" library="supply1" deviceset="+3V3" device=""/>
<part name="GND17" library="supply1" deviceset="GND" device=""/>
<part name="GND25" library="supply1" deviceset="GND" device=""/>
<part name="GND26" library="supply1" deviceset="GND" device=""/>
<part name="GND77" library="supply1" deviceset="GND" device=""/>
<part name="GND78" library="supply1" deviceset="GND" device=""/>
<part name="GND79" library="supply1" deviceset="GND" device=""/>
<part name="GND80" library="supply1" deviceset="GND" device=""/>
<part name="GND81" library="supply1" deviceset="GND" device=""/>
<part name="GND82" library="supply1" deviceset="GND" device=""/>
<part name="GND83" library="supply1" deviceset="GND" device=""/>
<part name="+3V21" library="supply1" deviceset="+3V3" device=""/>
<part name="VCC4" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC5" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC6" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC7" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC8" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC1" library="supply1" deviceset="VCCINT" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V9" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V10" library="supply1" deviceset="+3V3" device=""/>
<part name="VCC2" library="supply1" deviceset="VCCINT" device=""/>
<part name="VCC3" library="supply1" deviceset="VCCINT" device=""/>
<part name="+3V11" library="supply1" deviceset="+3V3" device=""/>
<part name="C11" library="rcl" deviceset="C-US" device="C0402" value="4.7uF"/>
<part name="C12" library="rcl" deviceset="C-US" device="C0402" value="1nF"/>
<part name="C31" library="rcl" deviceset="C-US" device="C0402" value="2.2uF"/>
<part name="C32" library="rcl" deviceset="C-US" device="C0402" value="470pF"/>
<part name="GND85" library="supply1" deviceset="GND" device=""/>
<part name="GND87" library="supply1" deviceset="GND" device=""/>
<part name="GND90" library="supply1" deviceset="GND" device=""/>
<part name="GND91" library="supply1" deviceset="GND" device=""/>
<part name="C34" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC10" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND93" library="supply1" deviceset="GND" device=""/>
<part name="C35" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC11" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND94" library="supply1" deviceset="GND" device=""/>
<part name="C36" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC12" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND95" library="supply1" deviceset="GND" device=""/>
<part name="+3V12" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V14" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V22" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V23" library="supply1" deviceset="+3V3" device=""/>
<part name="GND99" library="supply1" deviceset="GND" device=""/>
<part name="GND100" library="supply1" deviceset="GND" device=""/>
<part name="GND102" library="supply1" deviceset="GND" device=""/>
<part name="GND103" library="supply1" deviceset="GND" device=""/>
<part name="GND104" library="supply1" deviceset="GND" device=""/>
<part name="C52" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC13" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND156" library="supply1" deviceset="GND" device=""/>
<part name="C1" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V25" library="supply1" deviceset="+3V3" device=""/>
<part name="GND149" library="supply1" deviceset="GND" device=""/>
<part name="U8" library="Texas Instruments_By_element14_Batch_1" deviceset="TLK100PHP" device=""/>
<part name="+3V80" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V81" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V82" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V83" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V84" library="supply1" deviceset="+3V3" device=""/>
<part name="C62" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="C63" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C64" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND205" library="supply1" deviceset="GND" device=""/>
<part name="GND206" library="supply1" deviceset="GND" device=""/>
<part name="GND207" library="supply1" deviceset="GND" device=""/>
<part name="C65" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="C66" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="C67" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND208" library="supply1" deviceset="GND" device=""/>
<part name="GND209" library="supply1" deviceset="GND" device=""/>
<part name="GND210" library="supply1" deviceset="GND" device=""/>
<part name="C68" library="rcl" deviceset="C-US" device="C0402" value="1.0uF"/>
<part name="GND211" library="supply1" deviceset="GND" device=""/>
<part name="R88" library="rcl" deviceset="R-US_" device="R0402" value="4.99k 1%"/>
<part name="GND214" library="supply1" deviceset="GND" device=""/>
<part name="R89" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R90" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R91" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="R92" library="rcl" deviceset="R-US_" device="R0402" value="49.9"/>
<part name="+3V85" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V86" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V87" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V88" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V89" library="supply1" deviceset="+3V3" device=""/>
<part name="C69" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND215" library="supply1" deviceset="GND" device=""/>
<part name="+3V91" library="supply1" deviceset="+3V3" device=""/>
<part name="C71" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND217" library="supply1" deviceset="GND" device=""/>
<part name="+3V92" library="supply1" deviceset="+3V3" device=""/>
<part name="C72" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND218" library="supply1" deviceset="GND" device=""/>
<part name="GND219" library="supply1" deviceset="GND" device=""/>
<part name="GND220" library="supply1" deviceset="GND" device=""/>
<part name="+3V95" library="supply1" deviceset="+3V3" device=""/>
<part name="C73" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND221" library="supply1" deviceset="GND" device=""/>
<part name="+3V96" library="supply1" deviceset="+3V3" device=""/>
<part name="C74" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND222" library="supply1" deviceset="GND" device=""/>
<part name="+3V97" library="supply1" deviceset="+3V3" device=""/>
<part name="C75" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND223" library="supply1" deviceset="GND" device=""/>
<part name="+3V98" library="supply1" deviceset="+3V3" device=""/>
<part name="C76" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND224" library="supply1" deviceset="GND" device=""/>
<part name="+3V99" library="supply1" deviceset="+3V3" device=""/>
<part name="C77" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND225" library="supply1" deviceset="GND" device=""/>
<part name="R95" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R96" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="GND227" library="supply1" deviceset="GND" device=""/>
<part name="R97" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="+3V102" library="supply1" deviceset="+3V3" device=""/>
<part name="R98" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R99" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="R100" library="rcl" deviceset="R-US_" device="R0402" value="2.2k"/>
<part name="+3V103" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V104" library="supply1" deviceset="+3V3" device=""/>
<part name="GND228" library="supply1" deviceset="GND" device=""/>
<part name="+3V105" library="supply1" deviceset="+3V3" device=""/>
<part name="Q1" library="crystal" deviceset="CRYSTAL" device="CTS406" value="406C35D25M00000"/>
<part name="C78" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="C79" library="rcl" deviceset="C-US" device="C0402" value="18pF"/>
<part name="GND226" library="supply1" deviceset="GND" device=""/>
<part name="GND229" library="supply1" deviceset="GND" device=""/>
<part name="Q2" library="crystal" deviceset="CRYSTAL" device="CTS406" value="406C35D12M00000"/>
<part name="GND230" library="supply1" deviceset="GND" device=""/>
<part name="GND231" library="supply1" deviceset="GND" device=""/>
<part name="GND232" library="supply1" deviceset="GND" device=""/>
<part name="+3V106" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V107" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V108" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V109" library="supply1" deviceset="+3V3" device=""/>
<part name="C80" library="rcl" deviceset="C-US" device="C0402" value="4.7uF"/>
<part name="GND18" library="supply1" deviceset="GND" device=""/>
<part name="GND19" library="supply1" deviceset="GND" device=""/>
<part name="GND20" library="supply1" deviceset="GND" device=""/>
<part name="GND21" library="supply1" deviceset="GND" device=""/>
<part name="+3V110" library="supply1" deviceset="+3V3" device=""/>
<part name="R2" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="+3V111" library="supply1" deviceset="+3V3" device=""/>
<part name="U$1" library="analog-adis" deviceset="ADIS16485" device="MODULE"/>
<part name="+3V113" library="supply1" deviceset="+3V3" device=""/>
<part name="GND172" library="supply1" deviceset="GND" device=""/>
<part name="GND173" library="supply1" deviceset="GND" device=""/>
<part name="GND174" library="supply1" deviceset="GND" device=""/>
<part name="IC17" library="tlb" deviceset="ADS8344" device="E"/>
<part name="GND175" library="supply1" deviceset="GND" device=""/>
<part name="C39" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="+3V67" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V68" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V69" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V70" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V71" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V72" library="supply1" deviceset="+3V3" device=""/>
<part name="R81" library="rcl" deviceset="R-US_" device="R0402" value="0"/>
<part name="C40" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND184" library="supply1" deviceset="GND" device=""/>
<part name="R4" library="rcl" deviceset="R-US_" device="R0402" value="10k"/>
<part name="+3V112" library="supply1" deviceset="+3V3" device=""/>
<part name="GND1" library="supply1" deviceset="GND" device=""/>
<part name="GND2" library="supply1" deviceset="GND" device=""/>
<part name="GND3" library="supply1" deviceset="GND" device=""/>
<part name="GND4" library="supply1" deviceset="GND" device=""/>
<part name="R5" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R6" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R7" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="R8" library="rcl" deviceset="R-US_" device="R0402" value="1k"/>
<part name="+3V3" library="supply1" deviceset="+3V3" device=""/>
<part name="C4" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="C5" library="rcl" deviceset="C-US" device="C0402" value="0.1uF"/>
<part name="VCC9" library="supply1" deviceset="VCCINT" device=""/>
<part name="GND6" library="supply1" deviceset="GND" device=""/>
<part name="Z1" library="tlb" deviceset="B725" device="90"/>
<part name="Z2" library="tlb" deviceset="B725" device="90"/>
<part name="Z3" library="tlb" deviceset="B725" device="90"/>
<part name="Z4" library="tlb" deviceset="B725" device="90"/>
<part name="GND7" library="supply1" deviceset="GND" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="GND9" library="supply1" deviceset="GND" device=""/>
<part name="GND10" library="supply1" deviceset="GND" device=""/>
<part name="CONS" library="con-lstb" deviceset="MA06-1" device="" value="68016-106HLF"/>
<part name="GND11" library="supply1" deviceset="GND" device=""/>
<part name="GND12" library="supply1" deviceset="GND" device=""/>
<part name="GND14" library="supply1" deviceset="GND" device=""/>
<part name="GND15" library="supply1" deviceset="GND" device=""/>
<part name="+3V24" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V26" library="supply1" deviceset="+3V3" device=""/>
<part name="R12" library="rcl" deviceset="R-US_" device="R0402" value="470"/>
<part name="R13" library="rcl" deviceset="R-US_" device="R0402" value="470"/>
<part name="+3V27" library="supply1" deviceset="+3V3" device=""/>
<part name="+3V28" library="supply1" deviceset="+3V3" device=""/>
<part name="U2" library="MAGJACK-POE" deviceset="MAGJACK-POE" device="" value="7499210121A"/>
<part name="IC6" library="LTC4257_" deviceset="LTC4257*" device="S8"/>
<part name="U$3" library="tlb" deviceset="SMAJ*ADI" device="" value="SMAJ58A"/>
<part name="C13" library="rcl" deviceset="C-US" device="C1206" value="2.2uF 100v"/>
<part name="U$4" library="xppower" deviceset="IU4824SA" device="" value="IU4803SA"/>
<part name="GND27" library="supply1" deviceset="GND" device=""/>
<part name="+3V2" library="supply1" deviceset="+3V3" device=""/>
<part name="XP" library="tlb" deviceset="43045-1212" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-127" y="162.56" size="1.778" layer="97">at vregin</text>
<text x="-101.6" y="162.56" size="1.778" layer="97">at vregout</text>
<text x="-35.56" y="160.02" size="1.778" layer="97">around at32</text>
<text x="353.06" y="71.12" size="1.778" layer="97">The library part seems to have TD and RD
reversed relative to the Wurth 7499210121A</text>
</plain>
<instances>
<instance part="C16" gate="G$1" x="-30.48" y="152.4"/>
<instance part="C19" gate="G$1" x="7.62" y="152.4"/>
<instance part="C20" gate="G$1" x="20.32" y="152.4"/>
<instance part="C18" gate="G$1" x="-5.08" y="152.4"/>
<instance part="+3V6" gate="G$1" x="-43.18" y="157.48"/>
<instance part="C15" gate="G$1" x="-43.18" y="152.4"/>
<instance part="C2" gate="G$1" x="10.16" y="-93.98"/>
<instance part="C3" gate="G$1" x="25.4" y="-93.98"/>
<instance part="GND28" gate="1" x="17.78" y="-101.6"/>
<instance part="GND29" gate="1" x="-393.7" y="-177.8"/>
<instance part="GND30" gate="1" x="-391.16" y="-177.8"/>
<instance part="GND31" gate="1" x="-388.62" y="-177.8"/>
<instance part="GND32" gate="1" x="-386.08" y="-177.8"/>
<instance part="GND33" gate="1" x="-383.54" y="-177.8"/>
<instance part="GND34" gate="1" x="-381" y="-177.8"/>
<instance part="GND35" gate="1" x="-378.46" y="-177.8"/>
<instance part="GND36" gate="1" x="-370.84" y="-177.8"/>
<instance part="C6" gate="G$1" x="-152.4" y="86.36"/>
<instance part="GND53" gate="1" x="-152.4" y="76.2"/>
<instance part="JTAG1" gate="G$1" x="-175.26" y="-45.72"/>
<instance part="GND39" gate="1" x="-182.88" y="-55.88"/>
<instance part="GND41" gate="1" x="-182.88" y="-35.56" rot="R180"/>
<instance part="+3V15" gate="G$1" x="-198.12" y="-48.26" rot="R90"/>
<instance part="+3V56" gate="G$1" x="-152.4" y="106.68"/>
<instance part="R1" gate="G$1" x="-152.4" y="96.52" rot="R90"/>
<instance part="+3V8" gate="G$1" x="-93.98" y="114.3" rot="R90"/>
<instance part="+3V16" gate="G$1" x="-93.98" y="104.14" rot="R90"/>
<instance part="+3V17" gate="G$1" x="-93.98" y="96.52" rot="R90"/>
<instance part="+3V18" gate="G$1" x="-96.52" y="-25.4" rot="R90"/>
<instance part="+3V19" gate="G$1" x="-96.52" y="-27.94" rot="R90"/>
<instance part="+3V20" gate="G$1" x="-96.52" y="-30.48" rot="R90"/>
<instance part="GND17" gate="1" x="-73.66" y="-119.38" rot="R270"/>
<instance part="GND25" gate="1" x="-73.66" y="-121.92" rot="R270"/>
<instance part="GND26" gate="1" x="-73.66" y="-124.46" rot="R270"/>
<instance part="GND77" gate="1" x="-73.66" y="-127" rot="R270"/>
<instance part="GND78" gate="1" x="-78.74" y="5.08" rot="R270"/>
<instance part="GND79" gate="1" x="-78.74" y="7.62" rot="R270"/>
<instance part="GND80" gate="1" x="-78.74" y="10.16" rot="R270"/>
<instance part="GND81" gate="1" x="-78.74" y="12.7" rot="R270"/>
<instance part="GND82" gate="1" x="-78.74" y="15.24" rot="R270"/>
<instance part="GND83" gate="1" x="-78.74" y="17.78" rot="R270"/>
<instance part="+3V21" gate="G$1" x="-93.98" y="109.22" rot="R90"/>
<instance part="VCC4" gate="G$1" x="-78.74" y="111.76" rot="R90"/>
<instance part="VCC5" gate="G$1" x="-78.74" y="106.68" rot="R90"/>
<instance part="VCC6" gate="G$1" x="-78.74" y="101.6" rot="R90"/>
<instance part="VCC7" gate="G$1" x="-78.74" y="99.06" rot="R90"/>
<instance part="VCC8" gate="G$1" x="-88.9" y="-40.64" rot="R90"/>
<instance part="VCC1" gate="G$1" x="-88.9" y="-38.1" rot="R90"/>
<instance part="+3V1" gate="G$1" x="-96.52" y="-35.56" rot="R90"/>
<instance part="+3V9" gate="G$1" x="-96.52" y="-33.02" rot="R90"/>
<instance part="U1" gate="B" x="-53.34" y="-73.66"/>
<instance part="U1" gate="A" x="-53.34" y="63.5"/>
<instance part="+3V10" gate="G$1" x="-114.3" y="154.94"/>
<instance part="VCC2" gate="G$1" x="-99.06" y="154.94"/>
<instance part="VCC3" gate="G$1" x="-88.9" y="154.94"/>
<instance part="+3V11" gate="G$1" x="-127" y="154.94"/>
<instance part="C11" gate="G$1" x="-127" y="149.86"/>
<instance part="C12" gate="G$1" x="-114.3" y="149.86"/>
<instance part="C31" gate="G$1" x="-99.06" y="149.86"/>
<instance part="C32" gate="G$1" x="-88.9" y="149.86"/>
<instance part="GND85" gate="1" x="-127" y="142.24"/>
<instance part="GND87" gate="1" x="-114.3" y="142.24"/>
<instance part="GND90" gate="1" x="-99.06" y="142.24"/>
<instance part="GND91" gate="1" x="-88.9" y="142.24"/>
<instance part="C34" gate="G$1" x="81.28" y="149.86"/>
<instance part="VCC10" gate="G$1" x="81.28" y="154.94"/>
<instance part="GND93" gate="1" x="81.28" y="142.24"/>
<instance part="C35" gate="G$1" x="91.44" y="149.86"/>
<instance part="VCC11" gate="G$1" x="91.44" y="154.94"/>
<instance part="GND94" gate="1" x="91.44" y="142.24"/>
<instance part="C36" gate="G$1" x="101.6" y="149.86"/>
<instance part="VCC12" gate="G$1" x="101.6" y="154.94"/>
<instance part="GND95" gate="1" x="101.6" y="142.24"/>
<instance part="+3V12" gate="G$1" x="-30.48" y="157.48"/>
<instance part="+3V14" gate="G$1" x="-5.08" y="157.48"/>
<instance part="+3V22" gate="G$1" x="7.62" y="157.48"/>
<instance part="+3V23" gate="G$1" x="20.32" y="157.48"/>
<instance part="GND99" gate="1" x="-43.18" y="144.78"/>
<instance part="GND100" gate="1" x="-30.48" y="144.78"/>
<instance part="GND102" gate="1" x="-5.08" y="144.78"/>
<instance part="GND103" gate="1" x="7.62" y="144.78"/>
<instance part="GND104" gate="1" x="20.32" y="144.78"/>
<instance part="C52" gate="G$1" x="111.76" y="149.86"/>
<instance part="VCC13" gate="G$1" x="111.76" y="154.94"/>
<instance part="GND156" gate="1" x="111.76" y="142.24"/>
<instance part="C1" gate="G$1" x="33.02" y="152.4"/>
<instance part="+3V25" gate="G$1" x="33.02" y="157.48"/>
<instance part="GND149" gate="1" x="33.02" y="144.78"/>
<instance part="U8" gate="A" x="182.88" y="60.96"/>
<instance part="+3V80" gate="G$1" x="109.22" y="93.98" rot="R90"/>
<instance part="+3V81" gate="G$1" x="109.22" y="91.44" rot="R90"/>
<instance part="+3V82" gate="G$1" x="109.22" y="88.9" rot="R90"/>
<instance part="+3V83" gate="G$1" x="109.22" y="86.36" rot="R90"/>
<instance part="+3V84" gate="G$1" x="109.22" y="83.82" rot="R90"/>
<instance part="C62" gate="G$1" x="142.24" y="-12.7"/>
<instance part="C63" gate="G$1" x="152.4" y="-12.7"/>
<instance part="C64" gate="G$1" x="162.56" y="-12.7"/>
<instance part="GND205" gate="1" x="142.24" y="-20.32"/>
<instance part="GND206" gate="1" x="152.4" y="-20.32"/>
<instance part="GND207" gate="1" x="162.56" y="-20.32"/>
<instance part="C65" gate="G$1" x="190.5" y="-12.7"/>
<instance part="C66" gate="G$1" x="200.66" y="-12.7"/>
<instance part="C67" gate="G$1" x="210.82" y="-12.7"/>
<instance part="GND208" gate="1" x="190.5" y="-20.32"/>
<instance part="GND209" gate="1" x="200.66" y="-20.32"/>
<instance part="GND210" gate="1" x="210.82" y="-20.32"/>
<instance part="C68" gate="G$1" x="236.22" y="-12.7"/>
<instance part="GND211" gate="1" x="236.22" y="-20.32"/>
<instance part="R88" gate="G$1" x="111.76" y="43.18"/>
<instance part="GND214" gate="1" x="104.14" y="43.18" rot="R270"/>
<instance part="R89" gate="G$1" x="307.34" y="73.66" rot="R270"/>
<instance part="R90" gate="G$1" x="314.96" y="73.66" rot="R270"/>
<instance part="R91" gate="G$1" x="322.58" y="73.66" rot="R270"/>
<instance part="R92" gate="G$1" x="330.2" y="73.66" rot="R270"/>
<instance part="+3V85" gate="G$1" x="307.34" y="81.28"/>
<instance part="+3V86" gate="G$1" x="314.96" y="81.28"/>
<instance part="+3V87" gate="G$1" x="322.58" y="81.28"/>
<instance part="+3V88" gate="G$1" x="330.2" y="81.28"/>
<instance part="+3V89" gate="G$1" x="340.36" y="116.84"/>
<instance part="C69" gate="G$1" x="340.36" y="111.76"/>
<instance part="GND215" gate="1" x="340.36" y="104.14"/>
<instance part="+3V91" gate="G$1" x="365.76" y="116.84"/>
<instance part="C71" gate="G$1" x="365.76" y="111.76"/>
<instance part="GND217" gate="1" x="365.76" y="104.14"/>
<instance part="+3V92" gate="G$1" x="378.46" y="116.84"/>
<instance part="C72" gate="G$1" x="378.46" y="111.76"/>
<instance part="GND218" gate="1" x="378.46" y="104.14"/>
<instance part="GND219" gate="1" x="142.24" y="25.4" rot="R270"/>
<instance part="GND220" gate="1" x="142.24" y="22.86" rot="R270"/>
<instance part="+3V95" gate="G$1" x="264.16" y="116.84"/>
<instance part="C73" gate="G$1" x="264.16" y="111.76"/>
<instance part="GND221" gate="1" x="264.16" y="104.14"/>
<instance part="+3V96" gate="G$1" x="276.86" y="116.84"/>
<instance part="C74" gate="G$1" x="276.86" y="111.76"/>
<instance part="GND222" gate="1" x="276.86" y="104.14"/>
<instance part="+3V97" gate="G$1" x="289.56" y="116.84"/>
<instance part="C75" gate="G$1" x="289.56" y="111.76"/>
<instance part="GND223" gate="1" x="289.56" y="104.14"/>
<instance part="+3V98" gate="G$1" x="302.26" y="116.84"/>
<instance part="C76" gate="G$1" x="302.26" y="111.76"/>
<instance part="GND224" gate="1" x="302.26" y="104.14"/>
<instance part="+3V99" gate="G$1" x="251.46" y="116.84"/>
<instance part="C77" gate="G$1" x="251.46" y="111.76"/>
<instance part="GND225" gate="1" x="251.46" y="104.14"/>
<instance part="R95" gate="G$1" x="114.3" y="58.42"/>
<instance part="R96" gate="G$1" x="114.3" y="55.88"/>
<instance part="GND227" gate="1" x="106.68" y="55.88" rot="R270"/>
<instance part="R97" gate="G$1" x="254" y="43.18"/>
<instance part="+3V102" gate="G$1" x="261.62" y="43.18" rot="R270"/>
<instance part="R98" gate="G$1" x="254" y="68.58"/>
<instance part="R99" gate="G$1" x="254" y="66.04"/>
<instance part="R100" gate="G$1" x="254" y="40.64"/>
<instance part="+3V103" gate="G$1" x="261.62" y="68.58" rot="R270"/>
<instance part="+3V104" gate="G$1" x="261.62" y="66.04" rot="R270"/>
<instance part="GND228" gate="1" x="261.62" y="40.64" rot="R90"/>
<instance part="+3V105" gate="G$1" x="106.68" y="58.42" rot="R90"/>
<instance part="Q1" gate="G$1" x="177.8" y="132.08"/>
<instance part="C78" gate="G$1" x="165.1" y="129.54"/>
<instance part="C79" gate="G$1" x="190.5" y="129.54"/>
<instance part="GND226" gate="1" x="165.1" y="121.92"/>
<instance part="GND229" gate="1" x="190.5" y="121.92"/>
<instance part="Q2" gate="G$1" x="17.78" y="-78.74"/>
<instance part="GND230" gate="1" x="-274.32" y="-96.52"/>
<instance part="GND231" gate="1" x="-271.78" y="-96.52"/>
<instance part="GND232" gate="1" x="-269.24" y="-96.52"/>
<instance part="+3V106" gate="G$1" x="-269.24" y="-33.02"/>
<instance part="+3V107" gate="G$1" x="-271.78" y="-33.02"/>
<instance part="+3V108" gate="G$1" x="-274.32" y="-33.02"/>
<instance part="+3V109" gate="G$1" x="-304.8" y="-20.32"/>
<instance part="C80" gate="G$1" x="-304.8" y="-25.4"/>
<instance part="GND18" gate="1" x="-304.8" y="-33.02"/>
<instance part="GND19" gate="1" x="-111.76" y="86.36" rot="R270"/>
<instance part="GND20" gate="1" x="-111.76" y="83.82" rot="R270"/>
<instance part="GND21" gate="1" x="-111.76" y="88.9" rot="R270"/>
<instance part="+3V110" gate="G$1" x="-73.66" y="-114.3" rot="R90"/>
<instance part="R2" gate="G$1" x="-332.74" y="-68.58" rot="R180"/>
<instance part="+3V111" gate="G$1" x="-340.36" y="-68.58" rot="R90"/>
<instance part="U$1" gate="G$1" x="-274.32" y="-66.04"/>
<instance part="+3V113" gate="G$1" x="-281.94" y="-33.02"/>
<instance part="GND172" gate="1" x="289.56" y="-187.96" rot="R90"/>
<instance part="GND173" gate="1" x="294.64" y="-190.5" rot="R90"/>
<instance part="GND174" gate="1" x="236.22" y="-190.5" rot="R270"/>
<instance part="IC17" gate="G$1" x="269.24" y="-175.26"/>
<instance part="GND175" gate="1" x="215.9" y="-187.96" rot="R90"/>
<instance part="C39" gate="G$1" x="200.66" y="-187.96" rot="R90"/>
<instance part="+3V67" gate="G$1" x="294.64" y="-162.56" rot="R270"/>
<instance part="+3V68" gate="G$1" x="299.72" y="-165.1" rot="R270"/>
<instance part="+3V69" gate="G$1" x="327.66" y="-170.18" rot="R270"/>
<instance part="+3V70" gate="G$1" x="226.06" y="-187.96" rot="R90"/>
<instance part="+3V71" gate="G$1" x="190.5" y="-187.96" rot="R90"/>
<instance part="+3V72" gate="G$1" x="167.64" y="-139.7" rot="R90"/>
<instance part="R81" gate="G$1" x="175.26" y="-139.7"/>
<instance part="C40" gate="G$1" x="195.58" y="-142.24"/>
<instance part="GND184" gate="1" x="195.58" y="-149.86"/>
<instance part="R4" gate="G$1" x="330.2" y="-175.26"/>
<instance part="+3V112" gate="G$1" x="337.82" y="-175.26" rot="R270"/>
<instance part="GND1" gate="1" x="233.68" y="-243.84" rot="R270"/>
<instance part="GND2" gate="1" x="233.68" y="-261.62" rot="R270"/>
<instance part="GND3" gate="1" x="233.68" y="-279.4" rot="R270"/>
<instance part="GND4" gate="1" x="233.68" y="-297.18" rot="R270"/>
<instance part="R5" gate="G$1" x="233.68" y="-238.76"/>
<instance part="R6" gate="G$1" x="233.68" y="-256.54"/>
<instance part="R7" gate="G$1" x="233.68" y="-274.32"/>
<instance part="R8" gate="G$1" x="233.68" y="-292.1"/>
<instance part="+3V3" gate="G$1" x="-55.88" y="157.48"/>
<instance part="C4" gate="G$1" x="-55.88" y="152.4"/>
<instance part="GND5" gate="1" x="-55.88" y="144.78"/>
<instance part="C5" gate="G$1" x="121.92" y="149.86"/>
<instance part="VCC9" gate="G$1" x="121.92" y="154.94"/>
<instance part="GND6" gate="1" x="121.92" y="142.24"/>
<instance part="Z1" gate="G$1" x="210.82" y="-238.76"/>
<instance part="Z2" gate="G$1" x="210.82" y="-256.54"/>
<instance part="Z3" gate="G$1" x="210.82" y="-274.32"/>
<instance part="Z4" gate="G$1" x="210.82" y="-292.1"/>
<instance part="GND7" gate="1" x="203.2" y="-238.76" rot="R270"/>
<instance part="GND8" gate="1" x="203.2" y="-256.54" rot="R270"/>
<instance part="GND9" gate="1" x="203.2" y="-274.32" rot="R270"/>
<instance part="GND10" gate="1" x="203.2" y="-292.1" rot="R270"/>
<instance part="CONS" gate="1" x="-177.8" y="43.18"/>
<instance part="GND11" gate="1" x="-154.94" y="35.56" rot="R90"/>
<instance part="GND12" gate="1" x="-154.94" y="38.1" rot="R90"/>
<instance part="GND14" gate="1" x="340.36" y="33.02" rot="R270"/>
<instance part="GND15" gate="1" x="340.36" y="30.48" rot="R270"/>
<instance part="+3V24" gate="G$1" x="358.14" y="50.8" rot="R90"/>
<instance part="+3V26" gate="G$1" x="358.14" y="40.64" rot="R90"/>
<instance part="R12" gate="G$1" x="411.48" y="50.8" rot="R180"/>
<instance part="R13" gate="G$1" x="411.48" y="33.02" rot="R180"/>
<instance part="+3V27" gate="G$1" x="398.78" y="58.42" rot="R270"/>
<instance part="+3V28" gate="G$1" x="398.78" y="40.64" rot="R270"/>
<instance part="U2" gate="G$1" x="378.46" y="45.72"/>
<instance part="XP" gate="1" x="246.38" y="-233.68"/>
<instance part="XP" gate="2" x="246.38" y="-243.84"/>
<instance part="XP" gate="3" x="246.38" y="-256.54"/>
<instance part="XP" gate="4" x="246.38" y="-269.24"/>
<instance part="XP" gate="5" x="246.38" y="-279.4"/>
<instance part="XP" gate="6" x="246.38" y="-292.1"/>
<instance part="XP" gate="7" x="246.38" y="-238.76"/>
<instance part="XP" gate="8" x="246.38" y="-251.46"/>
<instance part="XP" gate="9" x="246.38" y="-261.62"/>
<instance part="XP" gate="10" x="246.38" y="-274.32"/>
<instance part="XP" gate="11" x="246.38" y="-287.02"/>
<instance part="XP" gate="12" x="246.38" y="-297.18"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<wire x1="25.4" y1="-99.06" x2="17.78" y2="-99.06" width="0.1524" layer="91"/>
<wire x1="17.78" y1="-99.06" x2="10.16" y2="-99.06" width="0.1524" layer="91"/>
<junction x="17.78" y="-99.06"/>
<pinref part="C3" gate="G$1" pin="2"/>
<pinref part="C2" gate="G$1" pin="2"/>
<pinref part="GND28" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-152.4" y1="78.74" x2="-152.4" y2="81.28" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="2"/>
<pinref part="GND53" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="JTAG1" gate="G$1" pin="2"/>
<pinref part="GND39" gate="1" pin="GND"/>
<wire x1="-182.88" y1="-53.34" x2="-182.88" y2="-50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JTAG1" gate="G$1" pin="10"/>
<pinref part="GND41" gate="1" pin="GND"/>
<wire x1="-182.88" y1="-38.1" x2="-182.88" y2="-40.64" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND17" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GNDANA"/>
</segment>
<segment>
<pinref part="GND25" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND_2"/>
</segment>
<segment>
<pinref part="GND26" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND_3"/>
</segment>
<segment>
<pinref part="GND77" gate="1" pin="GND"/>
<pinref part="U1" gate="B" pin="GND"/>
</segment>
<segment>
<pinref part="GND78" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND"/>
</segment>
<segment>
<pinref part="GND79" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_6"/>
</segment>
<segment>
<pinref part="GND80" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_5"/>
</segment>
<segment>
<pinref part="GND81" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_4"/>
</segment>
<segment>
<pinref part="GND82" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_3"/>
</segment>
<segment>
<pinref part="GND83" gate="1" pin="GND"/>
<pinref part="U1" gate="A" pin="GND_2"/>
</segment>
<segment>
<pinref part="C11" gate="G$1" pin="2"/>
<pinref part="GND85" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C12" gate="G$1" pin="2"/>
<pinref part="GND87" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C31" gate="G$1" pin="2"/>
<pinref part="GND90" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C32" gate="G$1" pin="2"/>
<pinref part="GND91" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="2"/>
<pinref part="GND93" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="2"/>
<pinref part="GND94" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="2"/>
<pinref part="GND95" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="2"/>
<pinref part="GND99" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="2"/>
<pinref part="GND100" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="2"/>
<pinref part="GND102" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="2"/>
<pinref part="GND103" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="2"/>
<pinref part="GND104" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="2"/>
<pinref part="GND156" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="2"/>
<pinref part="GND149" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C62" gate="G$1" pin="2"/>
<pinref part="GND205" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C63" gate="G$1" pin="2"/>
<pinref part="GND206" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C64" gate="G$1" pin="2"/>
<pinref part="GND207" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C65" gate="G$1" pin="2"/>
<pinref part="GND208" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C66" gate="G$1" pin="2"/>
<pinref part="GND209" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C67" gate="G$1" pin="2"/>
<pinref part="GND210" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C68" gate="G$1" pin="2"/>
<pinref part="GND211" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R88" gate="G$1" pin="1"/>
<pinref part="GND214" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C69" gate="G$1" pin="2"/>
<pinref part="GND215" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C71" gate="G$1" pin="2"/>
<pinref part="GND217" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C72" gate="G$1" pin="2"/>
<pinref part="GND218" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VSS"/>
<pinref part="GND219" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="EP"/>
<pinref part="GND220" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C73" gate="G$1" pin="2"/>
<pinref part="GND221" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C74" gate="G$1" pin="2"/>
<pinref part="GND222" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C75" gate="G$1" pin="2"/>
<pinref part="GND223" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C76" gate="G$1" pin="2"/>
<pinref part="GND224" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C77" gate="G$1" pin="2"/>
<pinref part="GND225" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R96" gate="G$1" pin="1"/>
<pinref part="GND227" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R100" gate="G$1" pin="2"/>
<pinref part="GND228" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C78" gate="G$1" pin="2"/>
<pinref part="GND226" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C79" gate="G$1" pin="2"/>
<pinref part="GND229" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C80" gate="G$1" pin="2"/>
<pinref part="GND18" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="86.36" x2="-76.2" y2="86.36" width="0.1524" layer="91"/>
<label x="-101.6" y="86.36" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="DM"/>
<pinref part="GND19" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="83.82" x2="-76.2" y2="83.82" width="0.1524" layer="91"/>
<label x="-101.6" y="83.82" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="DP"/>
<pinref part="GND20" gate="1" pin="GND"/>
</segment>
<segment>
<wire x1="-109.22" y1="88.9" x2="-76.2" y2="88.9" width="0.1524" layer="91"/>
<label x="-101.6" y="88.9" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="VBUS"/>
<pinref part="GND21" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND230" gate="1" pin="GND"/>
<pinref part="U$1" gate="G$1" pin="GND1"/>
</segment>
<segment>
<pinref part="GND231" gate="1" pin="GND"/>
<pinref part="U$1" gate="G$1" pin="GND2"/>
</segment>
<segment>
<pinref part="GND232" gate="1" pin="GND"/>
<pinref part="U$1" gate="G$1" pin="GND3"/>
</segment>
<segment>
<wire x1="287.02" y1="-187.96" x2="281.94" y2="-187.96" width="0.1524" layer="91"/>
<pinref part="GND172" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="292.1" y1="-190.5" x2="281.94" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="GND173" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<wire x1="238.76" y1="-190.5" x2="256.54" y2="-190.5" width="0.1524" layer="91"/>
<pinref part="GND174" gate="1" pin="GND"/>
<pinref part="IC17" gate="G$1" pin="COM"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="2"/>
<pinref part="GND175" gate="1" pin="GND"/>
<wire x1="205.74" y1="-187.96" x2="213.36" y2="-187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C40" gate="G$1" pin="2"/>
<pinref part="GND184" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="236.22" y1="-243.84" x2="243.84" y2="-243.84" width="0.1524" layer="91"/>
<pinref part="XP" gate="2" pin="S"/>
</segment>
<segment>
<pinref part="GND2" gate="1" pin="GND"/>
<wire x1="236.22" y1="-261.62" x2="243.84" y2="-261.62" width="0.1524" layer="91"/>
<pinref part="XP" gate="9" pin="S"/>
</segment>
<segment>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="236.22" y1="-279.4" x2="243.84" y2="-279.4" width="0.1524" layer="91"/>
<pinref part="XP" gate="5" pin="S"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="236.22" y1="-297.18" x2="243.84" y2="-297.18" width="0.1524" layer="91"/>
<pinref part="XP" gate="12" pin="S"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="2"/>
<pinref part="GND6" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z1" gate="G$1" pin="A"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z2" gate="G$1" pin="A"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z3" gate="G$1" pin="A"/>
<pinref part="GND9" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="Z4" gate="G$1" pin="A"/>
<pinref part="GND10" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="CONS" gate="1" pin="1"/>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="-170.18" y1="35.56" x2="-157.48" y2="35.56" width="0.1524" layer="91"/>
<label x="-167.64" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="GND12" gate="1" pin="GND"/>
<pinref part="CONS" gate="1" pin="2"/>
<wire x1="-157.48" y1="38.1" x2="-170.18" y2="38.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="360.68" y1="33.02" x2="342.9" y2="33.02" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SHIELD@0"/>
</segment>
<segment>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="360.68" y1="30.48" x2="342.9" y2="30.48" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="SHIELD@1"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<wire x1="-195.58" y1="-48.26" x2="-182.88" y2="-48.26" width="0.1524" layer="91"/>
<pinref part="JTAG1" gate="G$1" pin="4"/>
<pinref part="+3V15" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="-152.4" y1="104.14" x2="-152.4" y2="101.6" width="0.1524" layer="91"/>
<pinref part="+3V56" gate="G$1" pin="+3V3"/>
<pinref part="R1" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="+3V8" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="114.3" x2="-76.2" y2="114.3" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO_2"/>
</segment>
<segment>
<pinref part="+3V16" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="104.14" x2="-76.2" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO_3"/>
</segment>
<segment>
<pinref part="+3V17" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="96.52" x2="-76.2" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIO"/>
</segment>
<segment>
<pinref part="+3V18" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-25.4" x2="-71.12" y2="-25.4" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_2"/>
</segment>
<segment>
<pinref part="+3V19" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-27.94" x2="-71.12" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_3"/>
</segment>
<segment>
<pinref part="+3V20" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-30.48" x2="-71.12" y2="-30.48" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO_4"/>
</segment>
<segment>
<pinref part="+3V21" gate="G$1" pin="+3V3"/>
<wire x1="-91.44" y1="109.22" x2="-76.2" y2="109.22" width="0.1524" layer="91"/>
<pinref part="U1" gate="A" pin="VDDIN"/>
</segment>
<segment>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-35.56" x2="-71.12" y2="-35.56" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDANA"/>
</segment>
<segment>
<pinref part="+3V9" gate="G$1" pin="+3V3"/>
<wire x1="-93.98" y1="-33.02" x2="-71.12" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDIO"/>
</segment>
<segment>
<pinref part="+3V11" gate="G$1" pin="+3V3"/>
<pinref part="C11" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V10" gate="G$1" pin="+3V3"/>
<pinref part="C12" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C15" gate="G$1" pin="1"/>
<pinref part="+3V6" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C16" gate="G$1" pin="1"/>
<pinref part="+3V12" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C18" gate="G$1" pin="1"/>
<pinref part="+3V14" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C19" gate="G$1" pin="1"/>
<pinref part="+3V22" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C20" gate="G$1" pin="1"/>
<pinref part="+3V23" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C1" gate="G$1" pin="1"/>
<pinref part="+3V25" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_VA11"/>
<pinref part="+3V80" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="93.98" x2="144.78" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_IO_2"/>
<pinref part="+3V81" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="91.44" x2="144.78" y2="91.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_VD11"/>
<pinref part="+3V82" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="88.9" x2="144.78" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_IO"/>
<pinref part="+3V83" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="86.36" x2="144.78" y2="86.36" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VDD33_V18"/>
<pinref part="+3V84" gate="G$1" pin="+3V3"/>
<wire x1="111.76" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R89" gate="G$1" pin="1"/>
<pinref part="+3V85" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R90" gate="G$1" pin="1"/>
<pinref part="+3V86" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R91" gate="G$1" pin="1"/>
<pinref part="+3V87" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R92" gate="G$1" pin="1"/>
<pinref part="+3V88" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V89" gate="G$1" pin="+3V3"/>
<pinref part="C69" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V91" gate="G$1" pin="+3V3"/>
<pinref part="C71" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V92" gate="G$1" pin="+3V3"/>
<pinref part="C72" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V95" gate="G$1" pin="+3V3"/>
<pinref part="C73" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V96" gate="G$1" pin="+3V3"/>
<pinref part="C74" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V97" gate="G$1" pin="+3V3"/>
<pinref part="C75" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V98" gate="G$1" pin="+3V3"/>
<pinref part="C76" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="+3V99" gate="G$1" pin="+3V3"/>
<pinref part="C77" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R97" gate="G$1" pin="2"/>
<pinref part="+3V102" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R98" gate="G$1" pin="2"/>
<pinref part="+3V103" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R99" gate="G$1" pin="2"/>
<pinref part="+3V104" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R95" gate="G$1" pin="1"/>
<pinref part="+3V105" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V109" gate="G$1" pin="+3V3"/>
<pinref part="C80" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="ADVREF"/>
<pinref part="+3V110" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="+3V111" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V106" gate="G$1" pin="+3V3"/>
<pinref part="U$1" gate="G$1" pin="VCC3"/>
</segment>
<segment>
<pinref part="+3V107" gate="G$1" pin="+3V3"/>
<pinref part="U$1" gate="G$1" pin="VCC2"/>
</segment>
<segment>
<pinref part="+3V108" gate="G$1" pin="+3V3"/>
<pinref part="U$1" gate="G$1" pin="VCC1"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VDDRTC"/>
<pinref part="+3V113" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="292.1" y1="-162.56" x2="281.94" y2="-162.56" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VCC@1"/>
<pinref part="+3V67" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="297.18" y1="-165.1" x2="281.94" y2="-165.1" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VCC"/>
<pinref part="+3V68" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="281.94" y1="-170.18" x2="325.12" y2="-170.18" width="0.1524" layer="91"/>
<label x="287.02" y="-170.18" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="!SHDN"/>
<pinref part="+3V69" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<wire x1="228.6" y1="-187.96" x2="256.54" y2="-187.96" width="0.1524" layer="91"/>
<pinref part="IC17" gate="G$1" pin="VREF"/>
<pinref part="+3V70" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C39" gate="G$1" pin="1"/>
<wire x1="193.04" y1="-187.96" x2="198.12" y2="-187.96" width="0.1524" layer="91"/>
<pinref part="+3V71" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="+3V72" gate="G$1" pin="+3V3"/>
<pinref part="R81" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="+3V112" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="+3V3" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="TCT"/>
<pinref part="+3V24" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="RCT"/>
<pinref part="+3V26" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="YLED+"/>
<pinref part="+3V27" gate="G$1" pin="+3V3"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GLED+"/>
<pinref part="+3V28" gate="G$1" pin="+3V3"/>
</segment>
</net>
<net name="VCCINT" class="0">
<segment>
<pinref part="VCC8" gate="G$1" pin="VCCINT"/>
<wire x1="-86.36" y1="-40.64" x2="-71.12" y2="-40.64" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDCORE"/>
</segment>
<segment>
<pinref part="VCC1" gate="G$1" pin="VCCINT"/>
<wire x1="-86.36" y1="-38.1" x2="-71.12" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="VDDPLL"/>
</segment>
<segment>
<pinref part="VCC4" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDOUT"/>
</segment>
<segment>
<pinref part="VCC5" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE_2"/>
</segment>
<segment>
<pinref part="VCC6" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE_3"/>
</segment>
<segment>
<pinref part="VCC7" gate="G$1" pin="VCCINT"/>
<pinref part="U1" gate="A" pin="VDDCORE"/>
</segment>
<segment>
<pinref part="VCC2" gate="G$1" pin="VCCINT"/>
<pinref part="C31" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="VCC3" gate="G$1" pin="VCCINT"/>
<pinref part="C32" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="C34" gate="G$1" pin="1"/>
<pinref part="VCC10" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C35" gate="G$1" pin="1"/>
<pinref part="VCC11" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C36" gate="G$1" pin="1"/>
<pinref part="VCC12" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C52" gate="G$1" pin="1"/>
<pinref part="VCC13" gate="G$1" pin="VCCINT"/>
</segment>
<segment>
<pinref part="C5" gate="G$1" pin="1"/>
<pinref part="VCC9" gate="G$1" pin="VCCINT"/>
</segment>
</net>
<net name="/RESET" class="0">
<segment>
<wire x1="-182.88" y1="-45.72" x2="-198.12" y2="-45.72" width="0.1524" layer="91"/>
<label x="-195.58" y="-45.72" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="C6" gate="G$1" pin="1"/>
<junction x="-152.4" y="91.44"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="-152.4" y1="91.44" x2="-152.4" y2="88.9" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="91.44" x2="-76.2" y2="91.44" width="0.1524" layer="91"/>
<label x="-101.6" y="91.44" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="RESET_N"/>
</segment>
</net>
<net name="MCUXTAL3" class="0">
<segment>
<wire x1="20.32" y1="-78.74" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-91.44" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="25.4" y1="-60.96" x2="25.4" y2="-78.74" width="0.1524" layer="91"/>
<junction x="25.4" y="-78.74"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="25.4" y1="-60.96" x2="-35.56" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="PC02"/>
<pinref part="Q2" gate="G$1" pin="2"/>
<label x="-33.02" y="-60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCUXTAL4" class="0">
<segment>
<wire x1="10.16" y1="-91.44" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="15.24" y1="-78.74" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<wire x1="10.16" y1="-63.5" x2="10.16" y2="-78.74" width="0.1524" layer="91"/>
<junction x="10.16" y="-78.74"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="10.16" y1="-63.5" x2="-35.56" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="U1" gate="B" pin="PC03"/>
<pinref part="Q2" gate="G$1" pin="1"/>
<label x="-33.02" y="-63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="TCK" class="0">
<segment>
<wire x1="-152.4" y1="-50.8" x2="-167.64" y2="-50.8" width="0.1524" layer="91"/>
<label x="-160.02" y="-50.8" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-71.12" y1="-48.26" x2="-86.36" y2="-48.26" width="0.1524" layer="91"/>
<label x="-86.36" y="-48.26" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TCK"/>
</segment>
</net>
<net name="TDO" class="0">
<segment>
<wire x1="-152.4" y1="-48.26" x2="-167.64" y2="-48.26" width="0.1524" layer="91"/>
<label x="-160.02" y="-48.26" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="-35.56" y1="-25.4" x2="-20.32" y2="-25.4" width="0.1524" layer="91"/>
<label x="-33.02" y="-25.4" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TDO"/>
</segment>
</net>
<net name="TMS" class="0">
<segment>
<label x="-160.02" y="-45.72" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="5"/>
<wire x1="-152.4" y1="-45.72" x2="-167.64" y2="-45.72" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-71.12" y1="-45.72" x2="-86.36" y2="-45.72" width="0.1524" layer="91"/>
<label x="-86.36" y="-45.72" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TMS"/>
</segment>
</net>
<net name="TDI" class="0">
<segment>
<wire x1="-152.4" y1="-40.64" x2="-167.64" y2="-40.64" width="0.1524" layer="91"/>
<label x="-160.02" y="-40.64" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="9"/>
</segment>
<segment>
<wire x1="-71.12" y1="-50.8" x2="-86.36" y2="-50.8" width="0.1524" layer="91"/>
<label x="-86.36" y="-50.8" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="TDI"/>
</segment>
</net>
<net name="EVTO" class="0">
<segment>
<wire x1="-167.64" y1="-43.18" x2="-152.4" y2="-43.18" width="0.1524" layer="91"/>
<label x="-160.02" y="-43.18" size="1.778" layer="95"/>
<pinref part="JTAG1" gate="G$1" pin="7"/>
</segment>
</net>
<net name="MII_TXD_0" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_0"/>
<wire x1="144.78" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="91"/>
<label x="121.92" y="71.12" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB02"/>
<wire x1="-71.12" y1="-88.9" x2="-93.98" y2="-88.9" width="0.1524" layer="91"/>
<label x="-91.44" y="-88.9" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_1" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_1"/>
<wire x1="144.78" y1="68.58" x2="119.38" y2="68.58" width="0.1524" layer="91"/>
<label x="121.92" y="68.58" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB03"/>
<wire x1="-71.12" y1="-91.44" x2="-93.98" y2="-91.44" width="0.1524" layer="91"/>
<label x="-91.44" y="-91.44" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_2" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_2"/>
<wire x1="144.78" y1="66.04" x2="119.38" y2="66.04" width="0.1524" layer="91"/>
<label x="121.92" y="66.04" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB10"/>
<wire x1="-71.12" y1="-109.22" x2="-93.98" y2="-109.22" width="0.1524" layer="91"/>
<label x="-91.44" y="-109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TXD_3" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TXD_3"/>
<wire x1="144.78" y1="63.5" x2="119.38" y2="63.5" width="0.1524" layer="91"/>
<label x="121.92" y="63.5" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB11"/>
<wire x1="-35.56" y1="-30.48" x2="-10.16" y2="-30.48" width="0.1524" layer="91"/>
<label x="-33.02" y="-30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TX_EN" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TX_EN"/>
<wire x1="144.78" y1="60.96" x2="119.38" y2="60.96" width="0.1524" layer="91"/>
<label x="121.92" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB01"/>
<wire x1="-71.12" y1="-86.36" x2="-93.98" y2="-86.36" width="0.1524" layer="91"/>
<label x="-91.44" y="-86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_/PWRDN" class="0">
<segment>
<pinref part="U8" gate="A" pin="PWRDNN/INT"/>
<wire x1="144.78" y1="58.42" x2="119.38" y2="58.42" width="0.1524" layer="91"/>
<label x="121.92" y="58.42" size="1.778" layer="95"/>
<pinref part="R95" gate="G$1" pin="2"/>
</segment>
</net>
<net name="ETHER_/RST" class="0">
<segment>
<pinref part="U8" gate="A" pin="RESETN"/>
<wire x1="144.78" y1="55.88" x2="119.38" y2="55.88" width="0.1524" layer="91"/>
<label x="121.92" y="55.88" size="1.778" layer="95"/>
<pinref part="R96" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PX38"/>
<wire x1="-35.56" y1="-119.38" x2="-10.16" y2="-119.38" width="0.1524" layer="91"/>
<label x="-33.02" y="-119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_TX_CLK" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_TX_CLK"/>
<wire x1="220.98" y1="88.9" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<label x="226.06" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB00"/>
<wire x1="-71.12" y1="-83.82" x2="-93.98" y2="-83.82" width="0.1524" layer="91"/>
<label x="-91.44" y="-83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_COL" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_COL/PHYAD0"/>
<wire x1="220.98" y1="83.82" x2="248.92" y2="83.82" width="0.1524" layer="91"/>
<label x="226.06" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB16"/>
<wire x1="-35.56" y1="-43.18" x2="-10.16" y2="-43.18" width="0.1524" layer="91"/>
<label x="-33.02" y="-43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_0" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_0/PHYAD1"/>
<wire x1="220.98" y1="81.28" x2="248.92" y2="81.28" width="0.1524" layer="91"/>
<label x="226.06" y="81.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB05"/>
<wire x1="-71.12" y1="-96.52" x2="-93.98" y2="-96.52" width="0.1524" layer="91"/>
<label x="-91.44" y="-96.52" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_1" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_1/PHYAD2"/>
<wire x1="220.98" y1="78.74" x2="248.92" y2="78.74" width="0.1524" layer="91"/>
<label x="226.06" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB06"/>
<wire x1="-71.12" y1="-99.06" x2="-93.98" y2="-99.06" width="0.1524" layer="91"/>
<label x="-91.44" y="-99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_2" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_2/PHYAD3"/>
<wire x1="220.98" y1="76.2" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<label x="226.06" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB13"/>
<wire x1="-35.56" y1="-35.56" x2="-10.16" y2="-35.56" width="0.1524" layer="91"/>
<label x="-33.02" y="-35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RXD_3" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RXD_3/PHYAD4"/>
<wire x1="220.98" y1="73.66" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<label x="226.06" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB14"/>
<wire x1="-35.56" y1="-38.1" x2="-10.16" y2="-38.1" width="0.1524" layer="91"/>
<label x="-33.02" y="-38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_LINK" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_LINK/AN_0"/>
<wire x1="220.98" y1="68.58" x2="248.92" y2="68.58" width="0.1524" layer="91"/>
<label x="226.06" y="68.58" size="1.778" layer="95"/>
<pinref part="R98" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R12" gate="G$1" pin="1"/>
<wire x1="416.56" y1="50.8" x2="447.04" y2="50.8" width="0.1524" layer="91"/>
<label x="421.64" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_SPEED" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_SPEED/AN_1"/>
<wire x1="220.98" y1="66.04" x2="248.92" y2="66.04" width="0.1524" layer="91"/>
<label x="226.06" y="66.04" size="1.778" layer="95"/>
<pinref part="R99" gate="G$1" pin="1"/>
</segment>
</net>
<net name="MII_CRS" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_CRS/LED_CFG"/>
<wire x1="220.98" y1="50.8" x2="248.92" y2="50.8" width="0.1524" layer="91"/>
<label x="226.06" y="50.8" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB04"/>
<wire x1="-71.12" y1="-93.98" x2="-93.98" y2="-93.98" width="0.1524" layer="91"/>
<label x="-91.44" y="-93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_CLK" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_CLK"/>
<wire x1="220.98" y1="48.26" x2="248.92" y2="48.26" width="0.1524" layer="91"/>
<label x="226.06" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB17"/>
<wire x1="-35.56" y1="-45.72" x2="-10.16" y2="-45.72" width="0.1524" layer="91"/>
<label x="-33.02" y="-45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_DV" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_DV"/>
<wire x1="220.98" y1="45.72" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<label x="226.06" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB15"/>
<wire x1="-35.56" y1="-40.64" x2="-10.16" y2="-40.64" width="0.1524" layer="91"/>
<label x="-33.02" y="-40.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="MII_RX_ERR" class="0">
<segment>
<pinref part="U8" gate="A" pin="MII_RX_ERR/MDIX_EN"/>
<wire x1="220.98" y1="43.18" x2="248.92" y2="43.18" width="0.1524" layer="91"/>
<label x="226.06" y="43.18" size="1.778" layer="95"/>
<pinref part="R97" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB07"/>
<wire x1="-71.12" y1="-101.6" x2="-93.98" y2="-101.6" width="0.1524" layer="91"/>
<label x="-91.44" y="-101.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_LED_ACT" class="0">
<segment>
<pinref part="U8" gate="A" pin="LED_ACT/AN_EN"/>
<wire x1="220.98" y1="40.64" x2="248.92" y2="40.64" width="0.1524" layer="91"/>
<label x="226.06" y="40.64" size="1.778" layer="95"/>
<pinref part="R100" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="R13" gate="G$1" pin="1"/>
<wire x1="416.56" y1="33.02" x2="447.04" y2="33.02" width="0.1524" layer="91"/>
<label x="421.64" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_V18" class="0">
<segment>
<pinref part="U8" gate="A" pin="V18_PFBIN1"/>
<wire x1="144.78" y1="33.02" x2="119.38" y2="33.02" width="0.1524" layer="91"/>
<label x="121.92" y="33.02" size="1.778" layer="95"/>
<wire x1="213.36" y1="-10.16" x2="210.82" y2="-10.16" width="0.1524" layer="91"/>
<label x="190.5" y="-10.16" size="1.778" layer="95"/>
<pinref part="C65" gate="G$1" pin="1"/>
<wire x1="210.82" y1="-10.16" x2="200.66" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="200.66" y1="-10.16" x2="190.5" y2="-10.16" width="0.1524" layer="91"/>
<wire x1="190.5" y1="-10.16" x2="187.96" y2="-10.16" width="0.1524" layer="91"/>
<junction x="190.5" y="-10.16"/>
<pinref part="C66" gate="G$1" pin="1"/>
<junction x="200.66" y="-10.16"/>
<pinref part="C67" gate="G$1" pin="1"/>
<junction x="210.82" y="-10.16"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="V18_PFBIN2"/>
<wire x1="144.78" y1="30.48" x2="119.38" y2="30.48" width="0.1524" layer="91"/>
<label x="121.92" y="30.48" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="V18_PFBOUT"/>
<wire x1="220.98" y1="58.42" x2="248.92" y2="58.42" width="0.1524" layer="91"/>
<label x="226.06" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_VA11" class="0">
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBIN1"/>
<wire x1="119.38" y1="78.74" x2="144.78" y2="78.74" width="0.1524" layer="91"/>
<label x="121.92" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBIN2"/>
<wire x1="119.38" y1="76.2" x2="144.78" y2="76.2" width="0.1524" layer="91"/>
<label x="121.92" y="76.2" size="1.778" layer="95"/>
<wire x1="139.7" y1="-10.16" x2="142.24" y2="-10.16" width="0.1524" layer="91"/>
<label x="142.24" y="-10.16" size="1.778" layer="95"/>
<pinref part="C62" gate="G$1" pin="1"/>
<wire x1="142.24" y1="-10.16" x2="152.4" y2="-10.16" width="0.1524" layer="91"/>
<junction x="142.24" y="-10.16"/>
<pinref part="C63" gate="G$1" pin="1"/>
<wire x1="152.4" y1="-10.16" x2="162.56" y2="-10.16" width="0.1524" layer="91"/>
<junction x="152.4" y="-10.16"/>
<pinref part="C64" gate="G$1" pin="1"/>
<wire x1="162.56" y1="-10.16" x2="165.1" y2="-10.16" width="0.1524" layer="91"/>
<junction x="162.56" y="-10.16"/>
</segment>
<segment>
<pinref part="U8" gate="A" pin="VA11_PFBOUT"/>
<wire x1="220.98" y1="93.98" x2="248.92" y2="93.98" width="0.1524" layer="91"/>
<label x="226.06" y="93.98" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_VDD11" class="0">
<segment>
<pinref part="U8" gate="A" pin="VDD11"/>
<wire x1="220.98" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<label x="226.06" y="53.34" size="1.778" layer="95"/>
<wire x1="231.14" y1="-10.16" x2="236.22" y2="-10.16" width="0.1524" layer="91"/>
<label x="233.68" y="-10.16" size="1.778" layer="95"/>
<pinref part="C68" gate="G$1" pin="1"/>
<wire x1="236.22" y1="-10.16" x2="259.08" y2="-10.16" width="0.1524" layer="91"/>
<junction x="236.22" y="-10.16"/>
</segment>
</net>
<net name="ETHER_XO" class="0">
<segment>
<pinref part="U8" gate="A" pin="XO"/>
<wire x1="220.98" y1="60.96" x2="248.92" y2="60.96" width="0.1524" layer="91"/>
<label x="226.06" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="1"/>
<wire x1="175.26" y1="132.08" x2="165.1" y2="132.08" width="0.1524" layer="91"/>
<label x="149.86" y="132.08" size="1.778" layer="95"/>
<pinref part="C78" gate="G$1" pin="1"/>
<wire x1="165.1" y1="132.08" x2="147.32" y2="132.08" width="0.1524" layer="91"/>
<junction x="165.1" y="132.08"/>
</segment>
</net>
<net name="ETHER_XI" class="0">
<segment>
<pinref part="U8" gate="A" pin="XI"/>
<wire x1="144.78" y1="38.1" x2="119.38" y2="38.1" width="0.1524" layer="91"/>
<label x="121.92" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="Q1" gate="G$1" pin="2"/>
<wire x1="180.34" y1="132.08" x2="190.5" y2="132.08" width="0.1524" layer="91"/>
<label x="193.04" y="132.08" size="1.778" layer="95"/>
<pinref part="C79" gate="G$1" pin="1"/>
<wire x1="190.5" y1="132.08" x2="210.82" y2="132.08" width="0.1524" layer="91"/>
<junction x="190.5" y="132.08"/>
</segment>
</net>
<net name="ETHER_MDC" class="0">
<segment>
<pinref part="U8" gate="A" pin="MDC"/>
<wire x1="144.78" y1="40.64" x2="119.38" y2="40.64" width="0.1524" layer="91"/>
<label x="121.92" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB08"/>
<wire x1="-71.12" y1="-104.14" x2="-93.98" y2="-104.14" width="0.1524" layer="91"/>
<label x="-91.44" y="-104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_MDIO" class="0">
<segment>
<pinref part="U8" gate="A" pin="MDIO"/>
<wire x1="220.98" y1="25.4" x2="246.38" y2="25.4" width="0.1524" layer="91"/>
<label x="226.06" y="25.4" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="B" pin="PB09"/>
<wire x1="-71.12" y1="-106.68" x2="-93.98" y2="-106.68" width="0.1524" layer="91"/>
<label x="-91.44" y="-106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RBIAS" class="0">
<segment>
<pinref part="U8" gate="A" pin="RBIAS"/>
<pinref part="R88" gate="G$1" pin="2"/>
<wire x1="116.84" y1="43.18" x2="144.78" y2="43.18" width="0.1524" layer="91"/>
<label x="121.92" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RD-" class="0">
<segment>
<wire x1="220.98" y1="35.56" x2="330.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="330.2" y1="68.58" x2="330.2" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="TD-"/>
<wire x1="330.2" y1="48.26" x2="330.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="360.68" y1="48.26" x2="330.2" y2="48.26" width="0.1524" layer="91"/>
<junction x="330.2" y="48.26"/>
<label x="337.82" y="48.26" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="RD-"/>
<pinref part="R92" gate="G$1" pin="2"/>
<label x="226.06" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_RD+" class="0">
<segment>
<wire x1="220.98" y1="33.02" x2="322.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="322.58" y1="68.58" x2="322.58" y2="53.34" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="TD+"/>
<wire x1="322.58" y1="53.34" x2="322.58" y2="33.02" width="0.1524" layer="91"/>
<wire x1="360.68" y1="53.34" x2="322.58" y2="53.34" width="0.1524" layer="91"/>
<junction x="322.58" y="53.34"/>
<label x="337.82" y="53.34" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="RD+"/>
<pinref part="R91" gate="G$1" pin="2"/>
<label x="226.06" y="33.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_TD-" class="0">
<segment>
<wire x1="220.98" y1="30.48" x2="314.96" y2="30.48" width="0.1524" layer="91"/>
<wire x1="314.96" y1="68.58" x2="314.96" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RD-"/>
<wire x1="314.96" y1="38.1" x2="314.96" y2="30.48" width="0.1524" layer="91"/>
<wire x1="360.68" y1="38.1" x2="314.96" y2="38.1" width="0.1524" layer="91"/>
<junction x="314.96" y="38.1"/>
<label x="337.82" y="38.1" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="TD-"/>
<pinref part="R90" gate="G$1" pin="2"/>
<label x="226.06" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="ETHER_TD+" class="0">
<segment>
<wire x1="220.98" y1="27.94" x2="307.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="307.34" y1="68.58" x2="307.34" y2="43.18" width="0.1524" layer="91"/>
<pinref part="U2" gate="G$1" pin="RD+"/>
<wire x1="307.34" y1="43.18" x2="307.34" y2="27.94" width="0.1524" layer="91"/>
<wire x1="360.68" y1="43.18" x2="307.34" y2="43.18" width="0.1524" layer="91"/>
<junction x="307.34" y="43.18"/>
<label x="337.82" y="43.18" size="1.778" layer="95"/>
<pinref part="U8" gate="A" pin="TD+"/>
<pinref part="R89" gate="G$1" pin="2"/>
<label x="226.06" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="SPI_MISO" class="0">
<segment>
<wire x1="-327.66" y1="-58.42" x2="-297.18" y2="-58.42" width="0.1524" layer="91"/>
<label x="-325.12" y="-58.42" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DOUT"/>
</segment>
<segment>
<wire x1="-35.56" y1="-91.44" x2="-10.16" y2="-91.44" width="0.1524" layer="91"/>
<label x="-33.02" y="-91.44" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX27"/>
</segment>
</net>
<net name="SPI_CLK" class="0">
<segment>
<wire x1="-327.66" y1="-53.34" x2="-297.18" y2="-53.34" width="0.1524" layer="91"/>
<label x="-325.12" y="-53.34" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="SCLK"/>
</segment>
<segment>
<wire x1="-35.56" y1="-96.52" x2="-10.16" y2="-96.52" width="0.1524" layer="91"/>
<label x="-33.02" y="-96.52" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX29"/>
</segment>
</net>
<net name="SPI_MOSI" class="0">
<segment>
<wire x1="-327.66" y1="-63.5" x2="-297.18" y2="-63.5" width="0.1524" layer="91"/>
<label x="-325.12" y="-63.5" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="DIN"/>
</segment>
<segment>
<wire x1="-35.56" y1="-93.98" x2="-10.16" y2="-93.98" width="0.1524" layer="91"/>
<label x="-33.02" y="-93.98" size="1.778" layer="95"/>
<pinref part="U1" gate="B" pin="PX28"/>
</segment>
</net>
<net name="GYRO_/CS" class="0">
<segment>
<wire x1="-327.66" y1="-68.58" x2="-297.18" y2="-68.58" width="0.1524" layer="91"/>
<label x="-325.12" y="-68.58" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="/CS"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="PX14"/>
<wire x1="-30.48" y1="45.72" x2="2.54" y2="45.72" width="0.1524" layer="91"/>
<label x="-27.94" y="45.72" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_/RST" class="0">
<segment>
<wire x1="-297.18" y1="-83.82" x2="-327.66" y2="-83.82" width="0.1524" layer="91"/>
<label x="-325.12" y="-83.82" size="1.778" layer="95"/>
<pinref part="U$1" gate="G$1" pin="/RST"/>
</segment>
<segment>
<pinref part="U1" gate="A" pin="PX15"/>
<wire x1="-30.48" y1="43.18" x2="2.54" y2="43.18" width="0.1524" layer="91"/>
<label x="-27.94" y="43.18" size="1.778" layer="95"/>
</segment>
</net>
<net name="GYRO_SYNC" class="0">
<segment>
<pinref part="U1" gate="A" pin="PX16"/>
<wire x1="-30.48" y1="40.64" x2="2.54" y2="40.64" width="0.1524" layer="91"/>
<label x="-27.94" y="40.64" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="DIO2"/>
<wire x1="-251.46" y1="-66.04" x2="-226.06" y2="-66.04" width="0.1524" layer="91"/>
<label x="-246.38" y="-66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="POT0" class="0">
<segment>
<wire x1="205.74" y1="-162.56" x2="256.54" y2="-162.56" width="0.1524" layer="91"/>
<label x="220.98" y="-162.56" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH0"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-238.76" x2="213.36" y2="-238.76" width="0.1524" layer="91"/>
<label x="215.9" y="-238.76" size="1.778" layer="95"/>
<pinref part="Z1" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT1" class="0">
<segment>
<wire x1="205.74" y1="-165.1" x2="256.54" y2="-165.1" width="0.1524" layer="91"/>
<label x="220.98" y="-165.1" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH1"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-256.54" x2="213.36" y2="-256.54" width="0.1524" layer="91"/>
<label x="215.9" y="-256.54" size="1.778" layer="95"/>
<pinref part="Z2" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT2" class="0">
<segment>
<wire x1="205.74" y1="-167.64" x2="256.54" y2="-167.64" width="0.1524" layer="91"/>
<label x="220.98" y="-167.64" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH2"/>
</segment>
<segment>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-274.32" x2="213.36" y2="-274.32" width="0.1524" layer="91"/>
<label x="215.9" y="-274.32" size="1.778" layer="95"/>
<pinref part="Z3" gate="G$1" pin="C"/>
</segment>
</net>
<net name="POT4" class="0">
<segment>
<wire x1="205.74" y1="-172.72" x2="256.54" y2="-172.72" width="0.1524" layer="91"/>
<label x="220.98" y="-172.72" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH4"/>
</segment>
</net>
<net name="POT5" class="0">
<segment>
<wire x1="205.74" y1="-175.26" x2="256.54" y2="-175.26" width="0.1524" layer="91"/>
<label x="220.98" y="-175.26" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH5"/>
</segment>
</net>
<net name="POT6" class="0">
<segment>
<wire x1="205.74" y1="-177.8" x2="256.54" y2="-177.8" width="0.1524" layer="91"/>
<label x="220.98" y="-177.8" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH6"/>
</segment>
</net>
<net name="POT7" class="0">
<segment>
<wire x1="205.74" y1="-180.34" x2="256.54" y2="-180.34" width="0.1524" layer="91"/>
<label x="220.98" y="-180.34" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH7"/>
</segment>
</net>
<net name="POT3" class="0">
<segment>
<wire x1="205.74" y1="-170.18" x2="256.54" y2="-170.18" width="0.1524" layer="91"/>
<label x="220.98" y="-170.18" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="CH3"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<wire x1="228.6" y1="-292.1" x2="213.36" y2="-292.1" width="0.1524" layer="91"/>
<label x="215.9" y="-292.1" size="1.778" layer="95"/>
<pinref part="Z4" gate="G$1" pin="C"/>
</segment>
</net>
<net name="ADC_/CS" class="0">
<segment>
<wire x1="281.94" y1="-175.26" x2="325.12" y2="-175.26" width="0.1524" layer="91"/>
<label x="287.02" y="-175.26" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="!CS"/>
<pinref part="R4" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="2.54" y1="58.42" x2="-30.48" y2="58.42" width="0.1524" layer="91"/>
<label x="-27.94" y="58.42" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX09"/>
</segment>
</net>
<net name="ADC_CLK" class="0">
<segment>
<wire x1="281.94" y1="-172.72" x2="325.12" y2="-172.72" width="0.1524" layer="91"/>
<label x="287.02" y="-172.72" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DCLK"/>
</segment>
<segment>
<wire x1="-30.48" y1="60.96" x2="2.54" y2="60.96" width="0.1524" layer="91"/>
<label x="-27.94" y="60.96" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX08"/>
</segment>
</net>
<net name="ADC0_BUSY" class="0">
<segment>
<wire x1="325.12" y1="-180.34" x2="281.94" y2="-180.34" width="0.1524" layer="91"/>
<label x="287.02" y="-180.34" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="BUSY"/>
</segment>
<segment>
<wire x1="2.54" y1="63.5" x2="-30.48" y2="63.5" width="0.1524" layer="91"/>
<label x="-27.94" y="63.5" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX07"/>
</segment>
</net>
<net name="ADC0_DI" class="0">
<segment>
<wire x1="281.94" y1="-177.8" x2="325.12" y2="-177.8" width="0.1524" layer="91"/>
<label x="287.02" y="-177.8" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DI"/>
</segment>
<segment>
<wire x1="2.54" y1="68.58" x2="-30.48" y2="68.58" width="0.1524" layer="91"/>
<label x="-27.94" y="68.58" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX05"/>
</segment>
</net>
<net name="ADC0_DO" class="0">
<segment>
<wire x1="281.94" y1="-182.88" x2="325.12" y2="-182.88" width="0.1524" layer="91"/>
<label x="287.02" y="-182.88" size="1.778" layer="95"/>
<pinref part="IC17" gate="G$1" pin="DOUT"/>
</segment>
<segment>
<wire x1="-30.48" y1="66.04" x2="2.54" y2="66.04" width="0.1524" layer="91"/>
<label x="-27.94" y="66.04" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PX06"/>
</segment>
</net>
<net name="VPOT" class="0">
<segment>
<pinref part="R81" gate="G$1" pin="2"/>
<wire x1="180.34" y1="-139.7" x2="195.58" y2="-139.7" width="0.1524" layer="91"/>
<label x="185.42" y="-139.7" size="1.778" layer="95"/>
<pinref part="C40" gate="G$1" pin="1"/>
<wire x1="195.58" y1="-139.7" x2="200.66" y2="-139.7" width="0.1524" layer="91"/>
<junction x="195.58" y="-139.7"/>
</segment>
<segment>
<wire x1="243.84" y1="-233.68" x2="185.42" y2="-233.68" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-233.68" x2="185.42" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-251.46" x2="243.84" y2="-251.46" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-251.46" x2="185.42" y2="-269.24" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-269.24" x2="243.84" y2="-269.24" width="0.1524" layer="91"/>
<junction x="185.42" y="-251.46"/>
<wire x1="185.42" y1="-269.24" x2="185.42" y2="-287.02" width="0.1524" layer="91"/>
<wire x1="185.42" y1="-287.02" x2="243.84" y2="-287.02" width="0.1524" layer="91"/>
<junction x="185.42" y="-269.24"/>
<label x="218.44" y="-233.68" size="1.778" layer="95"/>
<pinref part="XP" gate="1" pin="S"/>
<pinref part="XP" gate="8" pin="S"/>
<pinref part="XP" gate="4" pin="S"/>
<pinref part="XP" gate="11" pin="S"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R8" gate="G$1" pin="2"/>
<wire x1="238.76" y1="-292.1" x2="243.84" y2="-292.1" width="0.1524" layer="91"/>
<pinref part="XP" gate="6" pin="S"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="238.76" y1="-274.32" x2="243.84" y2="-274.32" width="0.1524" layer="91"/>
<pinref part="XP" gate="10" pin="S"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="238.76" y1="-256.54" x2="243.84" y2="-256.54" width="0.1524" layer="91"/>
<pinref part="XP" gate="3" pin="S"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="238.76" y1="-238.76" x2="243.84" y2="-238.76" width="0.1524" layer="91"/>
<pinref part="XP" gate="7" pin="S"/>
</segment>
</net>
<net name="CONS_RX" class="0">
<segment>
<pinref part="CONS" gate="1" pin="4"/>
<wire x1="-170.18" y1="43.18" x2="-124.46" y2="43.18" width="0.1524" layer="91"/>
<wire x1="-124.46" y1="43.18" x2="-124.46" y2="78.74" width="0.1524" layer="91"/>
<label x="-167.64" y="43.18" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA00"/>
<wire x1="-124.46" y1="78.74" x2="-76.2" y2="78.74" width="0.1524" layer="91"/>
<label x="-101.6" y="78.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="CONS_TX" class="0">
<segment>
<pinref part="CONS" gate="1" pin="5"/>
<wire x1="-170.18" y1="45.72" x2="-127" y2="45.72" width="0.1524" layer="91"/>
<wire x1="-127" y1="45.72" x2="-127" y2="76.2" width="0.1524" layer="91"/>
<label x="-167.64" y="45.72" size="1.778" layer="95"/>
<pinref part="U1" gate="A" pin="PA01"/>
<wire x1="-127" y1="76.2" x2="-76.2" y2="76.2" width="0.1524" layer="91"/>
<label x="-101.6" y="76.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<wire x1="396.24" y1="50.8" x2="406.4" y2="50.8" width="0.1524" layer="91"/>
<pinref part="R12" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="YLED-"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<wire x1="396.24" y1="33.02" x2="406.4" y2="33.02" width="0.1524" layer="91"/>
<pinref part="R13" gate="G$1" pin="2"/>
<pinref part="U2" gate="G$1" pin="GLED-"/>
</segment>
</net>
<net name="POE+" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="V+"/>
<wire x1="360.68" y1="60.96" x2="340.36" y2="60.96" width="0.1524" layer="91"/>
<label x="342.9" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="POE-" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="V-"/>
<wire x1="360.68" y1="58.42" x2="340.36" y2="58.42" width="0.1524" layer="91"/>
<label x="342.9" y="58.42" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
</plain>
<instances>
<instance part="IC6" gate="G$1" x="-50.8" y="20.32"/>
<instance part="U$3" gate="G$1" x="-73.66" y="27.94" rot="R90"/>
<instance part="C13" gate="G$1" x="-10.16" y="-12.7"/>
<instance part="U$4" gate="G$1" x="25.4" y="-12.7"/>
<instance part="GND27" gate="1" x="43.18" y="-17.78" rot="R90"/>
<instance part="+3V2" gate="G$1" x="43.18" y="-7.62" rot="R270"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="1">
<segment>
<pinref part="U$4" gate="G$1" pin="VOUT-"/>
<pinref part="GND27" gate="1" pin="GND"/>
</segment>
</net>
<net name="POE-" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="VIN"/>
<wire x1="-63.5" y1="17.78" x2="-73.66" y2="17.78" width="0.1524" layer="91"/>
<label x="-83.82" y="17.78" size="1.778" layer="95"/>
<pinref part="U$3" gate="G$1" pin="A"/>
<wire x1="-73.66" y1="17.78" x2="-88.9" y2="17.78" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="25.4" x2="-73.66" y2="17.78" width="0.1524" layer="91"/>
<junction x="-73.66" y="17.78"/>
</segment>
</net>
<net name="POE+" class="0">
<segment>
<pinref part="IC6" gate="G$1" pin="GND"/>
<wire x1="-38.1" y1="22.86" x2="-12.7" y2="22.86" width="0.1524" layer="91"/>
<label x="-33.02" y="22.86" size="1.778" layer="95"/>
<wire x1="-12.7" y1="-7.62" x2="-10.16" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-10.16" x2="-10.16" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="22.86" x2="-12.7" y2="-7.62" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="C"/>
<wire x1="-73.66" y1="30.48" x2="-73.66" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-73.66" y1="35.56" x2="-12.7" y2="35.56" width="0.1524" layer="91"/>
<wire x1="-12.7" y1="35.56" x2="-12.7" y2="22.86" width="0.1524" layer="91"/>
<junction x="-12.7" y="22.86"/>
<pinref part="C13" gate="G$1" pin="1"/>
<pinref part="U$4" gate="G$1" pin="VIN+"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.1524" layer="91"/>
<junction x="-10.16" y="-7.62"/>
</segment>
</net>
<net name="POESW-" class="1">
<segment>
<wire x1="-10.16" y1="-17.78" x2="-10.16" y2="-20.32" width="0.1524" layer="91"/>
<junction x="-10.16" y="-20.32"/>
<wire x1="-10.16" y1="-20.32" x2="-22.86" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="-22.86" y1="-20.32" x2="-22.86" y2="17.78" width="0.1524" layer="91"/>
<pinref part="IC6" gate="G$1" pin="VOUT"/>
<wire x1="-22.86" y1="17.78" x2="-38.1" y2="17.78" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<pinref part="U$4" gate="G$1" pin="VIN-"/>
<wire x1="10.16" y1="-17.78" x2="0" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="0" y1="-17.78" x2="0" y2="-20.32" width="0.1524" layer="91"/>
<wire x1="0" y1="-20.32" x2="-10.16" y2="-20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="VOUT+"/>
<pinref part="+3V2" gate="G$1" pin="+3V3"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
