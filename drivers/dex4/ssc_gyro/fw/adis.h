#pragma once
#include "common/std_headers.h"

#include "dspcore/dspcore.h"
#include "embedded/embedded_pktcom.h"
#include "embedded/embedded_timing.h"

typedef struct adis_sample_t {
  U64 sample_ticks;
  U32 seqno;

  // These follow the names of the ADIS16480 registers
  S32 gyro_x, gyro_y, gyro_z;
  S32 accel_x, accel_y, accel_z;
  S32 deltang_x, deltang_y, deltang_z;
  S32 deltvel_x, deltvel_y, deltvel_z;
  S32 barom;
  S16 orient_11, orient_12, orient_13;
  S16 orient_21, orient_22, orient_23;
  S16 orient_31, orient_32, orient_33;
  S16 mag_x, mag_y, mag_z;
  S16 temperature;

  U16 sys_e_flag;
  U16 diag_sts;
  U16 alm_sts;
} adis_sample_t;

#define ADIS_SAMPLE_Q_SIZE 8

enum spi_mode_t {
  SPI_MODE_IDLE,
  SPI_MODE_GETSAMPLE,
  SPI_MODE_READREG16,
  SPI_MODE_WRITEREG8,
  SPI_MODE_WRITEREG16,
  SPI_MODE_DRAIN
};

typedef struct adis_engine_t {
  // Hardware config
  volatile struct avr32_spi_t *spi;
  int nrst_gpio_pin;
  int ncs_gpio_pin;
  int sync_pin;
  int spi_npcs;

  // Updated every interrupt
  enum spi_mode_t spi_mode;
  int spi_phase;
  U32 sync_count;
  U64 sync_time;
  int interrupt_profile[4];

  // Interface between main and interrupt code
  U16 writereg_addr;
  U16 writereg_value;

  U16 readreg_addr;
  U16 readreg_value;

  // Written by ISR
  U32 spi_accum;
  adis_sample_t cursample;

  // Read from gyro at startup
  U16 serial_num;
  U16 prod_id;
  U16 firm_rev;
  U16 firm_dm, firm_y;
  U16 ekf_cnfg_rd;

  // Queue of sample results. ISR pushes to head, client pops from tail
  int sample_q_head, sample_q_tail, sample_q_overruns, sample_q_deliveries;
  adis_sample_t sample_q[ADIS_SAMPLE_Q_SIZE];

} adis_engine_t;

extern adis_engine_t adis0;

void adis_dump_registers();
void adis_setup();
void adis_engine_poll(adis_engine_t *it);
void adis_engine_log_status(adis_engine_t *it, const char *name);

int adis_get_sample(volatile adis_engine_t *it, adis_sample_t *sample);
void adis_set_pwr_on(int v);

void pkt_tx_adis_sample(pkt_tx_buf *tx, adis_sample_t *s);
