#pragma once
#include "common/std_headers.h"

void ssc_gyro_setup(void);
void ssc_gyro_work(void);
void ssc_gyro_log_status(void);
