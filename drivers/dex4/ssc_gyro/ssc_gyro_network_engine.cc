#include "../../../comm/ssc_network_engine.h"
#include "../../../jit/runtime.h"
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/string_cast.hpp>
#include "./ssc_gyro_io.h"

struct SscGyroNetworkEngine : SscNetworkEngine {
  void setupHandlers() override;
  void setupAccessors() override;

  shared_ptr<YogaTimeseq> imuOut;
  unique_ptr<ImuAccessorSuite> imuAs;
};

void SscGyroNetworkEngine::setupAccessors()
{
  SscNetworkEngine::setupAccessors();
  imuAs = make_unique<ImuAccessorSuite>(imuOut->type);
}

void SscGyroNetworkEngine::setupHandlers()
{
  SscNetworkEngine::setupHandlers();
  updateHandlers[(U8)'a'] = [this](R rxTime, packet &rx, U64 pktTicks, bool &error) {
    R ts;
    rxSscTimestamp(*this, rx, ts, rxTime, pktTicks);
    U32 seqno;
    rx.get(seqno);
    auto imu0 = imuOut->addNew(ts);
    imuAs->rxState(rx, imu0);
  };
}

static EngineRegister sscGyroNetworkEngineReg("sscGyroNetwork",
  {
    "SscGyroImu"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 1) {
      return ctx.logError("sscGyroNetwork: expected 1 argument (imu)");
    }

    auto e = make_shared<SscGyroNetworkEngine>();
    if (args.size() > 0) {
      e->imuOut = args[0];
      e->imuOut->needBackdateInitial = true;
      e->imuOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    return true;
  }
);
