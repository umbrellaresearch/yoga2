'use strict';

const assert = require('assert');
const _ = require('lodash');
const util = require('util');
const path = require('path');
const net = require('net');
const vjs_topology = require('tlbcore/web/vjs_topology');
const yb = require('../binary');

describe('BaristaEngine', function() {
  it('should work', function(itCb) {

    let addr = vjs_topology.getLocalServer().bestAddr;
    let controlScript = `

Program
BeforeStart
  open = socket_open("${addr}",20021)
  Loop open = False 
    open = socket_open("${addr}",20021)
  targetPos = p[0,0,0,0,0,0]
  counter = 6
Robot Program
  sendToServer = 'send to server'
  socket_send_string(sendToServer)
  receiveFromServ = socket_read_ascii_float(6)
  Loop receiveFromServ[0] != 6
    Wait: 0.3
    receiveFromServ = socket_read_ascii_float(6)
  Loop counter < 6
    targetPos[counter] = receiveFromServ[counter+1]
    counter = counter+1
  MoveJ
    targetPos
`;

    let e = new yb.BaristaEngine({
      config: {
        robotHostname: 'localhost',
        controlScript,
      },
    });
    e.label = 'barista';
    e.verbose = 2;


    let monitorServer = net.createServer((c) => {
      console.log('Monitor server connected');

      c.on('data', (data) => {
        console.log('Monitor got', data.toString());
      });
      c.on('end', () => {
        console.log('Monitor server disconnected');
      });
    });
    monitorServer.listen(30004);

    let controlServer = net.createServer((c) => {
      console.log('Control server connected');
      c.on('data', (data) => {
        console.log('Control got', data.toString());
      });
      c.on('end', () => {
        console.log('Control server disconnected');
      });
    });
    controlServer.listen(30003);


    e.start();

    setTimeout(() => {

      let qsock = net.createConnection(20021, 'localhost', () => {
        console.log(`qsock connected`);
        qsock.on('data', (d) => {
          console.log(`qsock data ${d}`);
        });
        qsock.write('1,2,3,4\n');
        setTimeout(() => {
          qsock.write('5,6\n');
          qsock.write('7,8\n9,10\n');
          setTimeout(() => {
            qsock.write('xxxxyyyy');
            setTimeout(() => {
              qsock.write('foo\n');
            }, 10);
          }, 10);
        }, 10);
      });
      qsock.setNoDelay(true);

      setTimeout(() => {

          monitorServer.close();
          controlServer.close();
          e.stop();

        itCb(null);
      }, 500);
    }, 200);
  });
});
