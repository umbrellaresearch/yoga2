#pragma once
#include "common/std_headers.h"

#include "common/uv_wrappers.h"
#include "../../../deps/nlohmann-json/json.hpp"

struct UniversalNetworkEngine;

static inline void extendRxBuf(vector<u_char> &rxBuf, ssize_t nr, uv_buf_t const *buf)
{
  size_t oldSize = rxBuf.size();
  rxBuf.resize(oldSize + nr);
  memcpy(rxBuf.data() + oldSize, buf->base, nr);
}

static inline void forEachLine(vector<u_char> &rxBuf, std::function<void(string const &s)> onLine)
{
  auto curIt = rxBuf.begin();
  auto endIt = rxBuf.end();
  while (curIt < endIt) {
    auto eolIt = find(curIt, endIt, (u_char)10);
    if (eolIt == endIt) {
      // no newline, leave only the partial line
      rxBuf.erase(rxBuf.begin(), curIt);
      return;
    }

    onLine(string(curIt, eolIt));
    curIt = eolIt + 1;
  }
  rxBuf.clear();
}

static inline void forEachRtdePacket(vector<u_char> &rxBuf, std::function<void(U8 cmd, packet &rx)> onPacket)
{
  while (rxBuf.size() >= 3) {
    auto pktSize = ((U32)rxBuf[0] << 8) | ((U32)rxBuf[1] << 0);
    auto cmd = (U8)rxBuf[2];

    if (rxBuf.size() < pktSize) break;

    packet rx(rxBuf.data() + 3, pktSize - 3);
    onPacket(cmd, rx);
    rxBuf.erase(rxBuf.begin(), rxBuf.begin() + pktSize);
  }
}

static inline void forEachControlPacket(vector<u_char> &rxBuf, std::function<void(packet &rx)> onPacket)
{
  while (rxBuf.size() >= 4) {
    auto pktSize = ((U32)rxBuf[0] << 24) | ((U32)rxBuf[1] << 16) | ((U32)rxBuf[2] << 8) | ((U32)rxBuf[3] << 0);
    if (pktSize > 10000) {
      throw runtime_error(
          "forEachControlPacket: silly packet size "s + to_string(pktSize) + ", probable sync loss"s);
    }

    if (rxBuf.size() < pktSize) break;

    packet rx(rxBuf.data() + 4, pktSize - 4);
    try {
      onPacket(rx);
    }
    catch (exception const &ex) {
      cerr << "forEachControlPacket: caught exception in onPacket: "s + ex.what() + 
        " pkt=", rx.dump_hex() + "\n";
    }
    rxBuf.erase(rxBuf.begin(), rxBuf.begin() + pktSize);
  }
}

// From MessageSources tab in ClientInterface spreadsheet
static inline string universalMessageSourceName(U8 code)
{
  switch (code) {
    case 255: return "undefined"s;
    case 254: return "robot_interface"s;
    case 253: return "rtmachine"s;
    case 252: return "simulated_robot"s;
    case 251: return "gui"s;
    case 116: return "tool_a"s;
    case 126: return "tool_b"s;
    case 7: return "controller"s;
    case 8: return "rtde"s;
    default: return "message source "s + to_string((int)code);
  }
}

// From ReportLevels tab in ClientInterface spreadsheet
static inline string universalReportLevelName(uint32_t code)
{
  switch (code) {
    case 0: return "debug"s;
    case 1: return "info"s;
    case 2: return "warning"s;
    case 3: return "violation"s;
    case 4: return "fault"s;
    case 128: return "devel_debug";
    case 129: return "devel_info"s;
    case 130: return "devel_warning"s;
    case 131: return "devel_violation"s;
    case 132: return "devel_fault"s;

    default: return "report level "s + to_string((int)code);
  }
}

static vector<string> splitCommas(string const &s)
{
  vector<string> ret;

  string::size_type pos = 0;
  while (pos < s.size()) {
    auto commaPos = s.find(',', pos);
    if (commaPos == pos) {
      pos = commaPos + 1;
    }
    else if (commaPos == string::npos) {
      ret.push_back(string(&s[pos], &s[s.size()]));
      break;
    }
    else {
      ret.push_back(string(&s[pos], &s[commaPos]));
      pos = commaPos + 1;
    }
  }
  return ret;
}

static string joinCommas(vector<string> const &a)
{
  string ret;
  for (auto &it : a) {
    if (ret.size()) ret.push_back(',');
    ret += it;
  }
  return ret;
}

static vector<string> splitNewlines(string const &s)
{
  vector<string> ret;

  string::size_type pos = 0;
  while (pos < s.size()) {
    auto commaPos = s.find('\n', pos);
    if (commaPos == pos) {
      pos = commaPos + 1;
    }
    else if (commaPos == string::npos) {
      ret.push_back(string(&s[pos], &s[s.size()]));
      break;
    }
    else {
      ret.push_back(string(&s[pos], &s[commaPos]));
      pos = commaPos + 1;
    }
  }
  return ret;
}

static string asPrintableString(string const &s)
{
  string ret;
  for (auto &ci : s) {
    if (ci == '\n') {
      ret += "\\n";
    }
    else if (ci == '\r') {
      ret += "\\r";
    }
    else if (ci == '\t') {
      ret += "\\t";
    }
    else if (ci < 32 || ci >= 127) {
      char buf[10];
      snprintf(buf, 10, "\\x%02x", (u_int)(u_char)ci);
      ret += buf;
    }
    else {
      ret += ci;
    }
  }
  return ret;
}

static json parseVariableDump(packet &rx)
{
  auto t = rx.get_be_uint8();
  if (t == 0) {
    return json();
  }
  else if (t == 3 || t == 4) { // string
    auto value = rx.get_lenbe16_string();
    return json::parse(value);
  }
  else if (t == 5) { // list
    auto len = rx.get_be_uint16();
    string ret = "[";
    for (int i = 0; i < len; i++) {
      if (i > 0) ret += ", ";
      ret += parseVariableDump(rx);
    }
    ret += "]";
    return json::parse(ret);
  }
  else if (t == 10) { // pose
    string ret = "p[";
    for (int i = 0; i < 6; i++) {
      if (i > 0) ret += ", ";
      auto value = rx.get_be_float();
      ret += to_string(value);
    }
    ret += "]";
    return json::parse(ret);
  }
  else if (t == 12) { // bool
    auto v = rx.get_be_uint8();
    return json(v ? "true" : "false");
  }
  else if (t == 13) { // num
    auto v = rx.get_be_float();
    return json(v);
  }
  else if (t == 14) { // int
    auto v = (int32_t)rx.get_be_uint32();
    return json(v);
  }
  else if (t == 15) { // float or num
    auto v = rx.get_be_float();
    return json(v);
  }
  else {
    return json("{\"__t\":\"Unknown type tag " + to_string((int)t) + "\"}"s);
  }
}
