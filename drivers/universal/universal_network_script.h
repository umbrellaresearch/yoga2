#pragma once
#include "./universal_network_engine.h"

#include "./universal_network_utils.h"
#include "./universal_types.h"

struct UniversalNetworkScriptDataPortActive {
  UniversalNetworkScriptDataPortActive(shared_ptr<UniversalNetworkEngine> _engine, shared_ptr<UvStream> _sock)
      : engine(_engine), sock(_sock)
  {
  }

  void start()
  {
    sock->read_start([this](ssize_t nr, uv_buf_t const *buf) {
      double rxTime = realtime();
      extendRxBuf(rxBuf, nr, buf);
      forEachLine(rxBuf, [this, rxTime](string const &line) { onRxLine(line, rxTime); });
    });
  }

  ~UniversalNetworkScriptDataPortActive()
  {
    close();
  }

  void close()
  {
    if (sock && sock->is_active()) {
      sock->read_stop();
      sock->close();
    }
  }

  void txVector(vector<R> const &v)
  {
    /*
      Format described at
      https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/ethernet-socket-communication-via-urscript-15678/
    */
    string buf;
    buf += "(";
    for (size_t i = 0; i < v.size(); i++) {
      if (i > 0) buf += ",";
      buf += to_string(v[i]);
    }
    buf += ")\n";
    sock->write(buf, [this](int rc) {
      if (rc < 0) {
        engine->console("Error writing vector to script port"s);
      }
    });
  }

  shared_ptr<UniversalNetworkEngine> engine;
  shared_ptr<UvStream> sock;
  std::function<void(string const &line, double rxTime)> onRxLine;

  vector<u_char> rxBuf;
};

struct UniversalNetworkScriptDataPortListener {
  UniversalNetworkScriptDataPortListener(shared_ptr<UniversalNetworkEngine> _engine, string _port)
      : engine(_engine), listener(), port(_port)
  {
  }

  void start()
  {
    struct sockaddr_in a {
    };
    a.sin_family = PF_INET;
    a.sin_port = htons(atoi(port.c_str()));

    listener.tcp_init();
    int bindStatus = listener.tcp_bind((sockaddr const *)&a, SO_REUSEADDR);
    if (bindStatus < 0) {
      throw uv_error("bind", bindStatus);
    }
    listener.listen_accept(5, [this](shared_ptr<UvStream> acceptedStream, int status) {
      if (status < 0) throw uv_error("listen_accept("s + port + ")"s, status);
      engine->console("Accepted connection on port "s + port);
      auto newConn = make_shared<UniversalNetworkScriptDataPortActive>(engine, acceptedStream);
      newConn->onRxLine = onRxLine;
      newConn->start();
      actives.push_back(newConn);
    });
  }

  ~UniversalNetworkScriptDataPortListener()
  {
    close();
  }

  void close()
  {
    listener.close();
    for (auto &it : actives) {
      it->close();
    }
    actives.clear();
  }

  shared_ptr<UniversalNetworkEngine> engine;
  UvStream listener;
  string port;
  vector<shared_ptr<UniversalNetworkScriptDataPortActive>> actives;
  std::function<void(string const &line, double rxTime)> onRxLine;
};
