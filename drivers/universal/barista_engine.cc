#include "./universal_network_engine.h"

#include "./robotiq_gripper.h"
#include "./universal_network_control.h"
#include "./universal_network_monitor.h"
#include "./universal_network_script.h"

struct BaristaEngine : UniversalNetworkEngine {
  void afterSetRunning() override;
  void update(EngineUpdateContext &ctx) override;
  void afterSetStopped() override;
  virtual void setupAccessors();

  shared_ptr<YogaTimeseq> armCmdIn;
  unique_ptr<BaristaCmdAccessorSuite> armCmdAs;
  shared_ptr<YogaTimeseq> gripCmdIn;
  unique_ptr<RobotiqGripCmdAccessorSuite> gripCmdAs;

  shared_ptr<YogaTimeseq> armObsOut;
  unique_ptr<BaristaArmObsAccessorSuite> armObsAs;
  shared_ptr<YogaTimeseq> armStatusOut;
  unique_ptr<UniversalRobotStatusAccessorSuite> armStatusAs;
  shared_ptr<YogaTimeseq> gripStatusOut;
  unique_ptr<RobotiqGripStatusAccessorSuite> gripStatusAs;
  shared_ptr<YogaTimeseq> armKinOut;
  unique_ptr<UniversalRobotKinematicsAccessorSuite> armKinAs;

};

void BaristaEngine::setupAccessors()
{
  if (!armCmdIn) throw runtime_error("No armCmdIn seq");
  armCmdAs = make_unique<BaristaCmdAccessorSuite>(armCmdIn->type);
  if (!gripCmdIn) throw runtime_error("No gripCmdIn seq");
  gripCmdAs = make_unique<RobotiqGripCmdAccessorSuite>(gripCmdIn->type);

  if (!armObsOut) throw runtime_error("No armObsOut seq");
  armObsAs = make_unique<BaristaArmObsAccessorSuite>(armObsOut->type);
  if (!armStatusOut) throw runtime_error("No armStatusOut seq");
  armStatusAs = make_unique<UniversalRobotStatusAccessorSuite>(armStatusOut->type);
  if (!gripStatusOut) throw runtime_error("No gripStatusOut seq");
  gripStatusAs = make_unique<RobotiqGripStatusAccessorSuite>(gripStatusOut->type);
  if (!armKinOut) throw runtime_error("No armKinOut seq");
  armKinAs = make_unique<UniversalRobotKinematicsAccessorSuite>(armKinOut->type);
}

void BaristaEngine::afterSetRunning()
{
  setupAccessors();
  if (1) {
    console("Create monitor "s + config.robotHostname + ":30004");
    auto mon = addMonitor(config.robotHostname, "30004");
    mon->onRxDataPackage = [this, thisp = shared_from_this()](U8 recipeId, packet &rx, double rxTime) {
      if (!armObsOut) return;

      YogaValue armObs(rti->reg.get(), armObsAs->t, fromExternalTime(rxTime), armObsOut->mem);
      armObsAs->rx(rx, armObs);
      rx.check_at_end();

      if (verbose >= 3) {
        console("mon > data " + repr(armObs));
      }

      armObsOut->add(armObs);
      if (rti) rti->requestUpdate();
    };

    mon->addOutput("VECTOR6D", "target_q");
    mon->addOutput("VECTOR6D", "target_qd");
    mon->addOutput("VECTOR6D", "target_current");
    mon->addOutput("VECTOR6D", "target_moment");
    mon->addOutput("VECTOR6D", "actual_q");
    mon->addOutput("VECTOR6D", "actual_qd");
    mon->addOutput("VECTOR6D", "actual_current");

    mon->addOutput("VECTOR6D", "actual_TCP_pose");
    mon->addOutput("VECTOR6D", "actual_TCP_speed");
    mon->addOutput("VECTOR6D", "actual_TCP_force");

    mon->addOutput("VECTOR6INT32", "joint_mode");
    mon->addOutput("INT32", "safety_mode");

    for (int i = 0; i < 14; i++) {
      mon->addInput("DOUBLE", "input_double_register_"s + to_string(i));
    }
    mon->addInput("INT32", "input_int_register_0");

    mon->start();
  }

  if (1) {
    console("Create gripper "s + config.robotHostname + ":63352");
    static int logcnt=0;
    auto grip = addGripper(config.robotHostname, "63352");
    grip->onRxGripStatus = [this, thisp = shared_from_this()](YogaValue const &grip) {
      if (logcnt == 0) {
        logcnt++;
        
      }
      gripStatusOut->add(grip);
    };
    grip->start();
  }

  if (1) {
    console("Create control "s + config.robotHostname + ":30001");
    auto control = addControl(config.robotHostname, "30001");
    if (armKinOut) {
      control->onRxKinematics = [this, thisp = shared_from_this()](YogaValue const &kin) {
        armKinOut->add(kin);
      };
    }
#if 0
    if (globalVarsOut) {
      control->onRxGlobalVarDump = [this, thisp = shared_from_this()](json const &vars, double rxTime) {
        globalVarsOut->add(fromExternalTime(rxTime), asJson(vars));
      };
    }
#endif
    control->mainScript = config.controlScript;
    control->start();
  }
  UniversalNetworkEngine::afterSetRunning();
}

void BaristaEngine::update(EngineUpdateContext &ctx)
{
  UniversalNetworkEngine::update(ctx);

  if (armCmdIn && controlConn->commState == "running") {
    auto out0 = armCmdIn->getLast();
    if (out0.ts >= lastCmdTs + 0.002) {
      if (1) {
        monitorConn->txDataPackage([this, &out0](packet &tx) {
          armCmdAs->tx(tx, out0);
        });
      }
      lastCmdTs = out0.ts;
    }
  }
  if (gripCmdIn && gripperConn && gripperConn->commState == "running" && gripperConn->iterActive == 0) {
    auto out0 = gripCmdIn->getLast();
    if (out0.ts >= lastGripCmdTs + 0.002) {
      lastGripCmdTs = out0.ts;
      gripperConn->remoteIter(out0);
    }
  }
}

void BaristaEngine::afterSetStopped()
{
  if (controlConn) controlConn->txScript("stopj(1)\n"s);
  UniversalNetworkEngine::afterSetStopped();
}

static EngineRegister baristaEngineReg(
  "baristaEngine", 
  {
    "BaristaArmCmd",
    "RobotiqGripCmd",
    "BaristaArmObs",
    "UniversalRobotStatus",
    "RobotiqGripStatus"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 6) {
      return ctx.logError("baristaEngine: expected 6 arguments (armCmd, gripCmd, armObs, armStatus, gripStatus, kin)");
    }

    auto e = make_shared<BaristaEngine>();
    if (args.size() > 0) {
      e->armCmdIn = args[0];
      e->armCmdIn->isExtOut = true;
    }
    if (args.size() > 1) {
      e->gripCmdIn = args[1];
      e->gripCmdIn->isExtOut = true;
    }
    if (args.size() > 2) {
      e->armObsOut = args[2];
      e->armObsOut->isExtIn = true;
    }
    if (args.size() > 3) {
      e->armStatusOut = args[3];
      e->armStatusOut->isExtIn = true;
    }
    if (args.size() > 4) {
      e->gripStatusOut = args[4];
      e->gripStatusOut->isExtIn = true;
      e->gripStatusOut->add(YogaValue(ctx.reg, ctx.getType("RobotiqGripStatus"), e->gripStatusOut->mem));
    }
    if (args.size() > 5) {
      e->armKinOut = args[5];
      e->armKinOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    cerr << "Created BaristaEngine("s + e->label + ") verbose=" + repr(e->verbose) + "\n";

    return true;
  }
);

