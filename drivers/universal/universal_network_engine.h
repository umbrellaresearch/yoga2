#pragma once
#include "../../comm/engine.h"
#include "./universal_types.h"

#include <tuple>

struct UniversalNetworkEngine;
struct UniversalNetworkScriptDataPortListener;
struct UniversalNetworkMonitorPortActive;
struct UniversalNetworkControlPortActive;
struct UniversalRobotiqGripper;
#include "common/uv_wrappers.h"

struct UniversalNetworkConfig {
  string robotHostname;
  string controlScript;
};


struct UniversalNetworkEngine : GenericEngine {

  UniversalNetworkEngine();
  UniversalNetworkEngine(UniversalNetworkConfig const &_config);
  ~UniversalNetworkEngine() override;
  UniversalNetworkEngine(UniversalNetworkEngine const &) = delete;
  UniversalNetworkEngine(UniversalNetworkEngine &&) = delete;
  UniversalNetworkEngine &operator=(UniversalNetworkEngine const &) = delete;
  UniversalNetworkEngine &operator=(UniversalNetworkEngine &&) = delete;

  void afterSetStopped() override;

  void update(EngineUpdateContext &ctx) override;

  UniversalNetworkConfig config;
  shared_ptr<UniversalNetworkMonitorPortActive> monitorConn;
  shared_ptr<UniversalNetworkControlPortActive> controlConn;
  shared_ptr<UniversalRobotiqGripper> gripperConn;

  vector<shared_ptr<UniversalNetworkScriptDataPortListener>> scriptPorts;

  R lastUpdateTime{0.0};
  R lastCmdTs {0.0};
  R lastGripCmdTs {0.0};

  bool setNetworkConfig(YogaContext const &ctx, AstAnnoCall *spec);

  UniversalNetworkScriptDataPortListener *addScriptPort(string port);

  UniversalNetworkMonitorPortActive *addMonitor(string hostname, string port);

  UniversalNetworkControlPortActive *addControl(string hostname, string port);

  UniversalRobotiqGripper *addGripper(string hostname, string port);
};
