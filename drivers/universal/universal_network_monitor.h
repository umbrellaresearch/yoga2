#pragma once
#include "./universal_network_engine.h"

#include "./universal_network_utils.h"
#include "./universal_types.h"

/*
  RTDE spec:
  https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/real-time-data-exchange-rtde-guide-22229/

  UR Script spec:
  https://www.universal-robots.com/how-tos-and-faqs/how-to/ur-how-tos/ethernet-socket-communication-via-urscript-15678/
*/

enum {
  RTDE_REQUEST_PROTOCOL_VERSION = 86,      // ascii V
  RTDE_GET_URCONTROL_VERSION = 118,        // ascii v
  RTDE_TEXT_MESSAGE = 77,                  // ascii M
  RTDE_DATA_PACKAGE = 85,                  // ascii U
  RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS = 79, // ascii O
  RTDE_CONTROL_PACKAGE_SETUP_INPUTS = 73,  // ascii I
  RTDE_CONTROL_PACKAGE_START = 83,         // ascii S
  RTDE_CONTROL_PACKAGE_PAUSE = 80,         // ascii P

  RTDE_PROTOCOL_VERSION = 2
};

struct UniversalNetworkMonitorPortActive : enable_shared_from_this<UniversalNetworkMonitorPortActive> {

  UniversalNetworkMonitorPortActive(shared_ptr<UniversalNetworkEngine> _engine, string _hostname, string _port)
      : engine(_engine), hostname(_hostname), port(_port), sock()
  {
  }

  void addOutput(string const &type, string const &name)
  {
    outputTypes.push_back(type);
    outputVariables.push_back(name);
  }

  void addInput(string const &type, string const &name)
  {
    inputTypes.push_back(type);
    inputVariables.push_back(name);
  }

  void start()
  {
    /*
      We keep this object alive until all the callbacks return, by capturing thisp in the lambda.
      Because we also have a shared_ptr to the engine, that stays alive too.
    */

    uvGetAddrInfo(hostname, port, addrinfoForTcp(), [this, thisp = shared_from_this()](int status, struct addrinfo *result) {
      if (status < 0) throw uv_error("Monitor UvGetAddrInfo("s + hostname + ":"s + port + ")"s, status);

      assert(result->ai_family == PF_INET);
      auto remoteAddr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
      remoteAddr = reinterpret_cast<sockaddr *>(remoteAddr_in);
      remoteAddrDesc = sockaddr_desc(remoteAddr);

      sock.tcp_init();
      sock.tcp_nodelay(true);
      sock.tcp_connect(remoteAddr, [this, thisp = shared_from_this()](int rc) {
        if (rc < 0) {
          if (rc == UV_EAI_CANCELED) return;
          engine->console("Monitor socket couldn't connect: "s + uv_strerror(rc));
          return;
        }
        if (engine->verbose >= 0) engine->console("Monitor socket connected to "s + remoteAddrDesc);
        localAddrDesc = "local";

        commStart();

        sock.read_start([this, thisp = shared_from_this()](ssize_t nr, const uv_buf_t *buf) {
          if (nr == UV_EOF) {
            rxEof();
            sock.read_stop();
          }
          else if (nr < 0) {
            throw uv_error("Monitor("s + remoteAddrDesc + "): read tcp"s, nr);
          }
          else {
            double rxTime = realtime();
            sock.tcp_quickack(true);
            if (!rxAnyFlag) {
              rxAnyFlag = true;
              if (engine->verbose >= 1)
                engine->console(
                    "Receiving on monitor socket ("s + localAddrDesc +
                    " <=> "s + remoteAddrDesc.c_str() + ")"s);
            }

            extendRxBuf(rxBuf, nr, buf);
            forEachRtdePacket(rxBuf, [this, rxTime](U8 type, packet &rx) { rxPkt(type, rx, rxTime); });
          }
        });
      });
    });
  }

  void close()
  {
    if (sock.is_active()) {
      sock.read_stop();
    }
  }

  void rxEof()
  {
    engine->console("mon > EOF"s);
    commState = "EOF";
  }

  void rxPkt(U8 type, packet &rx, double rxTime)
  {
    if (engine->verbose >= 4) {
      engine->console("mon > pkt type="s + charname_hex(type) + " payload=" + rx.dump_hex());
    }

    switch ((int)type) {

      case RTDE_REQUEST_PROTOCOL_VERSION: {
        if (rx.size() != 1) {
          engine->console("mon > RTDE_REQUEST_PROTOCOL_VERSION: bad length "s + to_string(rx.size()));
          return;
        }
        bool success = !!rx.get_be_uint8();
        if (engine->verbose >= 2) engine->console("mon > requestProtocolVersion "s + (success ? "ok"s : "fail"s));
        rxRequestProtocolVersion(success, rxTime);
        break;
      }

      case RTDE_GET_URCONTROL_VERSION: {
        if (rx.size() != 16) {
          engine->console("mon > RTDE_GET_URCONTROL_VERSION: bad length "s + to_string(rx.size()));
          return;
        }
        U32 major = rx.get_be_uint32();
        U32 minor = rx.get_be_uint32();
        U32 bugfix = rx.get_be_uint32();
        U32 build = rx.get_be_uint32();
        if (engine->verbose >= 2)
          engine->console(
            "mon > getUrcontrolVersion "s + to_string(major) + "."s + to_string(minor) + 
            "."s + to_string(bugfix) + "."s + to_string(build));
        rxGetUrcontrolVersion(major, minor, bugfix, build, rxTime);
        break;
      }

      case RTDE_TEXT_MESSAGE: {
        string message = rx.get_len8_string();
        string source = rx.get_len8_string();
        uint32_t level = rx.get_be_uint8();
        if (engine->verbose >= 2)
          engine->console(
              "mon > textMessage \""s + asPrintableString(message) +
              "\" from \""s + asPrintableString(source) +
              "\" level " + to_string(level));
        rxTextMessage(message, source, level, rxTime);
        break;
      }

      case RTDE_DATA_PACKAGE: {
        U8 recipeId = rx.get_be_uint8();
        if (onRxDataPackage) {
          onRxDataPackage(recipeId, rx, rxTime);
        }
        break;
      }

      case RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS: {
        U8 id = rx.get_be_uint8();
        string types = rx.get_remainder_string();
        if (engine->verbose >= 2)
          engine->console("mon > controlPackageSetupOutputs id="s + to_string(id) + " types=\""s + types + "\"");
        rxControlPackageSetupOutputs(id, splitCommas(types), rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_SETUP_INPUTS: {
        int id = rx.get_be_uint8();
        string types = rx.get_remainder_string();
        if (engine->verbose >= 2)
          engine->console("mon > controlPackageSetupInputs id="s + to_string(id) + " types=\""s + types + "\""s);
        rxControlPackageSetupInputs(id, splitCommas(types), rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_START: {
        bool success = !!rx.get_be_uint8();
        if (engine->verbose >= 2) engine->console("mon > controlPackageStart "s + (success ? "ok"s : "fail"s));
        rxControlPackageStart(success, rxTime);
        break;
      }

      case RTDE_CONTROL_PACKAGE_PAUSE: {
        bool success = !!rx.get_be_uint8();
        if (engine->verbose >= 2) engine->console("mon > controlPackagePause "s + (success ? "ok"s : "fail"s));
        rxControlPackagePause(success, rxTime);
        break;
      }

      default: engine->console("mon > Unknown packet type "s + repr_02x(type));
    }
  }

  void rxRequestProtocolVersion(bool success, double rxTime)
  {
    if (commState == "protocolVersion.wait") {
      if (success) {
        txGetUrcontrolVersion();
        commState = "urcontrolVersion.wait";
      }
      else {
        throw runtime_error("rx requestProtocolVersion: failed to negotiate protocol version"s);
      }
    }
  }

  void rxGetUrcontrolVersion(U32 major, U32 minor, U32 bugfix, U32 build, double rxTime)
  {
    if (commState == "urcontrolVersion.wait") {
      if (major == 3 && minor <= 2 && bugfix < 19171) {
        throw runtime_error("UniversalNetworkEngine::monitorRxGetUrcontrolVersion: robot version too old"s);
      }
      txControlPackageSetupInputs();
      commState = "setupInputs.wait";
    }
  }

  void rxTextMessage(string message, string source, U8 level, double rxTime) {}

  void rxControlPackageSetupOutputs(U8 id, vector<string> types, double rxTime)
  {
    if (types != outputTypes) {
      engine->console(
          "mon > controlPackageSetupOutputs: type mismatch. Expected \""s + joinCommas(outputTypes) + 
          "\" got \""s + joinCommas(types).c_str() + "\""s);
      throw runtime_error("Output type mismatch"s);
    }
    negotiatedOutputId = id;
    if (commState == "setupOutputs.wait") {
      txControlPackageStart();
      commState = "controlPackageStart.wait";
    }
  }

  void rxControlPackageSetupInputs(U8 id, vector<string> types, double rxTime)
  {
    if (types != inputTypes) {
      engine->console(
          "mon > controlPackageSetupInputs: type mismatch. Expected \""s + joinCommas(inputTypes) +
          "\", got \""s + joinCommas(types) + "\""s);
      throw runtime_error("Input type mismatch"s);
    }
    negotiatedInputId = id;
    if (commState == "setupInputs.wait") {
      txControlPackageSetupOutputs();
      commState = "setupOutputs.wait";
    }
  }

  void rxControlPackageStart(bool success, double rxTime)
  {
    if (commState == "controlPackageStart.wait") {
      if (success) {
        commState = "running";
        if (engine->rti) engine->rti->requestUpdate();
      }
      else {
        throw runtime_error("rxControlPackageStart: starting failed"s);
      }
    }
  }

  void rxControlPackagePause(bool success, double rxTime) {}

  void commStart()
  {
    commState = "protocolVersion.wait";
    txRequestProtocolVersion(2);
  }

  void txPkt(U8 cmd, packet &tx)
  {
    size_t txSize = tx.size();
    assert(txSize >= 3);
    tx[0] = (U8)(txSize >> 8);
    tx[1] = (U8)(txSize >> 0);
    tx[2] = cmd;
    if (engine->verbose >= 3) {
      engine->console("mon < cmd="s + charname_hex(cmd) + " pkt="s + tx.dump_hex());
    }
    sock.write((char const *)tx.ptr(), tx.size(), [this](int status) {
      if (status == UV_EAI_CANCELED) {
        engine->console("mon > write cancelled"s);
        return;
      }
      if (status < 0) throw uv_error("mon txPkt", status);
    });
  }

  void setupTxHeader(packet &tx)
  {
    tx.add_be_uint16(0);
    tx.add_be_uint8(0);
  }

  void txRequestProtocolVersion(U16 version)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_uint16(version);
    if (engine->verbose >= 2) engine->console("mon < requestProtocolVersion "s + to_string(version));
    txPkt(RTDE_REQUEST_PROTOCOL_VERSION, tx);
  }

  void txGetUrcontrolVersion()
  {
    packet tx;
    setupTxHeader(tx);
    if (engine->verbose >= 2) engine->console("mon < getUrcontrolVersion"s);
    txPkt(RTDE_GET_URCONTROL_VERSION, tx);
  }

  void txControlPackageSetupInputs()
  {
    packet tx;
    setupTxHeader(tx);
    string variablesStr = joinCommas(inputVariables);
    tx.add_remainder_string(variablesStr);
    if (engine->verbose >= 2) engine->console("mon < controlPackageSetupInputs variables="s + variablesStr);
    txPkt(RTDE_CONTROL_PACKAGE_SETUP_INPUTS, tx);
  }

  void txControlPackageSetupOutputs()
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_double(monitorSpeed);
    string variablesStr = joinCommas(outputVariables);
    tx.add_remainder_string(variablesStr);
    if (engine->verbose >= 2)
      engine->console("mon < controlPackageSetupOutputs speed="s + to_string(monitorSpeed) + " variables=" + variablesStr);
    txPkt(RTDE_CONTROL_PACKAGE_SETUP_OUTPUTS, tx);
  }

  void txControlPackageStart()
  {
    packet tx;
    setupTxHeader(tx);
    if (engine->verbose >= 2) engine->console("mon < controlPackageStart"s);
    txPkt(RTDE_CONTROL_PACKAGE_START, tx);
  }

  void txControlPackagePause()
  {
    packet tx;
    setupTxHeader(tx);
    if (engine->verbose >= 2) engine->console("mon < controlPackagePause"s);
    txPkt(RTDE_CONTROL_PACKAGE_PAUSE, tx);
  }

  void txTextMessage(string message, string source, U8 type)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_len8_string(message);
    tx.add_len8_string(source);
    tx.add_be_uint8(type);
    if (engine->verbose >= 2)
      engine->console(
          "mon < textMessage message="s + asPrintableString(message) +
          " source="s + asPrintableString(source) +
          " type="s + charname_hex(type));
    txPkt(RTDE_TEXT_MESSAGE, tx);
  }

  void txDataPackage(std::function<void(packet &tx)> fillPkt)
  {
    packet tx;
    setupTxHeader(tx);
    tx.add_be_uint8(negotiatedInputId);
    fillPkt(tx);
    txPkt(RTDE_DATA_PACKAGE, tx);
  }

  shared_ptr<UniversalNetworkEngine> engine;
  string hostname, port;
  string commState;

  sockaddr *localAddr{nullptr};
  string localAddrDesc;
  sockaddr *remoteAddr{nullptr};
  string remoteAddrDesc;

  vector<u_char> rxBuf;
  bool rxAnyFlag{false};

  double monitorSpeed{125.0};
  U8 negotiatedInputId{0};
  vector<string> inputVariables;
  vector<string> inputTypes;
  U8 negotiatedOutputId{0};
  vector<string> outputVariables;
  vector<string> outputTypes;

  UvStream sock;

  std::function<void(U8 recipeId, packet &rx, double rxTime)> onRxDataPackage;
};
