#pragma once
#include "common/packetbuf.h"
#include "numerical/numerical.h"
#include "../../timeseq/trace_blobs.h"
#include <glm/mat4x4.hpp>

struct RemoteCameraConfig {
  string camType;
  string ctlHost;
  string cameraHost;
  string url;
  string authHeader;
  string usbId;
  string devSerial;
  R captureStillsInterval;
  vector<string> camSettings;
  glm::dmat4 baseOrientation;
};

DECL_PACKETIO(RemoteCameraConfig);

ostream & operator <<(ostream &s, const RemoteCameraConfig &a);

enum YogaVideoFrameFormat {
  YVFF_JPEG,
  YVFF_RGB,
  YVFF_DEPTH16
};

struct YogaVideoFrame {
  R priority;
  R format;
  R width;
  R height;
  glm::dmat4 orientation;
  BlobRef blob;
};
DECL_PACKETIO(YogaVideoFrame);

enum YogaAudioFrameFormat {
  YAFF_S16
};

struct YogaAudioFrame {
  R priority;
  R format;
  R sampleRate;
  R sampleCount;
  BlobRef blob;
};
DECL_PACKETIO(YogaAudioFrame);


struct VideoTraceSpec {
  string seqName;
  string traceName;
};

DECL_PACKETIO(VideoTraceSpec);

ostream & operator <<(ostream &s, const VideoTraceSpec &a);


DECL_PACKETIO(glm::dmat4);
DECL_PACKETIO(glm::dvec4);
