#include "./camera_client.h"
#include "./alsa_includes.h"
#include <thread>

#include "../../timeseq/timeseq.h"

/*

Test with:

{"method":"record","id":42,"params":[{"__type":"RemoteCameraRecordReq","txTime":1501021115.482347,"recordFor":2,"cameraConfig":{"__type":"RemoteCameraConfig","camType":"alsa","usbId":"","hostname":"","url":"","authHeader":"","devSerial":"hw:CARD=Snowball","captureStillsInterval":0},"traceSpec":{"__type":"VideoTraceSpec","traceDir":"/tmp/test-trace1","seqName":"frontAudio","traceName":"test-trace1"}}]}

{"method":"record","id":42,"params":[{"__type":"RemoteCameraRecordReq","txTime":1501021115.482347,"recordFor":2,"cameraConfig":{"__type":"RemoteCameraConfig","camType":"alsa","usbId":"","hostname":"","url":"","authHeader":"","devSerial":"hw:CARD=AT2005USB","captureStillsInterval":0},"traceSpec":{"__type":"VideoTraceSpec","traceDir":"/tmp/test-trace1","seqName":"frontAudio","traceName":"test-trace1"}}]}

{"method":"record","id":42,"params":[{"__type":"RemoteCameraRecordReq","txTime":1501021115.482347,"recordFor":2,"cameraConfig":{"__type":"RemoteCameraConfig","camType":"alsa","usbId":"","hostname":"","url":"","authHeader":"","devSerial":"hw:CARD=Elf","captureStillsInterval":0},"traceSpec":{"__type":"VideoTraceSpec","traceDir":"/tmp/test-trace1","seqName":"frontAudio","traceName":"test-trace1"}}]}

If it fails to open, it can help to run
  arecord --device hw:CARD=Snowball -f S16_LE -c1 -r44100 foo.wav


*/


#ifdef USE_ALSA
struct AlsaAudioClient : CameraClient {
  AlsaAudioClient();
  ~AlsaAudioClient() noexcept;

  AlsaAudioClient(AlsaAudioClient const &other) = delete;
  AlsaAudioClient(AlsaAudioClient &&other) = delete;
  AlsaAudioClient &operator=(const AlsaAudioClient &other) = delete;
  AlsaAudioClient &operator=(AlsaAudioClient &&other) = delete;

  bool isRunning() override { return running; }
  void pleaseStop() override;

  void startAlsaClient();

  bool running{false};

  double sampleRate{0.0};

  shared_ptr<std::thread> captureThread;
  snd_pcm_t *capture_handle = nullptr;
};

AlsaAudioClient::AlsaAudioClient() : CameraClient() {}

AlsaAudioClient::~AlsaAudioClient() noexcept {}

shared_ptr<CameraClient>
CameraClient::mkAlsaAudioClient(RemoteCameraConfig const &cameraConfig, VideoTraceSpec const &traceSpec)
{
  auto c = make_shared<AlsaAudioClient>();
  c->cameraConfig = cameraConfig;
  c->traceSpec = traceSpec;
  c->startAlsaClient();

  return static_pointer_cast<CameraClient>(c);
}

void AlsaAudioClient::startAlsaClient()
{
  int err = 0;
  snd_pcm_hw_params_t *hw_params = nullptr;

  if ((err = snd_pcm_open(&capture_handle, cameraConfig.devSerial.c_str(), SND_PCM_STREAM_CAPTURE, 0)) < 0) {
    throw runtime_error("cannot open audio device "s + cameraConfig.devSerial + " ("s + snd_strerror(err) + ")"s);
  }

  if ((err = snd_pcm_hw_params_malloc(&hw_params)) < 0) {
    throw runtime_error("audio device open failed ("s + snd_strerror(err) + ")");
  }

  if ((err = snd_pcm_hw_params_any(capture_handle, hw_params)) < 0) {
    throw runtime_error("cannot initialize hardware parameter structure ("s + snd_strerror(err) + ")");
  }

  if ((err = snd_pcm_hw_params_set_access(capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
    throw runtime_error("cannot set access type to RW_INTERLEAVED ("s + snd_strerror(err) + ")");
  }

  if ((err = snd_pcm_hw_params_set_format(capture_handle, hw_params, SND_PCM_FORMAT_S16_LE)) < 0) {
    throw runtime_error("cannot set sample format to S16_LE ("s + snd_strerror(err) + ")");
  }

  u_int exactRate = 48000;
  if ((err = snd_pcm_hw_params_set_rate_near(capture_handle, hw_params, &exactRate, 0)) < 0) {
    throw runtime_error("cannot set sample rate to "s + to_string(exactRate) + " ("s + snd_strerror(err) + ")");
  }
  sampleRate = exactRate;

  int channelCount = 2;
  if ((err = snd_pcm_hw_params_set_channels(capture_handle, hw_params, channelCount)) < 0) {
    throw runtime_error("cannot set channel count to "s + to_string(channelCount) + " ("s + snd_strerror(err) + ")");
  }

  int periods = 2;
  snd_pcm_uframes_t frames = 2048;

  if ((err = snd_pcm_hw_params_set_period_size(capture_handle, hw_params, frames, 0)) < 0) {
    throw runtime_error("cannot set period_size to "s + to_string(frames) + " ("s + snd_strerror(err) + ")");
  }

  if ((err = snd_pcm_hw_params_set_periods(capture_handle, hw_params, periods, 0)) < 0) {
    throw runtime_error("cannot set periods to "s + to_string(periods) + "("s + snd_strerror(err) + ")");
  }

  if ((err = snd_pcm_hw_params(capture_handle, hw_params)) < 0) {
    throw runtime_error("cannot set hw params ("s + snd_strerror(err) + ")");
  }

#if 1
  int dir = 0;
  snd_pcm_hw_params_get_period_size(hw_params, &frames, &dir);
#endif

  cerr << "periods="s + repr(periods) + " frames=" + repr(frames) + " dir=" + repr(dir) + "\n";

  snd_pcm_hw_params_free(hw_params);

  if ((err = snd_pcm_prepare(capture_handle)) < 0) {
    throw runtime_error("cannot prepare audio interface ("s + snd_strerror(err) + ")");
  }

  running = true;

  captureThread = make_shared<std::thread>([this, frames, channelCount]() {
    int err;
    vector<char> buf(frames * channelCount * sizeof(int16_t));
    while (running) {
      int nr = snd_pcm_readi(capture_handle, buf.data(), frames);
      /*
        ALSA really does use these return values. See
        http://www.alsa-project.org/alsa-doc/alsa-lib/group___p_c_m.html#ga4c2c7bd26cf221268d59dc3bbeb9c048
      */
      if (nr == -EPIPE) {
        cerr << "audio device overrun\n";
        if ((err = snd_pcm_prepare(capture_handle)) < 0) {
          throw runtime_error("cannot prepare audio interface after overrun ("s + snd_strerror(err) + ")");
        }
        continue;
      }
      else if (nr < 0) {
        throw runtime_error("cannot read from device ("s + snd_strerror(nr) + ")");
      }
      double frameTs = realtime() - ((double)frames / (double)sampleRate);
      if (frameTs >= recordUntil) {
        break;
      }

      vector<char> leftChan(frames * sizeof(int16_t));
      uint16_t *bothData = (uint16_t *)buf.data();
      uint16_t *lcData = (uint16_t *)leftChan.data();
      for (int i=0; i<frames; i++) {
        lcData[i] = bothData[i*2];
      }

      YogaAudioFrame frame;
      frame.format = YAFF_S16;
      frame.sampleCount = frames;
      frame.sampleRate = sampleRate;
      frame.blob = mkBlob(curChunkId, (U8 *)lcData, frames*sizeof(int16_t));
      frame.priority = 0;
      audioQueue.emplace_back(frameTs, frame);
    }
    snd_pcm_close(capture_handle);
    capture_handle = nullptr;
    running = false;
  });
}

void AlsaAudioClient::pleaseStop()
{
  CameraClient::pleaseStop();
  if (running) {
    captureThread->join();
    cerr << "Shut down alsa client\n";
  }
}


#else

shared_ptr<CameraClient>
CameraClient::mkAlsaAudioClient(RemoteCameraConfig const &cameraConfig, VideoTraceSpec const &traceSpec)
{
  return nullptr;
}

#endif
