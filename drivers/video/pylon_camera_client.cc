#include "./camera_client.h"

#include "./pylon_includes.h"
#ifdef __linux__
#include <turbojpeg.h> // http://www.libjpeg-turbo.org/Documentation/Documentation
#endif
#include <sys/file.h>
#include <thread>

#include "../../timeseq/timeseq.h"

#ifdef USE_PYLON
using namespace Pylon;

struct PylonCameraClient : CameraClient {
  PylonCameraClient()
   : CameraClient()
  {

  }
  ~PylonCameraClient()
  {
    pleaseStop();
    if (openLockFd != -1) {
      close(openLockFd);
      openLockFd = -1;
    }
  }

  void startPylonClient();
  void openPylonCamera();

  bool isRunning() override { return cam != nullptr; }

  int openLockFd{-1};
  IPylonDevice *pdev{nullptr};
  CInstantCamera *cam{nullptr};
  std::thread *grabThread{nullptr};
  double grabStartTime{0.0};
};

void CameraClient::enumeratePylonCameras()
{
  CTlFactory &TlFactory = CTlFactory::GetInstance();
  ITransportLayer *tl = TlFactory.CreateTl(CBaslerUsbCamera::DeviceClass());

  DeviceInfoList_t allDevices;
  tl->EnumerateDevices(allDevices);
  eprintf("Devices:\n");
  for (auto &dev : allDevices) {
    eprintf(
        "  name='%s' vendor='%s' model='%s' serial='%s'\n",
        dev.GetFullName().c_str(),
        dev.GetVendorName().c_str(),
        dev.GetModelName().c_str(),
        dev.GetSerialNumber().c_str());
  }
}

shared_ptr<CameraClient>
CameraClient::mkPylonCameraClient(RemoteCameraConfig const &cameraConfig, VideoTraceSpec const &traceSpec)
{
  auto c = make_shared<PylonCameraClient>();
  c->cameraConfig = cameraConfig;
  c->traceSpec = traceSpec;
  c->verbose = 0;
  c->startPylonClient();

  return static_pointer_cast<CameraClient>(c);
}

void compressJpeg(CGrabResultPtr const &grabResult, u_char *&jBuf, u_long &jSize)
{
  /*
    Called from multiple (libuv-worker) threads, so no static data here
  */
  /*
    tjInitCompress only takes 2 uS.
  */
  tjhandle h = tjInitCompress();
  jBuf = nullptr;
  jSize = 0;

  u_char *rawBuf = reinterpret_cast<u_char *>(grabResult->GetBuffer());
  size_t width = grabResult->GetWidth();
  size_t height = grabResult->GetHeight();

  auto pixelType = grabResult->GetPixelType();
  if (pixelType == PixelType_BGR8packed) {
    size_t stride{0};
    if (!grabResult->GetStride(stride)) throw runtime_error("No stride");

    /*
      tjCompress takes around 2500 uS for 800x600
      (on an Intel NUC7i7BNH).
    */
    tjCompress2(h, rawBuf, width, stride, height, TJPF_BGR, &jBuf, &jSize, TJSAMP_420, 90, 0);
  }
  else if (pixelType == PixelType_YUV422_YUYV_Packed) {

    auto expectSize = tjBufSizeYUV2(grabResult->GetWidth(), 4, grabResult->GetHeight(), TJSAMP_422);
    if (expectSize != grabResult->GetImageSize()) {
      throw runtime_error(
          "YUV Format error, expected "s + to_string(expectSize) + " got "s +
          to_string(grabResult->GetImageSize()));
    }

    /*
      Pylon delivers it as YUYV, but jpeg-turbo expects separate planes.
      YUV planarization takes about 350 uS
      (on an Intel NUC7i7BNH).
    */

    size_t ySize = width * height;
    size_t uSize = ySize / 2;
    size_t vSize = ySize / 2;
    u_char *yuvBuf = new u_char[ySize + uSize + vSize];

    u_char *yPlane = yuvBuf + 0;
    u_char *uPlane = yuvBuf + ySize;
    u_char *vPlane = yuvBuf + ySize + uSize;
    for (size_t i = 0; i < expectSize; i += 4) {
      *yPlane++ = rawBuf[i + 0];
      *uPlane++ = rawBuf[i + 1];
      *yPlane++ = rawBuf[i + 2];
      *vPlane++ = rawBuf[i + 3];
    }
    /*
      tjCompressFromYUV takes around 2400 uS for 800x600
      (on an Intel NUC7i7BNH).
    */
    if (tjCompressFromYUV(h, yuvBuf, width, 4, height, TJSAMP_422, &jBuf, &jSize, 80, 0) < 0) {
      throw runtime_error("jCompressFromYUV: "s + string(tjGetErrorStr()));
    }
    delete[] yuvBuf;
  }
  else {
    eprintf("Unknown pixel type 0x%x\n", (int)pixelType);
  }
}

void PylonCameraClient::openPylonCamera()
{
  if (openLockFd == -1) {
    openLockFd = open("/tmp/pylon.lock", O_RDWR | O_CREAT, 0777);
    if (openLockFd < 0) throw runtime_error("Can't open /tmp/pylon.lock: "s + strerror(errno));
  }
  if (0) eprintf("Locking /tmp/pylon.lock...\n");
  if (::flock(openLockFd, LOCK_EX) < 0) {
    throw runtime_error("Can't flock(LOCK_EX) /tmp/pylon.lock: "s + strerror(errno));
  }
  if (0) eprintf("Locked /tmp/pylon.lock\n");
  try {
    CTlFactory &TlFactory = CTlFactory::GetInstance();
    CDeviceInfo di;
    if (!cameraConfig.usbId.empty()) di.SetFullName(String_t(cameraConfig.usbId.c_str()));
    if (!cameraConfig.devSerial.empty()) di.SetSerialNumber(String_t(cameraConfig.devSerial.c_str()));
    di.SetDeviceClass(BaslerUsbDeviceClass);
    pdev = TlFactory.CreateDevice(di);
    assert(pdev);
    cam = new CInstantCamera(pdev);
    cam->Open();
    eprintf("Started device '%s'\n", cam->GetDeviceInfo().GetFullName().c_str());
    {
      using namespace GenApi;

      INodeMap &camCtl = cam->GetNodeMap();

      for (auto const &it : cameraConfig.camSettings) {

        size_t eqPos = it.find('=', 0);
        if (eqPos == string::npos) throw runtime_error("Bad camSetting "s + it);

        string name = it.substr(0, eqPos);
        string val = it.substr(eqPos + 1);

        auto node = camCtl.GetNode(name.c_str());

        try {
          CValuePtr(node)->FromString(val.c_str());
          if (0) eprintf("Set %s=%s\n", name.c_str(), CValuePtr(node)->ToString().c_str());
        }
        catch (GenericException const &ex) {
          eprintf("Setting %s: %s\n", it.c_str(), ex.what());
        }
      }

      try {
        double d = CFloatPtr(camCtl.GetNode("ResultingFrameRate"))->GetValue();
        eprintf("ResultingFrameRate=%g\n", d);
      }
      catch (LogicalErrorException const &ex) {
        eprintf("Getting ResultingFrameRate: %s\n", ex.what());
        throw;
      }
      try {
        double d = CFloatPtr(camCtl.GetNode("ExposureTime"))->GetValue();
        eprintf("ExposureTime=%g\n", d);
      }
      catch (LogicalErrorException const &ex) {
        eprintf("Getting ExposureTime: %s\n", ex.what());
        throw;
      }

      try {
        CCommandPtr(camCtl.GetNode("AcquisitionStart"))->Execute();
      }
      catch (LogicalErrorException const &ex) {
        eprintf("Triggering AcquisitionStart: %s\n", ex.what());
        throw;
      }
    }

    grabStartTime = realtime();
    cam->StartGrabbing(100000);
  }
  catch (GenericException const &ex) {
    eprintf("Opening camera usbId=%s devSerial=%s: %s\n", cameraConfig.usbId.c_str(), cameraConfig.devSerial.c_str(), ex.what());
    if (cam) cam->Close();
    delete cam;
    cam = nullptr;
  }
  if (::flock(openLockFd, LOCK_UN) < 0) {
    throw runtime_error("Can't flock(LOCK_UN) /tmp/pylon.lock: "s + strerror(errno));
  }
  if (0) eprintf("Unlocked /tmp/pylon.lock\n");
}

void PylonCameraClient::startPylonClient()
{
  if (cam) return;
  openPylonCamera();
  if (!cam) return;

  grabThread = new std::thread([this] {
    // On non-uv thread
    CGrabResultPtr grabResult;
    double prevFrameTs = 0.0;
    double baseTime = 0.0;
    int64_t baseTicks = 0;
    {
      using namespace GenApi;
      INodeMap &camCtl = cam->GetNodeMap();

      auto tsLatchCmd = CCommandPtr(camCtl.GetNode("TimestampLatch"));
      auto tsLatchVal = CIntegerPtr(camCtl.GetNode("TimestampLatchValue"));

      double bestDur = 1e9;
      for (int synci = 0; synci < 5; synci++) {
        double t0 = realtime();
        tsLatchCmd->Execute();
        int64_t ticks = tsLatchVal->GetValue();
        double t1 = realtime();
        if (synci == 0 || t1 - t0 < bestDur) {
          bestDur = t1 - t0;
          baseTime = t0;
          baseTicks = ticks;
        }
      }
      if (verbose >= 0 || bestDur > 0.001)
        eprintf("Synced in %0.6f baseTime=%0.6f baseTicks=%zd\n", bestDur, baseTime, baseTicks);
    }

    while (cam->IsGrabbing()) {
      /*
        If we can't keep up, throttling happens here. The pylon library keeps its own pool of buffers (should be 10)
        and will drop frames if we don't free up grabResult.
      */
      try {
        cam->RetrieveResult(5000, grabResult, TimeoutHandling_ThrowException);
      }
      catch (RuntimeException const &ex) {
        eprintf("Grabbing frame: %s\n", ex.what());
        throw;
      }
      int64_t grabTicks = grabResult->GetTimeStamp();
      double frameTs = baseTime + (double)(grabTicks - baseTicks) * 0.000000001;
      double ts = realtime();
      if (ts > recordUntil) {
        if (0) eprintf("Exceeded recordUntil (%0.3f > %0.3f)\n", ts, recordUntil);
        break;
        // cam->StopGrabbing();
      }
      if (grabResult->GrabSucceeded()) {
        if (verbose >= 3 || (verbose >= 2 && grabResult->GetImageNumber() % 20 == 1))
          eprintf(
              "Grabbed %dx%d (%lu bytes) image=%ld frameTs=%0.6f diff=%0.6f (1/%0.2f) delay=%0.6f\n",
              grabResult->GetWidth(),
              grabResult->GetHeight(),
              grabResult->GetImageSize(),
              grabResult->GetImageNumber(),
              frameTs,
              frameTs - prevFrameTs,
              1.0 / (frameTs - prevFrameTs),
              ts - frameTs);
        prevFrameTs = frameTs;

        /*
          We want to do the compression & writing on a uv worker thread. We can only enqueue work from the
          main thread, so first we use the asyncQueue to move to the main thread.
        */
        asyncQueue.push([this, grabResult, frameTs] {
          // On main thread

          auto mainFrame = make_shared<YogaVideoFrame>();
          mainFrame->format = YVFF_JPEG;
          mainFrame->width = grabResult->GetWidth();
          mainFrame->height = grabResult->GetHeight();
          mainFrame->orientation = cameraConfig.baseOrientation;
          mainFrame->priority = 0;

          if (cameraConfig.captureStillsInterval > 0.0 && frameTs - lastStillTs > cameraConfig.captureStillsInterval) {
            mainFrame->priority = 1;
            lastStillTs = frameTs;
          }

          uvWork(
              [grabResult, thisChunkId = curChunkId, mainFrame](string &error, shared_ptr<void> &result) {
                // On worker thread
                U8 *jBuf{nullptr};
                size_t jSize{0};
                compressJpeg(grabResult, jBuf, jSize);

                /*
                  Note that these might not be written in order, but the ChunkFile guarantees atomic writing of
                  the entire part + length and will return the correct starting position.
                */

                auto blobRef = mkBlob(thisChunkId, jBuf, jSize);
                mainFrame->blob = blobRef;

                tjFree(jBuf);
              },
              [this, frameTs, mainFrame](string const &error, shared_ptr<void> const &result) {
                // On main thread
                videoQueue.emplace_back(frameTs, *mainFrame);
              });
        });
      }
      else {
        eprintf("Grab error: %d %s\n", grabResult->GetErrorCode(), grabResult->GetErrorDescription().c_str());
      }
    }
    eprintf("Closing camera\n");
    try {
      cam->Close();
      delete cam;
      cam = nullptr;
    }
    catch (RuntimeException const &ex) {
      eprintf("Closing camera: %s\n", ex.what());
      throw;
    }
  });
}


#else

shared_ptr<CameraClient>
CameraClient::mkPylonCameraClient(RemoteCameraConfig const &cameraConfig, VideoTraceSpec const &traceSpec)
{
  return nullptr;
}

#endif
