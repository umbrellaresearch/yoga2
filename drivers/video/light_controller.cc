#include "../../../comm/engine.h"
#include "common/uv_wrappers.h"

struct LightControllerEngine : GenericEngine {
  LightControllerEngine()
  : ctlHost("localhost"),
    ctlPort("16178")
  {
  }
  string ctlHost;
  string ctlPort;
  string lights;

  void afterSetRunning() override;
  void update(EngineUpdateContext &ctx) override;

  UvStream sock;
  sockaddr *remoteAddr{nullptr};
  string remoteAddrDesc;
  
  R lastSendTime{0.0};
};

void LightControllerEngine::afterSetRunning()
{
  sock.udp_init();

  uvGetAddrInfo(
    ctlHost,
    ctlPort,
    addrinfoForUdp(),
    [this, thisp=shared_from_this()](int status, struct addrinfo *result) {
      if (status < 0) {
        throw uv_error("UvGetAddrInfo", status);
      }

      assert(result->ai_family == PF_INET);
      auto addr_in = new sockaddr_in(*(sockaddr_in *)result->ai_addr);
      remoteAddrDesc = sockaddr_desc(reinterpret_cast<sockaddr *>(addr_in));

      if (verbose >= 1) console("ctlHost at "s + remoteAddrDesc);

      remoteAddr = reinterpret_cast<sockaddr *>(addr_in);
    });
}

void LightControllerEngine::update(EngineUpdateContext &ctx)
{
  if (isRunning() && ctx.now - lastSendTime > 1.0 && remoteAddr) {
    if (!sock.stream) return;

    lastSendTime = ctx.now;

    if (verbose >= 1) console("send to "s + remoteAddrDesc + ": " + shellEscape(lights));

    packet tx;
    tx.add(lights);

    sock.udp_send((char const *)tx.ptr(), tx.remaining(), remoteAddr, [this](int status) {
      if (status == UV_EAI_CANCELED) {
        console("udp_send: cancelled"s);
        return;
      }
      if (status < 0) throw uv_error("udp_send", status);
    });


  }
}


static EngineRegister lightControllerReg(
  "lightController",
  {
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 0) {
      return ctx.logError("lightController: expected 0 arguments");
    }

    auto e = make_shared<LightControllerEngine>();

    if (!spec->options->getValueForKey("ctlHost", e->ctlHost, true)) {
      return ctx.logError("Bad ctlHost");
    }
    if (!spec->options->getValueForKey("ctlPort", e->ctlPort, false)) {
      return ctx.logError("Bad ctlPort");
    }

    vector<string> lights;
    if (!spec->options->getValueForKey("lights", lights)) {
      return ctx.logError("Bad lights");
    }
    e->lights = joinChar(lights, ',');

    if (!e->setEngineConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    return true;
  }
);
