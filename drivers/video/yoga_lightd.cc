#include "common/std_headers.h"
#include "common/packetbuf_types.h"
#include "common/uv_wrappers.h"
#include "../../../jit/jit_utils.h"
#include <getopt.h>

/*
  Testing:
  printf "\x0bpowerbar0:1" | nc -w 1 -u nimes 16178
*/

struct RequestState {
  double lastTime{0.0};
  bool desiredState{false};
  bool actualState{false};
};

map<string, shared_ptr<RequestState>> activeRequests;
R lastActivityTime;

shared_ptr<UvTimer> timeoutTimer;
shared_ptr<UvStream> mainSock;
bool fromInetd = false;


void powerbarCmd(string const &host, string const &port, string const &cmd, std::function<void (int)> const &cb)
{
  auto httpSock = make_shared<UvStream>();
  httpSock->tcp_init();
  httpSock->tcp_connect_dns(host, port, [cmd, host, cb, httpSock](int status) {
    if (status < 0) {
      cerr << host + ": connect: "s + uv_strerror(status) + "\n";
      cb(status);
      return;
    }
    string req;
    req = "GET /cmd.cgi?" + cmd + " HTTP/1.0\r\n" +
      "Authorization: Basic YWRtaW46YWRtaW4=\r\n" +  // admin:admin (sorry)
      "Host: " + host + "\r\n\r\n";
    httpSock->write(req, [cmd, host, cb, httpSock](int status) {
      if (status < 0) {
        cerr << host + ": write: "s + uv_strerror(status) + "\n";
        cb(status);
        return;
      }
      auto rsp = make_shared<string>();
      httpSock->read_start([cmd, host, cb, httpSock, rsp](ssize_t nr, uv_buf_t const *buf) {
        if (nr == UV_EOF) {
          auto lines = splitChar(*rsp, '\n');
          if (lines.size() > 0 && lines[0] == "HTTP/1.1 200 OK\r") {
            cb(0);
            return;
          }
          cerr << host + ": got response "s + *rsp + "\n";
          cb(0);
          return;
        }
        else if (nr < 0) {
          cerr << host + ": read: "s + uv_strerror(nr) + "\n";
          cb(nr);
          return;
        }
        else {
          rsp->append(buf->base, nr);
          return;
        }
      });
    });
  });
}

void setLight(string const &host, int lightNo, bool on, std::function<void (int) > const &cb)
{
  string cmd = "$A3+" + to_string(lightNo) + "+" + (on ? "1" : "0");
  powerbarCmd(host, "80", cmd, cb);
}

void syncLights()
{
  for (auto &[lightName, lightReq] : activeRequests) {
    if (lightReq->desiredState != lightReq->actualState) {
      bool lightState = lightReq->desiredState;
      lightReq->actualState = lightState;

      vector parts = splitChar(lightName, ':');
      if (parts.size() != 2) {
        cerr << "Bad light name "s + shellEscape(lightName) + "\n";
        continue;
      }
      setLight(parts[0], stoi(parts[1]), lightState, [lightName=lightName, lightState](int status) {
        if (status < 0) {
          cerr << "Light " + shellEscape(lightName) + ": " + uv_strerror(status) + "\n";
        }
        else {
          cerr << "Successfully turned " + lightName + " " + (lightState ? "on" : "off") + "\n";
        }
      });
    }
  }
}

void timeoutLights()
{
  R now = realtime();
  bool didSome = false;
  for (auto &it : activeRequests) {
    if (it.second->desiredState) {
      if (now - it.second->lastTime > 5.0) {
        it.second->desiredState = false;
        didSome = true;
      }
    }
  }
  if (didSome) {
    lastActivityTime = now;
    syncLights();
  }

  if (now - lastActivityTime > 15.0) {
    cerr << "Timeout: closing sockets, hoping for libuv exit\n";
    if (mainSock) {
      mainSock->read_stop();
      mainSock = nullptr;
    }
    if (timeoutTimer) {
      timeoutTimer->timer_stop();
      timeoutTimer = nullptr;
    }
    
  }
}

void handleMainSock()
{
  mainSock->udp_recv_start([](ssize_t nread, uv_buf_t const *buf, struct sockaddr const *addr, u_int flags) {
    R now = realtime();
    lastActivityTime = now;
    if (!addr) {
      if (1) cerr << "Nothing to read\n";
      return;
    }
    if (nread < 0) {
      cerr << "Read: "s + uv_strerror(nread) + "\n";
      return;
    }
    else if (nread == 0) {
      cerr << "Got empty packet flags=" + to_string(flags) + "\n";
      return;
    }
    else {
      if (0) cerr << "Got packet len="s + to_string(nread) + " flags=" + to_string(flags) + "\n";
    }
    packet rx((u_char *)buf->base, nread);
    string lights;
    rx.get(lights);

    for (auto light : splitChar(lights, ',')) {
      if (1) cerr << "Turn on " + light + "\n";
      auto &req = activeRequests[light];
      if (!req) {
        req = make_shared<RequestState>();
      }
      req->lastTime = realtime();
      req->desiredState = true;
    }
    syncLights();
  });
}

void usage()
{
  cerr << "usage: yoga_lightd [-d] [-C dir]\n";
}

int main(int argc, char * const * argv)
{
  apr_initialize();
  atexit(apr_terminate);

  static struct option longopts[] = {
    {nullptr, 0, nullptr, 0}
  };

  int ch;
  while ((ch = getopt_long(argc, argv, "C:d", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          cerr << string(optarg) + ": "s + strerror(errno);
          return 1;
        }
        break;

      case 'd':
        fromInetd = true;
        close(2);
        open("yoga_lightd.log", O_WRONLY|O_CREAT|O_APPEND, 0666);
        break;

      default:
        usage();
        return 2;
    }
  }

  argc -= optind;
  argv += optind;

  cerr << "yoga_lightd: main starting\n";

  lastActivityTime = realtime();

  timeoutTimer = make_shared<UvTimer>();
  timeoutTimer->timer_init();
  timeoutTimer->timer_start([]() {
    timeoutLights();
  }, 500, 500);

  mainSock = make_shared<UvStream>();
  mainSock->udp_init();
  if (fromInetd) {
    mainSock->udp_open(dup(0));
    handleMainSock();
  }
  else {
    mainSock->udp_bind_dns("0.0.0.0", "16178", [](int rc, string const &boundTo) {
      if (rc < 0) throw uv_error("bind to 0.0.0.0:16178", rc);
      cerr << "Bound to "s + boundTo + "\n";

      handleMainSock();
    });
  }

  uvRunMainThread();
  cerr << "yoga_lightd: main exiting\n";
  return 0;
}
