# Pretends to be an odrive

POLE_PAIRS = 15
VEL_TO_CURRENT = 0.01569606115 # close enough for realistic values?

class enums:
    AXIS_STATE_CLOSED_LOOP_CONTROL = 8
    AXIS_STATE_IDLE = 42

class Config:
    def __init__(self):
        self.pole_pairs = POLE_PAIRS

class CurrentControl:
    def __init__(self):
        self.Iq_setpoint = 0

class Controller:
    def __init__(self, encoder, motor):
        self.encoder = encoder
        self.motor = motor
        self.vel_setpoint = 0

    def __setattr__(self, item, value):
        if self.__dict__.get('encoder') is not None and \
            self.__dict__.get('motor') is not None and \
            item == 'vel_setpoint':
            self.__dict__[item] = value
            self.encoder.pll_vel = value
            self.motor.current_control.Iq_setpoint = value * VEL_TO_CURRENT
            return value
        else:
            self.__dict__[item] = value
            return value

class Motor:
    def __init__(self):
        self.config = Config()
        self.current_control = CurrentControl()

class Encoder:
    def __init__(self):
        self.pll_vel = 0
        self.pos_estimate = 0

class Axis:
    def __init__(self):
        self.encoder = Encoder()
        self.motor = Motor()
        self.controller = Controller(self.encoder, self.motor)

class FauxDrive:
    def __init__(self):
        self.axis0 = Axis()
        self.axis1 = Axis()
        self.serial_number = "123456789"

def find_any(name):
    return FauxDrive()
