#include "../../../comm/ssc_network_engine.h"
#include "./odrive_io.h"

struct ODriveNetworkEngine : SscNetworkEngine {
  void setupHandlers() override;
  void setupAccessors() override;
  void update(EngineUpdateContext &ctx) override;

  R lastCmdTs{0.0};
  int txSeqRx{0}, txSeq{0};

  shared_ptr<YogaTimeseq> stateOut;
  unique_ptr<ODriveStateAccessorSuite> stateAs;
  shared_ptr<YogaTimeseq> cmdIn;
  unique_ptr<ODriveCmdAccessorSuite> cmdAs;
};


void ODriveNetworkEngine::setupAccessors()
{
  SscNetworkEngine::setupAccessors();
  stateAs = make_unique<ODriveStateAccessorSuite>(stateOut->type);
  cmdAs = make_unique<ODriveCmdAccessorSuite>(cmdIn->type);
}

void ODriveNetworkEngine::update(EngineUpdateContext &ctx)
{
  double minDt = (txSeqRx == txSeq) ? 0.002 : 0.050;
  if (ctx.now - lastUpdateRequestTs >= minDt && cmdIn) {

    YogaPool tmpmem;
    auto cmd1 = cmdIn->getLast();
    if (!cmd1 && lastUpdateRequestTs == 0.0) {
      // Send an initial 0 drive command to prime the pump
      cmd1.setZero(tmpmem.it);
    }
    else if (!cmd1) {
      return;
    }

    txSeq++;
    packet tx;
    tx.add((U8)'V');
    tx.add((U32)txSeq);
    cmdAs->txCmd(tx, cmd1);
    txPkt(tx);
    lastUpdateRequestTs = ctx.now;
  }
  SscNetworkEngine::update(ctx);
}

void ODriveNetworkEngine::setupHandlers()
{
  SscNetworkEngine::setupHandlers();
  rxHandlers[(U8)'v'] = [this](double rxTime, packet &rx, bool &error) {
    if (!stateOut) return;
    rx.get(txSeqRx);
    auto state0 = stateOut->addNew(rxTime - rti->realTimeOffset);
    stateAs->rxState(rx, state0);
  };
}


static YogaCodeRegister registerOdriveTypes("odrive_network_engine.cc", R"(
  struct ODriveCmd {
    R~20 lVel, rVel; // radians/sec
  };

  struct ODriveStatus {
    R active; // 1 if the drive is responding to commands
    R~20 lVelActual, rVelActual; // radians/sec
    R~10 lTorque, rTorque; // N-m
    R~100 lPos, rPos; // radians
  };
)");

static EngineRegister odriveNetworkEngineReg(
  "odriveNetwork", 
  {
    "ODriveCmd",
    "ODriveStatus"
  },
  [](YogaContext const &ctx, Trace *trace, string const &engineName, AstAnnoCall *spec, vector<shared_ptr<YogaTimeseq>> const &args)
  {
    if (args.size() != 2) {
      return ctx.logError("odriveNetwork: expected 2 arguments (cmd, state)");
    }

    auto e = make_shared<ODriveNetworkEngine>();
    if (args.size() > 0) {
      e->cmdIn = args[0];
      e->cmdIn->isExtOut = true;
    }
    if (args.size() > 1) {
      e->stateOut = args[1];
      e->stateOut->needBackdateInitial = true;
      e->stateOut->isExtIn = true;
    }

    if (!e->setEngineConfig(ctx, spec)) return false;
    if (!e->setNetworkConfig(ctx, spec)) return false;
    trace->addEngine(engineName, e, true);

    return true;
  }
);

