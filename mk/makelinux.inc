
CXX = clang++-10
CC = clang-10

CFLAGS += 

INCLUDES += -I/usr/include/apr-1.0

ifneq (,$(wildcard /opt/pylon5/include))
INCLUDES += -I/opt/pylon5/include/
CXXFLAGS += -DUSE_PYLON
CAMERAD_LDFLAGS += \
	-L/opt/pylon5/lib64 \
	-Wl,-rpath,/opt/pylon5/lib64 \
	-lpylonbase \
	-lpylonutility \
	-lGenApi_gcc_v3_0_Basler_pylon_v5_0 \
	-lGCBase_gcc_v3_0_Basler_pylon_v5_0
endif

ifneq (,$(wildcard /usr/local/lib/librealsense2.so))
INCLUDES += -I/usr/local/include/librealsense2
CXXFLAGS += -DUSE_REALSENSE
LDFLAGS += -L/usr/local/lib -lrealsense2
endif

CAMERAD_LDFLAGS += -lturbojpeg
ifneq (,$(wildcard /usr/include/alsa))
CXXFLAGS += -DUSE_ALSA
CAMERAD_LDFLAGS += -lasound
endif

ifneq (,$(wildcard /usr/local/lib/libale.a))
CXXFLAGS += -DUSE_ALE
LDFLAGS += -lale
endif

INCLUDES += $(IMGUI_INCLUDES)
INCLUDES += -I/usr/include/eigen3
LDFLAGS += -lpthread -ldl -lm
LDFLAGS += -fuse-ld=lld-10

LLVM_CONFIG := llvm-config-10

LLVM_VERSION := $(shell $(LLVM_CONFIG) --version)

ifneq ($(LLVM_VERSION),10.0.1)
check.deps ::
	echo "Need llvm 10.0.1, found $(LLVM_VERSION)"
endif

LLVM_CXXFLAGS += $(shell $(LLVM_CONFIG) --cppflags)
CXXFLAGS += $(LLVM_CXXFLAGS)
LLVM_LDFLAGS += $(shell $(LLVM_CONFIG) --ldflags --libs core orcjit native passes --system-libs)

# export-dynamic is needed on Linux, so that functions like yogic_alloc defined in the main executable are available for dynamic
# lookup by the JIT loader
LLVM_LDFLAGS += -Wl,--export-dynamic

STUDIO_LDFLAGS += $(SDL_LDFLAGS) -lpng

ifneq (,$(wildcard /usr/include/spnav.h))
CXXFLAGS += -DUSE_SPNAV
STUDIO_LDFLAGS += -lspnav
endif

SDL_LDFLAGS += $(shell sdl2-config --libs)
SDL_CXXFLAGS += $(shell sdl2-config --cflags)
SDL_LDFLAGS += -lGL

CXXFLAGS += $(SDL_CXXFLAGS)

check.deps :: 
	@file $(shell $(LLVM_CONFIG) --libfiles) || ( echo "Yoga requires llvm-10-dev. make install.deps can install it for you"; false )
	@which $(CC) || ( echo "Yoga requires an executable for $(CC)"; false )
	@which $(CXX) || ( echo "Yoga requires an executable for $(CXX)" ; false )

ubuntu-deluxe:
	sudo docker build -t registry.gitlab.com/umbrellaresearch/yoga2:ubuntu-buildbase-clang10 mk/ubuntu-deluxe
	sudo docker push registry.gitlab.com/umbrellaresearch/yoga2:ubuntu-buildbase-clang10
