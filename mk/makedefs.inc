
ODIR = build.$(UNAME_MACHINE)

SHELL := bash
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

INSTALL_DIR ?= $(YOGA_DIR)

IMGUI_INCLUDES += \
	-I deps/imgui \
	-I deps/imgui/examples \
	-I deps/imgui/examples/libs \
	-I deps/imgui/examples/libs/gl3w \

INCLUDES += -I deps
INCLUDES += -I deps/tlbcore
INCLUDES += -I build.src

ifeq ($(CI_SERVER), yes)
DEBUG := 
else
DEBUG := -O2
-include mk/makelocal.inc
endif

BOTD_LDFLAGS =

ifeq ($(UNAME_SYSTEM),Darwin)
SUPPORTED_OS := yes
include mk/makedarwin.inc
endif

ifeq ($(UNAME_SYSTEM),Linux)
SUPPORTED_OS := yes
include mk/makelinux.inc
endif

ifneq (yes,$(SUPPORTED_OS))
$(error "Unsupported operating system $(UNAME_SYSTEM). Maybe you can add it in mk/makedefs.inc")
endif

CXXFLAGS += $(DEBUG)
CXXFLAGS += -std=c++1z
CXXFLAGS += -Wall -Wformat


LDFLAGS += -lsqlite3  -lz
LDFLAGS += -laprutil-1 -lapr-1
LDFLAGS += -luv
LDFLAGS += $(DEBUG)

ifeq (yes,$(SANITIZE))
CXXFLAGS += -fsanitize=undefined
LDFLAGS += -fsanitize=undefined
endif

show.defs :: ## Show preprocessor defines
	@echo "C defs = $(filter -D%, $(CFLAGS))"
	@echo "C++ defs = $(filter -D%, $(CXXFLAGS))"
