

require("yoga/drivers/dex4/cad42/cad42.yoga");

function jointPosPred(
  out R pos,
  in SscPneuservoState joint,
  in R lookahead)
{
  pos = joint.pos + lookahead * joint.vel;
}

function dex4Kin(
    out Cad42Positions kin,
    out Cad42Positions kinSoon,
    in InertialState inertial,
    in Dex4LegState ll,
    in Dex4LegState rl,
    in Dex4FootState lf,
    in Dex4FootState rf) {

  c = {
    soonPeriod: 0.391~*1,
    soonVelLim: 1.0~*1,
  };

  joints = Dex4JointVals({
    lht: jointPosPred(., ll.ht, 0),
    lhf: jointPosPred(., ll.hf, 0),
    lhs: jointPosPred(., ll.hs, 0),
    lk: jointPosPred(., ll.ki, 0),
    lai: jointPosPred(., ll.ai, 0), 
    lao: jointPosPred(., ll.ao, 0),
    rht: jointPosPred(., rl.ht, 0), 
    rhf: jointPosPred(., rl.hf, 0), 
    rhs: jointPosPred(., rl.hs, 0),
    rk: jointPosPred(., rl.ki, 0), 
    rai: jointPosPred(., rl.ai, 0), 
    rao: jointPosPred(., rl.ao, 0)
  });


  gyroOfs = GyroOfs(0, 0, 0);
  torso = matToHomo(inertial.robotMentalOrientation);

  calcCad42(_, _, kin, gyroOfs, joints, torso);

  rmsVel = (
    inertial.robotMentalRotRate[0]^2 +
    inertial.robotMentalRotRate[1]^2 +
    inertial.robotMentalRotRate[2]^2 +
    ll.ht.vel^2 / 2.0 +
    ll.hf.vel^2 +
    ll.hs.vel^2 +
    ll.ki.vel^2 / 2.0 +
    ll.ai.vel^2 / 4.0 +
    ll.ao.vel^2 / 4.0 +
    rl.ht.vel^2 / 2.0 +
    rl.hf.vel^2 +
    rl.hs.vel^2 +
    rl.ki.vel^2 / 2.0 +
    rl.ai.vel^2 / 4.0 +
    rl.ao.vel^2 / 4.0) ^ 0.5 / 10.0;

  soon = c.soonPeriod / max(1.0, rmsVel/c.soonVelLim); // seconds into the future

  jointsSoon = Dex4JointVals({
    lht: jointPosPred(., ll.ht, soon),
    lhf: jointPosPred(., ll.hf, soon),
    lhs: jointPosPred(., ll.hs, soon),
    lk: jointPosPred(., ll.ki, soon),
    lai: jointPosPred(., ll.ai, soon),
    lao: jointPosPred(., ll.ao, soon),
    rht: jointPosPred(., rl.ht, soon),
    rhf: jointPosPred(., rl.hf, soon),
    rhs: jointPosPred(., rl.hs, soon),
    rk: jointPosPred(., rl.ki, soon),
    rai: jointPosPred(., rl.ai, soon),
    rao: jointPosPred(., rl.ao, soon)
  });

  gyroOfsSoon = GyroOfs(
    inertial.robotMentalRotRate[0] * soon,
    inertial.robotMentalRotRate[1] * soon,
    inertial.robotMentalRotRate[2] * soon);

  calcCad42(_, _, kinSoon, gyroOfsSoon, jointsSoon, torso);

}

