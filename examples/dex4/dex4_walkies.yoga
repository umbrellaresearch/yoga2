

struct Dex4WalkiesState {
  R curMoveAge;
}

function basicBalance(
  out Dex4LegCtl rl,
  out Dex4LegCtl ll,
  out Dex4BalanceTarget target,
  in R t,
  in Dex4ToplevelCmd top,
  in YogaKeyboard kbd)
{

  c = {
    yAnkleTorqueFactor: 25~30,
    yKneeTorqueFactor: 25~30,
    yHipTorqueFactor: 18~30,
    xAnkleTorqueFactor: 0~30,
    xKneeTorqueFactor: 0~30,
    xHipTorqueFactor: 0~30,
    yAnklePosFactor: 0~30,
    yKneePosFactor: 0~30,
    yHipPosFactor: 0~30,
    xAnklePosFactor: 0.080~0.3,
    xKneePosFactor: 0.083~0.3,
    xHipPosFactor: 0.040~0.3,
  };
  ll.ht.pos = 0.0~1;
  ll.hf.pos = 0.0~1;
  ll.hs.pos = 0~1;
  ll.k.pos = 0~1;
  ll.ai.pos = 0~1 + 0.8~1 * top.toetap;
  ll.ao.pos = 0~1 + 0.8~1 * top.toetap;

  rl.ht.pos = 0~1;
  rl.hf.pos = 0.0~1;
  rl.hs.pos = 0~1;
  rl.k.pos = 0~1;
  rl.ai.pos = 0~1 + 0.8~1 * top.toetap;
  rl.ao.pos = 0~1 + 0.8~1 * top.toetap;

  target.laPos = R[3](-0.1~0.3, +0.03~0.3 + (kbd.b * 0.1), -1.15~1);
  target.raPos = R[3](+0.1~0.3, +0.03~0.3 + (kbd.b * 0.1), -1.15~1);
  target.laWeight = 0.5~1;
  target.raWeight = 0.5~1;

  ll.ai.leanForwardTorqueFb = -c.yAnkleTorqueFactor;
  ll.ao.leanForwardTorqueFb = -c.yAnkleTorqueFactor;
  rl.ai.leanForwardTorqueFb = -c.yAnkleTorqueFactor;
  rl.ao.leanForwardTorqueFb = -c.yAnkleTorqueFactor;
  ll.k.leanForwardTorqueFb = +c.yKneeTorqueFactor;
  rl.k.leanForwardTorqueFb = +c.yKneeTorqueFactor;
  ll.hs.leanForwardTorqueFb = +c.yHipTorqueFactor;
  rl.hs.leanForwardTorqueFb = +c.yHipTorqueFactor;

  ll.ai.leanBackTorqueFb = +c.yAnkleTorqueFactor;
  ll.ao.leanBackTorqueFb = +c.yAnkleTorqueFactor;
  rl.ai.leanBackTorqueFb = +c.yAnkleTorqueFactor;
  rl.ao.leanBackTorqueFb = +c.yAnkleTorqueFactor;
  ll.k.leanBackTorqueFb = -c.yKneeTorqueFactor;
  rl.k.leanBackTorqueFb = -c.yKneeTorqueFactor;
  ll.hs.leanBackTorqueFb = -c.yHipTorqueFactor;
  rl.hs.leanBackTorqueFb = -c.yHipTorqueFactor;

  ll.ai.leanRightTorqueFb = 0;
  ll.ao.leanRightTorqueFb = 0;
  rl.ai.leanRightTorqueFb = +c.xAnkleTorqueFactor;
  rl.ao.leanRightTorqueFb = +c.xAnkleTorqueFactor;
  ll.k.leanRightTorqueFb = 0;
  rl.k.leanRightTorqueFb = -c.xKneeTorqueFactor;
  ll.hs.leanRightTorqueFb = 0;
  rl.hs.leanRightTorqueFb = +c.xHipTorqueFactor;

  ll.ai.leanLeftTorqueFb = +c.xAnkleTorqueFactor;
  ll.ao.leanLeftTorqueFb = +c.xAnkleTorqueFactor;
  rl.ai.leanLeftTorqueFb = 0;
  rl.ao.leanLeftTorqueFb = 0;
  ll.k.leanLeftTorqueFb = -c.xKneeTorqueFactor;
  rl.k.leanLeftTorqueFb = 0;
  ll.hs.leanLeftTorqueFb = +c.xHipTorqueFactor;
  rl.hs.leanLeftTorqueFb = 0;


  ll.ai.leanForwardPosFb = -c.yAnklePosFactor;
  ll.ao.leanForwardPosFb = -c.yAnklePosFactor;
  rl.ai.leanForwardPosFb = -c.yAnklePosFactor;
  rl.ao.leanForwardPosFb = -c.yAnklePosFactor;
  ll.k.leanForwardPosFb = +c.yKneePosFactor;
  rl.k.leanForwardPosFb = +c.yKneePosFactor;
  ll.hs.leanForwardPosFb = +c.yHipPosFactor;
  rl.hs.leanForwardPosFb = +c.yHipPosFactor;

  ll.ai.leanBackPosFb = +c.yAnklePosFactor;
  ll.ao.leanBackPosFb = +c.yAnklePosFactor;
  rl.ai.leanBackPosFb = +c.yAnklePosFactor;
  rl.ao.leanBackPosFb = +c.yAnklePosFactor;
  ll.k.leanBackPosFb = -c.yKneePosFactor;
  rl.k.leanBackPosFb = -c.yKneePosFactor;
  ll.hs.leanBackPosFb = -c.yHipPosFactor;
  rl.hs.leanBackPosFb = -c.yHipPosFactor;

  ll.ai.leanRightPosFb = 0;
  ll.ao.leanRightPosFb = 0;
  rl.ai.leanRightPosFb = +c.xAnklePosFactor;
  rl.ao.leanRightPosFb = +c.xAnklePosFactor;
  ll.k.leanRightPosFb = 0;
  rl.k.leanRightPosFb = -c.xKneePosFactor;
  ll.hs.leanRightPosFb = 0;
  rl.hs.leanRightPosFb = +c.xHipPosFactor;

  ll.ai.leanLeftPosFb = +c.xAnklePosFactor;
  ll.ao.leanLeftPosFb = +c.xAnklePosFactor;
  rl.ai.leanLeftPosFb = 0;
  rl.ao.leanLeftPosFb = 0;
  ll.k.leanLeftPosFb = -c.xKneePosFactor;
  rl.k.leanLeftPosFb = 0;
  ll.hs.leanLeftPosFb = +c.xHipPosFactor;
  rl.hs.leanLeftPosFb = 0;

}

function dex4Walkies(
  out Dex4LegCtl rl,
  out Dex4LegCtl ll,
  out Dex4BalanceTarget target,
  update Dex4WalkiesState s,
  in Dex4ToplevelCmd top,
  in YogaKeyboard kbd)
{
  basicBalance(rl, ll, target, 0, top, kbd);
}
