import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import pandas as pd
from torch.utils.data import Dataset, DataLoader

class Net(nn.Module):

    sense_dim = 13
    cmd_dim = 1
    io_dim = sense_dim + cmd_dim
    
    def __init__(self):
        super(Net, self).__init__()
        self.fc1 = nn.Linear((self.io_dim) * 2 + 1, 32)
        self.fc2 = nn.Linear(32, 32)
        self.fc3 = nn.Linear(32, 32)
        self.fc4 = nn.Linear(32, self.io_dim)

    def forward(self, x):
        y = F.relu(self.fc1(x))
        y = F.relu(self.fc2(y))
        y = F.relu(self.fc3(y))
        y = self.fc4(y)
        y = y + 0.5 * x[:, 0:self.io_dim] + 0.5 * x[:, self.io_dim:self.io_dim*2]
        return y

class AcrobotDataset(Dataset):
    def __init__(self, csv_file):
        f = self.history_frame = pd.read_csv(csv_file)
        self.xs = f[[c for c in f.columns if (c=='dt' or c.startswith('t0.') or c.startswith('t2'))]]
        self.ys = f[[c for c in f.columns if (c.startswith('t1'))]]

    def __len__(self):
        return len(self.history_frame)

    def __getitem__(self, idx):
        input = torch.tensor(self.xs.iloc[idx].to_numpy(), dtype=torch.float32)
        target = torch.tensor(self.ys.iloc[idx].to_numpy(), dtype=torch.float32)

        return {'input': input, 'target': target}

def train():
    net_fn = 'data/acrobot_model.torch'
    net = Net()
    net.load_state_dict(torch.load(net_fn))

    ds = AcrobotDataset('/tmp/foo.csv')
    dataloader = DataLoader(ds, batch_size=32, shuffle=True, num_workers=0)

    optimizer = optim.Adam(net.parameters(), lr=0.001)
    criterion = nn.MSELoss()

    for epoch in range(2000):
        totloss = 0
        for i_batch, sample_batched in enumerate(dataloader):

            optimizer.zero_grad()   # zero the gradient buffers
            output = net(sample_batched['input'])
            loss = criterion(output, sample_batched['target'])
            totloss += float(loss)
            loss.backward()
            optimizer.step()    # Does the update

        if epoch%10 == 0:
            print("Epoch", epoch, "Loss", totloss / len(ds))

    torch.save(net.state_dict(), net_fn)

if __name__ == '__main__': train()
