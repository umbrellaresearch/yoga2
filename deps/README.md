# Dependencies

## Copied

- [catch2](https://github.com/catchorg/Catch2)
- [imgui](https://github.com/ocornut/imgui) or clone git@github.com:ocornut/imgui.git
- [nlohmann-json](https://github.com/nlohmann/json)

## Submodules

- [tlbcore](https://gitlab.com/tlb/tlbcore)
- [IconFontCppHeaders](https://github.com/juliettef/IconFontCppHeaders) or clone git@github.com:juliettef/IconFontCppHeaders.git
- [fontawesome](https://fontawesome.com)
