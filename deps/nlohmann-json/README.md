# nlohmann-json

This directory has the 2 relevant files from [https://github.com/nlohmann/json],
which is too massive a repo to bring in as a submodule.

This is version 3.8.0
