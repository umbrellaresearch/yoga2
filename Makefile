
default: build

export YOGA_DIR := $(realpath .)

include deps/tlbcore/mk/makesystem.inc
include mk/makedefs.inc

TLBCORE_SRC += \
	deps/tlbcore/common/chunk_file.cc \
	deps/tlbcore/common/hacks.cc \
	deps/tlbcore/common/hash.cc \
	deps/tlbcore/common/host_debug.cc \
	deps/tlbcore/common/host_profts.cc \
	deps/tlbcore/common/str_utils.cc \
	deps/tlbcore/common/parengine.cc \
	deps/tlbcore/common/packetbuf.cc \
	deps/tlbcore/common/uv_wrappers.cc \
	deps/tlbcore/numerical/haltonseq.cc \
	deps/tlbcore/numerical/numerical.cc \
	deps/tlbcore/numerical/polyfit.cc \
	deps/tlbcore/numerical/windowfunc.cc

YOGA_COMMON_SRC += \
	$(TLBCORE_SRC) \
	comm/engine.cc \
	comm/ssc_network_engine.cc \
	comm/ssc_io.cc \
	comm/packet_network_engine.cc \
	comm/parent_pipe.cc \
	comm/child_pipe.cc \
	comm/tcp_pipe.cc \
	constraint/constraint.cc \
	db/yoga_db.cc \
	db/yoga_layout.cc \
	db/blob.cc \
	db/policy.cc \
	db/jsonio.cc \
	drivers/video/video_types.cc \
	drivers/ui/ui_types.cc \
	geom/solid_geometry.cc \
	lib/core_types.cc \
	lib/scope_math.cc \
	jit/runtime.cc \
	jit/memory.cc \
	jit/jit_utils.cc \
	remote/livestream_rx_engine.cc \
	remote/livestream_tx_engine.cc \
	timeseq/gradient_set.cc \
	timeseq/timeseq.cc \
	timeseq/trace.cc \
	timeseq/trace_blobs.cc \
	timeseq/trace_export.cc


YOGA_JIT_SRC += \
	jit/ast.cc \
	jit/ast_dump.cc \
	jit/context.cc \
	jit/compilation.cc \
	jit/compilation_llvm.cc \
	jit/emit_ctx.cc \
	jit/expr_backprop.cc \
	jit/expr_deriv.cc \
	jit/expr_dump.cc \
	jit/expr_llvm.cc \
	jit/expr_peephole.cc \
	jit/expr_types.cc \
	jit/jit_geom.cc \
	jit/polyfit.cc \
	jit/param.cc \
	jit/rewrite.cc \
	jit/sourcefile.cc \
	jit/tokenize.cc \
	jit/type.cc \
	jit/type_llvm.cc \
	jit/value.cc \
	jit/yoga_engine.cc

YOGA_DRIVER_SRC += \
	drivers/dex4/ssc_gyro/ssc_gyro_network_engine.cc \
	drivers/dex4/ssc_leg/ssc_leg_network_engine.cc \
	drivers/dex4/ssc_foot/ssc_foot_network_engine.cc \
	drivers/dex4/ssc_harness/ssc_harness_network_engine.cc \
	drivers/video/light_controller.cc \
	drivers/video/remote_camera_engine.cc \
	drivers/universal/universal_network_engine.cc \
	drivers/universal/barista_engine.cc \
	drivers/greppy/odrive/odrive_network_engine.cc \
	drivers/realsense/realsense_engine.cc \
	drivers/ale/ale_engine.cc


YOGA_STUDIO_SRC += \
	$(YOGA_COMMON_SRC) \
	$(YOGA_JIT_SRC) \
	$(YOGA_DRIVER_SRC) \
	constraint/constraint_ui.cc \
	drivers/dex4/vis/vis_pneuservo.cc \
	drivers/dex4/vis/vis_dex4.cc \
	drivers/ui/ui.cc \
	drivers/ui/vis_ui.cc \
	drivers/ale/vis_ale.cc \
	geom/panel3d.cc \
	geom/texcache.cc \
	remote/client.cc \
	studio/studio_main.cc \
	studio/main_window.cc \
	studio/scope_model.cc \
	studio/scope_model_io.cc \
	studio/yogastudio_ui.cc \
	studio/test_ui.cc \
	studio/ysgui.cc \
	studio/ysui_scrubber.cc \
	studio/trace_render.cc \
	studio/panel_render.cc \
	studio/panel_render_search.cc \
	studio/policy_terrain_render.cc \
	studio/trace_predict.cc \
	studio/trace_rerun.cc \
	studio/trace_terrain.cc \
	studio/trace_squiggle.cc \
	studio/policy_search.cc \
	studio/render_model.cc \
	studio/vis_train.cc

YOGA_BOTD_SRC += \
	$(YOGA_COMMON_SRC) \
	$(YOGA_JIT_SRC) \
	$(YOGA_DRIVER_SRC) \
	constraint/constraint_uistubs.cc \
	remote/botd_main.cc

YOGA_CAMERAD_SRC += \
	$(YOGA_JIT_SRC) \
	$(YOGA_COMMON_SRC) \
	constraint/constraint_uistubs.cc \
	drivers/video/yoga_camerad.cc \
	drivers/video/axis_camera_client.cc \
	drivers/video/alsa_audio_client.cc \
	drivers/video/pylon_camera_client.cc

IMGUI_SRC += \
	deps/imgui/examples/imgui_impl_sdl.cpp \
	deps/imgui/examples/imgui_impl_opengl3.cpp \
	deps/imgui/imgui.cpp \
	deps/imgui/imgui_draw.cpp \
	deps/imgui/imgui_widgets.cpp \
	deps/imgui/imgui_demo.cpp \

YOGA_TEST_SRC += \
	constraint/constraint_uistubs.cc \
	db/test_db.cc \
	jit/test_compile.cc \
	jit/test_debug.cc \
	jit/test_packetio.cc \
	jit/test_rdwr.cc \
	jit/test_layout.cc \
	jit/test_linear.cc \
	jit/test_callyoga.cc \
	jit/test_bloat.cc \
	jit/test_matrix_math.cc \
	jit/test_math.cc \
	jit/test_pola.cc \
	jit/test_grad.cc \
	jit/test_param.cc \
	jit/test_update.cc \
	lib/test_lib.cc \
	test/test_main.cc \
	test/test_utils.cc \
	test/test_str_utils.cc \
	test/test_uv.cc \
	$(YOGA_COMMON_SRC) \
	$(YOGA_JIT_SRC)

IMGUI_CSRC += \
       deps/imgui/examples/libs/gl3w/GL/gl3w.c

YOGA_LIGHTD_SRC += \
	$(TLBCORE_SRC) \
	drivers/video/yoga_lightd.cc

CXX_HDRS += \
	comm/child_pipe.h \
	comm/engine.h \
	comm/packet_network_engine.h \
	comm/parent_pipe.h \
	comm/ssc_io.h \
	comm/ssc_network_engine.h \
	comm/tcp_pipe.h \
	constraint/constraint_fwd.h \
	db/blob.h \
	db/policy.h \
	db/yoga_db.h \
	db/yoga_layout.h \
	deps/imgui/imconfig.h \
	deps/imgui/imgui.h \
	deps/imgui/imgui_internal.h \
	deps/imgui/imstb_rectpack.h \
	deps/imgui/imstb_textedit.h \
	deps/imgui/imstb_truetype.h	\
	drivers/ale/ale_types.h \
	drivers/dex4/ssc_foot/ssc_foot_defs.h \
	drivers/dex4/ssc_foot/ssc_foot_io.h \
	drivers/dex4/ssc_gyro/ssc_gyro_defs.h \
	drivers/dex4/ssc_gyro/ssc_gyro_io.h \
	drivers/dex4/ssc_harness/ssc_harness_defs.h \
	drivers/dex4/ssc_harness/ssc_harness_io.h \
	drivers/dex4/ssc_leg/ssc_leg_defs.h \
	drivers/dex4/ssc_leg/ssc_leg_io.h \
	drivers/greppy/odrive/odrive_io.h \
	drivers/ssc/bootloader/hwdefs.h \
	drivers/ui/ui.h \
	drivers/ui/ui_types.h \
	drivers/universal/robotiq_gripper.h \
	drivers/universal/universal_network_control.h \
	drivers/universal/universal_network_engine.h \
	drivers/universal/universal_network_monitor.h \
	drivers/universal/universal_network_script.h \
	drivers/universal/universal_network_utils.h \
	drivers/universal/universal_types.h \
	drivers/video/alsa_includes.h \
	drivers/video/camera_client.h \
	drivers/video/pylon_includes.h \
	drivers/video/video_types.h	\
	geom/solid_geometry.h \
	geom/geom.h \
	geom/panel3d.h \
	geom/solid_geometry.h \
	geom/texcache.h \
	geom/yogagl.h \
	jit/annotation.h jit/annotation_impl.h \
	jit/ast.h jit/ast_impl.h \
	jit/compilation.h \
	jit/context.h \
	jit/effects.h jit/effects_impl.h \
	jit/emit_ctx.h \
	jit/expr.h jit/expr_impl.h \
	jit/jit_fwd.h \
	jit/jit_utils.h \
	jit/llvm_fwd.h \
	jit/memory.h \
	jit/param.h jit/param_impl.h \
	jit/parse.h \
	jit/pass_base.h \
	jit/pass_compute_exprs.h \
	jit/pass_debug_info.h \
	jit/pass_define_types.h \
	jit/pass_extract_params.h \
	jit/pass_warnings.h \
	jit/polyfit.h \
	jit/runtime.h \
	jit/sourcefile.h \
	jit/tokenize.h \
	jit/type.h jit/type_impl.h \
	jit/value.h \
	jit/yoga_engine.h \
	lib/core_types.h \
	lib/scope_math.h \
	remote/client.h \
	remote/livestream_rx_engine.h \
	remote/livestream_tx_engine.h \
	studio/code_drawer.h \
	studio/main_window.h \
	studio/panel_render.h \
	studio/policy_search.h \
	studio/render_model.h \
	studio/scope_model.h \
	studio/trace_render.h \
	studio/yogastudio_ui.h \
	studio/ysgui.h \
	test/test_utils.h \
	timeseq/gradient_set.h \
	timeseq/runtime_info.h \
	timeseq/timeseq.h \
	timeseq/timeseq_pipe.h \
	timeseq/trace.h \
	timeseq/trace_blobs.h

C_HDRS += \
	dspcore/dspcore.h \
	dspcore/fapproxdsp.h \
	dspcore/smootherdsp.h \
	dspcore/smoother.h \
	dspcore/vec3dsp.h \
	embedded/embedded_debounce.h \
	embedded/embedded_debug.h \
	embedded/embedded_hostif.h \
	embedded/embedded_mainloop.h \
	embedded/embedded_pktcom.h \
	embedded/embedded_printbuf.h \
	embedded/embedded_profts.h \
	embedded/embedded_timing.h \
	embedded/embedded_utils.h

# sort also removes duplicates
ALL_CXX_SRC = $(sort \
	$(YOGA_STUDIO_SRC) \
	$(YOGA_TEST_SRC) \
	$(YOGA_CAMERAD_SRC) \
	$(YOGA_BOTD_SRC) \
	$(YOGA_LIGHTD_SRC) \
	$(IMGUI_SRC) \
	$(IMGUI_CSRC))
	

build : bin/yogastudio bin/yoga_botd bin/yoga_test ./compile_commands.json ## Build yoga studio

ifeq ($(UNAME_SYSTEM),Linux)
build : bin/yoga_camerad
build : bin/yoga_lightd
endif


bin/yogastudio : $(patsubst %.cc,$(ODIR)/%.o,$(YOGA_STUDIO_SRC)) $(patsubst %.cpp,$(ODIR)/%.o,$(IMGUI_SRC)) $(patsubst %.c,$(ODIR)/%.o,$(IMGUI_CSRC)) $(patsubst %.m,$(ODIR)/%.o,$(YOGA_STUDIO_OBJCSRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LLVM_LDFLAGS) $(LDFLAGS) $(SDL_LDFLAGS) $(STUDIO_LDFLAGS) -ljpeg

bin/yoga_botd : $(patsubst %.cc,$(ODIR)/%.o,$(YOGA_BOTD_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LLVM_LDFLAGS) $(LDFLAGS) $(BOTD_LDFLAGS)

bin/yoga_camerad : $(patsubst %.cc,$(ODIR)/%.o,$(YOGA_CAMERAD_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LLVM_LDFLAGS) $(CAMERAD_LDFLAGS) $(LDFLAGS)

bin/yoga_test : $(patsubst %.cc,$(ODIR)/%.o,$(YOGA_TEST_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LLVM_LDFLAGS) $(LDFLAGS) 

bin/yoga_lightd : $(patsubst %.cc,$(ODIR)/%.o,$(YOGA_LIGHTD_SRC))
	@echo link $@
	@mkdir -p $(dir $@)
	@$(CXX) -o $@ $+ $(LDFLAGS) 

test :: bin/yoga_test ## Compile and run the entire test suite
	bin/yoga_test

testnew :: bin/yoga_test ## Compile and run only the tests tagged with [new]
	bin/yoga_test "[new]"

bloat :: build ## Report and log executable size
	@echo;echo;echo | tee -a logs/bloat.log
	@echo "Testing on $(UNAME_SYSTEM) $(UNAME_HOST) DEBUG=$(DEBUG)" | tee -a logs/bloat.log
	@date | tee -a logs/bloat.log
	@size bin/yogastudio bin/yoga_botd | tee -a logs/bloat.log
	@bin/yoga_test -d yes | tee -a logs/bloat.log



include $(YOGA_DIR)/mk/makerules.inc
include $(YOGA_DIR)/mk/makedeps.inc
include $(YOGA_DIR)/deploy/makedeploy.inc

ifneq (,$(wildcard $(ODIR)))
-include $(shell find $(ODIR) -name '*.d' 2>/dev/null)
endif
