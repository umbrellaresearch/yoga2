#pragma once
#include "common/std_headers.h"
#include "../jit/compilation.h"
#include "../jit/value.h"
#include "../jit/runtime.h"
#include "../db/policy.h"

struct Trace;
struct ScopeModel;
struct ScopeTraceRenderer;
struct ScopePanelRenderer;
struct YogaClient;
struct YogaLiveSim;
struct YogaDb;
struct YogaTimeseq;
struct PolicyTerrainMap;
struct PolicyTrendLines;
struct PolicySearchState;
struct PredictTrainingSet;
struct AstAnnoCall;

struct ScopeModelTraceSpec {
  ScopeModelTraceSpec() = default;
  ScopeModelTraceSpec(YogaRef const &_ref);
  string uniqueKey;
  YogaRef ref;
  string label;
};

struct ScopeModelVisTrace {
  ScopeModelTraceSpec spec;
  shared_ptr<ScopeTraceRenderer> renderer;

  double visHeightTarget{1.0};
  double visHeight{0.25};
  double scale{1.0};
  bool autoScalePending{false};
  bool removed{false};

  void setRenderer(ScopeModel &m);
  void scaleUp();
  void scaleDn();
  void expand();
};

struct ScopeModelPanelSpec {
  string uniqueKey;
  string style;
  string label;
  vector<YogaRef> refs;
};

ostream &operator <<(ostream &s, ScopeModelPanelSpec const &a);


struct ScopeModelVisPanel {
  ScopeModelPanelSpec spec;
  shared_ptr<ScopePanelRenderer> renderer;
  function<void(R, YogaValue &, Trace *, apr_pool_t *)> debugUpdate;
  bool removed{false};
  bool popout{false};

  void setRenderer(ScopeModel &m);
};

struct ScopeModelAvail {
  ScopeModelTraceSpec traceSpec;
  vector<ScopeModelPanelSpec> panelSpecs;
  vector<ScopeModelAvail> children;

  void setPanelSpecs(YogaContext const &ctx);
};

struct YogaCodeLineInfo {
  R ascent{0.0};
  R descent{0.0};
};

struct YogaDebugLocalsEntry {
  YogaDebugLocalsEntry(YogaCompilation *reg, YogaSourceLoc _sourceLoc, YogaRef _valueRef);
  YogaSourceLoc sourceLoc;
  YogaRef valueRef;
  YogaPtrAccessor valueAcc;
};


struct YogaCodeTabModel {
  string fileName;
  YogaSourceFile *f{nullptr};

  vector<YogaCodeLineInfo> lines;

  vector<YogaDebugLocalsEntry> debugLocalsSorted;
  int paramValueEpoch{0};

  void navigateToFunction(string const &funcName);

};

struct ScopeModelAlt {
  Trace *trace{nullptr};
  YogaTimeseq *seq{nullptr};
  GradientSet *grads{nullptr};
  R beginTs{0.0}, endTs{0.0};
  U32 adjustColor(U32 base, R modulation = 1.0) const;
  U32 adjustColor(float r, float g, float b, float a, R modulation = 1.0) const;
  U32 adjustColor(glm::vec4 c, R modulation = 1.0) const;
  glm::vec4 adjustCol4(float r, float g, float b, float a, R modulation = 1.0) const;
  glm::vec4 adjustCol4(glm::vec4 c, R modulation) const;
  float alpha{1.0};
  bool isParam{false};
  bool isSecondary{false};
  bool isPredict{false};

  YogaTimeseq * getSeq(YogaRef const &ref) const;
  YogaValue getValue(YogaRef const &ref, R ts) const;
  YogaValue getValueAfter(YogaRef const &ref, R ts) const;
  vector<YogaValue> getArgs(vector<YogaRef> const &refs, R ts) const;
  bool getRawArgs(vector<void *> &out, vector<YogaRef> const &refs, R ts) const;

};


struct AltTraceInfo {
  shared_ptr<Trace> trace;
  shared_ptr<Trace> baseTrace;
  shared_ptr<GradientSet> grads;

  shared_ptr<PolicyGradientSpec> gradientSpec;
  vector<R> overrideParamValues;
  R durationLimit{0.0};
  int paramValueEpoch{0};
};

struct PredictJob : AltTraceInfo {
  AstAnnoCall *predictDef{nullptr};
  string error;
  R predictStartTs{0.0};
  R predictDur{1.0};
};


struct ScopeModel : enable_shared_from_this<ScopeModel> {
  ScopeModel(YogaContext const &_ctx);

  YogaContext ctx;

  bool loadFromDb(string const &name, std::function<void(string const &error)> cb);
  bool reloadFromTrace(string const &name);
  void ensureTraceLocal(std::function<void(bool)> const &cb);

  void setError(string const &_error);
  void setLoadStatus(string const &_status);
  void clearLoadStatus();

  string traceName;
  bool isLiveRobot{false};
  string loadStatus;
  string simEngine;
  
  double cursPos{0.5}, cursPosTarget{-1.0};

  shared_ptr<json> traceNotes;
  string modelErr;
  string traceFlags;

  double traceBeginTs{0.0}, traceEndTs{0.0};

  double visTime{0.0};
  double visDur{2.0}, visDurTarget{-1.0};
  bool scrollCursorWhenZoomedOut{false};
  bool scrollCursor{false};
  double scrubberPresence{1.0};

  bool visRealOverlay{true};
  bool visCfOverlay{true};
  bool debugLocalsEnable{true};

  vector<ScopeModelAvail> availTree;

  vector<shared_ptr<ScopeModelVisTrace>> visTraces;
  bool visTracesNeedsUpdate{false};
  map<string, bool> visTracesChecked;

  vector<shared_ptr<ScopeModelVisPanel>> visPanels;
  bool visPanelsNeedsUpdate{false};
  map<string, bool> visPanelsChecked;

  void setupDebugLocals(YogaCodeTabModel &tab);
  string visSourceFile;
  map<string, YogaCodeTabModel> codeTabModels;

  void setVisDeet(string const &name, json value);
  shared_ptr<json> visDeets;


  double autoScroll{0};

  double ticInterval{0.1}, majorTicInterval{1.0};
  double smallestDt{0.001};
  int timeDigits{3};

  double spinnerPhase{0.0};
  bool spinnerPhaseUsed{false};

  void setTrace(shared_ptr<Trace> _trace);
  shared_ptr<Trace> trace;
  bool traceLoadActive{false};
  bool traceSimActive{false};

  shared_ptr<json> traceManifest;

  shared_ptr<YogaClient> liveClient;
  shared_ptr<YogaLiveSim> liveSim;

  void setTraceManifest(json const &manifest);

  vector<ScopeModelAvail> getAvailTree(json const &channels);
  vector<ScopeModelAvail> getAvailTreeChildren(YogaRef const &ref);

  vector<ScopeModelAvail *> getAvailTreeLin();
  void getAvailTreeLin1(vector<ScopeModelAvail *> &accum, ScopeModelAvail &node);

  void animate();
  void animate(double dt);
  R lastAnimateTime{0.0};

  void updateVis();
  void updateVisTraces();
  void updateVisPanels();
  string getVisInfo();
  void setVisInfo(string const &visInfo);
  void saveVisInfo();
  void restoreVisInfo();

  void mergeVisPanels(size_t &visIndex, ScopeModelPanelSpec const &spec, bool checked);
  shared_ptr<ScopeModelVisTrace> mkVisTrace(ScopeModelTraceSpec const &spec, json const &defaults);
  shared_ptr<ScopeModelVisPanel> mkVisPanel(ScopeModelPanelSpec const &spec, json const &defaults);

  void closeTrace(string const &uniqueKey);
  void scroll(double deltaX, double deltaY);
  void zoom(double factor);

  void addTrace(string const &uniqueKey);
  void removeTrace(string const &uniqueKey);

  void addPanel(string const &uniqueKey);
  void removePanel(string const &uniqueKey);

  double getVisBeginTs();
  double getVisEndTs();
  double getScreenTs(double screenPos);

  vector<string> getSourceFiles();
  YogaCodeTabModel &getCodeTab(string const &fn);

  /*
    Alt traces
  */
  Trace *baselineTrace();
  vector<ScopeModelAlt> alts();
  vector<ScopeModelAlt> alts(YogaRef const &filterByRef);
  ScopeModelAlt debugAlt();
  ScopeModelAlt debugAlt(Trace *trace);
  shared_ptr<AltTraceInfo> mkAltTrace();

  shared_ptr<AltTraceInfo> paramTrace;

  void updateAltTraces();
  static void rerunTrace(AltTraceInfo *newTraceInfo);
  bool paramAltValueThreadActive{false};

  /*
    Calculating gradients towards a desired outcome
  */
  PolicyGradientSpec desiredGradientSpec;
  shared_ptr<AltTraceInfo> mkAltTraceForGrads();

  /*
    Policy Terrain
  */
  void updatePolicyTerrain();
  void calcPolicyTerrainMap(
    shared_ptr<PolicyTerrainMap> newMap, 
    std::function<void (shared_ptr<PolicyTerrainMap>)> onDone,
    std::function<void (R)> onProgress);

  shared_ptr<AltTraceInfo> mkAltTraceForPoint(PolicyTerrainMap *newMap, size_t xi, size_t yi);
  shared_ptr<PolicyTerrainMap> mkTerrainMapFromSpec(PolicyTerrainSpec const &spec);
  void setPolicyTerrainMap(shared_ptr<PolicyTerrainMap> newMap);
  R getTerrainDatum(PolicyRewardMetric const &rMetric);

  shared_ptr<PolicyTerrainMap> policyTerrainMap;
  PolicyTerrainSpec desiredPolicyTerrainSpec;
  int policyTerrainThreadActive{0};


  /*
    Squiggles (trend lines for every parameter)
  */
  void updatePolicySquiggles();
  void calcPolicySquiggleMap(
    shared_ptr<PolicySquiggleMap> newMap, 
    std::function<void (shared_ptr<PolicySquiggleMap>)> onDone,
    std::function<void (R)> onProgress);

  shared_ptr<AltTraceInfo> mkAltTraceForPoint(PolicySquiggleMap *newMap, size_t pi, size_t vi);
  shared_ptr<PolicySquiggleMap> mkSquiggleMapFromSpec(PolicySquiggleSpec const &spec);
  shared_ptr<PolicySquiggleMap> mkSquiggleMapFromSpecForParam(PolicySquiggleSpec const &spec, YogaParamInfo *param);

  PolicySquiggleMap *getSquiggleMapForParam(YogaParamInfo *desiredParam, int priority);

  shared_ptr<PolicySquiggleMap> policySquiggleMap;
  PolicySquiggleSpec desiredPolicySquiggleSpec;

  vector<shared_ptr<PolicySquiggleMap>> policySquiggleMapZooms;
  YogaParamInfo *desiredPolicySquiggleZoomParam{nullptr};
  int desiredPolicySquiggleZoomParamPriority{0};
  int policySquiggleThreadActive{0};

  /*
    Policy Search
  */

  void updatePolicySearch();
  shared_ptr<PolicySearchState> policySearchState;
  PolicyParamSearchSpec desiredPolicySearchSpec;

  /*
    Prediction traces
  */
  void updatePredictTraces();
  shared_ptr<PredictJob> mkPredictJob(AstAnnoCall *pd);
  static void rerunPredictJob(PredictJob *job);

  AstAnnoCall *desiredPredictDef{nullptr};
  shared_ptr<PredictJob> predictJob;
  int predictThreadActive{0};

  void updateTraining();
  shared_ptr<PredictTrainingSet> mkPredictTrainingSet(AstAnnoCall *trainDef);
  static void runPredictTrainingSet(PredictTrainingSet *pts);
  int trainThreadActive{0};
  AstAnnoCall *desiredTrainDef{nullptr};
  shared_ptr<PredictTrainingSet> trainJob;

  /*
    Global
  */

  static void stopAllLiveClients();
  static void clearScopeModelCache();
  static ScopeModel *getSavedScopeModel(YogaContext const &ctx, string const &traceName);
  static ScopeModel *getLiveRobotScopeModel(YogaContext const &ctx, string const &options);
  static ScopeModel *getSimScopeModel(YogaContext const &ctx, string const &options);
  static ScopeModel *getLiveSimScopeModel(YogaContext const &ctx, string const &options);

  static map<string, shared_ptr<ScopeModel>> scopeModelCache;

};

struct YogaLiveSim {
  R lastKbdStateTs{0.0};
  R lastPuckStateTs{0.0};
  R timeDilation{1.0};

  void updateUi(ScopeModel &m);
};
