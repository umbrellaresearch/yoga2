#pragma once
#include "./ysgui.h"
#include "common/std_headers.h"
#include "numerical/polyfit.h"
#include "../geom/panel3d.h"
#include "../geom/texcache.h"
#include "../jit/type.h"
#include "../jit/runtime.h"
#include "./scope_model.h"

struct ScopeModel;
struct ScopeGraphLayout;
struct ScopeModelVisPanel;
struct YogaTimeseq;
struct MainWindow;
struct YogaTypeStruct;
struct ScopeModelPanelSpec;
struct YogaVideoFrame;

struct ScopePanelRenderer {
  ScopePanelRenderer(ScopeModel &_m, ScopeModelVisPanel &_tr);
  virtual ~ScopePanelRenderer();

  ScopeModel &m;
  ScopeModelVisPanel &tr;
  ImRect curbb;
  
  virtual void setBoundingBox();
  virtual float preferredAspect();
  virtual void drawHeader();
  virtual void drawSubheader();
  virtual void drawContents();
  virtual void playAudio(MainWindow *win);
  virtual void drawPopupMenu();
  virtual void handleCloseRequest();
};



struct ScopePanelRenderer3D : ScopePanelRenderer {
  ScopePanelRenderer3D(ScopeModel &_m, ScopeModelVisPanel &_tr);

  vector<shared_ptr<YogaTimeseq>> baseSeqs;
  Panel3dDrawList drawList;

  void drawContents() override;
  virtual void runControls();

  virtual void renderCallback(const ImDrawList* parent_list, const ImDrawCmd* cmd);
  virtual void addContents();

  virtual void capturePopupClickPos();
  virtual void clearPopupClickPos();
};



struct ScopePanelRendererVideoFrameMovie : ScopePanelRenderer3D {
  ScopePanelRendererVideoFrameMovie(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq);
  ~ScopePanelRendererVideoFrameMovie();

  bool glInited{false};
  TexCache texCache;

  bool fetchActive{false};
  BlobRef fetchedBlobRef;
  shared_ptr<Blob> fetchedBlob;
  double fetchedBlobTime{0.0};

  float preferredAspect() override;
  void runControls() override;

  bool getImage(glm::dmat4 &orientation, int &bestIndex, YogaValue const &v);
  bool decodeImage(int texIndex, Blob &buffer, YogaVideoFrame *f);

  void addContents() override;
};


struct ScopePanelRendererAudioFrameAudio : ScopePanelRenderer {
  ScopePanelRendererAudioFrameAudio(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq);

  R playedUntil{0.0};
  const int verbose=0;

  void playAudio(MainWindow *win) override;
};


struct ScopePanelRendererR33Arrows : ScopePanelRenderer3D {
  ScopePanelRendererR33Arrows(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq);

  shared_ptr<YogaTimeseq> baseSeq;
  YogaValueAccessor<glm::mat3> seqTrValue;

  void addContents() override;
};


struct ScopePanelRendererR44Arrows : ScopePanelRenderer3D {
  ScopePanelRendererR44Arrows(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq);

  shared_ptr<YogaTimeseq> baseSeq;
  YogaValueAccessor<glm::mat4> seqTrValue;

  void addContents() override;
};


struct ScopePanelRenderer3dYoga : ScopePanelRenderer3D {
  ScopePanelRenderer3dYoga(ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller _caller);

  YogaCaller caller;
  YogaType *renderSolidType{nullptr};
  map<string, vector<shared_ptr<StlSolid>>> solids;

  void addContents() override;
};



struct ScopePanelRenderer2dYoga : ScopePanelRenderer {
  ScopePanelRenderer2dYoga(ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller _caller);

  YogaCaller caller;

  YogaType *renderLineType{nullptr};
  YogaType *renderCircleType{nullptr};
  YogaType *renderCircleFilledType{nullptr};
  YogaType *renderArcArrowType{nullptr};

  void drawContents() override;
};


struct ScopePanelRendererCoords : ScopePanelRenderer3D {

  ScopePanelRendererCoords(ScopeModel &_m, ScopeModelVisPanel &_tr);

  struct RenderAxis {
    YogaRef ref;
    CallableYogaAccessor acc;
    glm::vec3 dir1, dir2;
    R len1, len2;
    R invScale;
  };

  vector<RenderAxis> axes;

  void addContents() override;
};


struct ScopePanelRendererPolicySearch : ScopePanelRenderer3D {

  ScopePanelRendererPolicySearch(ScopeModel &_m, ScopeModelVisPanel &_tr);

  struct RenderAxis {
    glm::vec3 dir;
    glm::vec4 color;
  };

  vector<RenderAxis> axes;

  void drawSubheader() override;
  void addContents() override;
};


struct ScopePanelRendererPolicyTerrain : ScopePanelRenderer3D {
  ScopePanelRendererPolicyTerrain(ScopeModel &_m, ScopeModelVisPanel &_tr);

  vec3 mouseHit; // in world coordinates
  bool mouseHitActive{false};

  vec3 clickHit; // in world coordinates
  bool clickHitActive{false};

  void drawSubheader() override;
  void runControls() override;
  void addContents() override;
  void capturePopupClickPos() override;
  void clearPopupClickPos() override;
  void drawPopupMenu() override;
  void handleCloseRequest() override;

};




struct ScopePanelRendererPhaseplot : ScopePanelRenderer {
  ScopePanelRendererPhaseplot(
    ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller const &_caller);

  YogaCaller caller;

  void drawContents() override;
};




struct ScopePanelRendererRegister {
  ScopePanelRendererRegister(string const &_clsName, string const &_panelStyle,
    std::function<void(ScopeModel &m, ScopeModelVisPanel &tr)> _f);
  
  string clsName;
  string panelStyle;
  std::function<void(ScopeModel &m, ScopeModelVisPanel &tr)> f;

};
