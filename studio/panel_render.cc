#include "./panel_render.h"
#include "./ysgui.h"
#include "./yogastudio_ui.h"
#include "../jit/runtime.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../geom/solid_geometry.h"
#include "../drivers/video/video_types.h"
#include "../drivers/ui/ui_types.h"
#include "./main_window.h"
#include "../remote/client.h"

ScopePanelRenderer::ScopePanelRenderer(ScopeModel &_m, ScopeModelVisPanel &_tr)
  : m(_m), tr(_tr)
{
}

ScopePanelRenderer::~ScopePanelRenderer()
{
}


void ScopePanelRenderer::drawHeader()
{
  if (SmallButton(("X##"s + tr.spec.uniqueKey).c_str())) {
    handleCloseRequest();
  }
  SameLine();
  PushStyleColor(ImGuiCol_Text, yss->panelHeaderCol);
  TextUnformatted(tr.spec.label.c_str());
  PopStyleColor();
  SameLine();
  {
    auto crm = GetContentRegionMax();
    SetCursorPosX(crm.x - 2*GetFontSize());
    if (SmallButton(("^##" + tr.spec.uniqueKey).c_str())) {
      tr.popout = true;
    }
  }
}

void ScopePanelRenderer::drawSubheader()
{
}

void ScopePanelRenderer::drawContents()
{
}

float ScopePanelRenderer::preferredAspect()
{
  return 1.0f;
}

void ScopePanelRenderer::playAudio(MainWindow *win)
{
}

void ScopePanelRenderer::setBoundingBox()
{
  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;
  const ImGuiID id = window->GetID((void *)this);
  auto const pos = window->DC.CursorPos;
  auto const wrm = GetContentRegionMaxAbs();
  float preferredWidth = wrm.x - pos.x;
  float preferredHeight = preferredAspect() * preferredWidth;
  const ImVec2 size = CalcItemSize(ImVec2(0, 0), preferredWidth, preferredHeight);
  curbb = ImRect(pos, pos + size);
  ItemSize(size, g.Style.FramePadding.y);
  if (!ItemAdd(curbb, id)) return;
  if (0) cerr << tr.spec.label + ": size=" + repr(size) + " curbb=" + repr(curbb) + "\n";
}

void ScopePanelRenderer::drawPopupMenu()
{
}

void ScopePanelRenderer::handleCloseRequest()
{
  m.removePanel(tr.spec.uniqueKey);
}


ScopePanelRenderer3D::ScopePanelRenderer3D(ScopeModel &_m, ScopeModelVisPanel &_tr)
  : ScopePanelRenderer(_m, _tr)
{
}

void ScopePanelRenderer3D::drawContents()
{
  setBoundingBox();

  if (0) RenderFrame(curbb.Min, curbb.Max, IM_COL32(0xcc, 0xcc, 0xcc, 0xff), true, 0.0f);
  runControls();

  drawList.vpL = curbb.Min.x;
  drawList.vpT = curbb.Min.y;
  drawList.vpR = curbb.Max.x;
  drawList.vpB = curbb.Max.y;

  auto &io = GetIO();
  bool showHover = IsItemHovered() &&
      io.MousePos.y >= curbb.Min.y &&
      io.MousePos.y <= curbb.Max.y &&
      io.MousePos.x >= curbb.Min.x &&
      io.MousePos.x <= curbb.Max.x;


  ImGuiID id = GetID("popup");
  if (showHover && !IsPopupOpen(id, 0)) {
    drawList.setPickRay();
    if (IsMouseClicked(0)) {
      capturePopupClickPos();
      OpenPopupEx(id);
    }
  }
  else if (!IsPopupOpen(id, 0)) {
    clearPopupClickPos();
  }


  GetWindowDrawList()->AddCallback([](const ImDrawList* parent_list, const ImDrawCmd* cmd) {
    auto this1 = reinterpret_cast<ScopePanelRenderer3D *>(cmd->UserCallbackData);
    this1->renderCallback(parent_list, cmd);
  }, reinterpret_cast<void *>(this));

  GetWindowDrawList()->AddCallback(ImDrawCallback_ResetRenderState, nullptr);

  if (BeginPopupEx(id, ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoSavedSettings)) {
    drawPopupMenu();
    EndPopup();
  }
}

void ScopePanelRenderer3D::renderCallback(const ImDrawList* parent_list, const ImDrawCmd* cmd)
{
  drawList.clear();
  addContents();
  drawList.glRender();
}

void ScopePanelRenderer3D::runControls()
{
  if (IsItemHovered()) {
    drawList.firstPersonControls();
  }
}

void ScopePanelRenderer3D::addContents()
{
}

void ScopePanelRenderer3D::capturePopupClickPos()
{
}

void ScopePanelRenderer3D::clearPopupClickPos()
{
}



ScopePanelRendererVideoFrameMovie::ScopePanelRendererVideoFrameMovie(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq)
  : ScopePanelRenderer3D(_m, _tr)
{
  assert(tr.spec.refs.size() == 1);
  if (0) cerr << "Create " + tr.spec.label + "\n";
}

ScopePanelRendererVideoFrameMovie::~ScopePanelRendererVideoFrameMovie()
{
  if (0) cerr << "Dispose " + tr.spec.label + "\n";
}

float ScopePanelRendererVideoFrameMovie::preferredAspect()
{
  return 0.75f;
}

void ScopePanelRendererVideoFrameMovie::runControls()
{
  drawList.camIso();
}

bool ScopePanelRendererVideoFrameMovie::decodeImage(int texIndex, Blob &buffer, YogaVideoFrame *f)
{
  if (f->format == YVFF_JPEG) return texCache.setJpeg(texIndex, buffer);
  if (f->format == YVFF_RGB) return texCache.setRgb(texIndex, f->width, f->height, buffer);
  return false;
}

bool ScopePanelRendererVideoFrameMovie::getImage(glm::dmat4 &orientation, int &bestIndex, YogaValue const &v)
{
  int verbose = 0;

  bestIndex = 0;
  auto vf = reinterpret_cast<YogaVideoFrame *>(v.buf);
  if (!vf->blob.isValid()) {
    return false;
  }
  orientation = vf->orientation;

  if (!texCache.getIndexFor(v.buf, bestIndex)) {
    Blob buffer;

    if (verbose) cerr << "  load from chunkId="s + repr_016x(vf->blob.chunkId) + 
        " partOfs=" + repr(vf->blob.partOfs) + 
        " partSize=" + repr(vf->blob.partSize) + "\n";

    if (m.liveClient) {
      if (!fetchActive) {
        auto loc = m.trace->blobLocationsByChunkId[vf->blob.chunkId];
        if (verbose) cerr << "  fetch " + repr(vf->blob) + " from " + loc.first + "\n";
        if (!loc.first.empty()) {
          fetchActive = true;
          m.liveClient->fetchBlob(loc.first, vf->blob, [this, vf](string const &err, shared_ptr<Blob> blob) {
            fetchActive = false;
            if (!err.empty()) {
              cerr << "Trying to fetch " + repr(vf->blob) + ": " + err + "\n";
              return;
            }
            fetchedBlobRef = vf->blob;
            fetchedBlob = blob;
            fetchedBlobTime = realtime();              
          });
        }
      }
    }

    if (fetchedBlob && realtime() - fetchedBlobTime < 1.0) {
      if (decodeImage(bestIndex, *fetchedBlob, vf)) {
        if (verbose) cerr << "  added to cache at index "s + repr(bestIndex) + "\n";
        texCache.setIndexFor(v.buf, bestIndex);
        return true;
      }
      else {
        if (verbose) cerr << "  blob not a valid image (format=" + repr(vf->format) + "\n";
        return false;
      }
    }

    if (!loadBlob(vf->blob, buffer)) {
      if (verbose) cerr << "  blob didn't load\n";
      return false;
    }

    if (decodeImage(bestIndex, buffer, vf)) {
      if (verbose) cerr << "  added to cache at index "s + repr(bestIndex) + "\n";
      texCache.setIndexFor(v.buf, bestIndex);
    }
    else {
      if (verbose) cerr << "  blob not a valid image (format=" + repr(vf->format) + "\n";
      return false;
    }
  } 
  else {
    if (verbose) cerr << "  found in cache at index "s + repr(bestIndex) + "\n";
  }

  return true;
}

void ScopePanelRendererVideoFrameMovie::addContents()
{
  if (!glInited) {
    glInited = true;
    texCache.alloc(4);
  }

  for (auto &alt : m.alts()) {
    if (alt.isSecondary) continue; // FIXME: but isSecondary is wrong for video traces.

    auto v = alt.getValue(tr.spec.refs[0], m.visTime);
    if (!v.isValid() || !v.nonZeroTs()) continue;

    glm::dmat4 orientation;
    int texi = 0;
    if (getImage(orientation, texi, v)) {
      auto size = texCache.sizes[texi];

      glm::mat4 model(
        -1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, size.y / size.x, 0,
        0, 0, 0, 1);

      drawList.addVideo(glm::mat4(orientation) * model, texCache.texIds[texi]);
    }
  }
}




ScopePanelRendererAudioFrameAudio::ScopePanelRendererAudioFrameAudio(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq)
  : ScopePanelRenderer(_m, _tr)
{
  if (0) cerr << "Create " + tr.spec.label + "\n";
}

void ScopePanelRendererAudioFrameAudio::playAudio(MainWindow *win)
{
  if (m.autoScroll == 1.0 && win->audioDevice) {

    R targetTime = m.visTime + 0.02;

    if (targetTime + 0.1 < playedUntil) {
      if (verbose) cerr << "Rewind from " + repr_0_3f(playedUntil) + " to " + repr_0_3f(targetTime) + "\n";
      playedUntil = targetTime;
      SDL_ClearQueuedAudio(win->audioDevice);
    }
    else if (targetTime - 0.1 > playedUntil) {
      if (verbose) cerr << "Skip from " + repr_0_3f(playedUntil) + " to " + repr_0_3f(targetTime - 0.1) + "\n";
      playedUntil = targetTime - 0.1;
      SDL_ClearQueuedAudio(win->audioDevice);
    }

    while (targetTime > playedUntil) {
      for (auto &alt : m.alts()) {
        if (alt.isSecondary) continue;

        auto yv = alt.getValueAfter(tr.spec.refs[0], playedUntil);
        if (!yv.isValid()) return;

        auto af = reinterpret_cast<YogaAudioFrame *>(yv.buf);
        if (!af->blob.isValid()) return;

        if (verbose >= 2) cerr << "Play "s + tr.spec.label + " " + repr_0_3f(playedUntil) + " - " + repr_0_3f(yv.ts) + " target=" + repr_0_3f(targetTime) + "\n";

        playedUntil = yv.ts;

        Blob buffer;
        if (!loadBlob(af->blob, buffer)) return;

        if (verbose >= 2) cerr << "  queue "s + repr(buffer.size()) + " bytes " + repr_0_3f(buffer.size() / sizeof(short) / (R)win->audioSpec.channels / (R)win->audioSpec.freq) + " S\n";

        if (SDL_QueueAudio(win->audioDevice, buffer.data(), buffer.size()) < 0) {
          cerr << "Queue audio error: "s + SDL_GetError() + "\n";
        }
      }
    }
  }
}


static vec4 modulateColor(vec4 base, R modulation)
{
  if (modulation == 1.0) return base;
  vec4 ret = base;
  ret.w = clamp(base.w * modulation, 0.0, 1.0);
  return ret;
}


ScopePanelRendererR33Arrows::ScopePanelRendererR33Arrows(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq)
  : ScopePanelRenderer3D(_m, _tr),
    baseSeq(_seq),
    seqTrValue(baseSeq->type, tr.spec.refs[0].subName)
{
}

void ScopePanelRendererR33Arrows::addContents()
{
  auto v = baseSeq->getEqualBefore(m.visTime);
  if (!v.isValid()) {
    cerr << tr.spec.label + ": Invalid value at "s + to_string(m.visTime) + "\n";
    return;
  }
  
  auto tm = seqTrValue.rd(v);

  if (0) cerr << "Got mat "s + to_string(tm) + "\n";

  drawList.addArrow(vec3(0, 0, 0), tm * vec3(1, 0, 0), tm * vec3(0, 1, 0), tm * vec3(0, 0, 1), vec4(1, 0, 0, 1));
  drawList.addArrow(vec3(0, 0, 0), tm * vec3(0, 1, 0), tm * vec3(1, 0, 0), tm * vec3(0, 0, 1), vec4(0, 1, 0, 1));
  drawList.addArrow(vec3(0, 0, 0), tm * vec3(0, 0, 1), tm * vec3(1, 0, 0), tm * vec3(0, 1, 0), vec4(0, 0, 1, 1));
}



ScopePanelRendererR44Arrows::ScopePanelRendererR44Arrows(ScopeModel &_m, ScopeModelVisPanel &_tr, shared_ptr<YogaTimeseq> _seq)
  : ScopePanelRenderer3D(_m, _tr),
    baseSeq(_seq),
    seqTrValue(baseSeq->type, tr.spec.refs[0].subName)
{
}

void ScopePanelRendererR44Arrows::addContents()
{

  auto v = baseSeq->getEqualBefore(m.visTime);
  if (!v.isValid()) {
    cerr << tr.spec.uniqueKey + ": Invalid value at "s + to_string(m.visTime) + "\n";
    return;
  }
  
  auto tm = seqTrValue.rd(v);
  if (0) cerr << "Got mat "s + to_string(tm) + "\n";

  drawList.addArrow(
    tm * vec4(0, 0, 0, 1),
    tm * vec4(1, 0, 0, 0), 
    tm * vec4(0, 1, 0, 0), 
    tm * vec4(0, 0, 1, 0), 
    vec4(1, 0, 0, 1));
  drawList.addArrow(
    tm * vec4(0, 0, 0, 1),
    tm * vec4(0, 1, 0, 0),
    tm * vec4(1, 0, 0, 0),
    tm * vec4(0, 0, 1, 0),
    vec4(0, 1, 0, 1));
  drawList.addArrow(
    tm * vec4(0, 0, 0, 1),
    tm * vec4(0, 0, 1, 0),
    tm * vec4(1, 0, 0, 0),
    tm * vec4(0, 1, 0, 0),
    vec4(0, 0, 1, 1));
}

static vector<YogaSetCons *> renderSetSorted(YogaSetCons *s)
{
  vector<YogaSetCons *> ret;
  for (auto it = s; it; it = it->next) {
    if (it->value && it->modulation > 0.0) {
      ret.push_back(it);
    }
  }
  sort(ret.begin(), ret.end(), [](YogaSetCons * const &a, YogaSetCons * const &b) {
    return *reinterpret_cast<R *>(a->value) < *reinterpret_cast<R *>(b->value);
  });
  return ret;
}

ScopePanelRenderer3dYoga::ScopePanelRenderer3dYoga(ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller _caller)
  : ScopePanelRenderer3D(_m, _tr),
    caller(_caller)
{
  renderSolidType = m.ctx.getTypeOrThrow("RenderSolid"); // Defined in drivers/ui/ui_types.cc
}

void ScopePanelRenderer3dYoga::addContents()
{
  const int verbose = 0;

  YogaPool tmpmem;

  for (auto &alt : m.alts()) {
    if (alt.isSecondary) continue;
    vector<void *> inArgs;
    if (!alt.getRawArgs(inArgs, tr.spec.refs, m.visTime)) continue;

    YogaPool tmpmem;
    
    YogaSetCons *renderSet{nullptr};
    R astonishment = 0.0;
    caller({&renderSet}, inArgs, astonishment, 0.0, tmpmem.it);
    
    for (auto o : renderSetSorted(renderSet)) {
      YogaValue yv(m.ctx.reg, o);

      if (yv.type == renderSolidType) {
        auto cv = reinterpret_cast<YogaRenderSolid const *>(yv.buf);
        if (verbose) cerr << "renderSolid " + shellEscape(cv->name) + " at " + to_string(cv->pos) + " color " + to_string(cv->color) + "\n";

        if (!strcmp(cv->name, "z1")) {
          auto p1 = cv->pos * glm::dvec4(0, 0, 0, 1);
          auto p2 = cv->pos * glm::dvec4(0, 0, 1, 1);
          drawList.addLine(p1, modulateColor(cv->color, o->modulation), p2, modulateColor(cv->color, o->modulation));
        }
        else if (!strcmp(cv->name, "zarrow")) {
          drawList.addArrow(
            cv->pos * glm::dvec4(0, 0, 0, 1),
            cv->pos * glm::dvec4(0, 0, 1, 0),
            cv->pos * glm::dvec4(1, 0, 0, 0),
            cv->pos * glm::dvec4(0, 1, 0, 0),
            modulateColor(cv->color, o->modulation));
        }
        else {
          auto [fullfn, isBuiltin] = YogaLayout::instance().requirePath(cv->name);

          auto &all = solids[fullfn];
          if (all.empty()) {
            all.push_back(StlSolid::getNamedSolid(fullfn, 1.0, glm::dmat4(1)));
          }
          for (auto &s : all) {
            drawList.addSolid(*s, cv->pos, modulateColor(cv->color, o->modulation));
          }
        }
      }
      else {
        cerr << "ScopePanelRenderer3dYoga: unknown set element " + repr(yv) + "\n";
      }
    }
  }
}



ScopePanelRenderer2dYoga::ScopePanelRenderer2dYoga(ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller _caller)
  : ScopePanelRenderer(_m, _tr),
    caller(_caller)
{
  renderLineType = m.ctx.getTypeOrThrow("RenderLine"); // Defined in drivers/ui/ui_types.cc
  renderCircleType = m.ctx.getTypeOrThrow("RenderCircle");
  renderCircleFilledType = m.ctx.getTypeOrThrow("RenderCircleFilled");
  renderArcArrowType = m.ctx.getTypeOrThrow("RenderArcArrow");
}

void ScopePanelRenderer2dYoga::drawContents()
{
  setBoundingBox();

  /*
    Map the Yoga coordinate space x,y ∈ [-1,+1] to fill our bounding box,
    while preserving aspect ratio
  */
  auto scale = 0.5 * min(curbb.Max.x - curbb.Min.x, curbb.Max.y - curbb.Min.y);
  Polyfit1 convX(
    0.5*(curbb.Min.x + curbb.Max.x),
    scale);
  Polyfit1 convY(
    0.5*(curbb.Min.y + curbb.Max.y),
    -scale);

  if (0) {
    GetWindowDrawList()->AddLine(
      ImVec2(convX(-1), convY(0)),
      ImVec2(convX(+1), convY(0)),
      IM_COL32(0xcc, 0xcc, 0xcc, 0xff),
      1.0f);
    GetWindowDrawList()->AddLine(
      ImVec2(convX(0), convY(-1)),
      ImVec2(convX(0), convY(+1)),
      IM_COL32(0xcc, 0xcc, 0xcc, 0xff),
      1.0f);
  }

  for (auto &alt : m.alts()) {
    vector<void *> inArgs;
    if (!alt.getRawArgs(inArgs, tr.spec.refs, m.visTime)) continue;

    YogaPool tmpmem;
    
    YogaSetCons *renderSet{nullptr};
    R astonishment = 0.0;
    caller({&renderSet}, inArgs, astonishment, 0.0, tmpmem.it);

    for (auto o : renderSetSorted(renderSet)) {
      YogaValue yv(m.ctx.reg, o);

      if (yv.type == renderLineType) {
        auto cv = reinterpret_cast<YogaRenderLine const *>(yv.buf);
        GetWindowDrawList()->AddLine(
          ImVec2(convX(cv->p1[0]), convY(cv->p1[1])),
          ImVec2(convX(cv->p2[0]), convY(cv->p2[1])),
          alt.adjustColor(cv->color, o->modulation),
          max(1.5, cv->thickness * scale));
      }

      else if (yv.type == renderCircleType) {
        auto cv = reinterpret_cast<YogaRenderCircle const *>(yv.buf);
        GetWindowDrawList()->AddCircle(
          ImVec2(convX(cv->pos[0]), convY(cv->pos[1])),
          max(1.0, scale *cv->radius),
          alt.adjustColor(cv->color, o->modulation),
          0,
          max(1.5, cv->thickness * scale));
      }

      else if (yv.type == renderCircleFilledType) {
        auto cv = reinterpret_cast<YogaRenderCircleFilled const *>(yv.buf);
        GetWindowDrawList()->AddCircleFilled(
          ImVec2(convX(cv->pos[0]), convY(cv->pos[1])),
          max(1.0, scale *cv->radius),
          alt.adjustColor(cv->color, o->modulation),
          0);
      }


      else if (yv.type == renderArcArrowType) {
        auto cv = reinterpret_cast<YogaRenderArcArrow const *>(yv.buf);

        auto col = alt.adjustColor(cv->color, o->modulation);

        auto segments = int(max(8.0, abs(cv->arcTo - cv->arcFrom) * 64.0));
        auto segAngle = (cv->arcTo - cv->arcFrom) / segments;
        for (int i=0; i<=segments; i++) {
          auto a0 = cv->arcFrom + i*segAngle;
          GetWindowDrawList()->PathLineTo(ImVec2(convX(cv->pos[0] + cos(a0) * cv->radius), convY(cv->pos[1] + sin(a0) * cv->radius)));
        }
        
        GetWindowDrawList()->PathStroke(col, false, max(1.5, cv->thickness*scale));

        auto arrowLenAngle = min(0.15, abs(cv->arcFrom - cv->arcTo));
        auto arrowLenRadial = min(0.05, min(abs(cv->arcFrom - cv->arcTo), cv->radius*0.1));

        auto arrowTailAngle = cv->arcTo + copysign(arrowLenAngle, cv->arcFrom - cv->arcTo);
        auto arrowTailRadius1 = cv->radius + arrowLenRadial;
        auto arrowTailRadius2 = cv->radius - arrowLenRadial;

        GetWindowDrawList()->AddLine(
          ImVec2(convX(cv->pos[0] + cos(cv->arcTo) * cv->radius), convY(cv->pos[1] + sin(cv->arcTo) * cv->radius)),
          ImVec2(convX(cv->pos[0] + cos(arrowTailAngle) * arrowTailRadius1), convY(cv->pos[1] + sin(arrowTailAngle) * arrowTailRadius1)),
          col, max(1.5, cv->thickness*scale));

        GetWindowDrawList()->AddLine(
          ImVec2(convX(cv->pos[0] + cos(cv->arcTo) * cv->radius), convY(cv->pos[1] + sin(cv->arcTo) * cv->radius)),
          ImVec2(convX(cv->pos[0] + cos(arrowTailAngle) * arrowTailRadius2), convY(cv->pos[1] + sin(arrowTailAngle) * arrowTailRadius2)),
          col, max(1.5, cv->thickness*scale));

        auto buttLenRadial = min(0.02, min(abs(cv->arcFrom - cv->arcTo), cv->radius*0.1));
        auto buttRadius1 = cv->radius + buttLenRadial;
        auto buttRadius2 = cv->radius - buttLenRadial;
        GetWindowDrawList()->AddLine(
          ImVec2(convX(cv->pos[0] + cos(cv->arcFrom) * buttRadius1), convY(cv->pos[1] + sin(cv->arcFrom) * buttRadius1)),
          ImVec2(convX(cv->pos[0] + cos(cv->arcFrom) * buttRadius2), convY(cv->pos[1] + sin(cv->arcFrom) * buttRadius2)),
          col, max(1.5, cv->thickness*scale));
      }
    }
  }
}


ScopePanelRendererCoords::ScopePanelRendererCoords(ScopeModel &_m, ScopeModelVisPanel &_tr)
  : ScopePanelRenderer3D(_m, _tr)
{
  for (auto &ref : tr.spec.refs) {
    for (auto &[accName, accessor] : ref.itemType->accessorsByName) {
      if (endsWith(accName, "::rd.d")) {
        std::mt19937 gen(std::hash<string>{}(accName));
        std::uniform_real_distribution<float> dis(0.1, 1.0);
        glm::vec3 dir = glm::normalize(glm::vec3(dis(gen), dis(gen), dis(gen)));
        R len = 0.25;
        R invScale = 1.0;
        axes.push_back(RenderAxis{ref, accessor, dir, dir, len, len, invScale});
      }
    }
  }
}

void ScopePanelRendererCoords::addContents()
{
  for (auto &alt : m.alts()) {

    R interval = min(m.visDur / 20.0, 0.05);
    R increment = m.visDur / 100.0;

    for (R ts = m.getVisBeginTs(); ts + interval < m.getVisEndTs(); ts += increment) {


      glm::vec3 totdir(0, 0, 0);
      for (auto &axis : axes) {

        auto seq = alt.getSeq(axis.ref);
        auto vs1 = seq->getEqualBefore(ts);
        if (!vs1.isValid()) {
          cerr << axis.ref.fullName + ":Invalid a value at "s + repr_0_3f(ts) + "\n";
          return;
        }
        auto vs2 = seq->getEqualBefore(ts + interval);
        if (!vs2.isValid()) {
          cerr << axis.ref.fullName + ":Invalid b value at "s + repr_0_3f(ts + interval) + "\n";
          return;
        }

        R cv1 = 0.0, cv2=0.0;
        axis.acc(vs1.buf, (U8 *)&cv1);
        axis.acc(vs2.buf, (U8 *)&cv2);

        totdir += float(axis.len1 * cbrt(cv1 * axis.invScale)) * axis.dir1;
        totdir += float(axis.len2 * cbrt(cv2 * axis.invScale)) * axis.dir2;
      }

      drawList.addDot(totdir, vec4(1, 0, 0, 1));

    }
  }
}


ScopePanelRendererPhaseplot::ScopePanelRendererPhaseplot(ScopeModel &_m, ScopeModelVisPanel &_tr, YogaCaller const &_caller)
  : ScopePanelRenderer(_m, _tr),
    caller(_caller)
{
}

void ScopePanelRendererPhaseplot::drawContents()
{
  setBoundingBox();

  auto scale = 0.5 * min(curbb.Max.x - curbb.Min.x, curbb.Max.y - curbb.Min.y);
  Polyfit1 convX(
    0.5*(curbb.Min.x + curbb.Max.x),
    scale);
  Polyfit1 convY(
    0.5*(curbb.Min.y + curbb.Max.y),
    -scale);

  GetWindowDrawList()->AddLine(
    ImVec2(convX(-1.05), convY(0.0)),
    ImVec2(convX(+1.05), convY(0.0)),
    IM_COL32(0xcc, 0xcc, 0xcc, 0xff));

  GetWindowDrawList()->AddLine(
    ImVec2(convX(+1.00), convY(0.0)-3),
    ImVec2(convX(+1.00), convY(0.0)+3),
    IM_COL32(0xaa, 0xaa, 0xaa, 0xff));

  GetWindowDrawList()->AddLine(
    ImVec2(convX(0.0), convY(-1.05)),
    ImVec2(convX(0.0), convY(+1.05)),
    IM_COL32(0xcc, 0xcc, 0xcc, 0xff));

  GetWindowDrawList()->AddLine(
    ImVec2(convX(0.0) - 3, convY(+1.0)),
    ImVec2(convX(0.0) + 3, convY(+1.0)),
    IM_COL32(0xaa, 0xaa, 0xaa, 0xff));

  size_t nArgs = tr.spec.refs.size();

  YogaPool tmpmem;

  for (auto &alt : m.alts()) {

    vector<vector<YogaValue>> valuess;
    bool missingValues = false;
    for (auto &ref : tr.spec.refs) {
      auto seq = alt.trace->getTimeseq(ref.seqName);
      if (!seq) {
        missingValues = true;
        break;
      }
      valuess.push_back(seq->getValuesInRange(m.getVisBeginTs(), m.getVisEndTs()));
    }
    if (missingValues) continue;

    vector<vector<YogaValue>::iterator> iterators;
    for (auto &it : valuess) {
      iterators.push_back(it.begin());
    }

    PolylineBuf pts;

    vector<void *> inArgs(nArgs);
    bool foundVisTime = false;
    ImVec2 visTimePt;

    while (1) {

      R ts = 1e99;
      for (size_t argi = 0; argi < nArgs; argi ++) {
        if (iterators[argi] == valuess[argi].end()) goto done;

        auto &yv = *iterators[argi];
        if (argi == 0 || yv.ts < ts) ts = yv.ts;
        inArgs[argi] = yv.buf;
      }

      dvec2 pt;
      R astonishment = 0.0;
      caller({&pt}, inArgs, astonishment, 0.0, tmpmem.it);

      pts(convX(pt.x), convY(pt.y));
      if (ts >= m.visTime && !foundVisTime) {
        visTimePt = ImVec2(convX(pt.x), convY(pt.y));
        foundVisTime = true;
      }

      for (size_t argi = 0; argi < nArgs; argi ++) {
        auto &yv = *iterators[argi];
        if (yv.ts == ts) iterators[argi]++;
      }
      
    }
  done: ;

    pts.draw(IM_COL32(0xff, 0x00, 0x00, 0xff), false, 1.0);
    GetWindowDrawList()->AddCircleFilled(visTimePt, 5.0f, IM_COL32(0xcc, 0x00, 0x00, 0xaa));
  }
}




// ------------------------


static map<string, map<string, ScopePanelRendererRegister *>> *renderersByType;
static map<string, ScopePanelRendererRegister *> *renderersByStyle;

ScopePanelRendererRegister::ScopePanelRendererRegister(
  string const &_clsName,
  string const &_panelStyle,
  std::function<void(ScopeModel &m, ScopeModelVisPanel &tr)> _f)
    : clsName(_clsName),
      panelStyle(_panelStyle),
      f(_f)
{
  if (!renderersByType) renderersByType = new map<string, map<string, ScopePanelRendererRegister *>>();
  (*renderersByType)[clsName][panelStyle] = this;
  if (!renderersByStyle) renderersByStyle = new map<string, ScopePanelRendererRegister *>();
  (*renderersByStyle)[panelStyle] = this;
}


void ScopeModelAvail::setPanelSpecs(YogaContext const &ctx)
{
  panelSpecs.clear();

  if (traceSpec.ref.typeName == "VideoFrame") {
    panelSpecs.push_back(ScopeModelPanelSpec{
      "movie(" + traceSpec.ref.fullName + ")",
      "movie",
      "movie(" + traceSpec.ref.fullName + ")",
      {traceSpec.ref}
    });
  }
  else if (traceSpec.ref.typeName == "AudioFrame") {
    panelSpecs.push_back(ScopeModelPanelSpec{
      "audio(" + traceSpec.ref.fullName + ")",
      "audio",
      "audio(" + traceSpec.ref.fullName + ")",
      {traceSpec.ref}
    });
  }
  else if (traceSpec.ref.typeName == "R[4,4]" || traceSpec.ref.typeName == "R[3,3]") {
    panelSpecs.push_back(ScopeModelPanelSpec{
      "renderArrows(" + traceSpec.ref.fullName + ")",
      "renderArrows",
      "renderArrows(" + traceSpec.ref.fullName + ")",
      {traceSpec.ref}
    });
  }
  if (0 && traceSpec.ref.itemType && traceSpec.ref.itemType->asStruct()) {
    panelSpecs.push_back(ScopeModelPanelSpec{
      "coords(" + traceSpec.ref.fullName + ")",
      "coords",
      "coords(" + traceSpec.ref.fullName + ")",
      {traceSpec.ref}
    });
  }
  if (traceSpec.ref.typeName != "R") {
    for (auto pd : ctx.reg->panelDefs) {
      if (pd->call->args.size() >= 2) {
        string a0;
        if (pd->call->args[0]->getLiteral(a0)) {
          YogaType *outType = nullptr;
          if (a0 == "ui.render2d" || a0 == "ui.render3d") {
            outType = ctx.reg->setType;
          }
          else if (a0 == "ui.phase") {
            outType = ctx.getTypeOrThrow("R[2]");
          }
          else {
            continue;
          }
        
          for (size_t ai = 1; ai < min((size_t)2, pd->call->args.size()); ai++) {
            string a1;
            if (pd->call->args[ai]->getLiteral(a1) && (a1 == "." || a1 == traceSpec.ref.seqName)) {
              if (auto fInfo = ctx.reg->getCompiledFunc(pd->call->literalFuncName)) {

                if (fInfo->outArgs.size() == 1 && fInfo->outArgs[0].first == outType) {
                  if (fInfo->inArgs.size() >= 1 && fInfo->inArgs[ai - fInfo->outArgs.size()].first->clsName == traceSpec.ref.typeName) {
            
                    vector<string> styleArgs;
                    vector<YogaRef> funcArgRefs;
                      for (size_t aj = 1; aj < pd->call->args.size(); aj++) {
                      string argName;
                      if (aj == ai) {
                        argName = traceSpec.ref.seqName;
                      }
                      else {
                        if (!pd->call->args[aj]->getLiteral(argName)) goto failPanel;
                      }
                      funcArgRefs.push_back(ctx.getSeqRef(argName));
                      styleArgs.push_back(argName);
                    }
                    panelSpecs.push_back(ScopeModelPanelSpec{
                      pd->call->literalFuncName + "(" + joinString(styleArgs, ", ") + ")." + a0,
                      pd->call->literalFuncName + "." + a0,
                      pd->call->literalFuncName + "(" + joinString(styleArgs, ", ") + ")." + a0,
                      funcArgRefs
                    });
                  failPanel: ;
                  }
                }
                else {
                  ctx(pd->call).logError("Expected to return a " + repr_type(outType));
                }
              }
              else {
                ctx(pd->call).logError("No such function");
              }
            }
          }
        }
      }
      else {
        ctx(pd->call).logError("panel specs must have at least 2 args");
      }
    }
  }
  if (0 && !panelSpecs.empty()) cerr << repr_type(traceSpec.ref.typeName) + ": panelSpecs=" + repr(panelSpecs) + "\n";

  // Somewhat ad-hoc
  if (0 && traceSpec.ref.itemType && traceSpec.ref.itemType->asStruct()) {
    auto ts = traceSpec.ref.itemType->asStruct();
    for (auto &mem : ts->members) {
      if (endsWith(mem->memberName, "vel")) {
        auto posName = mem->memberName.substr(0, mem->memberName.size()-3);
        if (auto posMem = ts->getMember(posName)) {

          panelSpecs.push_back(ScopeModelPanelSpec{
            "phase(" + traceSpec.ref.fullName + ": " + mem->memberName + " vs " + posMem->memberName + ")",
            "phase",
            "phase(" + traceSpec.ref.fullName + ": " + mem->memberName + " vs " + posMem->memberName + ")",
            {traceSpec.ref.member(posMem->memberName), traceSpec.ref.member(mem->memberName)}
          });

        }
      }
    }
  }


  if (renderersByType) {
    for (auto &it : (*renderersByType)[traceSpec.ref.typeName]) {
      if (it.second) {
        panelSpecs.push_back(ScopeModelPanelSpec{
          it.first + "(" + traceSpec.ref.fullName + ")",
          it.first,
          it.first + "(" + traceSpec.ref.fullName + ")",
          {traceSpec.ref}
        });
      }
    }
  }

  if (0) cerr << "type="s + repr_type(traceSpec.ref.typeName) + " " + repr_type(traceSpec.ref.itemType) + " panelSpecs=" + repr(panelSpecs) + "\n";
}


void ScopeModelVisPanel::setRenderer(ScopeModel &m)
{
  if (renderersByStyle) {
    auto rreg = (*renderersByStyle)[spec.style];
    if (rreg) {
      (rreg->f)(m, *this);
      return;
    }
  }

  if (renderersByType) {
    if (spec.refs.size() == 1) {
      if (!spec.refs[0].itemType) {
        cerr << "setRenderer: No type "s + repr_type(spec.refs[0].typeName) + "\n";
        return; // WRITEME: indicate error somewhere
      }
      auto rreg = (*renderersByType)[spec.refs[0].typeName][spec.style];
      if (rreg) {
        (rreg->f)(m, *this);
        return;
      }
    }
  }

  if (spec.style == "movie") {
    if (spec.refs.size() == 1) {
      auto seq = m.trace->getTimeseq(spec.refs[0].seqName);
      if (!seq) {
        cerr << "setRenderer: No trace "s + spec.refs[0].seqName + "\n";
        return;
      }
      renderer = make_shared<ScopePanelRendererVideoFrameMovie>(m, *this, seq);
    }
  }
  else if (spec.style == "audio") {
    if (spec.refs.size() == 1) {
      auto seq = m.trace->getTimeseq(spec.refs[0].seqName);
      if (!seq) {
        cerr << "setRenderer: No trace "s + spec.refs[0].seqName + "\n";
        return;
      }
      renderer = make_shared<ScopePanelRendererAudioFrameAudio>(m, *this, seq);
    }
  }
  else if (spec.style == "renderArrows") {
    if (spec.refs.size() == 1) {
      auto seq = m.trace->getTimeseq(spec.refs[0].seqName);
      if (!seq) {
        cerr << "setRenderer: No trace "s + spec.refs[0].seqName + "\n";
        return;
      }
      if (spec.refs[0].typeName == "R[4,4]") {
        renderer = make_shared<ScopePanelRendererR44Arrows>(m, *this, seq);
      }
      else if (spec.refs[0].typeName == "R[3,3]") {
        renderer = make_shared<ScopePanelRendererR33Arrows>(m, *this, seq);
      }
    }
  }
  else if (spec.style == "coords") {
    renderer = make_shared<ScopePanelRendererCoords>(m, *this);
  }
  else if (endsWith(spec.style, ".ui.render3d")) {
    auto panelFuncName = spec.style.substr(0, spec.style.size()-12);

    YogaCaller caller(m.ctx.mkCaller(panelFuncName));
    renderer = make_shared<ScopePanelRenderer3dYoga>(m, *this, caller);

#ifdef FIXME
    debugUpdate = [caller = caller, &m, refs=spec.refs]
      (R ts, YogaSetCons *&debugLog, Trace *trace, apr_pool_t *mem)
    {
      vector<void *> inArgs;
      if (!m.debugAlt(trace).getRawArgs(inArgs, refs, ts)) return;
      
      YogaSetCons *renderSet{nullptr};
      R astonishment = 0.0;
      caller.debug({&renderSet}, inArgs, m.ctx.reg->paramValues, astonishment, 0.0, debugLog, mem);
    };
#endif
  }
  else if (endsWith(spec.style, ".ui.render2d")) {
    auto panelFuncName = spec.style.substr(0, spec.style.size()-12);
    
    YogaCaller caller(m.ctx.mkCaller(panelFuncName));
    renderer = make_shared<ScopePanelRenderer2dYoga>(m, *this, caller);

#ifdef FIXME
    debugUpdate = [caller = caller, &m, refs=spec.refs]
      (R ts, YogaSetCons *&debugLog, Trace *trace, apr_pool_t *mem)
    {
      vector<void *> inArgs;
      if (!m.debugAlt(trace).getRawArgs(inArgs, refs, ts)) return;
      
      YogaSetCons *renderSet{nullptr};
      R astonishment = 0.0;
      caller.debug({&renderSet}, inArgs, m.ctx.reg->paramValues, astonishment, 0.0, debugLog, mem);
    };
#endif
  }
  else if (endsWith(spec.style, ".ui.phase")) {
    auto panelFuncName = spec.style.substr(0, spec.style.size()-9);
    
    YogaCaller caller(m.ctx.mkCaller(panelFuncName));
    renderer = make_shared<ScopePanelRendererPhaseplot>(m, *this, caller);
    // WRITEME? debugUpdate
  }
  else if (spec.style == "terrain") {
    renderer = make_shared<ScopePanelRendererPolicyTerrain>(m, *this);
  }
  else if (spec.style == "search") {
    renderer = make_shared<ScopePanelRendererPolicySearch>(m, *this);
  }

  if (!renderer) {
    cerr << "Can't create renderer " + spec.style + "\n";
  }
}
