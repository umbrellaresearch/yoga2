#include "./scope_model.h"
#include "../lib/core_types.h"
#include "../lib/scope_math.h"
#include "../db/yoga_layout.h"
#include "../db/yoga_db.h"
#include "../timeseq/trace.h"
#include "../jit/type.h"
#include "../jit/compilation.h"
#include "../jit/runtime.h"
#include "../jit/effects.h"
#include "../jit/yoga_engine.h"
#include "nlohmann-json/json.hpp"
#include "common/uv_wrappers.h"
#include "common/asyncjoy.h"
#include "../remote/client.h"
#include "./policy_search.h"
using json = nlohmann::json;

ScopeModel::ScopeModel(YogaContext const &_ctx)
  : ctx(_ctx)
{
  visDeets = make_shared<json>(json::object());
}

void ScopeModel::setError(string const &_error)
{
  loadStatus.clear();
  modelErr = _error;
  cerr << traceName + ": " + modelErr + "\n";
}

void ScopeModel::setLoadStatus(string const &_status)
{
  loadStatus = _status;
  if (0 && !loadStatus.empty()) {
    cerr << traceName + ": " + loadStatus + "\n";
  }
}

void ScopeModel::setTrace(shared_ptr<Trace> _trace)
{
  trace = _trace;
  paramTrace = nullptr;
  autoScroll = 0.0;
}

void ScopeModel::clearLoadStatus()
{
  loadStatus.clear();
}

void ScopeModel::setTraceManifest(json const &manifest)
{
  traceManifest = make_shared<json>(manifest);

  traceBeginTs = traceManifest->value("beginTs", 0.0);
  traceEndTs = traceManifest->value("endTs", 0.0);
  traceNotes = make_shared<json>(traceManifest->value("traceNotes", json::object()));

  if (isLiveRobot) {
    visDur = 2.0;
    cursPos = 0.75;
  }
  else if (!simEngine.empty() || 
          traceEndTs - traceBeginTs < 5.0) {
    visDur = max(1.0, (traceEndTs - traceBeginTs) * 1.0);
    visTime = traceBeginTs + 0.5 * (traceEndTs - traceBeginTs);
    scrollCursor = scrollCursorWhenZoomedOut = true;
    cursPos = (visTime - traceBeginTs) / visDur;
  }
  else {
    cursPos = 0.5;
  }
  scrubberPresence = scrollCursor ? 0.0 : 1.0;
  
  if (visTime == 0.0 || visTime < traceBeginTs || visTime > traceEndTs) {
    visTime = max(traceEndTs - 3.0, traceBeginTs + visDur * 0.75);
  }

  availTree = getAvailTree(traceManifest->value("timeseqInfos", json::array()));

  restoreVisInfo();
}

ScopeModelTraceSpec::ScopeModelTraceSpec(YogaRef const &_ref)
  :ref(_ref)
{
  uniqueKey = ref.fullName;
  label = ref.fullName;
}


vector<ScopeModelAvail> ScopeModel::getAvailTree(json const &channels)
{
  vector<ScopeModelAvail> ret;
  vector<tuple<string, bool, json>> sortedChannels;

  for (auto &it : channels) {
    string name = it.value("name", "");
    bool isDebug = startsWith(name, "debug.");
    sortedChannels.emplace_back(name, isDebug, it);
  }
  sort(sortedChannels.begin(), sortedChannels.end(), [](tuple<string, bool, json> const &a, tuple<string, bool, json> const &b) {
    auto &[aName, aIsDebug, aInfo] = a;
    auto &[bName, bIsDebug, bInfo] = b;
    if (aIsDebug != bIsDebug) return aIsDebug < bIsDebug;
    return aName < bName;
  });

  for (auto &[name, isDebug, info] : sortedChannels) {
    string typeName = info.value("type", "");
    auto &regType = ctx.reg->timeseqTypes[name];
    if (!regType) {
      regType = ctx.getType(typeName);
    }
    else {
      if (regType->clsName != typeName) {
        cerr << "Sequence " + name + " changed type. Program says " + repr_type(regType) + " but trace says " + repr_type(typeName) + ". Ignoring\n";
        continue; // ignore
      }
    }
    if (0) cerr << "Channel "s + name + "\n";
    ScopeModelAvail ent;
    ent.traceSpec = ScopeModelTraceSpec(ctx.getSeqRef(name));
    ent.children = getAvailTreeChildren(ent.traceSpec.ref);
    ent.setPanelSpecs(ctx);
    ret.emplace_back(ent);
  }
  return ret;
}



vector<ScopeModelAvail> ScopeModel::getAvailTreeChildren(YogaRef const &ref)
{
  vector<ScopeModelAvail> ret;

  auto t = ref.itemType;
  if (t && t->asStruct()) {
    if (!t->asStruct()->scopeNoMembers) {
      for (auto &memit : t->asStruct()->members) {
        ScopeModelAvail ent;
        ent.traceSpec = ScopeModelTraceSpec(ref.member(memit->memberName));
        if (!ent.traceSpec.ref.error.empty()) continue;
        ent.children = getAvailTreeChildren(ent.traceSpec.ref);
        ent.setPanelSpecs(ctx);
        ret.emplace_back(ent);
      }
    }
  }
  if (t && t->asMatrix()) {
    auto tm = t->asMatrix();
    for (int ri = 0; ri < min(4, tm->rows); ri++) {
      for (int ci = 0; ci < min(4, tm->cols); ci++) {
        ScopeModelAvail ent;
        ent.traceSpec = ScopeModelTraceSpec(ref.cell(ri, ci));
        if (!ent.traceSpec.ref.error.empty()) continue;
        ent.children = getAvailTreeChildren(ent.traceSpec.ref);
        ent.setPanelSpecs(ctx);
        ret.emplace_back(ent);
      }
    }
  }

  if (ref.subName.empty()) {
    ScopeModelAvail ent;
    ent.traceSpec = ScopeModelTraceSpec(ref.member("interval"));
    ent.setPanelSpecs(ctx);
    ret.emplace_back(ent);
  }
  return ret;
}


void ScopeModel::getAvailTreeLin1(vector<ScopeModelAvail *> &accum, ScopeModelAvail &node)
{
  accum.push_back(&node);
  for (auto &it : node.children) {
    getAvailTreeLin1(accum, it);
  }
}

vector<ScopeModelAvail *> ScopeModel::getAvailTreeLin()
{
  vector<ScopeModelAvail *> ret;
  for (auto &it : availTree) {
    getAvailTreeLin1(ret, it);
  }
  return ret;
}

void ScopeModel::updateVis()
{
  bool needSave = false;
  if (visTracesNeedsUpdate) {
    visTracesNeedsUpdate = false;
    updateVisTraces();
    uvUiActive = true;
    needSave = true;
  }
  if (visPanelsNeedsUpdate) {
    visPanelsNeedsUpdate = false;
    updateVisPanels();
    uvUiActive = true;
    needSave = true;
  }
  if (needSave) {
    saveVisInfo();
  }
}

void ScopeModel::setVisDeet(string const &name, json value)
{
  if (!visDeets) return;
  (*visDeets)[name] = value;
  saveVisInfo();
}

shared_ptr<ScopeModelVisTrace> 
ScopeModel::mkVisTrace(ScopeModelTraceSpec const &spec, json const &defaults)
{
  auto newTrace = make_shared<ScopeModelVisTrace>();
  newTrace->spec = spec;
  if (defaults.contains("heightTarget")) {
    newTrace->visHeightTarget = defaults["heightTarget"];
    newTrace->visHeight = newTrace->visHeightTarget;
  }
  else {
    newTrace->visHeightTarget = 1.0;
    newTrace->visHeight = 0.25;
  }
  if (defaults.contains("scale")) {
    newTrace->scale = defaults["scale"];
    newTrace->autoScalePending = false;
  }
  else {
    newTrace->scale = 1.0;
    newTrace->autoScalePending = true;
  }
  newTrace->setRenderer(*this);
  return newTrace;
}

shared_ptr<ScopeModelVisPanel>
ScopeModel::mkVisPanel(ScopeModelPanelSpec const &spec, json const &defaults)
{
  auto newPanel = make_shared<ScopeModelVisPanel>();
  newPanel->spec = spec;
  newPanel->setRenderer(*this);

  return newPanel;
}

// Update visTraces from visTracesChecked
void ScopeModel::updateVisTraces()
{
  auto availLin = getAvailTreeLin();
  const int verbose = 0;
  if (verbose) cerr << "updateVisTraces\n";

  size_t visIndex = 0;
  for (auto &availIt : availLin) {
    auto &key = availIt->traceSpec.uniqueKey;
    bool checked = visTracesChecked[key];

    if (visIndex < visTraces.size() && visTraces[visIndex]->spec.uniqueKey == key) {
      if (!checked) {
        if (verbose) cerr << "  Mark for removal "s + key + " at " + repr(visIndex) + "\n";
        visTraces[visIndex]->removed = true;
        visTraces[visIndex]->visHeightTarget = 0.0;
      }
      else {
        if (visTraces[visIndex]->visHeightTarget == 0.0) {
          if (verbose) cerr << "  Resurrect "s + key + " at " + repr(visIndex) + "\n";
          visTraces[visIndex]->removed = false;
          visTraces[visIndex]->visHeightTarget = 1.0;
        }
      }
      visIndex++;
    }
    else if (checked) {
      auto newTrace = mkVisTrace(availIt->traceSpec, json());
      if (verbose) cerr << "  Insert "s + key + " before " + repr(visIndex) + "\n";
      visTraces.insert(visTraces.begin() + visIndex, newTrace);
      visIndex++;
    }
  }

  if (verbose) {
    cerr << "  now:\n";
    for (auto &it : visTraces) {
      cerr << "    "s + it->spec.uniqueKey + " " + to_string(it->visHeightTarget) + " " + to_string(it->visHeight) + "\n";
    }
    cerr << "\n";
  }
}

void ScopeModel::mergeVisPanels(size_t &visIndex, ScopeModelPanelSpec const &spec, bool checked)
{
  if (visIndex < visPanels.size() && visPanels[visIndex]->spec.uniqueKey == spec.uniqueKey) {
    if (!checked) {
      visPanels[visIndex]->removed = true;
    }
    else {
      if (visPanels[visIndex]->removed) {
        visPanels[visIndex]->removed = false;
      }
    }
    visIndex++;
  }
  else if (checked) {
    auto newPanel = mkVisPanel(spec, json());
    visPanels.insert(visPanels.begin() + visIndex, newPanel);
    visIndex++;
  }

}

// Update visPanels from visPanelsChecked
void ScopeModel::updateVisPanels()
{
  auto availLin = getAvailTreeLin();
  const int verbose = 0;
  if (verbose) cerr << "updateVisPanels\n";

  size_t visIndex = 0;

  if (1) {
    mergeVisPanels(visIndex, ScopeModelPanelSpec({
      "terrain", "terrain", "Policy Terrain", {}
    }), desiredPolicyTerrainSpec.isValid());
  }

  if (1) {
    mergeVisPanels(visIndex, ScopeModelPanelSpec({
      "search", "search", "Policy Search", {}
    }), !!policySearchState);
  }

  if (1) {
    mergeVisPanels(visIndex, ScopeModelPanelSpec({
      "train", "train", "Training Job", {}
    }), !!trainJob);
  }

  for (auto &availIt : availLin) {
    for (auto &spec : availIt->panelSpecs) {
      bool checked = visPanelsChecked[spec.uniqueKey];
      mergeVisPanels(visIndex, spec, checked);
    }
  }

  for (auto it = visPanels.begin(); it != visPanels.end(); ) {
    if ((*it)->removed) {
      it = visPanels.erase(it);
    }
    else {
      it++;
    }
  }

  if (verbose) {
    cerr << "  now:\n";
    for (auto &it : visPanels) {
      cerr << "    "s + it->spec.uniqueKey + " " + (it->removed ? "removed" : "") + "\n";
    }
    cerr << "\n";
  }
}



void ScopeModel::animate()
{
  R curTime = realtime();
  R dt = (curTime - lastAnimateTime > 0.125) ? 0.01625 : (curTime - lastAnimateTime);
  lastAnimateTime = curTime;
  animate(dt);
}


void ScopeModel::animate(double dt)
{
  updateAltTraces();
  updatePolicyTerrain();
  updatePolicySquiggles();
  updatePredictTraces();
  updateTraining();
  if (policySearchState) policySearchState->poll(this);
  if (visDurTarget > 0.0) {
    if (visDur < visDurTarget) {
      visDur = min(visDurTarget, visDur * (1.0 + 3.0 * dt));
      if (scrollCursorWhenZoomedOut) {
        auto leftOver = traceBeginTs - (visTime - cursPos * visDur);
        auto rightOver = (visTime + (1.0 - cursPos) * visDur) - traceEndTs;
        if (leftOver > 0) visTime += leftOver;
        if (rightOver > 0) visTime -= rightOver;
      }
    }
    else if (visDur > visDurTarget) {
      visDur = max(visDurTarget, visDur / (1.0 + 3.0 * dt));
    }
    else if (visDur == visDurTarget) {
      visDurTarget = -1.0;
      if (visDur >= 0.8 * (traceEndTs - traceBeginTs) && scrollCursorWhenZoomedOut) {
        scrollCursor = true;
        cursPos = (visTime - traceBeginTs) / visDur;
      }
    }
    uvUiActive = true;
  }
  if (scrollCursor && scrubberPresence > 0.0) {
    scrubberPresence = max(0.0, scrubberPresence - 5.0*dt);
    uvUiActive = true;
  }
  else if (!scrollCursor && scrubberPresence < 1.0) {
    scrubberPresence = min(1.0, scrubberPresence + 5.0*dt);
    uvUiActive = true;
  }

  if (cursPosTarget > 0.0) {
    auto vt = visTime - (cursPos - 0.5) * visDur;
    if (cursPos < cursPosTarget) {
      cursPos = min(cursPosTarget, cursPos + 0.5 * dt);
    }
    else if (cursPos > cursPosTarget) {
      cursPos = max(cursPosTarget, cursPos - 0.5 * dt);
    }
    else if (cursPos == cursPosTarget) {
      cursPosTarget = -1.0;
    }
    visTime = vt + (cursPos - 0.5) * visDur;
    uvUiActive = true;
  }

  if (autoScroll) {
    visTime += autoScroll * dt;
    if (autoScroll > 0 && visTime > traceEndTs) {
      visTime = traceEndTs;
      autoScroll = 0.0;
    }
    else if (autoScroll < 0 && visTime < traceBeginTs) {
      visTime = traceBeginTs;
      autoScroll = 0.0;
    }
    if (scrollCursor) {
      cursPos = (visTime - traceBeginTs) / visDur;
    }
    uvUiActive = true;
  }

  for (auto &tr : visTraces) {
    if (tr->visHeight < tr->visHeightTarget) {
      tr->visHeight = min(tr->visHeightTarget, tr->visHeight * (1 + 3*dt) + 3*dt);
      uvUiActive = true;
    }
    else if (tr->visHeight > tr->visHeightTarget) {
      tr->visHeight = max(tr->visHeightTarget, tr->visHeight / (1 + 3*dt) - 3*dt);
      uvUiActive = true;
    }
  }
  for (auto it = visTraces.begin(); it != visTraces.end(); ) {
    if ((*it)->visHeight < 0.001) {
      it = visTraces.erase(it);
      uvUiActive = true;
    }
    else {
      it++;
    }
  }
}

void ScopeModelVisTrace::scaleUp()
{
  scale = tidyScaleUp(scale);
}

void ScopeModelVisTrace::scaleDn()
{
  scale = tidyScaleDn(scale);
}

void ScopeModelVisTrace::expand()
{
  if (visHeightTarget == 1.0) {
    visHeightTarget = 2.0;
  }
  else if (visHeightTarget == 2.0) {
    visHeightTarget = 4.0;
  }
  else if (visHeightTarget == 4.0) {
    visHeightTarget = 1.0;
  }
}

void ScopeModel::scroll(double deltaX, double deltaY)
{
  autoScroll = 0.0;
  if (!(isfinite(deltaX) && isfinite(deltaY))) throw runtime_error("ScopeModel.scroll: bad delta "s + to_string(deltaX) + " " + to_string(deltaY));
  if (deltaX != 0.0) {
    if (scrollCursor) {
      deltaX = - deltaX;
    }
    visTime = max(traceBeginTs, min(traceEndTs - smallestDt, visTime + deltaX * visDur));
    if (scrollCursor) {
      cursPos = (visTime - traceBeginTs) / visDur;
    }
  }
}

void ScopeModel::zoom(double factor)
{
  auto vd = visDurTarget >= 0.0 ? visDurTarget : visDur;
  if ((factor > 1.0 && (vd * factor <= max(1.0, (traceEndTs - traceBeginTs)*1.1))) ||
      (factor < 1 && (vd * factor >= 1.0/65536)) // avoid getting into double-precision round error territory
    ) {
    visDurTarget = vd * factor;
    scrollCursor = false;
  }
  saveVisInfo();
}

vector<string> ScopeModel::getSourceFiles()
{
  vector<string> ret;
  for (auto &it : ctx.reg->sources->requiredFilesInOrder) {
    if (it && !it->isBuiltin) {
      ret.push_back(it->fn);
    }
  }
  for (auto &it : ctx.reg->sources->requiredFilesInOrder) {
    if (it && it->isBuiltin) {
      ret.push_back(it->fn);
    }
  }
  return ret;
}

void setupDebugLocalsFunc(YogaContext const &ctx, YogaCodeTabModel &tab, FunctionEffects *effects, YogaRef baseRef)
{
  for (auto &[name, loc] : effects->debugLocalMemberLocs) {
    if (loc.file == tab.f) {
      auto ref = baseRef.member(name);
      tab.debugLocalsSorted.emplace_back(ctx.reg, loc, ref);
    }
  }
  for (auto &[debugMemberName, funcName] : effects->debugCallMap) {
    auto &slot = ctx.lookup(funcName);
    auto subEffects = slot.funcEffects;
    if (subEffects) {
      setupDebugLocalsFunc(ctx, tab, subEffects, baseRef.member(debugMemberName));
    }
  }
}

void ScopeModel::setupDebugLocals(YogaCodeTabModel &tab)
{
  for (auto &engineIt : ctx.reg->engineDefs) {
    string engineFn = engineIt->call->literalFuncName;
    string engineName = engineFn;
    if (!engineIt->options->getValueForKey("name", engineName)) {
      throw runtime_error("Warning: bad engine name, should have been caught by setupYogaEngines");
    }
    auto &slot = ctx.lookup(engineFn);
    auto compf = slot.compiledFunc;
    auto effects = slot.funcEffects;

    if (compf && effects && compf->debugLocalsType) {
      auto seqName = "debug." + engineName;      
      auto seqRef = ctx.getSeqRef(seqName);
      setupDebugLocalsFunc(ctx, tab, effects, seqRef);
    }    
  }
  sort(tab.debugLocalsSorted.begin(), tab.debugLocalsSorted.end(),
    [](YogaDebugLocalsEntry const &a, YogaDebugLocalsEntry const &b) {
      return a.sourceLoc.start < b.sourceLoc.start;
    });
}

YogaCodeTabModel &ScopeModel::getCodeTab(string const &fn)
{
  auto &tab = codeTabModels[fn];
  if (!tab.f) {
    tab.f = ctx.reg->sources->requiredFilesByName[fn];
    if (!tab.f) return tab;
    tab.lines.resize(tab.f->lineCount() + 3);
    setupDebugLocals(tab);
  }
  return tab;
}

YogaDebugLocalsEntry::YogaDebugLocalsEntry(YogaCompilation *reg, YogaSourceLoc _sourceLoc, YogaRef _valueRef)
  : sourceLoc(move(_sourceLoc)),
    valueRef(move(_valueRef)),
    valueAcc(_valueRef.seqType, valueRef.subName)
{

}

double ScopeModel::getVisBeginTs()
{
  return max(traceBeginTs, visTime - (cursPos+0.01) * visDur);
}

double ScopeModel::getVisEndTs()
{
  return min(traceEndTs, visTime + (1.01-cursPos) * visDur);
}

double ScopeModel::getScreenTs(double screenPos)
{
  return min(traceEndTs, max(traceBeginTs, visTime + (screenPos - cursPos) * visDur));
}


void ScopeModel::addTrace(string const &uniqueKey)
{
  visTracesChecked[uniqueKey] = true;
  visTracesNeedsUpdate = true;
}

void ScopeModel::removeTrace(string const &uniqueKey)
{
  visTracesChecked[uniqueKey] = false;
  visTracesNeedsUpdate = true;
}

void ScopeModel::addPanel(string const &uniqueKey)
{
  visPanelsChecked[uniqueKey] = true;
  visPanelsNeedsUpdate = true;
}

void ScopeModel::removePanel(string const &uniqueKey)
{
  visPanelsChecked[uniqueKey] = false;
  visPanelsNeedsUpdate = true;
}


void YogaCodeTabModel::navigateToFunction(string const &funcName)
{
  // WRITEME
}



shared_ptr<AltTraceInfo> ScopeModel::mkAltTrace()
{
  auto ret = make_shared<AltTraceInfo>();
  ret->baseTrace = trace;
  ret->paramValueEpoch = ctx.reg->paramValueEpoch;
  ret->overrideParamValues = ctx.reg->paramValues; // copy, may get munged later
  return ret;
}

vector<ScopeModelAlt> ScopeModel::alts()
{
  bool showMain = visRealOverlay;
  bool showParam = visCfOverlay && paramTrace;
  bool showPredict = true && predictJob; // FIXME

  vector<ScopeModelAlt> ret;
  ret.reserve(3);

  if (showMain) {
    ScopeModelAlt alt;
    alt.trace = trace.get();
    alt.beginTs = getVisBeginTs();
    alt.endTs = getVisEndTs();
    alt.isParam = false;
    ret.push_back(alt);
  }
  if (showParam) {
    ScopeModelAlt alt;
    alt.trace = paramTrace->trace.get();
    alt.grads = paramTrace->grads.get();
    alt.beginTs = getVisBeginTs();
    alt.endTs = getVisEndTs();
    alt.isParam = true;
    ret.push_back(alt);
  }
  if (ret.size() > 1) {
    ret[0].isSecondary = true;
    ret[0].alpha = 0.5f;
  }
  if (showPredict) {
    ScopeModelAlt alt;
    alt.trace = predictJob->trace.get();
    alt.beginTs = predictJob->predictStartTs;
    alt.endTs = predictJob->predictStartTs + predictJob->predictDur;
    alt.isPredict = true;
    alt.isSecondary = true;
    alt.alpha = 0.7f;
    ret.push_back(alt);
  }
  return ret;
}

vector<ScopeModelAlt> ScopeModel::alts(YogaRef const &ref)
{
  bool showMain = visRealOverlay;
  bool showParam = visCfOverlay && paramTrace;
  bool showPredict = true && predictJob; // FIXME

  vector<ScopeModelAlt> ret;
  ret.reserve(3);

  if (showMain) {
    ScopeModelAlt alt;
    alt.trace = trace.get();
    alt.seq = alt.trace->getTimeseq(ref.seqName).get();
    alt.isParam = false;
    alt.beginTs = getVisBeginTs();
    alt.endTs = getVisEndTs();
    if (alt.seq) {
      ret.push_back(alt);
    }
  }
  if (showParam) {
    ScopeModelAlt alt;
    alt.trace = paramTrace->trace.get();
    alt.seq = alt.trace->getTimeseq(ref.seqName).get();
    alt.grads = paramTrace->grads.get();
    alt.isParam = true;
    alt.beginTs = getVisBeginTs();
    alt.endTs = getVisEndTs();
    if (alt.seq) {
      ret.push_back(alt);
    }
  }
  if (ret.size() > 1) {
    ret[0].isSecondary = true;
    ret[0].alpha = 0.5f;
  }
  if (showPredict) {
    ScopeModelAlt alt;
    alt.trace = predictJob->trace.get();
    alt.seq = alt.trace->getTimeseq(ref.seqName).get();
    alt.beginTs = predictJob->predictStartTs;
    alt.endTs = predictJob->predictStartTs + predictJob->predictDur;
    alt.isPredict = true;
    alt.isSecondary = true;
    alt.alpha = 0.7f;
    if (alt.seq) {
      ret.push_back(alt);
    }
  }

  return ret;
}

ScopeModelAlt ScopeModel::debugAlt()
{
  ScopeModelAlt alt;
  bool showParam = visCfOverlay && paramTrace;

  if (showParam) {
    alt.trace = paramTrace->trace.get();
    alt.grads = paramTrace->grads.get();
    alt.isParam = true;
  }
  else {
    alt.trace = trace.get();
    alt.isParam = false;
  }
  return alt;
}

ScopeModelAlt ScopeModel::debugAlt(Trace *trace)
{
  ScopeModelAlt alt;
  alt.trace = trace;
  return alt;
}

Trace *ScopeModel::baselineTrace()
{
  return paramTrace ? paramTrace->trace.get() : trace.get();
}


YogaTimeseq * ScopeModelAlt::getSeq(YogaRef const &ref) const
{
  return trace->getTimeseq(ref.seqName).get();
}

YogaValue ScopeModelAlt::getValue(YogaRef const &ref, R ts) const
{
  if (!trace) return YogaValue();
  auto seq = trace->getTimeseq(ref.seqName);
  if (!seq) return YogaValue();
  auto v = seq->getEqualBefore(ts);
  if (!v.isValid()) return v;
  if (!ref.subName.empty()) {
    YogaPtrAccessor acc(seq->type, ref.subName);
    return acc.ptr(v);
  }
  else {
    return v;
  }
}

YogaValue ScopeModelAlt::getValueAfter(YogaRef const &ref, R ts) const
{
  if (!trace) return YogaValue();
  auto seq = trace->getTimeseq(ref.seqName);
  if (!seq) return YogaValue();
  auto v = seq->getAfter(ts);
  if (!v.isValid()) return v;
  if (!ref.subName.empty()) {
    YogaPtrAccessor acc(seq->type, ref.subName);
    return acc.ptr(v);
  }
  else {
    return v;
  }
}

vector<YogaValue> ScopeModelAlt::getArgs(vector<YogaRef> const &refs, R ts) const
{
  vector<YogaValue> ret;
  for (auto &ref : refs) {
    ret.push_back(getValue(ref, ts));
  }
  return ret;
}

bool ScopeModelAlt::getRawArgs(vector<void *> &out, vector<YogaRef> const &refs, R ts) const
{
  out.clear();
  for (auto &ref : refs) {
    auto v = getValue(ref, ts);
    if (!v.isValid()) return false;
    out.push_back(v.buf);
  }
  return true;
}

/*
  ScopeModel creation and lifetime
*/

map<string, shared_ptr<ScopeModel>> ScopeModel::scopeModelCache;

void ScopeModel::stopAllLiveClients()
{
  for (auto &mit : scopeModelCache) {
    if (auto m = mit.second) {
      mit.second = nullptr;
      if (m->liveClient) {
        m->liveClient->rpcControlledStop([m, liveClient1=m->liveClient](string const &err) {
          cerr << "controlledStop: "s + (err.empty() ? "ok"s : err) + "\n";
          liveClient1->stop();
        });
        m->liveClient = nullptr;
      }
      if (m->liveSim) {
        m->liveSim = nullptr;
      }
    }
  }
}

void ScopeModel::clearScopeModelCache()
{
  scopeModelCache.clear();
}


ScopeModel *ScopeModel::getSavedScopeModel(YogaContext const &ctx, string const &traceName)
{
  string cacheKey = "trace:"s + canonTraceName(traceName);
  auto &m = scopeModelCache[cacheKey];
  if (!m) {
    m = make_shared<ScopeModel>(ctx("getSavedScopeModel"));
    if (traceName == "test") {
    }
    else {
      m->traceLoadActive = true;
      m->loadFromDb(traceName, [traceName, m](string const &err) {
        m->traceLoadActive = false;
        if (!err.empty()) {
          m->setError(err);
        }
      });
    }
  }
  return m.get();
}

ScopeModel *ScopeModel::getLiveRobotScopeModel(YogaContext const &ctx, string const &options)
{
  if (ctx.reg->runtimeParams.liveHost.empty()) {
    return getLiveSimScopeModel(ctx, options);
  }
  string cacheKey = "live:" + ctx.reg->runtimeParams.liveHost;
  auto &m = scopeModelCache[cacheKey];
  if (!m) {
    m = make_shared<ScopeModel>(ctx("getLiveRobotScopeModel"));

    m->setTrace(make_shared<Trace>(ctx.reg->shared_from_this()));
    if (!m->trace) {
      m->setError("Can't start trace");
      return nullptr;
    }
    m->isLiveRobot = true;

    m->liveClient = make_shared<YogaClient>(ctx("client"));
    m->liveClient->model = m;
    if (options == "quick") { // WRITEME: parse general options
      m->liveClient->duration = 3.0;
    }
    m->liveClient->onAcquireStatus = [m](string const &status) {
      m->loadStatus = status;
    };
    if (!m->liveClient->startBotd()) {
      m->modelErr = "Failed to start botd:\n" + ctx.reg->diagLog.str();
    }
  }
  return m.get();
}


ScopeModel *ScopeModel::getLiveSimScopeModel(YogaContext const &ctx, string const &options)
{
  string cacheKey = "live:sim";
  auto &m = scopeModelCache[cacheKey];
  if (!m) {
    m = make_shared<ScopeModel>(ctx("getLiveSimScopeModel"));
    m->simEngine = "local";
    m->traceSimActive = true;

    m->setTrace(ctx.reg->startTrace());
    if (!m->trace) {
      m->setError("Can't start trace");
      return nullptr;
    }

    if (!setupYogaEngines(ctx, m->trace.get(), true)) {
      ctx.reg->checkpointDiagLog();
      throw runtime_error("setupYogaEngines failed");
    }

    m->liveSim = make_shared<YogaLiveSim>();
    if (!ctx.reg->getRuntimeParam("timeDilation", m->liveSim->timeDilation)) {
      ctx.logError("Bad timeDilation param");
      return nullptr;
    }

    m->trace->onAllStopped.push_back([&m]() {
      m->setTraceManifest(m->trace->buildManifest(true));
      m->visCfOverlay = true;
      m->visRealOverlay = false;
      m->liveSim = nullptr;
      m->traceSimActive = false;
    });

    m->visRealOverlay = true;
    m->visCfOverlay = false;
    if (auto uiKbd = m->trace->getTimeseq("ui.kbd")) {
      uiKbd->isExtIn = true;
    }
    if (auto uiPuck = m->trace->getTimeseq("ui.puck")) {
      uiPuck->isExtIn = true;
    }
    if (0) cerr << "sim model uses keyboard, running interactively\n";
    m->trace->startEngines();
    m->setTraceManifest(m->trace->buildManifest(true));
  }
  return m.get();
}


ScopeModel *ScopeModel::getSimScopeModel(YogaContext const &ctx, string const &options)
{
  string cacheKey = "sim"; // maybe add something?
  auto &m = scopeModelCache[cacheKey];
  if (!m) {

    m = make_shared<ScopeModel>(ctx("getSimScopeModel"));
    m->simEngine = "local";
    m->traceSimActive = true;

    if (!ctx.reg->runtimeParams.liveHost.empty()) {
      m->setError("Yoga code defines a liveHost, so cannot be simulated");
      return m.get();
    }

    m->setTrace(ctx.reg->startTrace());
    if (!m->trace) {
      m->setError("Can't start trace");
      return m.get();
    }

    if (!setupYogaEngines(ctx, m->trace.get(), true)) {
      ctx.reg->checkpointDiagLog();
      throw runtime_error("setupYogaEngines failed");
    }

    m->trace->onAllStopped.push_back([&m]() {
      if (0) cerr << "sim trace \n";
      m->setTraceManifest(m->trace->buildManifest(true));
      m->traceSimActive = false;
    });

    m->trace->onHalts.push_back([&m]() {
      if (0) cerr << "sim trace halt\n";
      m->trace->stopAllThreads();
    });
    m->visRealOverlay = false;
    m->setLoadStatus("Simulating...");
    uvWork(
      [trace1 = m->trace](string &error, shared_ptr<void> &result) {
        trace1->startEngines();
        trace1->updateIter();
      },
      [m](string const &error, shared_ptr<void> const &result) {
        uvUiActive = true;
        m->clearLoadStatus();
      }
    );
  }
  return m.get();
}


ostream & operator <<(ostream &s, ScopeModelPanelSpec const &a)
{
  s << a.label;
  return s;
}
