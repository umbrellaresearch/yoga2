#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "common/uv_wrappers.h"
#include "./scope_model.h"
#include "../timeseq/timeseq_pipe.h"
#include "common/asyncjoy.h"


R ScopeModel::getTerrainDatum(PolicyRewardMetric const &rMetric)
{
  return calcMetric(baselineTrace(), rMetric);
}


shared_ptr<PolicyTerrainMap> ScopeModel::mkTerrainMapFromSpec(PolicyTerrainSpec const &spec)
{
  auto xVarBound = spec.xVar.bound(ctx, spec.resolution);
  auto yVarBound = spec.yVar.bound(ctx, spec.resolution);
  if (!xVarBound || !yVarBound) {
    // happens when we have a param name saved from a previous run. 
    return nullptr;
  }

  auto newMap = make_shared<PolicyTerrainMap>(ctx, spec);

  newMap->xVarBound = xVarBound;
  newMap->yVarBound = yVarBound;

  return newMap;
}


shared_ptr<AltTraceInfo> ScopeModel::mkAltTraceForPoint(PolicyTerrainMap *newMap, size_t xi, size_t yi)
{
  auto ret = mkAltTrace();
  ret->durationLimit = newMap->spec.rMetric.sampleTime + 0.01;

  newMap->xVarBound->param->setNormValue(ctx, newMap->xVarBound->paramBase + newMap->xVarBound->paramRange * newMap->xVarBound->values[xi], &ret->overrideParamValues);
  newMap->yVarBound->param->setNormValue(ctx, newMap->yVarBound->paramBase + newMap->yVarBound->paramRange * newMap->yVarBound->values[yi], &ret->overrideParamValues);

  return ret;
}

void ScopeModel::calcPolicyTerrainMap(
  shared_ptr<PolicyTerrainMap> newMap, 
  std::function<void (shared_ptr<PolicyTerrainMap>)> onDone,
  std::function<void (R)> onProgress)
{
  auto t0 = clock();
  auto aj = make_shared<AsyncJoy>([newMap, t0, onDone](string const &err) {
    newMap->calcStats();
    onDone(newMap);
    auto t1 = clock();
    if (0) cerr << "Calculated "s + repr(newMap->rs.size()) + " policy terrrain points in " + repr_clock(t1-t0) + "\n";
  });

  for (size_t yi = 0; yi < newMap->yVarBound->values.size(); yi++) {
    for (size_t xi = 0; xi < newMap->xVarBound->values.size(); xi++) {
      auto newTraceInfo = mkAltTraceForPoint(newMap.get(), xi, yi);

      aj->start();
      uvWork(
          [newTraceInfo](string &error, shared_ptr<void> &result) {
            rerunTrace(newTraceInfo.get());
          },
          [newMap, newTraceInfo, onProgress, aj, this1=shared_from_this(), xi, yi](string const &error, shared_ptr<void> const &result) {
            newMap->rat(xi, yi) = calcMetric(newTraceInfo->trace.get(), newMap->spec.rMetric);
            aj->end();
            if (onProgress) {
              onProgress(aj->fractionDone());
            }
          });
    }
  }
  aj->end();
}

void ScopeModel::updatePolicyTerrain()
{
  if (!trace || traceLoadActive || traceSimActive) return;
  if (policyTerrainThreadActive) return;

  auto &spec = desiredPolicyTerrainSpec;
  if (!spec.isValid()) {
    policyTerrainMap = nullptr;
    return;
  }

  if (!policyTerrainMap ||
      policyTerrainMap->paramValueEpoch != ctx.reg->paramValueEpoch ||
      policyTerrainMap->spec.policyTerrainEpoch != spec.policyTerrainEpoch) {

    auto oldMap = policyTerrainMap;
    auto newMap = mkTerrainMapFromSpec(spec);
    if (!newMap) return;

    policyTerrainThreadActive ++;
    calcPolicyTerrainMap(newMap, [this, this1=shared_from_this()](shared_ptr<PolicyTerrainMap> doneMap) {
      policyTerrainMap = doneMap;
      visPanelsNeedsUpdate = true;
      policyTerrainThreadActive --;      
      uvUiActive = true;
    }, [oldMap](R fractionDone) {
      if (oldMap) {
        oldMap->animatePhase = fractionDone;
        uvUiActive = true;
      }
    });
  }
}


