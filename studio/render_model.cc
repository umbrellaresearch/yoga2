#include "./render_model.h"
#include "../geom/panel3d.h"
#include "./yogastudio_ui.h"
#include "../lib/scope_math.h"
#include "../jit/type.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include <SDL_scancode.h>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>

void RenderModel::loadSolids()
{
  auto &l = YogaLayout::instance();
  if (modelName == "test") {
    // WRITEME: write a loadSolidsDir function, scans dir for *.stl
    solids["torso"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/torso.stl", 1.0, dmat4(1)));
    solids["rk"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/rk.stl", 1.0, dmat4(1)));
    solids["rht"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/rht.stl", 1.0, dmat4(1)));
    solids["rhs"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/rhs.stl", 1.0, dmat4(1)));
    solids["rhf"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/rhf.stl", 1.0, dmat4(1)));
    solids["rf"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/rf.stl", 1.0, dmat4(1)));
    solids["ra"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/ra.stl", 1.0, dmat4(1)));

    solids["lk"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/lk.stl", 1.0, dmat4(1)));
    solids["lht"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/lht.stl", 1.0, dmat4(1)));
    solids["lhs"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/lhs.stl", 1.0, dmat4(1)));
    solids["lhf"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/lhf.stl", 1.0, dmat4(1)));
    solids["lf"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/lf.stl", 1.0, dmat4(1)));
    solids["la"].push_back(StlSolid::getNamedSolid(l.yogaDir + "/drivers/dex4/cad42/assets/la.stl", 1.0, dmat4(1)));
  }
  else {
    auto &slot = ctx.lookup(modelName);
    if (slot.compiledFunc) {
      auto f = slot.compiledFunc;
      if (1) {
        cerr << "Found renderSolidFunc " + shellEscape(modelName) + " " + repr(*f) + "\n";
      }
      if (f->inArgs.size() == 0 && 
          f->outArgs.size() == 1 &&
          f->outArgs[0].first->clsName == "Set") {
        renderSolidFunc = f;
        renderSolidType = f->outArgs[0].first;
        renderSolidPos = YogaValueAccessor<glm::mat4>(ctx.getTypeOrThrow("RenderSolid"), "pos");
        renderSolidName = YogaValueAccessor<char *>(ctx.getTypeOrThrow("RenderSolid"), "name");
        renderSolidCaller.assign(ctx.reg, modelName);
      }
    }
    else {
      throw runtime_error("No function " + shellEscape(modelName));
    }
  }
}

vector<string> RenderModel::getModelNames(YogaContext const &ctx)
{
  vector<string> ret;
  ret.push_back("test");
  for (auto &[modelName, slot] : ctx.reg->symbolTable) {
    if (slot.compiledFunc) {
      auto f = slot.compiledFunc;
      if (f->inArgs.size() == 0 && 
          f->outArgs.size() == 1 &&
          f->outArgs[0].first->clsName == "Set") {
        ret.push_back(modelName);
      }
    }
  }
  return ret;
}

void RenderModel::drawRenderView(const ImDrawList* parent_list, const ImDrawCmd* cmd)
{
  drawList.clear();
  addContents();
  drawList.glRender();
}

void RenderModel::addContents()
{
  if (0) {
    drawList.addArrowTriple(mat4(1));
  }
  if (0) {
    for (auto &solid : solids) {
      for (auto &s : solid.second) {
        drawList.addSolid(*s, mat4(1), vec4(0.4, 0.4, 0.667, 1.0));
      }
    }
  }
  if (renderSolidFunc) {
    auto renderSolidType = ctx.getTypeOrThrow("RenderSolid");

    YogaSetCons *renderSet{nullptr};
    R astonishment = 0.0;
    renderSolidCaller({&renderSet}, {}, astonishment);

    if (0) cerr << "Got renderSolid[] "s + (renderSet ? "ok" : "null") + "\n";

    for (auto o = renderSet; o; o = o->next) {
      YogaValue yv(ctx.reg, o);
      if (yv.type == renderSolidType) {
        glm::mat4 pos;
        renderSolidPos.rd(yv, pos);
        char *name{nullptr};
        renderSolidName.rd(yv, name);

        if (0) cerr << "renderSolid " + shellEscape(name) + " at " + to_string(pos) + "\n";

        auto [fullfn, isBuiltin] = YogaLayout::instance().requirePath(name);

        auto &all = solids[fullfn];
        if (all.empty()) {
          all.push_back(StlSolid::getNamedSolid(fullfn, 1.0, dmat4(1)));
        }
        for (auto &s : all) {
          drawList.addSolid(*s, pos, vec4(0.4, 0.4, 0.667, 1.0));
        }
      }
    }
  }
}
