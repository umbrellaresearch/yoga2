#include "ysgui.h"
#include "./main_window.h"
extern "C" {
#include <png.h>
}
#include "../drivers/video/video_types.h"

bool showDebugUi;

YsFontDir ysFonts;

bool SmallCheckbox(const char* label, bool* v)
{
  ImGuiContext& g = *GImGui;
  float savePaddingY = g.Style.FramePadding.y;
  g.Style.FramePadding.y = 0.0f;
  bool pressed = Checkbox(label, v);
  g.Style.FramePadding.y = savePaddingY;
  return pressed;
}

bool SelectablePopupMenu(string const &menuLabel)
{
  auto menuFlags = ImGuiSelectableFlags_NoHoldingActiveID | ImGuiSelectableFlags_SelectOnClick | ImGuiSelectableFlags_SpanAvailWidth;
  if (Selectable(menuLabel.c_str(), IsPopupOpen(menuLabel.c_str()), menuFlags)) {
    OpenPopup(menuLabel.c_str());
  }
  return BeginPopup(menuLabel.c_str());
}



PolylineBuf::PolylineBuf()
{
}

PolylineBuf::~PolylineBuf()
{
}

void PolylineBuf::reserve(size_t n)
{
  pts.reserve(n);
}

void PolylineBuf::draw(U32 color, bool closed, float width)
{
  auto dl = GetWindowDrawList();

  size_t i = 0;
  while (i + 1 < pts.size()) {

    /*
      AddPolyline doesn't handle acute angles well, leading to vanishingly thin parts
      on either side of a sharp angle. We avoid this by doing multiple calls to
      AddPolyline, breaking when there's an acute angle which we detect by a negative
      dot product between the two lines.
    */
    int nlin = 2;
    while (i + nlin < pts.size()) {
      auto [x0, y0] = pts[i+nlin-2];
      auto [x1, y1] = pts[i+nlin-1];
      auto [x2, y2] = pts[i+nlin];
      auto s0x = x1-x0, s0y = y1-y0; 
      auto s1x = x2-x1, s1y = y2-y1;
      auto dotprod = s1x*s0x + s1y*s0y;
      if (dotprod < 0) break;
      nlin ++;
    }

    dl->AddPolyline(&pts[i], nlin, color, false, width);
    i += nlin-1;
  }
}

PolybandBuf::PolybandBuf()
{
}

PolybandBuf::~PolybandBuf()
{
}

void PolybandBuf::reserve(size_t n)
{
  pts.reserve(n);
}

void PolybandBuf::fill(U32 color)
{
  if (pts.size() < 2) return;
  auto dl = GetWindowDrawList();

  dl->PrimReserve(6 * (pts.size() - 1), 4 * (pts.size() - 1));

  ImVec2 uv(dl->_Data->TexUvWhitePixel);
  for (size_t i = 0; i + 1 < pts.size(); i++) {
    dl->PrimQuadUV(
      ImVec2(pts[i].x, pts[i].y1),
      ImVec2(pts[i+1].x, pts[i+1].y1),
      ImVec2(pts[i+1].x, pts[i+1].y0),
      ImVec2(pts[i].x, pts[i].y0),
      uv, uv, uv, uv,
      color);
  }
}

void drawText(float x, float y, U32 color, string const &text, float hAlign, float vAlign)
{
  if (hAlign != 0.0f) {
    auto size = CalcTextSize(text.c_str());
    x -= hAlign * size.x;
    y -= vAlign * size.y;
  }
  else if (vAlign != 0.0f) {
    y -= vAlign * GetTextLineHeight();
  }
  GetWindowDrawList()->AddText(ImVec2(x, y), color, text.c_str());
}

bool drawWinopIcon(float &x, float y, U32 color, char const *label)
{
  bool ret = false;
  ImGuiContext& g = *GImGui;
  GetWindowDrawList()->AddText(ImVec2(x, y), color, label);

  if (IsMouseHoveringRect(ImVec2(x-2, y), ImVec2(x+GetFontSize(), y+GetTextLineHeight()))) {
    if (g.IO.MouseClicked[0]) {
      ret = true;
    }
  }
  x += 1.25f * GetFontSize();
  return ret;
}

SDL_Surface *loadPngAsSurface(string const &fn)
{
  png_image png;

  memset(&png, 0, sizeof(png));
  png.version = PNG_IMAGE_VERSION;
  png.opaque = nullptr;

  if (png_image_begin_read_from_file(&png, fn.c_str()) < 0) {
    throw runtime_error("PNG begin-read " + shellEscape(fn) + " failed: " + repr(png.warning_or_error));
  }
  png.format = PNG_FORMAT_RGBA;

  U32 rowStride = png.width * sizeof(U32);

  auto ret = SDL_CreateRGBSurface(0,
    png.width, png.height, 32,
    0xff << 0, 0xff << 8, 0xff << 16, 0xff << 24);
  if (!ret) return nullptr;

  if (0) cerr << 
      fn + ": size=" + repr(png.width) + "x" + repr(png.height) +
      " rowStride=" + repr(rowStride) + "\n";
  
  png_image_finish_read(&png, nullptr, ret->pixels, rowStride, nullptr);
  if (png.warning_or_error) {
    throw runtime_error("PNG read " + shellEscape(fn) + " failed: " + repr(png.warning_or_error));
  }

  png_image_free(&png);

  return ret;
}



struct YogaFontIcon {
  string fn;
  int codePoint;
  ImFont *font;
  png_image png;
  Blob pngPixels;
  int rectId = 0;
  ImFontAtlas::CustomRect const *dst = nullptr;
};

vector<YogaFontIcon> ysIcons;

void loadPngForFont(ImFont *font, string const &fn, int codePoint)
{
  ImGuiIO& io = GetIO();

  FILE *fp = fopen(fn.c_str(), "r");
  if (!fp) {
    cerr << "Warning: no " + fn + "\n";
    return;
  }

  YogaFontIcon icon;
  icon.fn = fn;
  icon.codePoint = codePoint;
  icon.font = font;
  memset(&icon.png, 0, sizeof(icon.png));
  icon.png.version = PNG_IMAGE_VERSION;
  icon.png.opaque = nullptr;

  if (png_image_begin_read_from_stdio(&icon.png, fp) < 0) {
    throw runtime_error("PNG begin-read " + shellEscape(icon.fn) + " failed: " + repr(icon.png.warning_or_error));
  }
  icon.png.format = PNG_FORMAT_RGBA;

  U32 rowStride = icon.png.width * sizeof(U32);
  icon.pngPixels.alloc(icon.png.height * rowStride);
  void *colormap = nullptr;

  png_image_finish_read(&icon.png, nullptr, icon.pngPixels.data(), rowStride, colormap);
  if (icon.png.warning_or_error) {
    throw runtime_error("PNG read " + shellEscape(icon.fn) + " failed: " + repr(icon.png.warning_or_error));
  }
  fclose(fp);

  for (auto pixi = 0; pixi < icon.png.height * rowStride; pixi += 4) {
    // Make it white, just keep the alpha channel
    icon.pngPixels.data()[pixi+0] = 0xff;
    icon.pngPixels.data()[pixi+1] = 0xff;
    icon.pngPixels.data()[pixi+2] = 0xff;
  }

  icon.rectId = io.Fonts->AddCustomRectFontGlyph(font, icon.codePoint,
    (int)icon.png.width, (int)icon.png.height, (float)icon.png.width);

  ysIcons.emplace_back(std::move(icon));
}

void blitIconsIntoFont()
{
  ImGuiIO& io = GetIO();
  U8 *texPixels=nullptr;
  int texWidth=0, texHeight=0, texBytesPerPixel = 0;
  io.Fonts->GetTexDataAsRGBA32(&texPixels, &texWidth, &texHeight, &texBytesPerPixel);
  if (0) cerr << "Font texture " + repr(texWidth) + "x" + repr(texHeight) +
    "x" + repr(texBytesPerPixel*8) + "\n";

  for (auto &icon : ysIcons) {
    icon.dst = io.Fonts->GetCustomRectByIndex(icon.rectId);
    assert(icon.png.width >= icon.dst->Width);
    assert(icon.png.height >= icon.dst->Height);

    if (0) cerr << "Font character 0x" + repr_04x(icon.codePoint) + " at " +
      repr(icon.dst->X) + "," + repr(icon.dst->Y) + " " +
      repr(icon.dst->Width) + "x" + repr(icon.dst->Height) + 
      "\n";
    for (int y=0; y < icon.dst->Height; y++) {
      ImU32* dstStart = reinterpret_cast<ImU32*>(texPixels) + (icon.dst->Y + y) * texWidth + icon.dst->X;
      U8 *srcStart = reinterpret_cast<U8 *>(&icon.pngPixels[y * icon.png.width * sizeof(U32)]);
      for (int x=0; x < icon.dst->Width; x++) {
        U32 r = srcStart[x*4+0];
        U32 g = srcStart[x*4+1];
        U32 b = srcStart[x*4+2];
        U32 a = srcStart[x*4+3];
        dstStart[x] = IM_COL32(r, g, b, a);
      }
    }
  }
}


const int ICON_MIN_YS = 0xe000;
const int ICON_MAX_YS = 0xe018;

ImFont *addFontWithIcons(
  string const &fontFn, string const &iconFn, float size)
{
  ImGuiIO& io = GetIO();
  auto font = io.Fonts->AddFontFromFileTTF(fontFn.c_str(), size);

  if (!iconFn.empty()) {
    ImFontConfig config;
    config.MergeMode = true;
    config.GlyphMinAdvanceX = size; // Use if you want to make the icon monospaced
    static const ImWchar icon_ranges[] = { ICON_MIN_FA, ICON_MAX_FA, 0 };
    io.Fonts->AddFontFromFileTTF(iconFn.c_str(), size, &config, icon_ranges);
  }

  if (0) cerr << "Loading fontFn=" + fontFn + " iconFn=" + iconFn + " size=" + repr(size) + "\n";

  for (int codePoint = ICON_MIN_YS; codePoint <= ICON_MAX_YS; codePoint++) {
    auto fn = "images/" + repr_04x(codePoint) + "@h" + repr(size) + ".png";
    loadPngForFont(font, fn, codePoint);
  }

  return font;
};




ostream & operator << (ostream &s, ImVec2 const &a)
{
  s << "ImVec2("s << a.x << "," << a.y << ")";
  return s;
}

ostream & operator <<(ostream &s, ImRect const &a)
{
  s << "ImRect("s << a.Min.x << 
    "," << a.Min.y <<
    " to " << a.Max.x <<
    "," << a.Max.y << ")";
  return s;
}

YsStyle *yss;

U32 YsStyle::goodGraphColor(size_t i)
{
  return _goodGraphColors[i % N_goodGraphColors];
}

U32 YsStyle::darkGraphColor(size_t i)
{
  return _darkGraphColors[i % N_goodGraphColors];
}

U32 YsStyle::lightGraphColor(size_t i)
{
  return _lightGraphColors[i % N_goodGraphColors];
}


glm::vec4 YsStyle::goodGraphCol4(size_t i)
{
  auto c = ImColor(goodGraphColor(i));
  return vec4(c.Value.x, c.Value.y, c.Value.z, c.Value.w);
}

glm::vec4 YsStyle::darkGraphCol4(size_t i)
{
  auto c = ImColor(darkGraphColor(i));
  return vec4(c.Value.x, c.Value.y, c.Value.z, c.Value.w);
}

glm::vec4 YsStyle::lightGraphCol4(size_t i)
{
  auto c = ImColor(lightGraphColor(i));
  return vec4(c.Value.x, c.Value.y, c.Value.z, c.Value.w);
}


static void darkle(U32 &col)
{
  ImColor col1(col);

  R y = 0.299 * col1.Value.x + 0.587 * col1.Value.y + 0.114 * col1.Value.z;
  R i = 0.596 * col1.Value.x - 0.274 * col1.Value.y - 0.321 * col1.Value.z;
  R q = 0.211 * col1.Value.x - 0.523 * col1.Value.y + 0.311 * col1.Value.z;

  float gamma = 1.0f;
  auto darky = powf((1.0f - powf(y, gamma)), 1.0f/gamma);

  R r2 = clamp(1.000 * darky + 0.956 * i + 0.621 * q, 0.0, 1.0);
  R g2 = clamp(1.000 * darky - 0.272 * i - 0.657 * q, 0.0, 1.0);
  R b2 = clamp(1.000 * darky - 1.107 * i + 1.705 * q, 0.0, 1.0);

  U32 col2 = ImColor(r2, g2, b2, col1.Value.w);

  if (0) cerr << repr_08x(col) + " => " + repr_08x(col2) + "  (v " + repr_0_3f(y) + " => " + repr_0_3f(darky) + "\n";

  col = col2;
}

YsStyle::YsStyle(string const &uiStyle)
{

  if (1) {
    size_t i = 0;
    _goodGraphColors[i++] = IM_COL32(0xF1, 0x58, 0x54, 0xff); // red
    _goodGraphColors[i++] = IM_COL32(0x5D, 0xA5, 0xDA, 0xff); // blue
    _goodGraphColors[i++] = IM_COL32(0xFA, 0xA4, 0x3A, 0xff); // orange
    _goodGraphColors[i++] = IM_COL32(0x60, 0xBD, 0x68, 0xff); // green
    _goodGraphColors[i++] = IM_COL32(0xF1, 0x7C, 0xB0, 0xff); // pink
    _goodGraphColors[i++] = IM_COL32(0xB2, 0x91, 0x2F, 0xff); // brown
    _goodGraphColors[i++] = IM_COL32(0xB2, 0x76, 0xB2, 0xff); // purple
    _goodGraphColors[i++] = IM_COL32(0xDE, 0xCF, 0x3F, 0xff); // yellow
    _goodGraphColors[i++] = IM_COL32(0x4D, 0x4D, 0x4D, 0xff); // gray
    // Munin:
    _goodGraphColors[i++] = IM_COL32(0x00, 0xcc, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x00, 0x66, 0xb3, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0x80, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0xcc, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x33, 0x00, 0x99, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x99, 0x00, 0x99, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xcc, 0xff, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0x00, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x80, 0x80, 0x80, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x00, 0x8f, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x00, 0x48, 0x7d, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xb3, 0x5a, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xb3, 0x8f, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x6b, 0x00, 0x6b, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x8f, 0xb3, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xb3, 0x00, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xbe, 0xbe, 0xbe, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x80, 0xff, 0x80, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x80, 0xc9, 0xff, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0xc0, 0x80, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0xe6, 0x80, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xaa, 0x80, 0xff, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xee, 0x00, 0xcc, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0x80, 0x80, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x66, 0x66, 0x00, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xff, 0xbf, 0xff, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x00, 0xff, 0xcc, 0xff);
    _goodGraphColors[i++] = IM_COL32(0xcc, 0x66, 0x99, 0xff);
    _goodGraphColors[i++] = IM_COL32(0x99, 0x99, 0x00, 0xff);

    i = 0;
    _darkGraphColors[i++] = IM_COL32(0xa1, 0x3b, 0x38, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x3e, 0x6f, 0x92, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xa8, 0x6e, 0x27, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x40, 0x7f, 0x46, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xa1, 0x53, 0x76, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x77, 0x61, 0x1f, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x77, 0x4f, 0x77, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x95, 0x8b, 0x2a, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x34, 0x34, 0x34, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x00, 0x89, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x00, 0x44, 0x78, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x56, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x89, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x22, 0x00, 0x67, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x67, 0x00, 0x67, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x89, 0xab, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x00, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x56, 0x56, 0x56, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x00, 0x60, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x00, 0x30, 0x54, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x78, 0x3c, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x78, 0x60, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x48, 0x00, 0x48, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x60, 0x78, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x78, 0x00, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x7f, 0x7f, 0x7f, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x56, 0xab, 0x56, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x56, 0x87, 0xab, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x81, 0x56, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x9a, 0x56, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x72, 0x56, 0xab, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x9f, 0x00, 0x89, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x56, 0x56, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x44, 0x44, 0x00, 0xff);
    _darkGraphColors[i++] = IM_COL32(0xab, 0x80, 0xab, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x00, 0xab, 0x89, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x89, 0x44, 0x67, 0xff);
    _darkGraphColors[i++] = IM_COL32(0x67, 0x67, 0x00, 0xff);

    i = 0;
    _lightGraphColors[i++] = IM_COL32(0xf8, 0xac, 0xaa, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xae, 0xd2, 0xed, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xfd, 0xd2, 0x9d, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xb0, 0xde, 0xb4, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xf8, 0xbe, 0xd8, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd9, 0xc8, 0x97, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd9, 0xbb, 0xd9, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xef, 0xe7, 0x9f, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xa6, 0xa6, 0xa6, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x80, 0xe6, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x80, 0xb3, 0xd9, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xc0, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xe6, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x99, 0x80, 0xcc, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xcc, 0x80, 0xcc, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xe6, 0xff, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0x80, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xc0, 0xc0, 0xc0, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x80, 0xc7, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x80, 0xa4, 0xbe, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd9, 0xad, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd9, 0xc7, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xb5, 0x80, 0xb5, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xc7, 0xd9, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd9, 0x80, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xdf, 0xdf, 0xdf, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xc0, 0xff, 0xc0, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xc0, 0xe4, 0xff, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xe0, 0xc0, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xf3, 0xc0, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xd5, 0xc0, 0xff, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xf7, 0x80, 0xe6, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xc0, 0xc0, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xb3, 0xb3, 0x80, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xff, 0xdf, 0xff, 0xff);
    _lightGraphColors[i++] = IM_COL32(0x80, 0xff, 0xe6, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xe6, 0xb3, 0xcc, 0xff);
    _lightGraphColors[i++] = IM_COL32(0xcc, 0xcc, 0x80, 0xff);
  }

  if (uiStyle == "dark") {
    isDark = true;
    darkle(gradientCol);
    darkle(squiggleCol);

    darkle(axisLineCol);
    darkle(axisLightLineCol);
    darkle(axisTicCol);
    darkle(axisLabelCol);
    darkle(axisLightLabelCol);
    darkle(closeIconCol);
    darkle(expandIconCol);
    darkle(boringIconCol);
    darkle(lineNumberCol);
    darkle(codeTextCol);
    darkle(debugValueBgCol);
    darkle(codeUnderlineCol);
    darkle(codeStructrefCol);
    darkle(codeDirtyCol);
    darkle(paramAxisCol);
    darkle(paramNeedleFillCol);
    darkle(paramNeedleBorderCol);
    darkle(paramOrigNeedleFillCol);
    darkle(paramOrigNeedleBorderCol);
    darkle(paramValueCol);

    darkle(panelHeaderCol);
    
    darkle(scopeCursorCol);
    darkle(scopeCursorLabelCol);

    darkle(logMessageErrCol);
    darkle(logMessageLogCol);
    darkle(logMessageBorderCol);
  
    darkle(textLightCol);
    darkle(textMedCol);
  
    darkle(textLinkCol);
    darkle(textErrCol);
    darkle(textRedCol);
    darkle(textGrnCol);
    darkle(textBluCol);

    darkle(scrubShade1);
    darkle(scrubShade2);
    darkle(scrubShade3);
    darkle(scrubGroove1);
    darkle(scrubGroove2);
    darkle(scrubTrough);
    darkle(scrubKnob1);
    darkle(scrubKnob2);
    darkle(scrubKnob3);

    for (size_t i = 0; i < N_goodGraphColors; i++) {
      darkle(_goodGraphColors[i]);
      darkle(_darkGraphColors[i]);
      darkle(_lightGraphColors[i]);
    }
  }

}

