#include "./panel_render.h"
#include "./ysgui.h"
#include "./yogastudio_ui.h"
#include "../jit/runtime.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../geom/solid_geometry.h"
#include "./policy_search.h"



ScopePanelRendererPolicySearch::ScopePanelRendererPolicySearch(ScopeModel &_m, ScopeModelVisPanel &_tr)
  : ScopePanelRenderer3D(_m, _tr)
{
  auto pss = m.policySearchState;
  assert(pss);

  for (auto param : pss->activeParams) {
    // Generate a random direction for each param, but consistent WRT paramName
    // Directions are all in the +,+,+ quadrant 
    auto seed = std::hash<string>{}(param->paramName);
    std::mt19937 gen(seed);
    std::uniform_real_distribution<float> posdis(0.1, 1.0);
    std::uniform_real_distribution<float> bidis(-1.0, 1.0);
    auto color = yss->goodGraphCol4(seed);
    glm::vec3 dir = glm::normalize(glm::vec3(bidis(gen), bidis(gen), posdis(gen)));
    axes.push_back(RenderAxis{dir, color});
  }
}


void ScopePanelRendererPolicySearch::drawSubheader()
{
  if (auto pss = m.policySearchState) {
    if (Button("Commit to params")) {
      pss->writeBackBestParams();
      m.policySearchState = nullptr;
      m.visPanelsNeedsUpdate = true;
    }
    if (Button("Cancel")) {
      m.policySearchState = nullptr;
      m.visPanelsNeedsUpdate = true;
    }
    if (Button("Inject random particles")) {
      pss->delRandomParticles(10);
      pss->addRandomParticles(10);
    }

  }
}

void ScopePanelRendererPolicySearch::addContents()
{
  if (auto pss = m.policySearchState) {

    drawList.addCircle(vec3(0, 0, 0),
      vec3(0.01, 0, 0),
      vec3(0, 0, 0.01),
      vec4(0, 0, 0, 0));

    auto nParams = pss->activeParams.size();

    int particleIndex = 0;
    for (auto particle : pss->particles) {
      vec3 pt(0, 0, 0);
      for (size_t pi = 0; pi < nParams; pi++) {
        auto param = pss->activeParams[pi];
        auto const &axis = axes.at(pi);

        auto len = particle->paramValues[pi] - param->getNormValue(m.ctx);
        if (len != 0.0) {
          vec3 pt2 = pt + float(len*0.125) * axis.dir;

          drawList.addLine(pt, axis.color, pt2, axis.color);
          pt = pt2;
        }
      }

      vec4 dotColor = (particleIndex == 0) ? vec4(0.8, 0.0, 0.0, 1.0) : vec4(0.8, 0.0, 0.0, 0.267);
      float dotSize = 0.005 + 5.0 * (0.02 / (particleIndex + 5.0));
      drawList.addCircle(pt,
        vec3(dotSize, 0, 0),
        vec3(0, 0, dotSize),
        dotColor);
      particleIndex ++;
    }
  }
}

