#pragma once
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "./scope_model.h"
#include "../db/policy.h"


struct PolicySearchState : enable_shared_from_this<PolicySearchState> {
  PolicySearchState(YogaContext const &_ctx, PolicyParamSearchSpec const &_spec);
  YogaContext ctx;
  PolicyParamSearchSpec spec;

  struct Particle {
    Particle(size_t dim);
    vector<R> paramValues;
    vector<R> velocities;

    bool evaluated;
    R evaluation;
  };


  vector<YogaParamInfo *> activeParams;

  void evalParticles(ScopeModel *m);
  void startEvals(ScopeModel *m, shared_ptr<vector<shared_ptr<Particle>>> const &newps);
  void poll(ScopeModel *m);
  shared_ptr<AltTraceInfo> mkAltTraceForParticle(ScopeModel *m, Particle *newp);

  bool isInBounds(Particle const &p);
  shared_ptr<Particle> mkRandomParticle(R variance);
  void writeBackBestParams();
  void delRandomParticles(size_t nDelete=10);
  void addRandomParticles(size_t nAdd);

  int iterCount = 0;

  int threadsRunning{0};
  vector<shared_ptr<Particle>> particles;  
};
