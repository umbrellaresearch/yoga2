#include "common/std_headers.h"
#include "./ysgui.h"
#include "../geom/yogagl.h"
#include "imgui_impl_sdl.h"
#include "../geom/panel3d.h"
#include "./main_window.h"
#include "./yogastudio_ui.h"
#include "../db/yoga_layout.h"
#include <uv.h>
#include <SDL.h>
#include "nlohmann-json/json.hpp"


shared_ptr<MainWindow> mainWindow0;

MainWindow::MainWindow(shared_ptr<YogaCompilation> _reg)
  : reg(_reg),
    ctx(reg->mkCtx("MainWindow")),
    mainui(make_shared<YogaStudioView>(ctx, this)),
    uiPollTimer(),
    redrawAsyncQueue()
{
  compilerOutput = make_shared<string>(reg->summarizeProgram());
  logEvents = 0;
}

MainWindow::~MainWindow()
{
  teardown();
}

bool MainWindow::reloadCode(string const &fn)
{
  auto reg2 = make_shared<YogaCompilation>();
  if (!reg2->compileMain(fn.empty() ? reg->sources->mainFn : fn)) {
    // awkward
    showCompilerOutputWindow = true;
    compilerOutput = make_shared<string>(reg2->diagLog.str());
    return false;
  }
  reg = reg2;

  compilerOutput = make_shared<string>(reg->summarizeProgram());

  ctx = reg->mkCtx("reloadCode");
  return true;
}

void MainWindow::uiPoll()
{
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    uvUiActive = true;
    if (logEvents) logEvent(event);
    ImGui_ImplSDL2_ProcessEvent(&event);
    handleEvent(event);
  }
  updateScroll();
}


void MainWindow::setup()
{
  setupWindow();
  setupGlLoader();
  if (0) setupJoysticks();

  // Setup Dear ImGui context
  IMGUI_CHECKVERSION();
  imguiContext = CreateContext();

  setupDisplayScale();
  setupStyle();
  setupFonts();
  
  // Setup Platform/Renderer bindings
  ImGui_ImplSDL2_InitForOpenGL(window, glContext);
  ImGui_ImplOpenGL3_Init(glsl_version);

  setupIcon();

  /*
    This is a big compromise between libuv, OpenGL and imgui.
    We arrange a repeating libuv timer event every 16 mS and check
    whether something requires a redraw.
    We could do better if we could get an event for the vertical
    retrace, but as far as I can tell we can only get that by
    calling SDL_GL_SwapWindow, which we can only do on the
    main thread (at least on Mac).
  */
  uiPollTimer.timer_init();
  uiPollTimer.timer_start([this]() {
    SetCurrentContext(imguiContext);
    yss = ysStyle;

    if (logEvents >= 2) cerr << "uiPoll\n";
    uiPoll();
    if (uvUiActive) {
      uvUiActive = false;
      uiActiveReservoir = 3;
    }
    if (uiActiveReservoir > 0) {
      uiActiveReservoir--;
      redrawAsyncQueue.push([this]() {
        if (logEvents >= 2) cerr << "uiRedraw\n";
        uiRedraw();
      }, 1);
    }
  }, 16, 16);
  SDL_RaiseWindow(window);
}

void MainWindow::setupJoysticks()
{
  int numJoysticks = SDL_NumJoysticks();

  for (int i = 0; i < numJoysticks; i++) {
    bool isGC = SDL_IsGameController(i);
    if (isGC) {
      auto controller = SDL_GameControllerOpen(i);
      if (controller) {
        cerr << "Game controller "s + SDL_GameControllerName(controller) + "\n";
        break;
      } else {
        cerr << "Could not open gamecontroller "s + repr(i) + ": " + SDL_GetError() + "\n";
      }
    }
  }
}

void MainWindow::setupWindow()
{
  // Decide GL+GLSL versions
#if __APPLE__
  // GL 3.2 Core + GLSL 150
  glsl_version = "#version 150";
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_FORWARD_COMPATIBLE_FLAG); // Always required on Mac
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
#else
  // GL 3.0 + GLSL 130
  glsl_version = "#version 130";
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif

  // Create window with graphics context
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);

#if __APPLE__
  SDL_SetHint(SDL_HINT_TOUCH_MOUSE_EVENTS, "0");
  SDL_SetHint(SDL_HINT_MOUSE_TOUCH_EVENTS, "1");
#endif

  int displayCount = SDL_GetNumVideoDisplays();
  SDL_Rect bestDisplaySize = {0,0,0,0};
  for (int displayId = 0; displayId < displayCount; displayId++) {
    SDL_Rect displaySize;
    if (SDL_GetDisplayBounds(displayId, &displaySize) != 0) {
      cerr << "SDL_GetDisplayUsableBounds failed: "s + SDL_GetError() + "\n";
      wantClose = true;
      return;
    }
    if (SDL_GetDisplayDPI(displayId, nullptr, &screenDpi, nullptr) != 0) {
      cerr << "SDL_GetDisplayDPI failed: "s + SDL_GetError() + "\n";
      wantClose = true;
      return;
    }
    // Note: the above numbers are the fake nominal pixels on Mac, not physical pixels.
    // We don't find out what the real resolution is until we call SDL_GL_GetDrawableSize.
    // Possibly we could defer font loading until then.

    if (0) cerr << 
      "Display "s + repr(displayId) + 
      " pos=" + repr(displaySize.x) + "," + repr(displaySize.y) + 
      " size=" + repr(displaySize.w) + "x" + repr(displaySize.h) + 
      " dpi=" + repr(screenDpi) + "\n";

    if (displaySize.w * displaySize.h > bestDisplaySize.w * bestDisplaySize.h) {
      bestDisplaySize = displaySize;
    }
  }

  if (0) cerr << 
    "Best Display"s +
    " pos=" + repr(bestDisplaySize.x) + "," + repr(bestDisplaySize.y) + 
    " size=" + repr(bestDisplaySize.w) + "x" + repr(bestDisplaySize.h) + 
    " dpi=" + repr(screenDpi) + "\n";


  auto winFlags =
    SDL_WINDOW_OPENGL | 
    SDL_WINDOW_RESIZABLE |
    SDL_WINDOW_ALLOW_HIGHDPI;
  int winW = min(2500, int(bestDisplaySize.w*1.0));
  int winH = bestDisplaySize.h - 40;
  R fracX = 0.5;
  if (tweakForPodcast) {
    winW = 1920;
    winH = 1080;
    fracX = 0.0;
    //winFlags |= SDL_WINDOW_BORDERLESS;
  }
  window = SDL_CreateWindow("Yoga Studio",
      bestDisplaySize.x + max(0, int(fracX * (bestDisplaySize.w - winW))) ,
      bestDisplaySize.y + 40,
      winW, winH, 
      (SDL_WindowFlags)winFlags);
}

void MainWindow::setupGlLoader()
{
  glContext = SDL_GL_CreateContext(window);

  SDL_GL_SetSwapInterval(0);

  // Initialize OpenGL loader
#if defined(IMGUI_IMPL_OPENGL_LOADER_GL3W)
  bool err = gl3wInit() != 0;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLEW)
  bool err = glewInit() != GLEW_OK;
#elif defined(IMGUI_IMPL_OPENGL_LOADER_GLAD)
  bool err = gladLoadGL() == 0;
#else
  bool err = false; // If you use IMGUI_IMPL_OPENGL_LOADER_CUSTOM, your loader is likely to requires some form of initialization.
#endif
  if (err) {
    cerr << "Failed to initialize OpenGL loader!\n";
    wantClose = true;
    return;
  }
}

void MainWindow::setupDisplayScale()
{
  const int verbose = 0;
  auto &io = GetIO();
  int ww, wh;
  SDL_GetWindowSize(window, &ww, &wh);
  if (verbose) cerr << "SDL_GL_GetWindowSize: "s  + repr(ww) + " " + repr(wh) + "\n";

  int dw, dh;
  SDL_GL_GetDrawableSize(window, &dw, &dh);
  if (verbose) cerr << "SDL_GL_GetDrawableSize: "s  + repr(dw) + " " + repr(dh) + "\n";                              

  io.FontGlobalScale = float(ww)/float(dw);

  if (verbose) cerr << "Display: io.FontGlobalScale=" + repr(io.FontGlobalScale) + "\n";
}

void MainWindow::setupStyle()
{
  string uiStyle = YogaLayout::instance().config->value("uiStyle", "light");
  ysStyle = new YsStyle(uiStyle);

  if (uiStyle == "dark") {
    StyleColorsDark();
  }
  else if (uiStyle == "classic") {
    StyleColorsClassic();
  }
  else if (uiStyle == "light") {
    StyleColorsLight();
  }
  else {
    throw runtime_error("Unknown uiStyle in config");
  }

  auto &style = GetStyle();
  style.WindowRounding = 4;
  style.FramePadding = ImVec2(4, 2); // normally (4, 3). Saves a little vertical space.
}

void MainWindow::setupFonts()
{
  auto &io = GetIO();

  float fontScale = 1.0 / io.FontGlobalScale;
  io.Fonts->AddFontDefault();
  if (1) {
    auto sansFn = YogaLayout::instance().imguiFontFile("DroidSans.ttf");
    auto iconFn = YogaLayout::instance().yogaDir + "/deps/fontawesome/webfonts/fa-solid-900.ttf";
    auto proggyFn = YogaLayout::instance().imguiFontFile("ProggyTiny.ttf");

    ysFonts.tinyFont = addFontWithIcons(proggyFn, "", 7.0f * fontScale);
    ysFonts.smlFont = addFontWithIcons(sansFn, iconFn, 9.0f * fontScale);
    ysFonts.medFont = addFontWithIcons(sansFn, iconFn, 12.0f * fontScale);
    ysFonts.lrgFont = addFontWithIcons(sansFn, iconFn, 14.0f * fontScale);

    if (!ysFonts.smlFont || !ysFonts.medFont || !ysFonts.lrgFont) {
      throw runtime_error("Failed to load "s + sansFn + " with " + iconFn);
    }    

    if (tweakForPodcast) {
      io.FontDefault = ysFonts.lrgFont;
    }
    else {
      io.FontDefault = ysFonts.medFont;
    }

    io.Fonts->Build();
    blitIconsIntoFont();
  }
}

void MainWindow::setupIcon()
{
  auto surf = loadPngAsSurface(YogaLayout::instance().yogaDir + "/images/appicon@256.png");
  if (!surf) {
    cerr << "loadPngAsSurface failed: "s + SDL_GetError() + "\n";
    return;
  }
  SDL_SetWindowIcon(window, surf);
  SDL_FreeSurface(surf);
}

void MainWindow::uiRedraw()
{
  auto &io = GetIO();

  // Start the Dear ImGui frame
  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplSDL2_NewFrame(window);
  NewFrame();

  if (!io.ConfigMacOSXBehaviors) {
    if (io.KeyCtrl && IsKeyPressed(SDL_SCANCODE_Q)) {
      wantClose = true;
    }
  }

  PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0);
  mainui->initGlobalDo();
  mainui->run();
  PopStyleVar();

  if (showDemoWindow) {
    ShowDemoWindow(&showDemoWindow);
  }
  if (showMetricsWindow) {
    ShowMetricsWindow(&showMetricsWindow);
  }
  if (showCompilerOutputWindow) {
    drawCompilerOutput(&showCompilerOutputWindow);
  }

  uiRender();

  mainui->execGlobalDoPost();
  if (wantClose) {
    mainui->handleClose();
    uiPollTimer.timer_stop();
    redrawAsyncQueue.async_close();
  }
}

void MainWindow::uiRender()
{
  auto &io = GetIO();

  // Rendering
  Render();
  SDL_GL_MakeCurrent(window, glContext);
  glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
  auto clear_color = GetStyle().Colors[ImGuiCol_WindowBg];
  glClearColor(clear_color.x, clear_color.y, clear_color.z, clear_color.w);
  glClear(GL_COLOR_BUFFER_BIT);
  ImGui_ImplOpenGL3_RenderDrawData(GetDrawData());
  SDL_GL_SwapWindow(window);
  double curTime = realtime();
  if (0) cerr << "Swap "s + to_string(loopCount) + " " + to_string(curTime - lastLoopTime) + "\n";
  lastLoopTime = curTime;
  if (0) cerr << "  swapInterval="s + to_string(SDL_GL_GetSwapInterval()) + "\n";
  loopCount++;
}

void MainWindow::drawCompilerOutput(bool *isOpen)
{
  if (Begin("Compiler Output", isOpen, ImGuiWindowFlags_AlwaysAutoResize)) {
    Text("%s", compilerOutput ? compilerOutput->c_str() : "");
  }
  End();
}

void MainWindow::teardown()
{
  ImGui_ImplOpenGL3_Shutdown();
  if (imguiContext) {
    DestroyContext(imguiContext);
    imguiContext = nullptr;
  }
  if (glContext) {
    SDL_GL_DeleteContext(glContext);
    glContext = nullptr;
  }
  if (window) {
    SDL_DestroyWindow(window);
    window = nullptr;
  }
}

void MainWindow::handleEvent(SDL_Event &event)
{
  auto &io = GetIO();
  switch (event.type) {
    case SDL_QUIT:
      wantClose = true;
      break;

    case SDL_WINDOWEVENT:
      if (0) logEvent(event);
      switch (event.window.event) {
        case SDL_WINDOWEVENT_CLOSE:
          if (event.window.windowID == SDL_GetWindowID(window)) {
            wantClose = true;
          }
          break;
      }
      break;

    case SDL_FINGERDOWN:
      scrolling = false;
      io.MouseWheel = 0.0;
      io.MouseWheelH = 0.0;
      break;
    case SDL_FINGERUP:
      break;
    case SDL_FINGERMOTION:
      break;

    case SDL_MOUSEMOTION:
      break;
    case SDL_MOUSEBUTTONDOWN:
      break;
    case SDL_MOUSEBUTTONUP:
      break;
    case SDL_MOUSEWHEEL:
      break;

    case SDL_MULTIGESTURE:
      if (event.mgesture.numFingers == 2) {
        if (0) cerr << "mguesture "s + to_string(event.mgesture.dTheta) + " " + to_string(event.mgesture.dDist) + "\n";
        if (!scrolling) {
          scrolling = true;
          scrollPrevX = event.mgesture.x;
          scrollPrevY = event.mgesture.y;
        } else {
          double dx = event.mgesture.x - scrollPrevX;
          double dy = event.mgesture.y - scrollPrevY;
          scrollAccelX = clamp(dx * 40.0, -240.0, +240.0);
          scrollAccelY = clamp(dy * 40.0, -240.0, +240.0);
          scrollPrevX = event.mgesture.x;
          scrollPrevY = event.mgesture.y;
        }
      }
      break;

    case SDL_KEYDOWN:
      break;
    case SDL_KEYUP:
      break;

    case SDL_DROPFILE:
      if (mainui) {
        mainui->handleReload(string(event.drop.file));
      }
      SDL_free(event.drop.file);
      break;

    default:
      break;
  }
}

void MainWindow::updateScroll()
{
  auto &io = GetIO();

  const R scrollFriction = 0.01;
  if (scrolling) {
    if (scrollAccelY > 0) scrollAccelY = max(0.0, scrollAccelY - scrollFriction);
    if (scrollAccelY < 0) scrollAccelY = min(0.0, scrollAccelY + scrollFriction);
    io.MouseWheel = -10.0 * scrollAccelY;

    if (scrollAccelX > 0) scrollAccelX = max(0.0, scrollAccelX - scrollFriction);
    if (scrollAccelX < 0) scrollAccelX = min(0.0, scrollAccelX + scrollFriction);
    io.MouseWheelH = -10.0 * scrollAccelX;
  }
  else {
    //io.MouseWheel = 0.0;
    //io.MouseWheelH = 0.0;
  }
}

void MainWindow::logEvent(SDL_Event &event)
{
  switch (event.type) {
    case SDL_QUIT:
      eprintf("Got SDL_QUIT 0x%x\n", event.type);
      break;

    case SDL_WINDOWEVENT:
      switch (event.window.event) {
        case SDL_WINDOWEVENT_SHOWN:
          eprintf("Window %d shown\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_HIDDEN:
          eprintf("Window %d hidden\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_EXPOSED:
          eprintf("Window %d exposed\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_MOVED:
          eprintf("Window %d moved to %d,%d\n",
                  event.window.windowID, event.window.data1,
                  event.window.data2);
          break;
        case SDL_WINDOWEVENT_RESIZED:
          eprintf("Window %d resized to %dx%d\n",
                  event.window.windowID, event.window.data1,
                  event.window.data2);
          break;
        case SDL_WINDOWEVENT_SIZE_CHANGED:
          eprintf("Window %d size changed to %dx%d\n",
                  event.window.windowID, event.window.data1,
                  event.window.data2);
          break;
        case SDL_WINDOWEVENT_MINIMIZED:
          eprintf("Window %d minimized\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_MAXIMIZED:
          eprintf("Window %d maximized\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_RESTORED:
          eprintf("Window %d restored\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_ENTER:
          eprintf("Mouse entered window %d\n",
                  event.window.windowID);
          break;
        case SDL_WINDOWEVENT_LEAVE:
          eprintf("Mouse left window %d\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_FOCUS_GAINED:
          eprintf("Window %d gained keyboard focus\n",
                  event.window.windowID);
          break;
        case SDL_WINDOWEVENT_FOCUS_LOST:
          eprintf("Window %d lost keyboard focus\n",
                  event.window.windowID);
          break;
        case SDL_WINDOWEVENT_CLOSE:
          eprintf("Window %d closed\n", event.window.windowID);
          break;
#if SDL_VERSION_ATLEAST(2, 0, 5)
        case SDL_WINDOWEVENT_TAKE_FOCUS:
          eprintf("Window %d is offered a focus\n", event.window.windowID);
          break;
        case SDL_WINDOWEVENT_HIT_TEST:
          eprintf("Window %d has a special hit test\n", event.window.windowID);
          break;
#endif
        default:
          eprintf("Window %d got unknown event %d\n",
                  event.window.windowID, event.window.event);
          break;
      }
      break;

    case SDL_FINGERDOWN:
      if (logEvents>=2) eprintf("Got SDL_FINGERDOWN\n");
      break;
    case SDL_FINGERUP:
      if (logEvents>=2) eprintf("Got SDL_FINGERUP\n");
      break;
    case SDL_FINGERMOTION:
      if (logEvents>=2) eprintf("Got SDL_FINGERMOTION\n");
      break;

    case SDL_MOUSEMOTION:
      if (logEvents>=2) eprintf("Got SDL_MOUSEMOTION\n");
      break;
    case SDL_MOUSEBUTTONDOWN:
      if (logEvents>=1) eprintf("Got SDL_MOUSEBUTTONDOWN\n");
      break;
    case SDL_MOUSEBUTTONUP:
      if (logEvents>=1) eprintf("Got SDL_MOUSEBUTTONUP\n");
      break;
    case SDL_MOUSEWHEEL:
      if (logEvents>=2) eprintf("Got SDL_MOUSEWHEEL x=%d y=%d dir=%u\n",
        event.wheel.x,
        event.wheel.y,
        event.wheel.direction);
      break;

    case SDL_MULTIGESTURE:
      if (logEvents>=2) eprintf("Got SDL_MULTIGESTURE. numFingers=%d x=%f y=%f\n",
        event.mgesture.numFingers,
        event.mgesture.x, 
        event.mgesture.y);
      break;


    case SDL_KEYDOWN:
      if (logEvents>=2) eprintf("Got SDL_KEYDOWN\n");
      break;
    case SDL_KEYUP:
      if (logEvents>=2) eprintf("Got SDL_KEYUP\n");
      break;
    
    case SDL_DROPBEGIN:
      if (logEvents>=1) eprintf("Got SDL_DROPBEGIN\n");
      break;
    case SDL_DROPFILE:
      if (logEvents>=1) eprintf("Got SDL_DROPFILE. f=%s\n", event.drop.file);
      break;
    case SDL_DROPCOMPLETE:
      if (logEvents>=1) eprintf("Got SDL_DROPFILE\n");
      break;


    default:
      eprintf("Got SDL_? 0x%x\n", event.type);
      break;
  }
}

ostream & operator <<(ostream &s, const SDL_AudioSpec &a)
{
  return s <<
    "SDL_AudioSpec(freq="s << a.freq <<
    " format=0x" << repr_04x(a.format) <<
    " channels=" << repr(int(a.channels)) <<
    " silence=" << repr(int(a.silence)) <<
    " samples=" << repr(a.samples) <<
    " size=" << repr(a.size) <<
    (a.callback ? " cb " : " nocb") <<
    ")";
}

void MainWindow::setupAudio()
{
  SDL_AudioSpec desired {};
  desired.freq = 48000;
  desired.format = AUDIO_S16SYS;
  desired.channels = 1;
  desired.samples = 1024;
  desired.callback = nullptr;

  audioDevice = SDL_OpenAudioDevice(
        nullptr, // name
        0, // iscapture
        &desired, &audioSpec,
        0);
  if (audioDevice == 0) {
    cerr << "No audio available: "s + SDL_GetError() + "\n";
    return;
  }

  SDL_PauseAudioDevice(audioDevice, 0);

  if (0) {
    cerr << "Audio got " + repr(audioSpec) + "\n";

    const int NSAMP = 4096*4;
    const int NCHAN = 2;
    vector<S16> s(NSAMP * NCHAN);
    for (auto i = 0; i < NSAMP; i++) {
      R t = R(i) / R(audioSpec.freq);
      R omega = 440.0 * M_2PI * (1.0 + t);
      R ampl = 2000.0 * 
          exp(min(0.0, (t - 0.02) / 0.01)) * 
          exp(min(0.0, (0.05 - t) / 0.05));
      s[i*NCHAN + 0] = (S16)(ampl * sin(t*omega));
      s[i*NCHAN + 1] = (S16)(ampl * sin(t*omega));
    }

    if (SDL_QueueAudio(audioDevice, s.data(), s.size() * sizeof(S16)) < 0) {
      cerr << "Queue audio error: "s + SDL_GetError() + "\n";
    }
  }
}
