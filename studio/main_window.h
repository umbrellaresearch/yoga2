#pragma once

#include "imgui.h"
#include "./yogastudio_ui.h"
#include "../db/yoga_layout.h"
#include "../jit/context.h"
#include "../geom/texcache.h"
#include <uv.h>
#include <stdio.h>
#include <SDL.h>


struct MainWindow {
  MainWindow(shared_ptr<YogaCompilation> _reg);
  ~MainWindow();

  shared_ptr<YogaCompilation> reg;
  YogaContext ctx;
  shared_ptr<YogaStudioView> mainui;
  ImGuiContext *imguiContext{nullptr};
  SDL_Window* window{nullptr};
  SDL_GLContext glContext{nullptr};
  YsStyle *ysStyle{nullptr};
  char const *glsl_version{nullptr};

  bool tweakForPodcast{false};
  bool showDemoWindow{false};
  bool showMetricsWindow{false};
  bool wantClose{false};
  bool showCompilerOutputWindow{false};

  shared_ptr<string> compilerOutput;

  UvTimer uiPollTimer;
  UvAsyncQueue redrawAsyncQueue;

  int logEvents{0};
  int loopCount{0};
  double lastLoopTime{0.0};
  int uiActiveReservoir{1};
  float screenDpi{150.0};

  bool scrolling{false};
  R scrollAccelX{0.0}, scrollAccelY{0.0};
  R scrollPrevX{0.0}, scrollPrevY{0.0};

  SDL_AudioSpec audioSpec{};
  int audioDevice{0};

  TexCache imgTextures;

  bool reloadCode(string const &fn=""s);
  void setup();
  void setupWindow();
  void setupGlLoader();
  void setupDisplayScale();
  void setupStyle();
  void setupFonts();
  void setupIcon();
  void setupJoysticks();
  void uiPoll();
  void uiRedraw();
  void uiRender();
  void requestClose();
  void teardown();
  void setupAudio();
  void drawCompilerOutput(bool *isOpen);

  void handleEvent(SDL_Event &event);
  void logEvent(SDL_Event &event);
  void updateScroll();

};

extern shared_ptr<MainWindow> mainWindow0;
