#include "./panel_render.h"
#include "./ysgui.h"
#include "./yogastudio_ui.h"
#include "../jit/runtime.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"
#include "../geom/solid_geometry.h"
#include "./main_window.h"

Panel3dShader policyTerrainShader("shaders/policy_terrain.glsl", VERT_POS_NORMAL_UV_COL, {
  "ViewMtx",
  "ProjMtx"
}, {
  "Position",
  "Normal",
  "Color"
});


ScopePanelRendererPolicyTerrain::ScopePanelRendererPolicyTerrain(ScopeModel &_m, ScopeModelVisPanel &_tr)
  :ScopePanelRenderer3D(_m, _tr)
{
  drawList.homeLookat = vec3(0, 0, 0);
  drawList.homeEyepos = vec3(0, -2.0, 3);
  drawList.homeFov = glm::radians(40.0f);
  drawList.homeUp = vec3(0, 0, 1);
  drawList.camHome();
}



void ScopePanelRendererPolicyTerrain::handleCloseRequest()
{
  m.policyTerrainMap = nullptr;
  m.desiredPolicyTerrainSpec = PolicyTerrainSpec();
  m.visPanelsNeedsUpdate = true;
}

string shortParamName(string const &n)
{
  if (n.size() > 30) {
    return n.substr(0, 7) + "..." + n.substr(n.size()-20);
  }
  return n;
}

void ScopePanelRendererPolicyTerrain::drawSubheader()
{
  auto ptm = m.policyTerrainMap;
  if (!ptm) return;

  Text("X: %s", 
    shortParamName(ptm->spec.xVar.paramName).c_str());
  SameLine();
  SetNextItemWidth(10 * GetFontSize());
  if (BeginCombo("##xRange", ptm->xVarBound->getRangeDesc().c_str())) {
    for (auto altRange : {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0}) {
      if (Selectable(ptm->xVarBound->getRangeDescForAltRange(altRange).c_str(), 
        m.desiredPolicyTerrainSpec.xVar.isValid() && altRange == ptm->spec.xVar.paramRange)) {
        m.desiredPolicyTerrainSpec.xVar.paramRange = altRange;
        m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
      }
    }
    if (Selectable("Disable", !m.desiredPolicyTerrainSpec.xVar.isValid())) {
      m.desiredPolicyTerrainSpec.xVar.paramName = "";
      m.visPanelsNeedsUpdate = true;
    }

    EndCombo();
  }
  
  Text("Y: %s", 
    shortParamName(ptm->spec.yVar.paramName).c_str());
  SameLine();
  SetNextItemWidth(10 * GetFontSize());
  if (BeginCombo("##yRange", ptm->yVarBound->getRangeDesc().c_str())) {
    for (auto altRange : {0.01, 0.02, 0.05, 0.1, 0.2, 0.5, 1.0}) {
      if (Selectable(ptm->yVarBound->getRangeDescForAltRange(altRange).c_str(), 
        m.desiredPolicyTerrainSpec.yVar.isValid() && altRange == ptm->spec.yVar.paramRange)) {
        m.desiredPolicyTerrainSpec.yVar.paramRange = altRange;
        m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
      }
    }
    if (Selectable("Disable", !m.desiredPolicyTerrainSpec.yVar.isValid())) {
      m.desiredPolicyTerrainSpec.yVar.paramName = "";
      m.visPanelsNeedsUpdate = true;
    }
    EndCombo();
  }

  Text("R: %s at t=%s", 
    ptm->spec.rMetric.ref.fullName.c_str(),
    repr_0_3f(ptm->spec.rMetric.sampleTime).c_str());
  SameLine();
  SetNextItemWidth(10 * GetFontSize());
  R terrainDatum = m.getTerrainDatum(ptm->spec.rMetric);
  if (BeginCombo("##rScale", ptm->spec.rMetric.getRangeDesc(terrainDatum).c_str())) {
    for (auto altScale : ptm->spec.rMetric.getAltScales()) {
      auto altRangeDesc = ptm->spec.rMetric.getRangeDescForAltScale(terrainDatum, altScale);
      if (Selectable(altRangeDesc.c_str(), 
        m.desiredPolicyTerrainSpec.rMetric.isValid() && altScale == ptm->spec.rMetric.rScale)) {
        m.desiredPolicyTerrainSpec.rMetric.rScale = altScale;
        m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
      }
    }
    EndCombo();
  }

}

void ScopePanelRendererPolicyTerrain::runControls()
{
  if (IsItemHovered()) {
    drawList.hoverControls();
  }
}


static vec3 paramToVec3(PolicyTerrainSpec const &spec, R x, R y, R r)
{
  return vec3(
    softclamp(x),
    softclamp(y),
    softclamp(r / spec.rMetric.rScale)*0.25);
}

static tuple<R, R, R> vec3ToParam(PolicyTerrainSpec const &spec, vec3 p)
{
  return make_tuple(
    softunclamp(p.x),
    softunclamp(p.y),
    softunclamp(p.z*4.0) * spec.rMetric.rScale);
}


void ScopePanelRendererPolicyTerrain::addContents()
{
  auto ptm = m.policyTerrainMap;
  if (!ptm) return;
  
  bool dimFlag = ptm->paramValueEpoch != m.ctx.reg->paramValueEpoch;

  auto &xs = ptm->xVarBound->values;
  auto &ys = ptm->yVarBound->values;
  auto &rs = ptm->rs;

  auto stride = xs.size();

  assert(rs.size() == xs.size() * ys.size());

  auto animateShiftX = ptm->animateTargetX * easeInRaisedCos(ptm->animatePhase);
  auto animateShiftY = ptm->animateTargetY * easeInRaisedCos(ptm->animatePhase);
  auto animateShiftR = ptm->animateTargetR * easeInRaisedCos(ptm->animatePhase);

  vector<vec3> pts;

  R terrainDatum = m.getTerrainDatum(ptm->spec.rMetric);
  for (size_t yi = 0; yi < ys.size(); yi++) {
    for (size_t xi = 0; xi < xs.size(); xi++) {
      R x = xs[xi] - animateShiftX;
      R y = ys[yi] - animateShiftY;
      R r = (rs[yi * stride + xi] - terrainDatum) - animateShiftR;
      pts.push_back(paramToVec3(ptm->spec, x, y, r));
    }
  }

  drawList.addArrowTriple(mat4(1, 0, 0, 0,  0, 1, 0, 0,  0, 0, 1, 0,  0, 0, 0, 1));

  vec3 bestHit;
  float bestDistance=9999.0;
  /*
    WRITEME: add normals and lighting.
    Possibly a texture for the surface
    Definitely lines and dots
    Mark highest point with a colored dot
    Add mouse selection to pick a point.
  */
  for (size_t yi = 0; yi + 1 < ys.size(); yi++) {
    for (size_t xi = 0; xi + 1 < xs.size(); xi++) {

      auto pi0 = (yi+0) * stride + (xi+0);
      auto pi1 = (yi+0) * stride + (xi+1);
      auto pi2 = (yi+1) * stride + (xi+0);
      auto pi3 = (yi+1) * stride + (xi+1);
      auto &p0 = pts[pi0];
      auto &p1 = pts[pi1];
      auto &p2 = pts[pi2];
      auto &p3 = pts[pi3];

      auto col = vec4(0.866, 0.666, 0.466, dimFlag ? 0.533 : 1.0);

      drawList.addQuad(p0, col, p1, col, p2, col, p3, col, 0, &policyTerrainShader);

      vec3 hit;
      float distance;
      if (drawList.hitTestTriangle(p0, p1, p2, hit, distance)) {
        if (distance < bestDistance) {
          bestDistance = distance;
          bestHit = hit;
        }
      }
      if (drawList.hitTestTriangle(p1, p3, p2, hit, distance)) {
        if (distance < bestDistance) {
          bestDistance = distance;
          bestHit = hit;
        }
      }
    }
  }

  if (bestDistance < 9999.0) {
    mouseHit = bestHit;
    mouseHitActive = true;
    drawList.addArrow(bestHit, vec3(0, 0, 1), vec3(1, 0, 0), vec3(0, 1, 0), vec4(0.231, 0.933, 0.675, 0.867));        
  }
  else {
    mouseHitActive = false;
  }

  for (size_t yi = 0; yi + 1 < ys.size(); yi++) {
    for (size_t xi = 0; xi + 1 < xs.size(); xi++) {
      auto pi0 = (yi+0) * stride + (xi+0);
      auto pi1 = (yi+0) * stride + (xi+1);
      auto pi2 = (yi+1) * stride + (xi+0);
      auto pi3 = (yi+1) * stride + (xi+1);

      vec3 above(0, 0, 0.005);

      auto col = vec4(0, 0, 0, 0.533);
      drawList.addLine(pts[pi0] + above, col, pts[pi1] + above, col);
      drawList.addLine(pts[pi0] + above, col, pts[pi2] + above, col);
      if (xi + 2 == xs.size()) {
        drawList.addLine(pts[pi1] + above, col, pts[pi3] + above, col);
      }
      if (yi + 2 == ys.size()) {
        drawList.addLine(pts[pi2] + above, col, pts[pi3] + above, col);
      }
    }
  }
  if (0) {
    auto col = vec4(0, 0, 0.333, 0.133);
    drawList.addQuad(vec3(-5,-5,0), col, vec3(+5,-5,0), col, vec3(-5,+5,0), col, vec3(+5,+5,0), col);
  }

}



void ScopePanelRendererPolicyTerrain::capturePopupClickPos()
{
  clickHit = mouseHit;
  clickHitActive = mouseHitActive;
}

void ScopePanelRendererPolicyTerrain::clearPopupClickPos()
{
  clickHitActive = false;
}

void ScopePanelRendererPolicyTerrain::drawPopupMenu()
{
  auto ptm = m.policyTerrainMap;
  if (ptm && clickHitActive) {

    if (Selectable("Set Params")) {
      auto [xRel, yRel, r] = vec3ToParam(ptm->spec, clickHit);

      auto xParam = m.ctx.reg->paramsByName[ptm->xVarBound->paramName];
      auto yParam = m.ctx.reg->paramsByName[ptm->yVarBound->paramName];
      if (!xParam || !yParam) {
        cerr << "Can't find params\n";
        return;
      }

      xParam->setNormValue(m.ctx, ptm->xVarBound->paramBase + ptm->xVarBound->paramRange * xRel);
      yParam->setNormValue(m.ctx, ptm->yVarBound->paramBase + ptm->yVarBound->paramRange * yRel);

      if (ptm->animatePhase == 0.0) {
        ptm->animateTargetX = xRel;
        ptm->animateTargetY = yRel;
        ptm->animateTargetR = r;
      }
    }
  }
  if (Selectable("Increase resolution")) {
    m.desiredPolicyTerrainSpec.resolution = 2 * m.desiredPolicyTerrainSpec.resolution + 1;
    m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
  }
  if (Selectable("Decrease resolution")) {
    m.desiredPolicyTerrainSpec.resolution = m.desiredPolicyTerrainSpec.resolution / 2 + 1;
    m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
  }
}

