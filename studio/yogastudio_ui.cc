#include "./yogastudio_ui.h"
#include "./scope_model.h"
#include "./ysgui.h"
#include "./main_window.h"
#include "numerical/polyfit.h"
#include "./trace_render.h"
#include "./panel_render.h"
#include "../jit/type.h"
#include "../remote/client.h"
#include "./code_drawer.h"
#include "./render_model.h"
#include "../drivers/ui/ui.h"
#include "nlohmann-json/json.hpp"
#include "./policy_search.h"


map<string, vector<YogaDbTraceInfoRowNameFlags>> YogaStudioView::matchingTraceNamesCache;
map<string, shared_ptr<RenderModel>> YogaStudioView::renderModelCache;



YogaStudioView::YogaStudioView(YogaContext const &_ctx, MainWindow *_parent)
  :ctx(_ctx), parent(_parent)
{
  path = {""s};
  initYogaKbdState(ctx);
  initYogaPuckState(ctx);
}

YogaStudioView::~YogaStudioView()
{
}

void YogaStudioView::logMessage(string const &s, R duration, U32 col)
{
  R curTime = realtime();
  logMessageQueue.push_back(UiLogMessage{s, curTime, duration, col});
}

void YogaStudioView::logMessageErr(string const &s)
{
  logMessage(s, 5.0, yss->logMessageErrCol);
}

void YogaStudioView::logMessageLog(string const &s)
{
  logMessage(s, 1.0, yss->logMessageLogCol);
}

void YogaStudioView::logMessageSync(string const &s)
{
  logMessage(s, 0.0, yss->logMessageLogCol);
}


void YogaStudioView::initGlobalDo()
{
  auto &io = GetIO();
  bool isCmd = io.ConfigMacOSXBehaviors ? io.KeySuper : io.KeyCtrl;
  activeModel = nullptr;
  doClose = isCmd && IsKeyPressed(SDL_SCANCODE_W);
  doReload = isCmd && IsKeyPressed(SDL_SCANCODE_R);
  doGoPath = false;
  if (isCmd && IsKeyPressed(SDL_SCANCODE_L)) {
    doGoPath = true;
    goPath = {"live"s};
  }
  if (isCmd && IsKeyPressed(SDL_SCANCODE_K)) {
    doGoPath = true;
    goPath = {"sim"s};
  }
  if (isCmd && IsKeyPressed(SDL_SCANCODE_O)) {
    doGoPath = true;
    goPath = {"traces"s};
  }
  if (isCmd && IsKeyPressed(SDL_SCANCODE_N)) {
    doGoPath = true;
    goPath = {""s};
  }
  doStop = IsKeyPressed(SDL_SCANCODE_ESCAPE);
  doSaveParams = isCmd && IsKeyPressed(SDL_SCANCODE_S);
  doRevertParams = isCmd && IsKeyPressed(SDL_SCANCODE_Z);
}

void YogaStudioView::initScopeModelDo(ScopeModel *m)
{
  activeModel = m;

  if (IsKeyPressed(SDL_SCANCODE_LEFTBRACKET)) {
    m->visRealOverlay = !m->visRealOverlay;
    if (!m->visRealOverlay) m->visCfOverlay = true;
  }
  if (IsKeyPressed(SDL_SCANCODE_RIGHTBRACKET)) {
    m->visCfOverlay = !m->visCfOverlay;
    if (!m->visCfOverlay) m->visRealOverlay = true;
  }

}


void YogaStudioView::execGlobalDoPre()
{
  if (doReload) {
    logMessageSync("Reloading...");
  }
}

void YogaStudioView::execGlobalDoPost()
{
  if (doClose) {
    handleClose();
  }
  if (doReload) {
    handleReload();
  }
  if (doGoPath) {
    handleClose();
    path = goPath;
  }
  if (doStop) {
    if (activeModel && activeModel->liveClient) {
      activeModel->liveClient->rpcControlledStop([](string const &err) {
        cerr << "controlledStop: "s + (err.empty() ? "ok"s : err) + "\n";
      });
    }
    if (activeModel && activeModel->liveSim) {
      activeModel->liveSim = nullptr;
      if (activeModel->trace && !activeModel->trace->rti->hardHalt) {
        activeModel->trace->rti->hardHalt = true;
        activeModel->trace->updateIter();
      }
    }
  }
  if (doSaveParams) {
    if (!ctx.reg->saveParamEdits()) {
      cerr << "saveParamEdits failed\n";
    }
    doSaveParams = false;
  }
  if (doRevertParams) {
    if (!ctx.reg->revertParamEdits()) {
      cerr << "revertParamEdits failed\n";
    }
    doRevertParams = false;
  }
}

void YogaStudioView::run()
{
  // full size of the parent window.
  SetNextWindowPos(ImVec2(0.0, 0.0), 0, ImVec2(0.0, 0.0));
  SetNextWindowSize(GetIO().DisplaySize, 0);
  if (!Begin("Yoga Studio", nullptr, ImGuiWindowFlags_NoDecoration)) {
    End();
    return;
  }

  if (path[0] == "") {
    drawFrontPage();
  }
  else if (path[0] == "traces") {
    if (path[1] == "") {
      path = {"traces"s, "%"s};
    }
    if (path[1].find('%') != string::npos) {
      drawTracesDir(path[1]);
    }
  }
  else if (path[0] == "scope") {
    if (path[1] == "latest") {
      if (path[2] == "") {
        path = {"scope"s, "latest"s, "%"s};
      }
      string newTraceName;
      if (YogaDb::instance().getLatestTraceName(newTraceName, path[2])) {
        path = {"scope"s, newTraceName};
      }
    }
    if (path[1].find('/') != string::npos) {
      path[1] = canonTraceName(path[1]);
    }
    auto m = ScopeModel::getSavedScopeModel(ctx, path[1]);
    if (!m) return draw404();
    initScopeModelDo(m);
    drawScope(*m, path[2]);
  }
  else if (path[0] == "live") {
    auto m = ScopeModel::getLiveRobotScopeModel(ctx, path[1]);
    if (!m) return draw404();
    initScopeModelDo(m);
    if (m->liveClient && !m->liveClient->onTraceSavedAs) {
      m->liveClient->onTraceSavedAs = [this](string const &newTraceName) {
        path = {"scope", newTraceName};
      };
    }
    drawScope(*m, ""s);
  }
  else if (path[0] == "sim") {
    auto m = ScopeModel::getSimScopeModel(ctx, path[1]);
    if (!m) return draw404();
    initScopeModelDo(m);
    drawScope(*m, ""s);
  }
  else if (path[0] == "test") {
    if (path[1] == "constraints") {
      drawTestConstraints();
    }
    else if (path[1] == "code") {
      drawTestCode();
    }
    else if (path[1] == "renderView") {
      drawTestRenderView();
    }
    else if (path[1] == "polyline") {
      drawTestPolyline();
    }
    else {
      draw404();
    }
  }
  else {
    draw404();
  }
  End();

  execGlobalDoPre();

  drawLogMessages();

}

void YogaStudioView::drawLogMessages()
{
  if (!logMessageQueue.empty()) {
    auto dpySize = GetIO().DisplaySize;
    PushFont(ysFonts.lrgFont);
    PushStyleColor(ImGuiCol_Border, yss->logMessageBorderCol);
    PushStyleVar(ImGuiStyleVar_WindowBorderSize, 2.0);

    SetNextWindowPos(ImVec2(dpySize.x*0.5 - GetFontSize()*25.0, dpySize.y*0.35));
    SetNextWindowSize(ImVec2(GetFontSize() * 30.0, 
      GetFrameHeightWithSpacing() + 
      GetTextLineHeightWithSpacing() * (2+logMessageQueue.size())),
      ImGuiCond_Always);


    if (!Begin("Log##logMessageQueue", nullptr, ImGuiWindowFlags_NoDecoration)) {
      End();
      return;
    }
    R curTime = realtime();
    for (auto &msg : logMessageQueue) {
      PushStyleColor(ImGuiCol_Text, msg.col);
      BulletText("%s", msg.msg.c_str());
      PopStyleColor();
    }
    PopFont();
    while (!logMessageQueue.empty() && curTime - logMessageQueue.front().timestamp > logMessageQueue.front().duration) {
      logMessageQueue.pop_front();
    }
    uvUiActive = true;

    End();
    PopStyleVar();
    PopStyleColor();
  }
}

void YogaStudioView::handleClose()
{
  if (activeModel) {
    activeModel->saveVisInfo();
  }
  ScopeModel::stopAllLiveClients();
  path = {""s};
}

void YogaStudioView::handleReload(string const &fn)
{
  if (activeModel) {
    activeModel->saveVisInfo();
  }
  ScopeModel::stopAllLiveClients();
  if (!parent->reloadCode(fn)) {
    logMessageErr("Reload error (see console)");
    //path = {"error"};
    return;
  }

  ctx = parent->ctx;
  logMessageLog("Reloaded OK");

  initYogaKbdState(ctx);
  initYogaPuckState(ctx);
  matchingTraceNamesCache.clear();
  ScopeModel::clearScopeModelCache();
  renderModelCache.clear();
}


void YogaStudioView::setWindowTitle(string const &title)
{
  SDL_SetWindowTitle(parent->window, title.c_str());
}


void YogaStudioView::drawFrontPage()
{
  setWindowTitle("Yoga Studio");
  drawHamburgerMenu();

  PushFont(ysFonts.lrgFont);
  Text("Yoga Studio is an development environment for robots. See https://yoga.dev for documentation.");
  Text("Running:");
  SameLine();
  PushStyleColor(ImGuiCol_Text, yss->textLinkCol);
  Text("%s", shellEscape(ctx.reg->sources->mainFn).c_str());
  PopStyleColor();

  Text("");
  if (!ctx.reg->runtimeParams.liveHost.empty()) {
    if (Button(("Run live on robot "s + ctx.reg->runtimeParams.robotName).c_str())) {
      doGoPath = true;
      goPath = {"live"s};
    }
  }
  else {
    if (Button("Run simulator")) {
      doGoPath = true;
      goPath = {"sim"s};
    }
  }
  if (Button("Saved Traces")) {
    doGoPath = true;
    goPath = {"traces"s, ctx.reg->runtimeParams.tracePrefix + "_%"};
  }

  PopFont();

  if (iconFadePhase < 1.5) {
    auto texid = parent->imgTextures.getTexidFromImagePng(reinterpret_cast<void const *>("appicon"), "appicon.png");

    auto tl = GetWindowPos();
    auto sz = GetWindowSize();
    auto imgScale = min(sz.x, sz.y)*0.3;
    ImVec2 pc(tl.x + 0.5 * sz.x, tl.y + 0.5 * sz.y);
    ImVec2 p1(pc.x - 0.5 * imgScale, pc.y - 0.5 * imgScale);
    ImVec2 p2(pc.x + 0.5 * imgScale, pc.y + 0.5 * imgScale);

    auto alpha = 0.7 * (0.5 * (1.0 + cos(clamp(iconFadePhase-0.5, 0.0, 1.0) * M_PI)));

    GetWindowDrawList()->AddImage(texid, p1, p2,
      ImVec2(0, 0), ImVec2(1,1),
      ImColor(1.0f, 1.0f, 1.0f, float(alpha)));
    if (0) {
      iconFadePhase += 0.0125;
      uvUiActive = true;
    }
  }

}

void YogaStudioView::drawTestMenu()
{
  if (MenuItem("Code")) {
    doGoPath = true;
    goPath = {"test"s, "code"s};
  }
  if (MenuItem("Constraints")) {
    doGoPath = true;
    goPath = {"test"s, "constraints"s};
  }
  if (MenuItem("polyline")) {
    doGoPath = true;
    goPath = {"test"s, "polyline"s};
  }
  for (auto modelName : RenderModel::getModelNames(ctx)) {
    if (MenuItem(("RenderView: "s + modelName).c_str())) {
      doGoPath = true;
      goPath = {"test"s, "renderView"s, modelName};
    }
  }
}


void YogaStudioView::draw404()
{
  setWindowTitle("Yoga Studio: not found");

  drawHamburgerMenu();

  PushFont(ysFonts.lrgFont);
  PushStyleColor(ImGuiCol_Text, yss->textRedCol);
  BulletText("Not found: %s", joinChar(path.path, ' ').c_str());
  PopStyleColor();
  PopFont();

}


vector<YogaDbTraceInfoRowNameFlags> &YogaStudioView::getMatchingTraceNames(string const &nameLike, int startIndex, int maxCount)
{
  string cacheKey = nameLike + "[" + to_string(startIndex) + "..." + to_string(maxCount) + "]";
  if (matchingTraceNamesCache.find(cacheKey) == matchingTraceNamesCache.end()) {
    YogaDb::instance().getMatchingTraceNames(matchingTraceNamesCache[cacheKey], nameLike, startIndex, maxCount);
  }
  return matchingTraceNamesCache[cacheKey];
}

static string mkCmdKey(char const *key)
{
  auto &io = GetIO();
  if (io.ConfigMacOSXBehaviors) {
    return "Cmd-"s + key;
  }
  else {
    return "C-"s + key;
  }
}

void YogaStudioView::drawHamburgerMenu()
{
  if (BeginChild("hamburger", ImVec2(GetFontSize()*6, GetTextLineHeightWithSpacing()))) {
    if (BeginMenu("Studio")) {
      if (MenuItem("Home", mkCmdKey("N").c_str())) {
        doGoPath = true;
        goPath = {""s};
      }
      MenuItem("Stop", "Esc", &doStop, !!activeModel);
      
      MenuItem("Reload", mkCmdKey("R").c_str(), &doReload);
      if (MenuItem("Start Live", mkCmdKey("L").c_str())) {
        doGoPath = true;
        goPath = {"live"s};
      }
      if (MenuItem("Start Sim", mkCmdKey("K").c_str())) {
        doGoPath = true;
        goPath = {"sim"s};
      }
      if (MenuItem("Trace Directory", mkCmdKey("O").c_str())) {
        doGoPath = true;
        goPath = {"traces"s};
      }
      MenuItem("Show Compiler Output", nullptr, &parent->showCompilerOutputWindow);
      if (showDebugUi) {
        if (BeginMenu("Debug")) {
          MenuItem("Show Imgui Demo", nullptr, &parent->showDemoWindow);
          MenuItem("Show Imgui Metrics", nullptr, &parent->showMetricsWindow);
          EndMenu();
        }
        if (BeginMenu("Tests")) {
          drawTestMenu();
          EndMenu();
        }
      }
      MenuItem("Quit", mkCmdKey("Q").c_str(), &parent->wantClose);
      EndMenu();
    }
    EndChild();
  }
}

void YogaStudioView::drawTracesDir(string const &nameLike)
{
  drawHamburgerMenu();

  setWindowTitle(nameLike == "%" ? "Yoga Studio: all traces"s : "Yoga Studio: traces like "s + nameLike);
  Columns(3, nullptr, false);
  int itemi = 0;
  for (auto &it : getMatchingTraceNames(nameLike, 0, 150)) {
    bool selected{false};
    if (Selectable(it.name.c_str(), selected)) {
      doGoPath = true;
      goPath = {"scope"s, it.name};
      return;
    }
    itemi++;
    if (itemi % 50 == 0) {
      NextColumn();
    }
  }
  Columns(1);
  if (itemi == 0) {
    Text("No matching traces");
  }
}

void YogaLiveSim::updateUi(ScopeModel &m)
{
  syncYogaKbdState();
  if (yogaKbdState.isValid() && yogaKbdState.ts != lastKbdStateTs) {
    if (auto uiKbd = m.trace->getTimeseq("ui.kbd")) {
      lastKbdStateTs = yogaKbdState.ts;
      auto yv = yogaKbdState.dup(uiKbd->mem);
      yv.ts = m.trace->getEndTs();
      if (0) cerr << "Add ui.kbd " + repr_0_3f(yv.ts) + "\n";

      auto last = uiKbd->getLast();
      uiKbd->add(yv.ts - 0.001, last.buf);
      uiKbd->add(yv);
    }
  }
}


void YogaStudioView::drawScope(ScopeModel &m, string const &visInfo)
{
  if (m.liveClient) {
    setWindowTitle("Yoga Studio: live on "s + m.liveClient->liveHost);
  }
  else if (!m.simEngine.empty()) {
    setWindowTitle("Yoga Studio: sim on "s + m.simEngine);
  }
  else {
    setWindowTitle("Yoga Studio: scope "s + m.traceName);
  }

  if (m.liveClient) {
    syncYogaKbdState();
    if (m.liveClient->remoteRunning) {
      if (yogaKbdState.isValid() && yogaKbdState.ts != m.liveClient->lastSentKbdTs) {
        m.liveClient->rpcSetInteractive("ui.kbd", yogaKbdState);
        m.liveClient->lastSentKbdTs = yogaKbdState.ts;
      }

      if (yogaPuckState.isValid() && yogaPuckState.ts != m.liveClient->lastSentPuckTs) {
        m.liveClient->rpcSetInteractive("ui.puck", yogaPuckState);
        m.liveClient->lastSentPuckTs = yogaPuckState.ts;
      }
    }
  }
  else if (m.liveSim && !m.trace->rti->hardHalt) {
    syncYogaKbdState();
    m.liveSim->updateUi(m);

    R now = realtime();
    EngineUpdateContext ctx(now);
    ctx.rtFence = (now - m.trace->rti->realTimeOffset) * m.liveSim->timeDilation;
    m.trace->updateIter(ctx);
    m.traceEndTs = m.trace->getEndTs();
    m.visTime = m.traceEndTs;
    if (m.trace->rti->hardHaltTs) {
      m.visDur = m.trace->rti->hardHaltTs;
      m.cursPos = m.visTime / m.trace->rti->hardHaltTs;
    }
    else {
      m.cursPos = 0.75;
      m.visDur = 2.0;
    }
    uvUiActive = true;
  }

  if (!m.liveClient) {
    if (IsKeyDown(SDL_SCANCODE_RIGHT)) {
      bool shift = GetIO().KeyShift;
      m.autoScroll = shift ? 0.1 : 1.0;
    }
    else if (IsKeyDown(SDL_SCANCODE_LEFT)) {
      bool shift = GetIO().KeyShift;
      m.autoScroll = shift ? -0.1 : -1.0;
    }
    else {
      m.autoScroll = 0;
    }
  }

  m.animate();
  if (!m.modelErr.empty()) {
    drawHamburgerMenu();
    PushFont(ysFonts.lrgFont);
    PushStyleColor(ImGuiCol_Text, yss->textRedCol);
    NewLine();
    TextUnformatted(m.modelErr.c_str());
    PopStyleColor();
    PopFont();
  }
  else if (!m.loadStatus.empty()) {
    drawHamburgerMenu();
    PushFont(ysFonts.lrgFont);
    PushStyleColor(ImGuiCol_Text, yss->textGrnCol);
    NewLine();
    TextUnformatted(m.loadStatus.c_str());
    PopStyleColor();
    PopFont();
  }
  else if (m.liveClient && !m.liveClient->clientError.empty()) {
    drawHamburgerMenu();
    PushFont(ysFonts.lrgFont);
    PushStyleColor(ImGuiCol_Text, yss->textRedCol);
    NewLine();
    TextUnformatted(m.liveClient->clientError.c_str());
    PopStyleColor();
    PopFont();
  }
  else {
    drawScopePage(m);
  }
}

void YogaStudioView::drawScopePage(ScopeModel &m)
{
  ImGuiWindow* window = GetCurrentWindow();
  auto avail = GetContentRegionAvail();

  ImRect bb(window->DC.CursorPos, window->DC.CursorPos + avail);

  float boxL = window->DC.CursorPos.x;
  float boxT = window->DC.CursorPos.y;
  float boxR = boxL + avail.x;
  float boxB = boxT + avail.y;

  R bottomPanelSplit = m.visDeets->value("bottomPanelSplit", 0.70);

  bool hasRightPanels = m.visPanels.size() > 0;
  bool hasBottomPanels = true;
  float scopeSelectorR = boxL + 20.0f*GetFontSize();
  float scopeGraphsR = hasRightPanels ? scopeSelectorR + round((boxR - scopeSelectorR) * 0.75f) : boxR;
  float bottomPanelsT = hasBottomPanels ? boxT + round((boxB - boxT) * bottomPanelSplit) : boxB;

  if (1) {
    if (BeginChild("overlaySelector", ImVec2(scopeSelectorR - boxL, 3.0f*GetFrameHeight()), 0)) {
      drawHamburgerMenu();
      SameLine();
      drawOverlaySelector(m, ImVec2(-1.0, -1.0));
    }
    EndChild();
    if (BeginChild("scopeSelector", ImVec2(scopeSelectorR - boxL, 0))) {
      drawScopeSelector(m, ImVec2(-1, -1));
    }
    EndChild();
  }

  if (1) {
    SetNextWindowPos(ImVec2(scopeSelectorR+1, boxT), ImGuiCond_Always);
    if (BeginChild("scopeGraphs", ImVec2(scopeGraphsR - (scopeSelectorR+1), bottomPanelsT-1 - boxT))) {
      drawScopeGraphs(m, ImVec2(-1.0, -1.0));
    }
    EndChild();
  }

  if (1) {
    SetNextWindowPos(ImVec2(scopeSelectorR+1, bottomPanelsT+1), ImGuiCond_Always);
    if (BeginChild("bottomPanels", ImVec2(scopeGraphsR - (scopeSelectorR+1), boxB - (bottomPanelsT+1)))) {
      InvisibleButton("#vsplitter", ImVec2(-1.0f, 0.3f * GetFontSize()));
      RenderFrame(GetItemRectMin(), GetItemRectMax(), GetColorU32(ImGuiCol_Border), true, 1.0f);
      if (IsItemActive()) {
        bottomPanelSplit += GetIO().MouseDelta.y / round(boxB - boxT);
        m.setVisDeet("bottomPanelSplit", bottomPanelSplit);
      }
      drawVisSource(m);
    }
    EndChild();
  }

  if (hasRightPanels) {
    SetNextWindowPos(ImVec2(scopeGraphsR+1, boxT), ImGuiCond_Always);
    if (BeginChild("rightPanels", ImVec2(boxR - (scopeGraphsR+1), boxB - boxT))) {
      drawScopeRightPanels(m);
    }
    EndChild();
  }
}


void YogaStudioView::drawVisSource(ScopeModel &m)
{
  drawVisSourceFileSelector(m);
  drawDebugLocalsEnable(m);
  drawParamSaveButton(m);
  if (BeginChild(("visSource:"s + m.visSourceFile).c_str(), ImVec2(0, 0))) {
    auto &tab = m.getCodeTab(m.visSourceFile);
    if (!GetCurrentWindow()->WasActive) {
      YogaCodeDrawer(m, tab, true);
    }
    YogaCodeDrawer(m, tab, false);
  }
  EndChild();
}

void YogaStudioView::drawVisSourceFileSelector(ScopeModel &m)
{
  SetNextItemWidth(25.0f * GetFontSize());
  static ImGuiComboFlags flags = 0;
  if (BeginCombo("Src##visSourceFile", m.visSourceFile.c_str(), flags)) {
    auto sourceFiles = m.getSourceFiles();
    for (auto &it : sourceFiles) {
      bool isSelected = it == m.visSourceFile;
      if (Selectable(it.c_str(), isSelected)) {
        m.visSourceFile = it;
        m.saveVisInfo();
      }
      if (isSelected) {
        SetItemDefaultFocus();
      }
    }
    EndCombo();
  }
}

void YogaStudioView::drawParamSaveButton(ScopeModel &m)
{
  if (ctx.reg->paramsNeedSaving()) {
    SameLine();
    if (SmallButton("Save Params")) {
      doSaveParams = true;
    }
    SameLine();
    if (SmallButton("Revert")) {
      doRevertParams = true;
    }
  }
  if (!m.desiredPolicySearchSpec.participatingParams.empty()) {
    SameLine();
    if (SmallButton("Clear searchable")) {
      m.desiredPolicySearchSpec.participatingParams.clear();
    }
  }
  if (!m.desiredGradientSpec.empty()) {
    SameLine();
    if (SmallButton("Clear Grads")) {
      m.desiredGradientSpec.clear();
    }
  }
  if (m.desiredPolicyTerrainSpec.isValid()) {
    SameLine();
    if (SmallButton("Clear Terrain")) {
      m.desiredPolicyTerrainSpec.clear();
    }
  }
  if (m.desiredPolicySquiggleSpec.isValid()) {
    SameLine();
    if (SmallButton("Clear Squiggles")) {
      m.desiredPolicySquiggleSpec.clear();
    }
  }
}

void YogaStudioView::drawDebugLocalsEnable(ScopeModel &m)
{
  SameLine();
  Checkbox("Values", &m.debugLocalsEnable);
}

void YogaStudioView::drawOverlaySelector(ScopeModel &m, const ImVec2& sizeReq)
{
  if (m.isLiveRobot) {
    Text("Live");
  }
  else {
    Checkbox("real", &m.visRealOverlay);
    SameLine();
    Checkbox("cf", &m.visCfOverlay);
    if (m.ctx.reg->predictDefs.size()) {
      auto cur = m.desiredPredictDef;
      Text("Predict:");
      SameLine();
      if (BeginCombo("##predict", cur ? cur->call->gloss().c_str() : "<none>")) {
        if (Selectable("<none>")) {
          m.desiredPredictDef = nullptr;
        }
        for (auto pd : m.ctx.reg->predictDefs) {
          if (Selectable(pd->call->gloss().c_str())) {
            m.desiredPredictDef = pd;
          }
        }
        EndCombo();
      }
    }
    if (0) {
      if (m.ctx.reg->trainDefs.size()) {
        auto cur = m.desiredTrainDef;
        Text("Train:");
        SameLine();
        if (BeginCombo("train", cur ? cur->call->gloss().c_str() : "<none>")) {
          if (Selectable("<none>")) {
            m.desiredTrainDef = nullptr;
          }
          for (auto pd : m.ctx.reg->trainDefs) {
            if (Selectable(pd->call->gloss().c_str())) {
              m.desiredTrainDef = pd;
            }
          }
          EndCombo();
        }
      }
    }
  }
}

float YogaStudioView::getScopeSelectorHeight(ScopeModel &m, vector<ScopeModelAvail> const &avail, YogaType *parentType)
{
  // WRITEME someday: cache result in model, invalidate when chancing availTree.
  float ret = 0.0;
  if (parentType && parentType->asMatrix()) {
    ret += GetTextLineHeight() * (1 + min(4, parentType->asMatrix()->rows));
  }
  else {
    ret += GetTextLineHeight();
    for (auto &it : avail) {
      ret += getScopeSelectorHeight(m, it.children, it.traceSpec.ref.itemType);
    }
  }
  return ret;
}

void YogaStudioView::drawScopeSelectorLevel(ScopeModel &m, ImRect bb, vector<ScopeModelAvail> const &avail, float indent, YogaType *parentType)
{
  ImGuiWindow* window = GetCurrentWindow();
  if (parentType && parentType->asMatrix()) {
    int ri=0, ci=0;
    for (auto &it : avail) {
      auto isCell = it.traceSpec.ref.memberName == ("r" + to_string(ri) + "c" + to_string(ci));
      if (isCell && ci != 0) {
        SameLine();
      }
      SetCursorPosX(bb.Min.x + indent + (isCell ? ci * 30.0f : 0.0f));
      bool &visible = m.visTracesChecked[it.traceSpec.ref.fullName];
      auto label = (isCell ? "##"s : ""s) + it.traceSpec.ref.memberName + "##visTrace." + it.traceSpec.ref.fullName;
      if (SmallCheckbox(label.c_str(), &visible)) {
        m.visTracesNeedsUpdate = true;
      }

      if (!isCell) {
        drawScopeSelectorLevel(m, bb, it.children, indent+10, it.traceSpec.ref.itemType);
      }
      if (isCell) {
        ci++;
        if (ci >= min(4, parentType->asMatrix()->cols)) {
          ci = 0;
          ri++;
        }
      }
    }
  }
  else {
    for (auto &it : avail) {
      auto rhsX = bb.Max.x - 2.0;
      SetCursorPosX(bb.Min.x + indent);
      bool hasPanelMenu = !it.panelSpecs.empty();

      bool &visible = m.visTracesChecked[it.traceSpec.uniqueKey];
      auto label = it.traceSpec.ref.memberName + "##visTrace." + it.traceSpec.ref.fullName;
      auto typeGloss = it.traceSpec.ref.typeName.substr(0, 18);
      if (1) {
        auto sz = CalcTextSize(typeGloss.c_str());
        auto col = hasPanelMenu ?
          yss->textLinkCol : (
            (it.traceSpec.ref.itemType && it.traceSpec.ref.itemType->isPrimitive()) ? 
            yss->textLightCol : 
            yss->textMedCol);
        rhsX -= sz.x + 6.0;
        window->DrawList->AddText(ImVec2(rhsX + 5.0, GetCursorScreenPos().y + 3.0f), col, typeGloss.c_str());
      }
      if (SmallCheckbox(label.c_str(), &visible)) {
        m.visTracesNeedsUpdate = true;
      }
      if (hasPanelMenu) {
        SameLine();
        SetCursorPosX(rhsX);

        if (SelectablePopupMenu("##" + it.traceSpec.uniqueKey)) {
          for (auto &spec : it.panelSpecs) {
            bool &visible = m.visPanelsChecked[spec.uniqueKey];
            auto menuItemLabel = spec.label + " panel";
            if (MenuItem(menuItemLabel.c_str(), nullptr, &visible, true)) {
              m.visPanelsNeedsUpdate = true;
            }
          }
          EndMenu();
        }
      }
      drawScopeSelectorLevel(m, bb, it.children, indent+10, it.traceSpec.ref.itemType);
    }
  }
}

void YogaStudioView::drawScopeSelector(ScopeModel &m, const ImVec2& sizeReq)
{
  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;

  ImVec2 pos = window->DC.CursorPos;
  ImVec2 size = CalcItemSize(sizeReq, CalcItemWidth(), g.Font->FontSize*5.0f + g.Style.FramePadding.y*2.0f);

  if (0) cerr << "drawScopeSelector wrm="s + repr(GetContentRegionMaxAbs()) + " sizeReq="s + repr(sizeReq) + " pos=" + repr(pos) + " size=" + repr(size) + "\n";

  ImRect bb(pos, pos + size);
  ItemSize(size, g.Style.FramePadding.y);
  if (!ItemAdd(bb, 0))
      return;

  RenderFrame(bb.Min, bb.Max, GetColorU32(ImGuiCol_WindowBg), true, g.Style.FrameRounding);
  SetCursorPos(ImVec2(0, 0)); // bb.Min

  drawScopeSelectorLevel(m, bb, m.availTree, 0.0f, nullptr);

  m.updateVis();
}

ostream & operator <<(ostream &s, const ScopeGraphLayout &a)
{
  return s <<
    "ScopeGraphLayout(plotT="s << repr(a.plotT) <<
    ", plotR=" << repr(a.plotR) <<
    ", plotB=" << repr(a.plotB) <<
    ", plotL=" << repr(a.plotL) <<
    ", textL=" << repr(a.textL) <<
    ", infoX=" << repr(a.infoX) <<
    ", selX=" << repr(a.selX) <<
    ", convTimeToX=" << repr(a.convTimeToX) <<
    ", convXToTime=" << repr(a.convXToTime) <<
    ", labelOnLeft=" << repr(a.labelOnLeft) <<
    ")";
}


void YogaStudioView::drawScopeGraphs(ScopeModel &m, ImVec2 const &sizeReq)
{
  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;

  ImVec2 pos = window->DC.CursorPos;
  ImVec2 size = CalcItemSize(sizeReq, CalcItemWidth(), 10 * GetTextLineHeight() + g.Style.FramePadding.y*2.0f);
  ItemSize(size, g.Style.FramePadding.y);
  ImRect bb(pos, pos + size);
  auto scrubberBox = ImRect(bb.Min.x, bb.Max.y - GetFontSize() * m.scrubberPresence, bb.Max.x, bb.Max.y);
  if (!m.scrollCursor) {
    drawScopeTimeScrubber(m, scrubberBox);
  }

  if (!ItemAdd(bb, 0))
      return;

  if (IsItemHovered()) {
    ImGuiIO& io = GetIO();
    if (io.MouseWheelH != 0.0 || io.MouseWheel != 0.0) {
      if (0) cerr << "Mouse " + repr(io.MouseWheelH) + " " + repr(io.MouseWheel) + "\n";
      m.scroll(-0.1 * io.MouseWheelH, 0.1 * io.MouseWheel);
    }
    if (IsKeyPressed(SDL_SCANCODE_UP)) {
      m.zoom(0.5);
    }
    if (IsKeyPressed(SDL_SCANCODE_DOWN)) {
      m.zoom(2.0);
    }
  }

  if (0) RenderFrame(bb.Min, bb.Max, GetColorU32(ImGuiCol_WindowBg), false, 0.0f);

  ScopeGraphLayout lo;
  RenderQueue q;
  lo.plotBox = ImRect(bb.Min.x, bb.Min.y, bb.Max.x, bb.Max.y - GetFontSize() * m.scrubberPresence);

  lo.plotL = snap5(lo.plotBox.Min.x + 3.0);
  lo.plotR = snap5(lo.plotBox.Max.x - 3.0);
  lo.plotT = snap5(lo.plotBox.Min.y);
  lo.plotB = snap5(lo.plotBox.Max.y);

  lo.convTimeToX = Polyfit1(
    lo.plotL + (-m.visTime / m.visDur + m.cursPos) * (lo.plotR - lo.plotL),
    (lo.plotR - lo.plotL) / m.visDur);

  lo.convXToTime = Polyfit1(
    m.visTime + m.visDur * (-lo.plotL / (lo.plotR - lo.plotL) - m.cursPos),
    m.visDur / (lo.plotR - lo.plotL));

  // Time picking precision
  lo.selTimeWindow = 0.5 * GetTextLineHeight() / (lo.plotR - lo.plotL) * m.visDur;

  lo.lineHeight = GetTextLineHeight();

  lo.selX = snap5(lo.convTimeToX(m.visTime));
  lo.textL = snap5(lo.plotL + 7.0);

  if ((lo.selX - lo.plotL) < 0.85 * (lo.plotR - lo.plotL)) {
    lo.labelOnLeft = false;
    lo.infoX = lo.selX + 3;
  }
  else {
    lo.labelOnLeft = true;
    lo.infoX = lo.selX - 3;
  }

  if (0) cerr << "mkScopeGraphsPlotBox "s + repr(lo) + "\n";


  drawScopePlotArea(m, lo, q);

  while (!q.textLayer.empty()) {
    q.textLayer.front()();
    q.textLayer.pop_front();
  }
  while (!q.buttonLayer.empty()) {
    q.buttonLayer.front()();
    q.buttonLayer.pop_front();
  }
  while (!q.cursorLayer.empty()) {
    q.cursorLayer.front()();
    q.cursorLayer.pop_front();
  }
  while (!q.tooltipLayer.empty()) {
    q.tooltipLayer.front()();
    q.tooltipLayer.pop_front();
  }
}

void YogaStudioView::drawScopePlotArea(ScopeModel &m, ScopeGraphLayout &lo, RenderQueue &q)
{
  double totVisHeight = 0.0;
  for (auto &tr : m.visTraces) {
    totVisHeight += tr->visHeight;
  }

  drawScopeCursor(m, lo, q);

  double accVisHeight = 0.0;
  if (totVisHeight < 1.0) {
    accVisHeight += 0.5 * (1.0 - totVisHeight);
    totVisHeight = 1.0;
  }

  for (auto &tr : m.visTraces) {
    auto lo2 = lo;
    double hdrT = lo.plotT + 8.0;
    lo2.plotT = accVisHeight / totVisHeight * (lo.plotB - hdrT) + hdrT;
    lo2.plotB = (accVisHeight + tr->visHeight) / totVisHeight * (lo.plotB - hdrT) + hdrT;

    if (tr->visHeightTarget != 0.0 || totVisHeight == 1.0) {
      if (tr->renderer) {
        auto stackDepth = GetCurrentWindow()->IDStack.size();
        PushID(tr->spec.uniqueKey.c_str());
        tr->renderer->setupLayout(lo2);
        tr->renderer->drawAll(q);
        PopID();
        assert(stackDepth == GetCurrentWindow()->IDStack.size());
      }
      else {
        // WRITEME: renderer can be null if an error happened finding the timeseq
        // or in JITing the accessor function. We should figure out how to
        // display something useful.
        // Maybe the answer is for setRenderer to install a dummy renderer
        // instead of a complete one.
      }
    }
    accVisHeight += tr->visHeight;
  }
}

void YogaStudioView::drawScopeCursor(ScopeModel &m, ScopeGraphLayout &lo, RenderQueue &q)
{
  ImGuiWindow* window = GetCurrentWindow();
  if (0) cerr << "drawScopeCursor "s + to_string(lo.selX) + " " + to_string(lo.plotT) + " " + to_string(lo.plotB) + "\n";
  window->DrawList->AddLine(
    ImVec2(lo.selX, lo.plotT + lo.lineHeight),
    ImVec2(lo.selX, lo.plotB),
    yss->scopeCursorCol,
    0.5f);
  string timeLabel = "t=" + repr_0_3f(m.visTime);
  drawText(lo.selX, lo.plotT, yss->scopeCursorLabelCol, timeLabel, 0.5, 0.0);
}

void YogaStudioView::drawScopeRightPanels(ScopeModel &m)
{
  for (auto &it : m.visPanels) {
    if (it->renderer) {
      PushID(it->spec.uniqueKey.c_str());
      if (it->popout) {
        SetNextWindowSize(ImVec2(800, 0));
        if (Begin((it->spec.label + "##" + it->spec.uniqueKey).c_str(), &it->popout,
          ImGuiWindowFlags_NoScrollbar|ImGuiWindowFlags_NoScrollWithMouse)) {
          it->renderer->drawSubheader();
          it->renderer->drawContents();
        }
        End();
      }
      else {
        it->renderer->drawHeader();
        it->renderer->drawSubheader();
        it->renderer->drawContents();
      }
      if (parent) {
        it->renderer->playAudio(parent);
      }
      PopID();
    }
  }
}

// --- render models

RenderModel &YogaStudioView::getRenderModel(string const &modelName)
{
  auto &slot = renderModelCache[modelName];
  if (!slot) {
    slot = make_shared<RenderModel>(ctx("getRenderModel"), modelName);
    slot->loadSolids();
  }
  return *slot;
}

void YogaStudioView::drawRenderView(RenderModel &m)
{
  ImGuiWindow* window = GetCurrentWindow();
  ImGuiContext& g = *GImGui;
  const ImGuiID id = window->GetID((void *)this);
  const ImVec2 pos = window->DC.CursorPos;
  const ImVec2 size = CalcItemSize(ImVec2(-1, -1), CalcItemWidth(), 400);
  auto curbb = ImRect(pos, pos + size);
  ItemSize(size, g.Style.FramePadding.y);
  if (!ItemAdd(curbb, id)) return;
  if (IsItemHovered()) {
    m.drawList.firstPersonControls();
  }

  m.drawList.vpL = curbb.Min.x;
  m.drawList.vpT = curbb.Min.y;
  m.drawList.vpR = curbb.Max.x;
  m.drawList.vpB = curbb.Max.y;

  window->DrawList->AddCallback([](const ImDrawList* parent_list, const ImDrawCmd* cmd) {
    RenderModel *m1 = reinterpret_cast<RenderModel *>(cmd->UserCallbackData);
    if (0) cerr << "draw 3D callback\n";
    m1->drawRenderView(parent_list, cmd);
  }, reinterpret_cast<void *>(&m));

  window->DrawList->AddCallback(ImDrawCallback_ResetRenderState, nullptr);

}

U32 ScopeModelAlt::adjustColor(U32 base, R modulation) const
{
  ImColor c(base);
  c.Value.w = c.Value.w * alpha * modulation;
  return (U32)c;
}


U32 ScopeModelAlt::adjustColor(float r, float g, float b, float a, R modulation) const
{
  return (U32)ImColor(r, g, b, a*alpha*modulation);
}


U32 ScopeModelAlt::adjustColor(vec4 c, R modulation) const
{
  return (U32)ImColor(c.x, c.y, c.z, c.w*alpha*modulation);
}


vec4 ScopeModelAlt::adjustCol4(float r, float g, float b, float a, R modulation) const
{
  return vec4(r, g, b, a*alpha*modulation);
}


vec4 ScopeModelAlt::adjustCol4(vec4 c, R modulation) const
{
  return vec4(c.x, c.y, c.z, c.w*alpha*modulation);
}
