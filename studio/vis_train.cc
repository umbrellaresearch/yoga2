#include "./predict_train.h"
#include "./scope_model.h"
#include "./panel_render.h"


struct ScopePanelRendererTrain : ScopePanelRenderer {
  ScopePanelRendererTrain(ScopeModel &_m, ScopeModelVisPanel &_tr)
    : ScopePanelRenderer(_m, _tr)
  {
  }

  void drawContents() override;
  void handleCloseRequest() override;
  void drawSubheader() override;
};


void ScopePanelRendererTrain::drawContents()
{
  setBoundingBox();

  auto pts = m.trainJob;
  if (!pts) return;

  R lossYAxis = curbb.Min.y + 0.4*(curbb.Max.y - curbb.Min.y);
  R lossYScale = 0.4*(curbb.Min.y - curbb.Max.y);
  R gradNormYAxis = curbb.Min.y + 0.9*(curbb.Max.y - curbb.Min.y);
  R gradNormYScale = 0.4*(curbb.Min.y - curbb.Max.y);

  R lossMax = 1.0;
  R gradNormMax = 1.0;
  for (auto &loss : pts->lossGraph) {
    lossMax = max(lossMax, loss);
  }
  for (auto &gradNorm : pts->gradNormGraph) {
    gradNormMax = max(gradNormMax, gradNorm);
  }
  R xAxis = curbb.Min.x + 5;
  auto xScale = (curbb.Max.x - curbb.Min.x - 10.0) / R(max((size_t)1, 
    max(pts->lossGraph.size(),
        pts->gradNormGraph.size())));

  if (1) {
    PolybandBuf gr;
    auto x0 = xAxis;
    auto x1 = xAxis;
    auto y0 = lossYAxis;
    auto y1 = lossYAxis;
    for (auto &loss : pts->lossGraph) {
      y1 = min(y1, lossYAxis + lossYScale * loss / lossMax);
      x1 += xScale;
      if (floor(x1) > floor(x0)) {
        gr(x0, y0, y1);
        x0 = x1;
        y1 = lossYAxis;
      }
    }
    gr.fill(IM_COL32(0xcc, 0x00, 0x00, 0xff));
  }
  if (1) {
    PolybandBuf gr;
    auto x0 = xAxis;
    auto x1 = xAxis;
    auto y0 = gradNormYAxis;
    auto y1 = gradNormYAxis;
    for (auto &gradNorm : pts->gradNormGraph) {
      y1 = min(y1, gradNormYAxis + gradNormYScale * gradNorm / gradNormMax);
      x1 += xScale;
      if (floor(x1) > floor(x0)) {
        gr(x0, y0, y1);
        x0 = x1;
        y1 = gradNormYAxis;
      }
    }
    gr.fill(IM_COL32(0x00, 0xcc, 0x00, 0xff));
  }
  if (1) {
    auto y0 = lossYAxis;
    auto y1 = y0 + lossYScale;
    auto xEnd = xAxis + xScale * pts->lossGraph.size();
    GetWindowDrawList()->AddLine(ImVec2(xAxis, y0), ImVec2(xAxis, y1), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    GetWindowDrawList()->AddLine(ImVec2(xAxis, y1), ImVec2(xAxis+3, y1), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    drawText(xAxis+4, y1, IM_COL32(0x00, 0x00, 0x00, 0xff), "loss=" + fmta2(lossMax), 0.0, 0.4);
    GetWindowDrawList()->AddLine(ImVec2(xEnd, y0), ImVec2(xEnd, y0+3), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    drawText(xEnd, y0+5, IM_COL32(0x00, 0x00, 0x00, 0xff), fmta2(pts->lossGraph.size()), 1.0, 0.0);
  }
  if (1) {
    auto y0 = gradNormYAxis;
    auto y1 = y0 + gradNormYScale;
    auto xEnd = xAxis + xScale * pts->gradNormGraph.size();
    GetWindowDrawList()->AddLine(ImVec2(xAxis, y0), ImVec2(xAxis, y1), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    GetWindowDrawList()->AddLine(ImVec2(xAxis, y1), ImVec2(xAxis+3, y1), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    drawText(xAxis+4, y1, IM_COL32(0x00, 0x00, 0x00, 0xff), "gradNorm=" + fmta2(gradNormMax), 0.0, 0.4);
    GetWindowDrawList()->AddLine(ImVec2(xEnd, y0), ImVec2(xEnd, y0+3), IM_COL32(0xaa, 0xaa, 0xaa, 0xff));
    drawText(xEnd, y0+5, IM_COL32(0x00, 0x00, 0x00, 0xff), fmta2(pts->gradNormGraph.size()), 1.0, 0.0);
  }

}

void ScopePanelRendererTrain::handleCloseRequest()
{
  m.trainJob = nullptr;
  m.desiredTrainDef = nullptr;
  m.visPanelsNeedsUpdate = true;
}

void ScopePanelRendererTrain::drawSubheader()
{
  Text("%s", m.trainJob ? m.trainJob->trainDef->gloss().c_str() : "none");
}


static ScopePanelRendererRegister regScopePanelRendererTrain("", "train",
  [](ScopeModel &m, ScopeModelVisPanel &tr)
  {
    assert(tr.spec.style == "train");
    tr.renderer = make_shared<ScopePanelRendererTrain>(m, tr);
  }
);
