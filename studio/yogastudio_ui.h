#pragma once
#include "./ysgui.h"
#include "../db/yoga_db.h"
#include "./scope_model.h"
#include "numerical/polyfit.h"
#include "../jit/value.h"

struct ImRect;
struct ScopeModelAvail;
struct MainWindow;
struct ScopeGraphLayout;
struct YogaClient;
struct RenderModel;

ostream & operator <<(ostream &s, const ScopeGraphLayout &a);

struct UiLogMessage {
  string msg;
  R timestamp;
  R duration;
  U32 col;
};

struct YogaStudioView {
  YogaStudioView(YogaContext const &_ctx, MainWindow *_parent);
  ~YogaStudioView();

  YogaContext ctx;
  MainWindow *parent{nullptr};
    
  NavPath path;
  NavPath goPath;

  static map<string, vector<YogaDbTraceInfoRowNameFlags>> matchingTraceNamesCache;
  static map<string, shared_ptr<RenderModel>> renderModelCache;

  deque<UiLogMessage> logMessageQueue;

  R iconFadePhase{0.0};

  YogaStudioView();

  void setWindowTitle(string const &title);
  void logMessage(string const &s, R duration, U32 col);
  void logMessageErr(string const &s);
  void logMessageLog(string const &s);
  void logMessageSync(string const &s);

  void run();
  void drawTestMenu();
  void drawTestConstraints();
  void drawFrontPage();
  void draw404();
  void drawLogMessages();

  void drawTracesDir(string const &nameLike);
  void drawScope(ScopeModel &m, string const &visInfo);
  void drawLive(string const &robotName, string const &options);

  vector<YogaDbTraceInfoRowNameFlags> &getMatchingTraceNames(string const &prefix, int startIndex, int maxCount);

  void handleClose();
  void handleReload(string const &fn=""s);

  void navigate(vector<string> const &newPath);

  void drawHamburgerMenu();
  void drawModalProgress();

  void drawScopePage(ScopeModel &m);
  void playScopeAudio(ScopeModel &m);

  void drawVisSourceFileSelector(ScopeModel &m);
  void drawParamSaveButton(ScopeModel &m);
  void drawDebugLocalsEnable(ScopeModel &m);
  void drawOverlaySelector(ScopeModel &m, ImVec2 const &size_arg);

  float getScopeSelectorHeight(ScopeModel &m, vector<ScopeModelAvail> const &avail, YogaType *parentType);
  void drawScopeSelector(ScopeModel &m, ImVec2 const &size_arg);
  void drawScopeSelectorLevel(ScopeModel &m, ImRect bb, vector<ScopeModelAvail> const &avail, float indent, YogaType *parentType);

  void drawScopeGraphs(ScopeModel &m, ImVec2 const &size_arg);

  void drawScopeBottomPanels(ScopeModel &m, ImVec2 const &size_arg);
  void drawScopeRightPanels(ScopeModel &m);

  ScopeGraphLayout mkScopeGraphsPlotBox(ScopeModel &m, ImRect bb);
  void drawScopeTimeScrubber(ScopeModel &m, ImRect bb);
  void drawScopePlotArea(ScopeModel &m, ScopeGraphLayout &lo, RenderQueue &q);
  void drawScopeCursor(ScopeModel &m, ScopeGraphLayout &lo, RenderQueue &q);

  void drawScopePlotTrace(ScopeModel &m, ScopeGraphLayout &lo2, ScopeModelVisTrace const &tr);
  void drawVisSource(ScopeModel &m);

  RenderModel &getRenderModel(string const &modelName);
  void drawRenderView(RenderModel &m);

  void drawTestPolyline();
  void drawTestRenderView();
  void drawTestCode();

  void initGlobalDo();
  void initScopeModelDo(ScopeModel *m);

  void execGlobalDoPre();
  void execGlobalDoPost();
  
  bool doReload{false}, doClose{false}, doStop{false}, doGoPath{false};
  bool doSaveParams{false}, doRevertParams{false};
  bool doStartPolicySearch{false};

  ScopeModel *activeModel{nullptr};

};

