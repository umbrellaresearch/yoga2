#pragma once
#include "common/std_headers.h"
#include "numerical/numerical.h"
#include "../geom/yogagl.h"
#include "imgui.h"
#define IMGUI_DEFINE_MATH_OPERATORS
#include "imgui_internal.h"
#include "common/uv_wrappers.h"
// Use the _c version, because the cpp version uses u8"..." which doesn't convert to a const char *
// which is what all the imgui functions take.
#include "IconFontCppHeaders/IconsFontAwesome5_c.h"
#include "../timeseq/trace_blobs.h"
#include <SDL_scancode.h>
#include <SDL_audio.h>
#include "../lib/scope_math.h"

using namespace ImGui;

struct ScopeModel;
struct ScopeGraphLayout;
struct SDL_Surface;

struct RenderQueue {
  deque<std::function<void ()>> textLayer;
  deque<std::function<void ()>> buttonLayer;
  deque<std::function<void ()>> cursorLayer;
  deque<std::function<void ()>> tooltipLayer;
};

extern bool showDebugUi;

bool SmallCheckbox(const char* label, bool* v);
bool SelectablePopupMenu(string const &menuLabel);


struct YsFontDir {
  ImFont *tinyFont;
  ImFont *smlFont;
  ImFont *medFont;
  ImFont *lrgFont;
};

extern YsFontDir ysFonts;


ostream & operator <<(ostream &s, ImVec2 const &a);
ostream & operator <<(ostream &s, ImRect const &a);

struct PolylineBuf {
  PolylineBuf();
  ~PolylineBuf();
  vector<ImVec2> pts;

  void reserve(size_t n);

  inline void operator() (float x, float y) {
    pts.emplace_back(x, y);
  }

  inline void operator() (ImVec2 pt) {
    pts.push_back(pt);
  }

  void draw(U32 color, bool closed=false, float width=1.0f);
};


struct PolybandBuf {
  PolybandBuf();
  ~PolybandBuf();
  void reserve(size_t n);

  struct Pt {
    float x, y0, y1;
  };
  vector<Pt> pts;


  void operator() (float x, float y0, float y1) {
    pts.push_back({round(x), y0, y1});
  }
  void fill(U32 color);
};


void drawText(float x, float y, U32 color, string const &text, float hAlign=0.0f, float vAlign=0.0f);

bool drawWinopIcon(float &x, float y, U32 color, char const *label);

void loadPngForFont(ImFont *font, string const &fn, int codePoint);
void blitIconsIntoFont();
ImFont *addFontWithIcons(
  string const &fontFn, string const &iconFn, float size);

SDL_Surface *loadPngAsSurface(string const &fn);

struct YsStyle {
  YsStyle(string const &uiStyle);
  bool isDark{false};

  U32 goodGraphColor(size_t i);
  U32 darkGraphColor(size_t i);
  U32 lightGraphColor(size_t i);

  glm::vec4 goodGraphCol4(size_t i);
  glm::vec4 darkGraphCol4(size_t i);
  glm::vec4 lightGraphCol4(size_t i);

  // Given inline here, but changed in the constructor in dark mode
  U32 gradientCol = IM_COL32(0xcc, 0x5b, 0xfc, 0xff);
  U32 squiggleCol = IM_COL32(0xff, 0x5b, 0xfc, 0xff);
  U32 axisLineCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xff);
  U32 axisLightLineCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xcc);
  U32 axisTicCol = IM_COL32(0x99, 0x99, 0x99, 0xcc);
  U32 axisLabelCol = IM_COL32(0x00, 0x00, 0x00, 0xff);
  U32 axisLightLabelCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xff);
  U32 closeIconCol = IM_COL32(0xcc, 0x00, 0x00, 0xff);
  U32 expandIconCol = IM_COL32(0x00, 0xaa, 0x00, 0xff);
  U32 boringIconCol = IM_COL32(0x00, 0x00, 0x00, 0xff);
  U32 lineNumberCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xff);
  U32 codeTextCol = IM_COL32(0x00, 0x00, 0x00, 0xff);
  U32 debugValueBgCol = IM_COL32(0xff, 0xee, 0x00, 0xcc);
  U32 codeUnderlineCol = IM_COL32(0x00, 0x00, 0xff, 0x88);
  U32 codeStructrefCol = IM_COL32(0x03, 0xbb, 0x81, 0xff);
  U32 codeDirtyCol = IM_COL32(0xff, 0xff, 0x88, 0xff);
  U32 paramAxisCol = IM_COL32(0x1d, 0x9e, 0xdb, 0xff);
  U32 paramNeedleFillCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xff);
  U32 paramNeedleBorderCol = IM_COL32(0x00, 0x00, 0x00, 0xff);
  U32 paramOrigNeedleFillCol = IM_COL32(0xcc, 0xcc, 0xcc, 0x88);
  U32 paramOrigNeedleBorderCol = IM_COL32(0x00, 0x00, 0x00, 0x88);
  U32 paramValueCol = IM_COL32(0x00, 0x00, 0x00, 0xff);

  U32 panelHeaderCol = IM_COL32(0x00, 0x00, 0xcc, 0xff);

  U32 scopeCursorCol = IM_COL32(0xff, 0xcc, 0x99, 0xff);
  U32 scopeCursorLabelCol = IM_COL32(0xb2, 0x8e, 0x6b, 0xff);

  U32 logMessageErrCol = IM_COL32(0xff, 0x00, 0x00, 0xff);
  U32 logMessageLogCol = IM_COL32(0x00, 0x00, 0x00, 0xff);
  U32 logMessageBorderCol = IM_COL32(0xff, 0x88, 0x22, 0xcc);
  
  U32 textLightCol = IM_COL32(0xcc, 0xcc, 0xcc, 0xff);
  U32 textMedCol = IM_COL32(0x88, 0x88, 0x88, 0xff);
  
  U32 textLinkCol = IM_COL32(0x00, 0x00, 0xcc, 0xff);
  U32 textErrCol = IM_COL32(0xff, 0x00, 0x00, 0xff);
  U32 textRedCol = IM_COL32(0xff, 0x00, 0x00, 0xff);
  U32 textGrnCol = IM_COL32(0x00, 0xcc, 0x00, 0xff);
  U32 textBluCol = IM_COL32(0x00, 0x00, 0xff, 0xff);

  U32 scrubShade1 = IM_COL32(0xff, 0xff, 0xff, 0xff);
  U32 scrubShade2 = IM_COL32(0xd3, 0xd4, 0xd1, 0xff);
  U32 scrubShade3 = IM_COL32(0xee, 0xee, 0xee, 0xff);
  U32 scrubGroove1 = IM_COL32(0x57, 0x59, 0x55, 0xff);
  U32 scrubGroove2 = IM_COL32(0x7b, 0x7e, 0x76, 0xff);
  U32 scrubTrough = IM_COL32(0x00, 0x00, 0x00, 0x20);
  U32 scrubKnob1 = IM_COL32(0x68, 0x49, 0x6c, 0xff);
  U32 scrubKnob2 = IM_COL32(0xb7, 0x8c, 0xbe, 0xff);
  U32 scrubKnob3 = IM_COL32(0xa1, 0x6e, 0xa9, 0xff);

  U32 terrainYardstickCol = IM_COL32(0x3b, 0xee, 0xac, 0xcc);

  enum { N_goodGraphColors = 38 };
  U32 _goodGraphColors[N_goodGraphColors];
  U32 _darkGraphColors[N_goodGraphColors];
  U32 _lightGraphColors[N_goodGraphColors];

};

extern YsStyle *yss;
