#pragma once
#include "./scope_model.h"
#include "../jit/sourcefile.h"
#include "../jit/annotation.h"
#include "../remote/client.h"
#include "./ysgui.h"
#include "./policy_search.h"

struct YogaCodeDrawer {
  YogaCodeDrawer(
    ScopeModel &_m,
    YogaCodeTabModel &_tab,
    bool _layoutOnly)
    : m(_m),
      tab(_tab),
      layoutOnly(_layoutOnly)
  {
    auto window = GetCurrentWindow();
    auto &style = GetStyle();
    if (window->SkipItems) return;
    if (!tab.f) return;

    lineHeight = ceil(GetTextLineHeightWithSpacing() * 1.0 + 0.0f);
    fontSize = GetFontSize();

    R totHeight = lineHeight;
    for (auto &it : tab.lines) {
      totHeight += max(lineHeight, it.ascent + it.descent);
    }

    SetNextItemWidth(-1);
    ImVec2 frame_size(CalcItemWidth(), totHeight);

    const ImRect frame_bb(window->DC.CursorPos, window->DC.CursorPos + frame_size);
    const ImRect inner_bb(frame_bb.Min + style.FramePadding, frame_bb.Max - style.FramePadding);
    const ImRect total_bb(frame_bb.Min, frame_bb.Max);
    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, 0, &frame_bb))
        return;

    boxT = inner_bb.Min.y;
    boxR = inner_bb.Max.x;
    boxB = inner_bb.Max.y;
    boxL = inner_bb.Min.x;

    if (0) cerr << "Code: inner_bb=" + repr(inner_bb) + " frame_size=" + repr(frame_size) + "\n";

    textB = boxT + 5.0;
    textL = 0.0;
    textR = 0.0;

    auto flagsSave = window->DrawList->Flags;
    // Something about antialiased lines sucks
    //window->DrawList->Flags &= ~ImDrawListFlags_AntiAliasedLines;

    drawAll();

    window->DrawList->Flags = flagsSave;
    if (m.liveClient) {
      m.liveClient->syncYogaParams();
    }
  }

  ScopeModel &m;
  YogaCodeTabModel &tab;
  bool layoutOnly;

  R boxT, boxR, boxB, boxL;
  R contentH;

  R lineHeight{0.0};
  R fontSize{0.0};
  R textX{0.0}, textY{0.0};
  R textT{0.0}, textR{0.0}, textB{0.0}, textL{0.0};
  R debugTextX{0.0};
  int linei{0};

  void drawAll()
  {
    auto window = GetCurrentWindow();

    char const *textBegin = tab.f->text.c_str();
    char const *p = textBegin;
    auto &annos = m.ctx.reg->annosByFile[tab.f];
    auto annoPtr = annos.begin();
    auto annoEnd = annos.end();

    bool trySetInitialScroll = layoutOnly && !window->WasActive && !tab.debugLocalsSorted.empty();

    if (0) cerr << "layoutOnly=" + repr(layoutOnly) + " trySet=" + repr(trySetInitialScroll) + "\n";

    auto dlPtr = tab.debugLocalsSorted.begin();
    auto dlEnd = tab.debugLocalsSorted.end();

    if (0) cerr << "drawAll annos=" + to_string(annos.size()) + "\n";

    linei = 1;
    int firstInterestingPos = -1;
    while (1) {

      if (!startLine()) {
        while (*p && *p != '\n') {
          p++;
        }
        if (*p == 0) {
          break;
        }
        else if (*p == '\n') {
          p++;
        }
        endLine();
        continue;
      }

      drawLineNumber(to_string(linei) + " ");

      int indent = 0;
      while (*p == ' ') {
        indent++;
        p++;
      }
      if (indent > 0) {
        textX += indent * 0.8 * GetFontSize();
      }

      string chunk;
      AnnoBase *anno{nullptr};
      vector<YogaDebugLocalsEntry *> lineDls;
      auto flush = [this, &chunk, &anno]() {
        if (!chunk.empty()) {
          drawAnno(chunk, anno);
          chunk.clear();
        }
      };
      
      while (*p && *p != '\n') {
        int pos = p - textBegin;
        if (!anno) {
          while (annoPtr != annoEnd && pos > (*annoPtr)->sourceLoc.start) {
            annoPtr++;
          }
          if (annoPtr != annoEnd && pos == (*annoPtr)->sourceLoc.start) {
            flush();
            anno = *annoPtr;
            annoPtr++;
            continue;
          }
        }
        else {
          if (pos == anno->sourceLoc.end) {
            flush();
            anno = nullptr;
            continue;
          }
        }

        while (dlPtr != dlEnd && pos > dlPtr->sourceLoc.start) {
          dlPtr++;
        }
        if (dlPtr != dlEnd && pos == dlPtr->sourceLoc.start) {
          lineDls.push_back(&*dlPtr);
          dlPtr++;
          continue;
        }
        chunk += *p++;
      }
      flush();

      for (auto dli=lineDls.rbegin(); dli != lineDls.rend(); dli++) {
        drawDebugLocals(*dli);
      }

      endLine();

      if (trySetInitialScroll && firstInterestingPos == -1 && !lineDls.empty()) {
        firstInterestingPos = GetCursorPosY();
        uvUiActive = true; // have to redraw after this
      }

      if (*p == 0) {
        break;
      }
      else if (*p == '\n') {
        p++;
      }
    }

    if (trySetInitialScroll && firstInterestingPos != -1) {
      auto newScrollY = max(0.0f, firstInterestingPos - 80.0f);
      SetScrollFromPosY(newScrollY);
    }
  }

  bool startLine()
  {
    textX = boxL + 20.0;
    auto &li = tab.lines[linei];
    auto ah = max(li.ascent, ceil(lineHeight * 0.00));
    auto dh = max(li.descent, ceil(lineHeight * 1.00));
    textY = textB + ah;
    textT = textY - ah;
    textB = textY + dh;
    debugTextX = boxR - 10.0;
    SetCursorPosY(textY - boxT);
    return layoutOnly || !IsClippedEx(ImRect(boxL, textT, boxR, textB), 0, false);
  }

  void endLine()
  {
    auto &li = tab.lines[linei];
    li.ascent = max(li.ascent, textY - textT);
    li.descent = max(li.descent, textB - textY);
    if (0) cerr << "End T="s + repr(textT) + " Y=" + repr(textY) + " B=" + repr(textB) + " ascent="s + repr(li.ascent) + " descent=" + repr(li.descent) + "\n";
    linei++;
  }

  R getIndent()
  {
    return textX - boxL;
  }

  void drawLineNumber(string const &s)
  {
    auto width = CalcTextSize(s.c_str());

    if (!layoutOnly) {
     GetWindowDrawList()->AddText(ImVec2(textX - width.x - 4.0, textY), yss->lineNumberCol, s.c_str());
    }
  }

  void setupText(string const &s)
  {
    textL = textX;
    auto sz = CalcTextSize(s.c_str());
    textX += sz.x;
    textR = textX;
  }

  void setupDebugText(string const &s)
  {
    textR = debugTextX;
    auto sz = CalcTextSize(s.c_str());
    debugTextX -= sz.x;
    textL = debugTextX;
  }

  void drawText(string const &s, U32 color)
  {
    if (!layoutOnly) {
      GetWindowDrawList()->AddText(ImVec2(textL, textY), color, s.c_str());
      if (0) {
        GetWindowDrawList()->AddRect(ImVec2(textL, textT), ImVec2(textR, textB), IM_COL32(0xff, 0x00, 0x00, 0x44));
      }
    }
  }

  void drawTextBox(U32 color)
  {
    if (!layoutOnly) {
      GetWindowDrawList()->AddRectFilled(ImVec2(textL-1, textB+1), ImVec2(textR+1, textT-1), color);
    }
  }

  void drawUnderline(U32 color)
  {
    if (!layoutOnly) {
      GetWindowDrawList()->AddLine(ImVec2(textL, textB-0.5), ImVec2(textR, textB-0.5), color);
    }
  }

  void doPlainText(string const &s)
  {
    setupText(s);
    drawText(s, yss->codeTextCol);
  }

  void drawDebugLocals(YogaDebugLocalsEntry *dl)
  {
    auto alt = m.debugAlt();
    if (!alt.trace) return;
    auto seq = alt.trace->getTimeseq(dl->valueRef.seqName);
    YogaValue seqValue = seq->getEqualBefore(m.visTime);
    if (!seqValue.isValid()) return;

    //YogaPtrAccessor acc(seq->type, dl->valueRef.subName);
    YogaValue value = dl->valueAcc.ptr(seqValue);

    R modulation = 1.0; // FIXME
    auto modClamp = clamp(modulation, 0.0, 1.0);
    auto modStr = "@ "s + (modulation == 1.0 ? "1" : 
      (modulation == 0.0 ? "0" : repr_0_3f(modulation)));
    
    auto modColor = ImColor(
      linearComb(modClamp, yss->isDark ? 1.0f : 0.0f, 1.0-modClamp, yss->isDark ? 1.0f : 0.8f),
      linearComb(modClamp, yss->isDark ? 1.0f : 0.0f, 1.0-modClamp, yss->isDark ? 0.6f : 0.0f),
      linearComb(modClamp, yss->isDark ? 1.0f : 0.0f, 1.0-modClamp, yss->isDark ? 0.6f : 0.0f),
      1.0f);

    auto valStr = repr_0_3f(value);
    auto valSz = CalcTextSize(valStr.c_str());

    if (modulation != 0.0 && modulation != 1.0) {
      auto modSz = CalcTextSize("@ 0.999");
      debugTextX -= fontSize * 0.4;
      textR = debugTextX;
      debugTextX -= modSz.x;
      textL = debugTextX;
      drawText(modStr, (ImU32)modColor);
    }

    debugTextX -= fontSize * 2.0;
    textR = debugTextX;
    debugTextX -= valSz.x;
    textL = debugTextX;

    bool hovered, held;
    if (debugLocalsBehavior(dl, hovered, held)) {
      m.addTrace(dl->valueRef.fullName);
    }
    else if (hovered) {
      beginAnnoTooltip();
      Text("%s", (dl->valueRef.fullName + " = " + valStr).c_str());
      endAnnoTooltip();
    }

    if (modulation == 1.0) {
      drawTextBox(yss->debugValueBgCol);
    }
    drawText(valStr, (ImU32)modColor);
  }

  void beginAnnoTooltip()
  {
    PushStyleColor(ImGuiCol_PopupBg, IM_COL32(0xff, 0xff, 0xa0, 0xbb));
    BeginTooltip();
  }

  void endAnnoTooltip()
  {
    EndTooltip();
    PopStyleColor();
  }

  void drawAnno(string const &s, AnnoBase *anno)
  {
    if (anno) {
      if (auto anno1 = anno->asStructref()) drawAnnoStructref(s, anno1);
      else if (auto anno1 = anno->asCall()) drawAnnoCall(s, anno1);
      else if (auto anno1 = anno->asParam()) drawAnnoParam(s, anno1);
      else if (auto anno1 = anno->asTrain()) drawAnnoTrain(s, anno1);
      else drawAnnoNone(s, anno1);
    }
    else {
      drawAnnoNone(s, anno);
    }
  }

  bool annoBehavior(AnnoBase *anno, bool &hovered, bool &held)
  {
    if (!anno) return false;
    int id = GetID((void *)anno);
    return ButtonBehavior(ImRect(textL, textT, textR, textB), id, &hovered, &held);
  }

  bool debugLocalsBehavior(YogaDebugLocalsEntry *dl, bool &hovered, bool &held)
  {
    if (!dl) return false;
    int id = GetID((void *)dl);
    return ButtonBehavior(ImRect(textL, textT, textR, textB), id, &hovered, &held);
  }


  bool dragBehavior(R &normValue, bool &hovered, bool &held, bool &popup)
  {
    auto window = GetCurrentWindow();
    auto &style = GetStyle();
    auto &io = GetIO();
    auto &g = *GImGui;
    hovered = held = popup = false;
    auto id = GetID("drag");

    ImRect total_bb(textL, textT, textR, textB);
    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id)) return false;

    hovered = ItemHoverable(total_bb, id);
    bool temp_input_is_active = TempInputIsActive(id);
    bool temp_input_start = false;
    bool is_popup_open = IsPopupOpen(id, 0);
    if (!temp_input_is_active && !is_popup_open) {
      const bool focus_requested = FocusableItemRegister(window, id);
      bool is_ctrl = io.KeyAlt;
      const bool clicked = (hovered && io.MouseClicked[0] && !is_ctrl);
      const bool double_clicked = (hovered && io.MouseDoubleClicked[0] && !is_ctrl);
      const bool ctrl_clicked = (hovered && io.MouseClicked[0] && is_ctrl);
      if (ctrl_clicked) {
        popup = true;
      }
      else if (focus_requested || clicked || double_clicked || g.NavActivateId == id || g.NavInputId == id) {
        SetActiveID(id, window);
        SetFocusID(id, window);
        FocusWindow(window);
        // FIXME g.ActiveIdAllowNavDirFlags = (1 << ImGuiDir_Up) | (1 << ImGuiDir_Down);
        if (focus_requested || (clicked && io.KeyCtrl) || double_clicked || g.NavInputId == id) {
          temp_input_start = true;
          FocusableItemUnregister(window);
        }
      }
    }

    held = g.ActiveId == id;

    R vMin = -2.0, vMax = 2.0;
    if (DragBehavior(id, ImGuiDataType_Double, &normValue, 0.0, &vMin, &vMax, "%0.6f", 1.0, ImGuiDragFlags_None)) {
      MarkItemEdited(id);
      return true;
    }
    return false;
  }


  bool clickPopupBehavior(bool &hovered)
  {
    auto &style = GetStyle();
    auto id = GetID("menu");
    auto &io = GetIO();

    ImRect total_bb(textL, textT, textR, textB);
    ItemSize(total_bb, style.FramePadding.y);
    if (!ItemAdd(total_bb, id)) return false;

    hovered = ItemHoverable(total_bb, id);
    bool clicked = (hovered && io.MouseClicked[0]);
    bool is_popup_open = IsPopupOpen(id, 0);
    if (clicked && !is_popup_open) {
      auto menuFlags = ImGuiSelectableFlags_NoHoldingActiveID | ImGuiSelectableFlags_SelectOnClick | ImGuiSelectableFlags_SpanAvailWidth;
      OpenPopup("popup", menuFlags);
    }

    return BeginPopup("popup");
  }


  void drawAnnoNone(string const &s, AnnoBase *anno)
  {
    setupText(s);
    drawText(s, yss->codeTextCol);
  }

  void drawAnnoCall(string const &s, AnnoCall *anno)
  {
    setupText(s);
    bool hovered, held;
    if (annoBehavior(anno, hovered, held)) {
      cerr << "Clicked " + repr(anno->sourceLoc) + "\n";
      tab.navigateToFunction(anno->funcName);
    }
    else if (hovered) {
      beginAnnoTooltip();
      Text("Look into function");
      endAnnoTooltip();
    }
    if (held) {
      drawUnderline(yss->codeUnderlineCol);
    }
    drawText(s, yss->codeTextCol);
  }

  void drawAnnoStructref(string const &s, AnnoStructref *anno)
  {
    setupText(s);
    bool hovered, held;
    if (annoBehavior(anno, hovered, held)) {
      cerr << "Clicked "s + anno->repr() + "\n";
      //m.addTrace(anno->refName); // FIXME: need context
    }
    else if (hovered) {
      beginAnnoTooltip();
      Text("%s", ("Graph "s + anno->funcName + " " + anno->refName).c_str());
      endAnnoTooltip();
    }
    drawText(s, yss->codeStructrefCol);
    if (held) {
      drawUnderline(yss->codeUnderlineCol);
    }
  }
  
  void drawAnnoParam(string const &s, AnnoParam *anno)
  {
    if (0) cerr << "annoParam t="s + repr(textT) + " y=" + repr(textY) + "\n";
    textT = min(textT, textY-6.0);
    textB = max(textB, textY + ceil(lineHeight * 1.00) +6.0);
    PushID((void *)anno);
    if (anno->param->paramType->isReal()) {
      YogaVisNumber(*this, anno);
    }
    else if (anno->param->isExtern) {
      drawAnnoParamExtern(s, anno);
    }
    else {
      setupText(s);
      drawText(s, yss->codeTextCol);
    }
    PopID();
  }

  void drawAnnoParamExtern(string const &s, AnnoParam *anno)
  {
    bool hovered = false;
    setupText(s);
    if (clickPopupBehavior(hovered)) {
      if (MenuItem("Randomize", nullptr)) {
        anno->param->randomize(m.ctx);
      }
      EndPopup();
    }
    if (hovered) {
      beginAnnoTooltip();
      Text("%s", (anno->param->paramName).c_str());
      if (anno->param->isValueChanged(m.ctx)) {
        Text("%s", ("norm(value - saved) = "s + repr_0_3f(anno->param->normChangeFromSaved(m.ctx))).c_str());
      }
      Text("%s", repr_0_3f(anno->param->getYogaValue(m.ctx)).c_str());
      endAnnoTooltip();
    }

    drawText(s, yss->codeStructrefCol);
  }

  void drawAnnoTrain(string const &s, AnnoTrain *anno)
  {
    setupText(s);
    bool hovered = false;
    if (clickPopupBehavior(hovered)) {
      if (MenuItem("Train by minimizing this loss function", nullptr)) {
        m.desiredTrainDef = anno->call;
      }
      EndPopup();
    }

    drawText(s, yss->codeStructrefCol);
  }

  struct YogaVisNumber {
    YogaVisNumber(YogaCodeDrawer &_d, AnnoParam *_anno)
    : d(_d), 
      anno(_anno)
    {
      auto param1dGraphWidth = GetFontSize() * 10.0;
      d.textL = d.textX;
      d.textX += param1dGraphWidth + 2;
      d.textR = d.textX;

      Layout lo;
      lo.param1dGraphWidth = param1dGraphWidth;
      lo.iconScale = 1.0;
      lo.lineL = snap5(d.textL + 1);
      lo.centerX = snap5(lo.lineL + 0.5 * param1dGraphWidth);
      lo.lineR = snap5(lo.lineL + param1dGraphWidth);
      lo.lineY = snap5(d.textY + d.lineHeight * 0.75 + 1);
      lo.grHeight = d.lineHeight;
      lo.lineHeight = d.lineHeight;

      if (!d.layoutOnly) {
        fillLayout(lo);
        draw(lo);
      }
    }

    YogaCodeDrawer &d;
    AnnoParam *anno;

    struct Layout {
      R lineL{0.0}, centerX{0.0}, lineR{0.0}, lineY{0.0};
      R param1dGraphWidth;
      R xScale;
      R needleX;
      R needleY;
      R iconScale;
      R grHeight;
      R lineHeight;
    };

    void fillLayout(Layout &lo)
    {
      auto normValue = anno->param->getNormValue(d.m.ctx);
      lo.xScale = lo.param1dGraphWidth / 4.0;
      lo.needleX = snap5(lo.centerX + normValue * lo.xScale);
      lo.needleY = lo.lineY - 1;
    }

    bool isDirty()
    {
      return anno->param->getValue(d.m.ctx) != d.m.ctx.reg->savedParamValues[anno->param->paramIndex];
    }

    void draw(Layout const &lo)
    {
      if (isDirty()) {
        drawDirty(lo);
      }
      drawAxis(lo);
      drawPolicyVariation(lo);
      drawNeedle(lo);
      drawGrad(lo);
      drawSquiggle(lo, d.m.policySquiggleMap.get());
      drawValue(lo);
      drawMenu(lo);
    }

    void drawMenu(Layout const &lo)
    {
      bool hovered{false}, held{false}, popup{false};
      R normValue = anno->param->getNormValue(d.m.ctx);
      if (d.dragBehavior(normValue, hovered, held, popup)) {
        if (0) cerr << "Drag normValue=" + repr(normValue) + " (was " + repr(anno->param->getNormValue(d.m.ctx)) + ")\n";
        anno->param->setNormValue(d.m.ctx, normValue);
      }
      auto popupId = GetID("popup");
      if (popup) {
        OpenPopupEx(popupId);
      }
      if (BeginPopupEx(popupId, ImGuiWindowFlags_AlwaysAutoResize|ImGuiWindowFlags_NoTitleBar|ImGuiWindowFlags_NoSavedSettings)) {
        bool participating = d.m.desiredPolicySearchSpec.participatingParams.count(anno->param);
        if (MenuItem("Searchable", nullptr, &participating)) {
          if (participating) {
            d.m.desiredPolicySearchSpec.participatingParams.insert(anno->param);
          }
          else {
            d.m.desiredPolicySearchSpec.participatingParams.erase(anno->param);
          }
        }
        if (MenuItem("Revert", nullptr, false, isDirty())) {
          anno->param->setValue(d.m.ctx, d.m.ctx.reg->savedParamValues.at(anno->param->paramIndex));
        }

        for (auto scale : {0.1, 0.25, 0.5}) {
          if (MenuItem(("Make policy terrain X +- "s + repr(scale)).c_str())) {
            d.m.desiredPolicyTerrainSpec.xVar.paramName = anno->param->paramName;
            d.m.desiredPolicyTerrainSpec.xVar.paramRange = scale;
            d.m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
          }
        }
        for (auto scale : {0.1, 0.25, 0.5}) {
          if (MenuItem(("Make policy terrain Y +- "s + repr(scale)).c_str())) {
            d.m.desiredPolicyTerrainSpec.yVar.paramName = anno->param->paramName;
            d.m.desiredPolicyTerrainSpec.yVar.paramRange = scale;
            d.m.desiredPolicyTerrainSpec.policyTerrainEpoch ++;
          }
        }
        if (MenuItem("Disable policy squiggles", nullptr, false, d.m.desiredPolicySquiggleSpec.isValid())) {
          d.m.desiredPolicySquiggleSpec.rMetric = PolicyRewardMetric();
          d.m.desiredPolicySquiggleSpec.policySquiggleEpoch ++;
        }
        if (MenuItem("Increase range")) {
          anno->param->scaleUp(d.m.ctx);
        }
        if (MenuItem("Decrease range")) {
          anno->param->scaleDn(d.m.ctx);
        }
        EndPopup();
      }

      if (hovered && !IsPopupOpen(popupId, 0)) {
        d.beginAnnoTooltip();
        drawTooltip();
        d.endAnnoTooltip();
      }
    }

    void drawPolicyVariation(Layout const &lo)
    {
      auto &pts = d.m.desiredPolicyTerrainSpec;
      if (anno->param->paramName == pts.xVar.paramName) {
        drawPolicyVariation1(lo, anno->param->getNormValue(d.m.ctx), pts.xVar.paramRange, IM_COL32(0xcc, 0x00, 0x00, 0x99),
          "Terrain X");
      }
      if (anno->param->paramName == pts.yVar.paramName) {
        drawPolicyVariation1(lo, anno->param->getNormValue(d.m.ctx), pts.yVar.paramRange, IM_COL32(0x00, 0xcc, 0x00, 0x99),
          "Terrain Y");
      }
      if (d.m.desiredPolicySearchSpec.participatingParams.count(anno->param)) {
        drawPolicyVariation1(lo, 0.0, 2.0, IM_COL32(0x66, 0x66, 0xff, 0x77),
          "Search");
      }
    }

    void drawPolicyVariation1(Layout const &lo, R paramBase, R paramScale, U32 col, string const &label)
    {
      R ptMinX = snap5(lo.centerX + (paramBase - paramScale) * lo.xScale);
      R ptMaxX = snap5(lo.centerX + (paramBase + paramScale) * lo.xScale);

      auto y0 = lo.lineY - 0.8*lo.grHeight - 6;
      auto y1 = y0 + 5.5;
      auto y2 = lo.lineY;
      GetWindowDrawList()->AddLine(
        ImVec2(ptMaxX, y0),
        ImVec2(ptMaxX, y2),
        col, 2.0f);
      GetWindowDrawList()->AddLine(
        ImVec2(ptMinX, y0),
        ImVec2(ptMinX, y2),
        col, 2.0f);

      array pts {
        ImVec2(ptMinX, y1),
        ImVec2(ptMinX, y0+1),
        ImVec2(ptMinX+1, y0),
        ImVec2(ptMaxX-1, y0),
        ImVec2(ptMaxX, y0+1),
        ImVec2(ptMaxX, y1)
      };
      GetWindowDrawList()->AddConvexPolyFilled(pts.data(), pts.size(), col);

      if (!label.empty() && lo.grHeight > lo.lineHeight) {
        ::drawText(lo.needleX, y0, yss->axisLabelCol, label, 0.5f, 1.0f);
      }
    }
    
    void drawDirty(Layout const &lo)
    {
      GetWindowDrawList()->AddRectFilled(
        ImVec2(lo.lineL, lo.lineY-5),
        ImVec2(lo.lineR, lo.lineY-0.5),
        yss->codeDirtyCol);

      if (1) {
        auto origNormValue = anno->param->normalizeValue(d.m.ctx.reg->savedParamValues[anno->param->paramIndex]);
        auto origNeedleX = snap5(lo.centerX + origNormValue * lo.xScale);

        array path {
          ImVec2(origNeedleX, lo.needleY),
          ImVec2(origNeedleX - 3.0*lo.iconScale, lo.needleY - 4.0*lo.iconScale),
          ImVec2(origNeedleX + 3.0*lo.iconScale, lo.needleY - 4.0*lo.iconScale)
        };
        GetWindowDrawList()->AddConvexPolyFilled(
          &path[0], path.size(),
          yss->paramOrigNeedleFillCol);
        GetWindowDrawList()->AddPolyline(
          &path[0], path.size(),
          yss->paramOrigNeedleBorderCol, true, 1.0f);
      }
    }

    void drawAxis(Layout const &lo)
    {
      GetWindowDrawList()->AddLine(
        ImVec2(lo.lineL, lo.lineY),
        ImVec2(lo.lineR, lo.lineY),
        yss->paramAxisCol, 1.0f);
      GetWindowDrawList()->AddLine(
        ImVec2(lo.centerX, lo.lineY - 2),
        ImVec2(lo.centerX, lo.lineY + 2),
        yss->paramAxisCol, 1.0f);
    }

    void drawNeedle(Layout const &lo)
    {
      array path {
        ImVec2(lo.needleX, lo.needleY),
        ImVec2(lo.needleX - 3.0*lo.iconScale, lo.needleY - 4.0*lo.iconScale),
        ImVec2(lo.needleX + 3.0*lo.iconScale, lo.needleY - 4.0*lo.iconScale)
      };
      GetWindowDrawList()->AddConvexPolyFilled(
        &path[0], path.size(),
        yss->paramNeedleFillCol);
      GetWindowDrawList()->AddPolyline(
        &path[0], path.size(),
        yss->paramNeedleBorderCol, true, 1.0f);

      if (auto pss = d.m.policySearchState) {
        if (pss->particles.size() > 0) {        
          auto y0 = lo.lineY - 0.8*lo.grHeight;
          auto y2 = lo.lineY;
          for (size_t pi = 0; pi < pss->activeParams.size(); pi++) {
            if (pss->activeParams[pi] == anno->param) {
              for (auto const &particle : pss->particles) {
                auto nv = particle->paramValues[pi];
                auto x = snap5(lo.centerX + nv * lo.xScale);
                GetWindowDrawList()->AddLine(
                  ImVec2(x, y0),
                  ImVec2(x, y2),
                  IM_COL32(0x66, 0x66, 0xff, 0x77), 1.0f);
              }
            }
          }
        }
      }
    }

    void drawGrad(Layout const &lo)
    {
      if (d.m.paramTrace && d.m.paramTrace->grads) {
        auto normGrad = anno->param->getNormGrad(d.m.ctx, d.m.paramTrace->grads.get());
        if (normGrad != 0.0) {
          R dir = (normGrad > 0.0) ? 1.0 : -1.0;
          R tipX = lo.needleX + dir*4.0 + compressPower(normGrad, 5) * lo.xScale;

          GetWindowDrawList()->AddLine(
            ImVec2(lo.needleX, lo.lineY - 1.0),
            ImVec2(tipX, lo.lineY - 1.0),
            yss->gradientCol, 2.0f);

          GetWindowDrawList()->AddLine(
            ImVec2(tipX, lo.lineY - 1.0),
            ImVec2(tipX - dir*4.0, lo.lineY - 5.5),
            yss->gradientCol, 2.0f);
        }
      }
    }

    void drawValue(Layout const &lo)
    {
      auto paramValue = anno->param->getValue(d.m.ctx);
      auto label = fmteng(paramValue, 3);
      auto sz = CalcTextSize(label.c_str());
      ImVec2 pt(lo.needleX - 0.5 * sz.x, lo.needleY - 3.0*lo.iconScale - 0.85*lo.lineHeight);
      GetWindowDrawList()->AddText(pt, yss->paramValueCol, label.c_str());
    }

    void drawSquiggle(Layout const &lo, PolicySquiggleMap *psm)
    {
      if (psm) {
        auto varIndex = psm->varsByParamIndex[anno->param->paramIndex];
        if (varIndex == (size_t)-1) return;
        auto vb = psm->varBounds.at(varIndex).get();

        PolylineBuf path;

        R axisY = lo.lineY;
        R scaleY = -0.5 * lo.grHeight;
        R terrainDatum = d.m.getTerrainDatum(psm->spec.rMetric);
        for (size_t xi = 0; xi < vb->values.size(); xi++) {
          auto val = psm->rat(varIndex, xi);
          auto clampedVal = softclamp((val - terrainDatum) / psm->spec.rMetric.rScale);

          R ptX = snap5(lo.centerX + (vb->paramBase + vb->values[xi] * vb->paramRange) * lo.xScale);
          R ptY = axisY + scaleY * clampedVal;
          path(ptX, ptY);
        }

        path.draw(yss->squiggleCol, false, 1.5f);
      }
    }

    void drawTooltip()
    {
      string desc = anno->param->getDesc(d.m.ctx, (d.m.paramTrace && d.m.paramTrace->grads) ? d.m.paramTrace->grads.get() : nullptr);
      Text("%s", desc.c_str());
      if (d.m.desiredPolicySquiggleSpec.rMetric.isValid()) {
        if (BeginChild("squiggle", ImVec2(20.0 * d.lineHeight, 9.0 * d.lineHeight))) {

          if (auto psm = d.m.getSquiggleMapForParam(anno->param, 2)) {

            auto wp = GetWindowPos();
            auto wh = GetWindowHeight();
            auto ww = GetWindowWidth();

            Layout lo;
            lo.param1dGraphWidth = ww;
            lo.iconScale = 2.0;
            lo.lineL = wp.x;
            lo.centerX = wp.x + ww/2;
            lo.lineR = wp.x + ww;
            lo.lineY = wp.y + 0.6 * wh;
            lo.grHeight = 0.5 * wh;
            lo.lineHeight = d.lineHeight;
            fillLayout(lo);
            
            drawAxis(lo);
            drawPolicyVariation(lo);
            drawNeedle(lo);
            drawGrad(lo);
            drawSquiggle(lo, psm);
            drawValue(lo);
          }
        }
        EndChild();
      }
    }

  };
};

