#pragma once
#include "./twibus.h"
#include "../../dspcore/dspcore.h"

typedef struct ad7416_engine {
  twibus_engine super; // structural inheritance -- must be first member
  int chipno;
  int state;
  dsp824 temperature;
} ad7416_engine;

void ad7416_engine_create(ad7416_engine *it, twibus *bus, int chipno);
