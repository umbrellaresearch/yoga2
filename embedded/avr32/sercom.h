#pragma once

typedef struct sercom sercom;
sercom *host_sercom;

#include "../embedded_pktcom.h"

void sercom_tx_u8(volatile sercom *it, U8 data);
void sercom_tx_needspace(volatile sercom *it);
Bool sercom_tx_empty(volatile sercom *it);
int sercom_tx_qlen(volatile sercom *it);
int sercom_tx_qspace(volatile sercom *it);
void sercom_tx_drain(volatile sercom *it);
void sercom_tx_str(volatile sercom *it, char const *s);
void sercom_tx_u8_hex(volatile sercom *it, U8 x);
void sercom_tx_u16_hex(volatile sercom *it, U16 x);
void sercom_tx_u32_hex(volatile sercom *it, U32 x);
void sercom_tx_udec(volatile sercom *it, U32 x);
void sercom_tx_dec(volatile sercom *it, S32 x);
void sercom_tx_s8(volatile sercom *it, S8 x);
void sercom_tx_u16(volatile sercom *it, U16 x);
void sercom_tx_s16(volatile sercom *it, S16 x);
void sercom_tx_u32(volatile sercom *it, U32 x);
void sercom_panic_write(volatile sercom *it, char const *s);

U8 sercom_rx_u8(volatile sercom *it);
U16 sercom_rx_u16(volatile sercom *it);
U8 sercom_rx_peek(volatile sercom *it);
U8 sercom_rx_ahead(volatile sercom *it, int offset);
void sercom_rx_consume(volatile sercom *it, int len);
Bool sercom_rx_empty(volatile sercom *it);
int sercom_rx_qlen(volatile sercom *it);

#if defined(SERCOM_USE_PKT)
pkt_rx_buf *sercom_rx_pkt(volatile sercom *it);
Bool sercom_tx_pkt_timeout(volatile sercom *it, pkt_tx_buf *tx, int timeout);
Bool sercom_tx_pkt_nowait(volatile sercom *it, pkt_tx_buf *tx);
void sercom_tx_pkt(volatile sercom *it, pkt_tx_buf *tx);
#endif

void sercom_set_comm(volatile sercom *it);
void sercom_report_errors(volatile sercom *it);
sercom *sercom_create(int usarti, int baudrate);

void sercom_log_status(sercom *it, char const *name);

void sercom_poll(sercom *it);

void sercom_setup();
