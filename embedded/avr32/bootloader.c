#include "common/std_headers.h"
#include "hwdefs.h"
#include "../embedded_hostif.h"
#include "../embedded_pktcom.h"
#include "../embedded_timing.h"
#include "../embedded_debug.h"

#include "../avr32/sercom.h"
#include "../avr32/ethcom.h"

#include "FLASHC/flashc.h"
#include "WDT/wdt.h"
#include "MACB/macb.h"
#include "GPIO/gpio.h"
#include "./avrhw.h"


// https://tools.ietf.org/html/rfc1350

pkt_endpoint boothost_ep;

void tftp_send_rrq(char const *filename)
{
  debug_printf("bootloader: request %s from %d.%d.%d.%d:%d",
               filename, boothost_ep.ip4_addr[0], boothost_ep.ip4_addr[1], boothost_ep.ip4_addr[2], boothost_ep.ip4_addr[3], boothost_ep.udp_port);
  pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, &boothost_ep);
  if (!tx) {
    if (1) log_printf("tftp_send_rrq: no tx buf");
    return;
  }
  pkt_tx_netu16(tx, 1); // RRQ opcode
  pkt_tx_str(tx, filename);
  pkt_tx_u8(tx, 0); // null terminate
  pkt_tx_str(tx, "octet");
  pkt_tx_u8(tx, 0); // null terminate
  ethcom_tx_pkt(&ethcom0, tx);
}

void tftp_send_ack(pkt_endpoint *dst, U16 blockno)
{
  pkt_tx_buf *tx = ethcom_start_tx_udp(&ethcom0, dst);
  if (!tx) {
    if (1) log_printf("tftp_send_ack: no tx buf");
    return;
  }
  pkt_tx_netu16(tx, 4); // ACK opcode
  pkt_tx_netu16(tx, blockno); // ACK opcode
  ethcom_tx_pkt(&ethcom0, tx);
}

bool flash_compare(U32 offset, U8 *p, U32 size)
{
  return memcmp(AVR32_FLASH + BOOTLOADER_SIZE + offset, p, size) == 0;
}

void flash_write(U32 offset, U8 *p, U32 size)
{
  flashc_memcpy(AVR32_FLASH + BOOTLOADER_SIZE + offset, p, size, FALSE);
}

void flash_erase()
{
  flashc_lock_all_regions(FALSE);
  flashc_erase_all_pages(FALSE);
}


int main(void)
{
  wdt_disable();
  avrhw_setup();
  sercom_setup();
  sercom_tx_str(host_sercom, "\n\n" "bootloader 0.1" "\n");
  wdt_clear();
  avrhw_report_rcause();
  wdt_clear();

  pkt_setup();

  debug_printf("ethcom...");
  ethcom_init(&ethcom0);

  ethcom0.verbose = 0;


  U32 prot_size = flashc_get_bootloader_protected_size();
  debug_printf("bootloader: flash protected size = %08x", prot_size);
  flashc_set_bootloader_protected_size(BOOTLOADER_SIZE);
  prot_size = flashc_get_bootloader_protected_size();
  debug_printf("bootloader: new flash protected size = %08x", prot_size);


  int tftp_state = 0;
  U32 t0 = get_ticks();
  bool mismatch = false;
  while (1) {
    ethcom_poll(&ethcom0);
    pkt_rx_buf *rx = NULL;

    if (tftp_state == 0) { // send arp request
      boothost_ep.ip4_addr[0] = ETHERNET_CONF_BOOTIP0;
      boothost_ep.ip4_addr[1] = ETHERNET_CONF_BOOTIP1;
      boothost_ep.ip4_addr[2] = ETHERNET_CONF_BOOTIP2;
      boothost_ep.ip4_addr[3] = ETHERNET_CONF_BOOTIP3;
      boothost_ep.udp_port = 69;
      debug_printf("bootloader: send arp %d.%d.%d.%d",
        boothost_ep.ip4_addr[0], boothost_ep.ip4_addr[1], boothost_ep.ip4_addr[2], boothost_ep.ip4_addr[3]);
      ethcom_send_arp(&ethcom0, &boothost_ep);
      t0 = get_ticks();
      tftp_state = 1;
    }
    else if (tftp_state == 1) { // wait for arp reply
      if (boothost_ep.mac_addr[0] != 0 || boothost_ep.mac_addr[1] != 0 || boothost_ep.mac_addr[2] != 0 ||
          boothost_ep.mac_addr[3] != 0 || boothost_ep.mac_addr[4] != 0 || boothost_ep.mac_addr[5] != 0 ) {
        debug_printf("bootloader: got arp %02x:%02x:%02x:%02x:%02x:%02x",
          boothost_ep.mac_addr[0], boothost_ep.mac_addr[1], boothost_ep.mac_addr[2],
          boothost_ep.mac_addr[3], boothost_ep.mac_addr[4], boothost_ep.mac_addr[5]);
          tftp_state = 2;
      }
      else if (get_ticks() - t0 > SECONDSTOTICKS(1.0)) {
        tftp_state = 0;
      }
    }
    else if (tftp_state == 2) { // request bootimage
      tftp_send_rrq(BOOTLOAD_FILE);
      t0 = get_ticks();
      if (mismatch) {
        tftp_state = 6;
      }
      else {
        tftp_state = 3;
      }
    }
    else if (tftp_state == 3 || // downloading file, compare against flash
             tftp_state == 6) { // downloading file, program flash
      rx = ethcom_rx_pkt(&ethcom0);

      if (rx && pkt_rx_remaining(rx) >= 4) {
        U16 rx_opcode = pkt_rx_netu16(rx);
        if (rx_opcode == 3) { // data packet
          U16 blockno = pkt_rx_netu16(rx);
          if (blockno > 0) {
            int datasize = pkt_rx_remaining(rx);
            U32 offset = (U32)(blockno-1)*512;

            if (tftp_state == 3) {
              if (!flash_compare(offset, &rx->buf[rx->rdpos], datasize)) {
                debug_printf("bootloader: flash compare %08x %d different", offset, datasize);
                mismatch = true;
              } else {
                debug_printf("bootloader: flash compare %08x %d same", offset, datasize);
              }
            }
            else if (tftp_state == 6) {
              debug_printf("bootloader: flash program %08x (to %08x) %d", offset, AVR32_FLASH + BOOTLOADER_SIZE + offset, datasize);
              flash_write(offset, &rx->buf[rx->rdpos], datasize);
            }

            tftp_send_ack(&rx->src, blockno);
            if (datasize < 512) {
              debug_printf("bootloader: got complete file");
              ethcom_drain(&ethcom0);
              sercom_tx_drain(host_sercom);
              if (tftp_state == 3) {
                if (mismatch) {
                  debug_printf("bootloader: mismatch, erasing and re-requesting file");
                  flash_erase();
                  ethcom0.local.udp_port ++;
                  tftp_state = 2;
                } else {
                  debug_printf("bootloader: match, so done");
                  tftp_state = 4;
                }
              }
              else if (tftp_state == 6) {
                debug_printf("bootloader: done programming");
                tftp_state = 4;
              }
              t0 = get_ticks();
            }
            else {
              t0 = get_ticks();
            }
          }
        }
      }
      else if (get_ticks() - t0 > SECONDSTOTICKS(2.0)) {
        debug_printf("bootloader: timeout waiting for download in state %d", tftp_state);
        tftp_state = 2;
      }
    }
    else if (tftp_state == 4) { // done programming
      if (get_ticks() - t0 > SECONDSTOTICKS(0.2)) {
        debug_printf("bootloader: starting application");
        tftp_state = 5;
        t0 = get_ticks();
#if defined(ETHER_NRST_PIN)
        debug_printf("bootloader: resetting PHY");
        gpio_clr_gpio_pin(ETHER_NRST_PIN);
#endif
      }
    }
    else if (tftp_state == 5) { // preparing to start application
      if (get_ticks() - t0 > SECONDSTOTICKS(0.2)) {
        void (*p)() = (void *)(AVR32_FLASH + BOOTLOADER_SIZE);
        p();
        debug_printf("returned from application?");
      }
    }

    if (rx) {
      free_rx_buf(rx);
    }
  }
}


void panic_str(char const *message)
{
  Disable_global_interrupt();
  while (1) {}
}
