#!/usr/local/bin/python

def make_crc32_table(quotient):
    crctab=[]
    for i in range(256):
        crc=i<<24
        for j in range(8):
            if crc&0x80000000:
                crc=(crc<<1)&0xffffffff ^ quotient
            else:
                crc=(crc<<1)&0xffffffff
        crctab.append(crc)
    return crctab
    
def print_avr(f, crctab):
    for i in range(256):
        ent=crctab[i]
        print>>f, "{0x%02x, 0x%02x, 0x%02x, 0x%02x}," % (
            (ent>>0)&0xff, (ent>>8)&0xff, (ent>>16)&0xff, (ent>>24)&0xff)

if __name__=='__main__':
    tab = make_crc32_table(0x04c11db7)
    print_avr(open('build.src/crc32.c', 'w'), tab)
