#include "anycore/std_headers.h"
#include "./pca9532.h"

void pca9532_start_transaction(volatile twibus_transaction_state *st, int device_addr, int regaddr, int autoincrement)
{
  if (autoincrement) {
    st->regaddr = 0x10 | (regaddr & 0xF);
  }
  else {
    st->regaddr = regaddr;
  }
  st->regaddrlen = 1;
  twibus_transaction_start(st, device_addr);
}

void pca9532_start_autoincrement_write_transaction(volatile twibus_transaction_state *st, int device_addr, int first_regaddr, U8 regval[])
{
  int i;
  for (i = 0; i < PCA9532_NO_OF_REGS; i++) {
    st->tx_buf[i] = regval[i];
  }
  st->tx_len = PCA9532_NO_OF_REGS;
  st->rx_len = 0;
  pca9532_start_transaction(st, device_addr, first_regaddr, 1);
}

void pca9532_write_all_regs(pca9532_engine *it, volatile twibus_transaction_state *st)
{
  pca9532_start_autoincrement_write_transaction(st, it->device_addr, 0, it->regval);
}

int pca9532_engine_run(pca9532_engine *it, volatile twibus_transaction_state *st)
{
  int tmode = st->mode;
  int tmode_idle = (tmode == TWIBUS_MODE_IDLE ||
                    tmode == TWIBUS_MODE_RX_COMPLETE ||
                    tmode == TWIBUS_MODE_TX_COMPLETE);
  if (tmode_idle) {
    pca9532_write_all_regs(it, st);
    return 1;
  }
  return 0;
}

U8 pca9532_khz_to_regval(dsp824 frequency) {
  dsp1616 regval = div_dsp824_dsp824_dsp1616(DSP824(0.152), frequency) - DSP1616(1.0);
  return (U8)((regval >> 16) & 0xFF);
}

U8 pca9532_dutycycle_to_regval(dsp824 dutycycle) {
  return (mul_dsp824_dsp1616_dsp1616(dutycycle, DSP1616(256.0)) >> 16) & 0xFF;
}

void pca9532_set_blinkrate0_freq(pca9532_engine *it, dsp824 khz)
{
  it->regval[PCA9532_REGNO_PSC0] = pca9532_khz_to_regval(khz);
}

void pca9532_set_blinkrate0_dutycycle(pca9532_engine *it, dsp824 dutycycle){
  it->regval[PCA9532_REGNO_PWM0] = pca9532_dutycycle_to_regval(dutycycle);
}

void pca9532_set_blinkrate1_freq(pca9532_engine *it, dsp824 khz)
{
  it->regval[PCA9532_REGNO_PSC1] = pca9532_khz_to_regval(khz);
}

void pca9532_set_blinkrate1_dutycycle(pca9532_engine *it, dsp824 dutycycle){
  it->regval[PCA9532_REGNO_PWM1] = pca9532_dutycycle_to_regval(dutycycle);
}

int pca9532_change_bitpair(int full_value, int first_bitno_of_pair, int new_value_of_pair) {
  int mask = 255 ^ (3 << first_bitno_of_pair);
  return (full_value & mask) | (new_value_of_pair << first_bitno_of_pair);
}

int pca9532_get_led_selector_regno(int ledno){
  return (ledno >> 2) + PCA9532_REGNO_LS0;
}

void pca9532_set_led_state(pca9532_engine *it, pca9532_led_state state, int ledno) {
  int led_selector_regno = pca9532_get_led_selector_regno(ledno);
  int led_first_bitno = ((ledno & 3) << 1);
  state = state & 3;
  it->regval[led_selector_regno] = (U8)pca9532_change_bitpair(it->regval[led_selector_regno], led_first_bitno, state);
}

void pca9532_set_all_leds_to_state(pca9532_engine *it, pca9532_led_state state){
  int i;
  U8 new_regval = (U8)(state | (state << 2) | (state << 4) | (state << 6));
  for (i = PCA9532_REGNO_LS0; i < 4 + PCA9532_REGNO_LS0; i++) {
    it->regval[i] = new_regval;
  }
}

void pca9532_engine_create(pca9532_engine *it, twibus *bus, int chipno)
{
  int i;
  it->super.run_func = (int (*)(twibus_engine *, volatile twibus_transaction_state *st)) pca9532_engine_run;
  it->device_addr = 0x60 | (chipno & 7);
  for (i = 0; i < PCA9532_NO_OF_REGS; i++) {
    (it->regval)[i] = (U8)0;
  }
  twibus_add_engine(bus, &it->super);
}
