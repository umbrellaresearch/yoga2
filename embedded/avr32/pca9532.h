#pragma once

#include "anycore/dspcore.h"
#include "yogstudio/embedded/avr32/twibus.h"

typedef enum {
  // ---- read only -----
  PCA9532_REGNO_INPUT0,
  PCA9532_REGNO_INPUT1,
  // --- read/write -----
  PCA9532_REGNO_PSC0,
  PCA9532_REGNO_PWM0,
  PCA9532_REGNO_PSC1,
  PCA9532_REGNO_PWM1,
  PCA9532_REGNO_LS0,
  PCA9532_REGNO_LS1,
  PCA9532_REGNO_LS2,
  PCA9532_REGNO_LS3,
  // --- Number of registers, last enum --
  PCA9532_NO_OF_REGS
} pca9532_registers;

typedef struct pca9532_engine {
  twibus_engine super; // stuctural inheritance -- must be first member
  int device_addr;
  U8 regval[PCA9532_NO_OF_REGS];
} pca9532_engine;

typedef enum { LED_ON = 1, LED_OFF = 0, LED_BLINK_RATE0 = 2, LED_BLINK_RATE1 = 3 } pca9532_led_state;

void pca9532_set_blinkrate0_freq(pca9532_engine *it, dsp824 khz);

void pca9532_set_blinkrate0_dutycycle(pca9532_engine *it, dsp824 dutycycle);

void pca9532_set_blinkrate1_freq(pca9532_engine *it, dsp824 khz);

void pca9532_set_blinkrate1_dutycycle(pca9532_engine *it, dsp824 dutycycle);

void pca9532_set_led_state(pca9532_engine *it, pca9532_led_state state, int ledno);

void pca9532_set_all_leds_to_state(pca9532_engine *it, pca9532_led_state state);

void pca9532_engine_create(pca9532_engine *it, twibus *bus, int chipno);
