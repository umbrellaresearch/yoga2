#pragma once

enum {
  TWIBUS_MODE_IDLE,
  TWIBUS_MODE_TX,
  TWIBUS_MODE_TX_WAIT,
  TWIBUS_MODE_TX_COMPLETE,
  TWIBUS_MODE_RX,
  TWIBUS_MODE_RX_COMPLETE,
};

#define TWIBUS_TX_BUF_SIZE 32
#define TWIBUS_RX_BUF_SIZE 32

typedef struct twibus_transaction_state {
  volatile struct avr32_twi_t *twi;
  int mode;

  int tx_len, tx_pos;
  int rx_len, rx_pos;
  U32 regaddr;
  int regaddrlen;
  U32 transaction_start_time;

  U8 tx_buf[TWIBUS_TX_BUF_SIZE];
  U8 rx_buf[TWIBUS_RX_BUF_SIZE];
  struct {
    int spurious_interrupts;
    int spurious_reads;
    int nacks;
    int total_interrupts;
  } counts;
} twibus_transaction_state;

// Subclass from this (C structural inheritance)
typedef struct twibus_engine {
  int (*run_func)(struct twibus_engine *it, volatile twibus_transaction_state *st);
} twibus_engine;

#define TWIBUS_MAX_ENGINES 32

typedef struct twibus {
  twibus_transaction_state transaction;
  int n_engines;
  int active_engine;
  twibus_engine *engines[TWIBUS_MAX_ENGINES];
} twibus;

extern twibus twibus0;

void twibus_add_engine(twibus *it, twibus_engine *engine);
void twibus_transaction_start(volatile twibus_transaction_state *st, int device_addr);
void twibus_transaction_setup_hw(volatile twibus_transaction_state *st);
void twibus_poll(twibus *it);
void twibus_setup();

// This explores the entire bus for debugging

typedef struct twibus_explorer_engine {
  twibus_engine super; // structural inheritance -- must be first member
  int devaddr;         // counts up
  int state;
} twibus_explorer_engine;

void twibus_explorer_engine_create(twibus_explorer_engine *it, twibus *bus);
