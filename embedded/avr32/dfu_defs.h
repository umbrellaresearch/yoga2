#pragma once

/* Copyright (C) 2006-2008, Atmel Corporation All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. The name of ATMEL may not be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL ``AS IS'' AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE EXPRESSLY AND
 * SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#define DFU_FUNCTIONAL_DESCRIPTOR 0x21

#define DFU_DETACH 0x00
#define DFU_DNLOAD 0x01
#define DFU_UPLOAD 0x02
#define DFU_GETSTATUS 0x03
#define DFU_CLRSTATUS 0x04
#define DFU_GETSTATE 0x05
#define DFU_ABORT 0x06

#define CMD_GRP_DNLOAD 0x01
#define CMD_GRP_UPLOAD 0x03
#define CMD_GRP_EXEC 0x04
#define CMD_GRP_SELECT 0x06

// CMD_GRP_DNLOAD commands
#define CMD_PROGRAM_START 0x00

// CMD_GRP_UPLOAD commands
#define CMD_READ_MEMORY 0x00
#define CMD_BLANK_CHECK 0x01

// CMD_GRP_EXEC commands
#define CMD_ERASE 0x00
#define CMD_START_APPLI 0x03

// CMD_GRP_SELECT commands
#define CMD_SELECT_MEMORY 0x03

// CMD_ERASE arguments
#define CMD_ERASE_ARG_CHIP 0xFF

// CMD_START_APPLI arguments
#define CMD_START_APPLI_ARG_RESET 0x00
#define CMD_START_APPLI_ARG_NO_RESET 0x01

// CMD_SELECT_MEMORY arguments
#define CMD_SELECT_MEMORY_ARG_UNIT 0x00
#define CMD_SELECT_MEMORY_ARG_PAGE 0x01

// Memory units (CMD_SELECT_MEMORY_ARG_UNIT arguments)
#define MEM_FLASH 0x00
#define MEM_EEPROM 0x01
#define MEM_SECURITY 0x02
#define MEM_CONFIGURATION 0x03
#define MEM_BOOTLOADER 0x04
#define MEM_SIGNATURE 0x05
#define MEM_USER 0x06

// DFU status
#define STATUS_OK 0x00
#define STATUS_errTARGET 0x01
#define STATUS_errFILE 0x02
#define STATUS_errWRITE 0x03
#define STATUS_errERASE 0x04
#define STATUS_errCHECK_ERASED 0x05
#define STATUS_errPROG 0x06
#define STATUS_errVERIFY 0x07
#define STATUS_errADDRESS 0x08
#define STATUS_errNOTDONE 0x09
#define STATUS_errFIRMWARE 0x0A
#define STATUS_errVENDOR 0x0B
#define STATUS_errUSBR 0x0C
#define STATUS_errPOR 0x0D
#define STATUS_errUNKNOWN 0x0E
#define STATUS_errSTALLEDPKT 0x0F

// DFU state
#define STATE_appIDLE 0x00
#define STATE_appDETACH 0x01
#define STATE_dfuIDLE 0x02
#define STATE_dfuDNLOAD_SYNC 0x03
#define STATE_dfuDNBUSY 0x04
#define STATE_dfuDNLOAD_IDLE 0x05
#define STATE_dfuMANIFEST_SYNC 0x06
#define STATE_dfuMANIFEST 0x07
#define STATE_dfuMANIFEST_WAIT_RESET 0x08
#define STATE_dfuUPLOAD_IDLE 0x09
#define STATE_dfuERROR 0x0A
