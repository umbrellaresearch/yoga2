#include "common/std_headers.h"
#include "hwdefs.h"
#include "./avr32/sercom.h"
#include "./avr32/ethcom.h"

struct sercom *host_sercom;
struct ethcom *host_ethcom;
