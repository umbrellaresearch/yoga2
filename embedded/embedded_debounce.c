#include "common/std_headers.h"
#include "./embedded_debug.h"
#include "./embedded_debounce.h"


int debounce_run(debounce *it, int curvalue, dsp824 dt)
{
  assert(dt > 0);

  if (curvalue) {
    it->cur += dt;
    if (it->cur > it->hi_lim) {
      it->on = 1;
      it->cur = it->hi_lim;
    }
  } else {
    it->cur -= dt;
    if (it->cur < it->lo_lim) {
      it->on = 0;
      it->cur = it->lo_lim;
    }
  }
  return it->on;
}

void debounce_init(debounce *it, dsp824 cur, dsp824 lo_lim, dsp824 hi_lim)
{
  it->cur = cur;
  it->lo_lim = lo_lim;
  it->hi_lim = hi_lim;
  it->on = (cur > (lo_lim + hi_lim)/2) ? 1 : 0;
}
