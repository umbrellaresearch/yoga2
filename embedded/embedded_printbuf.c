#include "common/std_headers.h"
#include <ctype.h>
#include "hwdefs.h"
#include "./embedded_printbuf.h"
#include "./embedded_utils.h"
#if defined(USE_PRINTBUF_DSP)
#include "dspcore/vec3dsp.h"
#endif

/*
  Sadly, the printf functions in avr32 libc don't work because they rely on floating point libraries that aren't implemented.
  Here's a simple but safe implementation
*/

void printbuf_init(printbuf *it)
{
  it->len = 0;
}

void printbuf_append(printbuf *it, char c)
{
  if (it->len >= PRINTBUF_SIZE-1) {
    it->buf[PRINTBUF_SIZE-4] = '.';
    it->buf[PRINTBUF_SIZE-3] = '.';
    it->buf[PRINTBUF_SIZE-2] = '.';
    it->buf[PRINTBUF_SIZE-1] = 0;
    it->len = PRINTBUF_SIZE-1;
    return;
  }
  it->buf[it->len++] = c;
}

void printbuf_append_str(printbuf *it, char const *s)
{
  while (*s) {
    printbuf_append(it, *s);
    s++;
  }
}

void printbuf_terminate(printbuf *it)
{
  if (it->len > PRINTBUF_SIZE-1) {
    it->len = PRINTBUF_SIZE-1;
  }
  it->buf[it->len] = 0;
}

// ----------------------------------------------------------------------

#define NUMBUF_SIZE 64

typedef struct numbuf {
  char *front_ptr;
  char *back_ptr;
  int len;
  char buf[NUMBUF_SIZE];
} numbuf;

void numbuf_init(numbuf *it)
{
  it->front_ptr = it->back_ptr = &it->buf[NUMBUF_SIZE/2];
  it->len = 0;
}

void numbuf_add_back(numbuf *it, char c)
{
  if (it->back_ptr <= &it->buf[NUMBUF_SIZE-1]) {
    *it->back_ptr++ = c;
  }
  it->len++; // Since there are loops around checking for len<width, I still increment len even though I'm not actually adding the characters
}

void numbuf_add_front(numbuf *it, char c)
{
  if (it->front_ptr > &it->buf[0]) {
    *--it->front_ptr = c;
  }
  it->len++;
}

void printbuf_append_numbuf(printbuf *out, numbuf *src)
{
  for (char *p=src->front_ptr; p!=src->back_ptr; p++) {
    printbuf_append(out, *p);
  }
}

// ----------------------------------------------------------------------

void numbuf_format_x(numbuf *out, U32 value, int width, int prec, int sign, char pad)
{
  do {
    U32 digit = value & 0x0f;
    numbuf_add_front(out, hex(digit));
    value = value >> 4;
  } while (value);
  while (out->len < width) {
    numbuf_add_front(out, pad);
  }
}

void numbuf_format_u(numbuf *out, U32 value, int width, int prec, int sign, char pad)
{
  do {
    U32 digit = value % 10;
    numbuf_add_front(out, '0' + digit);
    value = value / 10;
  } while (value);
  while (out->len < width) {
    numbuf_add_front(out, pad);
  }
}

void numbuf_format_d(numbuf *out, S32 value, int width, int prec, int sign, char pad)
{
  if (value<0) {
    numbuf_format_u(out, (U32)-value, (pad=='0') ? (width-1) : 0, prec, 0, pad);
    numbuf_add_front(out, '-');
    while (out->len < width) {
      numbuf_add_front(out, pad);
    }
  } else {
    numbuf_format_u(out, (U32)value, (pad=='0') ? ((sign>0) ? (width-1) : width) : 0, prec, 0, pad);
    if (sign>0) {
      numbuf_add_front(out, '+');
    }
  }
  while (out->len < width) {
    numbuf_add_front(out, pad);
  }
}

void numbuf_format_qx(numbuf *out, U64 value, int width, int prec, int sign, char pad)
{
  do {
    U32 digit = value & 0x0f;
    numbuf_add_front(out, hex(digit));
    value = value >> 4;
  } while (value);
  while (out->len < width) {
    numbuf_add_front(out, pad);
  }
}

void numbuf_format_qu(numbuf *out, U64 value, int width, int prec, int sign, char pad)
{
  // No 64 bit division implemented, do it in hex instead
  numbuf_format_qx(out, value, width-2, prec, sign, pad);
  numbuf_add_front(out, 'x');
  numbuf_add_front(out, '0');
}

void numbuf_format_qd(numbuf *out, S64 value, int width, int prec, int sign, char pad)
{
  if (value<0) {
    numbuf_format_qu(out, (U64)-value, (pad=='0') ? (width-1) : 0, prec, 0, pad);
    numbuf_add_front(out, '-');
    while (out->len < width) {
      numbuf_add_front(out, pad);
    }
  } else {
    numbuf_format_qu(out, (U64)value, (pad=='0') ? ((sign>0) ? (width-1) : width) : 0, prec, 0, pad);
    if (sign>0) {
      numbuf_add_front(out, '+');
    }
  }
  while (out->len < width) {
    numbuf_add_front(out, pad);
  }
}

#if defined(USE_PRINTBUF_DSP)
void numbuf_format_f824(numbuf *out, dsp824 value, int width, int prec, int sign, char pad)
{
  Bool negflag = 0;
  if (value < 0) {
    value = -value;
    negflag = 1;
  }
  U32 frac = (U32)value & ((1<<24)-1);
  // We limit to 8 digits of precision for our 24-bit mantissa
  do {
    frac = frac*10;
    U32 digit = frac>>24;
    numbuf_add_back(out, '0' + digit);
    frac = frac & ((1<<24)-1);
  } while (prec ? (out->len < prec) : (frac!=0 && out->len<8));
  numbuf_add_front(out, '.');
  numbuf_format_u(out, (U32)value>>24, width, 0, 0, pad);
  if (negflag) {
    numbuf_add_front(out, '-');
  }
  else if (sign>0) {
    numbuf_add_front(out, '+');
  }
}

void numbuf_format_f230(numbuf *out, dsp230 value, int width, int prec, int sign, char pad)
{
  Bool negflag = 0;
  if (value < 0) {
    value = -value;
    negflag = 1;
  }
  U32 frac = (U32)value & ((1<<30)-1);
  // We limit to 11 digits of precision for our 30-bit mantissa
  do {
    U64 fullfrac = (U64)frac * 10;
    U32 digit = fullfrac>>30;
    numbuf_add_back(out, '0' + digit);
    frac = (U32)fullfrac & ((1<<30)-1);
  } while (prec ? (out->len < prec) : (frac!=0 && out->len<11));
  numbuf_add_front(out, '.');
  numbuf_format_u(out, (U32)value>>30, width, 0, 0, pad);
  if (negflag) {
    numbuf_add_front(out, '-');
  }
  else if (sign>0) {
    numbuf_add_front(out, '+');
  }
}

void numbuf_format_f1616(numbuf *out, dsp1616 value, int width, int prec, int sign, char pad)
{
  Bool negflag = 0;
  if (value < 0) {
    value = -value;
    negflag = 1;
  }
  U32 frac = (U32)value & ((1<<16)-1);
  do {
    frac = frac*10;
    U32 digit = frac>>16;
    numbuf_add_back(out, '0' + digit);
    frac = frac & ((1<<16)-1);
  } while (prec ? (out->len < prec) : (frac!=0 && out->len<5));
  numbuf_add_front(out, '.');
  numbuf_format_u(out, (U32)value>>16, width, 0, 0, pad);
  if (negflag) {
    numbuf_add_front(out, '-');
  }
  else if (sign>0) {
    numbuf_add_front(out, '+');
  }
}

void numbuf_format_f1648(numbuf *out, dsp1648 value, int width, int prec, int sign, char pad)
{
  Bool negflag = 0;
  if (value < 0) {
    value = -value;
    negflag = 1;
  }
  U64 frac = (U64)value & (((U64)1<<48)-1);
  do {
    frac = frac*10;
    U32 digit = frac>>48;
    numbuf_add_back(out, '0' + digit);
    frac = frac & (((U64)1<<48)-1);
  } while (prec ? (out->len < prec) : (frac!=0 && out->len<10));
  numbuf_add_front(out, '.');
  numbuf_format_u(out, (U64)value>>48, width, 0, 0, pad);
  if (negflag) {
    numbuf_add_front(out, '-');
  }
  else if (sign>0) {
    numbuf_add_front(out, '+');
  }
}


void numbuf_format_fticks(numbuf *out, S64 value, int width, int prec, int sign, char pad)
{
  Bool negflag = 0;
  if (value < 0) {
    value = -value;
    negflag = 1;
  }

  // Number is 1<<32 as a float, so we're converting to 32.32
  U64 seconds = value * (U64)(4294967296.0 / FCPU);
  U64 frac = seconds & ((1LL<<32)-1);

  do {
    frac = frac*10;
    U32 digit = frac>>32;
    numbuf_add_back(out, '0' + digit);
    frac = frac & ((1LL<<32)-1);
  } while (prec ? (out->len < prec) : (frac!=0 && out->len<6));
  numbuf_add_back(out, 'S');
  numbuf_add_front(out, '.');
  numbuf_format_u(out, (U32)(seconds>>32), width, 0, 0, pad);
  if (negflag) {
    numbuf_add_front(out, '-');
  }
  else if (sign>0) {
    numbuf_add_front(out, '+');
  }
}


// ----------------------------------------------------------------------

void printbuf_append_dsp230(printbuf *out, dsp230 value, int width, int prec, int sign, char pad)
{
  numbuf nb;
  numbuf_init(&nb);
  numbuf_format_f230(&nb, value, width, prec, sign, pad);
  printbuf_append_numbuf(out, &nb);
}

void printbuf_append_dsp824(printbuf *out, dsp824 value, int width, int prec, int sign, char pad)
{
  numbuf nb;
  numbuf_init(&nb);
  numbuf_format_f824(&nb, value, width, prec, sign, pad);
  printbuf_append_numbuf(out, &nb);
}

void printbuf_append_dsp1616(printbuf *out, dsp1616 value, int width, int prec, int sign, char pad)
{
  numbuf nb;
  numbuf_init(&nb);
  numbuf_format_f1616(&nb, value, width, prec, sign, pad);
  printbuf_append_numbuf(out, &nb);
}

void printbuf_append_dsp1648(printbuf *out, dsp1648 value, int width, int prec, int sign, char pad)
{
  numbuf nb;
  numbuf_init(&nb);
  numbuf_format_f1648(&nb, value, width, prec, sign, pad);
  printbuf_append_numbuf(out, &nb);
}

void printbuf_append_fticks(printbuf *out, S64 value, int width, int prec, int sign, char pad)
{
  numbuf nb;
  numbuf_init(&nb);
  numbuf_format_fticks(&nb, value, width, prec, sign, pad);
  printbuf_append_numbuf(out, &nb);
}

void printbuf_append_mat3dsp230(printbuf *out, mat3dsp230 *arg, int width, int prec, int sign, char pad)
{
  printbuf_append_str(out, "mat3(");
  printbuf_append_dsp230(out, arg->xx, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->xy, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->xz, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->yx, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->yy, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->yz, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->zx, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->zy, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->zz, width, prec, sign, pad);
  printbuf_append_str(out, ")");
}


void printbuf_append_vec3dsp824(printbuf *out, vec3dsp824 *arg, int width, int prec, int sign, char pad)
{
  printbuf_append_str(out, "vec3(");
  printbuf_append_dsp824(out, arg->x, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp824(out, arg->y, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp824(out, arg->z, width, prec, sign, pad);
  printbuf_append_str(out, ")");
}

void printbuf_append_vec3dsp230(printbuf *out, vec3dsp230 *arg, int width, int prec, int sign, char pad)
{
  printbuf_append_str(out, "vec3(");
  printbuf_append_dsp230(out, arg->x, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->y, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->z, width, prec, sign, pad);
  printbuf_append_str(out, ")");
}

void printbuf_append_ea3dsp230(printbuf *out, ea3dsp230 *arg, int width, int prec, int sign, char pad)
{
  printbuf_append_str(out, "ea3(");
  printbuf_append_dsp230(out, arg->pitch, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->roll, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp230(out, arg->yaw, width, prec, sign, pad);
  printbuf_append_str(out, ")");
}

void printbuf_append_ea3dsp824(printbuf *out, ea3dsp824 *arg, int width, int prec, int sign, char pad)
{
  printbuf_append_str(out, "ea3(");
  printbuf_append_dsp824(out, arg->pitch, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp824(out, arg->roll, width, prec, sign, pad);
  printbuf_append_str(out, ", ");
  printbuf_append_dsp824(out, arg->yaw, width, prec, sign, pad);
  printbuf_append_str(out, ")");
}
#endif

#if defined(USE_PRINTBUF_DSP)
static Bool consume_format(char const **format, char const *expect)
{
  char const *p = *format;

  while (1) {
    if (!*expect) {
      *format = p - 1;
      return 1;
    }
    if (!*p) return 0;
    if (*p != *expect) return 0;
    p++; expect++;
  }
}
#endif

void printbuf_vprintf(printbuf *out, char const *format, va_list ap)
{
  numbuf nb;

  while (*format) {
    if (*format == '%') {
      format++;
      int width = 0;
      int prec = 0;
      int sign = 0;
      char pad = ' ';
      Bool islong = 0;
      Bool isshort = 0;
      Bool isquad = 0;
      if (*format=='+') {
        sign = +1;
        format++;
      }
      else if (*format=='-') {
        sign = -1;
        format++;
      }
      if (*format=='0') {
        pad = '0';
        format++;
      }
      while (isdigit(*format)) {
        width = width * 10 + (*format-'0');
        format++;
      }
      if (*format == '.') {
        format++;
        while (isdigit(*format)) {
          prec = prec * 10 + (*format-'0');
          format++;
        }
      }

      // flags
      while (1) {
        if (*format == 'l') {
          islong=1;
        }
        else if (*format == 'h') {
          isshort=1;
        }
        else if (*format == 'q') {
          isquad=1;
        }
        else {
          break;
        }
        format++;
      }

      if (*format=='s') {
        char *arg = va_arg(ap, char *);
        if (!arg) arg="(null)";
        while (*arg) {
          printbuf_append(out, *arg++);
        }
      }
      else if (*format=='d') {
        if (isquad) {
          S64 arg = va_arg(ap, S64);
          numbuf_init(&nb);
          numbuf_format_qd(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
        else {
          int arg = va_arg(ap, int);
          numbuf_init(&nb);
          numbuf_format_d(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
      }
      else if (*format=='u') {
        if (isquad) {
          S64 arg = va_arg(ap, S64);
          numbuf_init(&nb);
          numbuf_format_qu(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
        else {
          int arg = va_arg(ap, int);
          numbuf_init(&nb);
          numbuf_format_u(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
      }
      else if (*format=='x') {
        if (isquad) {
          S64 arg = va_arg(ap, S64);
          numbuf_init(&nb);
          numbuf_format_qx(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
        else {
          int arg = va_arg(ap, int);
          numbuf_init(&nb);
          numbuf_format_x(&nb, arg, width, prec, sign, pad);
          printbuf_append_numbuf(out, &nb);
        }
      }
      else if (*format=='@') {
        printbuf_format_func_t format_func = va_arg(ap, printbuf_format_func_t);
        int arg = va_arg(ap, S32);

        (*format_func)(out, arg, width, prec, sign, pad);
      }
      else if (*format=='{') {
#if defined(USE_PRINTBUF_DSP)
        if (consume_format(&format, "{dsp824}")) {
          dsp824 arg = va_arg(ap, dsp824);
          printbuf_append_dsp824(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{dsp230}")) {
          dsp230 arg = va_arg(ap, dsp230);
          printbuf_append_dsp230(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{dsp1616}")) {
          dsp1616 arg = va_arg(ap, dsp1616);
          printbuf_append_dsp1616(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{dsp1648}")) {
          dsp1648 arg = va_arg(ap, dsp1648);
          printbuf_append_dsp1648(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{mat3dsp230}")) {
          mat3dsp230 *arg = va_arg(ap, mat3dsp230 *);
          printbuf_append_mat3dsp230(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{vec3dsp824}")) {
          vec3dsp824 *arg = va_arg(ap, vec3dsp824 *);
          printbuf_append_vec3dsp824(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{vec3dsp230}")) {
          vec3dsp230 *arg = va_arg(ap, vec3dsp230 *);
          printbuf_append_vec3dsp230(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{ea3dsp824}")) {
          ea3dsp824 *arg = va_arg(ap, ea3dsp824 *);
          printbuf_append_ea3dsp824(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{ea3dsp230}")) {
          ea3dsp230 *arg = va_arg(ap, ea3dsp230 *);
          printbuf_append_ea3dsp230(out, arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{ticks}")) {
          S32 arg = va_arg(ap, S32);
          printbuf_append_fticks(out, (S64)arg, width, prec, sign, pad);
        }
        else if (consume_format(&format, "{ticks64}")) {
          S64 arg = va_arg(ap, S64);
          printbuf_append_fticks(out, arg, width, prec, sign, pad);
        }
#endif
      }
      else if (*format=='%') {
        printbuf_append(out, '%');
      }
      format++;
    } else {
      printbuf_append(out, *format);
      format++;
    }
  }
}

void printbuf_printf(printbuf *out, char const *format, ...)
{
  va_list ap;
  va_start(ap,format);
  printbuf_vprintf(out, format, ap);
}
