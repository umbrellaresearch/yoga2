#pragma once

#include <stdarg.h>

#define PRINTBUF_SIZE 508
typedef struct printbuf {
  int len;
  char buf[PRINTBUF_SIZE];
} printbuf;

typedef void (*printbuf_format_func_t)(printbuf *, int, int, int, int, int);

void printbuf_init(printbuf *it);
void printbuf_append(printbuf *it, char c);
void printbuf_append_str(printbuf *it, char const *s);
void printbuf_terminate(printbuf *it);
void printbuf_vprintf(printbuf *out, char const *format, va_list ap);
void printbuf_printf(printbuf *out, char const *format, ...);
