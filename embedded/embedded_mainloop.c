#include "common/std_headers.h"
#include "./embedded_pktcom.h"
#include "./embedded_debug.h"
#include "./embedded_timing.h"
#include "./embedded_hostif.h"
#include "./embedded_mainloop.h"
#include "./avr32/ethcom.h"
#include "./avr32/sercom.h"

mainloop mainloop0;

void mainloop_entry_poll(mainloop_entry *it)
{
}

void mainloop_entry_log(mainloop_entry *it)
{
  if (it->slowest_ticks > 15000) {
    log_printf("%s: (slowest=%d)", it->name, it->slowest_ticks);
    it->slowest_ticks = 0;
  }
  if (it->log_func) {
    (it->log_func)(it->cookie, it->name);
  }
}

void mainloop_add_entry(mainloop *it, char const *name,
			mainloop_poll_t poll_func, mainloop_poll_t fastpoll_func,
			mainloop_log_t log_func, void *cookie)
{
  if (it->n_entries >= MAINLOOP_MAX_ENTRIES) panic_printf("mainloop: entries overflow with %s\n", name);
  it->entries[it->n_entries].name = name;
  it->entries[it->n_entries].poll_func = poll_func;
  it->entries[it->n_entries].fastpoll_func = fastpoll_func;
  it->entries[it->n_entries].log_func = log_func;
  it->entries[it->n_entries].cookie = cookie;
  it->n_entries++;
}

void mainloop_poll_all(mainloop *it)
{
  for (int i=0; i<it->n_entries; i++) {
    mainloop_entry *e = &it->entries[i];
    if (e->poll_func) {
      U32 begin_t = get_ticks();
      (e->poll_func)(e->cookie);
      U32 end_t = get_ticks();
      U32 ticks = end_t - begin_t;
      if (ticks > e->slowest_ticks) e->slowest_ticks = ticks;
    }
  }
}

void mainloop_fastpoll_all(mainloop *it)
{
  for (int i=0; i<it->n_entries; i++) {
    mainloop_entry *e = &it->entries[i];
    if (e->fastpoll_func) {
      U32 begin_t = get_ticks();
      (e->fastpoll_func)(e->cookie);
      U32 end_t = get_ticks();
      U32 ticks = end_t - begin_t;
      if (ticks > e->slowest_ticks) e->slowest_ticks = ticks;
    }
  }
}

void mainloop_log_all(mainloop *it)
{
  log_printf("mainloop: time=%{dsp824}", it->time);
  for (int i=0; i<it->n_entries; i++) {
    mainloop_entry_log(&it->entries[i]);
  }
}


void mainloop_poll(mainloop *it)
{
  mainloop_fastpoll_all(it);

  rate_generator_set_budget(&it->rg, 10);

  if (rate_generator_update(&it->rg)) {

    it->time += DSP824(MAINLOOP_DT);

    U32 old_tickno = it->tickno;
    it->tickno++;
    it->tickmask = it->tickno & ~old_tickno;

    mainloop_poll_all(it);

    if (it->time - it->last_log_time > DSP824(0.5)) {
      it->last_log_time = it->time;
      if (host_sercom) sercom_report_errors(host_sercom);
      log_flush();
    }
    else if (it->time - it->last_log_status_time > DSP824(1.0)) {
      it->last_log_status_time = it->time;

      if (1) mainloop_log_all(it);
    }

    rate_generator_done(&it->rg);
  }
}

void mainloop_setup()
{
  mainloop *it = &mainloop0;

  rate_generator_enable(&it->rg, SECONDSTOTICKS(MAINLOOP_DT), 3);
}
