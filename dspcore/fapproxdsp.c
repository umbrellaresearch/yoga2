#include "common/std_headers.h"
#include "./fapproxdsp.h"
#include "../embedded/embedded_debug.h"

dsp824 fapprox_poly_0_3dsp824_calc(fapprox_poly_0_3dsp824 *it, dsp824 value)
{
  dsp824 x1 = value;
  dsp824 x2 = mul_dsp824_dsp824_dsp824(value, value);
  dsp824 x3 = mul_dsp824_dsp824_dsp824(x2, value);

  return (it->c0 +
          mul_dsp824_dsp824_dsp824(x1, it->c1) +
          mul_dsp824_dsp824_dsp824(x2, it->c2) +
          mul_dsp824_dsp824_dsp824(x3, it->c3));
}

dsp824 fapprox_poly_0_3dsp824_calc_deriv(fapprox_poly_0_3dsp824 *it, dsp824 value)
{
  dsp824 x1 = value;
  dsp824 x2 = mul_dsp824_dsp824_dsp824(value, value);

  return (it->c1 +
          mul_dsp824_dsp824_dsp824(x1, it->c2)/2 +
          mul_dsp824_dsp824_dsp824(x2, it->c3)/3);
}


// ======================================================================

void fapprox_poly_0_3dsp824_test1(fapprox_poly_0_3dsp824 *poly, dsp824 lo, dsp824 hi, dsp824 step)
{
  for (dsp824 x = lo; x <= hi; x += step) {
    dsp824 y = fapprox_poly_0_3dsp824_calc(poly, x);
    dsp824 yd = fapprox_poly_0_3dsp824_calc_deriv(poly, x);
    debug_printf("f[%+6.3{dsp824}] = %+{dsp824}  d=%+{dsp824}", x, y, yd);
  }
}

void test_fapproxdsp()
{
  fapprox_poly_0_3dsp824 poly = {DSP824(-0.3), DSP824(0.7), DSP824(0.9), DSP824(0.2)};
  fapprox_poly_0_3dsp824_test1(&poly, DSP824(-1.5), DSP824(1.5), DSP824(0.125));
}
