#include "./dspcore.h"
#include "../embedded/embedded_debug.h"


/*
  Calculate 1/sqrt(x) without using any divides. x should be fairly close to 1
*/
dsp230 invsqrtsat_dsp230_dsp230(dsp230 x)
{
  dsp230 y, target0, delta;
  dsp1616 derivative, inv_derivative;
  int iteri;

  if (x <= DSP230(0.251)) return DSP230(1.999); // otherwise we'd overflow

  y = DSP230(1.0); // initial value
  // 5 iterations should get us pretty close
  for (iteri=0; iteri<5; iteri++) {
    // Calculate y**2 * x, which should be close to 1 if we're getting somewhere
    target0 = mul_dsp230_dsp230_dsp230(y, mul_dsp230_dsp230_dsp230(y, x)) - DSP230(1.0);

    derivative = mul_dsp230_dsp1616_dsp1616(mul_dsp230_dsp230_dsp230(y, x), DSP1616(2.0));
    inv_derivative = inverse_dsp1616_dsp1616(derivative);

    delta = mul_dsp230_dsp1616_dsp230(target0, inv_derivative);
    if (delta==DSP230(0)) break;

    y = y - delta;
  }
  return y;
}

// ----------------------------------------------------------------------

/*
  Compute sin & cos for values of x within [-pi/4, +pi/4]
*/
void sincos_smallangle_dsp230_dsp230_dsp230(dsp230 x, dsp230 *sin_r, dsp230 *cos_r)
{
  /*
    OK, I'll probably go to hell for implementing my own sin & cos in 2008. But as you can
    see it's not that hard, and I get really good speed and control of rounding and
    precision.

    13 multiplies, 11 adds. It clocks in at 177 cycles.
  */
  static const dsp230 S0 = DSP230(+1.0);
  //static const dsp230 C0 = DSP230(+1.0 / (2.0));
  static const dsp230 S1 = DSP230(-1.0 / (2.0 * 3.0));
  static const dsp230 C1 = DSP230(+1.0 / (2.0 * 3.0 * 4.0));
  static const dsp230 S2 = DSP230(+1.0 / (2.0 * 3.0 * 4.0 * 5.0));
  static const dsp230 C2 = DSP230(-1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0));
  static const dsp230 S3 = DSP230(-1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0));
  static const dsp230 C3 = DSP230(+1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0));
  static const dsp230 S4 = DSP230(+1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0));
  static const dsp230 C4 = DSP230(-1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0 * 10.0));
  static const dsp230 S5 = DSP230(-1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0 * 10.0 * 11.0));
  static const dsp230 C5 = DSP230(+1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0 * 10.0 * 11.0 * 12.0));
  //static const dsp230 S6 = DSP230(+1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0 * 10.0 * 11.0 * 12.0 * 13.0));
  //static const dsp230 C6 = DSP230(-1.0 / (2.0 * 3.0 * 4.0 * 5.0 * 6.0 * 7.0 * 8.0 * 9.0 * 10.0 * 11.0 * 12.0 * 13.0 * 14.0));

  dsp230 tmp;
  dsp230 x2 = mul_dsp230_dsp230_dsp230(x, x);

  tmp = mul_dsp230_dsp230_dsp230(x2, S5);
  tmp = mul_dsp230_dsp230_dsp230(x2, S4 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, S3 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, S2 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, S1 + tmp);
  *sin_r = mul_dsp230_dsp230_dsp230(x, tmp) + x; // cheat here by omitting S0 because it's 1.0

  tmp = mul_dsp230_dsp230_dsp230(x2, C5);
  tmp = mul_dsp230_dsp230_dsp230(x2, C4 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, C3 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, C2 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, C1 + tmp);
  tmp = mul_dsp230_dsp230_dsp230(x2, tmp);
  *cos_r = tmp + S0 - (x2>>1); // cheat here by shifting because C0 is 0.5, and use S0 rather than C0
}

/*
  Compute sin & cos for any value of theta, using reflections to reduce theta to [-pi/4, +pi/4]
*/
void sincos_dsp230_dsp230_dsp230(dsp230 theta, dsp230 *sin_r, dsp230 *cos_r)
{

  S32 octant, quadrant;
  U32 subquad;
  dsp230 theta_base, theta_frac;
  dsp230 s1, c1;

  // optimization since this is the very common case
  if (theta < DSP230(M_PI/4.0) && theta > -DSP230(M_PI/4.0)) {
    sincos_smallangle_dsp230_dsp230_dsp230(theta, sin_r, cos_r);
    return;
  }

  // funky arithmetic here to avoid overflow
  octant = ((S64)theta * (S64)((4.0 / M_PI) * (float)(1<<29))) >> (29+30);
  quadrant = ((octant+1)>>1);
  theta_base = quadrant * DSP230(M_PI / 2.0);
  theta_frac = theta - theta_base;

  sincos_smallangle_dsp230_dsp230_dsp230(theta_frac, &s1, &c1);

  subquad = quadrant&3;
  if (subquad == 0) {
    *sin_r = +s1;
    *cos_r = +c1;
  }
  else if (subquad == 1) {
    *sin_r = c1;
    *cos_r = -s1;
  }
  else if (subquad == 2) {
    *sin_r = -s1;
    *cos_r = -c1;
  }
  else {
    *sin_r = -c1;
    *cos_r = +s1;
  }
}

/*
  Compute sin & cos for any value of theta, using reflections to reduce theta to [-pi/4, +pi/4]
*/
void sincos_dsp824_dsp230_dsp230(dsp824 theta, dsp230 *sin_r, dsp230 *cos_r)
{
  S32 octant, quadrant;
  U32 subquad;
  dsp824 theta_base;
  dsp230 theta_frac;
  dsp230 s1, c1;

  // optimization since this is the very common case
  if (theta < DSP824(M_PI/4.0) && theta > -DSP824(M_PI/4.0)) {
    sincos_smallangle_dsp230_dsp230_dsp230(conv_dsp824_dsp230(theta), sin_r, cos_r);
    return;
  }

  // funky arithmetic here to avoid overflow
  octant = ((S64)theta * (S64)((4.0 / M_PI) * (float)(1<<29))) >> (29+24);
  quadrant = ((octant+1)>>1);
  theta_base = quadrant * DSP824(M_PI / 2.0);
  theta_frac = conv_dsp824_dsp230(theta - theta_base);

  sincos_smallangle_dsp230_dsp230_dsp230(theta_frac, &s1, &c1);

  subquad = quadrant&3;
  if (subquad == 0) {
    *sin_r = +s1;
    *cos_r = +c1;
  }
  else if (subquad == 1) {
    *sin_r = c1;
    *cos_r = -s1;
  }
  else if (subquad == 2) {
    *sin_r = -s1;
    *cos_r = -c1;
  }
  else {
    *sin_r = -c1;
    *cos_r = +s1;
  }
}

void sincos_dsp824_dsp824_dsp824(dsp824 theta, dsp824 *sin_r, dsp824 *cos_r)
{
  dsp230 sin230, cos230;
  sincos_dsp824_dsp230_dsp230(theta, &sin230, &cos230);
  *sin_r = conv_dsp230_dsp824(sin230);
  *cos_r = conv_dsp230_dsp824(cos230);
}

// Joining Trevor in Hell ~dc
// 13 multiplies, 6 adds
dsp230 atan_dsp230_dsp230(dsp230 x) {
  // http://upload.wikimedia.org/math/8/a/a/8aa763c340d4ddbb93ada8b41124bcac.png
  //static const dsp230 A0 = DSP230(+1.0 / (2.0 * 0.0 + 1.0));
  static const dsp230 A1 = DSP230(-1.0 / (2.0 * 1.0 + 1.0));
  static const dsp230 A2 = DSP230(+1.0 / (2.0 * 2.0 + 1.0));
  static const dsp230 A3 = DSP230(-1.0 / (2.0 * 3.0 + 1.0));
  static const dsp230 A4 = DSP230(+1.0 / (2.0 * 4.0 + 1.0));
  static const dsp230 A5 = DSP230(-1.0 / (2.0 * 5.0 + 1.0));
  static const dsp230 A6 = DSP230(-1.0 / (2.0 * 6.0 + 1.0));

  dsp230 x2 = mul_dsp230_dsp230_dsp230(x, x);

  // n = 0
  dsp230 x2n1 = x;
  dsp230 acc  = x; // A0 = 1
  // n = 1
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2); // Each time we raise x two more
  acc += mul_dsp230_dsp230_dsp230(A1, x2n1);
  // n = 2
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2);
  acc += mul_dsp230_dsp230_dsp230(A2, x2n1);
  // n = 3
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2);
  acc += mul_dsp230_dsp230_dsp230(A3, x2n1);
  // n = 4
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2);
  acc += mul_dsp230_dsp230_dsp230(A4, x2n1);
  // n = 5
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2);
  acc += mul_dsp230_dsp230_dsp230(A5, x2n1);
  // n = 6
  x2n1 = mul_dsp230_dsp230_dsp230(x2n1, x2);
  acc += mul_dsp230_dsp230_dsp230(A6, x2n1);

  return acc;
}

dsp824 atan2_dsp824_dsp824_dsp824(dsp824 y, dsp824 x) {
  // http://en.wikipedia.org/wiki/Atan2

  dsp230 q = div_dsp824_dsp824_dsp230(y, x);

  if (x > 0) {
    return conv_dsp230_dsp824(atan_dsp230_dsp230(q));
  }
  else if (y >= 0 && x < 0) {
    return conv_dsp230_dsp824(atan_dsp230_dsp230(q)) + DSP824(M_PI);
  }
  else if (y < 0 && x < 0) {
    return conv_dsp230_dsp824(atan_dsp230_dsp230(q)) - DSP824(M_PI);
  }
  else if (y > 0 && x == 0) {
    return DSP824(+M_PI/2.0);
  }
  else if (y < 0 && x == 0) {
    return DSP824(-M_PI/2.0);
  }
  else {
    return 0; // Relatively safe value for undefined case
  }
}


// ----------------------------------------------------------------------

dsp824  inverse_dsp824_dsp824(   dsp824  a)    { return (dsp824) (S32)(((1LL<<60) / (S64)(S32)a) >> (60-24-24)); }
dsp1616 inverse_dsp824_dsp1616(  dsp824  a)    { return (dsp1616)(S32)(((1LL<<60) / (S64)(S32)a) >> (60-24-16)); }
dsp1616 inverse_dsp1616_dsp1616( dsp1616 a)    { return (dsp1616)(S32)(((1LL<<60) / (S64)(S32)a) >> (60-16-16)); }

dsp1616 div_dsp1616_dsp1616_dsp1616(dsp1616 a, dsp1616 b)    { return (dsp1616) (((S64)(S32)a << (16+16-16)) / (S64)(S32)b); }
dsp824  div_dsp824_dsp824_dsp824(dsp824 a, dsp824 b)         { return (dsp824)  (((S64)(S32)a << (24+24-24)) / (S64)(S32)b); }
dsp230  div_dsp824_dsp824_dsp230(dsp824 a, dsp824 b)         { return (dsp230)  (((S64)(S32)a << (24+30-24)) / (S64)(S32)b); }
dsp230  div_dsp230_dsp824_dsp230(dsp230 a, dsp824 b)         { return (dsp230)  (((S64)(S32)a << (30+24-30)) / (S64)(S32)b); }

dsp1616  div_dsp824_dsp824_dsp1616(dsp824 a, dsp824 b)       { return (dsp1616) (((S64)(S32)a << (24+16-24)) / (S64)(S32)b); }
dsp1616  div_int_dsp824_dsp1616(S32 a, dsp824 b)             { return (dsp1616) (((S64)(S32)a << (24+16))    / (S64)(S32)b); }


// ----------------------------------------------------------------------

/*
  The convrnd family does stochastic average rounding, so 1.7 has a 30% chance of getting
  rounded to 1 and a 70% chance of getting rounded to 2.
*/

static U32 convrnd_seed;
U32 convrnd_generator_u32()
{
  // Same as VAX rand(), but 32 bits instead of 31
  convrnd_seed = (1103515245 * convrnd_seed + 12345);
  return convrnd_seed;
}

U64 convrnd_generator_u64()
{
  U32 lo=convrnd_generator_u32();
  U32 hi=convrnd_generator_u32();
  return ((U64)hi << 32) | ((U64)lo << 0);
}

// ======================================================================

void test_inverse_dsp824_dsp824(dsp824 x)
{
  dsp824 invx = inverse_dsp824_dsp824(x);
  debug_printf("x=%{dsp824} 1/x=%{dsp824} (dsp824->dsp824)", x, invx);
}

void test_inverse_dsp824_dsp1616(dsp824 x)
{
  dsp1616 invx = inverse_dsp824_dsp1616(x);
  debug_printf("x=%{dsp824} 1/x=%{dsp1616} (dsp824->dsp1616)", x, invx);
}


void test_powers_dsp824(dsp824 x)
{
  dsp824 y = DSP824(1.000);
  int remaining = 30;
  int iteri = 0;
  while (remaining>0) {
    debug_printf("%{dsp824}**%d = %{dsp824}", x, iteri, y);
    y = mulsat_dsp824_dsp824_dsp824(y, x);
    remaining--;
    iteri++;
    if (y > DSP824(127.0)) remaining = min(remaining, 3);
  }
}

void test_powers_dsp230(dsp230 x)
{
  dsp230 y = DSP230(1.000);
  int remaining = 30;
  int iteri = 0;
  while (remaining>0) {
    debug_printf("%{dsp230}**%d = %{dsp230}", x, iteri, y);
    y = mulsat_dsp230_dsp230_dsp230(y, x);
    remaining--;
    iteri++;
    if (y > DSP824(127.0)) remaining = min(remaining, 3);
    else if (y==0) remaining = min(remaining, 1);
  }
}

__attribute__((__noinline__))
dsp824 test_mul1(dsp824 a, dsp824 b)
{
  return mulsat_dsp824_dsp824_dsp824(a,b);
}

void test_dspcore()
{
  int i;
  debug_printf("dsp_test");

  {
    test_inverse_dsp824_dsp824(DSP824(3.0));
    test_inverse_dsp824_dsp824(DSP824(0.3));
    test_inverse_dsp824_dsp824(DSP824(0.05));

    test_inverse_dsp824_dsp1616(DSP824(3.0));
    test_inverse_dsp824_dsp1616(DSP824(0.3));
    test_inverse_dsp824_dsp1616(DSP824(0.05));
    test_inverse_dsp824_dsp1616(DSP824(0.0005));
  }

  debug_printf("1.2*1.5 = %{dsp824}", test_mul1(DSP824(1.2), DSP824(1.5)));

  {
    dsp824 x = DSP824(1.0);
    dsp824 y = DSP824(1.0);
    dsp824 z = -DSP824(-1.0);

    debug_printf("Should be all one: %{dsp824} %{dsp824} %{dsp824}", x, y, z);
  }

  {
    static dsp824 values[3] = {
      DSP824(1.0),
      DSP824(1.75),
      DSP824(M_PI),
    };
    for (i=0; i<sizeof(values)/sizeof(values[0]); i++) {
      debug_printf("Format: %%+6.3{dsp824} = %+6.3{dsp824}", values[i]);
      debug_printf("Format: %%+.3{dsp824} = %+.3{dsp824}", values[i]);
      debug_printf("Format: %%{dsp824} = %{dsp824}", values[i]);
    }
  }

  test_powers_dsp824(DSP824(1.500));
  test_powers_dsp824(DSP824(-1.500));
  test_powers_dsp824(DSP824(+127.9999));
  test_powers_dsp824(DSP824(-127.9999));

  test_powers_dsp230(DSP230(0.037));

}
