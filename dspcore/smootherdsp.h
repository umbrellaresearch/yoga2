#pragma once
#include "./dspcore.h"

typedef struct smoother2_coeffs_dsp824 {
  dsp824 b0, b1, b2;
  dsp824 a0, a1;
} smoother2_coeffs_dsp824;

typedef struct smoother2_state_dsp824 {
  dsp824 x0, x1, x2;
  dsp824 y0, y1, y2;
} smoother2_state_dsp824;

dsp824 smoother2_dsp824_run(smoother2_coeffs_dsp824 *c, smoother2_state_dsp824 *s, dsp824 v);
void smoother2_dsp824_clear(smoother2_state_dsp824 *s, dsp824 v);
dsp824 smoother2_dsp824_deriv(smoother2_state_dsp824 *s);

#include "build.src/smoother_coeffs.h"

void smoother2_dsp824_setup_biquad(smoother2_coeffs_dsp824 *c, dsp824 w0, dsp824 q);
