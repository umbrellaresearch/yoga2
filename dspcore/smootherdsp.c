#include "./smootherdsp.h"

#define APPROX_TIMESTEP 0.001

dsp824 smoother2_dsp824_run(smoother2_coeffs_dsp824 *c, smoother2_state_dsp824 *s, dsp824 v) 
{
  s->x0 = s->x1;
  s->x1 = s->x2;
  s->x2 = v;
  s->y0 = s->y1;
  s->y1 = s->y2;

  dsp824 newy = (mulsat_dsp824_dsp824_dsp824(s->x0, c->b0) +
                 mulsat_dsp824_dsp824_dsp824(s->x1, c->b1) +
                 mulsat_dsp824_dsp824_dsp824(s->x2, c->b2) -
                 mulsat_dsp824_dsp824_dsp824(s->y0, c->a0) -
                 mulsat_dsp824_dsp824_dsp824(s->y1, c->a1));
  s->y2 = newy;
  return newy;
}

void smoother2_dsp824_clear(smoother2_state_dsp824 *s, dsp824 v)
{
  s->x0 = s->x1 = s->x2 = s->y0 = s->y1 = s->y2 = v;
}

dsp824 smoother2_dsp824_deriv(smoother2_state_dsp824 *s)
{
  return (s->y2 - s->y1) * (S32)(1.0/APPROX_TIMESTEP);
}

void smoother2_dsp824_setup_biquad(smoother2_coeffs_dsp824 *c, dsp824 w0, dsp824 q)
{
  dsp230 w0_230 = conv_dsp824_dsp230(w0);
  dsp230 sinw0=0, cosw0=0;
  sincos_dsp230_dsp230_dsp230(w0_230, &sinw0, &cosw0);
  
  dsp230 alpha = div_dsp230_dsp824_dsp230(sinw0, 2*q);
  dsp824 a0 = DSP824(1.0) + conv_dsp230_dsp824(alpha);
  dsp824 inv_a0 = div_dsp824_dsp824_dsp824(DSP824(1.0), a0);

#if !defined(__EMBEDDED__)
  eprintf("w0=%0.9f cosw0=%0.9f sinw0=%0.9f alpha=%0.9f a0=%0.9f inv_a0=%0.9f\n",
          w0 * (1.0/(1<<24)),
          cosw0 * (1.0 / (1<<30)),
          sinw0 * (1.0 / (1<<30)),
          alpha * (1.0 / (1<<30)),
          a0 * (1.0 / (1<<24)),
          inv_a0 * (1.0 / (1<<24)));
#endif
  
  c->b0 = mul_dsp230_dsp824_dsp824(((DSP230(1.0) - cosw0) / 2), inv_a0);
  c->b1 = mul_dsp230_dsp824_dsp824((DSP230(1.0) - cosw0), inv_a0);
  c->b2 = mul_dsp230_dsp824_dsp824(((DSP230(1.0) - cosw0) / 2), inv_a0);
  
  c->a0 = mul_dsp230_dsp824_dsp824((DSP230(1.0) - alpha), inv_a0);
  c->a1 = mul_dsp230_dsp824_dsp824((-2 * cosw0), inv_a0);
}

void smoother2_dsp824_setup_butt_sml(smoother2_coeffs_dsp824 *c)
{
  /*
    A simple butterworth filter with tc=0.318
    b=[+0.430972, +0.861943, +0.430972]
    a=[+0.217307, +0.506579, +1.000000]
  */

  c->b0 = DSP824(+0.430972);
  c->b1 = DSP824(+0.861943); 
  c->b2 = DSP824(+0.430972);
  c->a0 = DSP824(+0.217307);
  c->a1 = DSP824(+0.506579);
}

