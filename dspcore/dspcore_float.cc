#include "./dspcore.h"

#include "tlbcore/common/numerical.h"

#define MAXS16 0x7fff
#define MAXS32 0x7fffffff
#define MAXS64 0x7fffffffffffffffLL

#define MAKE_TODSP_FT(QA, QB, QT, FT)                               \
  dsp##QA##QB conv_##FT##_dsp##QA##QB(FT x)                         \
  {                                                                 \
    if (isnan(x)) return (dsp##QA##QB)(S##QT)0;                     \
    if (x >= (FT)(MAXS##QT) * (1.0 / (FT)((S##QT)1 << (QB)))        \
        || x <= -(FT)(MAXS##QT) * (1.0 / (FT)((S##QT)1 << (QB)))) { \
      throw overflow_error(#FT " to dsp" #QA #QB " overflow");      \
    }                                                               \
    return (dsp##QA##QB)(S##QT)(x * (FT)((S##QT)1 << (QB)));        \
  }

#define MAKE_FROMDSP_FT(QA, QB, QT, FT) \
  FT conv_dsp##QA##QB##_##FT(dsp##QA##QB x) { return (FT)(S##QT)x * (1.0 / (FT)((S##QT)1 << (QB))); }

#define MAKE_CONV(QA, QB, QT)        \
  MAKE_TODSP_FT(QA, QB, QT, float)   \
  MAKE_TODSP_FT(QA, QB, QT, double)  \
  MAKE_FROMDSP_FT(QA, QB, QT, float) \
  MAKE_FROMDSP_FT(QA, QB, QT, double)

MAKE_CONV(4, 12, 16);
MAKE_CONV(2, 30, 32);
MAKE_CONV(8, 24, 32);
MAKE_CONV(16, 16, 32);
MAKE_CONV(40, 24, 64);
MAKE_CONV(32, 32, 64);
MAKE_CONV(24, 40, 64);
MAKE_CONV(18, 46, 64);
MAKE_CONV(16, 48, 64);
MAKE_CONV(10, 54, 64);
MAKE_CONV(4, 60, 64);
