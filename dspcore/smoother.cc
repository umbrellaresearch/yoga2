#include "./smoother.h"

#define APPROX_TIMESTEP 0.001

double smoother2Run(Smoother2Coeffs &c, Smoother2State &s, double v)
{
  s.x0 = s.x1;
  s.x1 = s.x2;
  s.x2 = v;
  s.y0 = s.y1;
  s.y1 = s.y2;

  double newy = (s.x0 * c.b0 + s.x1 * c.b1 + s.x2 * c.b2 - s.y0 * c.a0 - s.y1 * c.a1);
  s.y2 = newy;
  return newy;
}
void smoother2Clear(Smoother2State &s, double v)
{
  s.x0 = s.x1 = s.x2 = s.y0 = s.y1 = s.y2 = v;
}
double smoother2Deriv(Smoother2State &s)
{
  return (s.y2 - s.y1) * (1.0 / APPROX_TIMESTEP);
}

Smoother2Coeffs smoother2SetupBiquad(double w0, double q)
{
  double cosw0 = cos(w0);
  double sinw0 = sin(w0);

  double alpha = sinw0 / (2.0 * q);
  if (0) eprintf("w0=%0.9f cosw0=%0.9f sinw0=%0.9f, alpha=%0.9f\n", w0, cosw0, sinw0, alpha);

  double a0 = 1 + alpha;

  double a1 = (-2 * cosw0) / a0;
  double a2 = (1 - alpha) / a0;
  double b0 = ((1 - cosw0) / 2) / a0;
  double b1 = (1 - cosw0) / a0;
  double b2 = ((1 - cosw0) / 2) / a0;

  Smoother2Coeffs ret;
  ret.b2 = b0;
  ret.b1 = b1;
  ret.b0 = b2;

  ret.a0 = a2;
  ret.a1 = a1;

  return ret;
}
