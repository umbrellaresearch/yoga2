
#vertex
uniform mat4 ProjMtx;
uniform mat4 ViewMtx;

in vec3 Position;
in vec4 Color;

out vec4 Frag_Color;

void main()
{
  Frag_Color = Color;
  gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
  gl_PointSize = 3.0;
}


#fragment

in vec4 Frag_Color;

out vec4 Out_Color;

void main()
{
  Out_Color = Frag_Color;
  //Out_Color = vec4(1.0, 0.5, 0.25, 1.0);
}
