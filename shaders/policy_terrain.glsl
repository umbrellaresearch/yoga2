
#vertex
uniform mat4 ProjMtx;
uniform mat4 ViewMtx;

in vec3 Position;
in vec3 Normal;
in vec4 Color;

out vec3 Frag_Pos;
out vec3 Frag_Normal;
out vec4 Frag_Color;
out vec3 Frag_LightPos;
out vec3 Frag_RawPos;

void main()
{
  Frag_Color = Color;
  Frag_Normal = mat3(ViewMtx) * Normal;
  Frag_RawPos = Position;
  Frag_Pos = (ViewMtx * vec4(Position, 1)).xyz;
  Frag_LightPos = (ViewMtx * vec4(1, -1, 5, 1)).xyz;
  gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
}


#fragment

in vec3 Frag_Pos;
in vec3 Frag_Normal;
in vec4 Frag_Color;
in vec3 Frag_LightPos;
in vec3 Frag_RawPos;

out vec4 Out_Color;

void main()
{
  vec3 norm = normalize(Frag_Normal);
  vec3 lightDir = normalize(Frag_LightPos - Frag_Pos);
  vec3 viewDir = normalize(-Frag_Pos);
  vec3 reflectDir = reflect(-lightDir, norm);

  float alt = Frag_RawPos.z * 12.0;
  float fwAlt = fwidth(alt);
  float contourDistancePixels = abs(fract(alt+0.5) - 0.5) / clamp(fwAlt, 0.001, 100);

  /*
    Surf effect at 0 altitude
  */
  float surf = clamp(1.0 - abs((alt + 0.1)/0.1), 0, 1);

  /*
    Add an underwater effect where we cut the red and green parts of the light
    more than the blue.
  */
  float underwater = (alt < 0) ? 0.1 - 0.05*alt : 0.0;

  float ambient = 0.3;
  float diffuse = 0.7 * max(dot(norm, lightDir), 0.0);

  float contour = 0.5 * clamp(1.0 - pow(2.0 * contourDistancePixels, 2), 0, 1);

  vec4 lightCol = vec4(
    (ambient + diffuse) / (1.0 + 2.0 * underwater), 
    (ambient + diffuse) / (1.0 + 1.5 * underwater), 
    (ambient + diffuse) / (1.0 + 0.5 * underwater), // blue fades less
    1);

  vec4 glowCol = vec4(
    (contour + surf) / (1.0 + 2.0 * underwater), 
    (contour + surf) / (1.0 + 1.5 * underwater), 
    (contour + surf) / (1.0 + 0.5 * underwater),
    1);

  vec4 specCol = pow(clamp(dot(viewDir, reflectDir) - 3.0*underwater, 0, 1), 16) * vec4(0.85, 0.85, 0.90, 0);

  Out_Color = specCol + glowCol + lightCol * Frag_Color;
}
