#include "common/std_headers.h"
#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"
#include "./test_utils.h"

ofstream testlog("testlog.md");


int main(int argc, char **argv)
{
  apr_initialize();
  atexit(apr_terminate);
  int result = Catch::Session().run(argc, argv);
  return result;
}
