
#define CATCH_CONFIG_EXTERNAL_INTERFACES
#include "./test_utils.h"
#include "catch2/catch.hpp"

static string curTestName;

bool isMatch(string const &l, string const &re)
{
  regex re1(re);
  return regex_search(l, re1);
}

string atline(int line, string const &error)
{
  return "test.*:"s + to_string(line) + ":.*"s + error;
}


shared_ptr<YogaCompilation> yogac(string const &code, char const *file, int line)
{
  testlog << "```js\n" << code << "\n```\n";
  auto reg = make_shared<YogaCompilation>();
  reg->verbose = 0;
  reg->doCheckpointDiagLog = false;

  /*
    If these are referred to in the text of the test case, define them
   */
  if (regex_search(code, regex("TestState"))) {
    if (!reg->parseText("preamble", R"(
      struct TestState {
        R foo;
        R bar;
        R buz;
        ...
      }
      )", ParseToplevel)) goto fail;
  }
  if (regex_search(code, regex("TestConfig"))) {
    if (!reg->parseText("preamble", R"(
      struct TestConfig {
        R wug;
        ...
      }
      )", ParseToplevel)) goto fail;
  }
  if (regex_search(code, regex("Foobar"))) {
    if (!reg->parseText("preamble", R"(
      struct Foobar {
        R foo;
        R bar;
      }
      )", ParseToplevel)) goto fail;
  }

  {
    auto fakeFn = string(file) + ":" + repr(line);
    if (0) cerr << "fakeFn " + fakeFn + "\n";

    if (!reg->parseText(fakeFn, code, ParseToplevel)) goto fail;
  }

  if (!reg->run()) goto fail;
  if (reg->verbose >= 1) reg->dumpToFiles("jit.log", "Test success");

  return reg;

fail:
  auto diag = reg->diagLog.str();
  testlog << "\n* Diagnostics:\n\n```txt\n" << diag << "\n```\n";
  return reg;
}


bool checkerrs(YogaCompilation *reg, vector<string> const &errors)
{
  auto diagLog = reg->diagLog.str();

  if (errors.empty()) {
    if (diagLog.empty()) return true;
    reg->dumpToFiles("jit.log", "Test failure");
    cerr << "Unexpected errors:\n" + diagLog;
    return false;
  }
  else {
    for (auto &errit : errors) {
      if (!isMatch(diagLog, errit)) {
        cerr << "Failed to find expected error:\n" + errit + "\n";
        if (!diagLog.empty()) {
          cerr << "But there were these errors:\n" + diagLog + "\n";
        }
        reg->dumpToFiles("jit.log", "Test failure");
        return false;
      }
    }
    return true;
  }
}

bool checkerrs(YogaContext const &ctx, vector<string> const &errors)
{
  return checkerrs(ctx.reg, errors);
}

bool checkerrs(shared_ptr<YogaCompilation> reg, vector<string> const &errors)
{
  return checkerrs(reg.get(), errors);
}


bool checkGradient(YogaCaller f, vector<YogaValue> const &inVals,
  string const &outName, string const &inName, R expectedGrad)
{
  bool ok = true;
  const int verbose = 0;
  YogaPool tmpmem;

  R astonishment = 0.0;
  vector<YogaValue> outVals;
  f.allocOutTypes(outVals, tmpmem.it);
  f(outVals, inVals, astonishment, 0.0, tmpmem.it);

  if (verbose) cerr << "f=" + f.funcName +  " in=" + repr(inVals) + " out=" + repr(outVals) + "\n";

  if (!f.f->gradAddr) throw new runtime_error("No fGrad defined");

  vector<YogaValue> inValsGradOut;
  vector<YogaValue> outValsGradIn;
  f.allocInTypes(inValsGradOut, tmpmem.it);
  f.allocOutTypes(outValsGradIn, tmpmem.it);
  vector<YogaValue> outValsGradZero;
  for (auto [argType, argname] : f.f->outArgs) {
    outValsGradZero.emplace_back(f.reg, argType, tmpmem.it);
  }

  vector<R> paramsGrad;
  f.allocParams(paramsGrad);

  f.grad(
    outVals,
    outValsGradZero,
    inVals,
    inValsGradOut,
    paramsGrad,
    astonishment,
    0.0,
    tmpmem.it);
  if (verbose >= 1) {
    cerr << "  With zero "s +
      " in=" + repr(inVals) + " \u2207 " + repr(inValsGradOut) +
      " out=" + repr(outVals) + " \u2207 " + repr(outValsGradIn) +
      " paramsGrad=" + repr(paramsGrad) +
      "\n";
  }

  auto [inNameArg, inNameMember] = splitAtDotNumber(inName, 1);
  auto [outNameArg, outNameMember] = splitAtDotNumber(outName, 1);

  auto inArgIndex = f.f->findInArgOrThrow(inNameArg);
  auto outArgIndex = f.f->findOutArgOrThrow(outNameArg);
  
  R estimatedGrad = 0.0;

  if (1) {
    auto inValsPlus = dup(inVals, tmpmem.it);
    auto inValsMinus = dup(inVals, tmpmem.it);

    R eps = 0.0001;
    R orig = 0.0;
    inVals[inArgIndex].rd(inNameMember, orig);
    inValsPlus[inArgIndex].wr(inNameMember, orig + eps);
    inValsMinus[inArgIndex].wr(inNameMember, orig - eps);

    vector<YogaValue> outValsPlus;
    f.allocOutTypes(outValsPlus, tmpmem.it);
    f(outValsPlus, inValsPlus, astonishment, 0.0, tmpmem.it);

    vector<YogaValue> outValsMinus;
    f.allocOutTypes(outValsMinus, tmpmem.it);
    f(outValsMinus, inValsMinus, astonishment, 0.0, tmpmem.it);

    R outPlus=0.0, outMinus=0.0;
    outValsPlus[outArgIndex].rd(outNameMember, outPlus);
    outValsMinus[outArgIndex].rd(outNameMember, outMinus);

    estimatedGrad = (outPlus - outMinus) / (2.0 * eps);
  }

  vector<R> estimatedParamsGrad(f.reg->paramsByIndex.size());
  for (int parami=0; parami < estimatedParamsGrad.size(); parami++) {

    R eps = 0.0001;
    R orig = f.reg->paramValues[parami];

    f.reg->paramValues[parami] = orig + eps;
    vector<YogaValue> outValsPlus;
    f.allocOutTypes(outValsPlus, tmpmem.it);
    f(outValsPlus, inVals, astonishment, 0.0, tmpmem.it);

    f.reg->paramValues[parami] = orig - eps;
    vector<YogaValue> outValsMinus;
    f.allocOutTypes(outValsMinus, tmpmem.it);
    f(outValsMinus, inVals, astonishment, 0.0, tmpmem.it);

    f.reg->paramValues[parami] = orig;

    R outPlus=0.0, outMinus=0.0;
    outValsPlus[outArgIndex].rd(outNameMember, outPlus);
    outValsMinus[outArgIndex].rd(outNameMember, outMinus);

    estimatedParamsGrad[parami] = (outPlus - outMinus) / (2.0 * eps);
  }


  if (1) {
    vector<YogaValue> inValsGrad, outValsGrad;
    for (auto [argType, argname] : f.f->outArgs) {
      outValsGrad.emplace_back(f.reg, argType, tmpmem.it);
    }
    for (auto [argType, argname] : f.f->inArgs) {
      inValsGrad.emplace_back(f.reg, argType, tmpmem.it);
    }
    outValsGrad[outArgIndex].wr(outNameMember, 1.0);

    vector<R> paramsGrad;
    f.allocParams(paramsGrad);
    f.grad(outVals, outValsGrad, inVals, inValsGrad, paramsGrad, astonishment, 0.0, tmpmem.it);

    R computedGrad=0.0;
    inValsGrad[inArgIndex].rd(inNameMember, computedGrad);

    auto gradOk = true;
    if (!isnan(expectedGrad)) {
      if (abs(computedGrad - expectedGrad) > 1e-6) gradOk = false;
    }
    if (abs(computedGrad - estimatedGrad) > 1e-3) gradOk = false;

    for (int parami=0; parami < estimatedParamsGrad.size(); parami++) {
      if (abs(paramsGrad[parami] - estimatedParamsGrad[parami]) > 1e-3) gradOk = false;
    }

    if (!gradOk) ok = false;
    if (verbose || !gradOk) {
      cerr << (gradOk ? "  "s : "Mismatch: "s) + inName + " to " + outName +
        " in=" + repr(inVals) + " \u2207 " + repr(inValsGrad) +
        " out=" + repr(outVals) + " \u2207 " + repr(outValsGrad) + "\n";
      cerr <<
        "    computed=  "s + repr(computedGrad) + "\n" +
        "    estimated= "s + repr(estimatedGrad) + "\n" +
        "    expected=  "s + repr(expectedGrad) + "\n" +
        "\n";
      cerr << 
        "    paramsGrad=" + repr(paramsGrad) + "\n" +
        "    estimated= " + repr(estimatedParamsGrad) + "\n";
    }
  }

  return ok;
}




struct TestUtilsListener : Catch::TestEventListenerBase {

  using TestEventListenerBase::TestEventListenerBase; // inherit constructor

  void testCaseStarting( Catch::TestCaseInfo const& testInfo ) override {
    curTestName = testInfo.name;
  }
  
  void testCaseEnded( Catch::TestCaseStats const& testCaseStats ) override {
    curTestName = "";
  }
};
CATCH_REGISTER_LISTENER( TestUtilsListener )
