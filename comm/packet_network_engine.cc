#include "./packet_network_engine.h"

#include "common/uv_wrappers.h"
#include "../lib/core_types.h"

PacketNetworkEngine::PacketNetworkEngine() 
{
  // id_cnt = int(fmod(realtime()*1000.0, 1000000.0));
  tx_version = "yoga 1.0";
}

PacketNetworkEngine::~PacketNetworkEngine()
{
  if (in_stream) delete in_stream;
  if (out_stream && out_stream != in_stream) delete out_stream;
  if (stderr_stream) delete stderr_stream;
}

void PacketNetworkEngine::setupStreams()
{
  out_stream->write(tx_version + "\n", [](int status) {
  });
  in_stream->read_boxed_start([this, thisp = shared_from_this()](ssize_t err, packet *rx) {
    if (rx_version.empty()) {
      rx_version = in_stream->rx_header;
    }
    uvUiActive = true;
    if (err == UV_EOF) {
      rxEof();
      in_stream->read_stop();
    }
    else if (err < 0) {
      throw uv_error("PacketNetworkEngine: read", err);
    }
    else {
      rxPacket(*rx);
    }
  });
  streamsReady = true;
  callPopAll(onReady);
}

void PacketNetworkEngine::afterSetRunning()
{
  setupHandlers();
}

void PacketNetworkEngine::setupHandlers()
{
  addApiMethod("ping", [](packet &rx, PacketApiCb &&cb) {
    PingPong req;
    rx.get(req);

    cb.ok([req](packet &tx) {
      PingPong rep;
      rep.txTs = req.txTs;
      rep.rxTs = realtime();
      tx.add(rep);
    });
  });
}

void PacketNetworkEngine::update(EngineUpdateContext &ctx)
{
  GenericEngine::update(ctx);
}

void PacketNetworkEngine::afterSetStopped()
{
  if (verbose >= 1) console("afterSetStopped");
  if (in_stream) {
    in_stream->read_stop();
    in_stream->close();
  }
  if (out_stream) {
    out_stream->close();
  }

  if (stderr_stream) {
    stderr_stream->read_stop();
    stderr_stream->close();
  }
  failOutstandingRpcs("stopped");
  callPopAll(onRxEof);
  api.clear();
}

void PacketNetworkEngine::failOutstandingRpcs(string const &err)
{
  assert(!err.empty());
  for (auto &it : replyCallbacks) {
    if (it.second) {
      packet fake;
      it.second(err, fake);
      it.second = nullptr;
    }
  }
  if (verbose >= 2) console("Clear all callbacks"s);
  replyCallbacks.clear();
  repliesOutstanding = 0;
}

void PacketNetworkEngine::rxPacket(packet &rx)
{
  char subtype;
  rx.get(subtype);
  if (subtype == 'R') {
    string method;
    rx.get(method);
    int id{0};
    rx.get(id);
    if (verbose >= 1) console("> rpc."s + to_string(id) + "("s + method + ") size=" + to_string(rx.remaining()));
    auto f = api[method];
    if (!f) {
      if (verbose >= 0 && warningFilter("unknownMethod")) console("rx request with unknown method="s + shellEscape(method));
      packet tx;
      tx.add('e');
      tx.add(id);
      tx.add("RPC method " + shellEscape(method) + " not found"s);
      txPacket(tx);
      return;
    }
    f(rx, PacketApiCb(shared_from_this(), id));
    if (rx.remaining() > 0) {
      console("Leftover bytes in rpc."s + to_string(id) + "(" + method + ") remaining=" + to_string(rx.remaining()));
    }
  }
  else if (subtype == 'o') {
    int id{0};
    rx.get(id);
    if (verbose >= 1) console("> ok."s + to_string(id) + "(...) size=" + to_string(rx.remaining()));
    auto f = replyCallbacks[id];
    if (!f) {
      if (verbose >= 0 && warningFilter("unknownErr")) console("rx OK reply with unknown id="s + to_string(id));
      return;
    }
    replyCallbacks[id] = nullptr;
    f("", rx);
    repliesOutstanding--;
    clientRepliesReceived++;
    if (rx.remaining() > 0) {
      console("Leftover bytes in ok."s + to_string(id) + " remaining=" + to_string(rx.remaining()));
    }
  }
  else if (subtype == 's') {
    int id{0};
    rx.get(id);
    if (verbose >= 1) console("> stream."s + to_string(id) + "(...) size=" + to_string(rx.remaining()));
    auto f = replyCallbacks[id];
    if (!f) {
      if (verbose >= 0 && warningFilter("unknownStream")) console("rx streaming reply with unknown id="s + to_string(id));
      return;
    }
    f("", rx);
    if (rx.remaining() > 0) {
      console("Leftover bytes in stream."s + to_string(id) + " remaining=" + to_string(rx.remaining()));
    }
  }
  else if (subtype == 'e') {
    int id{0};
    rx.get(id);
    auto f = replyCallbacks[id];
    if (!f) {
      if (verbose >= 0 && warningFilter("unknownErr")) console("rx error reply with unknown id="s + to_string(id));
      return;
    }
    replyCallbacks[id] = nullptr;
    string err;
    rx.get(err);
    if (verbose >= 1) console("> err."s + to_string(id) + "(" + err + ") size=" + to_string(rx.remaining()));
    f(err, rx);
    repliesOutstanding--;
    clientErrorsReceived++;
    if (rx.remaining() > 0) {
      console("Leftover bytes in err."s + to_string(id) + " remaining=" + to_string(rx.remaining()));
    }
  }
  else {
    if (verbose >= 0 && warningFilter("unknownSubtype")) console("rx packet with unknown subtype");
  }
}

string PacketNetworkEngine::progress()
{
  return label + ": " + 
      repr(clientRepliesReceived) + " / " + 
      repr(clientRequestsSent) +
      (repliesOutstanding ? "..." : "");
}

void PacketApiCb::err(string const &error)
{
  if (id) {
    if (engine->verbose >= 0) engine->console("< err."s + to_string(id) + "("s + error + ")");
    packet tx;
    tx.add('e');
    tx.add(id);
    auto suffix = " (on " + YogaLayout::instance().localHostName + ")";
    if (!endsWith(error, suffix)) {
      tx.add(error + suffix);
    }
    else {
      tx.add(error);
    }
    engine->txPacket(tx);
    id = 0;
  }
}

void PacketApiCb::ok(PacketFiller fillBody)
{
  if (id) {
    if (engine->verbose >= 1) engine->console("< ok."s + to_string(id) + "(...)");
    packet tx;
    tx.add('o');
    tx.add(id);
    fillBody(tx);
    engine->txPacket(tx);
    id = 0;
  }
}

void PacketApiCb::ok()
{
  if (id) {
    if (engine->verbose >= 1) engine->console("< ok."s + to_string(id) + "()");
    packet tx;
    tx.add('o');
    tx.add(id);
    engine->txPacket(tx);
    id = 0;
  }
}


void PacketApiCb::stream(PacketFiller fillBody)
{
  if (id) {
    if (engine->verbose >= 1) engine->console("< stream."s + to_string(id) + "(...)");
    packet tx;
    tx.add('s');
    tx.add(id);
    fillBody(tx);
    engine->txPacket(tx);
  }
}


void PacketNetworkEngine::rpc(
  string const &method, 
  PacketFiller fillBody, 
  PacketRpcCb cb)
{
  int id = id_cnt;
  id_cnt = id_cnt % 1000000000 + 1;
  replyCallbacks[id] = cb;
  repliesOutstanding++;
  clientRequestsSent++;

  if (verbose >= 1) console("< rpc."s + to_string(id) + "("s + method + ")");

  packet *tx = new packet();
  tx->add('R');
  tx->add(method);
  tx->add(id);
  fillBody(*tx);
  if (!streamsReady) {
    onReady.push_back([this, tx]() {
      txPacket(*tx);
      delete tx;
    });
  }
  else {
    txPacket(*tx);
    delete tx;
  }
}

void PacketNetworkEngine::rxEof()
{
  if (verbose >= 1) console("> eof");
  // If someone was waiting for ready, we're not going to get any readier.
  callPopAll(onReady);
  failOutstandingRpcs("Lost connection to " + label);
  callPopAll(onRxEof);
  if (!noEofExit) {
    stop();
  }
}

// ------------

void PacketNetworkEngine::txPacket(packet &tx)
{
  if (!isActive()) {
    if (verbose >= 0) console("txPacket: discard, not running");
    return;
  }

  if (verbose >= 1 && tx.size() > 500000) {
    console("txPacket large packet ("s + to_string(tx.size()) + ")");
  }
  else if (verbose >= 3) {
    console("txPacket packet ("s + to_string(tx.size()) + ")");
  }

  out_stream->write_boxed(tx, [this, thisp = shared_from_this()](int status) {
    if (status == UV_ECANCELED) {
      if (isActive()) {
        console("txPacket: uv_write: "s + uv_strerror(status));
        rxEof();
      }
    }
    else if (status < 0) {
      if (warningFilter("uv_write")) console("txPacket: uv_write: "s + uv_strerror(status));
      failOutstandingRpcs("uv_write: "s + uv_strerror(status));
      rxEof();
    }
  });
}


void PacketNetworkEngine::addApiMethod(
    string const &method,
    PacketApiFunction const &f)
{
  api[method] = f;
}

size_t PacketNetworkEngine::workOutstanding() const
{
  return isActive() ? repliesOutstanding : 0;
}

// ----------

void PacketNetworkEngine::sendPing()
{
  rpc("ping",
    [](packet &tx) {
      PingPong req;
      req.txTs = realtime();
      tx.add(req);
    },
    [this, thisp = shared_from_this()](string const &error, packet &rx) {
      if (error.size()) {
        console("ping: error "s + error);
        return;
      }
      PingPong rep;
      rx.get(rep);
      double rx2Ts = realtime();
      console("RPC time sync: "s + repr_0_6f(rep.rxTs - rep.txTs) + " "s + repr_0_6f(rx2Ts - rep.txTs));
  });
}
