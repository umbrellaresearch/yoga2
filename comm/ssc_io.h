#pragma once
#include "common/packetbuf.h"
#include "numerical/numerical.h"
#include "../jit/runtime.h"

const double SSC_INVERSE_TIME_QUANTUM = 1048576;                // binary microseconds
const double SSC_TIME_QUANTUM = 1.0 / SSC_INVERSE_TIME_QUANTUM; // binary microseconds


struct SscRawDump {
  double ts;
  vector<double> values;
};

DECL_PACKETIO(SscRawDump);

template <class T>
double sscConvTimestamp(T &engine, double rxTime, U64 sampleTicks, U64 pktTicks)
{
  double sampleAge = (S64)(pktTicks - sampleTicks) * (1.0 / 66.0e6);
  if (sampleAge < 0 || sampleAge > 0.010) {
    engine.console(
      "sscConvTimestamp: "s + engine.remoteAddrDesc + 
      ": sampleAge="s + repr_0_6f(sampleAge) +
      " (sampleTicks="s + repr_016x(sampleTicks) +
      " pktTicks="s + repr_016x(pktTicks) +
      ")");
  }
  return round((rxTime - sampleAge) * SSC_INVERSE_TIME_QUANTUM) * SSC_TIME_QUANTUM;
}

template <class T>
void rxSscTimestamp(T &engine, packet &rx, double &ts, double rxTime, U64 pktTicks)
{
  U64 sampleTicks;
  rx.get(sampleTicks);
  if (sampleTicks != 0) {
    R tsExt{0.0};
    tsExt = sscConvTimestamp(engine, rxTime, sampleTicks, pktTicks);
    ts = engine.fromExternalTime(tsExt);
  }
  else {
    ts = 0.0;
  }
}

// ----------------------------------------------------------------------


inline void rxDouble(packet &rx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value;
  rx.get(value);
  acc.wr(yv, value);
}

inline void txDouble(packet &tx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value;
  acc.rd(yv, value);
  tx.add(value);
}


inline void rxDoubleVec(packet &rx, YogaPtrAccessor &acc, YogaValue const &yv, size_t n)
{
  auto p = acc.ptr(yv);
  for (size_t i=0; i < n; i++) {
    double value;
    rx.get(value);
    ((double *)p.buf)[i] = value;
  }
}

inline void txDoubleVec(packet &tx, YogaPtrAccessor &acc, YogaValue const &yv, size_t n)
{
  auto p = acc.ptr(yv);

  for (size_t i=0; i < n; i++) {
    auto value = ((double *)p.buf)[i];
    tx.add(value);
  }
}


inline void rxDsp824(packet &rx, double &value)
{
  S32 raw;
  rx.get(raw);
  value = (double)raw * (1.0 / 16777216.0);
}

inline void rxDsp824(packet &rx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value;
  rxDsp824(rx, value);
  acc.wr(yv, value);
}


inline void txDsp824(packet &tx, double const &value)
{
  S32 raw = (S32)(min(125.0, max(-125.0, value)) * 16777216.0);
  tx.add(raw);
}

inline void txDsp824(packet &tx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value;
  acc.rd(yv, value);
  txDsp824(tx, value);
}

inline void rxDsp1616(packet &rx, double &value)
{
  S32 raw = 0;
  rx.get(raw);
  value = (double)raw * (1.0 / 65536.0);
}

inline void rxDsp1616(packet &rx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value = 0.0;
  rxDsp1616(rx, value);
  acc.wr(yv, value);
}

inline void rxBool(packet &rx, double &value)
{
  U8 raw = 0;
  rx.get(raw);
  value = raw ? 1.0 : 0.0;
}

inline void rxBool(packet &rx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value = 0.0;
  rxBool(rx, value);
  acc.wr(yv, value);
}

inline void txDsp1616(packet &tx, double const &value)
{
  S32 raw = (S32)(min(32500.0, max(-32500.0, value)) * 65535.0);
  tx.add(raw);
}

inline void txDsp1616(packet &tx, YogaValueAccessor<double> &acc, YogaValue const &yv)
{
  double value;
  acc.rd(yv, value);
  txDsp1616(tx, value);
}

// ----------------------------------------------------------------------

#if 0
void rxSscRawDump(T &engine, packet &rx, SscRawDump &s, double rxTime, U64 pktTicks)
{
  rxSscTimestamp(engine, rx, s.ts, rxTime, pktTicks);

  U64 count = 0;
  rx.get(count);
  if (count > 1000) throw runtime_error("pktRxSscRawDump: Unreasonable size "s + to_string(count));
  s.values.resize(count);
  for (U64 i = 0; i < count; i++) {
    rxDsp824(engine, rx, s.values[i]);
  }
}
#endif
