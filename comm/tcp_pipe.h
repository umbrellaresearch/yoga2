#pragma once
#include "./packet_network_engine.h"

struct TcpPipe : PacketNetworkEngine {
  TcpPipe(string const &_host, string const &_port);
  ~TcpPipe();

  string host, port;

  void setupSocket();
  void afterSetWarming(R &delay) override;
};

