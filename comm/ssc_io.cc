#include "./ssc_io.h"
#include "common/packetbuf_types.h"

namespace packetio {
  string packet_get_typetag(SscRawDump const &x)
  {
    return "SscRawDump:1";
  }
  void packet_wr_value(packet &p, SscRawDump const &x)
  {
    packetio::packet_wr_value(p, x.ts);
    packetio::packet_wr_value(p, x.values);
  }
  void packet_rd_value(packet &p, SscRawDump &x)
  {
    packetio::packet_rd_value(p, x.ts);
    packetio::packet_rd_value(p, x.values);
  }
  std::function<void(packet &, SscRawDump &)> packet_rd_value_compat(SscRawDump const &x, string const &typetag)
  {
    if (typetag == "SscRawDump:1") {
      return static_cast<void(*)(packet &, SscRawDump &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(SscRawDump);
