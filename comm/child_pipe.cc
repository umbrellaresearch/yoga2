#include "./child_pipe.h"

ChildPipe::ChildPipe(
  vector<string> const &_execArgs,
  vector<string> const &_execEnv)
  : execArgs(_execArgs),
    execEnv(_execEnv)
{
}

void ChildPipe::afterSetWarming(R &delay)
{
  setupChild();
  if (execArgs.size() > 0) {
    if (execArgs[0] == "ssh") {
      delay = max(delay, 0.5); // enough to start ssh in most cases
    }
    else {
      delay = max(delay, 0.1);
    }
  }
}

void ChildPipe::setupChild()
{
  assert(child_proc == nullptr);
  if (execArgs.empty()) {
    throw runtime_error("PacketNetworkEngine::setupChild: no args"s);
  }

  remoteDesc = "";
  for (auto const &argit : execArgs) {
    if (remoteDesc.size() > 300) {
      remoteDesc += " ...";
      break;
    }
    if (!remoteDesc.empty()) remoteDesc += " ";
    remoteDesc += argit;
  }
  if (label == "GenericEngine") label = engineName; // + "(" + remoteDesc + ")";
  console("spawn "s + remoteDesc);

  bool doStderr = false;

  in_stream = new UvStream();
  out_stream = new UvStream();
  if (doStderr) stderr_stream = new UvStream();

  child_proc = new UvProcess(
      execArgs[0],
      execArgs,
      execEnv,
      out_stream,
      in_stream,
      stderr_stream,
      [this, thisp = shared_from_this()](int64_t exit_status, int term_signal) {
        if (term_signal != 0) {
          console("Child exit with "s + signal_name(term_signal));
        }
        else if (exit_status != 0) {
          console("Child exit code "s + to_string(exit_status));
        }
        else {
          console("Child exit ok"s);
        }
        delete child_proc;
        child_proc = nullptr;
      });
  setupStreams();

  if (stderr_stream) {
    stderr_stream->read_start([this, thisp = shared_from_this()](ssize_t nr, const uv_buf_t *buf) {
      if (nr == UV_EOF) {
        stderr_stream->read_stop();
      }
      else if (nr < 0) {
        throw uv_error("PacketNetworkEngine: read stderr", nr);
      }
      else {
        char *chunk_begin = buf->base;
        char *chunk_end = buf->base + nr;

        while (chunk_end > chunk_begin) {
          auto nl = static_cast<char *>(memchr(chunk_begin, 10, chunk_end - chunk_begin));
          if (nl == nullptr) {
            stderr_buf.append(chunk_begin, chunk_end - chunk_begin);
            chunk_begin = chunk_end;
          }
          else {
            stderr_buf.append(chunk_begin, nl - chunk_begin);
            console(stderr_buf);
            stderr_buf.clear();
            chunk_begin = nl + 1;
          }
        }
      }
    });
  }
}
