#pragma once
#include "../jit/compilation.h"
#include "../comm/child_pipe.h"
#include "./livestream_rx_engine.h"
#include "../studio/scope_model.h"

struct YogaDb;

struct YogaClient : enable_shared_from_this<YogaClient> {
  YogaClient(YogaContext const &_ctx);

  YogaContext ctx;
  string liveHost;
  shared_ptr<PacketNetworkEngine> toServer;
  shared_ptr<LivestreamRxEngine> rxEngine;
  shared_ptr<ScopeModel> model;

  shared_ptr<YogaRpcPool> blobFetcherPool;

  string clientError;
  R duration{0.0}; // unlimited
  bool isRemote{false};
  bool stopRequested{false};
  bool remoteRunning{false};
  int sentParamValueEpoch{0};

  R lastSentKbdTs{0.0};
  R lastSentPuckTs{0.0};

  std::function<void(string const &)> onTraceSavedAs;
  std::function<void(string const &)> onAcquireStatus;

  void runRemote();
  void stop();

  void fetchBlob(string const &host, BlobRef const &blobRef, std::function<void(string const &err, shared_ptr<Blob> blob)> cb);

  void rpcAcquire(function<void(string const &err)> cb);
  void rpcCompile(function<void(string const &err)> cb);
  void rpcStart(function<void(string const &err)> cb);
  void rpcControlledStop(function<void(string const &err)> cb);
  void rpcUpdateYogaParams(function<void(string const &err)> cb);
  void rpcSetInteractive(string const &seqName, YogaValue yv);
  void syncYogaParams();

  void warmupVideoSources(json &manifest);

  bool startBotd();

};
