#include "./livestream_rx_engine.h"
#include "../comm/child_pipe.h"
#include "common/packetbuf_types.h"
#include "nlohmann-json/json.hpp"

LivestreamRxEngine::LivestreamRxEngine()
{
}

void LivestreamRxEngine::setupDsts(json const &manifest)
{
  auto ltrace = trace.lock();
  if (!ltrace) {
    cerr << "LivestreamRxEngine::setupDsts: no trace\n";
    return;
  }
  for (auto &tsInfo : manifest["timeseqInfos"]) {
    string name = tsInfo.value("name", "");
    string typeName = tsInfo.value("type", "");
    YogaType *type = mkCtx("load").getType(typeName);
    if (!type) {
      console("Not loading "s + shellEscape(name) + ": no type named " + shellEscape(typeName));
      continue;
    }
    auto seq = make_shared<YogaTimeseq>(rti->reg.get(), type);
    dsts[name] = seq;
    if (verbose >= 1) console("will rx "s + shellEscape(name));

    ltrace->addTimeseq(name, seq, true);
  }
}

void LivestreamRxEngine::afterSetRunning()
{
  if (0) console("setRunning. robotPipe="s + (robotPipe ? robotPipe->engineName : "null") + "\n");
  lastRxTs = 0;
  if (robotPipe) {
    robotPipe->addApiMethod(
      "grow",
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb cb) {
        auto oldLastRxTs = lastRxTs;
        while (1) {
          auto c = rx.get_char();
          if (c == '.') {
            break;
          }
          else if (c == 'P') {
            string seqName;
            rx.get(seqName);

            auto seq = dsts[seqName];
            if (!seq) {
              console("grow: no dst " + shellEscape(seqName));
              cb.err("No such seqName");
              return;
            }

            double ts{0.0};
            rx.get(ts);
            lastRxTs = max(lastRxTs, ts);
            
            auto buf = seq->type->mkInstance();
            (*seq->type->packetRd)(rx, buf, seq->mem);
            seq->add(ts, buf);
          }
          else if (c == 'L') {
            if (auto ltrace = trace.lock()) {
              rx.get(ltrace->blobLocationsByChunkId);
            }
            else {
              return cb.err("trace missing");
            }
          }
          else {
            throw runtime_error("LivestreamRxEngine::start: Protocol error type=0x" + repr_02x(c));
          }
        }
        if (lastRxTs != oldLastRxTs) {
          for (auto &it : onGrows) {
            it(lastRxTs);
          }
        }
        uvUiActive = true;
        cb.ok();
      }
    );
  }

  GenericEngine::afterSetRunning();
}

