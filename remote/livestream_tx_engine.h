#pragma once
#include "../comm/packet_network_engine.h"
#include "../timeseq/timeseq.h"

struct LivestreamTxEngine : GenericEngine {

  LivestreamTxEngine();

  shared_ptr<PacketNetworkEngine> uiPipe;

  struct LivestreamSrc {
    string seqName;
    shared_ptr<YogaTimeseq> seq;
    R interval{0.0};
    R lastGrowTs{0.0};
  };
  vector<LivestreamSrc> srcs;
  size_t sourcei {0};
  R traceEndTs {0.0};
  R growInterval{0.1};

  R lastUpdateTime{0.0};
  R lastLiveActiveTime{0.0};
  int outstandingGrows{0};
  bool schemaSent{false};

  void afterSetRunning() override;

  void update(EngineUpdateContext &ctx) override;
  bool isNetwork() const override { return true; }

};
