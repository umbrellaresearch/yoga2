#pragma once
#include "../comm/packet_network_engine.h"
#include "../timeseq/timeseq.h"

struct LivestreamRxEngine : GenericEngine {

  LivestreamRxEngine();

  shared_ptr<PacketNetworkEngine> robotPipe;

  R lastRxTs{0.0};
  map<string, shared_ptr<YogaTimeseq>> dsts;

  vector<std::function<void(R lastRxTs)>> onGrows;

  void afterSetRunning() override;
  bool isNetwork() const override { return true; }
  
  void setupDsts(json const &manifest);

};
