#include "common/std_headers.h"
#include "../db/yoga_layout.h"
#include "../db/yoga_db.h"
#include "../jit/compilation.h"
#include "../jit/yoga_engine.h"
#include "../timeseq/trace.h"
#include "../comm/parent_pipe.h"
#include "./livestream_tx_engine.h"
#include "nlohmann-json/json.hpp"
#include <getopt.h>
#include <sys/file.h>

struct YogaServer : enable_shared_from_this<YogaServer> {
  YogaServer(YogaContext const &_ctx, shared_ptr<ParentPipe> _parent)
  : ctx(_ctx), parent(_parent)
  {
    sessionTok = getRandTok(5);
  }

  ~YogaServer()
  {
    if (!(lockFile < 0)) {
      close(lockFile);
    }
    logAccess("close");
  }

  bool authorized = false;
  bool requireAuth = false;
  string robotName;
  string lockFn;
  string sessionTok;
  string peerName;
  R maxDuration = 0.0;
  int lockFile=-1;

  void logConnection()
  {
    int rc = parent->in_stream->get_peername(peerName);
    if (rc < 0) {
      cerr << "get_peername on stdio: "s + uv_strerror(rc) + "\n";
      return;
    }
    logAccess("connected to " + peerName);
  }

  void setupHandlers()
  {

    parent->onRxEof.push_back([this, thisp=shared_from_this()]() {
      if (!(lockFile < 0)) {
        close(lockFile);
        lockFile = -1;
      }
      if (trace) {
        trace->stopAllThreads();
      }
    });

    parent->addApiMethod("controlledStop",
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb &&cb) {
        cerr << "yoga_botd: controlledStop\n";
        if (trace) {
          trace->rti->hardHalt = true;
          trace->rti->requestUpdate();
        }
      }
    );

    parent->addApiMethod("acquire", 
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb &&cb) {
        json acquireParams;
        rx.get(acquireParams);
        cerr << "Acquire request with " + acquireParams.dump() + "\n";

        auto maintenance = YogaLayout::instance().config->value("maintenance", ""s);
        if (!maintenance.empty()) {
          return cb.err(maintenance);
        }

        if (requireAuth) {
          string auth = acquireParams.value("auth", "");
          if (!auth.empty()) {
            auto allows = YogaLayout::instance().config->value("allowAuth", json::array());
            for (auto &it : allows) {
              if (string(it) == auth) {
                cerr << "Accepting auth token " + auth + "\n";
                authorized = true;
              }
            }
          }
          if (auth == "freebie") maxDuration = 30.0;
          if (!authorized) {
            logAccess("reject " + acquireParams.dump());
            return cb.err("Unauthorized. Check your ~/.yoga/config.json");
          }
        }

        robotName = acquireParams.value("robotName", "");
        if (!robotName.empty()) {
          if (!isValidTraceName(robotName)) {
            logAccess("badRobotName " + acquireParams.dump());
            return cb.err("Bad robotName");
          }
  #ifdef __linux__
          lockFn = "/var/lock/yoga_"s + robotName + ".lock";
  #else
          lockFn = "/tmp/yoga_"s + robotName + ".lock";
  #endif
          lockFile = open(lockFn.c_str(), O_RDWR|O_CREAT, 0666);
          if (lockFile < 0) {
            logAccess("badLock " + acquireParams.dump());
            return cb.err("Can't open lockfile " + lockFn);
          }
          logAccess("authorized " + acquireParams.dump());
          tryLock(cb);
        }
        else {
          setupAuthorizedHandlers();
          cb.ok([](packet &tx) {
            tx.add(true);
          });
        }
      });
  }

  void logAccess(string const &op)
  {
    auto authLogFn = YogaLayout::instance().homeDir + "/.yoga/auth.log";
    auto authLogFd = open(authLogFn.c_str(), O_WRONLY|O_CREAT|O_APPEND, 0666);
    if (authLogFd < 0) {
      cerr << authLogFn + ": " + strerror(errno);
      return;
    }
    string msg;
    {
      auto t = time(nullptr);
      auto tm = localtime(&t);
      msg += getTimeStamp(tm);
    }
    msg += ": ";
    msg += sessionTok;
    msg += " ";
    msg += op;
    msg += "\n";
    if (write(authLogFd, msg.data(), msg.size()) < 0) {
      cerr << authLogFn + ": " + strerror(errno);
    }
    if (close(authLogFd) < 0) {
      cerr << authLogFn + ": " + strerror(errno);
    }
  }


  void tryLock(PacketApiCb cb)
  {
    if (::flock(lockFile, LOCK_EX | LOCK_NB) < 0) {
      if (errno == EWOULDBLOCK) {
        auto ownerMsg = readFile(lockFile, lockFn);
        cerr << lockFn + " owned by " + ownerMsg + ", sleeping\n";
        logAccess("waitLock");

        cb.stream([this, ownerMsg](packet &tx) {
          lseek(lockFile, 0, SEEK_SET);
          tx.add(false);
          tx.add(YogaLayout::instance().localHostName + ": Lock on "s + robotName + " owned by " + ownerMsg);
        });

        auto t = new UvTimer();
        t->timer_init();
        t->timer_start([this, this1=shared_from_this(), cb, t]() {
          tryLock(cb);
          t->timer_stop();
          delete t;
        }, 500, 500);

        return;
      }
      else {
        string lockMsg = "botd pid " + repr(getpid());
        ftruncate(lockFile, 0);
        lseek(lockFile, 0, SEEK_SET);
        writeFile(lockFile, lockMsg, lockFn);
        logAccess("locked");

        return cb.err("Locking "s + lockFn + ": " + strerror(errno));
      }
    }

    setupAuthorizedHandlers();
    cb.ok([](packet &tx) {
      tx.add(true);
    });
  }

  void setupAuthorizedHandlers()
  {
    parent->addApiMethod("compile", 
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb &&cb) {
        ctx.reg->sources->rx(rx);

        if (!ctx.reg->compileMain()) {
          cerr << ctx.reg->diagLog.str() << "\n";
          cb.err(ctx.reg->diagLog.str());
          return;
        }

        ctx.reg->summarizeProgram(cerr);

        cerr << ctx.reg->diagLog.str() << "\n";

        cb.ok();
      }
    );

    parent->addApiMethod("start",
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb &&cb) {

        double duration{0.0};
        rx.get(duration);

        cerr << "yoga_botd: start duration=" + to_string(duration) + "\n";

        trace = ctx.reg->startTrace();
        if (!trace) return cb.err("Can't create trace");

        if (1) {
          shared_ptr<YogaTimeseq> kbdSeq;
          trace->bindSeq(ctx, kbdSeq, "YogaKeyboard", "ui.kbd");
          kbdSeq->addNew(0.0);
          kbdSeq->isExtIn = true;
        }

        if (1) {
          shared_ptr<YogaTimeseq> puckSeq;
          trace->bindSeq(ctx, puckSeq, "YogaPuck", "ui.puck");
          puckSeq->addNew(0.0);
          puckSeq->isExtIn = true;
        }

        if (duration != 0.0) {
          trace->rti->hardHaltTs = duration;
        }
        if (maxDuration != 0.0) {
          if (trace->rti->hardHaltTs == 0.0) {
            trace->rti->hardHaltTs = maxDuration;
          }
          else {
            trace->rti->hardHaltTs = min(trace->rti->hardHaltTs, maxDuration);
          }
        }

        auto txEngine = make_shared<LivestreamTxEngine>();
        txEngine->uiPipe = parent;
        trace->addEngine("tx", txEngine, true);

        if (!setupYogaEngines(ctx("setupYogaEngines"), trace.get())) {
          cerr << "setupYogaEngines failure:\n";
          cerr << ctx.reg->diagLog.str() << "\n";
          cb.err(ctx.reg->diagLog.str());
          return;
        }

        cb.ok([this](packet &tx) {
          tx.add(trace->rti->traceName);
          auto manifest = trace->buildManifest(true);
          tx.add(manifest);
        });        
        txEngine->schemaSent = true;

        trace->startUpdateThread();

        trace->onAllStopped.push_back([this]() {
          cerr << "yoga_botd: onAllStopped, saving\n";
          logAccess("save "s + trace->rti->traceName);
          trace->save([this](string const &err, size_t totSize) {
            if (!err.empty()) {
              cerr << "saving trace: "s + err + "\n";
            }
            else {
              cerr << "saved trace: "s + trace->rti->traceName + "\n";
            }

            parent->rpc(
              "traceSavedAs",
              [this](packet &tx) {
                tx.add(trace->rti->traceName);
                auto manifest = trace->buildManifest(true);
                tx.add(manifest);

                vector<YogaDbLastSeen> lastSeen;
                YogaDb::addLocalLastSeen(lastSeen, trace.get());
                tx.add(lastSeen);

                string abortReason = "halt";
                tx.add(abortReason);

              },
              [](string const &err, packet &rx) {
                if (!err.empty()) {
                  cerr << "Reporting traceSavedAs: "s + err + "\n";
                }
              }
            );

          });
        });
      }
    );


    parent->addApiMethod(
      "setInteractive",
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb cb) {
        string seqName;
        rx.get(seqName);
        if (!isValidSeqName(seqName)) return cb.err("Bad seqName");

        if (!trace) return cb.err("No trace yet");
        auto seq = trace->getTimeseq(seqName);
        if (!seq) return cb.err("No interactive sequence " + seqName);

        auto last = seq->getLast();

        auto yv = seq->addNew(realtime() - trace->rti->realTimeOffset);
        seq->type->packetRd(rx, yv.buf, seq->mem);

        seq->add(yv.ts - 0.001, last.buf);
        seq->add(yv);

        if (0) cerr << "botd: setInteractive :" + repr(yv) + "\n";

        cb.ok();
      }
    );

    parent->addApiMethod("updateYogaParams",
      [this, thisp=shared_from_this()](packet &rx, PacketApiCb cb) {
        bool anyChanged = false;
        while (rx.get_bool()) {
          string paramName;
          rx.get(paramName);
          U32 paramDimension;
          rx.get(paramDimension);

          if (ctx.reg) {
            auto paramInfo = ctx.reg->paramsByName[paramName];
            if (!paramInfo) {
              cerr << "No param named " + paramName + "\n";
              continue;
            }
            if (paramInfo->paramDimension != paramDimension) {
              return cb.err("Param " + paramName + " expected " + repr(paramInfo->paramDimension) +
                " dims, got " + repr(paramDimension));
            }
            anyChanged = true;
            for (size_t i = 0; i < paramDimension; i++) {
              rx.get(ctx.reg->paramValues[paramInfo->paramIndex + i]);
            }
          }
        }
        if (anyChanged && trace) {
          trace->rti->yogaParamsUpdateTs = realtime() - trace->rti->realTimeOffset;
        }
        cb.ok();
      }
    );

  }

  YogaContext ctx;
  shared_ptr<ParentPipe> parent;
  shared_ptr<Trace> trace;
};


void usage()
{
  cerr << "usage: yoga_botd [-d] [-C dir]\n";
}

int main(int argc, char** argv)
{
  apr_initialize();
  atexit(apr_terminate);
  if (0) kill(getpid(), SIGSTOP);/*  */

  static struct option longopts[] = {
    {nullptr, 0, nullptr, 0}
  };
  bool fromInetd=false;

  int ch;
  while ((ch = getopt_long(argc, argv, "C:d", longopts, nullptr)) != -1) {
    switch (ch) {
      case 'C':
        if (chdir(optarg) < 0) {
          cerr << string(optarg) + ": "s + strerror(errno);
          return 1;
        }
        break;

      case 'd':
        fromInetd = true;
        close(2);
        open("yoga_botd.log", O_WRONLY|O_CREAT|O_APPEND, 0666);
        break;

      default:
        usage();
        return 2;
    }
  }

  argc -= optind;
  argv += optind;

  auto reg = make_shared<YogaCompilation>();
  reg->emitDebugVersions = false;
  reg->emitGradVersions = false;
  
  auto parent = make_shared<ParentPipe>();
  parent->verbose = 2;
  parent->label = "bot-ui";
  parent->tx_version += "/botd 1.0";

  auto server = make_shared<YogaServer>(reg->mkCtx("server"), parent);
  server->requireAuth = fromInetd;
  server->setupHandlers();

  parent->start();

  server->logConnection();

  cerr << "botd running\n";

  uvRunMainThread();

  cerr << "botd exiting\n";

  return 0;
}



