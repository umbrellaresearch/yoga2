#include "./client.h"
#include "../db/yoga_layout.h"
#include "../db/yoga_db.h"
#include "../jit/compilation.h"
#include "../timeseq/trace.h"
#include "nlohmann-json/json.hpp"
#include "common/packetbuf_types.h"
#include "../comm/tcp_pipe.h"

YogaClient::YogaClient(YogaContext const &_ctx)
  : ctx(_ctx)
{
  blobFetcherPool = YogaLayout::instance().mkRpcPool("fetchd");
}

void YogaClient::rpcStart(function<void(string const &err)> cb)
{
  toServer->rpc(
    "start",
    [this](packet &tx) {
      tx.add(duration);
    }, 
    [this, thisp=shared_from_this(), cb](string const &err, packet &rx) {
      if (err.size()) {
        cb(err);
        return;
      }
      string traceName;
      rx.get(traceName);
      auto manifest = make_shared<json>();
      rx.get(*manifest);
      cerr << "Botd engines started, traceName="s + traceName + "\n";
      warmupVideoSources(*manifest);

      rxEngine->setupDsts(*manifest);
      model->setTraceManifest(*manifest);
      cb("");
    }
  );
}


void YogaClient::rpcControlledStop(function<void(string const &err)> cb)
{
  if (!toServer) return cb("");
  if (stopRequested) return cb("");
  stopRequested = true;
  toServer->rpc(
    "controlledStop",
    [](packet &tx) {
    }, 
    [thisp=shared_from_this(), cb](string const &err, packet &rx) {
      if (!err.empty()) {
        cb(err);
        return;
      }
      cb("");
    }
  );
}


void YogaClient::rpcAcquire(function<void(string const &err)> cb)
{
  if (!toServer) return cb("no connection");

  auto &layout = YogaLayout::instance();
  json params({});
  params["clientUser"] = layout.localUser;
  params["clientHost"] = layout.localHostName;
  params["auth"] = (*layout.config).value("auth", "");
  params["robotName"] = ctx.reg->runtimeParams.robotName;

  toServer->rpc(
    "acquire",
    [&params](packet &tx) {
      tx.add(params);
    }, 
    [this, this1=shared_from_this(), cb](string const &err, packet &rx) {
      if (!err.empty()) {
        if (onAcquireStatus) onAcquireStatus("");
        return cb(err);
      }
      bool ok=false;
      rx.get(ok);
      if (ok) {
        if (onAcquireStatus) onAcquireStatus("");
        return cb("");
      }

      string status;
      rx.get(status);
      if (onAcquireStatus) {
        onAcquireStatus(status);
      }
    }
  );
}

void YogaClient::rpcCompile(function<void(string const &err)> cb)
{
  if (!toServer) return cb("no connection");
  toServer->rpc(
    "compile",
    [this](packet &tx) {
      ctx.reg->sources->tx(tx);
    }, 
    [cb](string const &err, packet &rx) {
      cb(err);
    }
  );
}

void YogaClient::rpcSetInteractive(string const &seqName, YogaValue yv)
{
  if (!toServer) return;
  toServer->rpc(
    "setInteractive",
    [&seqName, &yv](packet &tx) {
      tx.add(seqName);
      yv.type->packetWr(tx, yv.buf);
    },
    [](string const &err, packet &rx) {
      if (!err.empty()) {
        cerr << "rpcSetInteractive: " + err + "\n";
      }
    }
  );
}

void YogaClient::syncYogaParams()
{
  if (!toServer) return;
  if (ctx.reg->paramValueEpoch > sentParamValueEpoch) {
    sentParamValueEpoch = ctx.reg->paramValueEpoch;
    rpcUpdateYogaParams([](string const &err) {
      if (!err.empty()) {
        cerr << "updateYogaParams: "s + err + "\n";
      }
    });
  }
}

void YogaClient::rpcUpdateYogaParams(function<void(string const &err)> cb)
{
  if (!toServer) return;
  toServer->rpc(
    "updateYogaParams",
    [this](packet &tx) {
      for (auto param : ctx.reg->paramsByIndex) {
        if (param && param->isValueChanged(ctx)) {

          tx.add(true);
          tx.add(param->paramName);
          tx.add((U32)param->paramDimension);

          for (auto i = 0; i < param->paramDimension; i++) {
            tx.add(ctx.reg->paramValues[param->paramIndex + i]);
          }
        }
      }
      tx.add(false);
    },
    [cb](string const &err, packet &rx) {
      cb(err);
    }
  );
}

void YogaClient::runRemote()
{
  assert(model);
  assert(toServer->isActive());
  rxEngine = make_shared<LivestreamRxEngine>();
  rxEngine->robotPipe = toServer;

  model->trace->addEngine("rx", rxEngine, false);

  rxEngine->onGrows.push_back([this, thisp=shared_from_this()](R lastRxTs) {
    if (0) rxEngine->console("Grow to "s + repr(lastRxTs));
    model->traceEndTs = max(model->traceEndTs, lastRxTs);
    model->visTime = model->traceEndTs;
  });

  toServer->addApiMethod("traceSavedAs",
    [this, thisp=shared_from_this()](packet &rx, PacketApiCb cb) {
      string traceName;
      rx.get(traceName);
      json manifest;
      rx.get(manifest);
      vector<YogaDbLastSeen> lastSeen;
      rx.get(lastSeen);
      string abortReason;
      rx.get(abortReason);

      if (traceName.empty()) {
        cerr << "traceSavedAs: Empty trace name\n";
        cb.ok();
        stop();
      }

      YogaDb::instance().addTraceInfo(YogaDbTraceInfoRow(traceName));
      YogaDb::instance().addLastSeen(lastSeen);

      if (onTraceSavedAs) onTraceSavedAs(traceName);

      cb.ok();
      stop();
    }
  );

  toServer->verbose = 0;
  rpcAcquire([this, thisp=shared_from_this()](string const &err) {
    if (!err.empty()) {
      model->setError(err);
      stop();
      return;
    }
      
    rpcCompile([this, thisp=shared_from_this()](string const &err) {
      if (!err.empty()) {
        model->setError(err);
        stop();
        return;
      }

      rpcUpdateYogaParams([this, thisp=shared_from_this()](string const &err) {
        if (!err.empty()) {
          model->setError(err);
          stop();
          return;
        }

        rpcStart([this, thisp=shared_from_this()](string const &err) {
          if (!err.empty()) {
            model->setError(err);
            stop();
            return;
          }
          remoteRunning = true;
        });
      });
    });
  });

  rxEngine->start();

}

void YogaClient::stop()
{
  cerr << "Client stop\n";
  if (rxEngine) {
    rxEngine->stop();
    rxEngine = nullptr;
  }

  if (toServer) {
    toServer->stop();
  }
  if (toServer) {
    toServer->endDrain();
  }
  toServer = nullptr;

  if (blobFetcherPool) blobFetcherPool->stopAll();

  model = nullptr;
}


bool YogaClient::startBotd()
{
  liveHost = ctx.reg->runtimeParams.liveHost;
  if (liveHost.empty()) {
    return ctx.logError("A liveHost parameter of the runtime is required to run a live robot");
  }

  toServer = YogaLayout::instance().mkRpcConn("botd", liveHost, false);
  if (!toServer) {
    return ctx.logError("No connection to " + liveHost + ":botd");
  }
  toServer->rti = model->trace->rti;
  toServer->verbose = 0;

  toServer->onReady.push_back(
    [this, thisp=shared_from_this()]() {
      runRemote();
    });

  toServer->start();

  toServer->onRxEof.push_back(
    [this, thisp=shared_from_this()]() {
      stop();
    });


  return true;
}

void YogaClient::warmupVideoSources(json &manifest)
{
  auto videoHosts = manifest["videoHosts"];
  if (videoHosts.is_array()) {
    for (auto &vh : videoHosts) {
      if (vh.is_string() && vh != "") {
        if (1) cerr << "Warmup video source "s + string(vh) + "\n";
        blobFetcherPool->get(vh);
      }
    }
  }
}


void YogaClient::fetchBlob(string const &host, BlobRef const &blobRef, std::function<void(string const &err, shared_ptr<Blob> blob)> cb)
{
  auto fetcher = blobFetcherPool->get(host);
  if (!fetcher) {
    return cb("can't fetch", nullptr);
  }

  fetcher->rpc("fetchBlob",
    [&blobRef](packet &tx) {
      tx.add(blobRef);
    },
    [cb](string const &err, packet &rx) {
      if (!err.empty()) return cb(err, nullptr);
      auto blob = make_shared<Blob>();
      rx.get(*blob);
      cb("", blob);
    }
  );

}

