#pragma once
#include "./trace.h"

#include "./timeseq.h"

struct TimeseqPipe {
  YogaTimeseq *src;
  multimap<double, U8 *>::iterator srcIt;
  YogaTimeseq *dst;
  vector<pair<TimeseqChange, std::function<double(double t)>>> changes;
  size_t remaining;
  bool didWarn{false};

  bool valid() const
  {
    return remaining > 0 && srcIt != src->values_.end();
  }

  double frontTime() const
  {
    assert(valid());
    return srcIt->first;
  }

  void advance()
  {
    assert(valid());

    YogaValue val(src->reg, src->type, srcIt->second, srcIt->first);
    srcIt++;
    remaining--;

    if (!changes.empty()) {
      val = val.dup(dst->mem);
      for (auto &changeit : changes) {

        double relw = (val.ts - changeit.first.beginTs) / (changeit.first.endTs - changeit.first.beginTs);
        double weight = changeit.second(relw);
        if (weight != 0.0) {
#if 1
          if (1) {
#else
          if (!dst->applyNamedChange(val, changeit.first.changeKey.c_str(), weight, changeit.first.value)) {
#endif
            if (!didWarn) {
              cerr << "Failed to apply change "s + dst->seqName + "." + changeit.first.changeKey + "\n";
              didWarn = true;
            }
          }
        }
      }
    }
    dst->add(val);
  }
};
