#pragma once
#include "common/std_headers.h"

#include <atomic>
#include <typeinfo>

#include "./runtime_info.h"
#include "./trace_blobs.h"
#include "../lib/core_types.h"

struct GenericEngine;
struct YogaTimeseq;
struct GradientSet;
struct UvTimer;
struct TimeseqChange;
struct Trace;
struct EngineUpdateContext;

using GradFwdTrace = deque<std::function<void(GradientSet &grads)>>;


/*
  A Trace holds many named Timeseq<T>s, and manages threads to update them.
*/
struct Trace : enable_shared_from_this<Trace> {
  Trace(shared_ptr<YogaCompilation> _reg);
  ~Trace();
  Trace(Trace const &) = delete;
  Trace(Trace &&) = delete;
  Trace &operator=(Trace const &) = delete;
  Trace &operator=(Trace &&) = delete;

  int createNewDir(string const &_traceName);
  int createNewDirForScript(string const &scriptName);
  void close();
  void rerunFrom(Trace const &other, map<string, vector<TimeseqChange>> const &changes);

  void addTimeseq(string const &name, shared_ptr<YogaTimeseq> const &timeseq, bool dontSave = false);

  shared_ptr<YogaTimeseq> getTimeseq(string const &name);
  shared_ptr<YogaTimeseq> getOrAddTimeseq(string const &name, YogaType *type);
  
  bool bindSeq(
    YogaContext const &ctx,
    shared_ptr<YogaTimeseq> &ptr,
    string typeName,
    string seqName,
    bool dontSave = false);
  
  void clearTimeseqs();
  void setupConsole();
  void engineChangedStatus();

  void addEngine(string const &name, shared_ptr<GenericEngine> const &engine, bool autostart);
  shared_ptr<GenericEngine> getEngine(string const &name);
  void clearEngines();

  bool isAnyExtIO() const;

  json buildManifest(bool includeEmpty);
  void save(std::function<void(string const &error, size_t totSize)> const &cb);
  void load(string const &_traceName, std::function<void(string const &error, json const &manifest)> const &cb);

  string chunkFn(string const &res, string const &seqName, double chunkTs, char const *extension);

  shared_ptr<RuntimeInfo> rti;

  map<U64, pair<string, string>> blobLocationsByChunkId;
  shared_ptr<json> traceNotes;
  std::atomic<int> saveActive{0};

  /*
    Halting problem.
    When any timestamp goes beyond hardHaltTs it sets hardHalt and all engine activity stops
    When any timestmap goes beyond softHaltTs, it sets softHalt which can be noticed externally
  */

  struct TimeseqLink {
    shared_ptr<YogaTimeseq> timeseq;
    bool dontSave{false};
  };

  vector<TimeseqLink> timeseqs;
  shared_ptr<YogaTimeseq> consoleSeq; // Written by calling addTimeseq("console", ...);

  struct EngineLink {
    shared_ptr<GenericEngine> engine;
    bool startedByTrace{false};
    bool autostart{false};
  };
  vector<EngineLink> engines;

  deque<std::function<void(void)>> onHalts;
  deque<std::function<void(void)>> cleanups;
  deque<std::function<void(void)>> onAllStopped;
  deque<std::function<void(void)>> onAllDraining;

  void runBackprop(GradientSet &grads, double minTs, GradFwdTrace *gradFwdTrace);

  // --- Console
  void console(double ts, string const &line);

  // --- Life cycle
  void stopAllThreads();
  void warmupEngines(R &delay);
  void startEngines();
  void stopEngines();

  // --- Export
  void exportCsv(FILE *f, vector<string> const &seqs, vector<R> const &intervals, bool doHeader);

  // --- Syslog
  void startSyslogThread();
  void stopSyslogThread();
  shared_ptr<UvTimer> syslogTimer;
  int syslogFd{-1};
  off_t syslogLen{0};

  // --- Update
  void startUpdateThread();
  void startUpdateThread2();
  void updateIter(GradFwdTrace *gradFwdTrace=nullptr);
  void updateIter(EngineUpdateContext &ctx);
  void rerunUpdate();
  void updateTo(shared_ptr<YogaTimeseq> const &seq, double targetTs);
  void updateTo(map<string, double> targets);
  size_t workOutstanding() const;
  R getEndTs();

};
