#pragma once
#include "common/std_headers.h"

#include "./gradient_set.h"
#include "./runtime_info.h"
#include "../jit/value.h"
#include "./trace_blobs.h"

struct YogaTimeseq {
  YogaTimeseq(YogaCompilation *_reg, YogaType *_type);
  ~YogaTimeseq();
  YogaTimeseq(YogaTimeseq const &) = delete;
  YogaTimeseq(YogaTimeseq &&) = delete;
  YogaTimeseq &operator=(YogaTimeseq const &) = delete;
  YogaTimeseq &operator=(YogaTimeseq &&) = delete;

  double beginTs();
  double endTs();
  size_t sampleCount();
  bool empty();

  string desc();

  /*
    Create a new element with time=ts and return it.
    You probably want to fill in the value, and then call add.
    Or call addNew to do it all together, if you're sure nothing will go wrong while
    filling it in.
  */
  YogaValue getNew(double ts);
  /*
    Create a new element with time ts, add it and return it
  */  
  YogaValue addNew(double ts);

  /*
    Get entries, by various criterea.
    getEqualBefore is what you normally want for the latest value available
    at a certain time.
  */
  YogaValue getFirst();
  YogaValue getLast();
  YogaValue getLastMatching(std::function<bool(U8 *)> filter, int maxScan=20);
  vector<YogaValue> getNEqualAfter(double ts, size_t n);
  vector<YogaValue> getNBefore(double ts, size_t n);
  YogaValue getClosest(double ts);
  YogaValue getInputFor(double prevOutputTs, double minDelta);
  YogaValue getEqualAfter(double ts);
  YogaValue getEqualBefore(double ts);
  YogaValue getAfter(double ts);
  YogaValue getEqualAfterSoft(double ts);
  YogaValue getAfterSoft(double ts);
  /*
    Return an entry both before and after ts. Either could be invalid.
    Suitable for interpolating an exact value at ts.
  */
  pair<YogaValue, YogaValue> getBracket(double ts);
  /*
    Interpolate an precise value at ts, using the linearOp for the type
  */
  YogaValue getInterpolated(double ts);

  /*
   Add an entry with the given time and value.
   The value should probably have been allocated by getNew()
  */
  void add(double ts, U8 * value);
  void add(YogaValue const &value);

  void addAll(YogaTimeseq const &other);

  /*
    Erase items with matching times
  */
  void eraseBefore(double ts);         // t < ts
  void eraseEqualBefore(double ts);    // t <= ts
  void eraseAfter(double ts);          // t > ts
  void eraseEqualAfter(double ts);     // t >= ts

  /*
    Get type properties
  */
  string contentTypeName();           // ie, type->clsName
  string contentTypeVersionString();  // type and version


  /*
    Save (in compressed binary format) in (fn).bin.gz.
    Returns binary size
  */
  size_t savePacket(string const &fn) const;
  /*
    Load (in compressed binary format) from (fn).bin.gz.
    Returns binary size
  */
  size_t loadPacket(string const &fn);
  /*
    Write contents to a compressed file
  */
  void toPacketStream(struct gzFile_s *gzfp) const;
  /*
    Read contents from a compressed file
  */
  void fromPacketStream(struct gzFile_s *gzfp, YogaType *oldType);

  /*
    Create a new YogaTimeseq with no elements but same type
  */
  shared_ptr<YogaTimeseq> copyEmpty() const;
  /*
    Create a new YogaTimeseq with all the same elements (and same type)
    Bad news is that it references the memory of the source, so only
    use this for temporary purposes while the source is still alive.
  */
  shared_ptr<YogaTimeseq> copyFull() const;

  /*
    Create a new YogaTimeseq with the gradients of each element of *this
  */
  shared_ptr<YogaTimeseq> getGradOf(GradientSet &grads) const;

  vector<YogaValue> getValuesInRange(
      R itBeginTs,
      R itEndTs,
      double maxPts = 0.0,
      size_t stopAfter = 0);

  shared_ptr<RuntimeInfo> rti;
  string seqName;
  YogaCompilation *reg;
  YogaType *type;


  // If true, then when the first item is added we add an extra version with time=0
  bool needBackdateInitial{false};
  // If true, this is receiving updates over the network
  bool isExtIn{false};
  // If true, this is sending updates over the network
  bool isExtOut{false};

  map<double, U8 *> values_;
  apr_pool_t *mem{nullptr};
};

inline bool hasNaN(YogaTimeseq const &it)
{
  return false;
}
