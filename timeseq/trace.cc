#include "./trace.h"

#include "./timeseq.h"
#include "../lib/core_types.h"
#include "common/packetbuf_types.h"
#include "common/uv_wrappers.h"
#include "common/asyncjoy.h"
#include "../comm/engine.h"
#include "../jit/value.h"
#include "./trace_blobs.h"
#include "nlohmann-json/json.hpp"
#include <zlib.h>

Trace::Trace(shared_ptr<YogaCompilation> _reg)
    : rti(make_shared<RuntimeInfo>(_reg))
{
  traceNotes = make_shared<json>(json::object());
  if (0) cerr << "Trace: new " + repr_ptr(this) + "\n";
}

Trace::~Trace()
{
  if (0) cerr << "Trace: destruct " + repr_ptr(this) + "\n";
}

int Trace::createNewDir(string const &_traceName)
{
  if (!rti->traceName.empty() || !rti->traceDir.empty()) throw runtime_error("Trace already instantiated"s);
  rti->traceName = _traceName;
  rti->traceDir = YogaLayout::instance().tracesDir + "/"s + _traceName;

  if (mkdir(rti->traceDir.c_str(), 0777) < 0) {
    cerr << rti->traceDir + ": " + strerror(errno) + "\n";
    rti->traceName = "";
    rti->traceDir = "";
    return -1;
  }

  return 0;
}

int Trace::createNewDirForScript(string const &scriptName)
{
  time_t now{0};
  time(&now);
  struct tm t;
  localtime_r(&now, &t);
  auto dateCode = getTimeTok(&t);
  return createNewDir(scriptName + "_"s + dateCode);
}


void Trace::close()
{
  if (rti->traceDir.empty()) return;

  rti->traceDir = "";
}

R Trace::getEndTs()
{
  R endTs = 0.0;
  for (auto const &it : timeseqs) {
    R endTs1 = it.timeseq->endTs();
    if (endTs1 > endTs) endTs = endTs1;
  }
  return endTs;
}


json Trace::buildManifest(bool includeEmpty)
{
  json ret;
  R manifestEndTs{0.0}, manifestBeginTs{0.0};

  for (auto const &it : timeseqs) {
    if (!it.timeseq) {
      cerr << "buildManifest: timeseq disappeared\n";
      continue;
    }
    if (it.dontSave || (!includeEmpty && it.timeseq->empty())) {
      continue;
    }
    double beginTs1 = it.timeseq->beginTs();
    double endTs1 = it.timeseq->endTs();
    if (endTs1 > 1e6) {
      cerr << "timeseq "s + it.timeseq->seqName + ": endTs=" + repr_0_3f(endTs1) + "\n";
    }
    if (endTs1 != 0.0) {
      if (manifestEndTs == 0.0) {
        manifestBeginTs = beginTs1;
        manifestEndTs = endTs1;
      }
      else {
        if (beginTs1 < manifestBeginTs) manifestBeginTs = beginTs1;
        if (endTs1 > manifestEndTs) manifestEndTs = endTs1;
      }
    }
    json info = {
      {"name", it.timeseq->seqName},
      {"type", it.timeseq->contentTypeName()},
      {"isEmpty", it.dontSave || it.timeseq->sampleCount() == 0},
      {"isExtIn", it.timeseq->isExtIn},
      {"isExtOut", it.timeseq->isExtOut}
    };

    ret["timeseqInfos"].push_back(info);
  }

  {
    for (auto const &it : engines) {
      if (it.engine) {
        it.engine->addToManifest(ret);
      }
    }
  }

  for (auto &[blobId, loc] : blobLocationsByChunkId) {
    auto &[host, fn] = loc;
    ret["blobLocations"].push_back({
      {"host", host},
      {"fn", fn},
    });
  }

#if 0
  for (auto const &it : engines) {
    if (it.engine && it.engine->engineName.size() && it.engine->wiring.engineType.size()) {
      ret.yogaWirings.push_back(it.engine->wiring);
    }
  }
  // FIXME ret.yogaParams = *rti->yogaParams;
#endif
  if (traceNotes) {
    ret["traceNotes"] = *traceNotes;
  }
  ret["endTs"] = manifestEndTs;
  ret["beginTs"] = manifestBeginTs;
  return ret;
}


string Trace::chunkFn(string const &res, string const &seqName, double chunkTs, char const *extension)
{
  return rti->traceDir + "/chunk"s + res + "_"s + seqName + stringprintf("_%.0f%s", chunkTs, extension);
}

void Trace::save(std::function<void(string const &error, size_t totSize)> const &cb)
{
  if (rti->traceDir.empty()) {
    cb("traceDirEmpty", 0);
    return;
  }
  cerr << "Saving " + rti->traceDir + "\n";

  auto totSizeAccum = make_shared<size_t>(0);

  auto aj = make_shared<AsyncJoy>([thisp = shared_from_this(), cb, totSizeAccum](string const &err) {
    cb(err, *totSizeAccum);
  });

  auto manifest = buildManifest(true);
  writeFile(rti->traceDir + "/manifest.json", manifest.dump(2));

  for (auto &it : timeseqs) {
    auto seq = it.timeseq;
    double seqBeginTs = seq->beginTs();
    double seqEndTs = seq->endTs();
    auto seqName = seq->seqName;
    if (seq->sampleCount() == 0) {
      cerr << "  "s + rightPadTo(seqName, 20) + 
        ": (empty)\n";
      continue;
    }
    if (seqEndTs >= 1000000.0) {
      cerr << "  "s + rightPadTo(seqName, 20) + 
        ": (silly ts range " + repr_0_6f(seqBeginTs) + " - " + repr_0_6f(seqEndTs) + ")\n";
      continue;
    }
    if (it.dontSave) {
      cerr << "  "s + rightPadTo(seqName, 20) + 
        ": (dontSave)\n";
      continue;
    }

    cerr << "  "s + seq->desc() + "\n";

    string seqFn = rti->traceDir + "/seq_"s + seqName;
    aj->start();
    uvWork(
        [seq, seqFn, totSizeAccum](string &error, shared_ptr<void> &result) {
          size_t seqSize = seq->savePacket(seqFn);
          *totSizeAccum += seqSize;
        },
        [aj](string const &error, shared_ptr<void> const &result) {
          if (!error.empty()) {
            aj->error(error);
          }
          else {
            aj->end();
          }
        });
  }
  aj->end();
}

void Trace::load(string const &_traceName, std::function<void(string const &error, json const &manifest)> const &cb)
{
  if (!rti->traceName.empty() || !rti->traceDir.empty()) throw runtime_error("Trace already instantiated"s);
  rti->traceName = _traceName;
  rti->traceDir = YogaLayout::instance().tracesDir + "/"s + _traceName;

  if (rti->traceDir.empty()) {
    cb("traceDirEmpty", nullptr);
    return;
  }
  if (1) cerr << "Loading "s + rti->traceDir + "...\n";

  auto traceManifest = make_shared<json>();
  auto manifestFn = rti->traceDir + "/manifest.json";
  auto manifestStr = readFile(manifestFn); // throws if nonexistent
  if (manifestStr.empty()) {
    cb("traceDirEmpty", nullptr);
    return;
  }
  *traceManifest = json::parse(manifestStr);

  auto loadStartTime = clock();

  auto aj = make_shared<AsyncJoy>([this, thisp = shared_from_this(), cb, traceManifest, loadStartTime](string const &err) {
    auto loadEndTime = clock();
    cerr << "Loaded "s + rti->traceDir + " in " + repr_clock(loadEndTime - loadStartTime) + "\n";
    cb(err, *traceManifest);
  });

  auto ctx = rti->reg->mkCtx("load");
  
  for (auto &tsInfo : (*traceManifest)["timeseqInfos"]) {
    string name = tsInfo["name"];
    string typeStr = tsInfo["type"];
    string seqFn = rti->traceDir + "/seq_"s + name;

    YogaType *type = ctx.getType(typeStr);
    if (!type) {
      cerr << "  Not loading "s + seqFn + ": no type named " + shellEscape(typeStr) + "\n";
      continue;
    }

    if (!type->packetRd) {
      cerr << "  Not loading "s + seqFn + ": type named " + shellEscape(typeStr) + "  has no packetRd function\n";
      continue;
    }

    /*
      Threading is ugly here. First, on the main thread we open all the files and read in the types.
      Because reading types requires deep mutation inside the YogaCompilation, we do this on the main thread.
      Then we fork threads to handle reading each thread's data.
    */


    auto seq = make_shared<YogaTimeseq>(ctx.reg, type);

    seq->isExtIn = tsInfo.value("isExtIn", false);
    seq->isExtOut = tsInfo.value("isExtOut", false);
    seq->rti = rti;
    bool isEmpty = tsInfo.value("isEmpty", false);
    if (isEmpty) {
      addTimeseq(name, seq, true); // empty
      continue;
    }

    auto fullFn = seqFn + ".bin.gz";
    if (access(fullFn.c_str(), R_OK) < 0) {
      cerr << "  No "s + seqFn + "\n";
      addTimeseq(name, seq, true); // empty
      continue;
    }
    gzFile fp = gzopen(fullFn.c_str(), "r");
    if (!fp) {
      aj->setError(fullFn + ": load failed"s);
      continue;
    }
    if (0) cerr << "  Loading "s + seqFn + "\n";

    packet headerPacket = packet::from_gzfile_boxed(fp);
    YogaType *oldType{nullptr};
    packetio::packet_rd_value(headerPacket, oldType, ctx);

    aj->start();
    uvWork(
        [seqFn, seq, fp, type](string &error, shared_ptr<void> &result) {
          try {
            seq->fromPacketStream(fp, type);
            auto size = gztell(fp);
            result = static_pointer_cast<void>(seq);
            cerr << "  Loaded "s + seqFn + " size=" + repr_filesize(size) + "\n";
          }
          catch (overflow_error &err) {
            cerr << "  Failed to load "s + seqFn + ": " + err.what() + "\n";
          }
          catch (invalid_argument &err) {
            cerr << "  Failed to load "s + seqFn + ": " + err.what() + "\n";
          }
          catch (runtime_error &err) {
            cerr << "  Failed to load "s + seqFn + ": " + err.what() + "\n";
            error = err.what();            
          }
          gzclose(fp);
        },
        [aj, thisp = shared_from_this(), seq, name](string const &error, shared_ptr<void> const &result) {
          if (!error.empty()) {
            aj->error(error);
          }
          else if (result) {
            thisp->addTimeseq(name, static_pointer_cast<YogaTimeseq>(result), true);
            aj->end();
          }
          else {
            aj->end();
          }
        });

  }

  aj->end();

}



// ----------------------------------------------------------------------

void RuntimeInfo::requestUpdate()
{
  if (updateAsync) {
    updateAsyncPendingCnt++;
    uv_async_send(updateAsync);
  }
}

size_t Trace::workOutstanding() const
{
  size_t ret = 0;
  for (auto &it : engines) {
    if (it.engine && it.engine->isActive()) ret += it.engine->workOutstanding();
  }
  ret += rti->updateAsyncPendingCnt;
  return ret;
}

void Trace::stopAllThreads()
{
  stopEngines();
  stopSyslogThread();
  if (rti->updateAsync) {
    uv_close(reinterpret_cast<uv_handle_t *>(rti->updateAsync), [](uv_handle_t *h) { delete h; });
    rti->updateAsync = nullptr;
  }
  if (rti->updateTimer) {
    uv_close(reinterpret_cast<uv_handle_t *>(rti->updateTimer), [](uv_handle_t *h) { delete h; });
    rti->updateTimer = nullptr;
  }
}

void Trace::setupConsole()
{
  auto ctx = rti->reg->mkCtx("setupConsole");
  consoleSeq = getOrAddTimeseq("yoga.console", ctx.getTypeOrThrow("YogaConsoleMsg"));
  consoleSeq->isExtOut = true;
}

void Trace::warmupEngines(R &delay)
{
  for (auto &it : engines) {
    if (it.engine && it.autostart && it.engine->isInitial()) {
      it.engine->warmup(delay);
    }
  }
}

/*
  Start all engines that aren't already started.
  Stop all engines that we started here.
  (So you can make an engine that outlives being attached to a Trace by starting it first)
  Engines require start not to be called if already running, and stop not to be called unless it is.
*/
void Trace::startEngines()
{
  if (0) cerr << rti->traceName + ": Start engines\n";
  rti->realTimeOffset = realtime() - 0.1;

  if (!consoleSeq) setupConsole();

  for (auto &it : engines) {
    if (it.engine && it.autostart && (it.engine->isInitial() || it.engine->isWarming())) {
      if (0) cerr << rti->traceName + ": Start " + it.engine->engineName + "\n";
      it.engine->start();
      it.startedByTrace = true;
    }
    else if (it.engine) {
      if (0) cerr << rti->traceName + ": Don't start " +
        it.engine->engineName + " (autostart=" + repr(it.autostart) +
        " engineStatus=" + repr(it.engine->engineStatus) + ")\n";
    }
  }
  EngineUpdateContext ctx(realtime());
  ctx.rtFence = 0.000;
  updateIter(ctx);
}

void Trace::stopEngines()
{
  if (0) cerr << rti->traceName + ": Stop engines\n";
  for (auto &it : engines) {
    if (it.engine && it.engine->isRunning() && it.startedByTrace) {
      try {
        if (0) cerr << rti->traceName + ": Stop " + it.engine->engineName + "\n";
        it.engine->stop();
      }
      catch (exception const &ex) {
        cerr << rti->traceName + ": stopping " + it.engine->engineName + ": " + ex.what() + "\n";
        // continue, so we can save this Trace even with a broken Engine, but the engine should be fixed.
      }
    }
  }
}

void Trace::engineChangedStatus()
{
  int someInitial = 0, someWarming = 0, someRunning = 0, someStopped = 0, someDraining = 0;
  for (auto &it : engines) {
    if (it.engine && it.startedByTrace) {
      switch (it.engine->engineStatus) {
        case GenericEngine::Initial:
          someInitial++;
          break;
        case GenericEngine::Warming:
          someWarming++;
          break;
        case GenericEngine::Running:
          someRunning++;
          break;
        case GenericEngine::Draining:
          someDraining++;
          break;
        case GenericEngine::Stopped:
          someStopped++;
          break;
      }
    }
  }
  if (0) cerr << "engineStatus initial="s + repr(someInitial) +
      " warming=" + repr(someWarming) +
      " running=" + repr(someRunning) +
      " draining=" + repr(someDraining) +
      " stopped=" + repr(someStopped) + "\n";

  if (!onAllStopped.empty() && someStopped && !someInitial && !someWarming && !someRunning && !someDraining) {
    callPopAll(onAllStopped);
  }
}

/*
  Keep calling update as long as something changes
*/

void Trace::updateIter(GradFwdTrace *gradFwdTrace)
{
  EngineUpdateContext ctx(realtime());
  ctx.gradFwdTrace = gradFwdTrace;

  updateIter(ctx);
}

void Trace::updateIter(EngineUpdateContext &ctx)
{
  if (saveActive.load()) {
    cerr << "Trace: updateIter: ignoring due to saveActive=" + repr(saveActive.load()) + "\n";
    return;
  }

  while (!rti->hardHalt) {
    ctx.updateCnt = 0;
    for (auto &it : engines) {
      if (it.engine && it.engine->isActive()) it.engine->update(ctx);
    }
    if (ctx.updateCnt == 0) break;
  }
  if (rti->hardHalt) {
    stopAllThreads();
    callPopAll(onHalts);
    callPopAll(cleanups);
  }
}

/*
  Start a uv_async thread to call Engine::update functions whenever triggered by .requestUpdate()
*/
void Trace::startUpdateThread()
{
  R delay = 0.0;
  warmupEngines(delay);
  if (delay == 0.0) {
    startUpdateThread2();
    return;
  }

  auto launchTimer = new uv_timer_t;
  uv_timer_init(uv_default_loop(), launchTimer);
  launchTimer->data = this;
  uv_timer_start(launchTimer, [](uv_timer_t *timer) {
    auto this1 = static_cast<Trace *>(timer->data);
    this1->startUpdateThread2();
  }, int(delay*1000.0), 0);
}

void Trace::startUpdateThread2()
{
  startEngines();

  rti->updateAsync = new uv_async_t;
  rti->updateAsync->data = this;
  uv_async_init(uv_default_loop(), rti->updateAsync, [](uv_async_t *async) {
    auto it = static_cast<Trace *>(async->data);
    it->rti->updateAsyncPendingCnt = 0;
    it->updateIter();
  });

  rti->updateTimer = new uv_timer_t;
  rti->updateTimer->data = this;
  uv_timer_init(uv_default_loop(), rti->updateTimer);
  uv_timer_start(rti->updateTimer, [](uv_timer_t *timer) {
    auto it = static_cast<Trace *>(timer->data);
    it->updateIter();
  }, 10, 10);

  rti->requestUpdate();
}

/*
  Run all engines to hardHalt, like when re-running a stored trace
  where just the sensor values have been loaded.
*/
void Trace::rerunUpdate()
{
  startEngines();
  rti->requestUpdate();
  updateIter();
  stopEngines();
}

/*
  Start a uv_timer periodic callback to capture new messages written to /var/log/syslog
  and log them to the console
*/
void Trace::startSyslogThread()
{
  if (syslogTimer) return;

#if defined(__APPLE__)
  const char *syslogFn = "/var/log/system.log";
#else
  const char *syslogFn = "/var/log/syslog";
#endif
  syslogFd = open(syslogFn, O_RDONLY, 0);
  if (syslogFd < 0) {
    cerr << string(syslogFn) + ": " + strerror(errno) + "(continuing without)\n";
    return;
  }
  syslogLen = lseek(syslogFd, 0, SEEK_END);

  syslogTimer = make_shared<UvTimer>();
  syslogTimer->timer_init();
  syslogTimer->timer_start(
      [this]() {
        if (saveActive.load()) return;
        off_t newSyslogLen = lseek(syslogFd, 0, SEEK_END);
        if (newSyslogLen > syslogLen) {
          if (0) cerr << "syslog grew from " + repr(syslogLen) + " to " + repr(newSyslogLen) + "\n";
          double now = realtime();
          ssize_t bufSize = min((ssize_t)(newSyslogLen - syslogLen), (ssize_t)50000);

          lseek(syslogFd, newSyslogLen - (off_t)bufSize, SEEK_SET);
          auto buf = new char[bufSize];
          ssize_t nr = read(syslogFd, buf, bufSize);
          if (nr < 0) {
            cerr << "syslog: "s + strerror(errno) + "\n";
            syslogTimer->timer_stop();
          }
          else if (nr != bufSize) {
            cerr << "syslog: Tried to read " + repr(bufSize) + ", read " + repr(nr) + "\n";
            syslogTimer->timer_stop();
          }
          else {
            int bol = 0;
            while (bol < bufSize) {
              int eol = bol;
              while (eol < bufSize && buf[eol] != '\n') eol++;
              // syslog lines usually start with 16 bytes of timestamp
              if (eol - 16 > bol) {
                string line(&buf[bol + 16], &buf[eol]);
                console(now - rti->realTimeOffset, "syslog: "s + line);
              }
              else if (eol - 1 > bol) {
                string line(&buf[bol], &buf[eol]);
                console(now - rti->realTimeOffset, "syslog: "s + line);
              }
              bol = eol + 1;
            }
          }
          delete[] buf;
        }
        syslogLen = newSyslogLen;
      },
      100,
      100);
}

void Trace::stopSyslogThread()
{
  if (syslogTimer) {
    syslogTimer->timer_stop();
  }
  if (syslogFd != -1) {
    ::close(syslogFd);
    syslogFd = -1;
  }
}

/*
  Write something to both stderr and the console Timeseq.
*/

void Trace::console(double ts, string const &line)
{
  if (ts > 1e6) {
    cerr << "Silly timestamp for following message ts="s + repr_0_3f(ts) + " rti->realTimeOffset=" + repr_0_3f(rti->realTimeOffset) + "\n";
  }
  cerr << line + "\n";
  if (ts <= 1e6 && !saveActive.load() && consoleSeq) {
    auto yv = consoleSeq->addNew(ts);
    auto msg = reinterpret_cast<YogaConsoleMsg *>(yv.buf);
    msg->message = yogic_intern(line);
  }
}

/*
  Bind an engine or timeseq to this trace. They'll be started and stopped by startEngines/stopEngines, and
  update will be called whenever there is new data to process.
  Sets the label of the engine to its name (the label is prepended to all logs generated by the engine).
*/

void Trace::addEngine(string const &name, shared_ptr<GenericEngine> const &engine, bool autostart)
{
  engine->trace = shared_from_this();
  engine->rti = rti;
  engine->label = engine->engineName = name;
  engines.push_back(Trace::EngineLink{engine, false, autostart});
  if (engine->verbose >= 1) engine->console("verbose="s + to_string(engine->verbose));
}

shared_ptr<GenericEngine> Trace::getEngine(string const &name)
{
  for (auto &it : engines) {
    if (it.engine && it.engine->engineName == name) return it.engine;
  }
  return nullptr;
}

void Trace::clearEngines()
{
  engines.clear();
}

void Trace::addTimeseq(string const &name, shared_ptr<YogaTimeseq> const &timeseq, bool dontSave)
{
  for (auto &it : timeseqs) {
    if (it.timeseq && it.timeseq->seqName == name) {
      throw new runtime_error("Timeseq "s + name + " already exists");
    }
  }
  timeseq->rti = rti;
  timeseq->seqName = name;
  timeseqs.push_back(Trace::TimeseqLink{timeseq, dontSave});
}

shared_ptr<YogaTimeseq> Trace::getTimeseq(string const &name)
{
  for (auto &it : timeseqs) {
    if (it.timeseq && it.timeseq->seqName == name) return it.timeseq;
  }
  return nullptr;
}

shared_ptr<YogaTimeseq> Trace::getOrAddTimeseq(string const &name, YogaType *type)
{
  auto seq = getTimeseq(name);
  if (!seq) {
    seq = make_shared<YogaTimeseq>(rti->reg.get(), type);
    addTimeseq(name, seq, false);
  }
  return seq;
}

bool Trace::bindSeq(
  YogaContext const &ctx,
  shared_ptr<YogaTimeseq> &ptr,
  string typeName,
  string seqName,
  bool dontSave)
{
  if (ptr) return true;

  auto seq = getTimeseq(seqName);
  if (seq) {
    ptr = seq;
  }
  else {
    auto t = ctx("bindSeq").getType(typeName);
    if (!t) return ctx.logError("Seq type "s + shellEscape(typeName) + " not defined");
    ptr = make_shared<YogaTimeseq>(ctx.reg, t);
    addTimeseq(seqName, ptr, dontSave);
  }
  return true;
}


void Trace::clearTimeseqs()
{
  timeseqs.clear();
}


bool Trace::isAnyExtIO() const
{
  for (auto &it : timeseqs) {
    if (it.timeseq && (it.timeseq->isExtIn || it.timeseq->isExtOut)) return true;
  }
  return false;
}
