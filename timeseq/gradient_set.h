#pragma once
#include "common/std_headers.h"

#include "numerical/windowfunc.h"
#include "numerical/numerical.h"
#include "../jit/context.h"

struct GradientSet {
  GradientSet(YogaContext const &_ctx);
  ~GradientSet();
  YogaContext ctx;
  std::unordered_map<U8 *, U8 *> grads;

  double gradMult{1.0};
  double windowBeginTs{0.0};
  double windowEndTs{1.0e99};
  apr_pool_t *mem{nullptr};
  bool nanFlag{false};
  bool errFlag{false};
  string windowFuncName{"rectangular"};
  std::function<double(double)> windowFunc{&rectangularWindow};

  vector<double> paramGradAccum;

  void addGrad(U8 *value, U8 *g, double coeff, YogaType *t);
  void addWindowedGrad(U8 *value, U8 *g, double coeff, double ts, YogaType *t);
  U8 *getGrad(U8 *value, YogaType *t);
  YogaValue getGrad(YogaValue const &value);
};
