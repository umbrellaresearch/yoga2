#include "common/std_headers.h"

#include "./timeseq.h"

#include <zlib.h>

#include "./trace.h"
#include "common/packetbuf_types.h"
#include "../comm/engine.h"



YogaTimeseq::YogaTimeseq(YogaCompilation *_reg, YogaType *_type)
  : reg(_reg),
    type(_type)
{
  mem = new_yogic_pool(reg->mem, "fromPacketStream");
}

YogaTimeseq::~YogaTimeseq()
{
  if (mem) free_yogic_pool(mem);
}


string YogaTimeseq::desc()
{
  auto lSampleCount = sampleCount();
  auto lBeginTs = beginTs();
  auto lEndTs = endTs();

  double hz = (lSampleCount > 1 && lEndTs > lBeginTs) ? 
    ((double)lSampleCount - 1.0) / (lEndTs - lBeginTs) : 
    0.0;

  return rightPadTo(seqName, 20) + 
    ": " + repr_0_6f(lBeginTs) + " - " + repr_0_6f(lEndTs) + " " + repr(lSampleCount) +
    (hz != 0.0 ? (" (" + repr_0_3f(1000.0/hz) + "mS interval, " + repr_0_3f(hz) + " Hz)") : "");
}



void YogaTimeseq::add(double ts, U8 *value)
{
  values_.insert(make_pair(ts, value));

  if (needBackdateInitial) {
    needBackdateInitial = false;
    if (ts > 0.0) {
      values_.insert(make_pair(0.0, value));
    }
  }
  if (rti) {
    if (0) cerr << "add(" + repr(ts) + ", " + repr_ptr(value) + 
        "): timeWindow=" + repr_0_3f(rti->timeWindow) +
        " values_.size()=" + repr(values_.size()) + "\n";
    if (rti->timeWindow > 0.0 && ts > rti->timeWindow) {
      eraseBefore(ts - rti->timeWindow);
    }
    if (!rti->hardHalt && rti->hardHaltTs && ts > rti->hardHaltTs) {
      if (0) cerr << seqName + ": hardHalt because " +
          repr_0_3f(ts) + " > " + repr_0_3f(rti->hardHaltTs) + "\n";
      rti->hardHalt = true;
    }
    if (!rti->softHalt && rti->softHaltTs && ts > rti->softHaltTs) {
      if (0) cerr << seqName + ": softHalt because " +
          repr_0_3f(ts) + " > " + repr_0_3f(rti->softHaltTs) + "\n";
      rti->softHalt = true;
    }
  }
}

void YogaTimeseq::add(YogaValue const &value)
{
  assert(value.type == type);
  add(value.ts, value.buf);
}

void YogaTimeseq::addAll(YogaTimeseq const &other)
{
  for (auto &[ts, buf] : other.values_) {
    add(ts, buf);
  }
}

void YogaTimeseq::eraseBefore(double ts)
{
  auto it = values_.lower_bound(ts);
  if (it != values_.end() && it != values_.begin()) {
    values_.erase(values_.begin(), it);
  }
}

void YogaTimeseq::eraseEqualBefore(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it != values_.end() && it != values_.begin()) {
    values_.erase(values_.begin(), it);
  }
}

void YogaTimeseq::eraseAfter(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it != values_.end()) {
    values_.erase(it, values_.end());
  }
}

void YogaTimeseq::eraseEqualAfter(double ts)
{
  auto it = values_.lower_bound(ts);
  if (it != values_.end()) {
    values_.erase(it, values_.end());
  }
}

double YogaTimeseq::beginTs()
{
  auto it = values_.begin();
  if (it == values_.end()) return 0.0;
  return it->first;
}

double YogaTimeseq::endTs()
{
  auto it = values_.rbegin();
  if (it == values_.rend()) return 0.0;
  return it->first;
}

size_t YogaTimeseq::sampleCount()
{
  return values_.size();
}

bool YogaTimeseq::empty()
{
  return values_.empty();
}


YogaValue YogaTimeseq::getNew(double ts)
{
  YogaValue ret(reg, type, ts, mem);
  return ret;
}

YogaValue YogaTimeseq::addNew(double ts)
{
  YogaValue ret(reg, type, ts, mem);
  add(ret);
  return ret;
}



YogaValue YogaTimeseq::getFirst()
{
  auto it = values_.begin();
  if (it == values_.end()) return YogaValue(reg, type, type->getZero(), 0.0);
  return YogaValue(reg, type, it->second, it->first);
}

YogaValue YogaTimeseq::getLast()
{
  auto it = values_.rbegin();
  if (it == values_.rend()) return YogaValue(reg, type, type->getZero(), 0.0);
  return YogaValue(reg, type, it->second, it->first);
}

YogaValue YogaTimeseq::getLastMatching(std::function<bool(U8 *)> filter, int maxScan)
{
  auto it = values_.rbegin();
  if (it == values_.rend()) return YogaValue(reg, type, mem);
  while (true) {
    if (it->second && filter(it->second)) {
      return YogaValue(reg, type, it->second, it->first);
    }
    it++;
    maxScan--;
    if (it == values_.rend() || maxScan <= 0) return YogaValue(reg, type, mem);
  }
}


vector<YogaValue> YogaTimeseq::getNEqualAfter(double ts, size_t n)
{
  vector<YogaValue> ret;

  if (values_.empty()) return ret;

  auto it = values_.lower_bound(ts);
  while (it != values_.end() && ret.size() < n) {
    ret.emplace_back(reg, type, it->second, it->first);
    it++;
  }
  return ret;
}

vector<YogaValue> YogaTimeseq::getNBefore(double ts, size_t n)
{
  vector<YogaValue> ret;

  if (values_.empty()) return ret;

  auto it = values_.lower_bound(ts);
  while (it != values_.end() && ret.size() < n) {
    if (it->first < ts) {
      ret.emplace_back(reg, type, it->second, it->first);
    }
    if (it == values_.begin()) break;
    it--;
  }
  reverse(ret.begin(), ret.end());
  return ret;
}

YogaValue YogaTimeseq::getClosest(double ts)
{
  if (values_.empty()) return YogaValue(reg, type, type->getZero(), 0.0);

  auto it = values_.lower_bound(ts); // it.first >= ts
  if (it == values_.end()) {
    // beyond end of values, so choose last one
    return YogaValue(reg, type, values_.rbegin()->second, values_.rbegin()->first); // already checked to not be empty
  }
  auto itNext = it;
  auto itPrev = it;
  itNext++;
  if (itNext == values_.end()) {
    itNext = it;
  }
  if (itPrev != values_.begin()) {
    itPrev--;
  }

  double diffIt = abs(ts - it->first);
  double diffNext = abs(ts - itNext->first);
  double diffPrev = abs(ts - itPrev->first);

  // WRITEME: think through logic here, make sure I'm not missing a case
  if (diffIt <= diffNext && diffIt <= diffPrev) {
    return YogaValue(reg, type, it->second, it->first);
  }
  else if (diffNext <= diffPrev && diffNext <= diffIt) {
    return YogaValue(reg, type, itNext->second, itNext->first);
  }
  else {
    return YogaValue(reg, type, itPrev->second, itNext->first);
  }
}

YogaValue YogaTimeseq::getInputFor(double prevOutputTs, double minDelta)
{
  if (values_.empty()) return YogaValue(reg, type, type->getZero(), 0.0);

  auto it = values_.upper_bound(prevOutputTs); // it.first > prevOutputTs
  if (it == values_.end()) {
    // beyond end of values, so choose last one
    return YogaValue(reg, type, values_.rbegin()->second, values_.rbegin()->first); // already checked to not be empty
  }
  auto itNext = it;
  itNext++;
  int bumpCount = 0;
  while (itNext != values_.end() && itNext->first < prevOutputTs + minDelta) {
    if (++bumpCount > 50) {
      cerr << "timeseq: it->first=" + repr_0_3f(it->first) +
          " itNext->first=" + repr_0_3f(itNext->first) +
          " prevOutputTs=" + repr_0_3f(prevOutputTs) +
          " minDelta=" + repr_0_3f(minDelta) + "\n";
      throw runtime_error("getInputFor error"s);
    }
    it = itNext;
    itNext++;
  }
  return YogaValue(reg, type, it->second, it->first);
}

YogaValue YogaTimeseq::getEqualAfter(double ts)
{
  auto it = values_.lower_bound(ts);
  if (it == values_.end()) return YogaValue(reg, type, type->getZero(), 0.0);
  return YogaValue(reg, type, it->second, it->first);
}

YogaValue YogaTimeseq::getAfter(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it == values_.end()) return YogaValue(reg, type, type->getZero(), 0.0);
  return YogaValue(reg, type, it->second, it->first);
}

YogaValue YogaTimeseq::getEqualAfterSoft(double ts)
{
  auto it = values_.lower_bound(ts);
  if (it != values_.end()) {
    return YogaValue(reg, type, it->second, it->first);
  }
  auto rit = values_.rbegin();
  if (rit != values_.rend()) {
    return YogaValue(reg, type, rit->second, rit->first);
  }
  return YogaValue(reg, type, type->getZero(), 0.0);
}

YogaValue YogaTimeseq::getAfterSoft(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it != values_.end()) {
    return YogaValue(reg, type, it->second, it->first);
  }
  auto rit = values_.rbegin();
  if (rit != values_.rend()) {
    return YogaValue(reg, type, rit->second, rit->first);
  }
  return YogaValue(reg, type, type->getZero(), 0.0);
}

pair<YogaValue, YogaValue> YogaTimeseq::getBracket(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it != values_.end()) {
    if (it != values_.begin()) {
      auto it2 = it;
      it2--;
      return make_pair(
        YogaValue(reg, type, it2->second, it2->first),
        YogaValue(reg, type, it->second, it->first));
    }
    else {
      return make_pair(YogaValue(reg, type, it->second, it->first), YogaValue(reg, type, it->second, it->first));
    }
  }
  auto rit = values_.rbegin();
  if (rit != values_.rend()) {
    auto rit2 = rit;
    rit2++;
    if (rit2 != values_.rend()) {
      return make_pair(
        YogaValue(reg, type, rit2->second, rit2->first),
        YogaValue(reg, type, rit->second, rit->first));
    }
    else {
      return make_pair(
        YogaValue(reg, type, rit->second, rit->first),
        YogaValue(reg, type, rit->second, rit->first));
    }
  }
  return make_pair(YogaValue(), YogaValue());
}

YogaValue YogaTimeseq::getEqualBefore(double ts)
{
  auto it = values_.upper_bound(ts);
  if (it == values_.end()) {
    auto rit = values_.rbegin();
    if (rit == values_.rend()) return YogaValue(reg, type, type->getZero());
    return YogaValue(reg, type, rit->second, rit->first);
  }
  if (it == values_.begin()) {
    return YogaValue(reg, type, type->getZero(), 0.0);
  }
  it--;
  return YogaValue(reg, type, it->second, it->first);
}

// -------------

size_t YogaTimeseq::savePacket(string const &fn) const
{
  auto fullfn = fn + ".bin.gz";
  gzFile fp = gzopen(fullfn.c_str(), "w");
  if (!fp) {
    throw runtime_error(fullfn + ": save failed"s);
  }
  toPacketStream(fp);
  auto ret = gztell(fp);
  gzclose(fp);
  return ret;
}

size_t YogaTimeseq::loadPacket(string const &fn)
{
  auto fullfn = fn + ".bin.gz";
  if (access(fullfn.c_str(), R_OK) < 0) {
    return 0;
  }
  gzFile fp = gzopen(fullfn.c_str(), "r");
  if (!fp) {
    throw runtime_error(fullfn + ": load failed"s);
  }

  packet headerPacket = packet::from_gzfile_boxed(fp);
  YogaType *oldType{nullptr};
  packetio::packet_rd_value(headerPacket, oldType, reg->mkCtx("load"));

  fromPacketStream(fp, oldType);
  auto ret = gztell(fp);
  gzclose(fp);
  return ret;
}

string YogaTimeseq::contentTypeName()
{
  if (!type) return "";
  return type->clsName;
}


string YogaTimeseq::contentTypeVersionString()
{
  if (!type) return "";
  return type->getTypeAndVersion();
}

YogaValue YogaTimeseq::getInterpolated(double ts)
{
  auto [v1, v2] = getBracket(ts);
  if (v1.ts == 0.0 && v2.ts == 0.0) {
    return v1;
  }
  else if (v1.ts == 0.0) {
    return v2;
  }
  else if (v2.ts == 0.0) {
    return v1;
  }
  else if (v1.ts == v2.ts) {
    return v1;
  }
  else if (v2.ts > v1.ts) {
    throw runtime_error("WRITEME: getInterpolated");
    //double cb = limit((ts - v1.ts) / (v2.ts - v1.ts), 0.0, 1.0);
    //return linearComb(1.0 - cb, v1, cb, v2);
  }
  else {
    throw runtime_error("getInterpolated: unhandled case"s);
  }
}



shared_ptr<YogaTimeseq> YogaTimeseq::copyFull() const
{
  auto ret = make_shared<YogaTimeseq>(reg, type);
  ret->seqName = seqName;
  ret->values_ = values_;
  ret->needBackdateInitial = needBackdateInitial;
  ret->isExtIn = isExtIn;
  ret->isExtOut = isExtOut;
  return ret;
}

shared_ptr<YogaTimeseq> YogaTimeseq::copyEmpty() const
{
  auto ret = make_shared<YogaTimeseq>(reg, type);
  ret->seqName = seqName;
  ret->needBackdateInitial = needBackdateInitial;
  ret->isExtIn = isExtIn;
  ret->isExtOut = isExtOut;
  return ret;
}

shared_ptr<YogaTimeseq> YogaTimeseq::getGradOf(GradientSet &grads) const
{
  auto ret = make_shared<YogaTimeseq>(reg, type);
  ret->seqName = seqName + "Grad";

  for (auto &[ts, v] : values_) {
    if (!v) continue;
    U8 *g = grads.getGrad(v, type);
    assert(g);
    ret->add(ts, g);
  }

  return ret;
};

#if 0
  void addPulse(R pulseBeginTs, R pulseEndTs, T const &value, std::function<double(double)> windowFunc)
  {
    for (auto it = values_.lower_bound(pulseBeginTs); it != values_.end() && it->first <= pulseEndTs; it++) {
      double ts = it->first;
      double relw = (ts - pulseBeginTs) / (pulseEndTs - pulseBeginTs);
      double coeff = windowFunc(relw);
      if (coeff == 0.0) continue;

      auto vp = static_pointer_cast<T>(it->second);
      if (!vp) continue;

      it->second = static_pointer_cast<void>(make_shared<T>(linearComb(1.0, *vp, coeff, value)));
    }
  }

  void addPulseGrad(
      GradientSet &grads,
      R pulseBeginTs,
      R pulseEndTs,
      T const &value,
      std::function<double(double)> windowFunc)
  {
    for (auto it = values_.lower_bound(pulseBeginTs); it != values_.end() && it->first <= pulseEndTs; it++) {
      double ts = it->first;
      double relw = (ts - pulseBeginTs) / (pulseEndTs - pulseBeginTs);
      double coeff = windowFunc(relw);
      if (coeff == 0.0) continue;

      auto vp = static_pointer_cast<T>(it->second);
      if (!vp) continue;

      grads.addGrad(vp.get(), value, coeff);
    }
  }

  double getPulseMetric(R pulseBeginTs, R pulseEndTs, T const &value, std::function<double(double)> windowFunc)
  {
    double tot = 0.0;
    double denom = 0.0;
    for (auto it = values_.lower_bound(pulseBeginTs); it != values_.end() && it->first <= pulseEndTs; it++) {
      double ts = it->first;
      double relw = (ts - pulseBeginTs) / (pulseEndTs - pulseBeginTs);
      double coeff = windowFunc(relw);
      if (coeff == 0.0) continue;
      denom += coeff;

      auto vp = static_pointer_cast<T>(it->second);
      if (!vp) continue;

      tot += coeff * linearMetric(*vp.get(), value);
    }
    if (denom == 0.0) return 0.0;
    return tot / denom;
  }
#endif

vector<YogaValue> YogaTimeseq::getValuesInRange(
    R itBeginTs,
    R itEndTs,
    double maxPts,
    size_t stopAfter)
{
  vector<YogaValue> ret;
  ret.reserve(values_.size()+2);
  auto it = values_.lower_bound(itBeginTs);
  if (it == values_.end()) {
    auto rit = values_.rbegin();
    if (rit != values_.rend()) {
      ret.emplace_back(reg, type, rit->second, rit->first);
      ret.emplace_back(reg, type, rit->second, itEndTs);
    }
    return ret;
  }
  if (it != values_.begin()) {
    it--;
  }

  U8 *vp{nullptr};
  double minDt = (maxPts >= 1.0) ? (itEndTs - itBeginTs) / maxPts : 0.0;
  double callVt = 0.0;
  size_t callCount = 0;
  while (true) {
    if (it == values_.end()) {
      if (vp) {
        ret.emplace_back(reg, type, vp, callVt);
      }
      break;
    }
    double vt = it->first;
    vp = it->second;
    if (vt - callVt >= minDt || vt >= itEndTs) {
      ret.emplace_back(reg, type, vp, vt);
      callVt = vt;
    }
    if (vt >= itEndTs) {
      break;
    }
    if (stopAfter > 0 && callCount >= stopAfter) {
      break;
    }
    callCount++;
    it++;
  }
  return ret;
}


void YogaTimeseq::toPacketStream(struct gzFile_s *gzfp) const
{
  packet p;

  packetio::packet_wr_value(p, type, reg->mkCtx("toPacketStream"));

  p.to_gzfile_boxed(gzfp);
  for (auto &[ts, v] : values_) {
    if (!v) continue;
    p.clear();
    p.add(ts);
    (*type->packetWr)(p, v);
    p.to_gzfile_boxed(gzfp);
  }
}

void YogaTimeseq::fromPacketStream(struct gzFile_s *gzfp, YogaType *oldType)
{
  /*
    WRITEME: if oldType and type are different, we need to do some kind of conversion.
    Note that oldType hasn't been compiled yet -- we just read it from the stream
    with packet_rd. If it's equivalent to this->type, then we shouldn't bother.
    Otherwise we should compile a read function that reads the old format but stuffs
    it into a new-format object in memory.
  */

 auto packetRd = type->getPacketRdForLegacy(oldType);
 if (!packetRd) throw runtime_error("Can't read old format");

  while (1) {
    packet p = packet::from_gzfile_boxed(gzfp);
    if (p.size() == 0) break;
    double ts = 0.0;
    p.get(ts);
    auto buf = type->mkInstance(mem);
    (*packetRd)(p, buf, mem);
    p.check_at_end();
    add(ts, buf);
    if (ts > 1e6) {
      cerr << "fromPacketStream: "s + repr_type(oldType) + ": silly ts=" + repr_0_3f(ts) + "\n";
    }
  }
}
