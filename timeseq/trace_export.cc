#include "./trace.h"

#include "./timeseq.h"
#include "common/uv_wrappers.h"
#include "../comm/engine.h"
#include "../jit/runtime.h"
#include "../jit/context.h"
#include "../jit/compilation.h"

struct ExportColBase {
  ExportColBase(string const &_colName, YogaTimeseq *_seq)
    : colName(_colName),
      seq(_seq)
    {
    }
  virtual ~ExportColBase()
  {
  }
  string colName;
  YogaTimeseq *seq;

  virtual void writeValue(FILE *f, YogaValue const &yv) = 0;

  virtual void writeHeader(FILE *f, string const &prefix)
  {
    auto colNameEsc = quoteStringC(dotJoin(prefix, colName));
    fwrite(colNameEsc.c_str(), colNameEsc.size(), 1, f);
  }
};

template<typename T>
struct ExportCol : ExportColBase {
  ExportCol(string const &_colName, YogaTimeseq *_seq, YogaType *_t, string const &_subName)
    : ExportColBase(_colName, _seq),
      acc(_t, _subName)
  {
  }
  YogaValueAccessor<T> acc;

  void writeValue(FILE *f, YogaValue const &yv) override;
};

template<typename T>
void ExportCol<T>::writeValue(FILE *f, YogaValue const &yv)
{
  auto v = acc.rd(yv);
  auto vStr = to_string(v);
  fwrite(vStr.c_str(), vStr.size(), 1, f);
}


void Trace::exportCsv(FILE *f, vector<string> const &seqNames, vector<R> const &dts, bool doHeader)
{
  vector<shared_ptr<ExportColBase>> cols;

  for (auto &seqName : seqNames) {
    auto seq = getTimeseq(seqName);
    if (!seq) throw runtime_error("No seq "s + seqName);

    auto t = seq->type;

    for (auto &[accName, acc] : t->accessorsByName) {
      if (endsWith(accName, "::rd.d")) {
        auto memName = accName.substr(0, accName.size() - 6);
        auto col = make_shared<ExportCol<R>>(
          dotJoin(seqName, memName),
          seq.get(),
          t,
          memName);
        cols.push_back(col);
      }
    }
  }

  R endTs = 0.0;
  R step = 0.01;

  if (doHeader) {
    fputs("\"dt\"", f);
    for (auto prefix : {"t0", "t1", "t2"}) {
      for (auto &col : cols) {
        fputc(',', f);
        col->writeHeader(f, prefix);
        endTs = max(endTs, col->seq->endTs());
      }
    }
    fputc('\n', f);
  }

  for (auto dt : dts) {
    for (auto ts0 = step; ts0 < endTs - step; ts0 += step) {
      auto ts1 = ts0 + 0.5 * dt;
      auto ts2 = ts0 + dt;

      auto dtStr = to_string(dt);
      fputs(dtStr.c_str(), f);

      for (auto [prefix, ts] : {
        make_tuple("t0", ts0),
        make_tuple("t1", ts1),
        make_tuple("t2", ts2)
      }) {
        for (auto &col : cols) {
          fputc(',', f);
          auto yv = col->seq->getEqualBefore(ts);
          col->writeValue(f, yv);
        }
      }

      fputc('\n', f);

    }
  }

}
