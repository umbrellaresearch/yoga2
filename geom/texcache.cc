#include "./texcache.h"
#include <csetjmp>
extern "C" {
#include <jpeglib.h>
#include <png.h>
}
#include "../drivers/video/video_types.h"
#include "./yogagl.h"
#include "../db/yoga_layout.h"

TexCache::TexCache()
  :n_tex(0)
{
}

TexCache::~TexCache()
{
  if (n_tex > 0) {
    glDeleteTextures(n_tex, texIds.data());
  }
  n_tex = 0;
}

void TexCache::alloc(size_t _n_tex)
{
  if (n_tex != 0) return;
  n_tex = _n_tex;
  texIds.resize(n_tex);
  lastUses.resize(n_tex);
  keys.resize(n_tex);
  sizes.resize(n_tex);

  GLint saveTex;
  glGetIntegerv(GL_TEXTURE_BINDING_2D, &saveTex);

  glGenTextures(n_tex, texIds.data());
  for (int i=0; i<n_tex; i++) {
    glBindTexture(GL_TEXTURE_2D, texIds[i]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    float bgColor[4] = {0.0f, 0.0f, 0.0f, 1.0f};
    glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, bgColor);
    lastUses[i] = 0;
    keys[i] = nullptr;
  }
  
  glBindTexture(GL_TEXTURE_2D, saveTex);
}

bool TexCache::getIndexFor(void const *key, int &bestIndex)
{
  if (n_tex == 0) alloc(10);
  for (int texi = 0; texi < n_tex; texi ++) {
    if (key == keys[texi]) {
      lastUses[texi] = lastUseCounter++;
      bestIndex = texi;
      return true;
    }
  }

  bestIndex = 0;
  for (auto texi = 1; texi < n_tex; texi ++) {
    if (lastUses[texi] < lastUses[bestIndex]) bestIndex = texi;
  }
  return false;
}

void TexCache::setIndexFor(void const *key, int bestIndex)
{
  keys[bestIndex] = key;
  lastUses[bestIndex] = lastUseCounter++;
}

U32 TexCache::getTexid(int texIndex)
{
  lastUses[texIndex] = lastUseCounter++;
  return texIds[texIndex];
}

void TexCache::setPixelsRGB(int index, U32 width, U32 height, U8 *pixels)
{
  GLint saveTex = 0;
  glGetIntegerv(GL_TEXTURE_BINDING_2D, &saveTex);
  glBindTexture(GL_TEXTURE_2D, texIds[index]);
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGB,
      width, height, 0,
      GL_RGB, GL_UNSIGNED_BYTE, pixels);
  glBindTexture(GL_TEXTURE_2D, saveTex);

  sizes[index] = ImVec2(width, height);
}

void TexCache::setPixelsRGBA(int index, U32 width, U32 height, U8 *pixels)
{
  GLint saveTex = 0;
  glGetIntegerv(GL_TEXTURE_BINDING_2D, &saveTex);
  glBindTexture(GL_TEXTURE_2D, texIds[index]);
  glTexImage2D(
      GL_TEXTURE_2D, 0, GL_RGBA,
      width, height, 0,
      GL_RGBA, GL_UNSIGNED_BYTE, pixels);
  glBindTexture(GL_TEXTURE_2D, saveTex);

  sizes[index] = ImVec2(width, height);
}


struct MyJpegErrorMgr {
  struct jpeg_error_mgr pub; // must be first member
  jmp_buf setjmpBuf;
  string errMsg;
};

static void jpegErrorExit(j_common_ptr cinfo)
{
  MyJpegErrorMgr *mgr = reinterpret_cast<MyJpegErrorMgr *>(cinfo->err);

  char errBuf[JMSG_LENGTH_MAX];
  (*cinfo->err->format_message)(cinfo, errBuf);
  mgr->errMsg = errBuf;
}


bool TexCache::setJpeg(int texIndex, Blob &buffer)
{
  assert(texIndex >= 0 && texIndex < n_tex);
  if (buffer.size() < 2) {
    cerr << "TexCache::setJpeg: short blob size="s + to_string(buffer.size()) + "\n";
    return false;
  }
  if (buffer[0] != 0xff || buffer[1] != 0xd8) {
    if (buffer[0] != 0 || buffer[1] != 0) {
      cerr << "TexCache::setJpeg: not a jpeg "s + to_string(buffer[0]) + " " + to_string(buffer[1]) + "\n";
    }
    return false;
  }

  struct jpeg_source_mgr srcmgr;
  struct jpeg_decompress_struct cinfo;

  struct MyJpegErrorMgr jerr;
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = &jpegErrorExit;
  if (setjmp(jerr.setjmpBuf)) {
    cerr << "jpeg error "s + jerr.errMsg + "\n";
    jpeg_destroy_decompress(&cinfo);
    return false;
  }

  cinfo.src = &srcmgr;
  jpeg_create_decompress(&cinfo);

  jpeg_mem_src(&cinfo, buffer.data(), buffer.size());
  
  jpeg_read_header(&cinfo, (boolean)1);
  cinfo.do_fancy_upsampling = (boolean)0;
  cinfo.dct_method = JDCT_IFAST;
  cinfo.out_color_space = JCS_RGB;
  jpeg_start_decompress(&cinfo);

  if (cinfo.output_components != 3) {
    cerr << "jpeg: wrong color space, failing\n";
    jpeg_abort_decompress(&cinfo);
    jpeg_destroy_decompress(&cinfo);
    return false;
  }

  auto row_stride = cinfo.output_width * cinfo.output_components;
  Blob pixels(row_stride * cinfo.image_height);

  const int MAX_LINES = 16;
  assert(cinfo.rec_outbuf_height < MAX_LINES);
  u_char *lines[MAX_LINES];

  for (int y = 0; y < cinfo.image_height; y += cinfo.rec_outbuf_height) {
    for (int i = 0; i < cinfo.rec_outbuf_height; i++) {
      lines[i] = &pixels[(y + i) * row_stride];
    }
    jpeg_read_scanlines(&cinfo, lines, min(
      cinfo.rec_outbuf_height, 
      (int)(cinfo.image_height - y)));
  }
  jpeg_finish_decompress(&cinfo);
  jpeg_destroy_decompress(&cinfo);

  if (0) cerr << "jpeg decompressed to " + 
      repr(cinfo.output_width) + "x" + 
      repr(cinfo.image_height) + "x" + 
      repr(cinfo.output_components) + "\n";

  setPixelsRGB(texIndex, cinfo.image_width, cinfo.image_height, pixels.data());
  return true;
}

bool TexCache::setRgb(int texIndex, int width, int height, Blob &buffer)
{
  setPixelsRGB(texIndex, width, height, buffer.data());
  return true;
}

bool TexCache::setJpegFile(int texIndex, string const &fn)
{
  int fd = open(fn.c_str(), O_RDONLY, 0777);
  if (fd < 0) {
    cerr << fn + ": " + strerror(errno) + "\n";
    return false;
  }

  struct stat st;
  if (fstat(fd, &st) < 0) {
    return false;
  }
  auto size = st.st_size;

  Blob buf(size);

  auto nr = pread(fd, buf.data(), size, 0);

  if (nr < 0) {
    cerr << fn + ": "s + strerror(errno) + "\n";
    close(fd);
    return false;
  }
  else if (nr == 0) {
    cerr << fn + ": empty\n";
    close(fd);
    return false;
  }
  else if (nr < size) {
    cerr << fn + ": partial read\n";
    close(fd);
    return false;
  }

  return setJpeg(texIndex, buf);
}

bool TexCache::setPngFile(int texIndex, string const &fn)
{
  FILE *fp = fopen(fn.c_str(), "r");
  if (!fp) {
    cerr << fn + ": " + strerror(errno) + "\n";
    return false;
  }

  png_image png;

  memset(&png, 0, sizeof(png));
  png.version = PNG_IMAGE_VERSION;
  png.opaque = nullptr;

  if (png_image_begin_read_from_stdio(&png, fp) < 0) {
    throw runtime_error("PNG begin-read " + shellEscape(fn) + " failed: " + repr(png.warning_or_error));
  }
  png.format = PNG_FORMAT_RGBA;

  U32 rowStride = png.width * sizeof(U32);
  Blob pixels(png.height * rowStride);
  
  png_image_finish_read(&png, 0x00000000, pixels.data(), rowStride, nullptr);
  if (png.warning_or_error) {
    throw runtime_error("PNG read " + shellEscape(fn) + " failed: " + repr(png.warning_or_error));
  }
  fclose(fp);

  setPixelsRGBA(texIndex, png.width, png.height, pixels.data());

  png_image_free(&png);

  if (0) cerr << 
      fn + ": size=" + repr(png.width) + "x" + repr(png.height) +
      " rowStride=" + repr(rowStride) + "\n";

  return true;
}

void *TexCache::getTexidFromImagePng(void const *key, string const &fn)
{
  int index = 0;
  if (!getIndexFor(key, index)) {
    setIndexFor(key, index);
    setPngFile(index, YogaLayout::instance().yogaImagesDir + "/" + fn);
  }

  return reinterpret_cast<void *>((size_t)getTexid(index));
}

