#pragma once
#include "common/std_headers.h"
#include "numerical/numerical.h"
#include "./yogagl.h"
#include "../geom/geom.h"
#include <typeindex>


struct StlSolid;

enum VertexType {
  VERT_POS_NORMAL_UV_COL,
  VERT_POS_COL,
};

struct VertPosNormalUvCol {
  static const VertexType vertexType = VERT_POS_NORMAL_UV_COL;
  VertPosNormalUvCol(vec3 _pos, vec3 _normal, vec2 _uv, vec4 _col)
    : pos(move(_pos)),
      normal(move(_normal)),
      uv(move(_uv)),
      col(move(_col))
  {
  }
  VertPosNormalUvCol(vec3 _pos, vec3 _normal, vec4 _col)
    : pos(move(_pos)),
      normal(move(_normal)),
      uv(0.0f, 0.0f),
      col(move(_col))
  {
  }
  VertPosNormalUvCol(vec3 _pos, vec4 _col)
    : pos(move(_pos)),
      normal(0.0f, 0.0f, 0.0f),
      uv(0.0f, 0.0f),
      col(move(_col))
  {
  }
  VertPosNormalUvCol(vec3 _pos, vec2 _uv, vec4 _col)
    : pos(move(_pos)),
      normal(0.0f, 0.0f, 0.0f),
      uv(move(_uv)),
      col(move(_col))
  {
  }
  vec3 pos;
  vec3 normal;
  vec2 uv;
  vec4 col;
};



struct VertPosCol {
  static const VertexType vertexType = VERT_POS_COL;
  VertPosCol(vec3 _pos, vec4 _col)
    : pos(move(_pos)),
      col(move(_col))
  {
  }
  vec3 pos;
  vec4 col;
};


struct Panel3dShader {
  Panel3dShader(
    string const &_fn,
    VertexType _vertexType,
    vector<string> const &_uniformNames,
    vector<string> const &_attribNames);

  void compile();
  void drawElements();
  void setupProgram(glm::mat4 const &proj, glm::mat4 const &view);
  void setupTexVideoProgram(U32 texId);

  U32 shaderIndex{0};
  U32 progHandle{0}, vtxHandle{0}, geoHandle{0}, tcHandle{0}, teHandle{0}, frgHandle{0};

  string fn;
  VertexType vertexType;
  vector<pair<string, U32>> uniforms;
  vector<pair<string, U32>> attribs;

};

extern Panel3dShader plainDotShader;
extern Panel3dShader plainLineShader;
extern Panel3dShader litSolidShader;
extern Panel3dShader flatSolidShader;

ostream & operator <<(ostream &s, const VertPosNormalUvCol &a);

struct Panel3dDrawCmd {
  Panel3dDrawCmd(
    U32 _shaderIndex,
    U32 _elemIndex,
    U32 _elemCount,
    U32 _drawMode,
    U32 _texId)
    : shaderIndex(_shaderIndex),
      elemIndex(_elemIndex),
      elemCount(_elemCount),
      drawMode(_drawMode),
      texId(_texId)
  {
  }
  U32 shaderIndex;
  U32 elemIndex;
  U32 elemCount;
  U32 drawMode;
  U32 texId;
};


struct Panel3dShaderSetup {
  U32 vertexArrayObject{0};
  U32 vertexBuffer{0};
  U32 elementBuffer{0};
  vector<VertPosNormalUvCol> vtxBufPosNormalUvCol;
  vector<VertPosCol> vtxBufPosCol;
  vector<U32> idxBuf;
};

struct Panel3dDrawList {

  vector<Panel3dDrawCmd> cmdBuf;

  vector<Panel3dShaderSetup> setups;

  vec3 homeEyepos;
  vec3 homeLookat;
  vec3 homeUp;
  float homeFov;

  vec3 eyepos;
  vec3 lookat;
  vec3 up;
  float fov{40.0 * M_PI/180.0};
  float dt{1.0f/60.0f};
  float nearZ{1.0f};
  float farZ{200.0f};

  bool fullScreen{false};
  int vpL{0}, vpT{0}, vpR{0}, vpB{0};

  vec3 pickRayOrigin, pickRayDir;
  bool pickRayActive{false};

  vec3 saveDir;
  bool saveDirActive{false};

  Panel3dDrawList();
  ~Panel3dDrawList();
  void glRender();
  void setupShader(Panel3dShader *shader, glm::mat4 &proj, glm::mat4 &view, U32 texUnit);

  void camFov(R target);
  void camEyepos(vec3 target);
  void camLookat(vec3 target);
  void camStrafe(vec3 dir);
  void camHoverFly(vec3 dir);
  void camFlyHome();
  void camPan(vec2 dir);
  void camOrbitElevate(R orbit, R elevate);
  void camHome();
  void setHoverHome();
  void camIso();
  void camCheckSanity();

  void firstPersonControls();
  void hoverControls();

  void setPickRay();

  void clear();

  void addCmd(Panel3dShader *shader, U32 elemIndex, U32 elemCount, U32 drawMode, U32 texId=0);

  void addDot(
    vec3 const &a,
    vec4 const &col,
    Panel3dShader *shader=nullptr);

  void addLine(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    Panel3dShader *shader=nullptr);

  void addTriangle(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    vec3 const &c, vec4 const &cCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addCircle(
    vec3 const &center,
    vec3 const &axis0,
    vec3 const &axis1,
    vec4 const &col,
    int segments = 16,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addQuad(
    vec3 const &a, vec4 const &aCol,
    vec3 const &b, vec4 const &bCol,
    vec3 const &c, vec4 const &cCol,
    vec3 const &d, vec4 const &dCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addTriangle(
    vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
    vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
    vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addQuad(
    vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
    vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
    vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
    vec3 const &d, vec3 const &dNorm, vec4 const &dCol,
    U32 texId = 0,
    Panel3dShader *shader=nullptr);

  void addText(
    vec3 const &origin, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
    string const &text,
    vec4 const &col,
    ImFont *font,
    Panel3dShader *shader=nullptr);

  void addArrow(
    vec3 const &root,
    vec3 const &axis,
    vec3 const &ca, 
    vec3 const &cb, 
    vec4 const &col,
    Panel3dShader *shader=nullptr);

  void addArrowTriple(
    mat4 const &model,
    Panel3dShader *shader=nullptr);

  void addVideo(mat4 const &model, U32 videoTex, Panel3dShader *shader=nullptr);

  void addSolid(
    StlSolid const &solid,
    mat4 const &m,
    vec4 col,
    Panel3dShader *shader=nullptr);

  bool hitTestTriangle(vec3 a, vec3 b, vec3 c, vec3 &hit, float &distance);
  bool hitTestQuad(vec3 a, vec3 b, vec3 c, vec3 d, vec3 &hit, float &distance);
  bool hitTestPlane(vec3 planeOrig, vec3 planeNormal, vec3 &hit, float &distance);

};

void panel3dInit();

void logGlErrors(char const *where);

#define LOG_GL_ERRORS() logGlErrors((string(__FILE__) + ": " + to_string(__LINE__)).c_str())
