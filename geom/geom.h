#pragma once

#define GLM_FORCE_CUDA 1
#define GLM_ENABLE_EXPERIMENTAL 1
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat2x2.hpp>
#include <glm/mat3x3.hpp>
#include <glm/mat4x4.hpp>
#include <glm/gtx/string_cast.hpp>

using glm::vec2;
using glm::vec3;
using glm::vec4;
using glm::mat2;
using glm::mat3;
using glm::mat4;
using glm::dvec2;
using glm::dvec3;
using glm::dvec4;
using glm::dmat2;
using glm::dmat3;
using glm::dmat4;

std::ostream & operator << (std::ostream &s, glm::dmat2 const &a);
std::ostream & operator << (std::ostream &s, glm::dmat3 const &a);
std::ostream & operator << (std::ostream &s, glm::dmat4 const &a);

std::ostream & operator << (std::ostream &s, glm::dvec2 const &a);
std::ostream & operator << (std::ostream &s, glm::dvec3 const &a);
std::ostream & operator << (std::ostream &s, glm::dvec4 const &a);


std::ostream & operator << (std::ostream &s, glm::mat2 const &a);
std::ostream & operator << (std::ostream &s, glm::mat3 const &a);
std::ostream & operator << (std::ostream &s, glm::mat4 const &a);

std::ostream & operator << (std::ostream &s, glm::vec2 const &a);
std::ostream & operator << (std::ostream &s, glm::vec3 const &a);
std::ostream & operator << (std::ostream &s, glm::vec4 const &a);
