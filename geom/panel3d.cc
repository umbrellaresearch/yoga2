#include "./panel3d.h"
#include "../geom/solid_geometry.h"
#if defined(__APPLE__)
#include "TargetConditionals.h"
#endif
#include "common/uv_wrappers.h"
#include <SDL_scancode.h>
#include "imgui.h"
#include "imgui_internal.h"
#include "path_config.h"

static string g_GlslVersionString;

static vector<Panel3dShader *> *allShaders;


Panel3dShader plainDotShader("shaders/plain_dot.glsl", VERT_POS_COL, {
  "ViewMtx",
  "ProjMtx",
}, {
  "Position",
  "Color"
});

Panel3dShader plainLineShader("shaders/plain_line.glsl", VERT_POS_COL, {
  "ViewMtx",
  "ProjMtx"
}, {
  "Position",
  "Color"
});

Panel3dShader litSolidShader("shaders/lit_solid.glsl", VERT_POS_NORMAL_UV_COL, {
  "ViewMtx",
  "ProjMtx"
}, {
  "Position",
  "Normal",
  "Color"
});

Panel3dShader flatSolidShader("shaders/flat_solid.glsl", VERT_POS_NORMAL_UV_COL, {
  "ViewMtx",
  "ProjMtx",
  "TexUnit",
}, {
  "Position",
  "TexCoord",
});


void logGlErrors(char const *where)
{
  while (auto err = glGetError()) {
    cerr << "GL error after "s + where + ": " + to_string(err) + "\n";
  }
}


ostream & operator <<(ostream &s, const VertPosNormalUvCol &a)
{
  return s <<
    "vtx(pos="s << to_string(a.pos) <<
    ((a.uv.x != 0.0f || a.uv.y != 0.0f) ? (
      " uv="s + to_string(a.uv)
    ) : ""s) <<
    " n=" << to_string(a.normal) <<
    " col=" << to_string(a.col) << ")";
}

static void panel3dCheckShader(GLuint handle, string const &desc)
{
  GLint status = 0, log_length = 0;
  glGetShaderiv(handle, GL_COMPILE_STATUS, &status);
  glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_length);
  if ((GLboolean)status == GL_FALSE) {
    cerr << "panel3dCheckShader: failed to compile "s + desc + 
      " (with GLSL '" + g_GlslVersionString + "')\n";
  }
  if (log_length > 0) {
    vector<char> buf(log_length + 1);
    glGetShaderInfoLog(handle, log_length, NULL, (GLchar*)&buf[0]);
    cerr << &buf[0] << "\n";
  }
  if (!status) throw runtime_error("shader compilation failed");
}

static void panel3dCheckProgram(GLuint handle, string const &desc)
{
  GLint status = 0, log_length = 0;
  glGetProgramiv(handle, GL_LINK_STATUS, &status);
  glGetProgramiv(handle, GL_INFO_LOG_LENGTH, &log_length);
  if ((GLboolean)status == GL_FALSE) {
    cerr << "panel3dCheckShader: failed to link "s + desc + 
      " (with GLSL '" + g_GlslVersionString + "')\n";
  }
  if (log_length > 0) {
    vector<char> buf(log_length+1);
    glGetProgramInfoLog(handle, log_length, NULL, (GLchar*)&buf[0]);
    cerr << &buf[0] << "\n";
  }
  if (!status) throw runtime_error("shader compilation failed");
}



Panel3dShader::Panel3dShader(
  string const &_fn,
  VertexType _vertexType,
  vector<string> const &_uniformNames,
  vector<string> const &_attribNames)
  : fn(_fn),
    vertexType(_vertexType)
{
  if (!allShaders) allShaders = new vector<Panel3dShader *>();
  shaderIndex = allShaders->size();
  allShaders->push_back(this);

  for (auto &name : _uniformNames) {
    uniforms.emplace_back(name, (U32)-1);
  }
  for (auto &name : _attribNames) {
    attribs.emplace_back(name, (U32)-1);
  }
}


void Panel3dShader::compile()
{
  const int verbose = 0;

  int glsl_version = 130;
  sscanf(g_GlslVersionString.c_str(), "#version %d", &glsl_version);

  auto fullfn = string(INSTALL_DIR) + "/" + fn;
  auto shaderCode = readFile(fullfn);

  string vtxCode, geoCode, tcCode, teCode, frgCode;
  string *addTo = nullptr;
  for (auto const &line : splitChar(shaderCode, '\n')) {
    if (line[0] == '#') {
      if (line == "#vertex") {
        addTo = &vtxCode;
        continue;
      }
      else if (line == "#geometry") {
        addTo = &geoCode;
        continue;
      }
      else if (line == "#tessctl") {
        addTo = &tcCode;
        continue;
      }
      else if (line == "#tesseval") {
        addTo = &teCode;
        continue;
      }
      else if (line == "#fragment") {
        addTo = &frgCode;
        continue;
      }
      else {
        cerr << fn + ": warning: Unknown directive "s + line + "\n";
      }
    }
    if (addTo) {
      *addTo += line;
      *addTo += '\n';
    }
  }

  if (verbose >= 1) {
    cerr << "Shader "s + fn + " version " + repr(glsl_version) + "\n";
    if (verbose >= 2) {
      if (!vtxCode.empty()) cerr << "Vtx code:\n"s + vtxCode + "\n";
      if (!geoCode.empty()) cerr << "Geo code:\n"s + geoCode + "\n";
      if (!tcCode.empty()) cerr << "TC code:\n"s + tcCode + "\n";
      if (!teCode.empty()) cerr << "TE code:\n"s + teCode + "\n";
      if (!frgCode.empty()) cerr << "Frg code:\n"s + frgCode + "\n";
    }
  }

  if (!vtxCode.empty()) {
    const GLchar* fullCode[2] = { g_GlslVersionString.c_str(), vtxCode.c_str() };
    vtxHandle = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vtxHandle, 2, fullCode, NULL);
    glCompileShader(vtxHandle);
    panel3dCheckShader(vtxHandle, "vertex shader "s + fn);
  }

  if (!geoCode.empty()) {
    const GLchar* fullCode[2] = { g_GlslVersionString.c_str(), geoCode.c_str() };
    geoHandle = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geoHandle, 2, fullCode, NULL);
    glCompileShader(geoHandle);
    panel3dCheckShader(geoHandle, "geometry shader "s + fn);
  }

  if (!tcCode.empty()) {
    const GLchar* fullCode[2] = { g_GlslVersionString.c_str(), tcCode.c_str() };
    teHandle = glCreateShader(GL_TESS_CONTROL_SHADER);
    glShaderSource(tcHandle, 2, fullCode, NULL);
    glCompileShader(tcHandle);
    panel3dCheckShader(tcHandle, "tesselation control shader "s + fn);
  }

  if (!teCode.empty()) {
    const GLchar* fullCode[2] = { g_GlslVersionString.c_str(), teCode.c_str() };
    geoHandle = glCreateShader(GL_TESS_EVALUATION_SHADER);
    glShaderSource(teHandle, 2, fullCode, NULL);
    glCompileShader(teHandle);
    panel3dCheckShader(teHandle, "geometry shader "s + fn);
  }

  if (!frgCode.empty()) {
    const GLchar* fullCode[2] = { g_GlslVersionString.c_str(), frgCode.c_str() };
    frgHandle = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(frgHandle, 2, fullCode, NULL);
    glCompileShader(frgHandle);
    panel3dCheckShader(frgHandle, "fragment shader "s + fn);
  }

  progHandle = glCreateProgram();
  if (verbose >= 2) {
    cerr << " Shaders handles vtx=" + repr(vtxHandle) + 
      " geo=" + repr(geoHandle) +
      " tc=" + repr(tcHandle) +
      " te=" + repr(teHandle) +
      " frg=" + repr(frgHandle) +
      "\n";
  }
  if (!vtxCode.empty()) glAttachShader(progHandle, vtxHandle);
  if (!geoCode.empty()) glAttachShader(progHandle, geoHandle);
  if (!tcCode.empty()) glAttachShader(progHandle, tcHandle);
  if (!teCode.empty()) glAttachShader(progHandle, teHandle);
  if (!frgCode.empty()) glAttachShader(progHandle, frgHandle);

  glLinkProgram(progHandle);
  panel3dCheckProgram(progHandle, "shader program "s + fn);

  for (auto &[name, index] : attribs) {
    auto loc = glGetAttribLocation(progHandle, name.c_str());
    if (verbose || loc < 0) cerr << "  Attrib "s + name + " at " + repr(loc) + "\n";
    if (loc < 0) {
      cerr << fn + ": No attrib " + shellEscape(name) + "\n";
    }
    index = loc;
  }

  for (auto &[name, index] : uniforms) {
    auto loc = glGetUniformLocation(progHandle, name.c_str());
    if (verbose || loc < 0) cerr << "  Uniform "s + name + " at " + repr(loc) + "\n";
    if (loc < 0) {
      cerr << fn + ": No uniform " + shellEscape(name) + "\n";
    }
    index = loc;
  }

}


// Functions
void panel3dInit()
{
  auto glslVersion = reinterpret_cast<char const *>(glGetString(GL_SHADING_LANGUAGE_VERSION));
  if (!glslVersion) throw runtime_error("No GLSL available");
  cerr << "GLSL version "s + glslVersion + "\n";

  // WRITEME someday: handle other versions
#if __APPLE__
  // GL 3.2 Core + GLSL 150
  g_GlslVersionString = "#version 150\n";
#else
  g_GlslVersionString = "#version 130\n";
#endif

  // Make a dummy GL call (we don't actually need the result)
  // IF YOU GET A CRASH HERE: it probably means that you haven't initialized the OpenGL function loader used by this code.
  // Desktop OpenGL 3/4 need a function loader. See the IMGUI_IMPL_OPENGL_LOADER_xxx explanation above.
  GLint current_texture;
  glGetIntegerv(GL_TEXTURE_BINDING_2D, &current_texture);

  for (auto &it : *allShaders) {
    it->compile();
  }
}


Panel3dDrawList::Panel3dDrawList()
  : setups(allShaders->size()),
    homeEyepos(-0.01, +2.0, 0.0),
    homeLookat(0, 0, -0.5),
    homeUp(0, 0, 1),
    homeFov(glm::radians(40.0)),
    eyepos(homeEyepos),
    lookat(homeLookat),
    up(homeUp),
    fov(homeFov),
    pickRayOrigin(0, 0, 0),
    pickRayDir(0, 0, 0),
    saveDir(0, 0, 0)
{
  camHome();
  cmdBuf.reserve(1024);
}

Panel3dDrawList::~Panel3dDrawList()
{
}

void Panel3dDrawList::camHome()
{
  if (lookat == homeLookat && eyepos == homeEyepos && fov == homeFov && up == homeUp) return;
  lookat = homeLookat;
  eyepos = homeEyepos;
  fov = homeFov;
  up = homeUp;
  uvUiActive = true;
}

void Panel3dDrawList::setHoverHome()
{
  homeLookat = vec3(0, 0, 0);
  homeEyepos = vec3(0, -15, 15);
  homeFov = glm::radians(40.0f);
  homeUp = vec3(0, 0, 1);
  camHome();
}

void Panel3dDrawList::camIso()
{
  vec3 newLookat = vec3(0, 0, 0.0);
  vec3 newEyepos = vec3(0.0, +2.0, 0.0);
  float newFov = glm::radians(41.0f);
  vec3 newUp = vec3(0, 0, 1);
  if (lookat == newLookat && eyepos == newEyepos && fov == newFov && up == newUp) return;
  lookat = newLookat;
  eyepos = newEyepos;
  fov = newFov;
  up = newUp;
  uvUiActive = true;
}

void Panel3dDrawList::camCheckSanity()
{
  if (isnan(lookat.x) || isnan(lookat.y) || isnan(lookat.z) || 
      isnan(eyepos.x) || isnan(eyepos.y) || isnan(eyepos.z) ||
      isnan(up.x) || isnan(up.y) || isnan(up.z) || 
      isnan(fov)) {
    throw runtime_error("NaNs in Panel3dDrawList lookat="s + to_string(lookat) + 
      " eyepos=" + to_string(eyepos) +
      " up=" + to_string(up) +
      " fov=" + to_string(fov));
  }
}


void Panel3dDrawList::clear()
{
  for (auto &setup : setups) {
    setup.vtxBufPosNormalUvCol.clear();
    setup.vtxBufPosCol.clear();
    setup.idxBuf.clear();
  }
  cmdBuf.clear();
}

void Panel3dDrawList::addCmd(Panel3dShader *shader, U32 elemIndex, U32 elemCount, U32 drawMode, U32 texId)
{
  if (drawMode == GL_TRIANGLES) {
    if (elemCount % 3) {
      throw runtime_error("addCmd: given "s + to_string(elemCount) + " elements but we're in GL_TRIANGLES mode");
    }
  }
  else if (drawMode == GL_LINES) {
    if (elemCount % 2) {
      throw runtime_error("addCmd: given "s + to_string(elemCount) + " elements but we're in GL_LINES mode");
    }
  }
  else if (drawMode == GL_POINTS) {
  }
  else {
    throw runtime_error("addCmd: unknown drawMode");
  }

  if (!cmdBuf.empty()) {
    auto &last = cmdBuf.back();
    if (last.shaderIndex == shader->shaderIndex &&
        last.drawMode == drawMode &&
        last.texId == texId &&
        last.elemIndex + last.elemCount == elemIndex) {
      // This is the common case
      last.elemCount += elemCount;
      return;
    }
  }
  cmdBuf.emplace_back(shader->shaderIndex, elemIndex, elemCount, drawMode, texId);
}


void Panel3dDrawList::addDot(
  vec3 const &a, vec4 const &aCol,
  Panel3dShader *shader)
{
  if (!shader) shader = &plainDotShader;
  assert(shader->vertexType == VertPosCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);
  U32 i1 = setup.vtxBufPosCol.size();

  setup.vtxBufPosCol.emplace_back(a, aCol);

  int elemStart = setup.idxBuf.size();
  setup.idxBuf.push_back(i1);

  addCmd(shader, elemStart, 1, GL_POINTS, 0);
}


void Panel3dDrawList::addLine(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  Panel3dShader *shader)
{
  if (!shader) shader = &plainLineShader;
  assert(shader->vertexType == VertPosCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);
  U32 i1 = setup.vtxBufPosCol.size();

  setup.vtxBufPosCol.emplace_back(a, aCol);
  setup.vtxBufPosCol.emplace_back(b, bCol);

  int elemStart = setup.idxBuf.size();
  setup.idxBuf.push_back(i1);
  setup.idxBuf.push_back(i1+1);

  addCmd(shader, elemStart, 2, GL_LINES, 0);
}


void Panel3dDrawList::addTriangle(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  vec3 const &c, vec4 const &cCol,
  U32 texId,
  Panel3dShader *shader)
{
  auto normal = glm::normalize(glm::cross(b-a, c-a));
  addTriangle(
    a, normal, aCol,
    b, normal, bCol,
    c, normal, cCol,
    texId, shader);
}

void Panel3dDrawList::addTriangle(
  vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
  vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
  vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
  U32 texId,
  Panel3dShader *shader)
{
  if (!shader) shader = &litSolidShader;
  assert(shader->vertexType == VertPosNormalUvCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);

  U32 i1 = setup.vtxBufPosNormalUvCol.size();

  setup.vtxBufPosNormalUvCol.emplace_back(a, aNorm, aCol);
  setup.vtxBufPosNormalUvCol.emplace_back(b, bNorm, bCol);
  setup.vtxBufPosNormalUvCol.emplace_back(c, cNorm, cCol);

  int elemStart = setup.idxBuf.size();
  setup.idxBuf.push_back(i1);
  setup.idxBuf.push_back(i1+1);
  setup.idxBuf.push_back(i1+2);

  addCmd(shader, elemStart, 3, GL_TRIANGLES, texId);
}

void Panel3dDrawList::addCircle(
    vec3 const &center,
    vec3 const &axis0,
    vec3 const &axis1,
    vec4 const &col,
    int segments,
    U32 texId,
    Panel3dShader *shader)
{
  float segmentAngle = M_2PI / segments;

  vec3 rad0 = axis0;

  for (int i=0; i<segments; i++) {
    float theta1 = (i+1) * segmentAngle;
    vec3 rad1 = (cosf(theta1) * axis0 + sinf(theta1) * axis1);

    addTriangle(
      center, col,
      center + rad0, col,
      center + rad1, col,
      texId, shader);

    rad0 = rad1;
  }
}

void Panel3dDrawList::addQuad(
  vec3 const &a, vec3 const &aNorm, vec4 const &aCol,
  vec3 const &b, vec3 const &bNorm, vec4 const &bCol,
  vec3 const &c, vec3 const &cNorm, vec4 const &cCol,
  vec3 const &d, vec3 const &dNorm, vec4 const &dCol,
  U32 texId,
  Panel3dShader *shader)
{
  if (!shader) shader = &litSolidShader;
  assert(shader->vertexType == VertPosNormalUvCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);

  U32 i1 = setup.vtxBufPosNormalUvCol.size();

  setup.vtxBufPosNormalUvCol.emplace_back(a, aNorm, aCol);
  setup.vtxBufPosNormalUvCol.emplace_back(b, bNorm, bCol);
  setup.vtxBufPosNormalUvCol.emplace_back(c, cNorm, cCol);
  setup.vtxBufPosNormalUvCol.emplace_back(d, dNorm, dCol);

  int elemStart = setup.idxBuf.size();
  setup.idxBuf.push_back(i1);
  setup.idxBuf.push_back(i1+1);
  setup.idxBuf.push_back(i1+2);

  setup.idxBuf.push_back(i1+1);
  setup.idxBuf.push_back(i1+3);
  setup.idxBuf.push_back(i1+2);

  addCmd(shader, elemStart, 6, GL_TRIANGLES, texId);
}

void Panel3dDrawList::addQuad(
  vec3 const &a, vec4 const &aCol,
  vec3 const &b, vec4 const &bCol,
  vec3 const &c, vec4 const &cCol,
  vec3 const &d, vec4 const &dCol,
  U32 texId,
  Panel3dShader *shader)
{
  auto normal = glm::normalize(glm::cross(b-a, c-a));
  addQuad(
    a, normal, aCol,
    b, normal, bCol,
    c, normal, cCol,
    d, normal, dCol,
    texId,
    shader);
}


void Panel3dDrawList::addText(
    vec3 const &origin, vec3 const &xDir, vec3 const &yDir, vec3 const &norm,
    string const &text,
    vec4 const &col,
    ImFont *font,
    Panel3dShader *shader)
{

  if (!shader) shader = &flatSolidShader;
  assert(shader->vertexType == VertPosNormalUvCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);

  auto *s = text.c_str();
  auto *text_end = &text.back();

  auto lineStart = origin;
  auto curPos = origin;

  while (1) {
    u_int c = u_int(*s);
    if (c == 0) {
      break;
    }
    if (c < 0x80) {
      s ++;
    }
    else {
      s += ImTextCharFromUtf8(&c, s, text_end);
      if (c == 0) // Malformed UTF-8?
          break;
    }
    if (c == '\n') {
      lineStart += yDir;
      curPos = lineStart;
    }
    else if (c == '\r') {
      continue;
    }

    const ImFontGlyph* glyph = font->FindGlyph((ImWchar)c);
    if (!glyph) continue;
    vec3 xMov = glyph->AdvanceX * xDir;
    if (glyph->Visible) {

      auto p00 = curPos + glyph->X0 * xDir + glyph->Y0 * yDir;
      auto p01 = curPos + glyph->X1 * xDir + glyph->Y0 * yDir;
      auto p10 = curPos + glyph->X0 * xDir + glyph->Y1 * yDir;
      auto p11 = curPos + glyph->X1 * xDir + glyph->Y1 * yDir;

      vec2 uv00(glyph->U0, glyph->V0);
      vec2 uv01(glyph->U1, glyph->V0);
      vec2 uv10(glyph->U0, glyph->V1);
      vec2 uv11(glyph->U1, glyph->V1);

      U32 i1 = setup.vtxBufPosNormalUvCol.size();

      setup.vtxBufPosNormalUvCol.emplace_back(p00, norm, uv00, col);
      setup.vtxBufPosNormalUvCol.emplace_back(p01, norm, uv01, col);
      setup.vtxBufPosNormalUvCol.emplace_back(p10, norm, uv10, col);
      setup.vtxBufPosNormalUvCol.emplace_back(p11, norm, uv11, col);

      int elemStart = setup.idxBuf.size();
      setup.idxBuf.push_back(i1);
      setup.idxBuf.push_back(i1+1);
      setup.idxBuf.push_back(i1+2);

      setup.idxBuf.push_back(i1+1);
      setup.idxBuf.push_back(i1+3);
      setup.idxBuf.push_back(i1+2);

      addCmd(shader, elemStart, 6, GL_TRIANGLES, U32(reinterpret_cast<uintptr_t>(font->ContainerAtlas->TexID)));

    }
    curPos += xMov;
  }

}

void Panel3dDrawList::addArrow(
  vec3 const &root,
  vec3 const &axis,
  vec3 const &ca,
  vec3 const &cb,
  vec4 const &col,
  Panel3dShader *shader)
{
  const int NR = 16;
  for (int i=0; i<NR; i++) {
    float theta0 = (i+0) * (M_2PI/NR);
    float theta1 = (i+1) * (M_2PI/NR);

    vec3 rad0 = (sinf(theta0) * ca + cosf(theta0) * cb);
    vec3 rad1 = (sinf(theta1) * ca + cosf(theta1) * cb);

    float shaftRad = 0.1/4;
    float headRad = 0.2/4;
    float tipRad = 0.0/4;

    float basePos = 0.0/4;
    float headPos = 0.7/4;
    float tipPos = 0.9/4;

    addTriangle(
      root + vec3(0, 0, 0), root + -1.0f * axis, col,
      root + shaftRad * rad0, root + -1.0f * axis, col,
      root + shaftRad * rad1, root + -1.0f * axis, col,
      0,
      shader);

    addQuad(
      root + basePos * axis + shaftRad * rad0, root + rad0, col,
      root + basePos * axis + shaftRad * rad1, root + rad1, col,
      root + headPos * axis + shaftRad * rad0, root + rad0, col,
      root + headPos * axis + shaftRad * rad1, root + rad1, col,
      0,
      shader);

    addQuad(
      root + headPos * axis + shaftRad * rad0, root + -1.0f*axis, col,
      root + headPos * axis + shaftRad * rad1, root + -1.0f*axis, col,
      root + headPos * axis + headRad * rad0, root + -1.0f*axis, col,
      root + headPos * axis + headRad * rad1, root + -1.0f*axis, col,
      0,
      shader);

    addQuad(
      root + headPos * axis + headRad * rad0, root + axis + rad0, col,
      root + headPos * axis + headRad * rad1, root + axis + rad1, col,
      root + tipPos * axis + tipRad * rad0, root + axis + rad0, col,
      root + tipPos * axis + tipRad * rad1, root + axis + rad1, col,
      0,
      shader);
  }
}

void Panel3dDrawList::addArrowTriple(mat4 const &model, Panel3dShader *shader)
{
  addArrow(model * vec4(0, 0, 0, 1), model * vec4(1, 0, 0, 0), model * vec4(0, 1, 0, 0), model * vec4(0, 0, 1, 0), vec4(1.0, 0.0, 0.0, 1.0), shader);
  addArrow(model * vec4(0, 0, 0, 1), model * vec4(0, 1, 0, 0), model * vec4(1, 0, 0, 0), model * vec4(0, 0, 1, 0), vec4(0.0, 1.0, 0.0, 1.0), shader);
  addArrow(model * vec4(0, 0, 0, 1), model * vec4(0, 0, 1, 0), model * vec4(1, 0, 0, 0), model * vec4(0, 1, 0, 0), vec4(0.0, 0.0, 1.0, 1.0), shader);
}

void Panel3dDrawList::addVideo(mat4 const &model, U32 texId, Panel3dShader *shader)
{
  if (!shader) shader = &flatSolidShader;
  assert(shader->vertexType == VertPosNormalUvCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);
  U32 i1 = setup.vtxBufPosNormalUvCol.size();

  mat3 modelRot = model;

  setup.vtxBufPosNormalUvCol.emplace_back(model * vec4(-1, 0, +1, 1), modelRot * vec3(0, -1, 0), vec2(0, 0), vec4(1, 1, 1, 1));
  setup.vtxBufPosNormalUvCol.emplace_back(model * vec4(+1, 0, +1, 1), modelRot * vec3(0, -1, 0), vec2(1, 0), vec4(1, 1, 1, 1));
  setup.vtxBufPosNormalUvCol.emplace_back(model * vec4(-1, 0, -1, 1), modelRot * vec3(0, -1, 0), vec2(0, 1), vec4(1, 1, 1, 1));
  setup.vtxBufPosNormalUvCol.emplace_back(model * vec4(+1, 0, -1, 1), modelRot * vec3(0, -1, 0), vec2(1, 1), vec4(1, 1, 1, 1));

  int elemStart = setup.idxBuf.size();
  setup.idxBuf.push_back(i1);
  setup.idxBuf.push_back(i1+1);
  setup.idxBuf.push_back(i1+2);

  setup.idxBuf.push_back(i1+1);
  setup.idxBuf.push_back(i1+3);
  setup.idxBuf.push_back(i1+2);

  addCmd(shader, elemStart, 6, GL_TRIANGLES, texId);
}

void Panel3dDrawList::addSolid(
  StlSolid const &solid,
  mat4 const &m,
  vec4 col,
  Panel3dShader *shader)
{
  if (!shader) shader = &litSolidShader;
  assert(shader->vertexType == VertPosNormalUvCol::vertexType);
  auto &setup = setups.at(shader->shaderIndex);

  mat3 mrot(m);
  U32 elemStart = setup.vtxBufPosNormalUvCol.size();
  U32 elemCount = 0;
  setup.vtxBufPosNormalUvCol.reserve(setup.vtxBufPosNormalUvCol.size() + solid.faces.size()*3);
  setup.idxBuf.reserve(setup.idxBuf.size() + solid.faces.size()*3);
  for (auto &face : solid.faces) {
    S32 i1 = setup.vtxBufPosNormalUvCol.size();

    setup.vtxBufPosNormalUvCol.emplace_back(m * vec4(face.v0, 1.0f), mrot * vec3(face.normal), col);
    setup.vtxBufPosNormalUvCol.emplace_back(m * vec4(face.v1, 1.0f), mrot * vec3(face.normal), col);
    setup.vtxBufPosNormalUvCol.emplace_back(m * vec4(face.v2, 1.0f), mrot * vec3(face.normal), col);

    setup.idxBuf.push_back(i1);
    setup.idxBuf.push_back(i1+1);
    setup.idxBuf.push_back(i1+2);

    elemCount += 3;
  }
  addCmd(&litSolidShader, elemStart, elemCount, GL_TRIANGLES, 0);
}


void Panel3dDrawList::glRender()
{
  if (cmdBuf.empty()) return;

  auto dd = ImGui::GetDrawData();
  assert(dd);

  glEnable(GL_BLEND);
  glBlendEquation(GL_FUNC_ADD);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glDisable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glEnable(GL_SCISSOR_TEST);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  glEnable(GL_PROGRAM_POINT_SIZE);

  /*
    TODO:
      try polygon mode front only
      enable multisample

   */
  
  if (fullScreen) {
    vpL = 0;
    vpR = dd->DisplaySize.x;
    vpT = 0;
    vpB = dd->DisplaySize.y;
  }
  if (1) {
    // viewport starts from lower left, and needs retina-adjusted coordinates

    auto glVpL = vpL * dd->FramebufferScale.x;
    auto glVpR = vpR * dd->FramebufferScale.x;

    auto glVpT = dd->FramebufferScale.y * (dd->DisplayPos.y + dd->DisplaySize.y - vpT);
    auto glVpB = dd->FramebufferScale.y * (dd->DisplayPos.y + dd->DisplaySize.y - vpB);

    if (0) cerr << "imgui l="s + to_string(vpL) + 
      " t=" + to_string(vpT) + 
      " r=" + to_string(vpR) +
      " b=" + to_string(vpB) + "\n";

    if (0) cerr << "glViewport l="s + to_string(glVpL) + 
      " t=" + to_string(glVpT) + 
      " r=" + to_string(glVpR) +
      " b=" + to_string(glVpB) + "\n";
    glViewport(glVpL, glVpB, glVpR - glVpL, glVpT - glVpB);
  }

  glClear(GL_DEPTH_BUFFER_BIT);

  float aspect = float(vpR - vpL) / float(vpB - vpT);
  auto proj = glm::perspective(fov, aspect, nearZ, farZ);
  auto view = glm::lookAt(eyepos, lookat, up);
  if (0) cerr << "fov="s + repr(fov) + " aspect=" + repr(aspect) + " proj=" + to_string(proj) + " view=" + to_string(view) + "\n";


  glDisable(GL_SCISSOR_TEST);

  vector<bool> didBufferElements(allShaders->size());
  vector<bool> didBufferVertex(allShaders->size());
  U32 arrayBufferBound = -2;

  U32 lastShaderIndex = -1;
  U32 lastTexId = -1;
  U32 lastDrawMode = -1;
  for (int cmdi=0; cmdi < cmdBuf.size(); cmdi++) {
    auto &cmd = cmdBuf[cmdi];
    auto &setup = setups.at(cmd.shaderIndex);

    if (cmd.shaderIndex != lastShaderIndex || cmd.texId != lastTexId || cmd.drawMode != lastDrawMode) {
      auto shader = allShaders->at(cmd.shaderIndex);
      setupShader(shader, proj, view, cmd.texId);
      lastShaderIndex = cmd.shaderIndex;
      lastTexId = cmd.texId;
      lastDrawMode = cmd.drawMode;
      if (!didBufferElements[cmd.shaderIndex]) {
        didBufferElements[cmd.shaderIndex] = true;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, setup.elementBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, 
          (GLsizeiptr)setup.idxBuf.size() * sizeof(U32),
          (const GLvoid*)setup.idxBuf.data(),
          GL_STREAM_DRAW);
      }
      if (!didBufferVertex[cmd.shaderIndex]) {
        didBufferVertex[cmd.shaderIndex] = true;
        glBindBuffer(GL_ARRAY_BUFFER, setup.vertexBuffer);
        arrayBufferBound = setup.vertexBuffer;
        if (shader->vertexType == VERT_POS_NORMAL_UV_COL) {
          glBufferData(GL_ARRAY_BUFFER,
            (GLsizeiptr)setup.vtxBufPosNormalUvCol.size() * sizeof(VertPosNormalUvCol),
            (const GLvoid*)setup.vtxBufPosNormalUvCol.data(),
            GL_STREAM_DRAW);
        }
        else if (shader->vertexType == VERT_POS_COL) {
          glBufferData(GL_ARRAY_BUFFER,
            (GLsizeiptr)setup.vtxBufPosCol.size() * sizeof(VertPosCol),
            (const GLvoid*)setup.vtxBufPosCol.data(),
            GL_STREAM_DRAW);
        }
        else {
          throw runtime_error("Unknown vertexType");
        }
      }
      if (arrayBufferBound != setup.vertexBuffer) {
        arrayBufferBound = setup.vertexBuffer;
        glBindBuffer(GL_ARRAY_BUFFER, setup.vertexBuffer);
      }
      LOG_GL_ERRORS();
    }

    if (0) {
      cerr << "draw " + repr(cmd.drawMode) + " " + repr(cmd.elemCount) + 
        " from " + repr(cmd.elemIndex) + "\n";
    }
    glDrawElements(cmd.drawMode,
      (GLsizei)cmd.elemCount,
      GL_UNSIGNED_INT,
      (void*)(cmd.elemIndex * sizeof(U32)));

    LOG_GL_ERRORS();
  }
}

void Panel3dDrawList::setupShader(Panel3dShader *shader, glm::mat4 &proj, glm::mat4 &view, U32 texUnit)
{
  const int verbose = 0;
  glUseProgram(shader->progHandle);

  auto &setup = setups.at(shader->shaderIndex);
  if (setup.vertexArrayObject == 0) { // first time

    glGenVertexArrays(1, &setup.vertexArrayObject);
    glBindVertexArray(setup.vertexArrayObject);

    glGenBuffers(1, &setup.elementBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, setup.elementBuffer);

    glGenBuffers(1, &setup.vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, setup.vertexBuffer);

    if (verbose) cerr << shader->fn + ": progHandle=" + repr(shader->progHandle) + 
        " setup vertexArrayObject="s + to_string(setup.vertexArrayObject) + 
        " elementBuffer=" + to_string(setup.elementBuffer) + 
        " vertexBuffer=" + to_string(setup.vertexBuffer) + "\n";

    for (auto &[name, index] : shader->attribs) {
      glEnableVertexAttribArray(index);
      if (shader->vertexType == VERT_POS_NORMAL_UV_COL) {
       if (verbose) {
         cerr << "  vertexAttrib<VertPosNormalUvCol> size="s + repr(sizeof(VertPosNormalUvCol)) +
              " " + name + "=" + repr(index) + "\n";
       }
       if (name == "Position") {
          glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 
            sizeof(VertPosNormalUvCol), (GLvoid*)offsetof(VertPosNormalUvCol, pos));
        }
        else if (name == "Normal") {
          glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE,
            sizeof(VertPosNormalUvCol), (GLvoid*)offsetof(VertPosNormalUvCol, normal));
        }
        else if (name == "TexCoord") {
          glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE,
            sizeof(VertPosNormalUvCol), (GLvoid*)offsetof(VertPosNormalUvCol, uv));
        }
        else if (name == "Color") {
          glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE,
            sizeof(VertPosNormalUvCol), (GLvoid*)offsetof(VertPosNormalUvCol, col));
        }
        else {
          throw runtime_error("Unknown shader attribute for VertPosNormalUvCol shader: "s + shellEscape(name));
        }
      }
      else if (shader->vertexType == VERT_POS_COL) {
        if (verbose) {
         cerr << "  vertexAttrib<VertPosCol> size="s + repr(sizeof(VertPosCol)) +
              " " + name + "=" + repr(index) + "\n";
        }
        if (name == "Position") {
          glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE,
            sizeof(VertPosCol), (GLvoid*)offsetof(VertPosCol, pos));
        }
        else if (name == "Color") {
          glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE,
            sizeof(VertPosCol), (GLvoid*)offsetof(VertPosCol, col));
        }
        else {
          throw runtime_error("Unknown shader attribute for VertPosCol shader: "s + shellEscape(name));
        }
      }
      else {
        throw runtime_error("Unknown vertexType");
      }
    }
  }
  else {
    glBindVertexArray(setup.vertexArrayObject);
  }

  LOG_GL_ERRORS();

  glBindSampler(0, 0);

  for (auto &[name, index] : shader->uniforms) {
    if (name == "ProjMtx") {
      glUniformMatrix4fv(index, 1, GL_FALSE, glm::value_ptr(proj));
    }
    else if (name == "ViewMtx") {
      glUniformMatrix4fv(index, 1, GL_FALSE, glm::value_ptr(view));
    }
    else if (name == "TexUnit") {
      glUniform1i(index, 0);
      glActiveTexture(GL_TEXTURE0 + 0);
      glBindTexture(GL_TEXTURE_2D, texUnit);
    }
  }
  LOG_GL_ERRORS();
}

void Panel3dDrawList::camFov(R target)
{
  if (isnan(target)) {
    throw runtime_error("Panel3dDrawList::camFov got nan: " + to_string(fov));
    return;
  }
  float dir = target - fov;
  float dirnorm = abs(dir);
  if (dirnorm < 0.00001) return;
  fov += dir * min(1.0f, dt * max(6.0f, 0.05f/dirnorm));
  uvUiActive = true;
}

void Panel3dDrawList::camEyepos(vec3 target)
{
  if (isnan(target.x) || isnan(target.y) || isnan(target.z)) {
    throw runtime_error("Panel3dDrawList::camEyepos got nan: " + to_string(target));
    return;
  }
  auto dir = target - eyepos;
  auto dirnorm = glm::l2Norm(dir);
  if (dirnorm < 0.001) return;
  eyepos += dir * min(1.0f, dt * max(10.0f, 0.1f/dirnorm));
  uvUiActive = true;
}

void Panel3dDrawList::camLookat(vec3 target)
{
  if (isnan(target.x) || isnan(target.y) || isnan(target.z)) {
    throw runtime_error("Panel3dDrawList::camLookat got nan: " + to_string(target));
    return;
  }
  auto dir = target - lookat;
  auto dirnorm = glm::l2Norm(dir);
  if (dirnorm < 0.001) return;
  lookat += dir * min(1.0f, dt * max(10.0f, 0.1f/dirnorm));
  uvUiActive = true;
}

void Panel3dDrawList::camStrafe(vec3 dir)
{
  if (isnan(dir.x) || isnan(dir.y) || isnan(dir.z)) {
    throw runtime_error("Panel3dDrawList::camStrafe got nan: " + to_string(dir));
    return;
  }
  if (glm::l2Norm(dir) < 0.0001) return;
  eyepos += dir * dt;
  lookat += dir * dt;
  uvUiActive = true;
}

void Panel3dDrawList::camHoverFly(vec3 dir)
{
  if (isnan(dir.x) || isnan(dir.y) || isnan(dir.z)) {
    throw runtime_error("Panel3dDrawList::camHoverFly got nan: " + to_string(dir));
    return;
  }
  if (glm::l2Norm(dir) < 0.0001) return;
  eyepos += dir * dt;
  lookat.x += dir.x * dt;
  lookat.y += dir.y * dt;
  lookat.z = 0.0f;
  uvUiActive = true;
}


void Panel3dDrawList::camFlyHome()
{
  auto dir = homeLookat - lookat;
  auto dirnorm = glm::l2Norm(dir);
  if (dirnorm < 0.001) return;

  vec3 oldEyeToLook = lookat - eyepos;
  lookat += dir * min(1.0f, dt * max(5.0f, 0.1f/dirnorm));

  vec3 newEyeToLook = lookat - eyepos;

  auto oldEyeToLookXY = hypot(oldEyeToLook.x, oldEyeToLook.y);
  auto newEyeToLookXY = hypot(newEyeToLook.x, newEyeToLook.y);

  auto moveXY = oldEyeToLookXY - newEyeToLookXY;

  vec3 newEyepos(
    eyepos.x - moveXY/oldEyeToLookXY * oldEyeToLook.x,
    eyepos.y - moveXY/oldEyeToLookXY * oldEyeToLook.y,
    eyepos.z);

  eyepos = newEyepos;

  uvUiActive = true;
}


void Panel3dDrawList::camPan(vec2 dir)
{
  if (isnan(dir.x) || isnan(dir.y)) {
    throw runtime_error("Panel3dDrawList::camPan got nan: " + to_string(dir));
    return;
  }
  if (abs(dir.x) + abs(dir.y) < 0.0001) return;
  lookat = eyepos + 
    glm::rotateZ(glm::normalize(lookat - eyepos), +0.05f*dir.x) +
    glm::rotateX(glm::normalize(lookat - eyepos), -0.05f*dir.y);
  uvUiActive = true;
}


void Panel3dDrawList::camOrbitElevate(R orbit, R elevate)
{
  if (isnan(orbit) || isnan(elevate)) {
    throw runtime_error("Panel3dDrawList::camOrbitElevate got nan: " + to_string(orbit) + ", " + to_string(elevate));
    return;
  }
  auto lookToEye = eyepos - lookat;
  eyepos = lookat + glm::rotateZ(lookToEye, float(-0.05*orbit));

  lookToEye = eyepos - lookat;
  auto side = glm::cross(lookToEye, up);
  auto updog = glm::dot(glm::normalize(lookToEye), up);
  if (updog > 0.95 && elevate > 0) return;
  if (updog < -0.95 && elevate < 0) return;
  auto newLookToEye = glm::rotate(lookToEye, float(0.05*elevate), side);

  eyepos = newLookToEye + lookat;
  uvUiActive = true;
}


void Panel3dDrawList::setPickRay()
{
  const int verbose = 0;
  auto &io = ImGui::GetIO();
  if (io.MousePos.x >= vpL && io.MousePos.x <= vpR && io.MousePos.y >= vpT && io.MousePos.y <= vpB &&
      vpR > vpL && vpB > vpT) {
    R pickX = (io.MousePos.x - vpL) / (vpR - vpL) * 2.0 - 1.0;
    R pickY = (io.MousePos.y - vpB) / (vpT - vpB) * 2.0 - 1.0;
    if (verbose) cerr << "\npick = " + repr(pickX) + "," + repr(pickY) + "\n";

    /*
      We're undoing this projection (from lit_solid.glsl):
        gl_Position = ProjMtx * ViewMtx * vec4(Position, 1);
      Since we only have a 2D screen coordinate, we unproject twice: once at
      z=0 and once at z=1. This gives us a line that intersects anything on the
      screen at that point.
    */

    float aspect = float(vpR - vpL) / float(vpB - vpT);
    auto proj = glm::perspective(fov, aspect, nearZ, farZ);
    auto iproj = glm::inverse(proj);
    auto view = glm::lookAt(eyepos, lookat, up);
    auto iview = glm::inverse(view);

    vec4 nearFrustPos = vec4(pickX, pickY, 0.0, 1.0);
    vec4 farFrustPos = vec4(pickX, pickY, 1.0, 1.0);
    vec4 nearEyeRay = iproj * nearFrustPos;
    nearEyeRay *= 1.0/nearEyeRay.w;
    vec4 farEyeRay = iproj * farFrustPos;
    farEyeRay *= 1.0/farEyeRay.w;

    vec3 nearWorldPos = iview * nearEyeRay;
    vec3 farWorldPos = iview * farEyeRay;

    pickRayDir = glm::normalize(farWorldPos - nearWorldPos);
    pickRayOrigin = nearWorldPos;
    pickRayActive = true;

    if (verbose) {
      cerr << " proj  = " + to_string(proj) + "\n";
      cerr << " iproj = " + to_string(iproj) + "\n";
      cerr << " view  = " + to_string(view) + "\n";
      cerr << " iview = " + to_string(iview) + "\n";
      cerr << " nearFrustPos = " + to_string(nearFrustPos) + "\n";
      cerr << " farFrustPos = " + to_string(farFrustPos) + "\n";
      cerr << " nearEyeRay = " + to_string(nearEyeRay) + "\n";
      cerr << " farEyeRay = " + to_string(farEyeRay) + "\n";
      cerr << " nearWorldPos = " + to_string(nearWorldPos) + "\n";
      cerr << " farWorldPos = " + to_string(farWorldPos) + "\n";
      cerr << " pickRayOrigin=" + to_string(pickRayOrigin) + " pickRayDir = " + to_string(pickRayDir) + "\n";
    }
  }
  else {
    if (0) cerr << "Mouse " + repr(io.MousePos.x) + "," + repr(io.MousePos.y) + " outside " + repr(vpL) + "-" + repr(vpR) + " , " + repr(vpT) + "-" + repr(vpB) + "\n";
    pickRayActive = false;
  }
}

bool Panel3dDrawList::hitTestTriangle(vec3 a, vec3 b, vec3 c, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  vec2 bary;
  if (glm::intersectRayTriangle(pickRayOrigin, pickRayDir, a, b, c, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }

  return false;
}


bool Panel3dDrawList::hitTestQuad(vec3 a, vec3 b, vec3 c, vec3 d, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  vec2 bary;
  if (glm::intersectRayTriangle(pickRayOrigin, pickRayDir, a, b, c, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }
  if (glm::intersectRayTriangle(pickRayOrigin, pickRayDir, b, c, d, bary, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }

  return false;
}


bool Panel3dDrawList::hitTestPlane(vec3 planeOrig, vec3 planeNormal, vec3 &hit, float &distance)
{
  if (!pickRayActive) return false;

  if (glm::intersectRayPlane(pickRayOrigin, pickRayDir, planeOrig, planeNormal, distance)) {
    hit = pickRayOrigin + distance * pickRayDir;
    return true;
  }

  return false;
}

void Panel3dDrawList::firstPersonControls()
{
  bool shift = ImGui::IsKeyDown(SDL_SCANCODE_LSHIFT) || ImGui::IsKeyDown(SDL_SCANCODE_RSHIFT);
  bool cmd = ImGui::IsKeyDown(SDL_SCANCODE_APPLICATION) || ImGui::IsKeyDown(SDL_SCANCODE_LGUI) || ImGui::IsKeyDown(SDL_SCANCODE_RGUI);
  bool alt = ImGui::IsKeyDown(SDL_SCANCODE_LALT) || ImGui::IsKeyDown(SDL_SCANCODE_RALT);

  if (cmd || alt) return;

  float speed = shift ? 0.01 : 1.0;

  vec3 strafe(0, 0, 0);
  if (ImGui::IsKeyDown(SDL_SCANCODE_W) || ImGui::IsKeyDown(SDL_SCANCODE_UP)) {
    strafe += speed * glm::normalize(lookat - eyepos);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_A) || ImGui::IsKeyDown(SDL_SCANCODE_LEFT)) {
    strafe += speed * glm::rotateZ(glm::normalize(lookat - eyepos), (float)M_PI_2);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_S) || ImGui::IsKeyDown(SDL_SCANCODE_DOWN)) {
    strafe -= speed * glm::normalize(lookat - eyepos);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_D) || ImGui::IsKeyDown(SDL_SCANCODE_RIGHT)) {
    strafe -= speed * glm::rotateZ(glm::normalize(lookat - eyepos), (float)M_PI_2);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_E)) {
    strafe += speed * vec3(0, 0, 1);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_C)) {
    strafe -= speed * vec3(0, 0, 1);
  }
  camStrafe(strafe);

  ImGuiIO& io = ImGui::GetIO();
  if (io.MouseWheelH != 0.0 || io.MouseWheel != 0.0) {
    camPan(vec2(io.MouseWheelH, io.MouseWheel));
  }

  camFov(ImGui::IsKeyDown(SDL_SCANCODE_Q) ? glm::radians(5.0) : glm::radians(40.0));

  if (ImGui::IsKeyDown(SDL_SCANCODE_X)) {
    camEyepos(vec3(shift ? -1 : 1, 0, 0));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_Y)) {
    camEyepos(vec3(0, shift ? -1 : 1, 0));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_Z)) {
    camEyepos(vec3(0, 0, shift ? -1 : 1));
    camLookat(vec3(0, 0, 0));
  }
  if (ImGui::IsKeyPressed(SDL_SCANCODE_H)) {
    camHome();
  }
  camCheckSanity();
}



void Panel3dDrawList::hoverControls()
{
  bool shift = ImGui::IsKeyDown(SDL_SCANCODE_LSHIFT) || ImGui::IsKeyDown(SDL_SCANCODE_RSHIFT);
  bool cmd = ImGui::IsKeyDown(SDL_SCANCODE_APPLICATION) || ImGui::IsKeyDown(SDL_SCANCODE_LGUI) || ImGui::IsKeyDown(SDL_SCANCODE_RGUI);
  bool alt = ImGui::IsKeyDown(SDL_SCANCODE_LALT) || ImGui::IsKeyDown(SDL_SCANCODE_RALT);

  if (cmd || alt) return;

  float speed = shift ? 0.01 : 1.0;
  speed *= max(1.0f, eyepos.z);

  auto fwdDir = glm::normalize(lookat - eyepos);
  fwdDir.z = 0.0;
  auto rightDir = glm::rotateZ(fwdDir, float(M_PI_2));

  vec3 strafe(0, 0, 0);
  if (ImGui::IsKeyDown(SDL_SCANCODE_W) || ImGui::IsKeyDown(SDL_SCANCODE_UP)) {
    strafe += speed * fwdDir;
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_A) || ImGui::IsKeyDown(SDL_SCANCODE_LEFT)) {
    strafe += speed * rightDir;
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_S) || ImGui::IsKeyDown(SDL_SCANCODE_DOWN)) {
    strafe += -speed * fwdDir;
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_D) || ImGui::IsKeyDown(SDL_SCANCODE_RIGHT)) {
    strafe += -speed * rightDir;
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_E)) {
    strafe += speed * vec3(0, 0, 0.75);
  }
  if (ImGui::IsKeyDown(SDL_SCANCODE_C)) {
    strafe += speed * vec3(0, 0, -0.75);
  }

  camHoverFly(strafe);

  ImGuiIO& io = ImGui::GetIO();

  if (ImGui::IsKeyDown(SDL_SCANCODE_Q)) {
    if (!saveDirActive) {
      saveDirActive = true;
      saveDir = lookat - eyepos;
    }
    camFov(glm::radians(5.0));
    if (io.MouseWheelH != 0.0 || io.MouseWheel != 0.0) {
      camPan(vec2(0.1*io.MouseWheelH, 0.1*io.MouseWheel));
    }
  }
  else {
    camFov(glm::radians(40.0));
    if (io.MouseWheelH != 0.0 || io.MouseWheel != 0.0) {
      camOrbitElevate(io.MouseWheelH, io.MouseWheel);
    }
    if (saveDirActive) {
      auto revertLookat = eyepos + saveDir;
      if (glm::l2Norm(revertLookat - lookat) < 0.001) {
        saveDirActive = false;
      }
      else {
        camLookat(revertLookat);
      }
    }
  }

  if (ImGui::IsKeyDown(SDL_SCANCODE_H)) {
    camFlyHome();
  }
  camCheckSanity();
}
