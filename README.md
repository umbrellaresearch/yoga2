# Yoga Studio 2

More content at [https://yoga.umbrellaresearch.com].

Yoga is a functional programming language for real-time robot control, and Yoga Studio is a development environment that lets you test and debug code live on robots.

You can modify parameters in real time and watch the robot's behavior change. If you've hacked on robots, you know how time-consuming it is to adjust all the parameters. Yoga makes the whole process 10x faster.

Did I mention that **you** can run this **live** on **real robots**? If you don't have your own compatible robot, the client will connect to a robot in our warehouse, a two-wheeled self-balancing setup. What makes it self-balancing and not self-faceplanting is the code in the [examples/greppy](./examples/greppy) directory.

<img src="images/ScreenshotAnno@2x.png" width="892" height="585">


In Yoga, any numeric value in the source code followed by a ~ and a range is an editable parameter. There are 4 parameters below:

| Code Editor | Yoga Studio |
| :-: | :-: |
| ![Left](images/paramdemo_vs.png "Parameters in VSCode") | ![Right](images/paramdemo_yoga.gif "Parameters in Yoga Studio") |

After a live run, you can scrub through the captured traces and video. If you drag parameters while running live, it sends the updated values to the robot. If you drag parameters while analyzing a previously captured trace, it will overlay a graph of what the new code would do (given the same inputs) over what the old code did.


## Build and test

Works on either Mac or Linux (Tested on Ubuntu 19.10 and 20.04).
```sh
git clone --recurse-submodules https://gitlab.com/umbrellaresearch/yoga2.git
cd yoga2
git config submodule.recurse true
make install.deps # On Mac, uses Brew. On Linux, apt-get.
make -j8
bin/yogastudio examples/greppy/main.yoga # Compiles greppy example, starts UI. Then click "Live"
```


## Examples included

T[examples/greppy](./examples/greppy): A two-wheeled balancing robot about 4' tall. Test hardware available for free on a time-shared basis! Try it with:
```sh
bin/yogastudio examples/greppy/main.yoga
```

[examples/acrobot](./examples/acrobot): A simulated 2-DOF underactuated acrobat. Try it with:
```sh
bin/yogastudio examples/acrobot/acrobot_fullauto.yoga sim
```

[examples/dex4](./examples/dex4): A full-size 12-DOF humanoid walking robot. Hardware not currently available for public use.

## Yoga Language

Yoga is a purely functional language for real-time control, and supports backpropagation to automatically modify
its parameters. See more at https://yoga.umbrellaresearch.com/yoga.md
