# Deploy Directory

This directory is for things specific to some deployment.
Currently, the only deployment is `charter`, for Umbrella Research's Charter St office.

## Charter deployment

Machines include:
* `orly`: onboard greppy1
* `nimes`: onboard harness
* `nice`: onboard dex42
* `rome`: a 44-core build server

