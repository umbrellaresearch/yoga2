#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"


TEST_CASE("update params work", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out Foobar o, update Foobar s, in Foobar i) {
      o.foo = nextFoo = s.foo = s.foo + dt * i.foo;
      if (nextFoo > 1.0) {
        o.bar = 1;
      }
      default {
        o.bar = 0;
      }
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto ft = ctx.getTypeOrThrow("Foobar");
  auto o = ctx.mkValue(ft);
  auto sPrev = ctx.mkValue(ft);
  auto sNext = ctx.mkValue(ft);
  auto i = ctx.mkValue(ft);

  sPrev.wr("foo", 1.5);
  sPrev.wr("bar", 0.5);
  
  i.wr("foo", 2.5);
  i.wr("bar", 3.5);

  sNext.wr("foo", 9.9);
  sNext.wr("bar", 9.9);

  R astonishment = 0.0;
  tp({o, sNext}, {sPrev, i}, astonishment, 0.01);

  if (0) cerr << "o=" + repr(o) + " sPrev=" + repr(sPrev) + " sNext=" + repr(sNext) + " i=" + repr(i) + "\n";

  CHECK(sNext.rd<R>("foo") == 1.525);
  CHECK(sNext.rd<R>("bar") == 0.5);
};




TEST_CASE("update params work with heavy nesting", "[compiler]") {

  auto reg = YOGAC(R"(
    struct Minor {
      Foobar fb1, fb2;
    };
    struct Major {
      Minor m1, m2;
    };

    function tp(update Major s) {
      s.m1 = s.m1;
      //s.m2.fb1 = s.m2.fb1;
      s.m2.fb2.foo = s.m2.fb2.foo + dt;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  auto ft = ctx.getTypeOrThrow("Major");
  auto sPrev = ctx.mkValue(ft);
  auto sNext = ctx.mkValue(ft);

  sPrev.wr("m1.fb1.foo", 0.5);
  sPrev.wr("m1.fb1.foo", 0.5);
  sPrev.wr("m1.fb1.bar", 1.5);
  sPrev.wr("m1.fb2.foo", 2.5);
  sPrev.wr("m1.fb2.bar", 3.5);
  sPrev.wr("m2.fb1.foo", 4.5);
  sPrev.wr("m2.fb1.bar", 5.5);
  sPrev.wr("m2.fb2.foo", 6.5);
  sPrev.wr("m2.fb2.bar", 7.5);

  R astonishment = 0.0;
  tp({sNext}, {sPrev}, astonishment, 0.01);

  if (0) cerr << "sPrev=" + repr(sPrev) + " sNext=" + repr(sNext) + "\n";

  CHECK(sNext.rd<R>("m1.fb1.foo") == 0.5);
  CHECK(sNext.rd<R>("m1.fb1.bar") == 1.5);
  CHECK(sNext.rd<R>("m1.fb2.foo") == 2.5);
  CHECK(sNext.rd<R>("m1.fb2.bar") == 3.5);
  CHECK(sNext.rd<R>("m2.fb1.foo") == 4.5);
  CHECK(sNext.rd<R>("m2.fb1.bar") == 5.5);
  CHECK(sNext.rd<R>("m2.fb2.foo") == 6.51);
  CHECK(sNext.rd<R>("m2.fb2.bar") == 7.5);
};

