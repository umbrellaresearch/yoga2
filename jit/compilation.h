#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./ast.h"
#include "./context.h"
#include "./param.h"
#include "./llvm_fwd.h"
#include "../db/yoga_layout.h"


enum YogaParserMode {
  ParseToplevel,
  ParseCell,
};


struct TopLevelSymbolValue {
  AstNode *ast{nullptr};
  AstFunction *astFunction{nullptr};
  YogaType *type{nullptr};

  FunctionEffects *funcEffects{nullptr};
  FunctionEffects *funcGradEffects{nullptr};

  YogaCompiledFuncInfo *compiledFunc{nullptr};

  bool isType{false}; // including a forward declaration
  bool isReserved{false};
};

struct CellValue {
  AstExpr *rhs{nullptr};
  R px{0.0}, py{0.0};
};

struct YogaCompilation : enable_shared_from_this<YogaCompilation> {

  YogaCompilation();
  ~YogaCompilation();

  YogaCompilation(YogaCompilation const &other) = delete;
  YogaCompilation(YogaCompilation &&other) = delete;
  YogaCompilation & operator = (YogaCompilation const &other) = delete;
  YogaCompilation & operator = (YogaCompilation &&other) = delete;

  template<typename T>
  bool getRuntimeParam(string const &key, T &value)
  {
    if (!runtime) return true;
    return runtime->getValueForKey(key, value);
  }

  int verbose{0};
  bool doCheckpointDiagLog{true};
  bool emitDebugVersions{true};
  bool emitGradVersions{true};
  YogaLayout &layout;

  shared_ptr<YogaSourceSet> sources;
  map<string, bool> multipleIncludeGuard;

  deque<unique_ptr<AstNode>> astPool;
  deque<unique_ptr<ExprNode>> exprPool;
  deque<unique_ptr<YogaType>> typePool;
  deque<unique_ptr<YogaParamInfo>> paramPool;
  deque<unique_ptr<FunctionEffects>> effectsPool;
  deque<unique_ptr<YogaSourceFile>> sourcesPool;
  deque<unique_ptr<YogaCompiledFuncInfo>> compiledFuncPool;
  deque<unique_ptr<AnnoBase>> annotationPool;
  deque<unique_ptr<StructMember>> structMemberPool;
  deque<YogaSourceLoc> sourceLocPool;
  shared_ptr<EmitCtx> emitCtx;

  YogaCompiledFuncInfo * mkCompiledFunc();

  YogaSourceLoc *internSourceLoc(YogaSourceLoc const &sourceLoc);

  YogaContext mkCtx(char const *desc);

  apr_pool_t *mem{nullptr};

  unordered_map<string, TopLevelSymbolValue> symbolTable;
  deque<pair<string, AstNode *>> topLevelsInOrder;

  unordered_map<string, CellValue> cellTable;

  void applyParamsGrad(vector<R> &paramsGrad, R lr);

  size_t paramCount{0};
  vector<YogaParamInfo *> paramsByIndex;
  unordered_map<string, YogaParamInfo *> paramsByName;
  vector<double> paramValues;
  vector<double> savedParamValues;

  unordered_map<string, YogaType *> timeseqTypes;

  map<string, bool> didParamBackupByFn;
  int codeEpoch{0};
  int paramValueEpoch{0};
  int paramSavedEpoch{0};

  /*
    Grouped by source file, all the annotations.
    They're added in whatever order by compilation passes, and sorted by sortAnnos()
    as part of run(), so they're in order for use by visualizers.
   */
  unordered_map<YogaSourceFile *, vector<AnnoBase *>> annosByFile;

  AstObject *runtime{nullptr};
  vector<AstObject *> polyfitDefs;
  vector<AstAnnoCall *> engineDefs;
  vector<AstAnnoCall *> initDefs;
  vector<AstAnnoCall *> panelDefs;
  vector<AstAnnoCall *> predictDefs;
  vector<AstAnnoCall *> trainDefs;

  struct YogaCommonRuntimeParams {
    string liveHost;
    string tracePrefix;
    string robotName;
    R duration{0.0};
  };
  YogaCommonRuntimeParams runtimeParams;

  YogaCompiledFuncInfo *getCompiledFunc(string const &clsName);
  AstAnnoCall *findEngineFunction(string const &fn);

  [[nodiscard]] bool compileMain(string const &fn);
  [[nodiscard]] bool compileMain();
  [[nodiscard]] bool run();
  [[nodiscard]] bool parseBuiltin();
  [[nodiscard]] bool parseForeignFunctions();
  [[nodiscard]] bool parseFile(string const &fn, bool isBuiltin, YogaParserMode mode);
  [[nodiscard]] bool parseText(string const &fn, string const &text, YogaParserMode mode);
  [[nodiscard]] bool getSequenceTypes();
  shared_ptr<Trace> startTrace();

  [[nodiscard]] static bool addGradients(YogaContext const &ctx);

  [[nodiscard]] static bool runPasses(YogaContext &ctx);
  [[nodiscard]] static bool initLlvm(YogaContext const &ctx);
  [[nodiscard]] static bool generateLlTypes(YogaContext const &ctx);
  [[nodiscard]] static bool generateLlModule(YogaContext const &ctx, 
    std::function<bool(YogaContext const &ctx, EmitCtx &e)> addCode);
  [[nodiscard]] static bool generateLlCodeForYogaFunctions(YogaContext const &ctx);
  [[nodiscard]] static bool generateLlCodeForYogaFunction(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects);
  [[nodiscard]] static bool generateLlCodeForYogaGrad(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects);
  [[nodiscard]] static bool generateLlCodeForYogaDebug(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects);
  [[nodiscard]] static bool generateLlCodeForYogaInternals(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects);
  [[nodiscard]] static bool generateLlAccessorsFor(YogaContext const &ctx, EmitCtx &e, YogaType *t);
  [[nodiscard]] static bool generateLlPacketWrFor(YogaContext const &ctx, EmitCtx &e, YogaType *t);
  [[nodiscard]] static bool generateLlPacketRdFor(YogaContext const &ctx, EmitCtx &e, YogaType *t);
  [[nodiscard]] static bool generateLlLinearOpsFor(YogaContext const &ctx, EmitCtx &e, YogaType *t);
  [[nodiscard]] static bool generateLlNormSqFor(YogaContext const &ctx, EmitCtx &e, YogaType *t);

  [[nodiscard]] static bool emitEffects(YogaContext const &ctx, EmitCtx &e, FunctionEffects *effects);

  [[nodiscard]] static bool compileTypes(YogaContext const &ctx);
  [[nodiscard]] static bool defineBuiltinTypes(YogaContext const &ctx);
  [[nodiscard]] static bool defineForeignFunctions(YogaContext const &ctx);
  [[nodiscard]] static bool defineReserved(YogaContext const &ctx);
  [[nodiscard]] static bool copyRuntimeParams(YogaContext const &ctx);

  [[nodiscard]] static bool sortAnnos(YogaContext const &ctx);

  CallableYogaAccessor getYogaAccessor(string const &clsName, string const &subName, string const &opName);
  CallableYogaPacketWr getYogaPacketWr(string const &clsName);
  CallableYogaPacketRd getYogaPacketRd(string const &clsName);


  static YogaType * getType(YogaContext const &ctx, string clsName);
  [[nodiscard]] static bool aliasType(YogaContext const &ctx, string newName, YogaType *t);


  static YogaTypePrimitive * getPrimitive(YogaContext const &ctx, string clsName);
  static YogaTypeStruct *getStruct(YogaContext const &ctx, string clsName, bool isOnehot);
  static YogaTypeMatrix *getMatrix(YogaContext const &ctx, YogaType *elementType, int rows, int cols);
  static YogaType *createNamedType(YogaContext const &ctx, string clsName);
  [[nodiscard]] static bool parseDims(YogaContext const &ctx, vector<int> &dims, string const &s);
  [[nodiscard]] static bool freezeAllTypes(YogaContext const &ctx);

  map<string, string> rewriteWithChanges();
  void rewriteFiles(map<string, string> const &nameToContents);
  bool paramsNeedSaving();
  [[nodiscard]] bool saveParamEdits();
  [[nodiscard]] bool revertParamEdits();

  void dumpToFiles(string const &basePath, string const &header);
  void dumpParses(ostream &s);
  void dumpTypes(ostream &s);
  void dumpEffects(ostream &s);
  void dumpParams(ostream &s);
  void summarizeProgram(ostream &s);
  string summarizeProgram();

  void checkpointDiagLog();

  // Commonly used types. For others, call getType(ctx, clsName);
  YogaType *voidType{nullptr};
  YogaType *boolType{nullptr};
  YogaType *rType{nullptr};
  YogaType *packetType{nullptr};
  YogaType *stringType{nullptr};
  YogaType *objectType{nullptr};
  YogaType *floatType{nullptr};
  YogaType *complexType{nullptr};
  YogaType *s32Type{nullptr};
  YogaType *u32Type{nullptr};
  YogaType *s64Type{nullptr};
  YogaType *u64Type{nullptr};
  YogaType *s16Type{nullptr};
  YogaType *u16Type{nullptr};
  YogaType *s8Type{nullptr};
  YogaType *u8Type{nullptr};
  YogaType *setType{nullptr};
  YogaType *memoryResourceType{nullptr};

  ostringstream callLog;
  ostringstream genLog;
  ostringstream diagLog; // errors and warnings intermingled
  ostringstream backpropLog;
  size_t diagLogCheckpointPos{0};
  int errCount{0};
  int warnCount{0};

  clock_t compileStartTime{0};
  clock_t compileEndTime{0};

  int serializeCounter{0};
  bool doneBuiltins{false};

  // Global behavior change
  U8 initializeWithByte{0x00};


};


struct YogaCodeRegister {
  YogaCodeRegister(string _fn, string _contents);

  string fn;
  string contents;

  static vector<YogaCodeRegister *> *registry;
};


struct YogaForeignRegister {
  YogaForeignRegister(string _fn, string _decl, CallableYogaFunction _f, CallableYogaDebug _fDebug=nullptr);
  string fn;
  string decl;
  CallableYogaFunction f;
  CallableYogaDebug fDebug;

  static vector<YogaForeignRegister *> *registry;
};
