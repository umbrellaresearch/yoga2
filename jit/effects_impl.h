#pragma once
#include "./effects.h"
#include "./compilation.h"


template<typename T, typename ...Args>
T * YogaContext::mkEffects(Args&& ...args) const
{
  unique_ptr ptr = make_unique<T>(std::forward<Args>(args)...);
  T *ret = ptr.get();
  reg->effectsPool.emplace_back(std::move(ptr));

  ret->sourceLoc = lastLoc;
  return ret;
}
