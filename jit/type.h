#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./sourcefile.h"
#include "./ast.h"
#include "./param.h"
#include "./llvm_fwd.h"
#include "./memory.h"
#include <any>

enum YogaDirection {
  YD_IN,
  YD_OUT,
  YD_ACCUM,
  YD_UPDATE,
};

inline ostream & operator <<(ostream &s, YogaDirection d)
{
  switch (d) {
    case YD_IN:  s << "in"; break;
    case YD_OUT: s << "out"; break;
    case YD_ACCUM: s << "accum"; break;
    case YD_UPDATE: s << "update"; break;
    default: throw runtime_error("Unknown YogaDirection"s);
  }
  return s;
}

struct YogaType  {
  YogaType(
    string const &_clsName)
    : clsName(_clsName)
  {
  }
  virtual ~YogaType()
  {
  }

  YogaSourceLoc sourceLoc;
  string clsName;
  bool frozen{false};
  bool isDebugLocals{false};
  int codeEpoch{0};

  llvm::Type *llType{nullptr};
  size_t llAllocSize{0};
  U8 *zeroValue{nullptr};

  CallableYogaPacketWr packetWr{nullptr};
  CallableYogaPacketRd packetRd{nullptr};


  unordered_map<string, CallableYogaAccessor> accessorsByName;
  unordered_map<string, YogaType *> ptrTypesByName;

  CallableYogaLinearOp linearOp{nullptr};
  CallableYogaNormOp normSqOp{nullptr};

  virtual YogaType *getNonPtrType() const {
    return nullptr;
  }

  virtual bool isStruct() const { return false; }
  virtual bool isObject() const { return false; }
  virtual bool isPrimitive() const { return false; }
  virtual bool isPtr() const { return false; }
  virtual bool isMatrix() const { return false; }
  virtual bool isSet() const { return false; }
  virtual bool isIntegral() const { return false; }
  virtual bool isSigned() const { return false; }
  virtual bool isReal() const { return false; }
  virtual bool isComplex() const { return false; }
  virtual bool isScalar() const { return false; }
  virtual bool isBool() const { return false; }
  virtual bool isVoid() const { return false; }
  virtual bool isString() const { return false; }
  virtual bool isPacket() const { return false; }
  virtual bool isEngine() const { return false; }
  virtual bool hasLinearOps() const { return false; }

  virtual YogaTypePrimitive *asPrimitive() { return nullptr; }
  virtual YogaTypeStruct *asStruct() { return nullptr; }
  virtual YogaTypePtr *asPtr() { return nullptr; }
  virtual YogaTypeSet *asSet() { return nullptr; }
  virtual YogaTypeMatrix *asMatrix() { return nullptr; }

  /*
    Attempt to freeze the layout of this data structures. Returns true if successful.
    Non-pointer types need to freeze the layout of member types first.
  */
  virtual bool tryFreeze(int depth=0) { return false; }

  virtual bool hasArrayNature() const { return false; }
  virtual YogaType *getElementType() const { return nullptr; }

  CallableYogaPacketRd getPacketRdForLegacy(YogaType *oldType);

  virtual bool isEquivalentType(YogaType *other) { return other->clsName == clsName; }

  U8 *mkInstance(apr_pool_t *mem=nullptr) {
    auto ret = yogic_alloc(llAllocSize, mem);
    memset(ret, 0, llAllocSize);
    return ret;
  }

  U8 *getZero(apr_pool_t *mem=nullptr) {
    if (!zeroValue) {
      zeroValue = yogic_alloc(llAllocSize, mem);
      memset(zeroValue, 0, llAllocSize);
    }
    return zeroValue;
  }

  template<typename T>
  U8 *mkInstance(T const &it, apr_pool_t *mem=nullptr) {
    if (sizeof(T) != llAllocSize) {
      throw runtime_error(
        "Conversion from cpp("s + typeid(T).name() + ") size=" + to_string(sizeof(T)) +
        " to yoga("s + clsName + ") size=" + to_string(llAllocSize) + ": size mismatch");
    }
    auto ret = yogic_alloc(llAllocSize, mem);
    memcpy(ret, (void *)&it, llAllocSize);
    return ret;
  }


  virtual string getSynopsis() const {
    return clsName;
  };

  string getSignature() const {
    auto syn = getSynopsis();
    return secureHashToIdentSuffix(syn, 16);
  }

  string getTypeAndVersion() const {
    return clsName + "@"s + getSignature();
  }

  virtual llvm::Type *getLlType(YogaContext const &ctx, EmitCtx &e); // type_llvm.cc

  virtual llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e)
  {
    return nullptr;
  }
  
  CallableYogaAccessor getAccessor(string const &subName, string const &opType)
  {
    return accessorsByName[subName + "::" + opType];
  }

  CallableYogaAccessor getAccessorOrThrow(string const &subName, string const &opType)
  {
    auto ret = getAccessor(subName, opType);
    if (!ret) throw runtime_error(
      "No such accessor "s + shellEscape(subName + "::" + opType) + 
      "' in " + shellEscape(clsName));
    return ret;
  }

  virtual bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
  {
    return true;
  }

  virtual bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const
  {
    ctx.logError("packetWr: unknown type " + shellEscape(clsName));
    return false;
  }

  virtual bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const
  {
    ctx.logError("packetWr: unknown type " + shellEscape(clsName));
    return false;
  }

  virtual void dump(ostream &s, string const &indent) const
  {
    s << indent << clsName << (frozen ? "" : " (...)"s) << "\n";
  }

  string desc() const {
    ostringstream s;
    dump(s, ""s);
    return s.str();
  }

  YogaType *ptrType;

};

inline ostream &
operator << (ostream &s, YogaType const &t)
{
  t.dump(s, ""s);
  return s;
}


inline ostream &
operator << (ostream &s, YogaType const *t)
{
  if (t) {
    return s << t->clsName;
  }
  else {
    return s << "null";
  }
}


struct YogaTypePrimitive : YogaType {
    YogaTypePrimitive(
    string const &_clsName)
    : YogaType(_clsName)
  {
    _isIntegral = (
      clsName == "S64" ||
      clsName == "U64" ||
      clsName == "S32" ||
      clsName == "U32" ||
      clsName == "S16" || 
      clsName == "U16" ||
      clsName == "S8" ||
      clsName == "U8");
    _isSigned = (
      clsName == "S64" ||
      clsName == "S32" ||
      clsName == "S16" ||
      clsName == "S8" ||
      clsName == "R" ||
      clsName == "F");
    _isReal = (
      clsName == "R");
    _isComplex = (
      clsName == "C");
    _isBool = (
      clsName == "bool");
    _isString = (
      clsName == "string");
    _isObject = (
      clsName == "Object");
    _isPacket = (
      clsName == "packet");
    _isVoid = (
      clsName == "void");
  }

  bool _isIntegral, _isSigned, _isReal, _isComplex, _isBool, _isObject, _isString, _isPacket, _isVoid;

  bool tryFreeze(int depth=0) override
  {
    frozen = true;
    return true;
  }

  YogaTypePrimitive *asPrimitive() override { return this; }
  bool isPrimitive() const override { return true; }

  bool isIntegral() const override
  {
    return _isIntegral;
  }
  bool isSigned() const override
  {
    return _isSigned;
  }
  bool isReal() const override
  {
    return _isReal;
  }
  bool isComplex() const override
  {
    return _isComplex;
  }
  bool isScalar() const override
  {
    return _isReal || _isIntegral;
  }
  bool isBool() const override
  {
    return _isBool;
  }
  bool isString() const override
  {
    return _isString;
  }
  bool isObject() const override
  {
    return _isObject;
  }
  bool isPacket() const override
  {
    return _isPacket;
  }
  bool isVoid() const override
  {
    return _isVoid;
  }
  bool hasLinearOps() const override
  {
    return isReal() || isComplex();
  }


  llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e) override;
  
  bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn) override;

  bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;
  bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;

};

struct YogaTypePtr : YogaType {
  YogaTypePtr(
    YogaType *_nonPtrType)
    : YogaType(_nonPtrType->clsName + "*"s),
      nonPtrType(_nonPtrType)
  {
  }

  YogaTypePtr *asPtr() override { return this; }
  bool isPtr() const override { return true; }

  YogaType *getNonPtrType() const override {
    return nonPtrType;
  }

  bool tryFreeze(int depth=0) override
  {
    if (frozen) return true;
    frozen = true;
    if (depth < 1000) nonPtrType->tryFreeze(depth + 1);
    return true;
  }

  bool isEquivalentType(YogaType *other) override {
    if (auto otherPtr = other->asPtr()) {
      return nonPtrType->isEquivalentType(otherPtr->nonPtrType);
    }
    return false;
  }

  YogaType *nonPtrType;

  llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e) override;
  
  bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn) override;

  bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;
  bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;

};

struct StructMember {
  StructMember(
    string _memberName,
    YogaType *_memberType)
    : memberName(_memberName),
      memberType(_memberType)
  {
  }

  YogaSourceLoc sourceLoc;
  string memberName;
  YogaType *memberType;
  AstExpr *initVal{nullptr};
  int32_t llIdx{0};
};

struct YogaTypeStruct : YogaType {
  YogaTypeStruct(
    string const &_clsName,
    bool _isOnehot)
    : YogaType(_clsName),
      isOnehot(_isOnehot)
  {
  }

  vector<StructMember *> members;
  bool autoCreate{false};
  bool isOnehot{false};
  bool scopeNoMembers{false};
  llvm::StructLayout const *llLayout{nullptr};

  YogaTypeStruct *asStruct() override { return this; }
  bool isStruct() const override { return true; }

  bool hasLinearOps() const override
  {
    if (isDebugLocals) return false;
    for (auto &memit : members) {
      if (memit->memberType->hasLinearOps()) return true;
    }
    return false;
  }

  string getSynopsis() const override {
    string ret = clsName;
    ret += "{";
    bool sep = false;
    for (auto const &it : members) {
      if (sep) ret += ',';
      sep = true;
      ret += quoteStringJs(it->memberName) + ":"s + quoteStringJs(it->memberType->clsName);
    }
    ret += "}";
    return ret;
  };

  StructMember *getMember(string const &name)
  {
    for (auto &it : members) {
      if (it->memberName == name) return it;
    }
    return nullptr;
  }

  StructMember *addMember(YogaContext const &ctx, string const &memberName, YogaType *memberType)
  {
    if (frozen) throw runtime_error("addMember: already frozen");
    assert(memberType);
    auto old = getMember(memberName);
    if (old) {
      if (old->memberType == memberType) { // WRITEME: check for options
        return old;
      }
      else {
        // maybe provide some info
        return nullptr;
      }
    }
    auto memberInfo = ctx.mkStructMember(memberName, memberType);
    memberInfo->llIdx = members.size();
    members.push_back(memberInfo);
    return memberInfo;
  }

  bool makeFieldMap(YogaContext const &ctx, EmitCtx &e);

  bool tryFreeze(int depth=0) override {
    if (frozen) return true;
    for (auto &it : members) {
      if (depth >= 1000 || !it->memberType->tryFreeze(depth + 1)) return false;
    }
    frozen = true;
    return true;
  }

  bool isEquivalentType(YogaType *other) override {
    if (auto otherStruct = other->asStruct()) {
      if (otherStruct->members.size() != members.size()) return false;
      if (otherStruct->isOnehot != isOnehot) return false;
      for (size_t i = 0; i < members.size(); i++) {
        if (members[i]->memberName != otherStruct->members[i]->memberName) return false;
        if (!members[i]->memberType->isEquivalentType(otherStruct->members[i]->memberType)) return false;
      }
      return true;
    }
    return false;
  }

  llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e) override;
  
  bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn) override;
  
  bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;
  bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;

  void dump(ostream &s, string const &indent) const override
  {
    s << indent << clsName << (frozen ? "" : " (...)"s) << "\n";
    for (auto &it : members) {
      s << indent << "  " << it->memberName << (
        frozen ? "" : " (...)"
      ) << "\n";
      it->memberType->dump(s, indent + "    "s);
    }
  }

};

struct YogaTypeMatrix : YogaType {
  YogaTypeMatrix(
    YogaType *_elementType,
    int _rows,
    int _cols)
    : YogaType(clsNameFor(_elementType, _rows, _cols)),
      elementType(_elementType),
      rows(_rows),
      cols(_cols)
  {
  }

  YogaType *elementType{nullptr};
  int rows;
  int cols;

  YogaTypeMatrix *asMatrix() override { return this; }
  bool isMatrix() const override { return true; }

  YogaType *getElementType() const override { return elementType; }

  bool hasLinearOps() const override
  {
    return elementType->hasLinearOps();
  }

  bool forEachElem(YogaContext const &ctx, std::function<bool(YogaContext const &ctx, int ri, int ci)> f)
  {
    if (rows < 0 || cols < 0) return ctx.logError("forEachElem: dynamic size");
    for (int ri=0; ri < rows; ri++) {
      for (int ci=0; ci < cols; ci++) {
        if (!f(ctx, ri, ci)) return false;
      }
    }
    return true;
  }

  bool tryFreeze(int depth=0) override
  {
    if (frozen) return true;
    if (rows < 0 || cols < 0) {
      frozen = true;
      return true;
    }
    else if (depth < 1000 && elementType->tryFreeze(depth + 1)) {
      frozen = true;
      return true;
    }
    return false;
  }

  bool isEquivalentType(YogaType *other) override {
    if (auto otherMatrix = other->asMatrix()) {
      if (!elementType->isEquivalentType(otherMatrix->elementType)) return false;
      return otherMatrix->rows == rows && otherMatrix->cols == cols;
    }
    return false;
  }

  llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e) override;

  type_info const &glmFloatTypeid() const;
  type_info const &glmDoubleTypeid() const;

  bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn) override;

  bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;
  bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;

  static string clsNameFor(YogaType *_elementType, int _rows, int _cols)
  {
    return _elementType->clsName + "["s + (_rows >= 0 ? to_string(_rows) : ""s) + ","s + (_cols >=0 ? to_string(_cols): ""s) + "]";
  }
};


struct YogaTypeSet : YogaType {
  YogaTypeSet()
    : YogaType("Set")
  {
  }

  YogaTypeSet *asSet() override { return this; }
  bool isSet() const override { return true; }

  bool tryFreeze(int depth=0) override
  {
    if (frozen) return true;
    if (depth >= 1000) return false;
    frozen = true;
    return true;
  }

  bool isEquivalentType(YogaType *other) override {
    if (auto otherSet = other->asSet()) {
      return true;
    }
    return false;
  }

  llvm::Type *makeLlType(YogaContext const &ctx, EmitCtx &e) override;
  
  bool addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn) override;

  bool addPacketWr(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;
  bool addPacketRd(YogaContext const &ctx, EmitCtx &e,
      llvm::Value *objPtr,
      llvm::Value *packetPtr) const override;

};


namespace packetio {
  void packet_wr_value(::packet &p, YogaType * const &x, YogaContext const &ctx);
  void packet_rd_value(::packet &p, YogaType * &x, YogaContext const &ctx);
} // namespace packetio
