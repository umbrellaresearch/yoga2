#pragma once
#include "./jit_utils.h"
#include "./sourcefile.h"

enum YogaToken {
  T_EOF,
  T_ERR,

  T_IF,
  T_SOFTIF,
  T_ELSE,

  T_DEFAULT,
  T_STRUCT,
  T_ONEHOT,

  T_DERIV,
  T_RUNTIME,
  T_POLYFIT,
  T_LIVE,
  T_INIT,
  T_FUNCTION,
  T_REQUIRE,
  T_EXTERN,
  T_PANEL,
  T_PREDICT,
  T_TRAIN,

  T_IN,
  T_OUT,
  T_UPDATE,

  T_TRUE,
  T_FALSE,
  T_NULL,

  T_NAME,
  T_CLSNAME,
  T_NUMBER,
  T_STRING,
  T_COLOR,
  
  T_LBRK,       // [
  T_RBRK,       // ]
  T_LPAR,       // (
  T_RPAR,       // )
  T_LWING,      // {
  T_RWING,      // }
  T_DOT,        // .
  T_PTR,        // ->
  T_INC,        // ++
  T_DEC,        // --
  T_AND,        // &
  T_STAR,       // *
  T_PLUS,       // +
  T_MINUS,      // -
  T_TILDA,      // ~
  T_BANG,       // !
  T_DIV,        // /
  T_MOD,        // %
  T_LEFT,       // <<
  T_RIGHT,      // >>
  T_LT,         // <
  T_GT,         // >
  T_LE,         // <=
  T_GE,         // >=
  T_EQEQ,       // ==
  T_BANGEQ,     // !=
  T_HAT,        // ^
  T_OR,         // |
  T_ANDAND,     // &&
  T_OROR,       // ||
  T_QUERY,      // ?
  T_COLON,      // :
  T_SEMI,       // ;
  T_ELLIPSIS,   // ...
  T_EQ,         // =
  T_STAREQ,     // *=
  T_DIVEQ,      // /=
  T_MODEQ,      // %=
  T_PLUSEQ,     // +=
  T_MINUSEQ,    // -=
  T_LEFTEQ,     // <<=
  T_RIGHTEQ,    // >>=
  T_ANDEQ,      // &=
  T_HATEQ,      // ^=
  T_TILTIL,     // ~~
  T_OREQ,       // |=
  T_COMMA,      // ,
  T_COLONCOLON, // ::
  T_PREPROC,    // #word

};

struct YogaTokenizer {

  YogaTokenizer(YogaSourceFile *_src);
  YogaTokenizer();

  YogaSourceFile *src;
  char const *p{nullptr};
  char const *tokenStart{nullptr};
  char const *textStart{nullptr};
  vector<int> ifdefStack;
  
  YogaToken tok;
  string literal;

  void start();
  void nextTok();

  bool match(YogaToken expected);
  bool consume(YogaToken expected);
  bool consume(YogaToken expected, string &literalOut);

  YogaSourceLoc getSourceLoc();

  bool isNumberAhead();
  YogaToken getTok();
  void eatSpace();

  static bool isDigit(int c);
  static bool isAlpha(int c);
  static bool isAlNum(int c);
  static bool isIdent(int c);
  static bool isIdentStart(int c);
  static bool isHexDigit(char c);
  static int fromHexDigit(char c);

  static string describeTok(YogaToken tok0, string const &literal0 = ""s);
  string describeCurTok();

  void dumpToks(ostream &s);
};
