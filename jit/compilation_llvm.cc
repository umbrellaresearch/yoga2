#include "./expr.h"
#include "./type.h"
#include "./param.h"
#include "./effects.h"
#include "./runtime.h"
#include "./expr_impl.h"

#include "./emit_ctx.h"
#include "llvm-c/Disassembler.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/IR/IRPrintingPasses.h"
#include "llvm/Analysis/BasicAliasAnalysis.h"
#include "llvm/Transforms/Scalar/GVN.h"
#include "llvm/Transforms/Scalar/SROA.h"
#include "llvm/Transforms/Scalar/EarlyCSE.h"
#include "llvm/Transforms/Scalar/Reassociate.h"
#include "llvm/Transforms/InstCombine/InstCombine.h"
#include "llvm/Transforms/Scalar/CorrelatedValuePropagation.h"
#include "llvm/Transforms/Scalar/MemCpyOptimizer.h"



// ----------------

bool YogaCompilation::initLlvm(YogaContext const &ctx)
{
  static std::once_flag llvmInited;
  std::call_once(llvmInited, []() {
    InitializeNativeTarget();
    LLVMInitializeNativeDisassembler();
    InitializeNativeTargetAsmPrinter();
    //InitializeNativeTargetAsmParser();

    string dlerr;
    if (sys::DynamicLibrary::LoadLibraryPermanently(nullptr, &dlerr)) {
      cerr << "LoadLibraryPermanently error: "s + shellEscape(dlerr) + "\n";
      throw runtime_error("initLlvm failed");
    }
  });

  if (!ctx.reg->emitCtx) {
    auto jtmb = JITTargetMachineBuilder::detectHost();
    if (!jtmb) {
      cerr << "JITTargetMachineBuilder::detectHost failed:"s + repr(jtmb.takeError()) + "\n";
      throw runtime_error("initLlvm failed");
    }

    if (1) {
      StringMap<bool> cpuFeatures;
      assert(sys::getHostCPUFeatures(cpuFeatures));
      for (auto &it : cpuFeatures) {
        jtmb->getFeatures().AddFeature(it.first(), it.second);
      }
    }
    if (0) cerr << "JIT Target Features "s + jtmb->getFeatures().getString() + "\n";

    auto dl = jtmb->getDefaultDataLayoutForTarget();
    if (!dl) {
      cerr << "getDefaultDataLayoutForTarget failed: " + repr(dl.takeError()) + "\n";
      throw runtime_error("initLlvm failed");
    }
    ctx.reg->emitCtx = std::make_shared<EmitCtx>(std::move(*jtmb), std::move(*dl), ctx.reg);

    EmitCtx &e = *ctx.reg->emitCtx;

    e.builder = make_unique<IRBuilder<>>(e.l);
    e.entryBuilder = make_unique<IRBuilder<>>(e.l);

    e.memoryResourceTy = ctx.reg->memoryResourceType->getLlType(ctx("setup"), e);

    auto consCell = StructType::create(e.l, "set.cons");
    // like YogaSetCons
    consCell->setBody({
      consCell->getPointerTo(), // next
      Type::getInt8PtrTy(e.l),  // type
      Type::getDoubleTy(e.l),   // modulation
      Type::getInt8PtrTy(e.l),  // value
      Type::getInt8PtrTy(e.l)   // sourceLoc
    });
    e.yogaSetTy = consCell->getPointerTo();

    e.yogaFunctionTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l)->getPointerTo(), // outArgs
      Type::getInt8PtrTy(e.l)->getPointerTo(), // inArgs
      Type::getDoublePtrTy(e.l), // params
      Type::getDoublePtrTy(e.l), // astonishment out
      Type::getDoubleTy(e.l), // dt
      e.memoryResourceTy // mem
    }, false);

    e.yogaDebugTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l)->getPointerTo(), // outArgs
      Type::getInt8PtrTy(e.l)->getPointerTo(), // inArgs
      Type::getDoublePtrTy(e.l), // params
      Type::getDoublePtrTy(e.l), // astonishment out
      Type::getDoubleTy(e.l), // dt
      Type::getInt8PtrTy(e.l), // locals
      e.memoryResourceTy // mem
    }, false);

    e.yogaGradTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l)->getPointerTo(), // outArgs
      Type::getInt8PtrTy(e.l)->getPointerTo(), // outArgs grad in
      Type::getInt8PtrTy(e.l)->getPointerTo(), // inArgs
      Type::getInt8PtrTy(e.l)->getPointerTo(), // inArgs grad out
      Type::getDoublePtrTy(e.l), // params
      Type::getDoublePtrTy(e.l), // params grad out
      Type::getDoublePtrTy(e.l), // astonishment out
      Type::getDoubleTy(e.l), // dt
      e.memoryResourceTy // mem
    }, false);

    e.yogaAccessorTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l),
      Type::getInt8PtrTy(e.l),
    }, false);

    e.yogaLinearOpTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l), // out
      Type::getDoubleTy(e.l), // coeffA
      Type::getInt8PtrTy(e.l), // a
      Type::getDoubleTy(e.l), // coeffB
      Type::getInt8PtrTy(e.l), // b
      e.memoryResourceTy // mem
    }, false);


    e.yogaNormTy = FunctionType::get(Type::getDoubleTy(e.l), {
      Type::getInt8PtrTy(e.l), // a
    }, false);


    e.yogicStrcatTy = FunctionType::get(Type::getInt8PtrTy(e.l), {
      Type::getInt8PtrTy(e.l), // a
      Type::getInt8PtrTy(e.l), // b
      e.memoryResourceTy // mem
    }, false);

    e.yogicAllocTy = FunctionType::get(Type::getInt8PtrTy(e.l), {
      Type::getInt64Ty(e.l), // size
      e.memoryResourceTy // mem
    }, false);

    e.packetWrValueFuncTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l), 
      Type::getInt8PtrTy(e.l)
    }, false);

    e.packetRdValueFuncTy = FunctionType::get(Type::getVoidTy(e.l), {
      Type::getInt8PtrTy(e.l), 
      Type::getInt8PtrTy(e.l)
    }, false);
  }

  return true;
}

bool YogaCompilation::generateLlTypes(YogaContext const &ctx)
{
  EmitCtx &e = *ctx.reg->emitCtx;
  for (auto &[symName, symVal] : ctx.reg->symbolTable) {
    if (symVal.type) {
      auto llt = symVal.type->getLlType(ctx, e);
      if (!llt) {
        return ctx.logError("emit: Failed to make LL type for "s + symName);
      }
    }
  }
  return true;
}

bool YogaCompilation::generateLlModule(YogaContext const &ctx, 
    std::function<bool(YogaContext const &ctx, EmitCtx &e)> addCode)
{
  EmitCtx &e = *ctx.reg->emitCtx;

  static int moduleCounter;
  string modName = "yoga_module"s + to_string(++moduleCounter);
  if (0) cerr << "Create module " + modName + "\n";
  auto m = std::make_unique<Module>(modName, e.l);
  e.m = m.get();
  if (0) {
    cerr << "Default module dl = "s + m->getDataLayout().getStringRepresentation() + "\n";
    cerr << "Replacing module dl = "s + e.dl.getStringRepresentation() + "\n";
  }
  m->setDataLayout(e.dl);

  auto debugPasses = ctx.reg->verbose >= 3;
  PassBuilder pb; // e.tm;
  LoopAnalysisManager lam(debugPasses);
  FunctionAnalysisManager fam(debugPasses);
  CGSCCAnalysisManager cgam(debugPasses);
  ModuleAnalysisManager mam(debugPasses);

  fam.registerPass([&pb] { return pb.buildDefaultAAPipeline(); });

  pb.registerModuleAnalyses(mam);
  pb.registerCGSCCAnalyses(cgam);
  pb.registerFunctionAnalyses(fam);
  pb.registerLoopAnalyses(lam);
  pb.crossRegisterProxies(lam, fam, cgam, mam);

  ModulePassManager mpm(debugPasses);

  if (ctx.reg->verbose >= 1) mpm.addPass(PrintModulePass(e.dumpStream, "Before opt"));
  if (1) {
    FunctionPassManager fpm;
    fpm.addPass(SROA());
    fpm.addPass(EarlyCSEPass(true));
    if (0) fpm.addPass(CorrelatedValuePropagationPass());
    fpm.addPass(InstCombinePass());
    if (0) fpm.addPass(ReassociatePass());
    fpm.addPass(GVN());
    if (0) fpm.addPass(MemCpyOptPass());
    if (0) fpm.addPass(InstCombinePass());
    mpm.addPass(createModuleToFunctionPassAdaptor(std::move(fpm)));
  }
  if (ctx.reg->verbose >= 1) mpm.addPass(PrintModulePass(e.dumpStream, "After opt"));

  if (!addCode(ctx, e)) {
    ModulePassManager mpm2(true);
    mpm2.addPass(PrintModulePass(e.dumpStream, "After error"));
    mpm2.run(*e.m, mam);
    e.dumpStream.flush();
    ctx.logGen(e.dumpStr);

    return false;
  }


  auto err = e.mainJD->define(absoluteSymbols(std::move(e.foreigns)));
  if (err) return ctx.logError("installing foreign symbols failed: " + repr(err));

  mpm.run(*e.m, mam);

  if (ctx.reg->verbose >= 1) {
    e.es.dump(e.dumpStream);
    e.dumpStream.flush();
    ctx.logGen(e.dumpStr);
  }

  err = e.compileLayer.add(*e.mainJD,
                ThreadSafeModule(std::move(m), e.tsc));
  if (err) return ctx.logError("emit: compileLayer.add: " + repr(err));

  if (0) {
    for (auto &gvit : e.m->global_values()) {
      auto name = gvit.getName();
      cerr << "Module has global " + string(name) + "\n";
    }
  }

  SymbolLookupSet symbols;
  for (auto &[symName, todo] : e.onResolved) {
    symbols.add(e.mangle(symName));
  }

  auto resolvedSymbols = e.es.lookup({{e.mainJD, JITDylibLookupFlags::MatchExportedSymbolsOnly}}, symbols);
  if (!resolvedSymbols) {
    e.es.dump(e.dumpStream);
    e.dumpStream.flush();
    ctx.logGen(e.dumpStr);
    return ctx.logError("emit: failed to resolve symbols");
  }

  for (auto &[symName, todo] : e.onResolved) {
    auto symValIt = resolvedSymbols->find(e.mangle(symName));

    if (symValIt == resolvedSymbols->end()) throw runtime_error("Can't find symbol " + symName);
    auto symAddr = symValIt->second.getAddress();    
    for (auto &it : todo) {
      if (!it(reinterpret_cast<void *>(symAddr))) return false;
    }
  }
  e.onResolved.clear();

  if (ctx.reg->verbose >= 1) {
    auto dis = LLVMCreateDisasm(
      llvm::sys::getProcessTriple().c_str(),
      nullptr,
      0,
      nullptr,
      nullptr);
    if (!dis) throw runtime_error("LLVMCreateDisasm failed");

    for (auto &[symName, symVal] : ctx.reg->symbolTable) {
      if (auto cf = symVal.compiledFunc) {
        for (auto [suffix, p] : {
          make_tuple(".yoga", (U8 *)cf->funcAddr),
          make_tuple(".yogagrad", (U8 *)cf->gradAddr),
          make_tuple(".yogadebug", (U8 *)cf->debugAddr)
          }) {

          ctx.logGen("Disassembly for "s + symName + suffix + " at " + repr_ptr(p));
          if (!p) {
            ctx.logGen("  (no function body)");
            continue;
          }
          int remaining = 100000; // FIXME
          int instCount = 0;
          while (remaining > 0) {
            char buf[256];
            auto instSize = LLVMDisasmInstruction(
              dis,
              p,
              remaining,
              reinterpret_cast<U64>(p),
              buf,
              sizeof(buf));
            if (instSize == 0) break;
            instCount++;
            p += instSize;
            remaining -= instSize;
            ctx.logGen("  "s + buf);
            if (strstr(buf, "retq")) break;
          }
          cerr << "Generated for "s + symName + suffix + " " + repr(instCount) + " instructions\n";
        }
      }
    }

    LLVMDisasmDispose(dis);
  }

  if (ctx.reg->verbose >= 1) cerr << "Created module " + modName + " allocs=" + repr(e.allocCount) + "\n";

  return true;
}

bool YogaCompilation::generateLlCodeForYogaFunctions(YogaContext const &ctx)
{
  return generateLlModule(ctx,
    [](YogaContext const &ctx, EmitCtx &e) {
      for (auto &[symName, symVal] : ctx.reg->symbolTable) {
        if (symVal.funcEffects && symVal.funcEffects->cppCallable) {
          auto effects = symVal.funcEffects;
          e.defineForeign(symName + ".yoga", (void *)symVal.funcEffects->cppCallable, e.yogaFunctionTy);
          e.defineForeign(symName + ".yogadebug", (void *)symVal.funcEffects->cppCallableDebug, e.yogaDebugTy);

          auto cf = ctx.reg->getCompiledFunc(symName);
          cf->inArgs = effects->inArgs;
          cf->outArgs = effects->outArgs;
          cf->funcAddr = symVal.funcEffects->cppCallable;
          cf->debugAddr = symVal.funcEffects->cppCallableDebug;
        }
        else {
          if (auto effects = symVal.funcEffects) {
            auto implName = symName + ".yoga";
            if (!generateLlCodeForYogaFunction(ctx(symName.c_str()), e, implName, effects)) return false;
            auto cf = ctx.reg->getCompiledFunc(symName);
            cf->inArgs = effects->inArgs;
            cf->outArgs = effects->outArgs;
            e.onResolved[implName].push_back([cf](void *symAddr) {
              if (symAddr) {
                cf->funcAddr = (CallableYogaFunction)symAddr;
              }
              return true;
            });
          }
          if (ctx.reg->emitGradVersions) {
            if (auto effects = symVal.funcGradEffects) {
              auto implName = symName + ".yogagrad";
              if (!generateLlCodeForYogaGrad(ctx(symName.c_str()), e, implName, effects)) return false;
              auto cf = ctx.reg->getCompiledFunc(symName);
              e.onResolved[implName].push_back([cf](void *symAddr) {
                if (symAddr) {
                  cf->gradAddr = (CallableYogaGrad)symAddr;
                }
                return true;
              });
            }
          }
          if (ctx.reg->emitDebugVersions) {
            if (auto effects = symVal.funcEffects) {
              auto implName = symName + ".yogadebug";
              if (!generateLlCodeForYogaDebug(ctx(symName.c_str()), e, implName, effects)) return false;
              auto cf = ctx.reg->getCompiledFunc(symName);
              cf->debugLocalsType = effects->debugLocalsType;
              e.onResolved[implName].push_back([cf](void *symAddr) {
                if (symAddr) {
                  cf->debugAddr = (CallableYogaDebug)symAddr;
                }
                return true;
              });
            }
          }
        }
        if (auto t = symVal.type) {
          // avoid dupes with multiple names in symbol table for the same type. Only install under canonical name
          if (symName == t->clsName) {
            if (!generateLlAccessorsFor(ctx(symName.c_str())("accessors"), e, t)) return false;
            if (!generateLlPacketWrFor(ctx(symName.c_str())("packetWr"), e, t)) return false;
            if (!generateLlPacketRdFor(ctx(symName.c_str())("packetRd"), e, t)) return false;
            if (!generateLlLinearOpsFor(ctx(symName.c_str())("linearOps"), e, t)) return false;
            if (!generateLlNormSqFor(ctx(symName.c_str())("normsq"), e, t)) return false;
            t->getZero(ctx.reg->mem);
          }
        }
      }
      return true;
    });
}




[[nodiscard]] bool YogaCompilation::emitEffects(YogaContext const &ctx, EmitCtx &e, FunctionEffects *effects)
{
  for (auto &assit : effects->assignments) {
    if (!assit.second.dst || !assit.second.getSrcPhi(ctx)) continue;
    auto &aset = assit.second;

    auto phip = e.getExprOut(ctx, aset.getSrcPhi(ctx));
    if (!phip) return ctx(aset.dst).logError("emit: No rvalue for phi");

    auto dstp = e.getExprOut(ctx, aset.dst);
    if (!dstp) return ctx(aset.dst).logError("emit: No lvalue for dst");

    if (aset.mode == AUGMODE_PLUSEQ_INPLACE) {
      if (!e.assignPluseqInplace(ctx, dstp, phip)) return false;
    }
    else {
      if (!e.assign(ctx, dstp, phip)) return false;
    }
  }

  if (auto debugLocalsPtr = e.argPtrs["debugLocals"]) {

    for (auto &[localMemberName, value, modulation] : effects->debugLocalItems) {
      auto srcVal = e.getExprOut(ctx, value);
      auto srcMod = e.getExprOut(ctx, modulation);
      if (!srcVal || !srcMod) return false;

      auto localMemInfo = effects->debugLocalsType->getMember(localMemberName);
      // WRITEME: store modulation too
      ExprOut memPtr;
      if (!e.getStructElem(ctx, memPtr, debugLocalsPtr, localMemInfo)) return false;
      if (!e.assign(ctx, memPtr, srcVal)) return false;
    }
  }

  e.builder->CreateRetVoid();
  return true;
}


bool YogaCompilation::generateLlCodeForYogaFunction(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects)
{
  enum {
    ARG_OUT,
    ARG_IN,
    ARG_PARAMS,
    ARG_ASTONISHMENT,
    ARG_DT,
    ARG_MEM,
    ARG_COUNT
  };

  auto f = e.startFunction(e.yogaFunctionTy, implName);

  assert(f->arg_end() - f->arg_begin() == ARG_COUNT);

  if (!e.settv(ctx, e.argPtrs["dt"s], ctx.reg->rType, &f->arg_begin()[ARG_DT])) return false;
  if (!e.settp(ctx, e.argPtrs["yogaParamsLin"], ctx.reg->rType, &f->arg_begin()[ARG_PARAMS])) return false;
  if (!e.settp(ctx, e.argPtrs["astonishment"s], ctx.reg->rType, &f->arg_begin()[ARG_ASTONISHMENT])) return false;
  if (!e.settv(ctx, e.argPtrs["mem"], ctx.reg->memoryResourceType, &f->arg_begin()[ARG_MEM])) return false;

  int inArgIndex = 0;
  for (auto &[argType, argName] : effects->inArgs) {
    if (!e.settp(ctx, e.argPtrs["in."s + argName], argType, e.getYogaArg(ARG_IN, inArgIndex, argType->llType))) return false;
    inArgIndex++;
  }

  int outArgIndex = 0;
  for (auto &[argType, argName] : effects->outArgs) {
    if (!e.settp(ctx, e.argPtrs["out."s + argName], argType, e.getYogaArg(ARG_OUT, outArgIndex, argType->llType))) return false;
    outArgIndex++;
  }

  if (!emitEffects(ctx, e, effects)) return false;

  return e.endFunction();
}


bool YogaCompilation::generateLlCodeForYogaGrad(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects)
{
  enum {
    ARG_OUT,
    ARG_OUT_GRAD_IN,
    ARG_IN,
    ARG_IN_GRAD_OUT,
    ARG_PARAMS,
    ARG_PARAMS_GRAD_OUT,
    ARG_ASTONISHMENT,
    ARG_DT,
    ARG_MEM,
    ARG_COUNT
  };

  auto f = e.startFunction(e.yogaGradTy, implName);

  assert(f->arg_end() - f->arg_begin() == ARG_COUNT);

  if (!e.settv(ctx, e.argPtrs["dt"s], ctx.reg->rType, &f->arg_begin()[ARG_DT])) return false;
  if (!e.settp(ctx, e.argPtrs["yogaParamsLin"], ctx.reg->rType, &f->arg_begin()[ARG_PARAMS])) return false;
  if (!e.settp(ctx, e.argPtrs["yogaParamsLinGrad"], ctx.reg->rType, &f->arg_begin()[ARG_PARAMS_GRAD_OUT])) return false;
  if (!e.settp(ctx, e.argPtrs["astonishment"s], ctx.reg->rType, &f->arg_begin()[ARG_ASTONISHMENT])) return false;
  if (!e.settv(ctx, e.argPtrs["mem"], ctx.reg->memoryResourceType, &f->arg_begin()[ARG_MEM])) return false;

  int inArgIndex = 0;
  for (auto [argType, argName] : effects->inArgs) {
    if (!e.settp(ctx, e.argPtrs["in."s + argName], argType, e.getYogaArg(ARG_IN, inArgIndex, argType->llType))) return false;
    if (!e.settp(ctx, e.argPtrs["inGrad."s + argName], argType, e.getYogaArg(ARG_IN_GRAD_OUT, inArgIndex, argType->llType))) return false;
    inArgIndex++;
  }

  int outArgIndex = 0;
  for (auto [argType, argName] : effects->outArgs) {
    if (!e.settp(ctx, e.argPtrs["out."s + argName], argType, e.getYogaArg(ARG_OUT, outArgIndex, argType->llType))) return false;
    if (!e.settp(ctx, e.argPtrs["outGrad."s + argName], argType, e.getYogaArg(ARG_OUT_GRAD_IN, outArgIndex, argType->llType))) return false;
    outArgIndex++;
  }

  if (!emitEffects(ctx, e, effects)) return false;

  return e.endFunction();
}


bool YogaCompilation::generateLlCodeForYogaDebug(YogaContext const &ctx, EmitCtx &e, string const &implName, FunctionEffects *effects)
{
  enum {
    ARG_OUT,
    ARG_IN,
    ARG_PARAMS,
    ARG_ASTONISHMENT,
    ARG_DT,
    ARG_DEBUG_LOCALS,
    ARG_MEM,
    ARG_COUNT
  };

  auto f = e.startFunction(e.yogaDebugTy, implName);

  assert(f->arg_end() - f->arg_begin() == ARG_COUNT);

  if (!e.settv(ctx, e.argPtrs["dt"s], ctx.reg->rType, &f->arg_begin()[ARG_DT])) return false;
  if (!e.settp(ctx, e.argPtrs["yogaParamsLin"], ctx.reg->rType, &f->arg_begin()[ARG_PARAMS])) return false;
  if (!e.settp(ctx, e.argPtrs["astonishment"s], ctx.reg->rType, &f->arg_begin()[ARG_ASTONISHMENT])) return false;
  if (!e.settp(ctx, e.argPtrs["debugLocals"], effects->debugLocalsType, 
    e.builder->CreateBitCast(&f->arg_begin()[ARG_DEBUG_LOCALS],
    effects->debugLocalsType->llType->getPointerTo()
    ))) return false;
  if (!e.settv(ctx, e.argPtrs["mem"], ctx.reg->memoryResourceType, &f->arg_begin()[ARG_MEM])) return false;

  int inArgIndex = 0;
  for (auto [argType, argName] : effects->inArgs) {
    if (!e.settp(ctx, e.argPtrs["in."s + argName], argType, e.getYogaArg(ARG_IN, inArgIndex, argType->llType))) return false;
    inArgIndex++;
  }

  int outArgIndex = 0;
  for (auto [argType, argName] : effects->outArgs) {
    if (!e.settp(ctx, e.argPtrs["out."s + argName], argType, e.getYogaArg(ARG_OUT, outArgIndex, argType->llType))) return false;
    outArgIndex++;
  }

  if (!emitEffects(ctx, e, effects)) return false;

  return e.endFunction();
}
