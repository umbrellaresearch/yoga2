#include "./ast.h"


string AstNode::gloss() const
{
  return "?";
}
string AstName::gloss() const
{
  return name;
}
string AstCall::gloss() const
{
  auto ret = func->gloss();
  ret += '(';
  auto firstarg = true;
  for (auto a : args) {
    if (!firstarg) ret += ", ";
    firstarg = false;
    ret += a->gloss();
  }
  ret += ')';
  return ret;
}

string AstIndex::gloss() const
{
  auto ret = lhs->gloss();
  ret += '[';
  auto firstarg = true;
  for (auto a : args) {
    if (!firstarg) ret += ", ";
    firstarg = false;
    ret += a->gloss();
  }  
  ret += ']';
  return ret;
}

string AstStructref::gloss() const
{
  auto ret = lhs->gloss();
  ret += '.';
  ret += memberName->gloss();
  return ret;
}

string AstNumber::gloss() const
{
  return literalNumber;
}

string AstAnnoCall::gloss() const
{
  return call->gloss() + " " + options->gloss();
}

string AstObject::gloss() const
{
  string ret = "{";
  bool firstarg = true;
  for (auto const &it : keyValues) {
    if (!firstarg) ret += ", ";
    firstarg = false;
    ret += it->key->gloss() + ": " + it->value->gloss();
  }
  ret += "}";
  return ret;
}

template<>
string repr(AstNode const * const &it)
{
  ostringstream oss;
  it->dump(oss, "  "s, 3);
  return oss.str();
}


void AstNode::dumpHead(ostream &s, string const &indent) const
{
  if (lv && rv) {
    s << "bv";
  }
  else if (lv) {
    s << "lv";
  }
  else if (rv) {
    s << "rv";
  }
  else {
    s << "  ";
  }
  s << indent;
  s << " ";
}

void AstNode::dump(ostream &s, string const &indent, int depth) const {
  dumpHead(s, indent);
  s << "AstNode " + sourceLoc.gloss() + "\n";
}

string AstNode::desc() const {
  ostringstream s;
  dump(s, ""s, 3);
  return s.str();
}


void AstName::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << (isClsName ? "clsName " : "name ") << name << " " << sourceLoc.gloss() << "\n";
}

void AstTernop::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "ternop " << op << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (lhs) lhs->dump(s, indent + "  "s, depth - 1);
    if (mhs) mhs->dump(s, indent + "  "s, depth - 1);
    if (rhs) rhs->dump(s, indent + "  "s, depth - 1);
  }
}

void AstBinop::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "binop " << op << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (lhs) lhs->dump(s, indent + "  "s, depth - 1);
    if (rhs) rhs->dump(s, indent + "  "s, depth - 1);
  }
}

void AstUnop::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "unop " << op << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (arg) arg->dump(s, indent + "  "s, depth - 1);
  }
}

void AstCall::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "call " << literalFuncName << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : args) {
      if (it) it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstIndex::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "index " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (lhs) lhs->dump(s, indent + "  "s, depth - 1);
    for (auto const &it : args) {
      if (it) it->dump(s, indent + "  "s, depth - 1);
    }
  }
}
 
void AstStructref::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "structref " << literalMemberName << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (lhs) lhs->dump(s, indent + "  "s, depth - 1);
  }
}

void AstNumber::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "number " << literalNumber << " " << sourceLoc.gloss() << "\n";
}

void AstRangeSpec::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "range " << dist << " " << scaleValue << " " << sourceLoc.gloss() << "\n";
}

void AstParam::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "param " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (lhs) lhs->dump(s, indent + "  "s, depth - 1);
    if (rangeSpec) rangeSpec->dump(s, indent + "  "s, depth - 1);
  }
}

void AstBoolean::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "boolean " << (literalValue ? "true" : "false") << " " << sourceLoc.gloss() << "\n";
}

void AstNull::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "null " << sourceLoc << "\n";
}

void AstString::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "string " << literalValue << " " << sourceLoc.gloss() << "\n";
}

void AstDeriv::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "deriv " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (arg) arg->dump(s, indent + "  "s, depth - 1);
    if (wrt) wrt->dump(s, indent + "  "s, depth - 1);
  }
}

void AstKeyValue::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "keyvalue " << literalKey << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    if (value) value->dump(s, indent + "  "s, depth - 1);
  }
}

void AstObject::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "object " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : keyValues) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstArray::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "array " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : elems) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstExtern::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "extern " << sourceLoc.gloss() << "\n";
}

void AstBlock::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "block " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : statements) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstIf::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "if " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    cond->dump(s, indent + "  "s, depth - 1);
    ifTrue->dump(s, indent + "  "s, depth - 1);
    if (ifFalse) {
      ifTrue->dump(s, indent + "  "s, depth - 1);
    }}
}

void AstDefault::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "default " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    block->dump(s, indent + "  "s, depth - 1);
  }
}

void AstSimple::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "simple " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    expr->dump(s, indent + "  "s, depth - 1);
  }
}

void AstTypeDecl::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "typeDecl " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    type->dump(s, indent + "  "s, depth - 1);
    for (auto const &it : exprs) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstMemberDecl::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "member " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    clsName->dump(s, indent + "  "s, depth - 1);
    if (rangeSpec) rangeSpec->dump(s, indent + "  "s, depth - 1);
    for (auto const &it : members) {
      if (it.first) it.first->dump(s, indent + "  "s, depth - 1);
      if (it.second) it.second->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstStructDef::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "struct " << literalName << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    name->dump(s, indent + "  "s, depth - 1);
    for (auto const &it : memberDecls) {
      it->dump(s, indent + "  "s, depth - 1);
    }
    for (auto const &it : options) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}


void AstEnumDecl::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "enum " << literalMemberName << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : memberOptions) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstOnehot::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "onehot " << literalClsName << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto const &it : members) {
      it->dump(s, indent + "  "s, depth - 1);
    }
  }
}

void AstArgDecl::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "arg ";
  if (dirOutOnly) s << "out";
  if (dirInOnly) s << "in";
  if (dirUpOnly) s << "update";
  s << " " << (optional ? " optional" : "") << " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    type->dump(s, indent + "  ", depth - 1);
    name->dump(s, indent + "  ", depth - 1);
  }
}

void AstFunction::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "function " << funcName + " " << sourceLoc.gloss() << "\n";
  if (depth <= 0) {
    s << indent << "   ...\n";
  }
  else {
    for (auto &it : args) {
      it->dump(s, indent + "  "s, depth - 1);
    }
    if (body) {
      body->dump(s, indent + "  "s, depth - 1);
    }
    else if (cppCallable) {
      s << indent << "  body: cpp callable\n";
    }
  }
}

void AstAnnoCall::dump(ostream &s, string const &indent, int depth) const
{
  dumpHead(s, indent);
  s << "annocall " << sourceLoc.gloss() << "\n";
  call->dump(s, indent + "  "s, depth - 1);
  options->dump(s, indent + "  "s, depth - 1);
}





