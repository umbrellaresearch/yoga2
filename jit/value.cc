#include "./value.h"
#include "./compilation.h"
#include "./type.h"
#include "./emit_ctx.h"
#include "./runtime.h"

bool operator == (YogaValue const &a, YogaValue const &b)
{
  return (
    a.reg == b.reg && 
    a.type == b.type &&
    a.ts == b.ts &&
    (
      (a.buf == nullptr && b.buf == nullptr) ||
      (a.buf != nullptr && b.buf != nullptr && a.type != nullptr && !memcmp(a.buf, b.buf, a.type->llAllocSize))
    )
  );
}

YogaValue::YogaValue(YogaCompilation *_reg, YogaSetCons *it)
  : reg(_reg),
    type(it->type),
    buf(it->value),
    ts(0.0)
{
}


YogaValue YogaValue::dup(apr_pool_t *mem) const
{
  assert(reg && type && buf);
  auto newbuf = yogic_alloc(type->llAllocSize, mem);
  memcpy(newbuf, buf, type->llAllocSize);
  return YogaValue(reg, type, newbuf, ts);
}

vector<YogaValue> dup(vector<YogaValue> const &a, apr_pool_t *mem)
{
  vector<YogaValue> ret;
  for (auto &it : a) {
    ret.push_back(it.dup(mem));
  }
  return ret;
}

void YogaValue::setZero(apr_pool_t *mem)
{
  assert(reg && type);
  auto newbuf = yogic_alloc(type->llAllocSize, mem);
  memset(newbuf, 0, type->llAllocSize);
  buf = newbuf;
}

void YogaValue::setCrap(apr_pool_t *mem)
{
  assert(reg && type);
  auto newbuf = yogic_alloc(type->llAllocSize, mem);
  memset(newbuf, reg->initializeWithByte, type->llAllocSize);
  buf = newbuf;
}


void YogaValue::checkForCrap()
{
  if (reg->initializeWithByte == 0) return;

  size_t crapCount = 0;
  U32 crapVal{0};
  memset(&crapVal, reg->initializeWithByte, sizeof(crapVal));

  for (size_t ofs = 0; ofs < type->llAllocSize; ofs += sizeof(U32)) {
    U32 v = *(U32 *)(buf + ofs);
    if (v == crapVal) {
      crapCount ++;
    }
  }
  if (crapCount) {
    cerr << "Craps in " + repr(*this) + "  (" + repr(crapCount) + "x)\n";
  }
}

bool YogaValue::copyValueFrom(YogaValue const &other)
{
  if (type != other.type) return false;
  memcpy(buf, other.buf, type->llAllocSize);
  return true;
}


ostream & operator <<(ostream &s, YogaValue const &a)
{
  if (a.ts != 0.0) s << "ts=" + repr_0_3f(a.ts) + ": ";
  YogaValuePrintOpts opts;
  a.writeValue(s, opts);
  return s;
}

string repr_0_3f(YogaValue const &a)
{
  ostringstream s;
  s.precision(3);
  s.setf(ios::showpos);
  s.setf(ios::fixed);
  YogaValuePrintOpts opts;
  opts.compact = true;
  a.writeValue(s, opts);
  return s.str();
}


void YogaValue::writeValue(ostream &s, YogaValuePrintOpts opts) const
{
  if (0 && !nonZeroValue()) {
    s << "0";
    return;
  }
  if (!isValid()) {
    s << "[invalid]";
    return;
  }
  if (type->isStruct()) {
    auto t1 = type->asStruct();
    if (opts.compact) {
      s << repr_type(t1) << "{";
    }
    else {
      s << repr_type(t1) << "({";
    }
    auto sep = "";
    for (auto &memit : t1->members) {
      s << sep << memit->memberName << ": ";
      sep = ", ";
      auto ofs = t1->llLayout->getElementOffset(memit->llIdx);
      YogaValue(reg, memit->memberType, buf + ofs, ts).writeValue(s, opts);
    }
    if (opts.compact) {
      s << "}";
    }
    else {
      s << "})";
    }
  }
  else if (type == reg->rType) {
    s << *(double const *)buf;
  }
  else if (type == reg->s32Type) {
    s << *(S32 const *)buf;
  }
  else if (type == reg->u32Type) {
    s << *(U32 const *)buf;
  }
  else if (type == reg->s64Type) {
    s << *(S64 const *)buf;
  }
  else if (type == reg->u64Type) {
    s << *(U64 const *)buf;
  }
  else if (type == reg->s16Type) {
    s << *(S16 const *)buf;
  }
  else if (type == reg->u16Type) {
    s << *(U16 const *)buf;
  }
  else if (type == reg->stringType) {
    s << *(char const * const *)buf;
  }
  else if (type == reg->memoryResourceType) {
    s << "mem("s + repr_ptr(buf) + ")";
  }
  else if (type == reg->boolType) {
    s << ((*(U8 *)buf) ? "true" : "false");
  }
  else if (type == reg->complexType) {
    R real = ((double const *)buf)[0];
    R imag = ((double const *)buf)[1];
    s << real << "+" << imag << "i";
  }
  else if (type->isMatrix()) {
    auto t1 = type->asMatrix();
    if (opts.compact) {
      s << "[";
    }
    else {
      s << repr_type(t1) << "(";
    }
    if (t1->rows >= 0 && t1->cols >= 0) {
      auto elSize = reg->emitCtx->dl.getTypeAllocSize(t1->elementType->llType);
      auto nElem = t1->rows * t1->cols;
      if (nElem > 9 && opts.compact) {
        for (auto ri = 0; ri < t1->rows; ri++) {
          if (ri > 0) s << "\n ";
          for (auto ci = 0; ci < t1->cols; ci++) {
            if (ci > 0) s << " ";
            YogaValue(reg, t1->elementType, buf + ((ri + ci*t1->rows) * elSize), ts).writeValue(s, opts);
          }
        }
      }
      else if (opts.compact) {
        for (auto i=0; i<nElem; i++) {
          if (i > 0) {
            if (i % t1->rows == 0) {
              s << "; ";
            }
            else {
              s << " ";
            }
          }
          YogaValue(reg, t1->elementType, buf + (i * elSize), ts).writeValue(s, opts);
        }
      }
      else {
        for (auto i=0; i<nElem; i++) {
          if (i > 0) {
            s << ", ";
          }
          YogaValue(reg, t1->elementType, buf + (i * elSize), ts).writeValue(s, opts);
        }
      }
    }
    else {
      // WRITEME: dynamic matrix
    }
    if (opts.compact) {
      s << "]";
    } else {
      s << ")";
    }
  }
  else {
    s << "[" << repr_type(type) << "]";
  }
}

