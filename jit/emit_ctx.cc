#include "./expr.h"
#include "./type.h"
#include "./param.h"
#include "./effects.h"
#include "./runtime.h"

#include "./expr_impl.h"

#include "./emit_ctx.h"


template<> string repr(llvm::Error const &it)
{
  string ret;
  {
    raw_string_ostream ros(ret);
    ros << it;
    ros.flush();
  }
  return ret;
}

template<> string repr(llvm::Type * const &it)
{
  if (!it) return "null";
  string ret;
  {
    raw_string_ostream ros(ret);
    it->print(ros, true, false);
    ros.flush();
  }
  return ret;
}


template<> string repr(llvm::FunctionType * const &it)
{
  if (!it) return "null";
  string ret;
  {
    raw_string_ostream ros(ret);
    it->print(ros, true, false);
    ros.flush();
  }
  return ret;
}


template<> string repr(llvm::Value * const &it)
{
  if (!it) return "null";
  string ret;
  {
    raw_string_ostream ros(ret);
    it->print(ros, true);
    ros.flush();
  }
  return ret;
}

EmitCtx::EmitCtx(JITTargetMachineBuilder jtmb, DataLayout const &_dl, YogaCompilation *_reg)
  : reg(_reg),
    objectLayer(es,
      []() { return make_unique<SectionMemoryManager>(); }),
    compileLayer(es, objectLayer, 
      make_unique<ConcurrentIRCompiler>(std::move(jtmb))),
    dl(_dl),
    mangle(es, dl),
    tsc(make_unique<LLVMContext>()),
    dumpStream(dumpStr)
{
  mainJD = es.getJITDylibByName("<main>");
  if (!mainJD) {
    mainJD = &es.createJITDylib("<main>");
  }

  mainJD->addGenerator(
      cantFail(DynamicLibrarySearchGenerator::GetForCurrentProcess(dl.getGlobalPrefix())));
  if (0) {
    auto err = rto.enable(*mainJD, mangle);
    if (err) throw runtime_error("rto.enable failed");
  }
}


FunctionCallee EmitCtx::defineForeign(string const &implName, void *f, FunctionType *fTy)
{
  auto ftarg = static_cast<JITTargetAddress>(reinterpret_cast<uintptr_t>(f));
  foreigns[mangle(implName)] = JITEvaluatedSymbol(ftarg, JITSymbolFlags::Exported);
  return m->getOrInsertFunction(implName, fTy);
}

AllocaInst *EmitCtx::createEntryBlockAlloca(Function *f, Type *t, std::string const &varName)
{
  IRBuilder<> TmpB(&f->getEntryBlock(),
                   f->getEntryBlock().begin());
  return TmpB.CreateAlloca(t, nullptr, varName);
}

ExprOut &EmitCtx::getExprOut(YogaContext const &ctx, ExprNode *n)
{
  auto &o = exprOutsByCseKey[n->cseKey];
  if (o.v || o.p) {
    if (o.t != n->type) throw runtime_error("getExprOut: Type mismatch for "s + n->cseKey);
    return o;
  }

  o.t = n->type;
  if (!n->emit(ctx(n), *this, o)) {
    ctx.logError("Failed to emit "s + n->cseKey + ":\n" + repr(*n));
  }

  return o;
}


void EmitCtx::setZero(AllocaInst *ptr)
{
  auto ty = ptr->getType()->getElementType();
  builder->CreateMemSet(ptr, 
    ConstantInt::get(Type::getInt8Ty(l), 0),
    ConstantInt::get(Type::getInt64Ty(l), dl.getTypeAllocSize(ty)),
    Align(dl.getABITypeAlignment(ty)));
}

void EmitCtx::setInitial(YogaContext const &ctx, AllocaInst *ptr)
{
  if (ctx.reg->initializeWithByte != 0x00) {
    auto ty = ptr->getType()->getElementType();
    builder->CreateMemSet(ptr, 
      ConstantInt::get(Type::getInt8Ty(l), ctx.reg->initializeWithByte),
      ConstantInt::get(Type::getInt64Ty(l), dl.getTypeAllocSize(ty)),
      Align(dl.getABITypeAlignment(ty)));
  }
  else {
    setZero(ptr);
  }
}

AllocaInst *EmitCtx::mkAlloca(YogaContext const &ctx, Type *ty, string const &name)
{
  allocCount++;
  auto ptr = builder->CreateAlloca(ty, nullptr, name);
  if (!ptr) return nullptr;
  if (0) setZero(ptr);
  return ptr;
}

AllocaInst *EmitCtx::mkAlloca(YogaContext const &ctx, ExprNode *n)
{
  return mkAlloca(ctx, n->type->llType, n->cseKey);
}

bool EmitCtx::load(YogaContext const &ctx, ExprOut &o)
{
  if (o.v) return true;
  if (!o.p) return ctx.logError("Load failed: no p");

  o.v = builder->CreateLoad(o.p);
  return true;
}

bool EmitCtx::alloc(YogaContext const &ctx, ExprOut &o)
{
  if (o.p) return true;
  if (!o.t) return ctx.logError("Alloc failed: no t");

  o.p = mkAlloca(ctx, o.t->llType);
  if (o.v) {
    builder->CreateStore(o.v, o.p);
  }
  return true;
}

bool EmitCtx::allocZero(YogaContext const &ctx, ExprOut &o)
{
  if (!alloc(ctx, o)) return false;
  setInitial(ctx, (AllocaInst *)o.p);
  return true;
}

bool EmitCtx::store(YogaContext const &ctx, ExprOut &o)
{
  if (!o.v) return ctx.logError("Store failed: no v");
  if (!o.p) {
    o.p = mkAlloca(ctx, o.t->llType);
  }
  builder->CreateStore(o.v, o.p);
  return true;
}

bool EmitCtx::setv(YogaContext const &ctx, ExprOut &o, Value *v)
{
  if (!o.t) throw runtime_error("setv: no t");
  if (o.v) throw runtime_error("setv: already has v");
  if (o.t->llType != v->getType()) {
    throw runtime_error("lltype mismatch " + repr(o.t->llType) + " != " + repr(v->getType()));
  }

  o.v = v;
  if (o.p) {
    if (!store(ctx, o)) return false;
  }
  return true;
}

bool EmitCtx::settv(YogaContext const &ctx, ExprOut &o, YogaType *t, Value *v)
{
  if (o.t != nullptr && o.t != t) {
    throw runtime_error("Tried to change ExprOut type");
  }
  o.t = t;
  return setv(ctx, o, v);
}

bool EmitCtx::setp(YogaContext const &ctx, ExprOut &o, Value *p)
{
  if (!o.t) throw runtime_error("setp: no t");
  if (o.v) throw runtime_error("setp: already has v");
  if (o.p) throw runtime_error("setp: already has p");
  if (o.t->llType != p->getType()->getPointerElementType()) {
    throw runtime_error("lltype mismatch " + repr(o.t->llType) + " != *" + repr(p->getType()));
  }

  o.p = p;
  return true;
}

bool EmitCtx::settp(YogaContext const &ctx, ExprOut &o, YogaType *t, Value *v)
{
  if (o.t != nullptr && o.t != t) {
    throw runtime_error("Tried to change ExprOut type");
  }
  o.t = t;
  return setp(ctx, o, v);
}

[[nodiscard]] bool EmitCtx::setextra(YogaContext const &ctx, ExprOut &o, string const &name, Value *v)
{
  if (!o.extra) o.extra = make_shared<map<string, Value *>>();
  (*o.extra)[name] = v;
  return true;
}

Value *EmitCtx::getv(ExprOut &o)
{
  if (!o.v) {
    if (o.p) {
      auto pname = o.p->getName();
      if (!pname.empty()) {
        o.v = builder->CreateLoad(o.p, pname + ".p2v");
      } else {
        o.v = builder->CreateLoad(o.p);
      }
    }
  }
  return o.v;
}

Value *EmitCtx::getp(ExprOut &o)
{
  if (!o.t) {
    throw runtime_error("getp: no type defined");
  }
  if (!o.p) {
    o.p = builder->CreateAlloca(o.t->llType, nullptr);
    if (o.v) {
      builder->CreateStore(o.v, o.p);
    }
  }
  return o.p;
}

Value *EmitCtx::getextra(YogaContext const &ctx, ExprOut &o, string const &name)
{
  if (!o.extra) return nullptr;
  return (*o.extra)[name];
}


bool EmitCtx::getMatrixElem(YogaContext const &ctx, ExprOut &o, ExprOut &mat, int ri, int ci)
{
  if (!alloc(ctx, mat)) return false;

  auto t = mat.t->asMatrix();
  if (!t) throw runtime_error("getMatrixElem: no t->asMatrix");
  if (ri < 0 || ri >= t->rows || ci < 0 || ci >= t->cols) throw runtime_error("getMatrixElem: index error");

  o.p = builder->CreateStructGEP(mat.p, ri + ci * t->rows);
  o.t = t->elementType;
  return true;
}

bool EmitCtx::getMatrixElems(YogaContext const &ctx, vector<ExprOut> &os, ExprOut &mat)
{
  auto mtm = mat.t->asMatrix();
  if (!mtm) return ctx.logError("getMatrixElems: no t->asMatrix");

  os.resize(mtm->rows * mtm->cols);

  for (int ci=0; ci < mtm->cols; ci++) {
    for (int ri=0; ri < mtm->rows; ri++) {
      if (!getMatrixElem(ctx, os[ri + ci*mtm->rows], mat, ri, ci)) return false;
    }
  }
  return true;
}


bool EmitCtx::setMatrixElem(YogaContext const &ctx, ExprOut &mat, int ri, int ci, ExprOut &rv)
{
  ExprOut el;
  if (!getMatrixElem(ctx, el, mat, ri, ci)) return false;
  if (!assign(ctx, el, rv)) return false;
  return true;
}

bool EmitCtx::setMatrixElem(YogaContext const &ctx, ExprOut &mat, int ri, int ci, Value *rv)
{
  ExprOut el;
  if (!getMatrixElem(ctx, el, mat, ri, ci)) return false;
  if (!setv(ctx, el, rv)) return false;
  return true;
}


bool EmitCtx::getStructElem(YogaContext const &ctx, ExprOut &o, ExprOut &st, StructMember const *memInfo)
{
  if (!alloc(ctx, st)) return false;
  
  o.p = builder->CreateStructGEP(st.p, memInfo->llIdx);
  o.t = memInfo->memberType;
  return true;
}

void * EmitCtx::lookup(YogaContext const &ctx, string const &name)
{
  auto &mainJD = es.createJITDylib("main");
  auto sym = es.lookup({&mainJD}, mangle(name));
  if (!sym) {
    ctx.logError("emit: Looking up "s + name + " failed: "s + repr(sym.takeError()));
    return nullptr;
  }
  return reinterpret_cast<void *>(sym->getAddress());
}


Function *EmitCtx::startFunction(FunctionType *fTy, string const &implName)
{
  if (curFunc) {
    throw runtime_error("EmitCtx::startFunction: Already inside function");
  }

  exprOutsByCseKey.clear();
  constantPool.clear();
  argPtrs.clear();

  curFunc = m->getOrInsertFunction(implName, fTy);
  auto f = reinterpret_cast<Function *>(curFunc.getCallee());

  //curFunc = Function::Create(fTy, Function::ExternalLinkage, implName, m);
  BasicBlock *bb = BasicBlock::Create(l, dotJoin(implName, "entry"), f);
  if (endsWith(bb->getName(), ".entry1")) {
    cerr << "Warning: Name suggesting a duplicate function "s + string(bb->getName()) + "\n";
  }
  builder->SetInsertPoint(bb);
  entryBuilder->SetInsertPoint(&f->getEntryBlock(), f->getEntryBlock().begin());
  return f;
}

llvm::Value *EmitCtx::getYogaArg(int idx1, int idx2, Type *argType)
{
  if (!curFunc) throw runtime_error("EmitCtx::getYogaArg: not in a function");

  auto argBlock = &reinterpret_cast<Function *>(curFunc.getCallee())->arg_begin()[idx1];
  auto ap = builder->CreateConstInBoundsGEP1_32(
    Type::getInt8PtrTy(l),
    argBlock,
    idx2);
  auto av = builder->CreateLoad(ap);
  auto argPtrType = argType->getPointerTo();
  auto avt = builder->CreateBitCast(av, argPtrType);
  return avt;
}

bool EmitCtx::endFunction()
{
  if (!curFunc) {
    throw runtime_error("EmitCtx::endFunction: Not inside function");
  }
  if (fpm) {
    FunctionAnalysisManager fam;
    fpm->run(*reinterpret_cast<Function *>(curFunc.getCallee()), fam);
  }

  bool errs = false;
#if 1
  errs = verifyFunction(*reinterpret_cast<Function *>(curFunc.getCallee()), &dumpStream);
#else
  string verifyErr;
  {
    raw_string_ostream os(verifyErr);
    errs = verifyFunction(*curFunc, &os);
  }
#endif
  if (errs) {
    dumpStream << "Verify function("s + string(curFunc.getCallee()->getName()) + ") failed\n"s;
  }
  dumpStream.flush();
  if (!dumpStr.empty()) {
    cerr << dumpStr;
    dumpStr.clear();
  }
  curFunc = nullptr;
  return true;
}


void
EmitCtx::dumpLlvmInsns(ostream &os)
{
  string s;
  {
    raw_string_ostream ros(s);
    es.dump(ros);
    ros.flush();
  }
  os << "es dump:\n" << s;
}

Value *EmitCtx::cast(Value *src, Type *dstTy, bool isSigned)
{
  Type *srcTy = src->getType();
  if (srcTy == dstTy) return src;

  if (dstTy->isFloatingPointTy()) {
    if (srcTy->isFloatingPointTy()) {
      return builder->CreateFPCast(src, dstTy);
    }
    if (srcTy->isIntegerTy()) {
      if (isSigned) {
        return builder->CreateSIToFP(src, dstTy);
      }
      else {
        return builder->CreateUIToFP(src, dstTy);
      }
    }
  }
  else if (dstTy->isIntegerTy()) {
    if (srcTy->isFloatingPointTy()) {
      if (isSigned) {
        return builder->CreateFPToSI(src, dstTy);
      }
      else {
        return builder->CreateFPToUI(src, dstTy);
      }
    }
    else if (srcTy->isIntegerTy()) {
      if (isSigned) {
        return builder->CreateSExtOrTrunc(src, dstTy);
      }
      else {
        return builder->CreateZExtOrTrunc(src, dstTy);
      }
    }
  }
  throw runtime_error("No cast from " + repr(srcTy) + " to " + repr(dstTy));
  return nullptr;
}


bool EmitCtx::assign(YogaContext const &ctx, ExprOut &dst, ExprOut &src)
{
  if (dst.v) return ctx.logError("assign: dst already has value");
  if (!dst.t) dst.t = src.t;

  if (dst.t == src.t || dst.t->clsName == src.t->clsName) {
    if (!load(ctx, src)) return false;
    return setv(ctx, dst, src.v);
  }
  auto dstTy = dst.t->llType;

  if (!load(ctx, src)) return false;

  if (dst.t->isReal()) {
    if (src.t->isBool()) {
      return setv(ctx, dst, builder->CreateUIToFP(src.v, dstTy));
    }
    else if (src.t->isIntegral()) {
      if (src.t->isSigned()) {
        return setv(ctx, dst, builder->CreateSIToFP(src.v, dstTy));
      }
      else {
        return setv(ctx, dst, builder->CreateUIToFP(src.v, dstTy));
      }
    }
    else if (src.t->isReal()) {
      return setv(ctx, dst, builder->CreateFPCast(src.v, dstTy));
    }
  }
  else if (dst.t->isBool()) {
    if (src.t->isReal()) {
      return setv(ctx, dst, builder->CreateFCmpOGE(
        src.v,
        getv(emitDouble(0.5))));
    }
  }
  else if (dst.t->isIntegral()) {
    /*
      We're missing some information here: whether the type is unsigned.
      We assume int1 is unsigned, and everything else is signed.
     */
    if (src.t->isBool()) {
      return setv(ctx, dst, builder->CreateZExt(src.v, dstTy));
    }
    else if (src.t->isIntegral()) {
      // This sort of does the wrong thing converting unsigned Yoga types.
      if (dst.t->isSigned()) {
        return setv(ctx, dst, builder->CreateSExtOrTrunc(src.v, dstTy));
      }
      else {
        return setv(ctx, dst, builder->CreateZExtOrTrunc(src.v, dstTy));
      }
    }
    else if (src.t->isReal()) {
      if (dst.t->isSigned()) {
        return setv(ctx, dst, builder->CreateFPToSI(src.v, dstTy));
      }
      else {
        return setv(ctx, dst, builder->CreateFPToUI(src.v, dstTy));
      }
    }
  }
  return ctx.logError("assign: Can't convert src type "s + repr_type(src.t) + " to " + repr_type(dst.t));
}

bool EmitCtx::assignPluseqInplace(YogaContext const &ctx, ExprOut &dst, ExprOut &src)
{
  if (dst.v) return ctx.logError("assignPluseqInplace: dst already has v");
  if (!dst.p) return ctx.logError("assignPluseqInplace: dst has no p");

  if (dst.t == src.t || dst.t->clsName == src.t->clsName) {
    if (!load(ctx, src)) return false;

    if (dst.t->isReal()) {
      auto oldDstv = builder->CreateLoad(dst.p);
      auto newDstv = builder->CreateFAdd(src.v, oldDstv);
      builder->CreateStore(newDstv, dst.p);
      return true;
    }
    if (auto dtm = dst.t->asMatrix()) {
      for (auto ci = 0; ci < dtm->cols; ci++) {
        for (auto ri = 0; ri < dtm->rows; ri++) {
          auto oldDstv = builder->CreateLoad(builder->CreateStructGEP(dst.p, ri + ci * dtm->rows));
          auto srcv = builder->CreateLoad(builder->CreateStructGEP(src.p, ri + ci * dtm->rows));
          auto newDstv = builder->CreateFAdd(srcv, oldDstv);
          builder->CreateStore(newDstv, builder->CreateStructGEP(dst.p, ri + ci * dtm->rows));
        }
      }
      return true;
    }

  }
  return ctx.logError("assignPluseqInplace: Can't convert src type "s + repr_type(src.t) + " to " + repr_type(dst.t));
}


Value *EmitCtx::emitDgemm(YogaContext const &ctx, 
    Value *order, Value *transA, Value *transB, 
    Value *m, Value *n, Value *k,
    Value *alpha,
    Value *a, Value *lda,
    Value *b, Value *ldb,
    Value *beta,
    Value *c, Value *ldc)
{
  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt32Ty(l), Type::getInt32Ty(l), Type::getInt32Ty(l),
    Type::getInt32Ty(l), Type::getInt32Ty(l), Type::getInt32Ty(l),
    Type::getDoubleTy(l),
    Type::getDoublePtrTy(l), Type::getInt32Ty(l),
    Type::getDoublePtrTy(l), Type::getInt32Ty(l),
    Type::getDoubleTy(l),
    Type::getDoublePtrTy(l), Type::getInt32Ty(l)
  }, false);

  auto func = this->m->getOrInsertFunction("cblas_dgemm"s, funcTy);
  if (!func) return ctx.logErrorNull("emit: Can't link in cblas_dgemm function");

  return builder->CreateCall(func, {
    order, transA, transB,
    m, n, k,
    alpha,
    a, lda,
    b, ldb,
    beta,
    c, ldc
  });
}

Value *EmitCtx::emitDcopy(YogaContext const &ctx, 
    Value *n, Value *x, Value *incX, Value *y, Value *incY)
{
  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt32Ty(l),
    Type::getDoublePtrTy(l),
    Type::getInt32Ty(l),
    Type::getDoublePtrTy(l),
    Type::getInt32Ty(l)
  }, false);

  auto func = m->getOrInsertFunction("cblas_dcopy"s, funcTy);
  if (!func) return ctx.logErrorNull("emit: Can't link in cblas_dcopy function");

  return builder->CreateCall(func, {
    n, x, incX, y, incY
  });
}

Value *EmitCtx::emitDaxpy(YogaContext const &ctx, 
    Value *n, Value *alpha, Value *x, Value *incX, Value *y, Value *incY)
{
  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt32Ty(l),
    Type::getDoubleTy(l),
    Type::getDoublePtrTy(l),
    Type::getInt32Ty(l),
    Type::getDoublePtrTy(l),
    Type::getInt32Ty(l)
  }, false);

  auto func = m->getOrInsertFunction("cblas_daxpy"s, funcTy);
  if (!func) return ctx.logErrorNull("emit: Can't link in cblas_daxpy function");

  return builder->CreateCall(func, {
    n, alpha, x, incX, y, incY
  });
}

Value *EmitCtx::asDoublePtr(Value *p)
{
  return builder->CreateBitCast(p, Type::getDoublePtrTy(l));
}


Value *EmitCtx::asU8Ptr(Value *p)
{
  return builder->CreateBitCast(p, Type::getInt8PtrTy(l));
}


Value *EmitCtx::allocArgsPad(YogaContext const &ctx, size_t size)
{
  auto t = ArrayType::get(Type::getInt8PtrTy(l), size);
  auto pad = mkAlloca(ctx, t);
  return pad;
}


ExprOut &EmitCtx::emitInt(int x)
{
  ExprOut ret(reg->s32Type);
  ret.v = ConstantInt::get(ret.t->llType, x);
  return constantPool.emplace_back(ret);
}


ExprOut &EmitCtx::emitDouble(double x)
{
  ExprOut ret(reg->rType);
  ret.v = ConstantFP::get(ret.t->llType, APFloat(x));

  return constantPool.emplace_back(ret);
}

ExprOut &EmitCtx::emitBool(bool x)
{
  ExprOut ret(reg->boolType);
  ret.v = ConstantInt::get(ret.t->llType, x ? 1 : 0);
  return constantPool.emplace_back(ret);
}


Value *EmitCtx::emitTypedPtr(YogaContext const &ctx, Type *ty, void *ptr)
{
  auto ptrAsInt = ConstantInt::get(Type::getInt64Ty(l), (U64)ptr);
  return builder->CreateBitOrPointerCast(ptrAsInt, ty);
}

Value *EmitCtx::emitRpRpRp(
  string const &name,
  Value *rp,
  Value *a0p,
  Value *a1p)
{
  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getDoublePtrTy(l),
    Type::getDoublePtrTy(l),
    Type::getDoublePtrTy(l)
  }, false);
  auto func = m->getOrInsertFunction(name, funcTy);
  if (!func) return nullptr;
  return builder->CreateCall(func, {
    asDoublePtr(rp),
    asDoublePtr(a0p),
    asDoublePtr(a1p)
  });
}

extern "C" void callClosure1(std::function<void(void *)> *f, void *a)
{
  (*f)(a);
}

bool EmitCtx::emitCallo(YogaContext const &ctx, ExprOut &a, std::function<void(void *)> const &f)
{
  auto fdup = new std::function<void(void *)>(f); // permanent leak

  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt8PtrTy(l),
    Type::getDoublePtrTy(l)
  }, false);
  auto func = m->getOrInsertFunction("callClosure1", funcTy);
  if (!func) return ctx.logError("Couldn't find callClosure1");

  if (!alloc(ctx, a)) return false;

  builder->CreateCall(func, {
    emitTypedPtr(ctx, Type::getInt8PtrTy(l), fdup),
    asDoublePtr(a.p),
  });
  return true;
}


extern "C" void callClosure2(std::function<void(void *, void *)> *f, void *a, void *b)
{
  (*f)(a, b);
}

bool EmitCtx::emitCalloi(YogaContext const &ctx, ExprOut &a, ExprOut &b, std::function<void(void *, void *)> const &f)
{
  auto fdup = new std::function<void(void *, void *)>(f); // permanent leak

  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt8PtrTy(l),
    Type::getDoublePtrTy(l),
    Type::getDoublePtrTy(l)
  }, false);
  auto func = m->getOrInsertFunction("callClosure2", funcTy);
  if (!func) return ctx.logError("Couldn't find callClosure2");

  if (!alloc(ctx, a)) return false;
  if (!store(ctx, b)) return false;

  builder->CreateCall(func, {
    emitTypedPtr(ctx, Type::getInt8PtrTy(l), fdup),
    asDoublePtr(a.p),
    asDoublePtr(b.p),
  });
  return true;
}


extern "C" void callClosure3(std::function<void(void *, void *, void *)> *f, void *a, void *b, void *c)
{
  (*f)(a, b, c);
}

bool EmitCtx::emitCalloii(YogaContext const &ctx, ExprOut &a, ExprOut &b, ExprOut &c, std::function<void(void *, void *, void *)> const &f)
{
  auto fdup = new std::function<void(void *, void *, void *)>(f); // permanent leak

  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt8PtrTy(l),
    Type::getDoublePtrTy(l),
    Type::getDoublePtrTy(l),
    Type::getDoublePtrTy(l)
  }, false);
  auto func = m->getOrInsertFunction("callClosure3", funcTy);
  if (!func) return ctx.logError("Couldn't find callClosure3");

  if (!alloc(ctx, a)) return false;
  if (!store(ctx, b)) return false;
  if (!store(ctx, c)) return false;

  builder->CreateCall(func, {
    emitTypedPtr(ctx, Type::getInt8PtrTy(l), fdup),
    asDoublePtr(a.p),
    asDoublePtr(b.p),
    asDoublePtr(c.p),
  });

  return true;
}

extern "C" void callLogPtr(std::function<void(void *)> *f, void *a)
{
  (*f)(a);
}

bool EmitCtx::emitLogPtr(YogaContext const &ctx, Value *a, std::function<void(void *)> const &f)
{
  auto fdup = new std::function<void(void *)>(f); // permanent leak

  auto funcTy = FunctionType::get(Type::getVoidTy(l), {
    Type::getInt8PtrTy(l),
    Type::getInt8PtrTy(l),
  }, false);
  auto func = m->getOrInsertFunction("callLogPtr", funcTy);
  if (!func) return ctx.logError("Couldn't find callLogPtr");

  builder->CreateCall(func, {
    emitTypedPtr(ctx, Type::getInt8PtrTy(l), fdup),
    asU8Ptr(a),
  });

  return true;
}


bool EmitCtx::emitCos(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  if (!load(ctx, a)) return false;
  auto rv = builder->CreateUnaryIntrinsic(Intrinsic::cos, a.v);
  rv->setFast(true);
  o.v = rv;
  return true;
}

bool EmitCtx::emitSin(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  if (!load(ctx, a)) return false;
  auto rv = builder->CreateUnaryIntrinsic(Intrinsic::sin, a.v);
  rv->setFast(true);
  o.v = rv;
  return true;
}

bool EmitCtx::emitSqrt(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  if (!load(ctx, a)) return false;
  auto rv = builder->CreateUnaryIntrinsic(Intrinsic::sqrt, a.v);
  rv->setFast(true);
  o.v = rv;
  return true;
}


bool EmitCtx::emitFabs(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  if (!load(ctx, a)) return false;
  auto rv = builder->CreateUnaryIntrinsic(Intrinsic::fabs, a.v);
  rv->setFast(true);
  o.v = rv;
  return true;
}

pair<Value *, Value *> EmitCtx::loadCplx(YogaContext const &ctx, ExprOut &cplx)
{
  auto realPtr = builder->CreateStructGEP(getp(cplx), 0);
  auto imagPtr = builder->CreateStructGEP(getp(cplx), 1);

  return make_pair(
    builder->CreateLoad(realPtr), 
    builder->CreateLoad(imagPtr));
}

bool EmitCtx::storeCplx(YogaContext const &ctx, ExprOut &cplx, Value *real, Value *imag)
{
  if (!alloc(ctx, cplx)) return false;
  auto realPtr = builder->CreateStructGEP(getp(cplx), 0);
  auto imagPtr = builder->CreateStructGEP(getp(cplx), 1);

  builder->CreateStore(real, realPtr);
  builder->CreateStore(imag, imagPtr);
  return true;
}

bool EmitCtx::emitStructref(YogaContext const &ctx, ExprOut &o, ExprOut &a, StructMember *memberInfo)
{
  return setp(ctx, o, builder->CreateStructGEP(
    getp(a),
    memberInfo->llIdx));
}

bool EmitCtx::emitAdd(YogaContext const &ctx, ExprOut &o, ExprOut &a0, ExprOut &a1)
{
  if (!a0 || !a1) return false;

  if (a0.t->isReal() && a1.t->isReal()) {
    ExprOut otmp;
    if (!settv(ctx, otmp, ctx.reg->rType, builder->CreateFAdd(getv(a0), getv(a1)))) return false;
    return assign(ctx, o, otmp);
  }
  if (a0.t->isBool() && a1.t->isBool()) {
    ExprOut otmp;
    if (!settv(ctx, otmp, ctx.reg->boolType, builder->CreateOr(getv(a0), getv(a1)))) return false;
    return assign(ctx, o, otmp);
  }
  if (a0.t->isIntegral() && a1.t->isIntegral()) {
    ExprOut otmp;
    // FIXME: type for settv should be larger of a0.t and a1.t
    if (!settv(ctx, otmp, a0.t, builder->CreateAdd(getv(a0), getv(a1)))) return false;
    return assign(ctx, o, otmp);
  }
  if (a0.t->isStruct() && a0.t == a1.t && a0.t == o.t) {
    auto a0ts = a0.t->asStruct();

    for (auto &memInfo : a0ts->members) {
      ExprOut a0el(memInfo->memberType), a1el(memInfo->memberType), oel(memInfo->memberType);

      if (!emitStructref(ctx, a0el, a0, memInfo)) return false;
      if (!emitStructref(ctx, a1el, a1, memInfo)) return false;
      if (!emitStructref(ctx, oel, o, memInfo)) return false;
      if (!emitAdd(ctx, oel, a0el, a1el)) return false;
    }
    return true;
  }
  if (a0.t->isComplex() && a1.t->isComplex()) {
    auto [a0Real, a0Imag] = loadCplx(ctx, a0);
    auto [a1Real, a1Imag] = loadCplx(ctx, a1);

    auto oReal = builder->CreateFAdd(a0Real, a1Real);
    auto oImag = builder->CreateFAdd(a0Imag, a1Imag);

    storeCplx(ctx, o, oReal, oImag);
    return true;
  }

  if (a0.t->isMatrix() && a1.t->isMatrix()) {
    auto a0tm = a0.t->asMatrix();
    auto a1tm = a1.t->asMatrix();
    if (a0tm->rows > 0 && a0tm->cols > 0 && 
        a1tm->rows > 0 && a1tm->cols > 0 && 
        a0tm->rows == a1tm->rows && 
        a0tm->cols == a1tm->cols) {

      if (!alloc(ctx, o)) return false;

      for (int ci=0; ci < a0tm->cols; ci++) {
        for (int ri=0; ri < a0tm->rows; ri++) {
          ExprOut a0el, a1el, rel;
          if (!getMatrixElem(ctx, a0el, a0, ri, ci)) return false;
          if (!getMatrixElem(ctx, a1el, a1, ri, ci)) return false;
          if (!getMatrixElem(ctx, rel, o, ri, ci)) return false;
          if (!setv(ctx, rel, builder->CreateFAdd(getv(a0el), getv(a1el)))) return false;
        }
      }
      return true;
    }
  }

  if (a0.t->isString() && a1.t->isString() && o.t->isString()) {
    auto yogicStrcatFunc = m->getOrInsertFunction("yogic_strcat"s, yogicStrcatTy);
    auto strcatRet = builder->CreateCall(yogicStrcatFunc, {
      getv(a0), getv(a1),
      getv(argPtrs["mem"])
    });
    return setv(ctx, o, strcatRet);
  }

  return ctx.logError("emit: Invalid types for +"s);
}

bool EmitCtx::emitMul(YogaContext const &ctx, ExprOut &o, ExprOut &a0, ExprOut &a1)
{
  if (!a0 || !a1) return false;

  if (a0.t->isReal() && a1.t->isReal()) {
    ExprOut otmp;
    if (!settv(ctx, otmp, ctx.reg->rType, builder->CreateFMul(getv(a0), getv(a1)))) return false;
    return assign(ctx, o, otmp);
  }

  if (a0.t->isComplex() && a1.t->isComplex() && o.t->isComplex()) {
    auto [a0Real, a0Imag] = loadCplx(ctx, a0);
    auto [a1Real, a1Imag] = loadCplx(ctx, a1);

    auto oReal = builder->CreateFSub(
      builder->CreateFMul(a0Real, a1Real),
      builder->CreateFMul(a0Imag, a1Imag));

    auto oImag = builder->CreateFAdd(
      builder->CreateFMul(a0Real, a1Imag),
      builder->CreateFMul(a0Imag, a1Real));

    storeCplx(ctx, o, oReal, oImag);

    return true;    
  }

  if (a0.t->isBool() && a1.t->isBool() && o.t->isBool()) {
    if (!setv(ctx, o, builder->CreateAnd(getv(a0), getv(a1)))) return false;
    return true;
  }

  if (a0.t->isBool() && a1.t->isReal()) {
    swap(a0, a1);
  }
  if (a0.t->isReal() && a1.t->isBool() && o.t->isBool()) {
    ExprOut a0Bool(ctx.reg->boolType);
    if (!assign(ctx, a0Bool, a0)) return false;
    if (!setv(ctx, o, builder->CreateAnd(getv(a0Bool), getv(a1)))) return false;
    return true;
  }
  if (a0.t->isReal() && a1.t->isString() && o.t->isString()) {
    ExprOut a0Bool(ctx.reg->boolType);
    if (!assign(ctx, a0Bool, a0)) return false;
    if (!setv(ctx, o, builder->CreateSelect(
        getv(a0Bool),
        getv(a1),
        Constant::getNullValue(a1.t->llType)))) return false;
    return true;
  }

  if (a0.t->isIntegral() && a1.t->isReal()) {
    swap(a0, a1);
  }
  if (a0.t->isReal() && a1.t->isIntegral()) {
    ExprOut a1Real(ctx.reg->rType);
    if (!assign(ctx, a1Real, a1)) return false;
    ExprOut oReal(ctx.reg->rType);
    if (!setv(ctx, oReal, builder->CreateFMul(getv(a0), getv(a1Real)))) return false;
    if (!assign(ctx, o, oReal)) return false;
    return true;
  }


  if (a0.t->isStruct() && a1.t->isReal()) {
    swap(a0, a1);
  }
  if (a0.t->isReal() && a1.t->isStruct() && o.t == a1.t) {
    // Elementwise multiplication by scalar
    auto a1ts = a1.t->asStruct();

    for (auto &memInfo : a1ts->members) {
      ExprOut a1el(memInfo->memberType), oel(memInfo->memberType);

      if (!emitStructref(ctx, a1el, a1, memInfo)) return false;
      if (!emitStructref(ctx, oel, o, memInfo)) return false;
      if (!emitMul(ctx, oel, a0, a1el)) return false;
    }
    return true;
  }
  
  if (a0.t->isMatrix() && a1.t->isMatrix() && o.t->isMatrix()) {
    auto a0tm = a0.t->asMatrix();
    auto a1tm = a1.t->asMatrix();
    auto otm = o.t->asMatrix();
    // WRITEME: handle large or dynamic matrices
    if (a0tm->rows > 0 && a0tm->cols > 0 && 
        a1tm->rows > 0 && a1tm->cols > 0 && 
        a0tm->cols == a1tm->rows &&
        a0tm->rows == otm->rows &&
        a1tm->cols == otm->cols) {

      vector<ExprOut> a0elems;
      if (!getMatrixElems(ctx, a0elems, a0)) return false;
      vector<ExprOut> a1elems;
      if (!getMatrixElems(ctx, a1elems, a1)) return false;

      for (int ri=0; ri < a0tm->rows; ri++) {
        for (int ci=0; ci < a1tm->cols; ci++) {
          Value *accum = nullptr;
          for (int ii=0; ii < a0tm->cols; ii++) {
            //ExprOut a0el, a1el;
            //if (!e.getMatrixElem(ctx, a0el, a0, ri, ii)) return false;
            //if (!e.getMatrixElem(ctx, a1el, a1, ii, ci)) return false;
            auto prodel = builder->CreateFMul(getv(a0elems[ri + ii*a0tm->rows]), getv(a1elems[ii + ci*a1tm->rows]));
            accum = accum ? builder->CreateFAdd(accum, prodel) : prodel;
          }
          ExprOut oel;
          if (!getMatrixElem(ctx, oel, o, ri, ci)) return false;
          if (!setv(ctx, oel, accum)) return false;
        }
      }
      return true;
    }
  }

  if (a0.t->isMatrix() && a1.t->isReal()) {
    swap(a0, a1);
  }

  if (a0.t->isReal() && a1.t->isMatrix()) {
    auto a1tm = a1.t->asMatrix();
    if (a1tm->rows > 0 && a1tm->cols > 0 && 
        a0.t == a1tm->elementType) {

      if (!alloc(ctx, o)) return false;

      for (int ri=0; ri < a1tm->rows; ri++) {
        for (int ci=0; ci < a1tm->cols; ci++) {
          ExprOut a1el;
          if (!getMatrixElem(ctx, a1el, a1, ri, ci)) return false;

          ExprOut oel;
          if (!getMatrixElem(ctx, oel, o, ri, ci)) return false;
          if (!setv(ctx, oel, builder->CreateFMul(getv(a0), getv(a1el)))) return false;
        }
      }
      return true;
    }
  }

  return ctx.logError("emit: Invalid types for *: "s + repr_type(a0.t) + ", " + repr_type(a1.t) + " => " + repr_type(o.t));
}

bool EmitCtx::emitConjugate(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  if (auto atm = a.t->asMatrix()) {
    if (atm->rows == 1 || atm->cols == 1) {
      if (!setp(ctx, o, getp(a))) return false;
      return true;
    }
    else {
      vector<ExprOut> aelems, oelems;
      if (!getMatrixElems(ctx, aelems, a)) return false;
      if (!getMatrixElems(ctx, oelems, o)) return false;

      for (int ri=0; ri < atm->rows; ri++) {
        for (int ci=0; ci < atm->cols; ci++) {
          if (!emitConjugate(ctx, oelems[ci + ri * atm->cols], aelems[ri + ci*atm->rows])) return false;
        }
      }
      return true;
    }
  }
  else if (a.t->isComplex()) {
    auto [aReal, aImag] = loadCplx(ctx, a);
    storeCplx(ctx, o, aReal, builder->CreateFNeg(aImag));
    return true;
  }
  else if (a.t->isScalar()) {
    if (!assign(ctx, o, a)) return false;
    return true;
  }
  return ctx.logError("emit: Invalid types for conjugate: "s + repr_type(a.t) + " => " + repr_type(o.t));
}

bool EmitCtx::emitConvDoubleBool(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  return setv(ctx, o,
    builder->CreateFCmpOGE(
      getv(a),
      getv(emitDouble(0.5))));
}

bool EmitCtx::emitNormSq(YogaContext const &ctx, ExprOut &o, ExprOut &a)
{
  Value *sumsq{nullptr};
  auto accum = [&sumsq, this](Value *vsq) {
    assert(vsq->getType()->isDoubleTy());
    if (sumsq) {
      sumsq = builder->CreateFAdd(sumsq, vsq);
    }
    else {
      sumsq = vsq;
    }
  };

  if (auto atm = a.t->asMatrix()) {
    for (int ci=0; ci<atm->cols; ci++) {
      for (int ri=0; ri<atm->rows; ri++) {
        ExprOut ael;
        if (!getMatrixElem(ctx, ael, a, ri, ci)) return false;
        ExprOut aelsq(ctx.reg->rType);
        emitNormSq(ctx, aelsq, ael);
        accum(getv(aelsq));
      }
    }
  }
  else if (auto ats = a.t->asStruct()) {
    for (auto &memInfo : ats->members) {
      ExprOut ael(memInfo->memberType);
      emitStructref(ctx, ael, a, memInfo);
      ExprOut aelsq(ctx.reg->rType);
      emitNormSq(ctx, aelsq, ael);
      accum(getv(aelsq));
    }
  }
  else if (a.t->isScalar() || a.t->isBool()) {
    ExprOut aReal(ctx.reg->rType);
    if (!assign(ctx, aReal, a)) return false;
    accum(builder->CreateFMul(getv(aReal), getv(aReal)));
  }
  else {
    // THINK. But doing nothing is appropriate for many types
  }

  if (!sumsq) {
    sumsq = getv(emitDouble(0.0));
  }
  if (!setv(ctx, o, sumsq)) return false;
  return true;
}

bool EmitCtx::emitCmp(YogaContext const &ctx, string const &cmp, ExprOut &o, ExprOut &a0, ExprOut &a1)
{
  if (!o.t) o.t = ctx.reg->boolType;
  if (a0.t->isReal() && a1.t->isReal() && o.t->isBool()) {
    if (cmp == ">=") return setv(ctx, o, builder->CreateFCmpOGE(getv(a0), getv(a1)));
    if (cmp == ">") return setv(ctx, o, builder->CreateFCmpOGT(getv(a0), getv(a1)));
    if (cmp == "<=") return setv(ctx, o, builder->CreateFCmpOLE(getv(a0), getv(a1)));
    if (cmp == "<") return setv(ctx, o, builder->CreateFCmpOLT(getv(a0), getv(a1)));
    if (cmp == "==") return setv(ctx, o, builder->CreateFCmpOEQ(getv(a0), getv(a1)));
    if (cmp == "!=") return setv(ctx, o, builder->CreateFCmpONE(getv(a0), getv(a1)));
  }

  if (a0.t->isIntegral() && a1.t->isIntegral() && o.t->isBool()) {
    if (a0.t->isSigned() && a1.t->isSigned()) {
      if (cmp == ">=") return setv(ctx, o, builder->CreateICmpSGE(getv(a0), getv(a1)));
      if (cmp == ">") return setv(ctx, o, builder->CreateICmpSGT(getv(a0), getv(a1)));
      if (cmp == "<=") return setv(ctx, o, builder->CreateICmpSLE(getv(a0), getv(a1)));
      if (cmp == "<") return setv(ctx, o, builder->CreateICmpSLT(getv(a0), getv(a1)));
    }
    else if (!a0.t->isSigned() && !a1.t->isSigned()) {
      if (cmp == ">=") return setv(ctx, o, builder->CreateICmpUGE(getv(a0), getv(a1)));
      if (cmp == ">") return setv(ctx, o, builder->CreateICmpUGT(getv(a0), getv(a1)));
      if (cmp == "<=") return setv(ctx, o, builder->CreateICmpULE(getv(a0), getv(a1)));
      if (cmp == "<") return setv(ctx, o, builder->CreateICmpULT(getv(a0), getv(a1)));
    }
    if (cmp == "==") return setv(ctx, o, builder->CreateICmpEQ(getv(a0), getv(a1)));
    if (cmp == "!=") return setv(ctx, o, builder->CreateICmpNE(getv(a0), getv(a1)));
  }

  if (a0.t->isString() && a1.t->isString()) {
    if (cmp == "==") {
      auto funcTy = FunctionType::get(Type::getInt1Ty(l), {
        Type::getInt8PtrTy(l),
        Type::getInt8PtrTy(l)
      }, false);
      auto func = m->getOrInsertFunction("yogic_string_eq"s, funcTy);
      if (!func) return ctx.logError("emit: Can't link in yogic_string_eq function");
      return setv(ctx, o, builder->CreateCall(func, { getv(a0), getv(a1) }));
    }
  }

  if (a0.t->isStruct() && a1.t == a0.t) {
    if (cmp == "==") {
      auto ts = a0.t->asStruct();

      Value *accum = nullptr;
      for (auto &mi : ts->members) {
        ExprOut a0el, a1el, cmpel;
        if (!getStructElem(ctx, a0el, a0, mi)) return false;
        if (!getStructElem(ctx, a1el, a1, mi)) return false;
        if (!emitCmp(ctx, cmp, cmpel, a0el, a1el)) return false;

        accum = accum ? builder->CreateAnd(accum, getv(cmpel)) : getv(cmpel);
      }
      return setv(ctx, o, accum ? accum : getv(emitBool(true)));
    }
  }

  return ctx.logError("emit: Invalid types for "s + cmp + ": " + repr_type(a0.t) + ", " + repr_type(a1.t) + " => " + repr_type(o.t));
}

bool EmitCtx::emitSetInit(YogaContext const &ctx, ExprOut &o)
{
  if (!o.t) o.t = ctx.reg->setType;
  if (!o.t->isSet()) return ctx.logError("emitSetInit: not a set "s + repr_type(o.t));
  return setv(ctx, o, ConstantPointerNull::get(::cast<PointerType>(o.t->llType)));
}


Value *EmitCtx::emitAlloc(YogaContext const &ctx, size_t size)
{
  if (!argPtrs["mem"]) {
    return ctx.logErrorNull("emitAlloc: no mem param");
  }

  auto yogicAllocFunc = m->getOrInsertFunction("yogic_alloc"s, yogicAllocTy);
  return builder->CreateCall(yogicAllocFunc, {
    ConstantInt::get(Type::getInt64Ty(l), size),
    getv(argPtrs["mem"])
  });
}


bool EmitCtx::emitSetCons(YogaContext const &ctx,
  ExprOut &o,
  ExprOut &next,
  ExprOut &value,
  ExprOut &modulation,
  YogaSourceLoc sourceLoc)
{
  if (!o.t) o.t = ctx.reg->setType;
  assert(o.t->isSet());
  
  if (!next) return ctx.logError("emitSetCons: Missing next");
  if (!value) return ctx.logError("emitSetCons: Missing value");
  if (!modulation) return ctx.logError("emitSetCons: Missing modulation");
  
  auto consAllocSize = dl.getTypeAllocSize(o.t->llType->getPointerElementType());

  auto space = emitAlloc(ctx, consAllocSize + value.t->llAllocSize);
  if (!space) return false;

  auto consp = builder->CreateBitCast(space, o.t->llType);
  auto objp = builder->CreateBitCast(builder->CreateConstInBoundsGEP1_64(
      space,
      consAllocSize),
    value.t->llType->getPointerTo());

  builder->CreateStore(getv(value), objp);

  auto typePtr = emitTypedPtr(ctx, builder->getInt8PtrTy(), value.t);
  auto sourceLocPtr = emitTypedPtr(ctx, builder->getInt8PtrTy(), ctx.reg->internSourceLoc(sourceLoc));

  builder->CreateStore(getv(next), builder->CreateStructGEP(consp, 0));
  builder->CreateStore(typePtr, builder->CreateStructGEP(consp, 1));
  builder->CreateStore(getv(modulation), builder->CreateStructGEP(consp, 2));
  builder->CreateStore(asU8Ptr(objp), builder->CreateStructGEP(consp, 3));
  builder->CreateStore(sourceLocPtr, builder->CreateStructGEP(consp, 4));

  return setv(ctx, o, consp);
}
