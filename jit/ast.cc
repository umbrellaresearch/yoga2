#include "./ast.h"
#include "./pass_base.h"

void AstNumber::initValues()
{
  if (literalNumber.size() > 2 && literalNumber[0] == '0' && literalNumber[1] == 'x') {
    u64Value = strtoul(literalNumber.c_str()+2, nullptr, 16);
    isU64 = true;
    if (0) cerr << "AstNumber hex " + literalNumber + " => " + repr(u64Value) + "\n";
  }
  else {
    char *end = nullptr;
    doubleValue = strtod(literalNumber.c_str(), &end);
    auto ePos = literalNumber.find('e');
    if (ePos != string::npos) {
      literalExponent = strtol(literalNumber.substr(ePos+1).c_str(), nullptr, 10);
    }
    else {
      literalExponent = 0;
    }
    isDouble = true;
    while (end && *end) {
      if (*end == 'i') {
        isImaginary = true;
        end++;
      }
      else {
        // shouldn't happen because we checked the format in YogaTokenizer::getTok
        throw runtime_error("AstNumber: invalid literal");
      }
    }
  }
}  



bool AstNode::scan(YogaContext &ctx, YogaPassBase *pass)
{
  return true;
}

bool AstName::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstTernop::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("lhs"), lhs)) return false;
    if (!pass->scan(ctx("mhs"), mhs)) return false;
    if (!pass->scan(ctx("rhs"), rhs)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstBinop::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("lhs"), lhs)) return false;
    if (!pass->scan(ctx("rhs"), rhs)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstUnop::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("arg"), arg)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstCall::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("func"), func)) return false;
    if (!pass->scan(ctx("args"), args)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstIndex::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("lhs"), lhs)) return false;
    for (auto &it : args) {
      if (!pass->scan(ctx("args"), it)) return false;
    }
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstStructref::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("lhs"), lhs)) return false;
    if (!pass->scan(ctx("memberName"), memberName)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstNumber::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstRangeSpec::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("scale"), scale)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstParam::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("lhs"), lhs)) return false;
    if (!pass->scan(ctx("rangeSpec"), rangeSpec)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstBoolean::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstNull::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstString::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstDeriv::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("wrt"), wrt)) return false;
    if (!pass->scan(ctx("arg"), arg)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstKeyValue::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("key"), key)) return false;
    if (!pass->scan(ctx("value"), value)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstObject::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("keyValues"), keyValues)) return false;
    if (defaultValue) {
      if (!pass->scan(ctx("default"), defaultValue)) return false;
    }
    if (prev) {
      if (!pass->scan(ctx("prev"), prev)) return false;
    }
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstArray::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("elems"), elems)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstExtern::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstBlock::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("statements"), statements)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstIf::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("cond"), cond)) return false;
    if (!pass->postCondScan(ctx, this)) return false;
    if (!pass->scan(ctx("ifTrue"), ifTrue)) return false;
    if (!pass->postIfTrueScan(ctx, this)) return false;
    if (!pass->scan(ctx("ifFalse"), ifFalse)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstDefault::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("block"), block)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstSimple::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("expr"), expr)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstTypeDecl::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("expr"), type)) return false;
    if (!pass->scan(ctx("exprs"), exprs)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}


bool AstMemberDecl::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("clsName"), clsName)) return false;
    if (!pass->scan(ctx("rangeSpec"), rangeSpec)) return false;
    for (auto &[memberName, memberValue] : members) {
      if (!pass->scan(ctx("memberName"), memberName)) return false;
      if (!pass->scan(ctx("memberValue"), memberValue)) return false;
    }
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstStructDef::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("name"), name)) return false;
    if (!pass->postNameScan(ctx, this)) return false;
    if (!pass->scan(ctx("memberDecls"), memberDecls)) return false;
    if (!pass->scan(ctx("options"), options)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

bool AstArgDecl::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("type"), type)) return false;
    if (!pass->scan(ctx("name"), name)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}


bool AstEnumDecl::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("memberName"), memberName)) return false;
    if (!pass->scan(ctx("memberOptions"), memberOptions)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}


bool AstOnehot::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("memberName"), clsName)) return false;
    if (!pass->scan(ctx("memberOptions"), members)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}


bool AstFunction::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("args"), args)) return false;
    if (!pass->postArgsScan(ctx, this)) return false;
    if (!pass->scan(ctx("body"), body)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}



bool AstAnnoCall::scan(YogaContext &ctx, YogaPassBase *pass)
{
  if (!pass->preScan(ctx, this)) return false;
  if (pass->doRecurse(ctx, this)) {
    if (!pass->scan(ctx("call"), call)) return false;
    if (!pass->scan(ctx("options"), options)) return false;
  }
  if (!pass->postScan(ctx, this)) return false;
  return true;
}

