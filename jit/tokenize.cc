#include "./tokenize.h"

/*
  C operator precedence:

  150      () [] -> .                      left to right
  140      ! ~ ++ -- - (type) * & sizeof   right to left
  135      ^
  130      * / %                           left to right
  120      + -                             left to right
  110      << >>                           left to right
  100      < <= > >=                       left to right
  90       == !=                           left to right
  80       &                               left to right
  70       ^                               left to right
  60       |                               left to right
  50       &&                              left to right
  40       ||                              left to right
  30       ?:                              right to left
  20       = += -= etc.                    right to left
  10       ,                               left to right
*/


YogaTokenizer::YogaTokenizer(YogaSourceFile *_src)
  :src(_src)
{
  assert(src);
  start();
}

YogaTokenizer::YogaTokenizer()
{
  start();
}

void YogaTokenizer::start()
{
  p = textStart = tokenStart = src->text.c_str();
}

void YogaTokenizer::nextTok()
{
again:
  tok = getTok();
  if (tok == T_ERR) {
    if (0) cerr << "Tokenizer: " + shellEscape(literal) + "\n";
    return;
  }
  if (tok == T_EOF) {
    if (ifdefStack.size() != 0) {
      tok = T_ERR;
      literal = "Unmatched #if";
    }
    return;
  }

  if (tok == T_PREPROC) {
    if (0) cerr << describeTok(tok, literal) + "\n";
    if (literal == "#if") {
      auto ifValType = getTok();
      if (ifValType == T_NUMBER) {
        ifdefStack.push_back(stoi(literal));
        goto again;
      }
      else {
        tok = T_ERR;
        literal = "#if requires a number";
        return;
      }
    }
    else if (literal == "#ifdef") {
      auto ifdefSym = getTok();
      if (0) cerr << "  "s + describeTok(ifdefSym, literal) + ": pushing false onto ifdef stack\n";
      ifdefStack.push_back(0);
      goto again;
    }
    else if (literal == "#endif") {
      if (ifdefStack.size() < 1) {
        tok = T_ERR;
        literal = "Unmatched #endif";
        return;
      }
      if (0) cerr << "  popping ifdef stack\n";
      ifdefStack.pop_back();
      goto again;
    }
    else if (literal == "#pragma") {
      getTok(); // ignore
      goto again;
    }
    else {
      if (0) cerr << "  Unknown directive"s + describeTok(tok, literal) + "\n";
    }
  }

  for (auto &it : ifdefStack) {
    if (!it) {
      if (0) cerr << "Ignored: "s + describeTok(tok, literal) + "\n";
      goto again;
    }
  }
  if (0) cerr << describeTok(tok, literal) + "\n";
}

bool YogaTokenizer::match(YogaToken expected)
{
  if (tok == expected) {
    return true;
  }
  // These are not properly reserved words.
  if (expected == T_NAME && (
    tok == T_UPDATE ||
    tok == T_LIVE ||
    tok == T_INIT ||
    tok == T_RUNTIME ||
    tok == T_PANEL ||
    tok == T_PREDICT ||
    tok == T_TRAIN ||
    tok == T_OUT ||
    tok == T_IN ||
    tok == T_POLYFIT)) {
    return true;
  }
  return false;
}

bool YogaTokenizer::consume(YogaToken expected)
{
  if (match(expected)) {
    nextTok();
    return true;
  }
  return false;
}

bool YogaTokenizer::consume(YogaToken expected, string &literalOut)
{
  if (match(expected)) {
    literalOut = literal;
    nextTok();
    return true;
  }
  return false;
}

YogaSourceLoc YogaTokenizer::getSourceLoc()
{
  return YogaSourceLoc(src, tokenStart - textStart, p - textStart);
}

/*
  Examples:
    3
    3.5
    3.5e9
    3e9
    3e-9
    3e+9
    -3
    +3
    - 3    (but only space, tab & newline allowed, not comments)
    + 3
*/
bool YogaTokenizer::isNumberAhead()
{
  if (p[0] >= '0' && p[0] <= '9') return true;
  if (p[0] == '.' && p[1] >= '0' && p[1] <= '9') return true;
#if 0
  if (p[0] == '-' || p[0] == '+') {
    char const *s = p+1;
    if (s[0] == ' ' || s[0] == '\t' || s[0] == '\n' || s[0] == '\r') {
      s += 1;
    }
    if (*s >= '0' && *s <= '9') return true;
  }
#endif
  return false;
}

/*
  Be careful to use these instead of the ctypes version, because
  they (isdigit, isalpha) can be locale-dependent.
  (Just for the record, Yoga is in the "C" locale).
*/
bool YogaTokenizer::isDigit(int c)
{
  if (c >= '0' && c <= '9') return true;
  return false;
}
bool YogaTokenizer::isAlpha(int c)
{
  if (c >= 'a' && c <= 'z') return true;
  if (c >= 'A' && c <= 'Z') return true;
  return false;
}

bool YogaTokenizer::isAlNum(int c)
{
  if (c >= 'a' && c <= 'z') return true;
  if (c >= 'A' && c <= 'Z') return true;
  if (c >= '0' && c <= '9') return true;
  return false;
}

bool YogaTokenizer::isIdent(int c)
{
  if (c >= 'a' && c <= 'z') return true;
  if (c >= 'A' && c <= 'Z') return true;
  if (c >= '0' && c <= '9') return true;
  if (c == '_') return true;
  return false;
}

bool YogaTokenizer::isIdentStart(int c)
{
  if (c >= 'a' && c <= 'z') return true;
  if (c >= 'A' && c <= 'Z') return true;
  if (c == '_') return true;
  return false;
}

bool YogaTokenizer::isHexDigit(char c)
{
  if (c >= '0' && c <= '9') return true;
  if (c >= 'a' && c <= 'f') return true;
  if (c >= 'A' && c <= 'F') return true;
  return false;
}

int YogaTokenizer::fromHexDigit(char c)
{
  if (c >= '0' && c <= '9') return (int)(c - '0');
  if (c >= 'a' && c <= 'f') return (int)(c - 'a') + 10;
  if (c >= 'A' && c <= 'F') return (int)(c - 'A') + 10;
  throw runtime_error("fromHexDigit on bad digit");
}

YogaToken YogaTokenizer::getTok()
{
  eatSpace();
  tokenStart = p;

  if (p[0] == 0) return T_EOF;

  if (isIdentStart(p[0])) {
    literal.clear();
    literal += *p++;
    while (1) {
      if (isIdent(p[0])) {
        literal += *p++;
      }
      else {
        break;
      }
    }

    switch (literal[0]) {

      case 'd':
        if (literal == "default"s) return T_DEFAULT;
        if (literal == "deriv"s) return T_DERIV;
        break;

      case 'e':
        if (literal == "else"s) return T_ELSE;
        if (literal == "extern"s) return T_EXTERN;
        break;

      case 'f':
        if (literal == "function"s) return T_FUNCTION;
        if (literal == "false"s) return T_FALSE;
        break;

      case 'i':
        if (literal == "if"s) return T_IF;
        if (literal == "in"s) return T_IN;
        if (literal == "init"s) return T_INIT;
        break;
      
      case 'l':
        if (literal == "live"s) return T_LIVE;
        break;

      case 'n':
        if (literal == "null"s) return T_NULL;
        break;

      case 'o':
        if (literal == "onehot"s) return T_ONEHOT;
        if (literal == "out"s) return T_OUT;
        break;

      case 'p':
        if (literal == "polyfit"s) return T_POLYFIT;
        if (literal == "panel"s) return T_PANEL;
        if (literal == "predict"s) return T_PREDICT;
        break;

      case 'r':
        if (literal == "runtime"s) return T_RUNTIME;
        if (literal == "require"s) return T_REQUIRE;
        if (literal == "rgba"s) return T_COLOR;
        break;

      case 's':
        if (literal == "softif"s) return T_SOFTIF;
        if (literal == "struct"s) return T_STRUCT;
        break;

      case 't':
        if (literal == "true"s) return T_TRUE;
        if (literal == "train"s) return T_TRAIN;
        break;

      case 'u':
        if (literal == "update"s) return T_UPDATE;
        break;

      default:
        break;
    }

    return T_NAME;
  }

  if (isNumberAhead()) {
    // Numbers suck. There's like, too many of them. -- Beavis
    literal.clear();
    bool hasDecimal{false};
    bool hasNonzeroBeforeDecimal = p[0] >= '1';
    bool startsWithZero = p[0] == '0';
    while (isDigit(p[0])) {
      if (p[0] >= '1') hasNonzeroBeforeDecimal = true;
      literal += *p++;
    }
    if (p[0] == '.') {
      hasDecimal = true;
      literal += *p++;
    }
    while (isDigit(p[0])) {
      literal += *p++;
    }
    if (p[0] == 'e' || p[0] == 'E') {
      literal += *p++;
      if (p[0] == '+' || p[0] == '-') {
        literal += *p++;
      }
      while (isDigit(*p)) {
        literal += *p++;
      }
    }
    if (p[0] == 'i') {
      literal += *p++;
    }
    if (startsWithZero && hasNonzeroBeforeDecimal && !hasDecimal) {
      // Avoid confusion with octals
      literal = "Integer with leading 0 prohibited for fear of confusion with octal"s;
      return T_ERR;
    }
    return T_NUMBER;
  }

  if (p[0] == '"') {
    bool isTriple = false;
    if (p[1] == '"' && p[2] == '"') {
      isTriple = true;
      p += 3;
      // avoid initial blank line
      if (p[0] == '\r' && p[1] == '\n') {
        p += 2;
      }
      else if (p[0] == '\n') {
        p += 1;
      }
    }
    else {
      p += 1;
    }
    literal.clear();
    while (1) {
      if (p[0] == '\\') {
        if (p[1] == '\\') {
          literal += '\\';
          p += 2;
        }
        else if (p[1] == 'b') {
          literal += (char)0x08;
          p += 2;
        }
        else if (p[1] == 'f') {
          literal += (char)0x0c;
          p += 2;
        }
        else if (p[1] == 'n') {
          literal += (char)0x0a;
          p += 2;
        }
        else if (p[1] == 'r') {
          literal += (char)0x0d;
          p += 2;
        }
        else if (p[1] == 't') {
          literal += (char)0x09;
          p += 2;
        }
        else if (p[1] == 'u' && isHexDigit(p[2]) && isHexDigit(p[3]) && isHexDigit(p[4]) && isHexDigit(p[5])) {
          uint32_t codept = (
            (fromHexDigit(p[2]) << 12) |
            (fromHexDigit(p[3]) << 8) |
            (fromHexDigit(p[4]) << 4) |
            (fromHexDigit(p[5]) << 0));
          p += 6;

          char mb[MB_LEN_MAX];
          int mblen = wctomb(mb, (wchar_t)codept);
          for (int mbi = 0; mbi < mblen; mbi++) {
            literal += mb[mbi];
          }
        }
        else {
          literal += p[1];
          p += 2;
        }
      }
      else if (isTriple && p[0] == '"' && p[1] == '"' && p[2] == '"') {
        p += 3;
        return T_STRING;
      }
      else if (!isTriple && p[0] == '"') {
        p += 1;
        return T_STRING;
      }
      else if (p[0] == 0) {
        literal = "EOF in string"s;
        return T_ERR;
      }
      else if (p[0] == '\r' && p[1] == '\n') {
        if (isTriple) {
          literal += '\n';
          p += 1;
        }
        else {
          literal = "Newline in string (maybe use triple quotes)";
          return T_ERR;
        }
      }
      else if (p[0] == '\n') {
        if (isTriple) {
          literal += '\n';
          p += 1;
        }
        else {
          literal = "Newline in string (maybe use triple quotes)";
          return T_ERR;
        }
      }
      else if (p[0] == '\t') {
        if (isTriple) {
          literal += p[0];
          p += 1;
        }
        else {
          literal = "Tab in string (maybe use triple quotes)";
          return T_ERR;
        }
      }
      else if (p[0] < 0x20) {
        literal = "Control character in string"s;
        return T_ERR;
      }
      else {
        literal += p[0];
        p += 1;
      }
    }
  }

  if (p[0] == '(') {
    p += 1;
    return T_LPAR;
  }
  if (p[0] == ')') {
    p += 1;
    return T_RPAR;
  }
  if (p[0] == '[') {
    p += 1;
    return T_LBRK;
  }
  if (p[0] == ']') {
    p += 1;
    return T_RBRK;
  }
  if (p[0] == '{') {
    p += 1;
    return T_LWING;
  }
  if (p[0] == '}') {
    p += 1;
    return T_RWING;
  }

  if (p[0] == '.') {
    if (p[1] == '.' && p[2] == '.') {
      p += 3;
      return T_ELLIPSIS;
    }
    p += 1;
    return T_DOT;
  }

  if (p[0] == '+') {
    if (p[1] == '+') {
      p += 2;
      return T_INC;
    }
    else if (p[1] == '=') {
      p += 2;
      return T_PLUSEQ;
    }
    else {
      p += 1;
      return T_PLUS;
    }
  }

  if (p[0] == '-') {
    if (p[1] == '-') {
      p += 2;
      return T_DEC;
    }
    else if (p[1] == '=') {
      p += 2;
      return T_MINUSEQ;
    }
    else if (p[1] == '>') {
      p += 2;
      return T_PTR;
    }
    else {
      p += 1;
      return T_MINUS;
    }
  }

  if (p[0] == '>') {
    if (p[1] == '>' && p[2] == '=') {
      p += 3;
      return T_RIGHTEQ;
    }
    else if (p[1] == '>') {
      p += 2;
      return T_RIGHT;
    }
    else if (p[1] == '=') {
      p+=2;
      return T_GE;
    }
    else {
      p += 1;
      return T_GT;
    }
  }

  if (p[0] == '<') {
    if (p[1] == '<' && p[2] == '=') {
      p+=3;
      return T_LEFTEQ;
    }
    else if (p[1] == '<') {
      p+=2;
      return T_LEFT;
    }
    else if (p[1] == '=') {
      p+=2;
      return T_LE;
    }
    else {
      p += 1;
      return T_LT;
    }
  }

  if (p[0] == '&') {
    if (p[1] == '&') {
      p += 2;
      return T_ANDAND;
    }
    else if (p[1] == '=') {
      p += 2;
      return T_ANDEQ;
    }
    else {
      p += 1;
      return T_AND;
    }
  }

  if (p[0] == '|') {
    if (p[1] == '|') {
      p += 2;
      return T_OROR;
    }
    else if (p[1] == '=') {
      p += 2;
      return T_OREQ;
    }
    else {
      p += 1;
      return T_OR;
    }
  }

  if (p[0] == '*') {
    if (p[1] == '=') {
      p += 2;
      return T_STAREQ;
    }
    else {
      p += 1;
      return T_STAR;
    }
  }

  if (p[0] == '~') {
    if (p[1] == '~') {
      p += 2;
      return T_TILTIL;
    }
    else {
      p += 1;
      return T_TILDA;
    }
  }

  if (p[0] == '!') {
    if (p[1] == '=') {
      p += 2;
      return T_BANGEQ;
    }
    else {
      p += 1;
      return T_BANG;
    }
  }

  if (p[0] == '/') {
    if (p[1] == '=') {
      p += 2;
      return T_DIVEQ;
    }
    else {
      p += 1;
      return T_DIV;
    }
  }

  if (p[0] == '%') {
    if (p[1] == '=') {
      p += 2;
      return T_MODEQ;
    }
    else {
      p += 1;
      return T_MOD;
    }
  }

  if (p[0] == '=') {
    if (p[1] == '=') {
      p += 2;
      return T_EQEQ;
    }
    else {
      p += 1;
      return T_EQ;
    }
  }

  if (p[0] == '^') {
    if (p[1] == '=') {
      p += 2;
      return T_HATEQ;
    }
    else {
      p += 1;
      return T_HAT;
    }
  }

  if (p[0] == '?') {
    p += 1;
    return T_QUERY;
  }

  if (p[0] == ':') {
    if (p[1] == ':') {
      p += 2;
      return T_COLONCOLON;
    }
    else {
      p += 1;
      return T_COLON;
    }
  }

  if (p[0] == ';') {
    p += 1;
    return T_SEMI;
  }

  if (p[0] == ',') {
    p += 1;
    return T_COMMA;
  }

  literal = "Unexpected character \""s + charname_hex(p[0]) + "\""s;
  return T_ERR;
}


void YogaTokenizer::eatSpace() {
  while (1) {
    if (p[0] == ' ' || p[0] == '\t' || p[0] == '\n' || p[0] == '\r') {
      p += 1;
    }
    else if (p[0] == '/' && p[1] == '/') {
      p += 2;
      while (p[0] != 0 && p[0] != '\n') {
        p += 1;
      }
      if (p[0] != 0) p += 1;
    }
    else if (p[0] == '/' && p[1] == '*') {
      p += 2;
      while (p[0] != 0 && !(p[0] == '*' && p[1] == '/')) {
        p += 1;
      }
      if (p[0] != 0) p += 1;
      if (p[0] != 0) p += 1;
    }
    else {
      break;
    }
  }
}

string YogaTokenizer::describeTok(YogaToken tok0, string const &literal0)
{
  switch (tok0) {
    case T_EOF: return "EOF"s;        
    case T_ERR: return "ERROR("s + literal0 + ")";

    case T_IF: return "if";
    case T_SOFTIF: return "softif";
    case T_ELSE: return "else";
    case T_DEFAULT: return "default";
    case T_STRUCT: return "struct";
    case T_ONEHOT: return "onehot";
    case T_DERIV: return "deriv";
    case T_RUNTIME: return "runtime";
    case T_LIVE: return "live";
    case T_INIT: return "init";
    case T_FUNCTION: return "function";
    case T_REQUIRE: return "require";
    case T_EXTERN: return "extern";
    case T_PANEL: return "panel";
    case T_PREDICT: return "predict";
    case T_TRAIN: return "train";

    case T_IN: return "in";
    case T_OUT: return "out";
    case T_UPDATE: return "update";

    case T_TRUE: return "true";
    case T_FALSE: return "false";
    case T_NULL: return "null";

    case T_NAME: return "NAME("s + literal0 + ")"s;
    case T_NUMBER: return "NUMBER("s + literal0 + ")"s;
    case T_STRING: return "STRING("s + literal0 + ")"s;
    case T_COLOR: return "COLOR("s + literal0 + ")"s;

    case T_LBRK: return "[";
    case T_RBRK: return "]";
    case T_LPAR: return "(";
    case T_RPAR: return ")";
    case T_LWING: return "{";
    case T_RWING: return "}";
    case T_DOT: return ".";
    case T_PTR: return "->";
    case T_INC: return "++";
    case T_DEC: return "--";
    case T_AND: return "&";
    case T_STAR: return "*";
    case T_PLUS: return "+";
    case T_MINUS: return "-";
    case T_TILDA: return "~";
    case T_BANG: return "!";
    case T_DIV: return "/";
    case T_MOD: return "%";
    case T_LEFT: return "<<";
    case T_RIGHT: return ">>";
    case T_LT: return "<";
    case T_GT: return ">";
    case T_LE: return "<=";
    case T_GE: return ">=";
    case T_EQEQ: return "==";
    case T_BANGEQ: return "!=";
    case T_HAT: return "^";
    case T_OR: return "|";
    case T_ANDAND: return "&&";
    case T_OROR: return "||";
    case T_QUERY: return "?";
    case T_COLON: return ":";
    case T_SEMI: return ";";
    case T_ELLIPSIS: return "...";
    case T_EQ: return "=";
    case T_STAREQ: return "*=";
    case T_DIVEQ: return "/=";
    case T_MODEQ: return "%=";
    case T_PLUSEQ: return "+=";
    case T_MINUSEQ: return "-=";
    case T_LEFTEQ: return "<<=";
    case T_RIGHTEQ: return ">>=";
    case T_ANDEQ: return "&=";
    case T_HATEQ: return "^+";
    case T_TILTIL: return "~~";
    case T_OREQ: return "|=";
    case T_COMMA: return ",";
    case T_COLONCOLON: return "::";
    case T_PREPROC: return "PREPROC("s +literal0 + ")"s;

    default: return "UNKNOWN_TOKEN("s + to_string(tok0) + ")"s;
  }
}

string YogaTokenizer::describeCurTok()
{
  return describeTok(tok, literal);
}

void YogaTokenizer::dumpToks(ostream &s)
{
  auto savep = p;
  while (1) {
    auto tok = getTok();
    s << describeCurTok() << "\n";
    if (tok == T_EOF || tok == T_ERR) break;
  }
  p = savep;
}
