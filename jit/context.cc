#include "./context.h"
#include "./expr.h"
#include "./value.h"
#include "./runtime.h"


string YogaContext::mkBanner(string const &errType, string const &s) const
{
  string ret;
  if (lastLoc.file) {
    ret += repr(lastLoc) + ": " + errType + ": ";
    ret += s;
    if (reg->verbose >= 3) {
      ret += " in " + getContext();
    }
  }
  else {
    ret += getContext(3) + ": " + errType + ": ";
    ret += s;
  }
  if (ret.size() < 1 || ret[ret.size()-1] != '\n') {
    ret += "\n";
  }
  return ret;
}


bool YogaContext::logError(string const &s) const
{
  reg->diagLog << mkBanner("error", s);
  reg->diagLog << lastLoc.highlightedLine();
  reg->errCount++;
  reg->checkpointDiagLog();
  return false;
}

nullptr_t YogaContext::logErrorNull(string const &s) const
{
  logError(s);
  return nullptr;
}

bool YogaContext::logError(string const &s, ExprNode *expr) const
{
  logError(s);
  logExpr("expr tree is:", expr);
  reg->checkpointDiagLog();
  return false;
}

void YogaContext::logWarning(string const &s) const
{
  reg->diagLog << mkBanner("warning", s);
  reg->diagLog << lastLoc.highlightedLine();
  reg->warnCount++;
  reg->checkpointDiagLog();
}

void YogaContext::logNote(string const &s) const
{
  reg->diagLog << mkBanner("note", s);
  reg->diagLog << lastLoc.highlightedLine();
  reg->checkpointDiagLog();
}

void YogaContext::logInfo(string const &s) const
{
  reg->diagLog << mkBanner("info", s);
  reg->diagLog << lastLoc.highlightedLine();
}

void YogaContext::logCall(string const &s) const
{
  reg->callLog << s;
  if (s.size() < 1 || s[s.size()-1] != '\n') {
    reg->callLog << "\n";
  }
}

void YogaContext::logGen(string const &s) const
{
  reg->genLog << s;
  if (s.size() < 1 || s[s.size()-1] != '\n') {
    reg->genLog << "\n";
  }
}

void YogaContext::logBackprop(string const &s) const
{
  reg->backpropLog << mkBanner("backprop", s);
}


void YogaContext::logExpr(string const &intro, ExprNode *expr) const
{
  reg->diagLog << "  " << intro << "\n";
  if (expr) {
    expr->dump(reg->diagLog, "    "s, 3);
    reg->diagLog << "\n";
  }
  else {
    reg->diagLog << "    null\n";
  }
  reg->checkpointDiagLog();
}

void YogaContext::logAst(string const &intro, AstNode *ast) const
{
  reg->diagLog << "  " << intro << "\n";
  if (ast) {
    ast->dump(reg->diagLog, "    "s, 3);
    reg->diagLog << "\n";
  } 
  
  else {
    reg->diagLog << "    null\n";
  }
  reg->checkpointDiagLog();
}

YogaContext YogaContext::operator ()(AstNode *_ast) const
{
  return YogaContext(reg, this, desc, _ast ? _ast->sourceLoc : lastLoc);
}

YogaContext YogaContext::operator ()(AstNode *_ast1, AstNode *_ast2) const
{
  if (_ast1) return (*this)(_ast2);
  if (_ast2) return (*this)(_ast1);
  YogaSourceLoc combineLoc(_ast1->sourceLoc.file, _ast1->sourceLoc.start, _ast2->sourceLoc.end);
  return YogaContext(reg, this, desc, combineLoc);
}


YogaContext YogaContext::operator ()(ExprNode *_node) const
{
  return YogaContext(reg, this, desc, _node ? _node->sourceLoc : lastLoc);
}


YogaType *YogaContext::getType(string const &name) const
{
  return reg->getType(*this, name);
}

YogaType *YogaContext::getTypeOrThrow(string const &name) const
{
  auto ret = getType(name);
  if (!ret) throw runtime_error("No yoga type named "s + shellEscape(name));
  return ret;
}

YogaValue YogaContext::mkValue(string const &clsName) const
{
  return YogaValue(reg, getTypeOrThrow(clsName), reg->mem);
}

YogaValue YogaContext::mkValue(YogaType *t) const
{
  if (!t) throw runtime_error("mkValue: null type");
  return YogaValue(reg, t, reg->mem);
}



YogaCaller YogaContext::mkCaller(string const &funcName) const
{
  YogaCaller ret(reg, funcName);
  return ret;
}

string YogaContext::getContext(int depthLim) const
{
  if (depthLim <= 0) return "..."s;
  if (parent) {
    auto firstDifferentParent = parent;
    while (firstDifferentParent && !strcmp(firstDifferentParent->desc, desc)) {
      firstDifferentParent = firstDifferentParent->parent;
    }
    if (firstDifferentParent) {
      return firstDifferentParent->getContext(depthLim-1) + ">"s + desc;
    }
  }
  return desc;
}

TopLevelSymbolValue &YogaContext::lookup(string const &name) const
{
  return reg->symbolTable[name];
}

bool YogaContext::setTopLevel(string const &name, AstNode *node) const
{
  auto &slot = lookup(name);
  if (slot.isReserved) {
    (*this)(node).logError(shellEscape(name) + " is a reserved word "s);
  }
  if (slot.ast) {
    if (slot.ast->asFunction()) {
      (*this)(node).logError(shellEscape(name) + " previously defined as function"s);
      (*this)(slot.ast).logNote("Previous definition");
    }
    else if (slot.ast->asStructDef()) {
      (*this)(node).logError(shellEscape(name) + " previously defined as struct"s);
      (*this)(slot.ast).logNote("Previous definition");
    }
    else {
      (*this)(node).logError(shellEscape(name) + " previously defined"s);
      (*this)(slot.ast).logNote("Previous definition");
    }
    return false;
  }
  slot.ast = node;
  reg->topLevelsInOrder.emplace_back(name, node);
  return true;
}


CallableYogaFunction YogaContext::getYogaFunction(string const &symName) const
{
  auto cf = lookup(symName).compiledFunc;
  if (!cf) return nullptr;
  return cf->funcAddr;
}

CallableYogaGrad YogaContext::getYogaGrad(string const &symName) const
{
  auto cf = lookup(symName).compiledFunc;
  if (!cf) return nullptr;
  return cf->gradAddr;
}

YogaRef YogaContext::getSeqRef(string const &seqName) const
{
  YogaRef ret;
  ret.seqName = seqName;
  ret.fullName = seqName;
  ret.memberName = seqName;
  ret.subName = "";
  ret.seqType = reg->timeseqTypes[seqName];
  ret.itemType = ret.seqType;
  if (!ret.seqType) {
    ret.error = "No type for sequence " + seqName;
    return ret;
  }
  ret.typeName = ret.seqType->clsName;
  return ret;
}

YogaRef YogaContext::getRef(string const &fullName) const
{
  if (!fullName.empty()) {
    for (int doti = 1; doti < 10; doti++) {
      auto [seqName, subName] = splitAtDotNumber(fullName, 2);
      if (reg->timeseqTypes.count(seqName)) {
        YogaRef ret;
        ret.seqName = seqName;
        ret.fullName = fullName;
        ret.memberName = fullName;
        ret.subName = subName;
        ret.seqType = reg->timeseqTypes[seqName];
        ret.itemType = nullptr; // WRITEME
        if (!ret.seqType) {
          ret.error = "No type for sequence " + seqName;
          return ret;
        }
        ret.typeName = ""; // WRITEME
        return ret;
      }
    }
  }
  {
    YogaRef ret;
    ret.fullName = fullName;
    ret.error = "no sequence found";
    return ret;
  }
}

string repr_arg(int argi)
{
  return to_string(argi + 1);
}

string repr_type(string const &clsName)
{
  return clsName;
}

string repr_type(YogaType const *t)
{
  if (!t) return "?";
  return repr_type(t->clsName);
}
