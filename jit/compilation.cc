#include "./ast.h"
#include "./compilation.h"
#include "./parse.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"
#include "./pass_extract_params.h"
#include "./pass_compute_exprs.h"
#include "./pass_define_types.h"
#include "./pass_debug_info.h"
#include "./pass_warnings.h"

#include "./ast_impl.h"
#include "./expr_impl.h"
#include "./param_impl.h"
#include "./effects_impl.h"
#include "../lib/scope_math.h"
#include "./yoga_engine.h"

static const int verboseAlloc = 0;

YogaCompilation::YogaCompilation()
  : layout(YogaLayout::instance()),
    sources(make_shared<YogaSourceSet>())
{
  static int epochCounter = 0;
  codeEpoch = ++epochCounter;

  if (verboseAlloc) cerr << "YogaCompilation " + repr_ptr(this) + "\n";
  mem = new_yogic_pool(nullptr, "YogaCompilation");
  compileStartTime = clock();
  if (!defineBuiltinTypes(mkCtx("builtinTypes"))) {
    throw runtime_error("defining builtin types failed");
  }
  if (!defineReserved(mkCtx("reserved"))) {
    throw runtime_error("defining reserved words failed");
  }
}

YogaCompilation::~YogaCompilation()
{
  if (verboseAlloc) cerr << "~YogaCompilation " + repr_ptr(this) + "\n";
}

YogaContext YogaCompilation::mkCtx(char const *desc)
{
  return YogaContext(this, desc);
}

YogaSourceLoc *YogaCompilation::internSourceLoc(YogaSourceLoc const &sourceLoc)
{
  sourceLocPool.emplace_back(sourceLoc);
  return &sourceLocPool.back();
}


YogaCompiledFuncInfo * 
YogaCompilation::mkCompiledFunc()
{
  auto ptr = make_unique<YogaCompiledFuncInfo>();
  auto *ret = ptr.get();
  compiledFuncPool.emplace_back(std::move(ptr));
  return ret;
}

YogaCompiledFuncInfo * 
YogaCompilation::getCompiledFunc(string const &clsName)
{
  auto &slot = symbolTable[clsName];
  if (!slot.compiledFunc) slot.compiledFunc = mkCompiledFunc();
  return slot.compiledFunc;
}

bool YogaCompilation::runPasses(YogaContext &ctx)
{

  if (!YogaPassDeclareTypes().run(ctx("declareTypes"))) {
    return ctx.logError("Declaring types failed");
  }
  if (!YogaPassDefineTypes().run(ctx("defineTypes"))) {
    return ctx.logError("Defining types failed");
  }
  if (!YogaPassExtractParams().run(ctx("extractParams"))) {
    return ctx.logError("Extracting params failed");
  }
  if (!YogaPassWarnings().run(ctx("warnings"))) {
    return ctx.logError("Emitting warnings failed");
  }
  if (!YogaPassComputeExprs().run(ctx("computeExprs"))) {
    return ctx.logError("Computing exprs failed");
  }
  if (!YogaPassDebugInfo().run(ctx("debugInfo"))) {
    return ctx.logError("Extracting debug info failed");
  }

  if (!ctx.reg->addGradients(ctx("addGradients"))) {
    return ctx.logError("Adding gradients failed");
  }

  if (!ctx.reg->initLlvm(ctx("initLlvm"))) {
    return ctx.logError("initializing llvm failed");
  }

  if (!ctx.reg->generateLlTypes(ctx("generateLlTypes"))) {
    return ctx.logError("Computing type layouts failed");
  }

  if (!ctx.reg->generateLlCodeForYogaFunctions(ctx("generateLlCode"))) {
    return ctx.logError("Computing machine code failed");
  }

  if (!ctx.reg->sortAnnos(ctx("sortAnnos"))) {
    return ctx.logError("Sorting annotations failed");
  }

  if (!setupTimeseqTypes(ctx("setupTimeseqTypes"))) {
    return ctx.logError("Setting up timeseqs failed");
  }
    
  if (!ctx.reg->copyRuntimeParams(ctx("copyRuntimeParams"))) {
    return ctx.logError("Copying runtime params failed");
  }
  return true;
}

bool YogaCompilation::run()
{
  YogaContext ctx(this, "compile");

  try {
    bool ok = runPasses(ctx);
    if (!freezeAllTypes(ctx("freezeAllTypes"))) {
      return ctx.logError("laying out types failed");
    }
    if (!ok) return false;
  }
  catch(runtime_error const &e)
  {
    cerr << "Internal compiler error: "s + e.what() + "\n";
    ctx.logError("Internal compiler error: "s + e.what());
    return false;
  }

  return errCount == 0;
}


bool YogaCompilation::compileMain(string const &fn)
{
  sources->mainFn = fn;
  return compileMain();
}

bool YogaCompilation::compileMain()
{
  if (!doneBuiltins) {
    doneBuiltins = true;
    if (!parseBuiltin()) {
      cerr << "Parse failure:\n";
      checkpointDiagLog();
      if (verbose >= 1) dumpToFiles("jit.log", "compileMain: parseBuiltin failed");
      return false;
    }
  }
  if (!parseForeignFunctions()) {
    cerr << "Parse failure:\n";
    checkpointDiagLog();
    if (verbose >= 1) dumpToFiles("jit.log", "compileMain: parseForeignFunctions failed");
    return false;
  }
  if (!parseFile(sources->mainFn, false, ParseToplevel)) {
    cerr << "Parse failure:\n";
    checkpointDiagLog();
    if (verbose >= 1) dumpToFiles("jit.log", "compileMain: parseFile failed");
    return false;
  }

  if (!run()) {
    cerr << "Compile failure:\n";
    checkpointDiagLog();
    if (verbose >= 1) dumpToFiles("jit.log", "compileMain: compile failed");
    return false;
  }

  checkpointDiagLog();

  if (verbose >= 1) dumpToFiles("jit.log", "compileMain: ok");
  return true;
}

bool YogaCompilation::defineReserved(YogaContext const &ctx)
{
  for (auto const *word : {
    "if", "softif", "else",
    "default",
    "struct", "onehot", 
    "typename",
    "deriv", 
    "engine", "function", "init", "live",
    "require",
    "true", "false", "null",
    "polyfit",
    }) {
    ctx.reg->symbolTable[word].isReserved = true;
  }
  return true;
}

bool YogaCompilation::copyRuntimeParams(YogaContext const &ctx)
{
  if (!ctx.reg->getRuntimeParam("liveHost", ctx.reg->runtimeParams.liveHost)) {
    return ctx.logError("Bad liveHost");
  }
  ctx.reg->runtimeParams.tracePrefix = "run";
  if (!ctx.reg->getRuntimeParam("tracePrefix", ctx.reg->runtimeParams.tracePrefix)) {
    return ctx.logError("Bad tracePrefix");
  }
  if (!ctx.reg->getRuntimeParam("robotName", ctx.reg->runtimeParams.robotName)) {
    return ctx.logError("Bad robotName");
  }
  if (!ctx.reg->getRuntimeParam("duration", ctx.reg->runtimeParams.duration)) {
    return ctx.logError("Bad duration");
  }
  return true;
}

bool YogaCompilation::sortAnnos(YogaContext const &ctx)
{
  for (auto &[srcFile, annos] : ctx.reg->annosByFile) {
    sort(annos.begin(), annos.end(), [](
      AnnoBase const * const &a, 
      AnnoBase const * const &b) {
       return (a->sourceLoc.start < b->sourceLoc.start);
      });
    if (0) {
      cerr << 
        repr(srcFile->fn) + ": " + 
        repr(annos.size()) + " annos\n";
    }
  }
  return true;
}


ostream & operator <<(ostream &s, const AnnoBase &a)
{
  return s << a.repr();
}

ostream & operator <<(ostream &s, YogaParamInfo const &a)
{
  s << 
    "yogaParams[" << a.paramIndex << 
    "/*name=" << a.paramName << 
    " dist=" << a.literalDist;
  if (a.isExtern) {
    s <<
      " extern";
  }
  s <<
    " type=" << (a.paramType ? a.paramType->clsName : "?"s) <<
    "*/] at " << a.sourceLoc;
  return s;
}

void FunctionEffects::dump(ostream &s, string const &indent) const
{
  s << indent << "FunctionEffects at " << sourceLoc << "\n";
  s << indent << "  inArgs:\n";
  for (auto const &[argType, argName] : inArgs) {
    s << indent << "    " << repr_type(argType) << " " << argName << "\n";
  }
  s << indent << "  outArgs:\n";
  for (auto const &[argType, argName] : outArgs) {
    s << indent << "    " << repr_type(argType) << " " << argName << "\n";
  }
  s << indent << "  effects:\n";
  for (auto const &[name, aset] : assignments) {
    aset.dump(s, indent + "    "s, 2);
  }
}


ostream & operator <<(ostream &s, FunctionEffects const &it)
{
  it.dump(s, "    "s);
  return s;
}


/*
  Builtins
*/

vector<YogaCodeRegister *> *YogaCodeRegister::registry;

YogaCodeRegister::YogaCodeRegister(string _fn, string _contents)
  :fn(move(_fn)), contents(move(_contents))
{
  if (!registry) {
    registry = new vector<YogaCodeRegister *>();
  }
  if (0) cerr << "Register " + fn + "\n";
  registry->push_back(this);
}

bool YogaCompilation::parseBuiltin()
{
  map<string, int> fnSectionMap;

  if (YogaCodeRegister::registry) {
    for (auto &it : *YogaCodeRegister::registry) {
      auto &fnSection = fnSectionMap[it->fn];
      fnSection ++;
      auto fnLabel = "[embedded in "s + it->fn;
      if (fnSection > 1) {
         fnLabel += " part "s + to_string(fnSection);
      }
      fnLabel += "]";
      if (0) cerr << fnLabel + "\n";
      if (!parseText(fnLabel, it->contents, ParseToplevel)) return false;
    }
  }
  return true;
}


vector<YogaForeignRegister *> *YogaForeignRegister::registry;


YogaForeignRegister::YogaForeignRegister(string _fn, string _decl, CallableYogaFunction _f, CallableYogaDebug _fDebug)
  : fn(move(_fn)),
    decl(move(_decl)),
    f(_f),
    fDebug(_fDebug)
{
  if (!registry) registry = new vector<YogaForeignRegister *>();
  registry->push_back(this);
}


bool YogaCompilation::parseForeignFunctions()
{
  auto ctx = mkCtx("foreignFunctions");

  if (YogaForeignRegister::registry) {
    for (auto *it : *YogaForeignRegister::registry) {
      auto *src = sources->mkSourceFile(it->fn, it->decl, true);
      if (!src) return ctx.logError("Can't make builtin source " + it->fn);
      YogaParser parser;
      parser.pushTokenizer(src);
      if (!parser.parseForeignDecl(ctx, it->f, it->fDebug)) {
        return false;
      }
    }
  }
  return true;
}

bool YogaCompilation::parseFile(string const &fn, bool isBuiltin, YogaParserMode mode)
{
  auto ctx = mkCtx(fn.c_str());
  auto *src = sources->getFile(ctx, fn, isBuiltin);
  if (!src) return false;
  
  return YogaParser().parseText(ctx, src, mode);
}

bool YogaCompilation::parseText(string const &fn, string const &text, YogaParserMode mode)
{
  auto ctx = mkCtx(fn.c_str());
  auto *src = sources->mkSourceFile(fn, text, true);
  if (!src) return false;
  return YogaParser().parseText(ctx, src, mode);
}

void YogaCompilation::dumpToFiles(string const &basePath, string const &header)
{
  struct OutFileSet {
    bool inited{false};
    ofstream parse, type, effects, params, calls, gen, backprop;
  };
  static map<string, OutFileSet> outFilesByName;
  auto &s = outFilesByName[basePath];
  if (!s.inited) {
    s.inited = true;
    cerr << "Dumping logs to " + basePath + "\n";
    if (mkdir(basePath.c_str(), 0777) < 0) {
      if (errno == EEXIST) {
      }
      else {
        throw runtime_error(basePath + ": " + strerror(errno));
      }
    }
    s.parse.open(basePath + "/parse"s, ios_base::out);
    s.type.open(basePath + "/type"s, ios_base::out);
    s.effects.open(basePath + "/effects"s, ios_base::out);
    s.params.open(basePath + "/params"s, ios_base::out);
    s.calls.open(basePath + "/calls"s, ios_base::out);
    s.gen.open(basePath + "/gen"s, ios_base::out);
    s.backprop.open(basePath + "/backprop"s, ios_base::out);
  }
  if (s.parse.good()) {
    s.parse << "\n\n" + header + "\n";
    dumpParses(s.parse);
  }
  if (s.type.good()) {
    s.type << "\n\n" + header + "\n";
    dumpTypes(s.type);
  }
  if (s.effects.good()) {
    s.effects << "\n\n" + header + "\n";
    dumpEffects(s.effects);
  }
  if (s.params.good()) {
    s.params << "\n\n" + header + "\n";
    dumpParams(s.params);
  }
  if (s.calls.good()) {
    s.calls << "\n\n" + header + "\n";
    s.calls << callLog.str();
  }
  if (s.gen.good()) {
    s.gen << "\n\n" + header + "\n";
    s.gen << genLog.str();
  }
  if (s.backprop.good()) {
    s.backprop << "\n\n" + header + "\n";
    s.backprop << backpropLog.str();
  }
}


void YogaCompilation::dumpParses(ostream &s)
{
  for (auto &[symName, symVal] : symbolTable) {
    if (symVal.ast) {
      s << "parse " + symName + "\n";
      symVal.ast->dump(s, "  ", 3);
      s << "\n";
    }
  }
}


void YogaCompilation::dumpTypes(ostream &s)
{
  for (auto &[symName, symVal] : symbolTable) {
    if (symVal.type) {
      s << "type " << symName << "\n";
      symVal.type->dump(s, "  ");
      s << "\n";
    }
  }
}


void YogaCompilation::dumpEffects(ostream &s)
{
  for (auto &[symName, symVal] : symbolTable) {
    if (symVal.funcEffects) {
      s << "Effects in " << symName << ":\n";
      for (auto &[name, aset] : symVal.funcEffects->assignments) {
        aset.dump(s, "  "s, 99);
      }
      s << "\n";
    }
  }
}

void YogaCompilation::dumpParams(ostream &s)
{
  for (auto *paramInfo : paramsByIndex) {
    if (!paramInfo) continue;
    s << paramInfo << "\n";
  }
}

string denseList(vector<string> const &l)
{
  string ret;
  int lineLen = 0;
  for (auto const &it : l) {
    if (lineLen > 70) {
      ret += "\n";
      lineLen = 0;
    }
    if (lineLen == 0) {
      ret += "    ";
      lineLen = 4;
    }
    else {
      ret += " ";
      lineLen += 1;
    }
    ret += it;
    lineLen += it.size();
  }
  return ret;
}

void YogaCompilation::summarizeProgram(ostream &s)
{
  vector<string> userTypes, debugLocalTypes;
  vector<string> allFuncs;

  for (auto &[symName, symVal] : symbolTable) {
    if (symVal.type && symVal.type->clsName == symName) { // only report canonical type names
      if (symVal.type->isDebugLocals) {
        debugLocalTypes.push_back(symName);
      }
      else {
        userTypes.push_back(symName);
      }
    }
    if (symVal.astFunction) {
      allFuncs.push_back(symName);
    }
  }

  sort(userTypes.begin(), userTypes.end());
  sort(debugLocalTypes.begin(), debugLocalTypes.end());
  sort(allFuncs.begin(), allFuncs.end());

  compileEndTime = clock();

  s << "User types from "s + sources->mainFn + ":\n"s +  denseList(userTypes) + "\n";
  if (verbose >= 1) s << "Local types:\n" + denseList(debugLocalTypes) + "\n";
  s << "Loaded functions:\n" + denseList(allFuncs) + "\n";
  s << "Compile time " +  repr_clock(compileEndTime - compileStartTime) + "\n";

  if (initializeWithByte != 0) {
    double dv{0.0};
    memset(&dv, initializeWithByte, sizeof(dv));
    float fv{0.0f};
    memset(&fv, initializeWithByte, sizeof(fv));
    int iv{0};
    memset(&iv, initializeWithByte, sizeof(iv));

    cerr << "Uninitialized byte=" + repr_02x(initializeWithByte) +
        " R=" + fmteng(dv, 3) +
        " F=" + fmteng(fv, 3) +
        " int=" + repr(iv) + "\n";
  }

}

string YogaCompilation::summarizeProgram()
{
  ostringstream oss;
  summarizeProgram(oss);
  return oss.str();
}

void YogaCompilation::checkpointDiagLog()
{
  if (doCheckpointDiagLog) {
    string const &dlstr = diagLog.str();
    if (dlstr.size() > diagLogCheckpointPos) {
      cerr << dlstr.substr(diagLogCheckpointPos);
      if (dlstr[dlstr.size() - 1] != '\n') {
        cerr << "\n";
      }
      diagLogCheckpointPos = dlstr.size();
    }
  }
}
