#pragma once
#include "./jit_utils.h"
#include "./llvm_fwd.h"
#include "./type.h"
#include "./value.h"
#include "./compilation.h"

struct YogaType;
struct YogaDebugInfo;
struct YogaValue;
struct YogaSourceLoc;


vector<YogaSetCons *> setAsVector(YogaSetCons *set);
vector<YogaSetCons *> setSortedBySourceLoc(YogaSetCons *set, YogaSourceFile *fileFilter);

namespace packetio {
void packet_wr_value(::packet &p, ::YogaSetCons * const &x, ::YogaType *valueType);
void packet_rd_value(::packet &p, ::YogaSetCons *&x, ::YogaType *valueType);
}



struct YogaCompiledFuncInfo {

  vector<pair<YogaType *, string> > inArgs;
  vector<pair<YogaType *, string> > outArgs;

  int findInArg(string const &name);
  int findOutArg(string const &name);
  int findInArgOrThrow(string const &name);
  int findOutArgOrThrow(string const &name);

  CallableYogaFunction funcAddr{nullptr};
  CallableYogaGrad gradAddr{nullptr};
  CallableYogaDebug debugAddr{nullptr};

  YogaTypeStruct *debugLocalsType{nullptr};
};



struct YogaRef {
  string seqName;
  string memberName;
  string fullName;
  string subName;
  string typeName;
  string error;
  YogaType *seqType{nullptr};
  YogaType *itemType{nullptr};
  string summary() const;
  YogaRef member(string const &name) const;
  YogaRef cell(int row, int col) const;

  static string fmtFunctionLabel(string const &funcName, vector<YogaRef> const &refs);
  static string fmtStandaloneLabel(vector<YogaRef> const &refs);

  json toJson() const;
  static YogaRef fromJson(YogaContext const &ctx, json const &j);
};


ostream & operator <<(ostream &s, YogaCompiledFuncInfo const &a);

struct YogaCaller {
  YogaCaller();
  YogaCaller(YogaCompilation *_reg, string const &_funcName);
  YogaCaller(YogaCaller const &other) = default;

  void assign(YogaCompilation *_reg, string const &_funcName);
  void validate();

  void operator()(
    vector<void *> const &outArgs,
    vector<void *> const &inArgs,
    vector<R> const &paramValues,
    R &astonishment,
    R dt,
    apr_pool_t *mem) const;

  void operator()(
    vector<void *> const &outArgs,
    vector<void *> const &inArgs,
    R &astonishment,
    R dt=0.0,
    apr_pool_t *mem=nullptr) const;

  void operator()(
    vector<YogaValue> const &outArgs,
    vector<YogaValue> const &inArgs,
    R &astonishment,
    R dt=0.0,
    apr_pool_t *mem=nullptr) const;

  void grad(
    vector<void *> const &outArgs,
    vector<void *> const &outArgsGradIn,
    vector<void *> const &inArgs,
    vector<void *> const &inArgsGradOut,
    vector<R> const &paramValues,
    vector<R> &paramsGradOut,
    R &astonishment,
    R dt=0.0, 
    apr_pool_t *mem=nullptr) const;

  void grad(
    vector<YogaValue> const &outArgs,
    vector<YogaValue> const &outArgsGradIn,
    vector<YogaValue> const &inArgs, 
    vector<YogaValue> const &inArgsGradOut,
    vector<R> &paramsGradOut,
    R &astonishment,
    R dt=0.0,
    apr_pool_t *mem=nullptr) const;

  void debug(
    vector<void *> const &outArgs,
    vector<void *> const &inArgs,
    vector<R> const &paramValues,
    R &astonishment,
    R dt, 
    void *debugLocals,
    apr_pool_t *mem) const;

  void debug(
    vector<YogaValue> const &outArgs,
    vector<YogaValue> const &inArgs, 
    R &astonishment,
    R dt,
    YogaValue &debugLocals,
    apr_pool_t *mem) const;

  void allocInTypes(vector<YogaValue> &a, apr_pool_t *mem);
  void allocOutTypes(vector<YogaValue> &a, apr_pool_t *mem);
  void allocParams(vector<R> &params);

  YogaCompilation *reg{nullptr};
  string funcName;
  YogaCompiledFuncInfo *f{nullptr};
};

template<typename MemberType>
struct YogaValueAccessor {
  YogaValueAccessor()
  {
  }

  YogaValueAccessor(YogaType *_t, string const &_subName)
    : t(_t),
      subName(_subName)
  {
    init();
  }

  YogaValueAccessor(YogaRef const &ref)
    : t(ref.seqType),
      subName(ref.subName)
  {
    init();
  }

  YogaValueAccessor & operator = (YogaRef const &ref)
  {
    t = ref.seqType;
    subName = ref.subName;
    init();
    return *this;
  }

  void assign(YogaType *_t, string const &_subName)
  {
    t = _t;
    subName = _subName;
    init();
  }

  void init()
  {
    if (!t) throw runtime_error("Trying to create YogaValueAccessor " + shellEscape(subName) + " with null type");

    label = t->clsName + "::" + subName + "<" + typeid(MemberType).name() + ">";
    string wrName = subName + "::wr." + typeid(MemberType).name();
    string rdName = subName + "::rd." + typeid(MemberType).name();
    wrf = t->accessorsByName[wrName];
    rdf = t->accessorsByName[rdName];
    if (0) {
      cerr << "Accessor "s + repr_type(t) + " " + wrName + " " + (wrf ? "ok" : "null") + "\n";
      cerr << "Accessor "s + repr_type(t) + " " + rdName + " " + (rdf ? "ok" : "null") + "\n";
    }
    if (!wrf && !rdf) {
      if (!wrf) {
        cerr << "Warning: no write accessor within "s + repr_type(t) + " named " + shellEscape(wrName) + "\n";
      }
      if (!rdf) {
        cerr << "Warning: no read accessor within "s + repr_type(t) + " named " + shellEscape(rdName) + "\n";
      }
      for (auto &[name, accessor] : t->accessorsByName) {
        cerr << "  defined accessor named "s + shellEscape(name) + " " + (accessor ? "ok" : "null") + "\n";
      }
    }
  }

  bool isValid()
  {
    return t && wrf && rdf;
  }

  YogaType *t{nullptr};
  string subName;
  string label;
  CallableYogaAccessor wrf{nullptr};
  CallableYogaAccessor rdf{nullptr};

  void wr(U8 *yvp, MemberType const &cv) const
  {
    if (!wrf) throw runtime_error("No write accessor for "s + shellEscape(label));
    wrf(yvp, const_cast<U8 *>(reinterpret_cast<U8 const *>(&cv)));
  }
  void wr(YogaValue const &yv, MemberType const &cv) const
  {
    if (!yv.buf) throw runtime_error("Invalid Value for "s + shellEscape(label));
    if (yv.type != t) throw runtime_error("Wrong type for "s + shellEscape(label));
    wr(yv.buf, cv);
  }
  void rd(U8 *yvp, MemberType &cv) const
  {
    if (!rdf) throw runtime_error("No read accessor for "s + shellEscape(label));
    rdf(yvp, reinterpret_cast<U8 *>(&cv));
  }
  void rd(YogaValue const &yv, MemberType &cv) const
  {
    if (!yv.buf) throw runtime_error("Invalid Value for "s + shellEscape(label));
    if (yv.type != t) throw runtime_error("Wrong type for "s + shellEscape(label));
    rd(yv.buf, cv);
  }
  MemberType rd(YogaValue const &yv) const
  {
    MemberType ret{};
    rd(yv, ret);
    return ret;
  }
};

struct YogaPtrAccessor {
  YogaPtrAccessor()
  {
  }

  YogaPtrAccessor(YogaType *_t, string const &_subName)
    : t(_t),
      subName(_subName)
  {
    init();
  }

  YogaPtrAccessor(YogaRef const &ref)
    : t(ref.seqType),
      subName(ref.subName)
  {
    init();
  }

  YogaPtrAccessor & operator = (YogaRef const &ref)
  {
    t = ref.seqType;
    subName = ref.subName;
    init();
    return *this;
  }

  void assign(YogaType *_t, string const &_subName)
  {
    t = _t;
    subName = _subName;
    init();
  }

  void init()
  {
    string ptrName = subName + "::ptr";
    label = t->clsName + "::" + subName;

    ptrf = t->accessorsByName[ptrName];
    ptrType = t->ptrTypesByName[ptrName];
    if (0) {
      cerr << "Accessor "s + repr_type(t) + " " + ptrName + " " + (ptrf ? "ok" : "null") + "\n";
    }
    if (!ptrf) cerr << "Warning: no ptr accessor "s + repr_type(t) + " " + shellEscape(ptrName) + "\n";
  }

  bool isValid()
  {
    return t && ptrf && ptrType;
  }

  YogaType *t{nullptr};
  string subName;
  string label;
  CallableYogaAccessor ptrf{nullptr};
  YogaType *ptrType{nullptr};

  YogaValue ptr(YogaValue const &yv) const
  {
    if (!yv.buf) throw runtime_error("Invalid Value for "s + shellEscape(label));
    if (yv.type != t) throw runtime_error("Wrong type for "s + shellEscape(label));
    if (!ptrf) throw runtime_error("No ptr accessor for "s + shellEscape(label));
    U8 *subYv{nullptr};
    ptrf(yv.buf, const_cast<U8 *>(reinterpret_cast<U8 const *>(&subYv)));
    return YogaValue(yv.reg, ptrType, subYv, yv.ts);
  }

};


template<typename T>
inline YogaValue & YogaValue::wr(string const &name, T const &value) {
  YogaValueAccessor<T>(type, name).wr(*this, value);
  return *this;
}

template<typename T>
inline void YogaValue::rd(string const &name, T &value) const {
  YogaValueAccessor<T>(type, name).rd(*this, value);
}

template<typename T>
inline T YogaValue::rd(string const &name) const {
  T ret;
  rd(name, ret);
  return ret;
}


YogaValue linearOpSum(R ca, YogaValue const &a, R cb, YogaValue const &b, apr_pool_t *mem);

extern "C" U8 * yogic_strcat(U8 *a, U8 *b, apr_pool_t *mem);
