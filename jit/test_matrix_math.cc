#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../geom/geom.h"
#include "../test/test_utils.h"


TEST_CASE("mat44Identity works", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R[4,4] o1) {
      o1 = mat44Identity();
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  glm::dmat4 o1;
  R astonishment = 0.0;
  tp({&o1}, {}, astonishment);
  glm::dmat4 o1expect(1);

  CHECK(o1 == o1expect);
};


TEST_CASE("mat4 * vec4 works", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R[4] v) {
      m = R[4,4](1,2,3,0, 4,5,6,0, 7,8,9,0, 0,0,0,1);
      v = nograd(m * R[4](1.0, 100.0, 10000.0, 1));
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  glm::dvec4 v;
  R astonishment = 0.0;
  tp({&v}, {}, astonishment);
  CHECK(astonishment < 2.0);
  glm::dvec4 vexpect(
    1*1 + 4*100 + 7*10000,
    2*1 + 5*100 + 8*10000,
    3*1 + 6*100 + 9*10000,
    1);

  CHECK(v == vexpect);
};

TEST_CASE("fromHomo(mat4 * vec4) works", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R[3] v) {
      m = R[4,4](1,2,3,0, 4,5,6,0, 7,8,9,0, 0,0,0,1);
      v = fromHomo(nograd(m * R[4](1.0, 100.0, 10000.0, 1)));
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  glm::dvec3 v;
  R astonishment = 0.0;
  tp({&v}, {}, astonishment);
  glm::dvec3 vexpect(
    1*1 + 4*100 + 7*10000,
    2*1 + 5*100 + 8*10000,
    3*1 + 6*100 + 9*10000);

  CHECK(v == vexpect);
};

TEST_CASE("mat4 * mat4 works", "[compiler]") {

  auto reg = YOGAC(R"(
    function tp(out R[4,4] c) {
      a = R[4,4](1,0,0,0, 0,0,1,0, 0,1,0,0, 0,0,0,1);
      b = R[4,4](1,2,3,0, 4,5,6,0, 7,8,9,0, 0,0,0,1);
      c = nograd(a * b);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto tp = ctx.mkCaller("tp");

  glm::dmat4 c;
  R astonishment = 0.0;
  tp({&c}, {}, astonishment);
  CHECK(astonishment < 2.0);

  if (0) cerr << to_string(c) + "\n";

  CHECK(c[0][0] == 1);
  CHECK(c[0][1] == 3);
  CHECK(c[0][2] == 2);
  CHECK(c[0][3] == 0);
  CHECK(c[1][0] == 4);
  CHECK(c[1][1] == 6);
  CHECK(c[1][2] == 5);
  CHECK(c[1][3] == 0);
  CHECK(c[2][0] == 7);
  CHECK(c[2][1] == 9);
  CHECK(c[2][2] == 8);
  CHECK(c[2][3] == 0);
  CHECK(c[3][0] == 0);
  CHECK(c[3][1] == 0);
  CHECK(c[3][2] == 0);
  CHECK(c[3][3] == 1);
};


TEST_CASE("trivial slicing matrix works", "[compiler]") {
  auto reg = YOGAC(R"(
    function lineMat(
      out R o,
      in R[4] p1,
      in R[4] p2)
    {
      o = p1[0];
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));
}  

TEST_CASE("slicing matrix works", "[compiler]") {
  auto reg = YOGAC(R"(
    function lineMat(
      out R[4] o,
      in R[4] p1,
      in R[4] p2)
    {
      rel = p2 - p1;
      o = R[4](rel[0], rel[1], rel[2], rel[3]);
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));
}

