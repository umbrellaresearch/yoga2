#include "./sourcefile.h"
#include "./context.h"
#include "common/packetbuf_types.h"
#include "../db/yoga_layout.h"

struct AnnoBase;

YogaSourceFile::YogaSourceFile(string const &_fn, string const &_text, bool _isBuiltin)
  : fn(_fn),
    text(_text),
    isBuiltin(_isBuiltin)
{
}

YogaSourceFile::YogaSourceFile()
  : fn("<empty>"s),
    text(""s),
    isBuiltin(true)
{
}

int YogaSourceFile::lineCount()
{
  int ret = 1;
  for (auto &c : text) {
    if (c == '\n') ret++;
  }
  return ret;
}

YogaSourceLoc::YogaSourceLoc(YogaSourceFile *_file, int _start, int _end)
  : file(_file),
    start(_start),
    end(_end)
{
}

YogaSourceLoc::YogaSourceLoc()
  : file(nullptr),
    start(0),
    end(0)
{
}


pair<int, int> YogaSourceLoc::calcLineNumber() const
{
  if (!file) return make_pair(0, 0);
  // TODO: pre-compute a table of line starts, do a binary search.
  // Currently we only use this when printing error messages, so performance isn't a problem.

  int linei = 1;
  int coli = 0;
  auto &text = file->text;
  for (int i=0; i < start && i < (int)text.size(); i++) {
    if (text[i] == '\n') {
      linei++;
      coli = 0;
    }
    else {
      coli++;
    }
  }
  return make_pair(linei, coli);
}

string YogaSourceLoc::highlightedLine() const
{
  if (!file) return ""s;

  auto lineNumber = calcLineNumber();

  string ret;
  int linei = 1;
  int coli = 0;
  auto &text = file->text;
  for (int i=0; i < (int)text.size(); i++) {
    if (linei == lineNumber.first) {
      ret += text[i];
    }
    if (text[i] == '\n') {
      linei++;
      coli = 0;
    }
    else {
      coli++;
    }
  }
  ret += string(lineNumber.second, ' ') + "^\n";
  return ret;
}

string YogaSourceLoc::tokenText() const
{
  if (!file) return ""s;
  return file->text.substr(start, end-start);
}

string YogaSourceLoc::gloss() const
{
  return repr(*this) + " " + quoteStringC(tokenText());
}


ostream & operator <<(ostream &s, const YogaSourceLoc &a)
{
  if (a.file) {
    auto lineNumber = a.calcLineNumber();
    s << a.file->fn << ":" << lineNumber.first << ":" << lineNumber.second;
  }
  else {
    s << "?";
  }
  return s;
}


[[nodiscard]] static bool readFile(YogaContext const &ctx, string const &fn, string &contents)
{
  int fd = open(fn.c_str(), O_RDONLY, 0);
  if (fd < 0) {
    return ctx.logError("Can't open "s + shellEscape(fn) + ": " + strerror(errno));
  }

  while (1) {
    char buf[8192];
    int nr = read(fd, buf, sizeof(buf));
    if (nr < 0) {
      return ctx.logError("Error reading "s + shellEscape(fn) + ": " + strerror(errno));
      break;
    }
    else if (nr == 0) {
      break;
    }
    else {
      contents.append(buf, nr);
    }
  }
  close(fd);
  return true;
}


YogaSourceSet::YogaSourceSet()
  :layout(YogaLayout::instance())
{
}

YogaSourceSet::~YogaSourceSet()
{
}

YogaSourceFile * YogaSourceSet::mkSourceFile(string const &_fn, string const &_text, bool isBuiltin)
{
  auto ptr = make_unique<YogaSourceFile>(_fn, _text, isBuiltin);
  YogaSourceFile *ret = ptr.get();
  sourceFilePool.emplace_back(std::move(ptr));
  requiredFilesInOrder.push_back(ret);
  requiredFilesByName[_fn] = ret;
  return ret;
}


YogaSourceFile *
YogaSourceSet::getFile(YogaContext const &ctx, string const &fn, bool isBuiltin)
{
  auto ret = requiredFilesByName[fn];
  if (ret) return ret;

  if (expectOnlyPseudoFiles) {
    ctx.logError("No pseudo file " + shellEscape(fn));
    return nullptr;
  }
  string contents;
  if (!readFile(ctx, fn, contents)) {
    ctx.logError("No file " + shellEscape(fn));
    return nullptr;
  }
  return mkSourceFile(fn, contents, isBuiltin);
}


void YogaSourceSet::tx(packet &p)
{
  for (auto srcFile : requiredFilesInOrder) {
    if (!srcFile || startsWith(srcFile->fn, "[embedded ")) continue;
    p.add(true);
    p.add(srcFile->fn);
    p.add(srcFile->text);
    p.add(srcFile->isBuiltin);
  }
  p.add(false);

  for (auto &[rnSource, rnTarget] : requireNameMap) {
    if (rnTarget.empty()) continue;
    p.add(true);
    p.add(rnSource);
    p.add(rnTarget);
  }
  p.add(false);
  p.add(mainFn);
}


void YogaSourceSet::rx(packet &p)
{
  requiredFilesInOrder.clear();
  requiredFilesByName.clear();

  expectOnlyPseudoFiles = true;
  while (p.fget<bool>()) {
    string fn, contents;
    bool isBuiltin{false};
    p.get(fn);
    p.get(contents);
    p.get(isBuiltin);
    mkSourceFile(fn, contents, isBuiltin);
  }

  while (p.fget<bool>()) {
    string rnSource, rnTarget;
    p.get(rnSource);
    p.get(rnTarget);
    requireNameMap[rnSource] = rnTarget;
  }

  p.get(mainFn);
}

