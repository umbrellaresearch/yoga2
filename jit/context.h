#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./tokenize.h"
#include "./llvm_fwd.h"

/* 
  Most of the code is in context.cc
  We create a lot of copies of this, at least one for every node in the
  parse tree. So keep it light. It's currently 40 bytes.
  We try to defer most processing to getContext(), which is only called
  when printing errors.
*/
struct YogaContext {
  YogaContext(YogaCompilation *_reg, YogaContext const *_parent, char const *_desc, YogaSourceLoc const &_lastLoc)
    : reg(_reg),
      parent(_parent),
      desc(_desc),
      lastLoc(_lastLoc)
  {
  }
  YogaContext(YogaCompilation *_reg, char const *_desc)
    : reg(_reg),
      parent(nullptr),
      desc(_desc)
  {
  }
  YogaContext operator ()(char const *_desc) const
  {
    return YogaContext(reg, this, _desc, lastLoc);
  }
  YogaContext operator ()(YogaSourceLoc _lastLoc) const
  {
    return YogaContext(reg, parent, desc, _lastLoc);
  }
  YogaContext operator ()(YogaTokenizer *tok) const
  {
    return YogaContext(reg, parent, desc, tok->getSourceLoc());
  }
  YogaContext operator ()(YogaTokenizer *tok, char const *_desc) const
  {
    return YogaContext(reg, this, _desc, tok->getSourceLoc());
  }
  YogaContext operator ()(ExprNode *_node) const;
  YogaContext operator ()(AstNode *_ast) const;
  YogaContext operator ()(AstNode *_ast1, AstNode *_ast2) const;

  YogaCompilation *reg;
  YogaContext const *parent;
  char const *desc;
  YogaSourceLoc lastLoc;


  /*
    Create nodes: expr, ast, annotation, struct member.
    These will return null if they fail for some reason.
    Nodes are added to a ctx.reg->___Pool for eventual destruction when we delete
    the YogaCompilation.

    mkExprRaw is guaranteed to return an Expr of type T.
    mkExpr calls the peephole optimizer, which might return some other type.
  */
  template<typename T, typename ...Args> ExprNode * mkExpr(Args&& ...args) const;

  template<typename T, typename ...Args> T * mkExprRaw(Args&& ...args) const;

  template<typename T, typename ...Args> T * mkAst(Args&& ...args) const;

  template<typename T, typename ...Args> T * mkAnnotation(Args&& ...args) const;

  template<typename ...Args> StructMember * mkStructMember(Args&& ...args) const;

  template <typename T, typename... Args>T * mkParam(string const &paramName, Args &&... args) const;

  template<typename T, typename ...Args> T * mkType(Args&& ...args) const;

  template<typename T, typename ...Args> T * mkEffects(Args&& ...args) const;

  YogaValue mkValue(string const &clsName) const;
  YogaValue mkValue(YogaType *t) const;
  YogaCaller mkCaller(string const &funcName) const;

  /*
    Convert a type to a desired type by inserting another node. Mutates a.
    Eg, a = mkExpr<ExprConvBoolDouble>(a);
  */
  [[nodiscard]] bool convertType(ExprNode *&a, YogaType *desired) const;
  /*
    Insert type conversions to make both nodes compatible. Similar to what C does
    for a+b.
  */
  [[nodiscard]] bool promoteTypes(ExprNode *&a, ExprNode *&b) const;

  /*
    Log errors. All these return false or null, for convenience in returning.
  */
  bool logError(string const &s) const;
  bool logError(string const &s, ExprNode *expr) const;
  nullptr_t logErrorNull(string const &s) const;
  void logWarning(string const &s) const;
  void logNote(string const &s) const;
  void logInfo(string const &s) const;
  void logBackprop(string const &s) const;

  void logCall(string const &s) const;
  void logGen(string const &s) const;

  /*
    Logging utilities
  */
  string mkBanner(string const &errType, string const &s) const;
  void logAst(string const &intro, AstNode *ast) const;
  void logExpr(string const &intro, ExprNode *expr) const;
  string getContext(int depthLim = 5) const;


  /*
    Get a type by name. It will call logError if it fails. The OrThrow variant throws
    instead of returning null, useful if you need a type that's "guaranteed" to exist.
  */
  YogaType *getType(string const &name) const;
  YogaType *getTypeOrThrow(string const &name) const;

  TopLevelSymbolValue &lookup(string const &name) const;
  bool setTopLevel(string const &name, AstNode *node) const;
  CallableYogaFunction getYogaFunction(string const &symName) const;
  CallableYogaGrad getYogaGrad(string const &symName) const;

  /*

  */
  YogaRef getSeqRef(string const &seqName) const;
  YogaRef getRef(string const &fullName) const;

};

string repr_arg(int argi);
string repr_type(YogaType const *t);
string repr_type(string const &clsName);
