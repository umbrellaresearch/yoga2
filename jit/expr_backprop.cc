#include "./ast.h"
#include "./compilation.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"
#include "./expr_impl.h"

/*
  For each node (keyed by cseKey, so equivalent nodes are grouped) a set of all the gradients
  feeding into each node.

  addGradient(ctx, n, g) adds gradient g to node n.

  getTotalGradient(ctx, n) returns the sum of all the gradients into node n (or null if there are none.)

  getTotalGradientOrZero(ctx, n) returns the above, or a ExprConstZero of the right type if none.

*/
struct GradientExprSet {
  struct NodeGradInfo {
    bool depDone{false};
    vector<ExprNode *> gradAccum;
  };

  map<string, NodeGradInfo> nodeInfo;
  vector<ExprNode *> fwdOrder;

  void addDeps(YogaContext const &ctx, ExprNode *n)
  {
    auto &infoSlot = nodeInfo[n->cseKey];
    if (!infoSlot.depDone) {
      infoSlot.depDone = true;
      n->addChildDeps(ctx, *this);
      fwdOrder.push_back(n);
    }
  }

  ExprNode *getTotalGradient(YogaContext const &ctx, ExprNode *n)
  {
    auto &info = nodeInfo[n->cseKey];
    if (info.gradAccum.empty()) return nullptr;
    auto ret = info.gradAccum[0];
    for (size_t i = 1; i < info.gradAccum.size(); i++) {
      if (ret->type->hasLinearOps() && info.gradAccum[i]->type->hasLinearOps()) {
        ret = ctx.mkExpr<ExprAdd>(ret, info.gradAccum[i]);
        if (!ret) return nullptr;
      }
    }
    return ret;
  }

  ExprNode *getTotalGradientOrZero(YogaContext const &ctx, ExprNode *n)
  {
    auto g = getTotalGradient(ctx, n);
    if (g) return g;
    return ctx.mkExpr<ExprConstZero>(n->type);
  }

  void addGradient(YogaContext const &ctx, ExprNode *n, ExprNode *g)
  {
    if (!g) return;
    if (n->type != g->type) throw runtime_error("addGradient: type mismatch n.t=" + repr_type(n->type) + " g.t=" + repr_type(g->type));
    auto &info = nodeInfo[n->cseKey];
    info.gradAccum.push_back(g);
  }

};

ExprNode *gradFromOutVar(YogaContext const &ctx, ExprNode *dst)
{
  if (auto dstv = dst->asVar()) {
    if (startsWith(dstv->name, "out.")) {
      return ctx.mkExpr<ExprVar>(dstv->type, dstv->funcName, "outGrad." + dstv->name.substr(4), YD_IN);
    }
  }
  else if (auto dsts = dst->asStructref()) {
    if (auto lhsGrad = gradFromOutVar(ctx, dsts->args[0])) {
      return ctx.mkExpr<ExprStructref>(lhsGrad, dsts->memberName, dsts->memberInfo);
    }
  }
  return nullptr;
}


bool YogaCompilation::addGradients(YogaContext const &ctxOuter)
{
  if (!ctxOuter.reg->emitGradVersions) return true;

  for (auto &[symName, symVal] : ctxOuter.reg->symbolTable) {
    if (auto effects = symVal.funcEffects) {
      auto ctx = ctxOuter(symVal.astFunction);

      auto gradEffects = symVal.funcGradEffects = ctx.mkEffects<FunctionEffects>();
      // All these have to copy, because we're going to mangle them below.
      gradEffects->assignments = effects->assignments;
      gradEffects->inArgs = effects->inArgs;
      gradEffects->outArgs = effects->outArgs;
      gradEffects->debugLocalItems = effects->debugLocalItems;

      GradientExprSet grads;

      for (auto &[dstKey, aset] : gradEffects->assignments) {
        if (aset.dst) {
          if (aset.dst->type->asSet()) {
            // WRITEME? Do we need this?
            continue;
          }
          if (auto dstGrad = gradFromOutVar(ctx, aset.dst)) {
            grads.addGradient(ctx, aset.dst, dstGrad);
          }
        }
      }

      for (auto &[dstKey, aset] : gradEffects->assignments) {
        if (aset.dst) {
          grads.addDeps(ctx, aset.dst);
        }
        if (auto srcPhi = aset.getSrcPhi(ctx)) {
          grads.addDeps(ctx, srcPhi);
        }
      }

      if (ctx.reg->verbose >= 2) ctx.logBackprop("Backprop " + shellEscape(symName) + " fwdOrder.size()=" + repr(grads.fwdOrder.size()));

      for (auto &[dstKey, aset] : gradEffects->assignments) {
        if (aset.dst) {
          if (auto srcPhi = aset.getSrcPhi(ctx)) {
            if (auto dstg = grads.getTotalGradient(ctx, aset.dst)) {
              if (ctx.reg->verbose >= 2) ctx.logBackprop("Found grad for assignment to " + repr_ref(aset.dst));
              grads.addGradient(ctx(aset.dst), srcPhi, dstg);
            }
            else {
              if (ctx.reg->verbose >= 2) ctx.logBackprop("No grad (because no dstg) for assignment to " + repr_ref(aset.dst));
            }
          } else {
            if (ctx.reg->verbose >= 2) ctx.logBackprop("No grad (because no srcPhi) for assignment to " + repr_ref(aset.dst));
          }
        }
      }

      for (auto it = grads.fwdOrder.rbegin(); it != grads.fwdOrder.rend(); it++) {
        (*it)->backprop(ctx(*it), grads);
      }

      for (auto &[argType, argName] : effects->inArgs) {
        // Don't write the grads for things with no linear ops.
        // This avoids a terrible hit when there are large pixel arrays as inputs.
        if (!argType->hasLinearOps()) continue;
        if (argName == "dt") throw logic_error("Why is dt an inArg?");
        auto inVar =  ctx(argName.c_str()).mkExpr<ExprVar>(argType, symName, "in."s + argName, YD_IN);
        auto inVarGrad = ctx(argName.c_str()).mkExpr<ExprVar>(argType, symName, "inGrad."s + argName, YD_OUT);
        auto g = grads.getTotalGradient(ctx, inVar);
        if (g) {
          if (ctx.reg->verbose >= 2) {
            ctx.logBackprop(
              "  Found grad for inArg " + repr_ref(inVar) +
              "  Destination is " + repr_ref(inVarGrad) +
              "  Grad is " + repr_ref(g));
          }
          auto &aset = gradEffects->assignments[inVarGrad->cseKey];
          aset.dst = inVarGrad;
          aset.srcs.emplace_back(g, ctx.mkExpr<ExprConstDouble>(1.0));
        }
      }

      // Inefficient: we should somehow keep track of which params are relevant instead of
      // having to scan them all
      for (auto paramInfo : ctx.reg->paramsByIndex) {
        if (!paramInfo) continue;
        auto param = ctx.mkExpr<ExprParam>(paramInfo);
        auto g = grads.getTotalGradient(ctx, param);
        if (g) {
          if (ctx.reg->verbose >= 2) {
            ctx.logBackprop("  Found grad for param: " + paramInfo->paramName + "\n" + repr(*param));
          }

          auto paramGrad = ctx.mkExpr<ExprParamGrad>(paramInfo);

          auto &aset = gradEffects->assignments[paramGrad->cseKey];
          aset.mode = AUGMODE_PLUSEQ_INPLACE;
          aset.dst = paramGrad;
          aset.srcs.emplace_back(g, ctx.mkExpr<ExprConstDouble>(1.0));
        }
      }

      if (ctx.reg->verbose >= 2) ctx.logBackprop("  Assignments for gradEffects:\n" + repr(*gradEffects));
    }
  }
  return true;
}

void ExprNode::addChildDeps(YogaContext const &ctx, GradientExprSet &grads)
{
  for (auto &it : args) {
    grads.addDeps(ctx, it);
  }
}

void ExprPhi::addChildDeps(YogaContext const &ctx, GradientExprSet &grads)
{
  // Can't delegate to ExprNode, since we put the dst in the args
  for (auto &[value, modulation] : aset.srcs) {
    grads.addDeps(ctx, value);
    grads.addDeps(ctx, modulation);
  }
}


void ExprNode::backprop(YogaContext const &ctx, GradientExprSet &grads)
{
  auto g = grads.getTotalGradient(ctx, this);
  if (g) {
    backprop(ctx, grads, g);
  }
}

void ExprNode::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("No backprop for " + repr(this));
}

void ExprConst::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}


void ExprParam::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing to do here. Gradients flowing into a param are extracted and summed at the end
  // of the .yogagrad function and written into yogaParamsLinGrad.
}

void ExprParamGrad::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}

void ExprVar::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing to do here. Gradients flowing into a var are extracted and summed at the end
  // of the .yogagrad function and written into the inArgGrad.XXX argument.
}

void ExprReturnVal::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}

void ExprReturnGrad::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}

void ExprMkComplex::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  auto gReal = ctx.mkExpr<ExprExtractReal>(g);
  auto gImag = ctx.mkExpr<ExprExtractImag>(g);
  grads.addGradient(ctx, args[0], gReal);
  grads.addGradient(ctx, args[1], gImag);
}


void ExprExtractReal::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMkComplex>(g, ctx.mkExpr<ExprConstDouble>(0.0)));
}

void ExprExtractImag::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMkComplex>(ctx.mkExpr<ExprConstDouble>(0.0), g));
}


void ExprStructref::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  vector<ExprNode *> mvs;
  auto a0tStruct = args[0]->type->asStruct();
  assert(a0tStruct);
  for (auto &it : a0tStruct->members) {
    if (it->memberName == memberName) {
      mvs.push_back(g);
    }
    else {
      mvs.push_back(ctx.mkExpr<ExprConstZero>(it->memberType));
    }
  }
  grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMkStruct>(args[0]->type->asStruct(), mvs));
}

void ExprMkStruct::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == type->asStruct()->members.size());
  for (size_t i = 0; i < args.size(); i++) {
    auto memberInfo = type->asStruct()->members[i];
    auto memGrad = ctx.mkExpr<ExprStructref>(g, memberInfo->memberName, memberInfo);
    grads.addGradient(ctx, args[i], memGrad);
  }
}

void ExprMkList::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("backprop: list unimplemented");
}

void ExprAdd::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  grads.addGradient(ctx, args[0], g);
  grads.addGradient(ctx, args[1], g);
}

void ExprSub::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  grads.addGradient(ctx, args[0], g);
  grads.addGradient(ctx, args[1], ctx.mkExpr<ExprNeg>(g));
}

void ExprMul::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // https://en.wikipedia.org/wiki/Product_rule
  assert(args.size() == 2);
  if (g->type->asStruct() && args[0]->type->isScalar() && args[1]->type->asStruct()) {
    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprDot>(g, args[1]));
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprMul>(args[0], g));
  }
  else if (g->type->asMatrix() && args[0]->type->asMatrix() && args[1]->type->asMatrix()) {
    /*
      EG, R[4,4] * R[4,1] => R[4,1]
      g is R[4,1]
      args[0] is R[4,4]
      args[1] is R[4,1]
      g * args[1] is R[4,1] * R[4,1] => R[4,4]
    */
#if 1
    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMul>(g, ctx.mkExpr<ExprConjugate>(args[1])));
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprMul>(ctx.mkExpr<ExprConjugate>(args[0]), g));
#else
    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMul>(g, ctx.mkExpr<ExprConjugate>(args[1])));
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprMul>(args[0], g));
#endif
  }
  else if (g->type->asMatrix() && args[0]->type->isScalar() && args[1]->type->asMatrix()) {
    // ignore grad to args[0], requires outer product
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprMul>(args[0], g));
  }
  else if (g->type->isScalar() && args[0]->type->isScalar() && args[1]->type->isScalar()) {
    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMul>(g, args[1]));
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprMul>(args[0], g));
  }
  else {
    ctx.logBackprop("Unimplemented: mul for "s + repr_type(args[0]->type) +
      " * " + repr_type(args[1]->type) +
      " => " + repr_type(g->type));
  }
}

void ExprConjugate::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  grads.addGradient(ctx, args[0], ctx.mkExpr<ExprConjugate>(g));
}


void ExprNograd::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}

void ExprDot::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("dot unimplemented");
  // WRITEME
}

void ExprDiv::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  // https://en.wikipedia.org/wiki/Quotient_rule
  if (args[0]->type->isScalar() && args[1]->type->isScalar()) {

    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprDiv>(g, args[1]));
    grads.addGradient(ctx, args[1], ctx.mkExpr<ExprNeg>(
      ctx.mkExpr<ExprMul>(g,
        ctx.mkExpr<ExprDiv>(
          args[0],
          ctx.mkExpr<ExprPow>(
            args[1],
            ctx.mkExpr<ExprConstDouble>(2.0))))));
  }
  else {
    ctx.logBackprop("Unimplemented types for div");
  }
}

void ExprPow::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  // https://en.wikipedia.org/wiki/Power_rule
  if (args[0]->type->isScalar() && args[1]->type->isScalar()) {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(
        g, 
        ctx.mkExpr<ExprMul>(
          args[1],
          ctx.mkExpr<ExprPow>(
            args[0], 
            ctx.mkExpr<ExprSub>(
              args[1], 
              ctx.mkExpr<ExprConstDouble>(1.0))))));
  }
  else {
    ctx.logBackprop("Unimplemented types for pow");
  }
}

void ExprCompare::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  ctx.logBackprop("compare unimplemented");
  // WRITEME
}

void ExprLogicalOr::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  ctx.logBackprop("logicalOr unimplemented");
  // WRITEME for scalars, not bools
}

void ExprLogicalAnd::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  ctx.logBackprop("logicalAnd unimplemented");
  // WRITEME for scalars, not bools
}

void ExprNot::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  ctx.logBackprop("not unimplemented");
  // WRITEME for scalars, not bools
}

void ExprNeg::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  grads.addGradient(ctx, args[0],
    ctx.mkExpr<ExprNeg>(
      g));
}

void ExprLim01::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  ctx.logBackprop("lim01 unimplemented");
  // WRITEME
}

void ExprConvBoolDouble::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  ctx.logBackprop("convBoolDouble unimplemented");
  // nothing?
}

void ExprConvIntegerDouble::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  ctx.logBackprop("convIntegerDouble unimplemented");
  // nothing?
}

void ExprConvDoubleBool::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 1);
  ctx.logBackprop("convDoubleBool unimplemented");
  // nothing?
}

void ExprLinearComb::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("linearComb unimplemented");
  // WRITEME
}

void ExprMinMax::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  assert(args.size() == 2);
  ctx.logBackprop("minMax unimplemented");
  // WRITEME
}

void ExprClamp::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  if (op == "clamp") {
    assert(args.size() == 3);
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprClampGrad>("clampGradArg",
        vector<ExprNode *>{args[0], args[1], args[2], g}));
    grads.addGradient(ctx, args[1],
      ctx.mkExpr<ExprClampGrad>("clampGradMin",
        vector<ExprNode *>{args[0], args[1], args[2], g}));
    grads.addGradient(ctx, args[2],
      ctx.mkExpr<ExprClampGrad>("clampGradMax",
        vector<ExprNode *>{args[0], args[1], args[2], g}));
  }
  else if (op == "relu") {
    assert(args.size() == 1);
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprClampGrad>("reluGrad",
        vector<ExprNode *>{args[0], g}));
  }
  else {
    ctx.logBackprop("Clamp(" + op + ") unimplemented");
  }
}

void ExprClampGrad::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // nothing
}

void ExprTrig1::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  // https://en.wikipedia.org/wiki/Differentiation_rules
  assert(args.size() == 1);
  if (op == "exp") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(
        g,
        args[0]));
  }
  else if (op == "sin") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(
        g,
        ctx.mkExpr<ExprTrig1>("cos", args[0])));
  }
  else if (op == "cos") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(
        g,
        ctx.mkExpr<ExprNeg>(
          ctx.mkExpr<ExprTrig1>("sin", args[0]))));
  }
  else if (op == "log") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprDiv>(
        g,
        args[0]));
  }
  else if (op == "tanh") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(
        g,
        ctx.mkExpr<ExprTrig1>("one_minus_tanhsq", args[0])));
  }
  else if (op == "abs") {
    grads.addGradient(ctx, args[0],
      ctx.mkExpr<ExprMul>(g,
        ctx.mkExpr<ExprTrig1>("sign", args[0])));
  }
  else if (op == "normangle" || op == "normangle2") {
    grads.addGradient(ctx, args[0], g);
  }
  else if (op == "sign") {
    grads.addGradient(ctx, args[0], ctx.mkExpr<ExprMul>(g, ctx.mkExpr<ExprConstDouble>(0.001)));
  }
  // WRITEME: more ops
  else {
    ctx.logBackprop("Unhandled backprop ExprTrig1("s + op + ")");
  }
}

void ExprTrig2::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  if (op == "XXXatan2") {
    // WRITEME
  }
  else {
    ctx.logBackprop("Unhandled backprop ExprTrig2("s + op + ")");
  }
}


void ExprConstructor::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("constructor unimplemented");
  // WRITEME
}

void ExprMatrixConstructor::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("matrixConstructor unimplemented");
  // WRITEME
}

void ExprMatrixMath::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("matrixMath unimplemented");
  // WRITEME
}


void ExprSum::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  ctx.logBackprop("sum unimplemented");
  // WRITEME
}


void ExprMatrixIndex::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  if (args.size() == 2 && 
      args[0]->type->isMatrix() &&
      args[1]->type->isPrimitive()) {

    auto a0tm = args[0]->type->asMatrix();

    // This is fairly inefficient to create a list of all the elements, even though
    // only one is nonzero.
    int ri{0}, ci{0};
    if (args[1]->isNumericConst(ri)) {
      if (ri < 0 || ri >= a0tm->rows) {
        return ctx.logBackprop("ExprMatrixIndex::backprop: Matrix index not in-bounds integer"s);
      }
      auto zero = ctx.mkExpr<ExprConstZero>(g->type);
      vector<ExprNode *> gradArgs(a0tm->cols * a0tm->rows, zero);
      gradArgs.at(int(ci) * a0tm->rows + int(ri)) = g;
      grads.addGradient(ctx, args[0], ctx.mkExpr<ExprConstructor>(a0tm->clsName, gradArgs));
      return;
    }
  }

  ctx.logBackprop("matrixIndex unhandled case");
}

void ExprPhi::backprop(YogaContext const &ctx, GradientExprSet &grads, ExprNode *g)
{
  for (auto &[value, modulation] : aset.srcs) {
    if (g->type->hasLinearOps() && value->type->hasLinearOps()) {
      grads.addGradient(ctx, value, 
        ctx.mkExpr<ExprMul>(g, modulation));
    }
    else {
      grads.addGradient(ctx, value, g);
    }
  }
}

void ExprCallYoga::backprop(YogaContext const &ctx, GradientExprSet &grads)
{
  vector<ExprNode *> outArgGradIns;
  for (auto &it : outVals) {
    auto g = grads.getTotalGradientOrZero(ctx, it);
    outArgGradIns.push_back(g);
  }

  ExprCallYogaGrad *computeGrad = dynamic_cast<ExprCallYogaGrad *>(ctx.mkExpr<ExprCallYogaGrad>(
    funcName, args, outTypes,
    outArgGradIns, type));

  for (size_t i = 0; i < computeGrad->inArgGradOuts.size() && i < args.size(); i++) {
    auto g = computeGrad->inArgGradOuts[i];
    grads.addGradient(ctx, args[i], g);
  }
}

void ExprCallYogaGrad::backprop(YogaContext const &ctx, GradientExprSet &grads)
{
  // nothing
}
