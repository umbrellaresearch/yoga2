#include "./llvm_fwd.h"
#include "./expr.h"
#include "./type.h"
#include "./param.h"
#include "./effects.h"
#include "./emit_ctx.h"
#include "./value.h"
#include "./memory.h"
#include "./runtime.h"
#include "./expr_impl.h"
#include "../geom/geom.h"

using namespace llvm;


Type *
YogaType::getLlType(YogaContext const &ctx, EmitCtx &e) {
  if (!llType) {
    llType = makeLlType(ctx, e);
    if (!llType) return ctx.logErrorNull("Failed to make type for "s + clsName);
    if (clsName == "void") {
      llAllocSize = 0;
    }
    else {
      llAllocSize = e.dl.getTypeAllocSize(llType);
    }
    if (isStruct()) {
      asStruct()->llLayout = e.dl.getStructLayout(dyn_cast<StructType>(llType));
    }
  }
  return llType;
}


Type *YogaTypePrimitive::makeLlType(YogaContext const &ctx, EmitCtx &e)
{
  if (clsName == "R") {
    return Type::getDoubleTy(e.l);
  }
  else if (clsName == "void") {
    return Type::getVoidTy(e.l);
  }
  else if (clsName == "S64") {
    return Type::getInt64Ty(e.l);
  }
  else if (clsName == "U64") {
    return Type::getInt64Ty(e.l);
  }
  else if (clsName == "C") {
    return StructType::create(
      clsName,
      Type::getDoubleTy(e.l),
      Type::getDoubleTy(e.l));
  }
  else if (clsName == "F") {
    return Type::getFloatTy(e.l);
  }
  else if (clsName == "S32") {
    return Type::getInt32Ty(e.l);
  }
  else if (clsName == "U32") {
    return Type::getInt32Ty(e.l);
  }
  else if (clsName == "S16") {
    return Type::getInt16Ty(e.l);
  }
  else if (clsName == "U16") {
    return Type::getInt16Ty(e.l);
  }
  else if (clsName == "S8") {
    return Type::getInt8Ty(e.l);
  }
  else if (clsName == "U8") {
    return Type::getInt8Ty(e.l);
  }
  else if (clsName == "string") {
    return Type::getInt8PtrTy(e.l);
  }
  else if (clsName == "Object") {
    /*
      Like YogaObjectKv
    */
    auto kvCell = StructType::create(e.l, clsName + ".kv");
    kvCell->setBody({
      kvCell->getPointerTo(),
      Type::getInt8PtrTy(e.l),
      Type::getInt8PtrTy(e.l),
      Type::getInt8PtrTy(e.l)
    });

    return kvCell->getPointerTo();
  }
  else if (clsName == "bool") {
    return Type::getInt1Ty(e.l);
  }
  else if (clsName == "packet") {
    return Type::getInt8PtrTy(e.l); // opaque
  }
  else if (clsName == "MemoryResource") {
    return StructType::create(e.l, "memory_resource")->getPointerTo();
  }
  else {
    throw runtime_error("Unknown primitive type "s + clsName);
  }
}



Type *YogaTypePtr::makeLlType(YogaContext const &ctx, EmitCtx &e)
{
  auto elt = nonPtrType->getLlType(ctx, e);
  if (!elt) return nullptr;

  // a shared_ptr
  return StructType::create(clsName, elt->getPointerTo(), elt->getPointerTo());
}


Type *YogaTypeStruct::makeLlType(YogaContext const &ctx, EmitCtx &e)
{
  vector<Type *> llMembers;
  for (auto &memit : members) {
    auto elt = memit->memberType->getLlType(ctx, e);
    if (!elt) return nullptr;
    llMembers.push_back(elt);
  }
  return StructType::create(e.l, llMembers, clsName);
}

Type *YogaTypeMatrix::makeLlType(YogaContext const &ctx, EmitCtx &e)
{
  if (rows < 0 || cols < 0) {
    throw runtime_error("WRITEME: variable matrix");
    return StructType::create(
      clsName,
      Type::getInt32Ty(e.l),
      Type::getInt32Ty(e.l),
      elementType->getLlType(ctx, e)->getPointerTo());
  }
  else {
    return ArrayType::get(
      elementType->getLlType(ctx, e),
      (U64)rows * (U64)cols);
  }
}

Type *YogaTypeSet::makeLlType(YogaContext const &ctx, EmitCtx &e)
{
  return e.yogaSetTy;
}

/*
*/

bool YogaTypePrimitive::addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
{

  if (1) {
    auto b = startFn(prefix, "ptr"s, ctx.reg->stringType);
    e.builder->CreateStore(
      e.builder->CreateBitOrPointerCast(b.yv.p, e.builder->getInt8PtrTy()),
      b.cv.p);
    b.setPointerType(this);
    if (!b.end()) return false;
  }

  if (clsName == "R" || clsName == "bool") {
    auto b = startFn(prefix, "rd."s + typeid(double).name(), ctx.reg->rType); // read R as R
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "R" || clsName == "bool") {
    auto b = startFn(prefix, "wr."s + typeid(double).name(), ctx.reg->rType); // write R from R
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }

  // In LLVM we use int1 as bool, but we convert to int8 to store it
  if (clsName == "bool") {
    auto b = startFn(prefix, "rd."s + typeid(bool).name(), ctx.reg->boolType); // read bool as bool
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "bool") {
    auto b = startFn(prefix, "wr."s + typeid(bool).name(), ctx.reg->boolType); // write bool from bool
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }

  if (clsName == "S32" || clsName == "S64" || clsName == "S16" ||
      clsName == "U32" || clsName == "U64" || clsName == "U16") {
    auto b = startFn(prefix, "rd."s + typeid(double).name(), ctx.reg->rType); // read R from int
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "S32" || clsName == "S64" || clsName == "S16" ||
      clsName == "U32" || clsName == "U64" || clsName == "U16") {
    auto b = startFn(prefix, "wr."s + typeid(double).name(), ctx.reg->rType); // read R from int
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }

  if (clsName == "S32") {
    auto b = startFn(prefix, "rd."s + typeid(S32).name(), ctx.reg->s32Type);
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "S32") {
    auto b = startFn(prefix, "wr."s + typeid(S32).name(), ctx.reg->s32Type);
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "U32") {
    auto b = startFn(prefix, "rd."s + typeid(U32).name(), ctx.reg->u32Type);
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "U32") {
    auto b = startFn(prefix, "wr."s + typeid(U32).name(), ctx.reg->u32Type);
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "S64") {
    auto b = startFn(prefix, "rd."s + typeid(S64).name(), ctx.reg->s64Type);
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "S64") {
    auto b = startFn(prefix, "wr."s + typeid(S64).name(), ctx.reg->s64Type);
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "U64") {
    auto b = startFn(prefix, "rd."s + typeid(U64).name(), ctx.reg->u64Type);
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "U64") {
    auto b = startFn(prefix, "wr."s + typeid(U64).name(), ctx.reg->u64Type);
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }

  // WRITEME: convert to std::string, interning with yogac_alloc
  if (clsName == "string") {
    auto b = startFn(prefix, "rd."s + typeid(char *).name(), ctx.reg->stringType);
    if (!e.assign(ctx, b.cv, b.yv)) return false;
    if (!b.end()) return false;
  }
  if (clsName == "string") {
    auto b = startFn(prefix, "wr."s + typeid(char *).name(), ctx.reg->stringType);
    if (!e.assign(ctx, b.yv, b.cv)) return false;
    if (!b.end()) return false;
  }


  return true;
}

bool YogaTypePtr::addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
{
  return true;
}

bool YogaTypeStruct::addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
{
  if (1) {
    auto b = startFn(prefix, "ptr"s, ctx.reg->stringType);
    e.builder->CreateStore(
      e.builder->CreateBitOrPointerCast(b.yv.p, e.builder->getInt8PtrTy()),
      b.cv.p);
    b.setPointerType(this);
    if (!b.end()) return false;
  }

  for (auto &memit : members) {
    auto subPrefix = dotJoin(prefix, memit->memberName);
    auto subStartFn = [&e, prefix, startFn, subPrefix, memit](string const &fn, string const &opType, YogaType *cType) {
      auto b = startFn(fn, opType, cType);
      b.yv.p = e.builder->CreateStructGEP(
        b.yv.p,
        memit->llIdx);
      b.yv.t = memit->memberType;
      b.yv.v = nullptr;
      return b;
    };

    if (!memit->memberType->addAccessors(ctx, e, subPrefix, subStartFn)) return false;
  }

  
  return true;
}

type_info const &YogaTypeMatrix::glmFloatTypeid() const
{
  if (elementType->isReal()) {
    if (rows == 2 && cols == 2) return typeid(glm::mat2);
    if (rows == 3 && cols == 3) return typeid(glm::mat3);
    if (rows == 4 && cols == 4) return typeid(glm::mat4);
    if (rows == 2 && cols == 1) return typeid(glm::vec2);
    if (rows == 3 && cols == 1) return typeid(glm::vec3);
    if (rows == 4 && cols == 1) return typeid(glm::vec4);
  }
  return typeid(void);
}

type_info const &YogaTypeMatrix::glmDoubleTypeid() const
{
  if (elementType->isReal()) {
    if (rows == 2 && cols == 2) return typeid(glm::dmat2);
    if (rows == 3 && cols == 3) return typeid(glm::dmat3);
    if (rows == 4 && cols == 4) return typeid(glm::dmat4);
    if (rows == 2 && cols == 1) return typeid(glm::dvec2);
    if (rows == 3 && cols == 1) return typeid(glm::dvec3);
    if (rows == 4 && cols == 1) return typeid(glm::dvec4);
  }
  return typeid(void);
}


bool YogaTypeMatrix::addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
{
  auto const &floatTid = glmFloatTypeid();
  //auto const &doubleTid = glmDoubleTypeid();

  if (1) {
    auto b = startFn(prefix, "ptr"s, ctx.reg->stringType);
    e.builder->CreateStore(
      e.builder->CreateBitOrPointerCast(b.yv.p, e.builder->getInt8PtrTy()),
      b.cv.p);
    b.setPointerType(const_cast<YogaTypeMatrix *>(this)); // FIXME
    if (!b.end()) return false;
  }

  if (0) {
    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto rcName = "r"s + to_string(ri) + "c"s + to_string(ci);
        auto b = startFn(dotJoin(prefix, rcName), "ptr", ctx.reg->stringType);
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.builder->CreateBitOrPointerCast(yvCellp, e.builder->getInt8PtrTy()),
          b.cv.p);
        b.setPointerType(elementType);
        if (!b.end()) return false;
      }
    }
  }

  if (floatTid != typeid(void)) {
    auto b = startFn(prefix, "rd."s + floatTid.name(), ctx.reg->floatType);
    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getFloatTy(),
          b.cv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(yvCellp), cvCellp->getType()->getPointerElementType()),
          cvCellp);
      }
    }
    if (!b.end()) return false;
  }

  if (floatTid != typeid(void)) {
    auto b = startFn(prefix, "wr."s + floatTid.name(), ctx.reg->floatType);
    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getFloatTy(),
          b.cv.p,
          ri + ci * rows);
        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(cvCellp), yvCellp->getType()->getPointerElementType()),
          yvCellp);
      }
    }
    if (!b.end()) return false;
  }

  if (1) {
    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto rcName = "r"s + to_string(ri) + "c"s + to_string(ci);
        auto subPrefix = dotJoin(prefix, rcName);
        auto subStartFn = [this, &e, prefix, startFn, subPrefix, ri, ci, rcName](string const &fn, string const &opType, YogaType *cType) {
          auto b = startFn(fn, opType, cType);
          b.yv.p = e.builder->CreateStructGEP(
            b.yv.p,
            ri + ci * rows);
          b.yv.t = elementType;
          b.yv.v = nullptr;
          if (!b.yv.p) throw runtime_error("addDoubleAccessors: GEP2_32");
          return b;
        };

        if (!elementType->addAccessors(ctx, e, subPrefix, subStartFn)) return false;
      }
    }
  }

  if (1) {
    auto b = startFn(prefix, "wr."s + typeid(R *).name(), ctx.reg->rType);

    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getDoubleTy(),
          b.cv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(cvCellp), yvCellp->getType()->getPointerElementType()),
          yvCellp);
      }
    }
    if (!b.end()) return false;
  }

  if (1) {
    auto b = startFn(prefix, "rd."s + typeid(R *).name(), ctx.reg->rType);

    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getDoubleTy(),
          b.cv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(yvCellp), cvCellp->getType()->getPointerElementType()),
          cvCellp);
      }
    }
    if (!b.end()) return false;
  }

  if (1) {
    auto b = startFn(prefix, "wr."s + typeid(S32 *).name(), ctx.reg->s32Type);

    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getInt32Ty(),
          b.cv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(cvCellp), yvCellp->getType()->getPointerElementType()),
          yvCellp);

      }
    }
    if (!b.end()) return false;
  }

  if (1) {
    auto b = startFn(prefix, "rd."s + typeid(S32 *).name(), ctx.reg->s32Type);

    for (int ci = 0; ci < min(4, cols); ci++) {
      for (int ri = 0; ri < min(4, rows); ri++) {
        auto yvCellp = e.builder->CreateStructGEP(
          b.yv.p,
          ri + ci * rows);
        auto cvCellp = e.builder->CreateConstInBoundsGEP1_32(
          e.builder->getInt32Ty(),
          b.cv.p,
          ri + ci * rows);

        e.builder->CreateStore(
          e.cast(e.builder->CreateLoad(yvCellp), cvCellp->getType()->getPointerElementType()),
          cvCellp);
      }
    }
    if (!b.end()) return false;
  }


  return true;
}

bool YogaTypeSet::addAccessors(
    YogaContext const &ctx, EmitCtx &e,
    string const &prefix,
    std::function<AccessorBuilder (string const &fn, string const &opType, YogaType *cType)> startFn)
{

  return true;
}

void AccessorBuilder::setPointerType(YogaType *ptrType)
{
  topLevelType->ptrTypesByName[subName + "::ptr"] = ptrType;
}


bool AccessorBuilder::end()
{
  e.builder->CreateRetVoid();
  if (!e.endFunction()) return false;

  auto implName = dotJoin(dotJoin(dotJoin(topLevelType->clsName, subName), opName), "yogaAccessor");
  auto accessorKey = subName + "::" + opName;
  e.onResolved[implName].push_back([t=topLevelType, accessorKey](void *symAddr) {
    if (symAddr) {
      t->accessorsByName[accessorKey] = (CallableYogaAccessor)symAddr;
    }
    return true;
  });
  return true;
}

bool YogaCompilation::generateLlLinearOpsFor(YogaContext const &ctx, EmitCtx &e, YogaType *t)
{
  enum {
    ARG_OUT,
    ARG_IN_COEFF_A,
    ARG_IN_A,
    ARG_IN_COEFF_B,
    ARG_IN_B,
    ARG_MEM,
    ARG_COUNT,
  };
  if (t->hasLinearOps()) {

    if (!t->llType || t->llType->isVoidTy()) throw runtime_error("Missing llType");

    auto implName = dotJoin(t->clsName, "yogaLinear");

    auto f = e.startFunction(e.yogaLinearOpTy, implName);

    if (!e.settp(ctx, e.argPtrs["out"], t,
      e.builder->CreateBitCast(&f->arg_begin()[ARG_OUT], t->llType->getPointerTo())))
      throw runtime_error("settp failed");

    if (!e.settv(ctx, e.argPtrs["coeffA"], ctx.reg->rType,
      &f->arg_begin()[ARG_IN_COEFF_A]))
      throw runtime_error("settp failed");

    if (!e.settp(ctx, e.argPtrs["a"], t,
      e.builder->CreateBitCast(&f->arg_begin()[ARG_IN_A], t->llType->getPointerTo()))) 
      throw runtime_error("settp failed");

    if (!e.settv(ctx, e.argPtrs["coeffB"], ctx.reg->rType,
      &f->arg_begin()[ARG_IN_COEFF_B]))
      throw runtime_error("settp failed");

    if (!e.settp(ctx, e.argPtrs["b"], t,
      e.builder->CreateBitCast(&f->arg_begin()[ARG_IN_B], t->llType->getPointerTo())))
      throw runtime_error("settp failed");

    if (!e.settv(ctx, e.argPtrs["mem"], ctx.reg->memoryResourceType,
      &f->arg_begin()[ARG_MEM]))
      throw runtime_error("settp failed");

    auto inArgCoeffA = ctx.mkExpr<ExprVar>(ctx.reg->rType, "+"s, "coeffA"s, YD_IN);
    auto inArgA = ctx.mkExpr<ExprVar>(t, "+"s, "a"s, YD_IN);
    auto inArgCoeffB = ctx.mkExpr<ExprVar>(ctx.reg->rType, "+"s, "coeffB"s, YD_IN);
    auto inArgB = ctx.mkExpr<ExprVar>(t, "+"s, "b"s, YD_IN);
    auto outSumDst = ctx.mkExpr<ExprVar>(t, "+"s, "out"s, YD_OUT);

    auto a1 = ctx.mkExpr<ExprMul>(inArgCoeffA, inArgA);
    auto b1 = ctx.mkExpr<ExprMul>(inArgCoeffB, inArgB);
    if (!a1 || !b1) return false;
    auto sum = ctx.mkExpr<ExprAdd>(a1, b1);
    if (!sum) return false;

    ExprOut outSumDstCell(t);
    if (!outSumDst->emit(ctx, e, outSumDstCell)) return false;

    ExprOut sumCell(t);
    if (!sum->emit(ctx, e, sumCell)) return false;

    if (!e.assign(ctx, outSumDstCell, sumCell)) return false;

    e.builder->CreateRetVoid();
    if (!e.endFunction()) return false;

    e.onResolved[implName].push_back([t](void *symAddr) {
      if (symAddr) {
        t->linearOp = (CallableYogaLinearOp)symAddr;
      }
      return true;
    });
  }
  return true;
}


bool YogaCompilation::generateLlNormSqFor(YogaContext const &ctx, EmitCtx &e, YogaType *t)
{
  enum {
    ARG_IN_A,
    ARG_COUNT,
  };
  if (t->hasLinearOps()) {

    if (!t->llType || t->llType->isVoidTy()) throw runtime_error("Missing llType");

    auto implName = dotJoin(t->clsName, "norm");

    auto f = e.startFunction(e.yogaNormTy, implName);

    if (!e.settp(ctx, e.argPtrs["a"], t,
      e.builder->CreateBitCast(&f->arg_begin()[ARG_IN_A], t->llType->getPointerTo())))
      throw runtime_error("settp failed");

    auto inArgA = ctx.mkExpr<ExprVar>(t, implName, "a"s, YD_IN);
    auto outSum = ctx.mkExpr<ExprSum>("normsq", inArgA);
    if (!outSum) return false;
    ExprOut outSumVal(ctx.reg->rType);
    if (!outSum->emit(ctx, e, outSumVal)) return false;

    e.builder->CreateRet(e.getv(outSumVal));
    if (!e.endFunction()) return false;

    e.onResolved[implName].push_back([t](void *symAddr) {
      if (symAddr) {
        t->normSqOp = (CallableYogaNormOp)symAddr;
      }
      return true;
    });
  }
  return true;
}


bool YogaCompilation::generateLlAccessorsFor(YogaContext const &ctx, EmitCtx &e, YogaType *t)
{
  enum {
    ARG_YV,
    ARG_CV,
    ARG_COUNT,
  };
  if (!t->addAccessors(ctx("addAccessors"), e, "",
    [&ctx, &e, t](string const &fn, string const &opName, YogaType *cType) {
      auto implName = dotJoin(dotJoin(dotJoin(t->clsName, fn), opName), "yogaAccessor");

      auto f = e.startFunction(e.yogaAccessorTy, implName);

      ExprOut yv(t), cv(cType);

      if (!t->llType) throw runtime_error("Missing llType");
      if (!cType->llType) throw runtime_error("Missing llType");

      yv.p = e.builder->CreateBitCast(&f->arg_begin()[ARG_YV], t->llType->getPointerTo());
      cv.p = e.builder->CreateBitCast(&f->arg_begin()[ARG_CV], cType->llType->getPointerTo());

      return AccessorBuilder(
        ctx,
        e,
        t,
        fn,
        opName,
        yv,
        cv);

     })) {
    return false;
  }
  return true;
}


CallableYogaAccessor YogaCompilation::getYogaAccessor(string const &clsName, string const &subName, string const &opName)
{
  auto &slot = symbolTable[clsName];
  if (slot.type) {
    return slot.type->accessorsByName[subName + "::" + opName];
  }
  return nullptr;
}


/*
  packetRd.
  Note: Ideally, we should pass the memory pool (e.argPtrs["mem"]) to the packet_rd_value routines.
  For now, any variable-sized data structures are allocated out of the global pool.
*/

bool YogaTypePrimitive::addPacketRd(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  llvm::FunctionCallee packetRdFunc{nullptr};
  //void *packetRdAddr{nullptr};
  if (clsName == "R") {
    //packetRdAddr = static_cast<void(*)(packet &, double &)>(&packetio::packet_rd_value);
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRd"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "F") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRf"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "bool") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRb"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "S32") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRi"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "U32") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRj"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "S64") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRx"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "U64") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRy"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "S16") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRs"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "U16") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRt"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "S8") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRc"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "U8") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRh"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "C") {
  #ifdef __APPLE__
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRNSt3__17complexIdEE"s, e.packetRdValueFuncTy);
  #else
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRSt7complexIdE"s, e.packetRdValueFuncTy);
  #endif
  }
  else if (clsName == "string") {
    packetRdFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_rd_valueER6packetRPc"s, e.packetRdValueFuncTy);
  }
  else if (clsName == "void" || clsName == "Object" || clsName == "packet" || clsName == "MemoryResource") {
    return true;
  }
  else {
    ctx.logWarning("addPacketRd: no handler for primitive type " + shellEscape(clsName));
    return true;
  }

  e.builder->CreateCall(packetRdFunc, {
    packetPtr,
    e.asU8Ptr(objPtr)
  });
  return true;
}

bool YogaTypePtr::addPacketRd(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  ctx.logError("addPacketRd: no handler for ptr type " + shellEscape(clsName));
  return false;
}

bool YogaTypeStruct::addPacketRd(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  for (auto &memit : members) {
    auto memPtr = e.builder->CreateStructGEP(
      objPtr,
      memit->llIdx);
    if (!memit->memberType->addPacketRd(ctx(memit->sourceLoc), e, memPtr, packetPtr)) return false;
  }
  return true;
}

bool YogaTypeMatrix::addPacketRd(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  if (elementType->clsName == "U8") {
    // WRITEME
  }
  else {
    for (auto ci = 0; ci < cols; ci++) {
      for (auto ri = 0; ri < rows; ri++) {
        auto memPtr = e.builder->CreateStructGEP(
          objPtr,
          ci * rows + ri,
          "access_r"s + to_string(ri) + "c" + to_string(ci));
        if (!elementType->addPacketRd(ctx, e, memPtr, packetPtr)) return false;
      }
    }
  }
  return true;
}

bool YogaTypeSet::addPacketRd(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  /*
    Like:
      packetio::packet_rd_value(p, *x);
  */

  auto packetRdDynamicFunc = e.m->getOrInsertFunction(
    "_ZN8packetio15packet_rd_valueER6packetRP11YogaSetConsRK11YogaContext"s, 
    e.builder->getVoidTy(),
    e.builder->getInt8PtrTy(), 
    e.builder->getInt8PtrTy(), 
    e.builder->getInt8PtrTy());

  auto ctxPtr = new YogaContext(ctx); // leaks
  
  e.builder->CreateCall(packetRdDynamicFunc, {
    packetPtr,
    e.asU8Ptr(objPtr),
    e.emitTypedPtr(ctx, e.builder->getInt8PtrTy(), ctxPtr)
  });
  return true;
}


bool YogaCompilation::generateLlPacketRdFor(YogaContext const &ctx, EmitCtx &e, YogaType *t)
{
  if (t == ctx.reg->voidType) return true;
  auto implName = dotJoin(t->clsName, "yogaPacketRd");

  auto f = e.startFunction(e.packetRdValueFuncTy, implName);

  auto packetPtr = &f->arg_begin()[0];
  if (!t->llType) throw runtime_error("Missing llType");
  auto basePtr = e.builder->CreateBitCast(&f->arg_begin()[1], t->llType->getPointerTo());

  if (!t->addPacketRd(ctx(t->sourceLoc), e, basePtr, packetPtr)) return false;

  e.builder->CreateRetVoid();

  if (!e.endFunction()) return false;

  e.onResolved[implName].push_back([t](void *symAddr) {
    if (symAddr) {
      t->packetRd = (CallableYogaPacketRd)symAddr;
    }
    return true;
  });

  return true;
}



/*
  packetWr
*/

bool YogaTypePrimitive::addPacketWr(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  llvm::FunctionCallee packetWrFunc{nullptr};
  if (clsName == "R") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKd"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "F") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKf"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "bool") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKb"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "S32") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKi"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "U32") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKj"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "S64") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKx"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "U64") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKy"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "S16") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKs"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "U16") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKt"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "S8") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKc"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "U8") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKh"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "C") {
#ifdef __APPLE__
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKNSt3__17complexIdEE"s, e.packetWrValueFuncTy);
#else
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKSt7complexIdE"s, e.packetWrValueFuncTy);
#endif
  }
  else if (clsName == "string") {
    packetWrFunc = e.m->getOrInsertFunction("_ZN8packetio15packet_wr_valueER6packetRKPKc"s, e.packetWrValueFuncTy);
  }
  else if (clsName == "void" || clsName == "Object" || clsName == "packet" || clsName == "MemoryResource") {
    return true;
  }
  else {
    ctx.logWarning("addPacketWr: no handler for primitive type " + shellEscape(clsName));
    return true;
  }

  e.builder->CreateCall(packetWrFunc, {
    packetPtr,
    e.asU8Ptr(objPtr)
  });
  return true;
}

bool YogaTypePtr::addPacketWr(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  ctx.logError("addPacketWr: no handler for ptr type " + shellEscape(clsName));
  return false;
}

bool YogaTypeStruct::addPacketWr(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  for (auto &memit : members) {
    auto memPtr = e.builder->CreateStructGEP(
      objPtr,
      memit->llIdx,
      "access_"s + memit->memberName);
    if (!memit->memberType->addPacketWr(ctx(memit->sourceLoc), e, memPtr, packetPtr)) return false;
  }
  return true;
}

bool YogaTypeMatrix::addPacketWr(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  if (elementType->clsName == "U8") {
    // WRITEME
  }
  else {
    for (auto ci = 0; ci < cols; ci++) {
      for (auto ri = 0; ri < rows; ri++) {
        auto memPtr = e.builder->CreateStructGEP(
          objPtr,
          ci * rows + ri,
          "access_r"s + to_string(ri) + "c" + to_string(ci));
        if (!elementType->addPacketWr(ctx, e, memPtr, packetPtr)) return false;
      }
    }
  }
  return true;
}

bool YogaTypeSet::addPacketWr(YogaContext const &ctx, EmitCtx &e,
    llvm::Value *objPtr,
    llvm::Value *packetPtr) const
{
  auto packetWrDynamicFunc = e.m->getOrInsertFunction(
    "_ZN8packetio15packet_wr_valueER6packetRKP11YogaSetCons"s, 
    e.builder->getVoidTy(),
    e.builder->getInt8PtrTy(), 
    e.builder->getInt8PtrTy());

  e.builder->CreateCall(packetWrDynamicFunc, {
    packetPtr,
    e.builder->CreateBitCast(objPtr, e.builder->getInt8PtrTy()),
  });
  return true;
}


bool YogaCompilation::generateLlPacketWrFor(YogaContext const &ctx, EmitCtx &e, YogaType *t)
{
  if (t == ctx.reg->voidType) return true;
  auto implName = dotJoin(t->clsName, "yogaPacketWr");

  auto f = e.startFunction(e.packetWrValueFuncTy, implName);

  auto packetPtr = &f->arg_begin()[0];
  auto basePtr = e.builder->CreateBitCast(&f->arg_begin()[1], t->llType->getPointerTo());

  if (!t->addPacketWr(ctx(t->sourceLoc), e, basePtr, packetPtr)) return false;

  e.builder->CreateRetVoid();

  if (!e.endFunction()) return false;

  e.onResolved[implName].push_back([t](void *symAddr) {
    if (symAddr) {
      t->packetWr = (CallableYogaPacketWr)symAddr;
    }
    return true;
  });

  return true;
}

// ---------------
