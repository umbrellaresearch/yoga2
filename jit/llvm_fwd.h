#pragma once
#include "common/std_headers.h"
#include "common/str_utils.h"

namespace llvm {
  class Value;
  class Type;
  class Function;
  class FunctionType;
  class AllocaInst;
  class Error;
  class Module;
  class StructLayout;
  class PassBuilder;
}


template<> string repr(llvm::Error const &a);
template<> string repr(llvm::Type * const &ty);
template<> string repr(llvm::Value * const &a);
template<> string repr(llvm::FunctionType * const &a);