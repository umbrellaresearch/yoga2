#pragma once
#include "./jit_utils.h"

apr_pool_t *new_yogic_pool(apr_pool_t *parent, char const *name);
void free_yogic_pool(apr_pool_t *mem);

extern "C" U8 *yogic_alloc(S64 x, apr_pool_t *mem);

U8 *yogic_intern(string const &s);

struct YogaPool {
  YogaPool(apr_pool_t *_parent = nullptr);
  ~YogaPool();
  apr_pool_t *it;
};
