#pragma once
#include "./compilation.h"
#include "./expr.h"


template<typename T, typename ...Args>
T * YogaContext::mkExprRaw(Args&& ...args) const
{
  auto ptr = std::make_unique<T>(std::forward<Args>(args)...);
  T *ret = ptr.get();
  reg->exprPool.emplace_back(std::move(ptr));

  ret->sourceLoc = lastLoc;
  if (!ret->resolveTypes(*this)) {
    logError("Couldn't resolve types");
    if (reg->verbose >= 0) logExpr("incomplete expr:", ret);
    return nullptr;
  }
  if (ret->cseKey.empty()) {
    logError("cseKey not set");
    if (reg->verbose) logExpr("incomplete expr:", ret);
    throw runtime_error("cseKey not set");
  }
  return ret;
}

/*
  This returns a pointer to an ExprNode rather than the subclass T,
  because the peephole optimizer might return a different class.
  mkExprRaw<T> is guaranteed to return a T*
*/

template<typename T, typename ...Args>
ExprNode * YogaContext::mkExpr(Args&& ...args) const
{
  ExprNode *ret = mkExprRaw<T>(std::forward<Args>(args)...);
  if (!ret) return nullptr;
  int peepCnt = 0;
  while (1) {
    if (++peepCnt > 1000) {
      return logErrorNull("Flapping peephole");
    }
    auto better = ret->peephole(*this);
    if (better) {
      ret = better;
    }
    else {
      break;
    }
  }
  return ret;
}

