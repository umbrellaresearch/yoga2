#pragma once
#include "common/std_headers.h"
#include "common/str_utils.h"
#include "numerical/numerical.h"
#include <apr_pools.h>

void callAll(deque<std::function<void(void)>> &q);
void callAll(vector<std::function<void(void)>> &q);
void callPopAll(deque<std::function<void(void)>> &q);
