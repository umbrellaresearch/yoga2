#include "./jit_utils.h"

#if !defined(TLBCORE_VERSION_MAJOR) || TLBCORE_VERSION_MAJOR != 2 || TLBCORE_VERSION_MINOR != 8
#error "Yoga2 requires tlbcore version 2.8. Make sure to use git pull --recurse-submodules
#endif

void callAll(vector<std::function<void(void)>> &q)
{
  for (auto &f : q) {
    f();
  }
}

void callAll(deque<std::function<void(void)>> &q)
{
  for (auto &f : q) {
    f();
  }
}

void callPopAll(deque<std::function<void(void)>> &q)
{
  while (!q.empty()) {
    auto f = std::move(q.front());
    q.pop_front();
    f();
  }
}

