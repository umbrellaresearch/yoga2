#include "./ast.h"
#include "./compilation.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"

struct PendingRewrite {
  PendingRewrite(
    YogaSourceLoc const &_sourceLoc,
    string _newText)
    : sourceLoc(_sourceLoc),
      newText(std::move(_newText))
  {
  }
  YogaSourceLoc sourceLoc;
  string newText;
};

static string fmtShort(double value, double prec)
{
  ostringstream oss;
  oss.setf(std::ios_base::fixed);
  oss.precision(prec);
  oss << value;
  string ret = oss.str();
  if (ret.find('.') != string::npos) {
    while (ret.size() > 2 && ret[ret.size()-1] == '0' && ret[ret.size()-2] == '0') {
      ret.pop_back();
    }
  }
  return ret;
}

map<string, string> YogaCompilation::rewriteWithChanges()
{

  unordered_map<YogaSourceFile *, vector<PendingRewrite>> todoByFile;

  map<string, string> ret;

  auto ctx = mkCtx("rewriteWithChanges");

  for (auto paramInfo : paramsByIndex) {
    if (!paramInfo) continue;
    auto file = paramInfo->sourceLoc.file;
    if (!file) continue;
    if (file->isBuiltin) continue;

    if (1) {
      int paramExponent = paramInfo->literalExponent;

      if (paramInfo->literalScale != paramInfo->savedLiteralScale) {
        /*
          Try to make nice numbers. Maybe it'd be better to do this all
          manually rather than starting with printf
        */
        string newText = stringprintf("%0.1e", paramInfo->literalScale);
        while (1) {
          auto p = newText.find("e+");
          if (p == string::npos) break;
          newText = newText.substr(0, p+1) + newText.substr(p+2);
        }
        while (1) {
          auto p = newText.find("e0");
          if (p == string::npos) break;
          newText = newText.substr(0, p+1) + newText.substr(p+2);
        }
        while (1) {
          auto p = newText.find("e-0");
          if (p == string::npos) break;
          newText = newText.substr(0, p+2) + newText.substr(p+3);
        }
        while (1) {
          auto p = newText.find(".0e");
          if (p == string::npos) break;
          newText = newText.substr(0, p) + newText.substr(p+2);
        }
        paramExponent = int(floor(log10(paramInfo->literalScale)-2));

        /*
          Danger: multiple params can share the same scale, for instance if we have
            R[4](1,2,3,4)~10
          then there will be 4 params all sharing a scaleSourceLoc.
        */
        todoByFile[file].push_back(PendingRewrite(
          paramInfo->scaleSourceLoc,
          newText));
      }

      if (paramInfo->isExtern) {
        if (paramInfo->isValueChanged(ctx)) {
          auto paramFn = paramInfo->getFileName();
          if (!paramFn.empty()) {
            if (ret.find(paramFn) != ret.end()) {
              cerr << "Warning: multiple writes to " + paramFn + ", last one will win\n";
            }
            ret[paramFn] = paramInfo->getExternJson(ctx);
          }
        }
      }
      else if (paramInfo->paramDimension == 1) {
        R newValue = paramValues[paramInfo->paramIndex];
        R savedValue = savedParamValues[paramInfo->paramIndex];
        if (newValue != savedValue) {
          string newText;
          if (paramExponent != 0) {
            newText = fmtShort(newValue / pow(10, paramExponent), 3) + "e" + to_string(paramExponent);
          }
          else {
            newText = fmtShort(newValue, 3);
          }
          todoByFile[file].push_back(PendingRewrite(
            paramInfo->paramSourceLoc,
            newText));
        }
      }
      else {
        cerr << "Warning: non-extern param with dimension " + repr(paramInfo->paramDimension) + " not handled\n";
      }
    }
  }

  for (auto &todoit : todoByFile) {
    auto const &file = todoit.first;
    auto todoSorted = todoit.second;
    sort(
      todoSorted.begin(),
      todoSorted.end(), 
      [](
        PendingRewrite const &a,
        PendingRewrite const &b)
        {
          return a.sourceLoc.start < b.sourceLoc.start;
        });
    
    auto &origFile = file->text;
    string newContents;
    int pos = 0;
    for (auto &changeit : todoSorted) {
      if (changeit.sourceLoc.start > pos) {
        newContents.append(
          origFile.substr(pos, changeit.sourceLoc.start - pos));
        pos = changeit.sourceLoc.start;
      }
      // FIXME: this is probably wrong when we have multiple changes at the same
      // sourceLoc in a row. We should probably skip past 
      auto oldText = origFile.substr(changeit.sourceLoc.start,
        changeit.sourceLoc.end - changeit.sourceLoc.start);
      newContents.append(changeit.newText);
      pos = changeit.sourceLoc.end;
      if (0) cerr << "Rewrite " + shellEscape(oldText) + " => " + shellEscape(changeit.newText) + "\n";
    }
    newContents.append(origFile.substr(pos));

    ret[file->fn] = newContents;
  }
  return ret;
}

void YogaCompilation::rewriteFiles(map<string, string> const &nameToContents)
{
  time_t now{0};
  time(&now);
  struct tm t{};
  localtime_r(&now, &t);
  auto dateCode = getTimeTok(&t);

  for (auto const &it : nameToContents) {
    auto &fn = it.first;
    if (fn.front() == '[') {
      // we use "[string]" for test files that exist only in memory
      cerr << "rewriteFiles: Ignoring changes to file named " + shellEscape(fn) + "\n";
      continue;
    }
    auto &contents = it.second;

    string newName = fn + ".new";
    string bakName = fn + ".bak"s + dateCode;

    FILE *newf = fopen(newName.c_str(), "w");
    if (!newf) throw runtime_error(("Can't open "s + newName).c_str());

    size_t nw = fwrite(contents.data(), contents.size(), 1, newf);
    if (nw != 1) {
      throw runtime_error(("Unable to write "s + newName + ": "s + strerror(errno)).c_str());
    }
    if (fclose(newf) < 0) {
      throw runtime_error(("Unable to write "s + newName + ": "s + strerror(errno)).c_str());
    }

    if (!didParamBackupByFn[fn]) {
      if (rename(fn.c_str(), bakName.c_str()) < 0) {
        throw runtime_error(("Unable to rename "s + fn + ": "s + strerror(errno)).c_str());
      }
      didParamBackupByFn[fn] = true;
    }
    if (rename(newName.c_str(), fn.c_str()) < 0) {
      throw runtime_error(("Unable to rename "s + fn + ": "s + strerror(errno)).c_str());
    }
  }
}

bool YogaCompilation::paramsNeedSaving()
{
  return paramValueEpoch != paramSavedEpoch;
}

bool YogaCompilation::saveParamEdits()
{
  auto changes = rewriteWithChanges();
  rewriteFiles(changes);
  paramSavedEpoch = paramValueEpoch;
  savedParamValues = paramValues;
  return true;
}

bool YogaCompilation::revertParamEdits()
{
  paramValues = savedParamValues;
  paramValueEpoch ++;
  paramSavedEpoch = paramValueEpoch;
  return true;
}
