#include "./ast.h"
#include "./compilation.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"
#include "./expr_impl.h"



ExprNode *ExprNode::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  ctx.logError("WRITEME: deriv function"s);
  ctx.logExpr("expr:", this);
  return nullptr;
}

ExprNode *ExprConstZero::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}

ExprNode *ExprConstOne::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}

ExprNode *ExprConstDouble::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.mkExpr<ExprConstDouble>(0.0);
}

ExprNode *ExprConstComplex::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.mkExpr<ExprConstComplex>(0.0);
}

ExprNode *ExprParam::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt == this) {
    return ctx.mkExpr<ExprConstDouble>(1.0);
  }
  else {
    return ctx.mkExpr<ExprConstDouble>(0.0);
  }
}

ExprNode *ExprParamGrad::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt == this) {
    return ctx.mkExpr<ExprConstDouble>(1.0);
  }
  else {
    return ctx.mkExpr<ExprConstDouble>(0.0);
  }
}

ExprNode *ExprVar::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt == this) {
    return ctx.mkExpr<ExprConstDouble>(1.0);
  }
  else {
    return ctx.mkExpr<ExprConstDouble>(0.0);
  }
}

ExprNode *ExprReturnVal::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}


ExprNode *ExprReturnGrad::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}

ExprNode *ExprMkComplex::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.mkExpr<ExprConstZero>(type);
  // WRITEME
}

ExprNode *ExprExtractReal::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.mkExpr<ExprConstZero>(type);
  // WRITEME
}

ExprNode *ExprExtractImag::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.mkExpr<ExprConstZero>(type);
  // WRITEME
}


ExprNode *ExprStructref::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}

ExprNode *ExprMkStruct::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    vector<ExprNode *> memberDerivs;
    for (auto &it : args) {
      memberDerivs.push_back(it->deriv(ctx, wrt));
    }
    return ctx.mkExpr<ExprMkStruct>(type->asStruct(), memberDerivs);
  }
}

ExprNode *ExprAdd::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    auto d0 = args[0]->deriv(ctx, wrt);
    auto d1 = args[1]->deriv(ctx, wrt);
    if (!d0 || !d1) return nullptr;
    return ctx.mkExpr<ExprAdd>(d0, d1);
  }
}

ExprNode *ExprSub::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    auto d0 = args[0]->deriv(ctx, wrt);
    auto d1 = args[1]->deriv(ctx, wrt);
    if (!d0 || !d1) return nullptr;
    return ctx.mkExpr<ExprSub>(d0, d1);
  }
}

ExprNode *ExprMul::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    auto d0 = args[0]->deriv(ctx, wrt);
    auto d1 = args[1]->deriv(ctx, wrt);
    if (!d0 || !d1) return nullptr;
    return ctx.mkExpr<ExprAdd>(
      ctx.mkExpr<ExprMul>(args[0], d1),
      ctx.mkExpr<ExprMul>(d0, args[1])); // careful, matrix args aren't commutative
  }
}

ExprNode *ExprConjugate::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    auto d0 = args[0]->deriv(ctx, wrt);
    if (!d0) return nullptr;
    return ctx.mkExpr<ExprConjugate>(d0);
  }
}


ExprNode *ExprNograd::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprConstZero>(type);
  }
}

ExprNode *ExprDot::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.logErrorNull("WRITEME: deriv(dot))"s);
  }
}

ExprNode *ExprDiv::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  return ctx.logErrorNull("WRITEME: deriv for /"s);
}

ExprNode *ExprNeg::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.mkExpr<ExprNeg>(args[0]->deriv(ctx, wrt));
  }
}

ExprNode *ExprTrig1::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    auto a0 = args[0];
    auto da0 = a0->deriv(ctx, wrt);
    if (op == "sin") {
      return ctx.mkExpr<ExprMul>(da0, ctx.mkExpr<ExprTrig1>("cos", a0));
    }
    if (op == "cos") {
      return ctx.mkExpr<ExprNeg>(ctx.mkExpr<ExprMul>(da0, ctx.mkExpr<ExprTrig1>("sin", a0)));
    }
    if (op == "exp") {
      return ctx.mkExpr<ExprMul>(da0, this);
    }
    return ctx.logErrorNull("WRITEME: deriv for "s + op);
  }
}

ExprNode *ExprTrig2::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.logErrorNull("WRITEME: deriv for "s + op);
  }
}

ExprNode *ExprConstructor::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    vector<ExprNode *> dargs;
    for (auto &it : args) {
      auto darg = it->deriv(ctx, wrt);
      if (!darg) return ctx.logErrorNull("Missing derivative"s);
      dargs.push_back(darg);
    }
    auto ret = ctx.mkExpr<ExprConstructor>(op, dargs);
    if (!ret) return ctx.logErrorNull("No constructor for "s + op);
    return ret;
  }
}

ExprNode *ExprMatrixConstructor::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    vector<ExprNode *> dargs;
    for (auto &it : args) {
      auto dit = it->deriv(ctx, wrt);
      if (!dit) return ctx(it).logErrorNull("No derivative for arg");
      dargs.push_back(it);
      dargs.push_back(dit);
    }
    auto ret = ctx.mkExpr<ExprMatrixConstructor>(op + "Deriv"s, dargs);
    if (!ret) return ctx.logErrorNull("No deriv operator for "s + op);
    return ret;
  }
}

ExprNode *ExprSum::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.logErrorNull("No deriv operator for "s + op);
  }
}

ExprNode *ExprMatrixMath::deriv(YogaContext const &ctx, ExprNode *wrt) 
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.logErrorNull("No deriv operator for "s + op);
  }
}

ExprNode *ExprCallYoga::deriv(YogaContext const &ctx, ExprNode *wrt)
{
  if (wrt->cseKey == cseKey) {
    return ctx.mkExpr<ExprConstOne>(type);
  }
  else {
    return ctx.logErrorNull("WRITEME: ExprCallYoga::deriv"s);
  }
}
