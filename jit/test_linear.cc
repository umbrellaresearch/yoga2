#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"


TEST_CASE("Linear Ops: primitive types", "[compiler]") {
  auto reg = YOGAC(R"(
    // Foobar
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  for (string tn : {"R", "Foobar"}) {
    auto t = ctx.reg->getType(ctx, tn);
    REQUIRE(t->linearOp);

    auto yvInA = ctx.mkValue(t);
    auto yvInB = ctx.mkValue(t);
    auto yvOut = ctx.mkValue(t);

    if (tn == "Foobar") {
      yvInA.wr("foo", 5.0);
      yvInB.wr("foo", 7.0);
      yvInA.wr("bar", 11.0);
      yvInB.wr("bar", 13.0);
    }
    else {
      yvInA.wr("", 5.0);
      yvInB.wr("", 7.0);
    }

    (*t->linearOp)(yvOut.buf, 1.0, yvInA.buf, 1.0, yvInB.buf, ctx.reg->mem);

    if (tn == "Foobar") {
      R outFoo=0.0;
      yvOut.rd("foo", outFoo);
      CHECK(outFoo == 5.0+7.0);
      R outBar=0.0;
      yvOut.rd("bar", outBar);
      CHECK(outBar == 11.0+13.0);
    }
    else {
      R out=0.0;
      yvOut.rd("", out);

      CHECK(out == 5.0+7.0);
    }
  }

};

