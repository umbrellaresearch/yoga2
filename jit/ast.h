#pragma once
#include "./jit_utils.h"
#include "./jit_fwd.h"
#include "./context.h"
#include "./sourcefile.h"
#include <glm/mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

/*
  AstNodes (and its many subclasses) are usually pointed to by raw pointers.
  They're all owned by the YogaCompilation, which has a deque of unique_ptrs for them.

  shared_ptr has a noticeable performance penalty when iterating through the Ast many times.
  And nodes need a parent pointer, which would further complicate using shared_ptrs.

  Don't put much logic in the AstNodes. Logic for passes belongs in the passes.
  They should all define a dump(ostream, string const &indent) method
*/


struct AstNode {
  AstNode() = default;
  virtual ~AstNode() = default;
  AstNode(AstNode const &other) = delete;
  AstNode(AstNode &&other) = delete;
  AstNode & operator = (AstNode const &other) = delete;
  AstNode & operator = (AstNode &&other) = delete;

  YogaSourceLoc sourceLoc;
  AstNode *parent{nullptr};

  ExprNode *lv{nullptr};
  ExprNode *rv{nullptr};
  YogaParamInfo *paramInfo{nullptr};
  YogaType *expectedType{nullptr};

  virtual struct AstName *asName() { return nullptr; }
  virtual struct AstTernop *asTernop() { return nullptr; }
  virtual struct AstBinop *asBinop() { return nullptr; }
  virtual struct AstUnop *asUnop() { return nullptr; }
  virtual struct AstCall *asCall() { return nullptr; }
  virtual struct AstIndex *asIndex() { return nullptr; }
  virtual struct AstStructref *asStructref() { return nullptr; }
  virtual struct AstNumber *asNumber() { return nullptr; }
  virtual struct AstRangeSpec *asRangeSpec() { return nullptr; }
  virtual struct AstParam *asParam() { return nullptr; }
  virtual struct AstBoolean *asBoolean() { return nullptr; }
  virtual struct AstNull *asNull() { return nullptr; }
  virtual struct AstString *asString() { return nullptr; }
  virtual struct AstDeriv *asDeriv() { return nullptr; }
  virtual struct AstKeyValue *asKeyValue() { return nullptr; }
  virtual struct AstObject *asObject() { return nullptr; }
  virtual struct AstArray *asArray() { return nullptr; }
  virtual struct AstExtern *asExtern() { return nullptr; }
  virtual struct AstStatement *asStatement() { return nullptr; }
  virtual struct AstBlock *asBlock() { return nullptr; }
  virtual struct AstIf *asIf() { return nullptr; }
  virtual struct AstDefault *asDefault() { return nullptr; }
  virtual struct AstSimple *asSimple() { return nullptr; }
  virtual struct AstTypeDecl *asTypeDecl() { return nullptr; }
  virtual struct AstMemberDecl *asMemberDecl() { return nullptr; }
  virtual struct AstStructDef *asStructDef() { return nullptr; }
  virtual struct AstArgDecl *asArgDecl() { return nullptr; }
  virtual struct AstEnumDecl *asEnumDecl() { return nullptr; }
  virtual struct AstOnehot *asOnehot() { return nullptr; }
  virtual struct AstFunction *asFunction() { return nullptr; }
  virtual struct AstAnnoCall *asAnnoCall() { return nullptr; }

  [[nodiscard]] virtual bool getLiteral(string &ret) const { return false; }
  [[nodiscard]] virtual bool getLiteralName(string &ret) const { return false; }
  [[nodiscard]] virtual bool getLiteralKey(string &ret) const { return false; }
  [[nodiscard]] virtual bool getLiteralNumber(string &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(string &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(double &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(U64 &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(int &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(bool &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(glm::dmat4 &ret) const { return false; }
  [[nodiscard]] virtual bool getValue(vector<string> &ret) const { return false; }
  [[nodiscard]] virtual bool getValueNodeForKey(string const &key, AstNode *&ret) const { return false; }

  [[nodiscard]] virtual bool getLiteralForKey(string const &key, string &ret, bool required=false)
  {
    AstNode *valueNode{nullptr};
    if (getValueNodeForKey(key, valueNode)) {
      return valueNode->getLiteral(ret);
    }
    return !required;
  }

  template<typename T>
  [[nodiscard]] bool getValueForKey(string const &key, T &ret, bool required=false)
  {
    AstNode *valueNode{nullptr};
    if (getValueNodeForKey(key, valueNode)) {
      return valueNode->getValue(ret);
    }
    return !required;
  }

  void adopt(AstNode *p)
  {
    if (p) {
      if (p->parent) throw runtime_error("Already adopted");
      p->parent = this;
    }
  }

  [[nodiscard]] virtual bool scan(YogaContext &ctx, YogaPassBase *pass);
  virtual void dumpHead(ostream &s, string const &indent) const;
  virtual void dump(ostream &s, string const &indent, int depth) const;
  string desc() const;
  virtual string gloss() const;
};


struct AstExpr;
struct AstBlock;
struct AstArgDecl;


template<>
string repr(AstNode const * const &it);


struct AstExpr : AstNode {
};


struct AstName : AstExpr {
  AstName(
    string _name,
    bool _isClsName)
    : name(std::move(_name)),
      isClsName(_isClsName)
  {
  }

  string name;
  bool isClsName;


  [[nodiscard]] bool getLiteral(string &ret) const override { ret = name; return true; }
  [[nodiscard]] bool getValue(string &ret) const override { ret = name; return true; }
  [[nodiscard]] bool getLiteralName(string &ret) const override { ret = name; return true; }
  [[nodiscard]] bool getLiteralKey(string &ret) const override { ret = name; return true; }

  AstName * asName() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};



struct AstTernop : AstExpr {
  AstTernop(
    string _op, 
    AstExpr *_lhs,
    AstExpr *_mhs,
    AstExpr *_rhs)
    : op(std::move(_op)),
      lhs(_lhs),
      mhs(_mhs),
      rhs(_rhs)
  {
    adopt(lhs);
    adopt(mhs);
    adopt(rhs);
  }
  string op;
  AstExpr *lhs;
  AstExpr *mhs;
  AstExpr *rhs;

  struct AstTernop *asTernop() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstBinop : AstExpr {
  AstBinop(
    string _op,
    AstExpr *_lhs,
    AstExpr *_rhs)
    : op(std::move(_op)),
      lhs(_lhs),
      rhs(_rhs)
  {
    adopt(lhs);
    adopt(rhs);
  }
  string op;
  AstExpr *lhs;
  AstExpr *rhs;

  [[nodiscard]] bool isAssign() const {
    return op == "="s || op == "*="s || op == "+="s || op == "%="s || op == "/="s;
  }
  [[nodiscard]] bool isExpect() const {
    return op == "~~"s;
  }

  struct AstBinop *asBinop() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstUnop : AstExpr {
  AstUnop(
    string _op,
    AstExpr *_arg)
    : op(std::move(_op)),
      arg(_arg)
  {
    adopt(arg);
  }
  string op;
  AstExpr *arg;

  struct AstUnop *asUnop() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstCall : AstExpr {
  AstCall(
    AstName *_func,
    vector<AstExpr *> _args)
    : func(_func),
      args(move(_args))
  {
    literalFuncName = func->name;
    adopt(func);
    for (auto &it : args) {
      adopt(it);
    }
  }

  AstName *func;
  string literalFuncName;
  vector<AstExpr *> args;

  struct AstCall *asCall() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};


struct AstIndex : AstExpr {
  AstIndex(
    AstExpr *_lhs,
    vector<AstExpr *> _args)
    : lhs(_lhs),
      args(move(_args))
  {
    adopt(lhs);
    for (auto &it : args) {
      adopt(it);
    }
  }

  AstExpr *lhs;
  vector<AstExpr *> args;

  struct AstIndex *asIndex() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};


struct AstStructref : AstExpr {
  AstStructref(
    AstExpr *_lhs,
    AstName *_memberName)
    : lhs(_lhs),
      memberName(_memberName)
  {
    literalMemberName = memberName->name;
    adopt(lhs);
    adopt(memberName);
  }

  AstExpr *lhs;
  AstName *memberName;
  string literalMemberName;

  [[nodiscard]] bool getLiteral(string &ret) const override {
    string firstPart;
    if (lhs->getLiteral(firstPart)) {
      ret = firstPart + "."s + literalMemberName;
      return true;
    }
    return false;
  }

  struct AstStructref *asStructref() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};


struct AstNumber : AstExpr {
  AstNumber(
    string _literalNumber)
    : literalNumber(move(_literalNumber))
  {
    initValues();
  }

  string literalNumber;
  bool isDouble{false};
  bool isImaginary{false};
  double doubleValue{0.0};
  int literalExponent{0};

  bool isU64{false};
  U64 u64Value{0};

  void initValues();

  [[nodiscard]] bool getLiteral(string &ret) const override {
    ret = literalNumber;
    return true;
  }
  [[nodiscard]] bool getLiteralNumber(string &ret) const override
  {
    ret = literalNumber;
    return true;
  }
  [[nodiscard]] bool getValue(double &ret) const override
  {
    if (isDouble) {
      ret = doubleValue;
      return true;
    }
    if (isU64) {
      ret = u64Value;
      return true;
    }
    return false;
  }
  [[nodiscard]] bool getValue(U64 &ret) const override
  {
    if (isU64) {
      ret = u64Value;
      return true;
    }
    if (isDouble) {
      if (doubleValue >= 0.0 && doubleValue <= (double)numeric_limits<U64>::max()) {
        ret = (U64)doubleValue;
        return true;
      }
    }
    return false;
  }
  [[nodiscard]] bool getValue(int &ret) const override
  {
    if (isU64) {
      if (u64Value <= (U64)INT_MAX) {
        ret = (int)u64Value;
        return true;
      }
    }
    if (isDouble) {
      if (doubleValue >= (double)numeric_limits<int>::min() && doubleValue <= (double)numeric_limits<int>::max()) {
        ret = (int)doubleValue;
        return true;
      }
    }
    return false;
  }
  [[nodiscard]] bool getValue(string &ret) const override
  {
    ret = literalNumber;
    return true;
  }

  AstNumber * asNumber() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};


struct AstRangeSpec: AstNode {
  AstRangeSpec(
    string _dist,
    AstExpr *_scale)
    : dist(std::move(_dist)),
      scale(_scale)
  {
    adopt(scale);
    if (!scale->getValue(scaleValue)) {
      throw runtime_error("Not a number");
    }
  }

  string dist;
  AstExpr *scale;
  double scaleValue{0.0};

  struct AstRangeSpec *asRangeSpec() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstParam: AstExpr {
  AstParam(
    AstExpr *_lhs,
    AstRangeSpec *_rangeSpec)
    : lhs(_lhs),
      rangeSpec(_rangeSpec)
  {
    adopt(lhs);
    adopt(rangeSpec);
  }

  AstExpr *lhs;
  AstRangeSpec *rangeSpec;

  struct AstParam *asParam() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstBoolean : AstExpr {
  AstBoolean(
    bool _literalValue)
    : literalValue(_literalValue)
  {
  }

  bool literalValue;

  struct AstBoolean *asBoolean() override { return this; }
  [[nodiscard]] bool getValue(bool &ret) const override { ret = literalValue; return true; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstNull : AstExpr {
  AstNull()
  {
  }

  struct AstNull *asNull() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstString : AstExpr {
  AstString(
    string _literalValue)
    : literalValue(std::move(_literalValue))
  {
  }

  string literalValue;

  [[nodiscard]] bool getLiteral(string &ret) const override
  {
    ret = literalValue;
    return true;
  }
  [[nodiscard]] bool getValue(string &ret) const override
  {
    ret = literalValue;
    return true;
  }

  [[nodiscard]] bool getLiteralKey(string &ret) const override
  {
    ret = literalValue;
    return true;
  }

  struct AstString *asString() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstDeriv : AstExpr {
  AstDeriv(
    AstExpr *_arg,
    AstExpr *_wrt)
    : arg(_arg),
      wrt(_wrt)
  {
    adopt(arg);
    adopt(wrt);
  }

  AstExpr *arg;
  AstExpr *wrt;

  struct AstDeriv *asDeriv() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstKeyValue : AstNode {
  AstKeyValue(
    AstNode *_key,
    AstExpr *_value)
    : key(_key),
      value(_value)
  {
    adopt(key);
    adopt(value);
    if (!key->getLiteralKey(literalKey)) throw runtime_error("not a key");
  }

  string literalKey;
  AstNode *key;
  AstExpr *value;

  struct AstKeyValue *asKeyValue() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstObject : AstExpr {
  AstObject(
    vector<AstKeyValue *> _keyValues,
    AstObject *_prev,
    AstNode *_defaultValue=nullptr)
    : keyValues(move(_keyValues)),
      prev(_prev),
      defaultValue(_defaultValue)
  {
    for (auto &it : keyValues) {
      adopt(it);
    }
    adopt(prev);
    adopt(defaultValue);
  }

  vector<AstKeyValue *> keyValues;
  AstObject *prev;
  AstNode *defaultValue;

  [[nodiscard]] bool getValueNodeForKey(string const &key, AstNode *&ret) const override
  {
    for (auto const &it : keyValues) {
      if (it->literalKey == key) {
        ret = it->value;
        return true;
      }
    }
    if (prev) return prev->getValueNodeForKey(key, ret);
    return false;
  }

  struct AstObject *asObject() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};


struct AstArray : AstExpr {
  AstArray(
    vector<AstExpr *> _elems)
      : elems(move(_elems))
  {
    for (auto & it : elems) {
      adopt(it);
    }
  }

  vector<AstExpr *> elems;

  [[nodiscard]] bool getValue(glm::dmat4 &ret) const override
  {
    if (elems.size() == 16) {
      for (int i=0; i < 16; i++) {
        if (!elems[i]->getValue(glm::value_ptr(ret)[i])) return false;
      }
      return true;
    }
    return false;
  }

  [[nodiscard]] bool getValue(vector<string> &ret) const override
  {
    ret.resize(elems.size());
    for (size_t i = 0; i < elems.size(); i++) {
      if (!elems[i]->getValue(ret[i])) return false;
    }
    return true;
  }

  struct AstArray *asArray() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstExtern : AstExpr {
  AstExtern(string _name)
    :name(std::move(_name))
  {
  }

  string name;

  struct AstExtern *asExtern() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};



struct AstStatement : AstNode {
  struct AstStatement *asStatement() override { return this; }
};

struct AstBlock : AstNode {
  AstBlock()
  {
  }
  vector<AstStatement *> statements;

  void addStatement(AstStatement *it)
  {
    statements.push_back(it);
    adopt(statements.back());
  }

  struct AstBlock *asBlock() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstIf : AstStatement {
  AstIf(
    AstExpr *_cond,
    AstBlock *_ifTrue,
    AstBlock *_ifFalse)
    : cond(_cond),
      ifTrue(_ifTrue),
      ifFalse(_ifFalse)
  {
    adopt(cond);
    adopt(ifTrue);
    adopt(ifFalse);
  }

  AstExpr *cond;
  AstBlock *ifTrue;
  AstBlock *ifFalse;

  struct AstIf *asIf() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstDefault : AstStatement {
  AstDefault(
    AstBlock *_block)
    : block(_block)
  {
    adopt(block);
  }

  AstBlock *block;

  struct AstDefault *asDefault() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstSimple : AstStatement {
  AstSimple(
    AstExpr *_expr)
    : expr(_expr)
  {
    adopt(expr);
  }

  AstExpr *expr;

  struct AstSimple *asSimple() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstTypeDecl : AstStatement {
  AstTypeDecl(
    AstName *_type,
    vector<AstExpr *> _exprs)
    : type(_type),
      exprs(move(_exprs))
  {
    adopt(type);
    for (auto &it : exprs) {
      adopt(it);
    }
  }

  AstName *type;
  vector<AstExpr *> exprs;

  struct AstTypeDecl *asTypeDecl() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};



struct AstMemberDecl : AstNode {
  AstMemberDecl(
    AstName *_clsName,
    AstRangeSpec *_rangeSpec,
    vector<pair<AstName *, AstExpr *>> _members)
    : clsName(_clsName),
      rangeSpec(_rangeSpec),
      members(move(_members))
  {
    adopt(clsName);
    adopt(rangeSpec);
    for (auto &it : members) {
      adopt(it.first);
      adopt(it.second);
    }
  }

  AstName *clsName;
  AstRangeSpec *rangeSpec;
  vector<pair<AstName *, AstExpr *>> members;

  struct AstMemberDecl *asMemberDecl() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstStructDef : AstNode {
  AstStructDef(
    AstName *const &_name,
    vector<AstMemberDecl *> _memberDecls,
    vector<AstKeyValue *> _options)
    : name(_name),
      memberDecls(move(_memberDecls)),
      options(move(_options))
  {
    literalName = name->name;
    adopt(name);
    for (auto &it : memberDecls) {
      adopt(it);
    }
    for (auto &it : options) {
      adopt(it);
    }
  }

  AstName *name;
  string literalName;
  vector<AstMemberDecl *> memberDecls;
  vector<AstKeyValue *> options;

  struct AstStructDef *asStructDef() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstEnumDecl : AstNode {
  AstEnumDecl(
    AstName *_memberName,
    vector<AstObject *> _memberOptions)
    : memberName(_memberName),
      memberOptions(move(_memberOptions))
  {
    literalMemberName = memberName->name;
    adopt(memberName);
    for (auto &it : memberOptions) {
      adopt(it);
    }
  }

  AstName *memberName;
  string literalMemberName;
  vector<AstObject *> memberOptions;

  struct AstEnumDecl *asEnumDecl() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstOnehot : AstNode {
  AstOnehot(
    AstName *_clsName,
    vector<AstEnumDecl *> _members)
    : clsName(_clsName),
      members(move(_members))
  {
    literalClsName = clsName->name;
    adopt(clsName);
    for (auto &it : members) {
      adopt(it);
    }
  }

  AstName *clsName;
  string literalClsName;
  vector<AstEnumDecl *> members;

  struct AstOnehot *asOnehot() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstArgDecl : AstNode {
  AstArgDecl(
    string const &_dir,
    AstName *_type,
    AstName *_name,
    bool _optional)
    : dirInOnly(_dir == "in"),
      dirOutOnly(_dir == "out"),
      dirUpOnly(_dir == "update"),
      dirInUp(_dir == "in" || _dir == "update"),
      dirOutUp(_dir == "out" || _dir == "update"),
      type(_type),
      name(_name),
      optional(_optional)
  {
    adopt(type);
    adopt(name);
    literalName = name->name;
    literalType = type->name;
  }

  bool dirInOnly, dirOutOnly, dirUpOnly, dirInUp, dirOutUp;
  AstName *type;
  AstName *name;
  string literalName;
  string literalType;
  bool optional;

  struct AstArgDecl *asArgDecl() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};


struct AstFunction : AstNode {
  AstFunction(
    string _funcName,
    vector<AstArgDecl *> _args,
    AstBlock *_body,
    CallableYogaFunction _cppCallable = nullptr,
    CallableYogaDebug _cppCallableDebug = nullptr)
    : funcName(move(_funcName)),
      args(move(_args)),
      body(_body),
      cppCallable(_cppCallable),
      cppCallableDebug(_cppCallableDebug)
  {
    for (auto &it : args) {
      adopt(it);
    }
    adopt(body);
  }

  string funcName;
  vector<AstArgDecl *> args;
  AstBlock *body;
  CallableYogaFunction cppCallable;
  CallableYogaDebug cppCallableDebug;

  struct AstFunction *asFunction() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
};

struct AstAnnoCall : AstNode {
  AstAnnoCall(
    AstCall *_call,
    AstObject *_options)
    : call(_call),
      options(_options)
  {
    adopt(call);
    adopt(options);
  }

  AstCall *call;
  AstObject *options;

  struct AstAnnoCall *asAnnoCall() override { return this; }
  [[nodiscard]] bool scan(YogaContext &ctx, YogaPassBase *pass) override;
  void dump(ostream &s, string const &indent, int depth) const override;
  string gloss() const override;
};
