#include "./polyfit.h"
#include "numerical/polyfit.h"
#include "./runtime.h"


static string dspconst(string const &type, R value)
{
  if (
    (type == "1616" && (value < -32765.0 || value > +32765.0)) ||
    (type == "824" && (value < -127.0 || value > +127.0))) {
    throw runtime_error("Value too large for DSP" + type + ": " + to_string(value));
  }
  return "DSP" + type + "(" + to_string(value) + ")";
}

static bool emitCode(string &code, string outName, string iType, string oType, string cType, vector<R> coeffs)
{
  code += "dsp" + oType + " " + outName + "_dsp" + iType + "_dsp" + oType + "(dsp" + iType + " x1)\n{\n";
  code += "  dsp" + oType + " y = " + dspconst(oType, coeffs[0]) + ";\n";

  for (size_t i = 1; i < coeffs.size(); i++) {
    if (i > 1) {
      code += "  dsp" + iType + " x" + to_string(i) + " = mulsat_dsp" + iType + "_dsp" + iType + "_dsp" + iType + "(x" + to_string(i-1) + ", x1);\n";
    }
    code += "  y += mulsat_dsp" + iType + "_dsp" + cType + "_dsp" + oType + "(x" + to_string(i) + ", " + dspconst(cType, coeffs[i]) + ");\n";
  }

  code += "  return y;\n";
  code += "}\n\n";

  return true;
}

bool createPolyfit(YogaContext const &ctx, AstObject *fit, ostream &outStream)
{
  string funcName;
  if (!fit->getValueForKey("f", funcName, true)) {
    return ctx(fit).logError("f not a name");
  }
  auto outName = funcName;
  if (!fit->getValueForKey("name", funcName, false)) {
    return ctx(fit).logError("name not a name");
  }
  R xLo{0.0}, xHi{0.0};
  if (!fit->getValueForKey("lo", xLo, true)) {
    return ctx(fit).logError("lo not a number");
  }
  if (!fit->getValueForKey("hi", xHi, true)) {
    return ctx(fit).logError("hi not a number");
  }

  YogaCaller fitfn(ctx.reg, funcName);

  const int N = 64;
  vector<R> xs(N), ys(N);
  R astonishment = 0.0;

  for (int i = 0; i < N; i++) {
    R frac = i * (1.0/(N-1));
    R x = xLo + frac * (xHi - xLo);
    R y = 0.0;
    fitfn({&y}, {&x}, astonishment);
    if (0) cerr << "x="s + to_string(x) + " y=" + to_string(y) + "\n";
    xs[i] = x;
    ys[i] = y;
  }

  auto poly = mkPolyfit3(xs, ys);

  string format;
  if (!fit->getValueForKey("format", format, false)) {
    return ctx(fit).logError("format not a string");
  }

  string code;
  code += "// " + outName + " poly=" + repr(poly) + " deriv=" + repr(poly.deriv()) + "\n\n";

  if (format == "") {
  }
  else if (format == "dsp.824.1616.1616") {
    if (!emitCode(code, outName, "824", "1616", "1616", poly.coeffs())) return false;
    if (!emitCode(code, outName + "Deriv", "824", "1616", "1616", poly.deriv().coeffs())) return false;
  }
  else {
    return ctx.logError("Unknown format " + shellEscape(format));
  }
  code += "\n";

  if (!code.empty()) {
    outStream << code;
  }


  return true;
}

static YogaCodeRegister registerPolyfitTypes("polyfit.cc", R"(

struct Polyfit1 {
  R c0, c1;
};

struct Polyfit2 {
  R c0, c1, c2;
};

struct Polyfit3 {
  R c0, c1, c2, c3;
};

struct Polyfit4 {
  R c0, c1, c2, c3, c4;
};

struct Polyfit5 {
  R c0, c1, c2, c3, c4, c5;
};

)");
