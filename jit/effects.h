#pragma once
#include "./jit_utils.h"
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"

struct FunctionEffects {

  YogaSourceLoc sourceLoc;

  /*
    If set, these override everything else here and mean this is a foreign function
  */
  CallableYogaFunction cppCallable{nullptr};
  CallableYogaDebug cppCallableDebug{nullptr};

  // maps the cseKey of dst to a set of (value, strength) pairs
  map<string, AssignmentSet> assignments;

  vector<tuple<string, ExprNode *, ExprNode *>> debugLocalItems;
  map<string, bool> debugLocalDones;
  YogaTypeStruct *debugLocalsType{nullptr};
  map<string, YogaSourceLoc> debugLocalMemberLocs;
  map<string, string> debugCallMap;

  vector<pair<YogaType *, string>> inArgs;
  vector<pair<YogaType *, string>> outArgs;

  vector<ExprNode *> fwdOrder;

  void dump(ostream &s, string const &indent) const;
};

ostream & operator <<(ostream &s, FunctionEffects const &it);
