	.section	__TEXT,__text,regular,pure_instructions
	.build_version macos, 10, 14
	.globl	_foo_times_scalar_sret  ## -- Begin function foo_times_scalar_sret
	.p2align	4, 0x90
_foo_times_scalar_sret:                 ## @foo_times_scalar_sret
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movddup	%xmm0, %xmm1            ## xmm1 = xmm0[0,0]
	mulpd	16(%rbp), %xmm1
	movupd	%xmm1, (%rdi)
	mulsd	32(%rbp), %xmm0
	movsd	%xmm0, 16(%rdi)
	movq	%rdi, %rax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_foo_times_scalar_yoga  ## -- Begin function foo_times_scalar_yoga
	.p2align	4, 0x90
_foo_times_scalar_yoga:                 ## @foo_times_scalar_yoga
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movupd	(%rsi), %xmm1
	movddup	%xmm0, %xmm2            ## xmm2 = xmm0[0,0]
	mulpd	%xmm1, %xmm2
	movupd	%xmm2, (%rdi)
	mulsd	16(%rsi), %xmm0
	movsd	%xmm0, 16(%rdi)
	xorpd	%xmm0, %xmm0
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_mat33_copy             ## -- Begin function mat33_copy
	.p2align	4, 0x90
_mat33_copy:                            ## @mat33_copy
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movl	$9, %ecx
	rep;movsq
	xorps	%xmm0, %xmm0
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_dummy                  ## -- Begin function dummy
	.p2align	4, 0x90
_dummy:                                 ## @dummy
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movsd	40(%rdi), %xmm0         ## xmm0 = mem[0],zero
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_mat33_call             ## -- Begin function mat33_call
	.p2align	4, 0x90
_mat33_call:                            ## @mat33_call
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	%rsi, %rax
	movl	$9, %ecx
	rep;movsq
	movsd	40(%rax), %xmm0         ## xmm0 = mem[0],zero
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_mat33_angle            ## -- Begin function mat33_angle
	.p2align	4, 0x90
_mat33_angle:                           ## @mat33_angle
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	callq	_sin
	movsd	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_test_sin               ## -- Begin function test_sin
	.p2align	4, 0x90
_test_sin:                              ## @test_sin
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	pushq	%rbx
	pushq	%rax
	.cfi_offset %rbx, -24
	movq	%rdi, %rbx
	movsd	(%rsi), %xmm0           ## xmm0 = mem[0],zero
	callq	_sin
	movsd	%xmm0, (%rbx)
	xorps	%xmm0, %xmm0
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_test_memset            ## -- Begin function test_memset
	.p2align	4, 0x90
_test_memset:                           ## @test_memset
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	$0, 64(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.globl	_test_zero              ## -- Begin function test_zero
	.p2align	4, 0x90
_test_zero:                             ## @test_zero
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	movq	$0, 64(%rdi)
	movq	$0, 56(%rdi)
	movq	$0, 48(%rdi)
	movq	$0, 40(%rdi)
	movq	$0, 32(%rdi)
	movq	$0, 24(%rdi)
	movq	$0, 16(%rdi)
	movq	$0, 8(%rdi)
	movq	$0, (%rdi)
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__literal8,8byte_literals
	.p2align	3               ## -- Begin function main
LCPI9_0:
	.quad	4606365442650518598     ## double 0.90929742682568171
	.section	__TEXT,__text,regular,pure_instructions
	.globl	_main
	.p2align	4, 0x90
_main:                                  ## @main
	.cfi_startproc
## %bb.0:
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset %rbp, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register %rbp
	leaq	L_.str(%rip), %rdi
	movsd	LCPI9_0(%rip), %xmm0    ## xmm0 = mem[0],zero
	movb	$1, %al
	callq	_printf
	xorl	%eax, %eax
	popq	%rbp
	retq
	.cfi_endproc
                                        ## -- End function
	.section	__TEXT,__cstring,cstring_literals
L_.str:                                 ## @.str
	.asciz	"%g\n"


.subsections_via_symbols
