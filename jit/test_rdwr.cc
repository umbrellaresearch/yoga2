#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"

TEST_CASE("Read-Write", "[compiler][rdwr]") {

  auto reg = YOGAC(R"(
    struct ReadWriteTest {
      R foo;
      bool fooBool;
      S32 fooS32;
      U32 fooU32;
      S64 fooS64;
      U64 fooU64;
      R[3,3] m33;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto t = ctx.getTypeOrThrow("ReadWriteTest");

  YogaValue yv = ctx.mkValue(t);

  if (1) {
    YogaValueAccessor<double> doubleAccFoo(t, "foo");
    REQUIRE(doubleAccFoo.wrf); REQUIRE(doubleAccFoo.rdf);

    doubleAccFoo.wr(yv, 7.0);
    CHECK(doubleAccFoo.rd(yv) == 7.0);
    doubleAccFoo.wr(yv, 0.0);
    CHECK(doubleAccFoo.rd(yv) == 0.0);
  }

  if (1) {
    YogaValueAccessor<double> doubleAccFooBool(t, "fooBool");
    REQUIRE(doubleAccFooBool.wrf); REQUIRE(doubleAccFooBool.rdf);
    YogaValueAccessor<bool> boolAccFooBool(t, "fooBool");
    REQUIRE(boolAccFooBool.wrf); REQUIRE(boolAccFooBool.rdf);

    doubleAccFooBool.wr(yv, 7.0);
    CHECK(boolAccFooBool.rd(yv) == true);
    doubleAccFooBool.wr(yv, 0.0);
    CHECK(boolAccFooBool.rd(yv) == false);

    boolAccFooBool.wr(yv, true);
    CHECK(doubleAccFooBool.rd(yv) == 1.0);
  }

  if (1) {
    YogaValueAccessor<double> doubleAccFooS32(t, "fooS32");
    REQUIRE(doubleAccFooS32.wrf); REQUIRE(doubleAccFooS32.rdf);
    YogaValueAccessor<S32> s32AccFooS32(t, "fooS32");
    REQUIRE(s32AccFooS32.wrf); REQUIRE(s32AccFooS32.rdf);

    doubleAccFooS32.wr(yv, 7.0);
    CHECK(s32AccFooS32.rd(yv) == 7);
    doubleAccFooS32.wr(yv, 0.0);
    CHECK(s32AccFooS32.rd(yv) == 0);

    s32AccFooS32.wr(yv, 9);
    CHECK(doubleAccFooS32.rd(yv) == 9.0);
  }

  if (1) {
    YogaValueAccessor<double> doubleAccFooU32(t, "fooU32");
    REQUIRE(doubleAccFooU32.wrf); REQUIRE(doubleAccFooU32.rdf);
    YogaValueAccessor<U32> u32AccFooU32(t, "fooU32");
    REQUIRE(u32AccFooU32.wrf); REQUIRE(u32AccFooU32.rdf);

    doubleAccFooU32.wr(yv, 7.0);
    CHECK(u32AccFooU32.rd(yv) == 7);
    doubleAccFooU32.wr(yv, 0.0);
    CHECK(u32AccFooU32.rd(yv) == 0);

    u32AccFooU32.wr(yv, 9);
    CHECK(doubleAccFooU32.rd(yv) == 9.0);
  }

  if (1) {
    YogaValueAccessor<double> doubleAccFooS64(t, "fooS64");
    REQUIRE(doubleAccFooS64.wrf); REQUIRE(doubleAccFooS64.rdf);
    YogaValueAccessor<S64> s64AccFooS64(t, "fooS64");
    REQUIRE(s64AccFooS64.wrf); REQUIRE(s64AccFooS64.rdf);

    doubleAccFooS64.wr(yv, 7.0);
    CHECK(s64AccFooS64.rd(yv) == 7);
    doubleAccFooS64.wr(yv, 0.0);
    CHECK(s64AccFooS64.rd(yv) == 0);

    s64AccFooS64.wr(yv, 9);
    CHECK(doubleAccFooS64.rd(yv) == 9.0);
  }

  if (1) {
    YogaValueAccessor<double> doubleAccFooU64(t, "fooU64");
    REQUIRE(doubleAccFooU64.wrf); REQUIRE(doubleAccFooU64.rdf);
    YogaValueAccessor<U64> u64AccFooU64(t, "fooU64");
    REQUIRE(u64AccFooU64.wrf); REQUIRE(u64AccFooU64.rdf);

    doubleAccFooU64.wr(yv, 7.0);
    CHECK(u64AccFooU64.rd(yv) == 7);
    doubleAccFooU64.wr(yv, 0.0);
    CHECK(u64AccFooU64.rd(yv) == 0);

    u64AccFooU64.wr(yv, 9);
    CHECK(doubleAccFooU64.rd(yv) == 9.0);
  }
}



