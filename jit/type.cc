#include "./type.h"
#include "./compilation.h"
#include "./type_impl.h"
#include "common/packetbuf_types.h"



// -------------


YogaTypeStruct *YogaCompilation::getStruct(YogaContext const &ctx, string clsName, bool isOnehot)
{
  auto &slot = ctx.lookup(clsName);
  if (!slot.type) {
    slot.type = ctx.mkType<YogaTypeStruct>(clsName, isOnehot);
    slot.isType = true;
  }
  return slot.type->asStruct();
}


YogaTypePrimitive *YogaCompilation::getPrimitive(YogaContext const &ctx, string clsName)
{
  auto &slot = ctx.lookup(clsName);
  if (!slot.type) {
    slot.type = ctx.mkType<YogaTypePrimitive>(clsName);
    slot.isType = true;
  }
  return slot.type->asPrimitive();
}

YogaType *YogaCompilation::getType(YogaContext const &ctx, string clsName)
{
  auto &slot = ctx.lookup(clsName);
  if (!slot.type) {
    auto t = createNamedType(ctx("createNamedType"), clsName);
    if (!t) return nullptr;
    if (slot.type != t) { // should have been filled in, if clsName is canonical.
      if (0) ctx.logWarning("Requested typed named "s + clsName + " but got "s + repr_type(t));
      slot.type = t;
      slot.isType = true;
    }
  }
  return slot.type;
}

bool YogaCompilation::aliasType(YogaContext const &ctx, string newName, YogaType *t)
{
  assert(t);
  auto &slot = ctx.lookup(newName);
  if (slot.type) {
    // WRITEME: ?
  }
  slot.type = t;
  slot.isType = true;
  return true;
}

YogaTypeMatrix *YogaCompilation::getMatrix(YogaContext const &ctx, YogaType *elementType, int rows, int cols)
{
  auto clsName = YogaTypeMatrix::clsNameFor(elementType, rows, cols);
  auto &slot = ctx.lookup(clsName);
  if (!slot.type) {
    slot.type = ctx.mkType<YogaTypeMatrix>(elementType, rows, cols);
    slot.isType = true;
  }
  return slot.type->asMatrix();
}


inline bool isDimChar(char c)
{
  return ((c >= '0' && c <= '9') || c == 'n' || c == 'm' || c == 'l');
}


bool YogaCompilation::parseDims(YogaContext const &ctx, vector<int> &dims, string const &s)
{
  if (s.empty()) return false;
  size_t pos = 0;
  while (pos < s.size()) {
    int dim = -1;
    if (isdigit(s[pos])) {
      auto i = pos;
      while (pos < s.size() && isdigit(s[pos])) pos++;
      dim = 0;
      while (i < pos) {
        dim = 10 * dim + (s[i] - '0');
        i++;
      }
    }
    dims.push_back(dim);
    if (pos == s.size()) {
      break;
    }
    else if (s[pos] == ',') {
      pos++;
      continue;
    }
    else {
      return false;
    }
  }
  return true;
}


YogaType *YogaCompilation::createNamedType(YogaContext const &ctx, string clsName)
{
  /*
    The clsName passed here as already been canonicalized, so we don't need to
    worry about spaces
  */
  if (clsName.back() == ']') {
    auto lbrkPos = clsName.rfind('[');
    if (lbrkPos != string::npos) {
      string keyClsName = clsName.substr(lbrkPos + 1, clsName.size() - lbrkPos - 2);
      string valueClsName = clsName.substr(0, lbrkPos);

      if (0) cerr << "array name split: " + shellEscape(clsName) + " => key=" + shellEscape(keyClsName) + " and value=" + shellEscape(valueClsName) + "\n";

      auto valueType = ctx.getType(valueClsName);
      if (!valueType) return ctx.logErrorNull("Unknown value type "s + valueClsName);

      vector<int> dims;
      if (parseDims(ctx, dims, keyClsName)) {
        if (dims.size() == 1) {
          return getMatrix(ctx, valueType, dims[0], 1);
        }
        else if (dims.size() == 2) {
          return getMatrix(ctx, valueType, dims[0], dims[1]);
        }
        else {
          return ctx.logErrorNull("Too many dimensions (2 max)"s);
        }
      }
      else {
        return ctx.logErrorNull("parseDims failed on " + shellEscape(keyClsName));
      }
    }
  }
  return nullptr;
}

bool YogaCompilation::defineBuiltinTypes(YogaContext const &ctx)
{
  ctx.reg->voidType = getPrimitive(ctx, "void"s);
  ctx.reg->boolType = getPrimitive(ctx, "bool"s);

  ctx.reg->floatType = getPrimitive(ctx, "F"s);
  ctx.reg->rType = getPrimitive(ctx, "R"s);
  if (!aliasType(ctx, "double"s, ctx.reg->rType)) return false;
  ctx.reg->complexType = getPrimitive(ctx, "C"s);

  ctx.reg->s32Type = getPrimitive(ctx, "S32"s);
  if (!aliasType(ctx, "int"s, ctx.reg->s32Type)) return false;
  ctx.reg->s64Type = getPrimitive(ctx, "S64"s);
  ctx.reg->u32Type = getPrimitive(ctx, "U32"s);
  ctx.reg->u64Type = getPrimitive(ctx, "U64"s);

  ctx.reg->s16Type = getPrimitive(ctx, "S16"s);
  ctx.reg->u16Type = getPrimitive(ctx, "U16"s);

  ctx.reg->s8Type = getPrimitive(ctx, "S8"s);
  ctx.reg->u8Type = getPrimitive(ctx, "U8"s);

  ctx.reg->stringType = getPrimitive(ctx, "string"s);
  ctx.reg->memoryResourceType = getPrimitive(ctx, "MemoryResource"s);

  ctx.reg->objectType = getPrimitive(ctx, "Object"s);
  //if (!aliasType(ctx, "jsonstr"s, objectType)) return false;

  ctx.reg->packetType = getPrimitive(ctx, "packet"s);

  if (1) {
    auto &slot = ctx.lookup("Set");
    if (!slot.type) {
      slot.type = ctx.mkType<YogaTypeSet>();
      slot.isType = true;
    }
    ctx.reg->setType = slot.type;
  }

  return true;
}


bool YogaCompilation::freezeAllTypes(YogaContext const &ctx)
{
  for (auto &[symName, symVal] : ctx.reg->symbolTable) {
    if (symVal.type) {
      if (!symVal.type->tryFreeze()) {
        return ctx.logError("Unable to freeze layout of "s + symName);
      }
      assert(symVal.type->isEquivalentType(symVal.type));
    }
  }
  return true;
}


CallableYogaPacketRd YogaType::getPacketRdForLegacy(YogaType *oldType)
{
  if (oldType && isEquivalentType(oldType)) {
    return packetRd;
  }
  /*
    WRITEME: if oldType and this are different, we need to do some kind of conversion.
    Note that oldType hasn't been compiled yet -- we just read it from the stream
    with packet_rd. If it's equivalent to this->type, then we shouldn't bother.
    Otherwise we should compile a read function that reads the old format but stuffs
    it into a new-format object in memory.
    Return nullptr if there's nothing useful we can do.
  */
  return nullptr;
}


namespace packetio {

  void packet_wr_value(packet &p, YogaType * const &x, YogaContext const &ctx)
  {
    if (auto xPrim = x->asPrimitive()) {
      packet_wr_value(p, "primitive");
      packet_wr_value(p, xPrim->clsName);
    }
    else if (auto xPtr = x->asPtr()) {
      packet_wr_value(p, "ptr");
      packet_wr_value(p, xPtr->nonPtrType); // FIXME: could be circular
    }
    else if (auto xStruct = x->asStruct()) {
      packet_wr_value(p, "struct");
      packet_wr_value(p, xStruct->clsName);
      packet_wr_value(p, xStruct->autoCreate);
      packet_wr_value(p, xStruct->isOnehot);
      packet_wr_value(p, (U64)xStruct->members.size());
      for (auto &it : xStruct->members) {
        packet_wr_value(p, it->memberName);
        packet_wr_value(p, it->memberType, ctx);
      }
    }
    else if (auto xMatrix = x->asMatrix()) {
      packet_wr_value(p, "matrix");
      packet_wr_value(p, xMatrix->elementType, ctx);
      packet_wr_value(p, xMatrix->rows);
      packet_wr_value(p, xMatrix->cols);
    }
    else if (x->isSet()) {
      packet_wr_value(p, "set");
    }
    else {
      throw runtime_error("Unknown YogaType subtype");
    }
  }

  void packet_rd_value(packet &p, YogaType * &x, YogaContext const &ctx)
  {
    /*
      Read in a type. Mutates *ctx.reg in complicated ways, so make sure
      to not multithread calls to this.
    */    
    string subtype;
    packet_rd_value(p, subtype);
    if (subtype == "primitive") {
      string clsName;
      packet_rd_value(p, clsName);
      x = ctx.reg->getPrimitive(ctx, clsName);
    }
    else if (subtype == "ptr") {
      YogaType *nonPtrType{nullptr};
      packet_rd_value(p, nonPtrType, ctx);
      x = ctx.mkType<YogaTypePtr>(nonPtrType);
    }
    else if (subtype == "struct") {
      string clsName;
      packet_rd_value(p, clsName);
      bool autoCreate{false}, isOnehot{false};
      packet_rd_value(p, autoCreate);
      packet_rd_value(p, isOnehot);

      x = ctx.mkType<YogaTypeStruct>(clsName, isOnehot);
      x->asStruct()->autoCreate = autoCreate;

      U64 nMembers{0};
      packet_rd_value(p, nMembers);
      for (U64 i=0; i < nMembers; i++) {
        string memberName;
        YogaType *memberType{nullptr};
        packet_rd_value(p, memberName);
        packet_rd_value(p, memberType, ctx);
        x->asStruct()->addMember(ctx, memberName, memberType);
      }
    }
    else if (subtype == "matrix") {
      YogaType *elementType{nullptr};
      packet_rd_value(p, elementType, ctx);
      int rows{0}, cols{0};
      packet_rd_value(p, rows);
      packet_rd_value(p, cols);
      x = ctx.mkType<YogaTypeMatrix>(elementType, rows, cols);
    }
    else if (subtype == "set") {
      x = ctx.reg->setType;
    }
    else {
      throw runtime_error("Unknown YogaType subtype " + shellEscape(subtype));
    }
  }
}


