#include "./compilation.h"
#include "./value.h"
#include "./type.h"
#include "./runtime.h"
#include "../test/test_utils.h"
#include "../geom/geom.h"

TEST_CASE("Struct layout: mixed R/bool with padding", "[compiler]") {
  /*
    Some of the tests here might be specific to x86-64, like
    expecting doubles to be aligned on an 8-byte boundary.
    */

  auto reg = YOGAC(R"(
    struct LayoutTest {
      R foo;
      R bar;
      bool b1;
      bool b2;
      bool b3;
      R buz;
    }
  )");
  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto t = ctx.getTypeOrThrow("LayoutTest");

  REQUIRE(t->llAllocSize == 32);

  YogaValue yv1 = ctx.mkValue(t);

  YogaValueAccessor<double> accFoo(t, "foo");
  YogaValueAccessor<double> accBar(t, "bar");
  YogaValueAccessor<bool> accb1(t, "b1");
  YogaValueAccessor<bool> accb2(t, "b2");
  YogaValueAccessor<bool> accb3(t, "b3");
  YogaValueAccessor<double> accBuz(t, "buz");
  accFoo.wr(yv1, 29.5);
  accBar.wr(yv1, 30.5);
  accb1.wr(yv1, true);
  accb2.wr(yv1, false);
  accb3.wr(yv1, true);
  accBuz.wr(yv1, 31.5);

  REQUIRE(accFoo.rd(yv1) == 29.5);
  REQUIRE(accBar.rd(yv1) == 30.5);
  REQUIRE(accb1.rd(yv1) == true);
  REQUIRE(accb2.rd(yv1) == false);
  REQUIRE(accb3.rd(yv1) == true);
  REQUIRE(accBuz.rd(yv1) == 31.5);

  auto yv2 = yv1.dup(reg->mem);
  REQUIRE(accFoo.rd(yv2) == 29.5);
  REQUIRE(accBar.rd(yv2) == 30.5);
  REQUIRE(accb1.rd(yv2) == true);
  REQUIRE(accb2.rd(yv2) == false);
  REQUIRE(accb3.rd(yv2) == true);
  REQUIRE(accBuz.rd(yv2) == 31.5);
  REQUIRE(yv1 == yv2);

  packet p;
  (*t->packetWr)(p, yv1.buf);
  REQUIRE(p.size() == 3*sizeof(double) + 3*sizeof(bool));
  
  YogaValue yv3 = ctx.mkValue(t);
  (*t->packetRd)(p, yv3.buf, reg->mem);

  REQUIRE(accFoo.rd(yv3) == 29.5);
  REQUIRE(accBar.rd(yv3) == 30.5);
  REQUIRE(accb1.rd(yv3) == true);
  REQUIRE(accb2.rd(yv3) == false);
  REQUIRE(accb3.rd(yv3) == true);
  REQUIRE(accBuz.rd(yv3) == 31.5);
  REQUIRE(yv1 == yv3);

};



TEST_CASE("Matrix layout: Compatible with glm::mat and glm::vec layout", "[compiler]") {

  auto reg = YOGAC(R"(
    struct LayoutTest {
      R[4,4] m4;
    };
    function mkpt(out LayoutTest o) {
      o.m4 = R[4,4](1,2,3,0, 5,6,7,0, 9,10,11,0, 0,0,0,1);
    }
    function adjpt(out LayoutTest o, in LayoutTest i) {
      o.m4 = mat44Translation(0, 0, 100) * i.m4;
    }
    function convpt(out R[4] v) {
      m = R[4,4](1,2,3,0, 5,6,7,0, 9,10,11,0, 0,0,0,1);
      v = nograd(m * R[4](0.25, 0.0, 0.0, 1));
    }
  )");

  auto ctx = reg->mkCtx("test");
  REQUIRE(checkerrs(ctx, {}));

  auto layoutTestType = ctx.getTypeOrThrow("LayoutTest");

  YogaValueAccessor<glm::mat4> accm4(layoutTestType, "m4");

  auto mkpt = ctx.getYogaFunction("mkpt");
  REQUIRE(mkpt != nullptr);
  auto adjpt = ctx.getYogaFunction("adjpt");
  REQUIRE(adjpt != nullptr);

  YogaValue o(reg.get(), layoutTestType, reg->mem);
  YogaValue r(reg.get(), layoutTestType, reg->mem);

  R astonishment = 0.0;
  {
    array<U8 *, 1> outArgs {{ o.buf }};
    array<U8 *, 0> inArgs {};
    mkpt(outArgs.data(), inArgs.data(), reg->paramValues.data(), &astonishment, 0.0, reg->mem);
  }

  glm::mat4 o_m4;
  accm4.rd(o, o_m4);
  CHECK(o_m4[0][0] == 1.0f);
  CHECK(o_m4[0][1] == 2.0f);
  CHECK(o_m4[0][2] == 3.0f);
  CHECK(o_m4[0][3] == 0.0f);

  CHECK(o_m4[1][0] == 5.0f);
  CHECK(o_m4[1][1] == 6.0f);
  CHECK(o_m4[1][2] == 7.0f);
  CHECK(o_m4[1][3] == 0.0f);

  CHECK(o_m4[2][0] == 9.0f);
  CHECK(o_m4[2][1] == 10.0f);
  CHECK(o_m4[2][2] == 11.0f);
  CHECK(o_m4[2][3] == 0.0f);

  CHECK(o_m4[3][0] == 0.0f);
  CHECK(o_m4[3][1] == 0.0f);
  CHECK(o_m4[3][2] == 0.0f);
  CHECK(o_m4[3][3] == 1.0f);

  {
    array<U8 *, 1> outArgs {{ r.buf }};
    array<U8 *, 1> inArgs {{ o.buf }};
    adjpt(outArgs.data(), inArgs.data(), reg->paramValues.data(), &astonishment, 0.0, reg->mem);
  }

  glm::mat4 r_m4;
  accm4.rd(r, r_m4);
  CHECK(r_m4[0][0] == 1.0f);
  CHECK(r_m4[0][1] == 2.0f);
  CHECK(r_m4[0][2] == 3.0f);
  CHECK(r_m4[0][3] == 0.0f);

  CHECK(r_m4[1][0] == 5.0f);
  CHECK(r_m4[1][1] == 6.0f);
  CHECK(r_m4[1][2] == 7.0f);
  CHECK(r_m4[1][3] == 0.0f);

  CHECK(r_m4[2][0] == 9.0f);
  CHECK(r_m4[2][1] == 10.0f);
  CHECK(r_m4[2][2] == 11.0f);
  CHECK(r_m4[2][3] == 0.0f);

  CHECK(r_m4[3][0] == 0.0f);
  CHECK(r_m4[3][1] == 0.0f);
  CHECK(r_m4[3][2] == 100.0f);
  CHECK(r_m4[3][3] == 1.0f);

}

