#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"
#include "./pass_base.h"
#include "./effects.h"

struct YogaPassDeclareTypes : YogaPassBase {
  YogaPassDeclareTypes()
  {
  }

  bool doTopLevel(YogaContext const &ctx, AstStructDef *node) override { return true; }

  bool postScan(YogaContext const &ctx, AstStructDef *node) override
  {
    auto clsName = node->name->name;

    auto t = ctx.reg->getStruct(ctx, clsName, false);
    if (!t) return ctx.logError("Can't create struct "s + shellEscape(clsName));

    if (ctx.reg->verbose >= 5) cerr << "Created struct " + shellEscape(clsName) + "\n";
    return true;
  }

  bool doTopLevel(YogaContext const &ctx, AstOnehot *node) override { return true; }

  bool postScan(YogaContext const &ctx, AstOnehot *node) override
  {
    auto clsName = node->clsName->name;

    auto t = ctx.reg->getStruct(ctx, clsName, true);
    if (!t) return ctx.logError("Can't create onehot "s + shellEscape(clsName));

    if (ctx.reg->verbose >= 5) cerr << "Created onehot " + shellEscape(clsName) + "\n";
    return true;
  }

};


struct YogaPassDefineTypes : YogaPassBase {
  YogaPassDefineTypes()
  {
  }

  bool doTopLevel(YogaContext const &ctx, AstStructDef *node) override { return true; }

  bool postScan(YogaContext const &ctx, AstStructDef *node) override
  {
    auto clsName = node->name->name;

    auto t = ctx.reg->getStruct(ctx, clsName, false);
    if (!t) return ctx.logError("Can't find struct "s + shellEscape(clsName));

    for (auto &option : node->options) {
      if (option->literalKey == "ellipsis") {
        t->autoCreate = true;
      }
      else if (option->literalKey == "scopeNoMembers") {
        auto vBoolean = option->value->asBoolean();
        if (vBoolean) {
          t->scopeNoMembers = vBoolean->literalValue;
        }
        else {
          return ctx(option).logError("Non-boolean value for scopeNoMembers");
        }
      }
      else {
        return ctx(option).logError("Unknown struct option "s + option->literalKey);
      }
    }

    for (auto &memberDecl : node->memberDecls) {
      auto memberClsName = memberDecl->clsName;
      auto memberType = ctx("memberType").getType(memberClsName->name);
      if (!memberType) {
        return ctx(memberDecl).logError("No type "s + shellEscape(memberClsName->name));
      }

      for (auto &memberInfo : memberDecl->members) {
        auto mi = t->addMember(ctx, memberInfo.first->name, memberType);
        (void)mi;
      }
    }
    if (ctx.reg->verbose >= 5) cerr << t->getSynopsis() + "\n";

    return true;
  }


  bool doTopLevel(YogaContext const &ctx, AstOnehot *node) override { return true; }

  bool postScan(YogaContext const &ctx, AstOnehot *node) override
  {
    auto clsName = node->clsName->name;

    auto t = ctx.reg->getStruct(ctx, clsName, true);
    if (!t) return ctx.logError("Can't find onehot " + shellEscape(clsName));

    for (auto &d : node->members) {
      auto mi = t->addMember(ctx, d->literalMemberName, ctx.reg->rType);
      (void)mi;
    }
    if (ctx.reg->verbose >= 5) cerr << t->getSynopsis() + "\n";

    return true;
  }

};

