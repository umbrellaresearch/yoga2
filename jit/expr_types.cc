#include "./expr.h"
#include "./type.h"
#include "./param.h"
#include "./effects.h"
#include "./runtime.h"

#include "./expr_impl.h"


void ExprNode::setCseKey(string const &_cseKey)
{
  cseKey = _cseKey;
  debugLocalName = _cseKey;
}

void ExprNode::setCse(string const &prefix, string const &deets, string const &extra)
{
  setCseKey(prefix + "."s + secureHashToIdentSuffix(deets, 12) + "."s + extra);
}

void ExprNode::setCseFromArgs(string const &extraDeets, string const &extraLiteral)
{
  string deets;
  for (auto &it : args) {
    deets += ";";
    deets += it->cseKey;
  }
  deets += ";";
  deets += extraDeets;
  setCse(op, deets, extraLiteral);
}

YogaType *ExprNode::argsAllSameType() const
{
  if (args.empty()) return nullptr;
  for (size_t i=1; i < args.size(); i++) {
    if (args[i]->type != args[0]->type) {
      return nullptr;
    }
  }
  return args[0]->type;
}



bool YogaContext::convertType(ExprNode *&a, YogaType *desired) const
{
  if (!a || !desired) return false;

  if (a->type->isBool() && desired->isReal()) {
    a = mkExpr<ExprConvBoolDouble>(a);
  }

  if (a->type->isIntegral() && desired->isReal()) {
    a = mkExpr<ExprConvIntegerDouble>(a);
  }

  if (a->type->isReal() && desired->isComplex()) {
    a = mkExpr<ExprMkComplex>(a, mkExpr<ExprConstDouble>(0.0));
  }

  if (a->type != desired) return false;

  return true;
}

bool YogaContext::promoteTypes(ExprNode *&a, ExprNode *&b) const
{
  if (!a || !b) return false;

  if (a->type->isReal() && (b->type->isScalar() || b->type->isBool())) {
    if (!convertType(b, a->type)) return false;
  }
  if (b->type->isReal() && (a->type->isScalar() || a->type->isBool())) {
    if (!convertType(a, b->type)) return false;
  }
  if (a->type->isComplex() && b->type->isReal()) {
    if (!convertType(b, a->type)) return false;
  }
  if (b->type->isComplex() && a->type->isReal()) {
    if (!convertType(a, b->type)) return false;
  }
  return true;
}

/*
  resolveTypes. Must also call setCse.
*/


bool ExprConstDouble::resolveTypes(YogaContext const &ctx)
{
  type = ctx.reg->rType;
  /*
    These special names are both more readable in the debugging dumps, and
    are used by the peephole optimizer to notice special values.
  */
  if (literalValue > -100.0 && literalValue < 100.0 && floor(literalValue) == literalValue) {
    if (literalValue >= 0) {
      setCseKey("cd_" + to_string(int(floor(literalValue))));
    }
    else {
      setCseKey("cd_neg" + to_string(int(floor(-literalValue))));
    }
  }
  else if (literalValue == 0.5) {
    setCseKey("cd_half");
  }
  else if (literalValue == 1.0/3.0) {
    setCseKey("cd_one_third");
  }
  else if (literalValue == -2.0/3.0 || literalValue == (1.0/3.0 - 1.0)) {
    setCseKey("cd_neg_two_thirds");
  }
  else if (literalValue == numeric_limits<double>::epsilon()) {
    setCseKey("cd_eps");
  }
  else if (literalValue == numeric_limits<double>::infinity()) {
    setCseKey("cd_inf");
  }
  else if (literalValue == -numeric_limits<double>::infinity()) {
    setCseKey("cd_neginf");
  }
  else if (literalValue == numeric_limits<double>::quiet_NaN()) {
    setCseKey("cd_quietnan");
  }
  else {
    setCse("cd"s, to_string(literalValue), "");
  }
  return true;
}

bool ExprConstComplex::resolveTypes(YogaContext const &ctx)
{
  type = ctx.reg->complexType;
  setCseFromArgs(to_string(literalValue.real()) + "+" + to_string(literalValue.imag()) + "i");
  return true;
}

bool ExprConstZero::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  return true;
}

bool ExprConstOne::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  return true;
}

bool ExprConstString::resolveTypes(YogaContext const &ctx)
{
  type = ctx.reg->stringType;

  setCseFromArgs(literalValue);
  return true;
}

bool ExprConstVoid::resolveTypes(YogaContext const &ctx)
{
  type = ctx.reg->voidType;
  setCseFromArgs();
  return true;
}

bool ExprParam::resolveTypes(YogaContext const &ctx)
{
  type = paramInfo->paramType;
  if (!type) return ctx.logError("Parameter of unknown type"s);
  if (type->isReal()) {
  }
  else if (type->asMatrix() && type->asMatrix()->elementType->isReal()) {
  }
  else {
    return ctx.logError("Parameter of incorrect type "s + repr_type(type));
  }
  setCseFromArgs(""s, paramInfo->paramName + "." + to_string(paramInfo->paramIndex));
  return true;
}

bool ExprParamGrad::resolveTypes(YogaContext const &ctx)
{
  type = paramInfo->paramType;
  if (!type) return ctx.logError("Parameter of unknown type"s);
  if (type->isReal()) {
  }
  else if (type->asMatrix() && type->asMatrix()->elementType->isReal()) {
  }
  else {
    return ctx.logError("Parameter of incorrect type "s + repr_type(type));
  }
  setCseFromArgs(""s, paramInfo->paramName + "." + to_string(paramInfo->paramIndex));
  return true;
}


bool ExprVar::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs(""s, name + "." + to_string(int(dir)));
  debugLocalName = name;
  return !!type;
}

bool ExprUnboundVar::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs(""s, name);
  debugLocalName = name;
  type = ctx.reg->voidType;
  return true;
}


bool ExprMkComplex::resolveTypes(YogaContext const &ctx)
{
  if (args[0]->type->isReal() && args[1]->type->isReal()) {
    type = ctx.reg->complexType;
    setCseFromArgs();
    return true;
  }
  return false;
}


bool ExprExtractReal::resolveTypes(YogaContext const &ctx)
{
  if (args[0]->type->isComplex()) {
    type = ctx.reg->rType;
    setCseFromArgs();
    return true;
  }
  return false;
}


bool ExprExtractImag::resolveTypes(YogaContext const &ctx)
{
  if (args[0]->type->isComplex()) {
    type = ctx.reg->rType;
    setCseFromArgs();
    return true;
  }
  return false;
}




bool ExprStructref::resolveTypes(YogaContext const &ctx)
{
  //setCseKey(args[0]->cseKey + "."s + memberInfo->memberName);
  setCseFromArgs();
  debugLocalName = args[0]->debugLocalName + "."s + memberName;

  // memberInfo can be none if we're looking up in an ExprObject
  // In that case, we leave type null here. Peephole should return one of the 
  // arguments to ExprObject corresponding to memberName
  if (memberInfo) {
    type = memberInfo->memberType;
    if (!type) return ctx.logError("Missing member type");
  }
  return true;
}

bool ExprMkStruct::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (!type) return ctx.logError("Missing member type");
  return true;
}


bool ExprMkList::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  type = ctx.reg->voidType;
  return true;
}

bool ExprReturnVal::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs("", to_string(argi));
  return true;
}

bool ExprReturnGrad::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs("", to_string(argi));
  return true;
}

bool ExprConvBoolDouble::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (args[0]->type->isBool()) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprConvIntegerDouble::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (args[0]->type->isIntegral()) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprConvDoubleBool::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (args[0]->type->isReal()) {
    type = ctx.reg->boolType;
    return true;
  }
  return false;
}

bool ExprAdd::resolveTypes(YogaContext const &ctx)
{
  if (!ctx.promoteTypes(args[0], args[1])) return false;
  setCseFromArgs();

  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isScalar() && a1t->isScalar()) {
    type = a0t;
    return true;
  }
  if (a0t->isComplex() && a1t->isComplex()) {
    type = a0t;
    return true;
  }
  if (auto atm = a0t->asMatrix()) {
    if (auto btm = a1t->asMatrix()) {
      if (atm->cols == btm->cols && 
          atm->rows == btm->rows &&
          atm->elementType == btm->elementType) {
        type = atm;
        return true;
      }
    }
  }
  if (a0t->isStruct() && a1t == a0t) {
    type = a0t;
    return true;
  }
  if (a0t->isString() && a1t->isString()) {
    type = a0t;
    return true;
  }
  if (a0t->isBool() && a1t->isBool()) {
    type = a0t;
    return true;
  }

  return false;
}


bool ExprSub::resolveTypes(YogaContext const &ctx)
{
  if (!ctx.promoteTypes(args[0], args[1])) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isScalar() && a1t->isScalar()) {
    type = a0t;
    return true;
  }
  if (a0t->isComplex() && a1t->isComplex()) {
    type = a0t;
    return true;
  }
  if (auto a0tm = a0t->asMatrix()) {
    if (auto a1tm = a1t->asMatrix()) {
      if (a0tm->cols == a1tm->cols && 
          a0tm->rows == a1tm->rows &&
          a0tm->elementType == a1tm->elementType) {
        type = a0tm;
        return true;
      }
    }
  }
  return false;
}


bool ExprMul::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  if (!ctx.promoteTypes(args[0], args[1])) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isScalar() && a1t->isScalar()) {
    type = a0t;
    return true;
  }

  if (a0t->isComplex() && a1t->isComplex()) {
    type = a0t;
    return true;
  }

  if (a0t->isReal() && a1t->isStruct()) {
    type = a1t;
    return true;
  }
  if (a0t->isStruct() && a1t->isReal()) {
    type = a0t;
    return true;
  }

  if (auto a0tm = a0t->asMatrix()) {
    if (auto a1tm = a1t->asMatrix()) {
      if (a0tm->cols == a1tm->rows && a0tm->elementType == a1tm->elementType) {
        type = ctx.reg->getMatrix(
          ctx,
          a0tm->elementType,
          a0tm->rows,
          a1tm->cols);
        return true;
      }
    }
  }

  if (a0t->isReal() && a1t->isMatrix()) {
    type = a1t;
    return true;
  }
  if (a0t->isMatrix() && a1t->isReal()) {
    type = a0t;
    return true;
  }

  if (a0t->isString() && a1t->isReal()) {
    type = a0t;
    return true;
  }
  if (a0t->isReal() && a1t->isString()) {
    type = a1t;
    return true;
  }


  return false;
}


bool ExprConjugate::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 1);
  setCseFromArgs();
  auto a0t = args[0]->type;

  if (auto a0tm = a0t->asMatrix()) {
    type = ctx.reg->getMatrix(ctx, a0tm->elementType, a0tm->cols, a0tm->rows);
    return true;
  }
  if (a0t->asPrimitive()) {
    type = a0t;
    return true;
  }
  return false;
}

bool ExprConjugate::knownOp(string const &_op)
{
  if (_op == "conj") return true;
  return false;
}


bool ExprNograd::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 1);
  setCseFromArgs();
  type = args[0]->type;
  return true;
}

bool ExprNograd::knownOp(string const &_op)
{
  if (_op == "nograd") return true;
  return false;
}


bool ExprDot::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (auto a0tm = a0t->asMatrix()) {
    if (auto a1tm = a1t->asMatrix()) {
      if (a0tm->cols == a1tm->cols && 
          a0tm->rows == a1tm->rows && 
          a0tm->elementType == a1tm->elementType) {
        type = a0tm->elementType;
        return true;
      }
    }
  }
  if (a0t->isStruct() && a1t == a0t) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprDot::knownOp(string const &_op)
{
  if (_op == "dot") return true;
  return false;
}


bool ExprDiv::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  if (!ctx.promoteTypes(args[0], args[1])) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isReal() && a1t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  if ((a0t->isStruct() || a0t->isMatrix()) && a1t->isReal()) {
    type = a0t;
    return true;
  }
  return false;
}


bool ExprPow::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isReal() && a1t->isScalar()) {
    type = a0t;
    return true;
  }
  return false;
}

bool ExprCompare::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  if (!ctx.promoteTypes(args[0], args[1])) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isReal() && a1t->isReal()) {
    type = ctx.reg->boolType;
    return true;
  }
  if (a0t->isStruct() && a1t->isStruct()) {
    type = ctx.reg->boolType;
    return true;
  }
  if (a0t->isString() && a1t->isString()) {
    type = ctx.reg->boolType;
    return true;
  }
  return false;
}

bool ExprCompare::knownOp(string const &_op)
{
  if (_op == "<" || _op == "<=" || _op == ">" || _op == ">=") return true;
  if (_op == "==") return true;
  return false;
}


bool ExprLogicalOr::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isReal() && a1t->isReal()) {
    type = a0t;
    return true;
  }
  if (a0t->isBool() && a1t->isBool()) {
    type = a0t;
    return true;
  }
  return false;
}


bool ExprLogicalAnd::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 2);
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (a0t->isReal() && a1t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  if (a0t->isBool() && a1t->isBool()) {
    type = ctx.reg->boolType;
    return true;
  }
  return false;
}

bool ExprNot::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 1);
  setCseFromArgs();
  auto a0t = args[0]->type;

  if (a0t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  if (a0t->isBool()) {
    type = ctx.reg->boolType;
    return true;
  }
  return false;
}

bool ExprNeg::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 1);
  setCseFromArgs();
  auto a0t = args[0]->type;

  if (a0t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprLim01::resolveTypes(YogaContext const &ctx)
{
  assert(args.size() == 1);
  setCseFromArgs();
  auto a0t = args[0]->type;

  if (a0t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprLinearComb::resolveTypes(YogaContext const &ctx)
{
  if (args.size() < 2) return false;

  for (size_t i=0; i+1 < args.size(); i += 2) {
    if (!ctx.convertType(args[i], ctx.reg->rType)) return false;
    if (i+3 < args.size()) {
      if (!ctx.promoteTypes(args[i+1], args[i+3])) return false;
    }
  }
  setCseFromArgs();

  num = nullptr;
  den = nullptr;
  result = nullptr;

  for (size_t i=0; i+1 < args.size(); i += 2) {
    // each pair is (strength, value)
    // values have to be the same
    if (args[i+0]->type->isReal()) {
      if (args[i+1]->type == args[1]->type) {
        // OK
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }

    auto term = ctx.mkExpr<ExprMul>(args[i+0], args[i+1]);
    terms.push_back(term);

    if (num) {
      num = ctx.mkExpr<ExprAdd>(num, term);
    } 
    else {
      num = term;
    }

    if (den) {
      den = ctx.mkExpr<ExprAdd>(den, args[i+0]);
    }
    else {
      den = args[i+0];
    }
  }

  den = ctx.mkExpr<ExprMinMax>("max", vector<ExprNode *>{
    den, ctx.mkExpr<ExprConstOne>(den->type)
  });

  result = ctx.mkExpr<ExprDiv>(num, den);

  type = args[1]->type;

  return true;
}

bool ExprMinMax::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (auto t = argsAllSameType()) {
    type = t;
    return true;
  }
  return false;
}

bool ExprMinMax::knownOp(string const &_op)
{
  if (_op == "min" || _op == "max") return true;
  return false;
}



bool ExprClamp::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (auto t = argsAllSameType()) {
    type = t;
    return true;
  }
  return false;
}

bool ExprClamp::knownOp(string const &_op)
{
  if (_op == "clamp") return true;
  if (_op == "relu") return true;
  return false;
}


bool ExprClampGrad::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (auto t = argsAllSameType()) {
    type = t;
    return true;
  }
  return false;
}


bool ExprTrig1::resolveTypes(YogaContext const &ctx)
{
  if (args.size() != 1) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;

  if (a0t->isReal()) {
    type = a0t;
    return true;
  }
  return false;
}

bool ExprTrig1::knownOp(string const &_op)
{
  if (_op == "sin" || _op == "cos" || _op == "tan") return true;
  if (_op == "sinh" || _op == "cosh" || _op == "tanh" || _op == "one_minus_tanhsq") return true;
  if (_op == "acos" || _op == "asin" || _op == "atan") return true;
  if (_op == "exp" || _op == "log") return true;
  if (_op == "sqrt" || _op == "sqr") return true;
  if (_op == "normangle" || _op == "normangle2") return true;
  if (_op == "sign" || _op == "abs") return true;
  return false;
}


bool ExprTrig2::resolveTypes(YogaContext const &ctx)
{
  if (args.size() != 2) return false;
  setCseFromArgs();
  auto a0t = args[0]->type;
  auto a1t = args[1]->type;

  if (op == "atan2" && a0t->isReal() && a1t->isReal()) {
    type = ctx.reg->rType;
    return true;
  }
  return false;
}

bool ExprTrig2::knownOp(string const &_op)
{
  if (_op == "atan2") return true;
  return false;
}


bool ExprObject::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs(joinString(keys, "."), "");
  type = ctx.reg->objectType;
  return true;
}


bool ExprConstructor::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  type = ctx.getType(clsName);
  if (!type) return ctx.logError("No matching types for "s + op);

  if (args.size() == 1 && args[0]->type == type) {
    return true;
  }

  if (auto typeStruct = type->asStruct()) {

    auto nMembers = typeStruct->members.size();
    if (args.size() == 1 && args[0]->asObject()) {
      auto a0obj = args[0]->asObject();

      map<string, pair<vector<StructMember *>, vector<ExprNode *>>> nameMap;

      for (auto &memit : typeStruct->members) {
        nameMap[memit->memberName].first.push_back(memit);
      }
      for (size_t keyi = 0; keyi < a0obj->keys.size(); keyi++) {
        nameMap[a0obj->keys[keyi]].second.push_back(a0obj->args[keyi]);
      }
      for (auto &mapit : nameMap) {
        if (mapit.second.second.size() > 1) {
          return ctx(a0obj).logError("Multiple values for "s + shellEscape(mapit.first));
        }
        if (mapit.second.second.empty() && a0obj->defaultValue == nullptr) {
          return ctx(a0obj).logError("No value for "s + shellEscape(mapit.first));
        }
        if (mapit.second.first.empty()) {
          return ctx(a0obj).logError("No member named "s + shellEscape(mapit.first));
        }
      }

      for (size_t keyi = 0; keyi < a0obj->keys.size(); keyi++) {
        if (a0obj->keys[keyi] == "default") continue;
        auto m = typeStruct->getMember(a0obj->keys[keyi]);
        if (!m) return ctx(a0obj->args[keyi]).logError(
          "Got value for "s + shellEscape(a0obj->keys[keyi]) + " but no such member in struct");
      }
      return true;
    }
    else if (args.size() == nMembers) {
      if (nMembers > 7) {
        ctx(args[0]).logWarning("With so many members, may I suggest using a named constructor instead?");
      }
      for (size_t argi = 0; argi < nMembers; argi++) {
        auto ft = typeStruct->members[argi]->memberType;
        auto at = args[argi]->type;
        if (ft == at) {
          // ok
        }
        else if (ft->isScalar() && at->isScalar()) {
          // ok
        }
        else {
          return ctx.logError(
            "Type mismatch for arg " + repr_arg(argi) + " of " + shellEscape(op) +
            ": expected " + repr_type(ft) +
            " got " + repr_type(at));
        }
      }
      return true;
    }

    return ctx.logError("Expected (Object) or (...)"s);
  }

  if (auto typeMatrix = type->asMatrix()) {

    size_t nMembers = (size_t)(typeMatrix->rows * typeMatrix->cols);
    if (args.size() == nMembers) {
      for (size_t argi = 0; argi < nMembers; argi++) {
        if (typeMatrix->elementType != args[argi]->type) {
          return ctx.logError("Type mismatch for arg " + repr_arg(argi) + 
            ": expected " + repr_type(typeMatrix->elementType) +
            " got " + repr_type(args[argi]->type));
        }
      }
    }
    else {
      return ctx.logError(
        "Expected "s + to_string(nMembers) + 
        " args for "s + op + ", got "s + to_string(args.size()));
    }

    return true;
  }

  return false;
}


bool ExprMatrixConstructor::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (op == "mat44Identity" && 
      args.empty()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
    return true;
  }
  if (op == "mat44Translation" && 
      args.size() == 3 && 
      args[0]->type->isReal() && 
      args[1]->type->isReal() && 
      args[2]->type->isReal()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
    return true;
  }
  if (op == "mat44TranslationDeriv" && 
      args.size() == 6 && 
      args[0]->type->isReal() && 
      args[1]->type->isReal() && 
      args[2]->type->isReal() &&
      args[3]->type->isReal() && 
      args[4]->type->isReal() && 
      args[5]->type->isReal()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
    return true;
  }
  if ((op == "mat44RotationX" || op == "mat44RotationY" || op == "mat44RotationZ") && 
      args.size() == 1 && 
      args[0]->type->isReal()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
    return true;
  }
  if ((op == "mat44RotationXDeriv" || op == "mat44RotationYDeriv" || op == "mat44RotationZDeriv") && 
      args.size() == 2 && 
      args[0]->type->isReal() &&
      args[1]->type->isReal()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
    return true;
  }
  if ((op == "mat33RotationX" || op == "mat33RotationY" || op == "mat33RotationZ") && 
      args.size() == 1 && 
      args[0]->type->isReal()) {
    type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 3, 3);
    return true;
  }
  return ctx.logError("No matching types for "s + op);
}

bool ExprMatrixConstructor::knownOp(string const &_op)
{
  if (_op == "mat44Translation") return true;
  if (_op == "mat44Identity") return true;
  if (_op == "mat33Identity") return true;
  if (_op == "mat22Identity") return true;
  if (_op == "mat44RotationX" || _op == "mat44RotationY" || _op == "mat44RotationZ") return true;
  if (_op == "mat33RotationX" || _op == "mat33RotationY" || _op == "mat33RotationZ") return true;
  return false;
}


bool ExprSum::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (op == "sum" && 
      args.size() == 1 && 
      args[0]->type->asStruct() && args[0]->type->asStruct()->isOnehot) {
    type = ctx.reg->rType;
    return true;
  }
  if (op == "hypot" && 
      args.size() == 1 &&
      args[0]->type->hasLinearOps()) {
    type = ctx.reg->rType;
    return true;
  }
  if (op == "normsq" && 
      args.size() == 1 &&
      args[0]->type->hasLinearOps()) {
    type = ctx.reg->rType;
    return true;
  }

  return ctx.logError("No matching types for "s + op);
}

bool ExprSum::knownOp(string const &_op)
{
  if (_op == "sum") return true;
  if (_op == "hypot") return true;
  if (_op == "normsq") return true;
  return false;
}


bool ExprMatrixMath::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (op == "fromHomo" && args.size() == 1) {
    if (auto a0tm = args[0]->type->asMatrix()) {
      if (a0tm->rows == 4 && a0tm->cols == 1) {
        type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 3, 1);
        return true;
      }
    }
  }
  if (op == "matToHomo" && args.size() == 1) {
    if (auto a0tm = args[0]->type->asMatrix()) {
      if (a0tm->rows == 3 && a0tm->cols == 3) {
        type = ctx.reg->getMatrix(ctx, ctx.reg->rType, 4, 4);
        return true;
      }
    }
  }

  return ctx.logError("No matching types for "s + op);
}

bool ExprMatrixMath::knownOp(string const &_op)
{
  if (_op == "fromHomo") return true;
  if (_op == "toHomo") return true;
  if (_op == "matFromHomo") return true;
  if (_op == "matToHomo") return true;
  return false;
}


bool ExprMatrixIndex::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  if (args.size() == 3 && 
      args[0]->type->asMatrix() &&
      args[1]->type->isPrimitive() &&
      args[2]->type->isPrimitive()) {
    type = args[0]->type->getElementType();
    return true;
  }
  if (args.size() == 2 && 
      args[0]->type->asMatrix() &&
      args[1]->type->isPrimitive()) {
    type = args[0]->type->getElementType();
    return true;
  }
  ctx.logError("Can't resolve types for index"s);
  ctx.logExpr("Expr is "s, this);
  return false;
}

bool ExprPhi::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs("", to_string(int(aset.mode)));
  type = aset.dst->type;
  return true;
}

bool ExprCallYoga::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();
  debugLocalName = funcName;

  int outi = 0;
  for (auto &it : outTypes) {
    outVals.push_back(ctx.mkExpr<ExprReturnVal>(it, this, outi));
    outi ++;
  }
  type = ctx.reg->voidType;

  return true;
}

bool ExprCallYogaGrad::resolveTypes(YogaContext const &ctx)
{
  setCseFromArgs();

  int outi = 0;
  for (auto &it : outTypes) {
    outVals.push_back(ctx.mkExpr<ExprReturnVal>(it, this, outi));
    outi ++;
  }

  outi = 0;
  for (auto &it : inArgs) {
    inArgGradOuts.push_back(ctx.mkExpr<ExprReturnGrad>(it->type, this, outi));
    outi ++;
  }

  return true;
}
