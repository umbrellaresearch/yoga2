#pragma once
#include "./jit_utils.h"
#include "./tokenize.h"
#include "./ast.h"
#include "./compilation.h"
#include "./context.h"
#include "./annotation.h"

struct YogaParser {

  YogaParser()
  {
  }

  deque<YogaTokenizer> includeStack;
  YogaTokenizer *tok{nullptr};
  int verbose;

  [[nodiscard]] bool parseFile(YogaContext const &ctx, string const &fn, bool isBuiltin, YogaParserMode mode)
  {
    bool &mig = ctx.reg->multipleIncludeGuard[fn];
    if (mig) return true;
    mig = true;

    auto src = ctx.reg->sources->getFile(ctx, fn, isBuiltin);
    if (!src) return false;
    return parseText(ctx, src, mode);
  }

  [[nodiscard]] bool parseText(YogaContext const &ctx, YogaSourceFile *src, YogaParserMode mode)
  {
    pushTokenizer(src);

    try {
      bool ok = false;
      switch (mode) {
        case ParseToplevel:
          ok = parseToplevel(ctx);
          break;
        case ParseCell:
          ok = parseCell(ctx);
          break;
      }

      if (tok->match(T_ERR)) {
        string err;
        tok->consume(T_ERR, err);
        ctx(tok).logError(err);
        popTokenizer();
        return false;
      }
      popTokenizer();
      return ok;
    }
    catch(runtime_error const &e)
    {
      ctx.reg->diagLog << "Internal compiler error: "s + e.what() + "\n";
      return false;
    }
  }

  void pushTokenizer(YogaSourceFile *src)
  {
    includeStack.emplace_back(src);
    tok = &includeStack.back();
    tok->nextTok();
  }

  void popTokenizer()
  {
    includeStack.pop_back();
    if (includeStack.size() > 0) {
      tok = &includeStack.back();
    }
    else {
      tok = nullptr;
    }
  }

  [[nodiscard]] bool parseToplevel(YogaContext const &ctx)
  {
    while (tok) {
      if (tok->match(T_EOF)) {
        break;
      }
      else if (tok->match(T_ERR)) {
        return parseTokenErr(ctx(tok, "err"));
      }
      else if (tok->match(T_FUNCTION)) {
        if (!parseFunctionDef(ctx(tok, "function"))) return false;
      }
      else if (tok->match(T_STRUCT)) {
        if (!parseStructDef(ctx(tok, "struct"))) return false;
      }
      else if (tok->match(T_ONEHOT)) {
        if (!parseOnehotDef(ctx(tok, "onehot"))) return false;
      }
      else if (tok->match(T_REQUIRE)) {
        if (!parseRequire(ctx(tok, "require"))) return false;
      }
      else if (tok->match(T_SEMI)) {
        tok->consume(T_SEMI);
      }
      else if (tok->consume(T_RUNTIME)) {
        auto runtime = parseObject(ctx(tok, "runtime"));
        if (!runtime) return false;
        if (ctx.reg->runtime) 
        runtime->prev = ctx.reg->runtime;
        ctx.reg->runtime = runtime;
      }
      else if (tok->consume(T_LIVE)) {
        auto engineDef = parseAnnoCall(ctx(tok, "live"));
        if (!engineDef) return false;
        ctx.reg->engineDefs.push_back(engineDef);
      }
      else if (tok->consume(T_INIT)) {
        auto initDef = parseAnnoCall(ctx(tok, "init"));
        if (!initDef) return false;
        ctx.reg->initDefs.push_back(initDef);
      }
      else if (tok->consume(T_PANEL)) {
        auto panelDef = parseAnnoCall(ctx(tok, "panel"));
        if (!panelDef) return false;
        ctx.reg->panelDefs.push_back(panelDef);
      }
      else if (tok->consume(T_PREDICT)) {
        auto predictDef = parseAnnoCall(ctx(tok, "predict"));
        if (!predictDef) return false;
        ctx.reg->predictDefs.push_back(predictDef);
      }
      else if (tok->consume(T_POLYFIT)) {
        auto ac = parseObject(ctx("polyfit"));
        if (!ac) return false;
        ctx.reg->polyfitDefs.push_back(ac);
      }
      else if (tok->consume(T_TRAIN)) {
        auto trainCtx = ctx(tok);
        auto ac = parseAnnoCall(ctx("train"));
        if (!ac) return false;
        ctx.reg->trainDefs.push_back(ac);
        trainCtx.mkAnnotation<AnnoTrain>(ac);
      }
      else {
        if (!parseGlobalDef(ctx(tok, "globalDef"))) return false;
      }
    }
    return true;
  }

  /*
    One or more statements separated by semicolons.
    Last statement must be an assignment to a name, which becomes the name of this cell.
  */
  [[nodiscard]] bool parseCell(YogaContext const &ctx)
  {
    auto lhs = parseSuffixed(ctx(tok, "cellName"));
    if (!lhs) {
      return ctx.logError("expected lhs");
    }

    string lhsKey;
    if (!lhs->getLiteral(lhsKey)) {
      return ctx.logError("lhs not a literal");
    }

    if (!tok->consume(T_EQ)) {
      return ctx.logError("expected =");
    }

    auto rhs = parseExpr(ctx(tok));
    if (!rhs) return false;

    ctx.reg->cellTable[lhsKey].rhs = rhs;
    return true;
  }


  [[nodiscard]] bool parseTokenErr(YogaContext const &ctx)
  {
    string err;
    tok->consume(T_ERR, err);
    return ctx.logError(err);
  }

  [[nodiscard]] bool parseFormals(YogaContext const &ctx, vector<AstArgDecl *> &args)
  {
    if (tok->consume(T_RPAR)) {
    }
    else {
      while (1) {
        auto a = parseArgDecl(ctx(tok, "argDecl"));
        if (!a) return false;
        args.emplace_back(a);
        if (tok->consume(T_RPAR)) {
          break;
        }
        else if (tok->consume(T_COMMA)) {
        }
        else {
          ctx(tok).logError("function args: Expected \",\" or ')'"s);
          return false;
        }
      }
    }
    return true;
  }

  [[nodiscard]] bool parseFunctionDef(YogaContext const &ctx)
  {
    if (!tok->consume(T_FUNCTION)) return ctx.logError("function: Expected function");
    string funcName;
    auto nameCtx = ctx(tok);
    if (!tok->consume(T_NAME, funcName)) return ctx(tok).logError("function: Expected name");

    if (!tok->consume(T_LPAR)) return ctx(tok).logError("function: Expected \"(\"");

    vector<AstArgDecl *> args;
    if (!parseFormals(ctx, args)) return false;

    auto body = parseBlock(ctx(tok, "block"));
    if (!body) return false;
    auto f = nameCtx.mkAst<AstFunction>(funcName, args, body);
    if (ctx.reg->verbose >= 5) {
      f->dump(cerr, ""s, 4);
    }
    if (!ctx.setTopLevel(funcName, f)) return false;
    ctx.lookup(funcName).astFunction = f;
    return true;
  }


  [[nodiscard]] bool parseForeignDecl(YogaContext const &ctx, CallableYogaFunction cppCallable, CallableYogaDebug cppCallableDebug)
  {
    if (!tok->consume(T_FUNCTION)) return ctx.logError("function: Expected function");
    string funcName;
    auto nameCtx = ctx(tok);
    if (!tok->consume(T_NAME, funcName)) return ctx(tok).logError("function: Expected name");

    if (!tok->consume(T_LPAR)) return ctx(tok).logError("function: Expected \"(\"");

    vector<AstArgDecl *> args;
    if (!parseFormals(ctx, args)) return false;

    auto f = nameCtx.mkAst<AstFunction>(funcName, args, nullptr, cppCallable, cppCallableDebug);
    if (!ctx.setTopLevel(funcName, f)) return false;
    auto &slot = ctx.lookup(funcName);
    slot.astFunction = f;
    return true;
  }

  AstArgDecl *parseArgDecl(YogaContext const &ctx)
  {
    string dir;
    if (tok->consume(T_IN)) {
      dir = "in";
    }
    else if (tok->consume(T_OUT)) {
      dir = "out";
    }
    else if (tok->consume(T_UPDATE)) {
      dir = "update";
    }
    else {
      return ctx(tok).logErrorNull("Expected 'in', 'out', 'update', or 'bi'");
    }
    auto clsName = parseClsName(ctx(tok, "argType"));
    if (!clsName) return nullptr;
    auto name = parseName(ctx(tok, "argName"));
    if (!name) return nullptr;
    bool optional = false;
    if (tok->consume(T_QUERY)) {
      optional = true;
    }
    return ctx(name).mkAst<AstArgDecl>(dir, clsName, name, optional);
  }


  AstBlock *parseBlock(YogaContext const &ctx)
  {
    if (!tok->consume(T_LWING)) return ctx.logErrorNull("Expected \"{\"");

    auto ret = ctx.mkAst<AstBlock>();

    while (1) {
      if (tok->consume(T_RWING)) {
        break;
      }
      else {
        auto st = parseStatement(ctx(tok, "statement"));
        if (!st) return nullptr;
        ret->addStatement(st);
      }
    }
    return ret;
  }

  AstStatement *parseStatement(YogaContext const &ctx, bool noTrailingSemiNeeded=false)
  {
    if (tok->consume(T_IF)) {
      if (!tok->consume(T_LPAR)) return ctx(tok).logErrorNull("if: Expected \"(\"");
      auto expr = parseExpr(ctx(tok, "ifCond"));
      if (!expr) return nullptr;
      if (!tok->consume(T_RPAR)) return ctx(tok).logErrorNull("if: Expected \")\"");
      auto ifTrue = parseBlock(ctx(tok, "ifTrue"));
      if (!ifTrue) return nullptr;
      AstBlock *ifFalse;
      if (tok->consume(T_ELSE)) {
        ifFalse = parseBlock(ctx(tok, "ifFalse"));
        if (!ifFalse) return nullptr;
      } 
      else {
        ifFalse = ctx.mkAst<AstBlock>(); // empty
      }
      return ctx.mkAst<AstIf>(expr, ifTrue, ifFalse);
    }
    else if (tok->consume(T_DEFAULT)) {
      auto block = parseBlock(ctx(tok, "default"));
      if (!block) return nullptr;
      return ctx.mkAst<AstDefault>(block);
    }
    else {
      auto expr = parseExpr(ctx(tok, "expr"));
      if (!expr) return nullptr;
      if (noTrailingSemiNeeded && tok->match(T_EOF)) {
        // OK
      }
      else if (!tok->consume(T_SEMI)) {
        return ctx(tok).logErrorNull("Expected \";\"");
      }
      return ctx.mkAst<AstSimple>(expr);
    }
  }

  AstExpr *parseExpr(YogaContext const &ctx)
  {
    auto lhs = parseSwitch(ctx("switch"));
    if (!lhs) return nullptr;

    auto opCtx = ctx(tok);
    if (tok->consume(T_EQ)) {
      auto rhs = parseExpr(ctx(tok));
      if (!rhs) return nullptr;
      return opCtx.mkAst<AstBinop>("="s, lhs, rhs);
    }
    else if (tok->consume(T_PLUSEQ)) {
      auto rhs = parseExpr(ctx(tok));
      if (!rhs) return nullptr;
      return opCtx.mkAst<AstBinop>("+="s, lhs, rhs);
    }
    else {
      return lhs;
    }
  }

  AstExpr *parseSwitch(YogaContext const &ctx)
  {
    auto cond = parseOr(ctx("switch?:"));
    if (!cond) return nullptr;

    auto opCtx = ctx(tok);
    if (tok->consume(T_QUERY)) {
      auto ifTrue = parseOr(ctx(tok, "?or:"));
      if (!ifTrue) return nullptr;
      if (!tok->consume(T_COLON)) return ctx(tok).logErrorNull("switch: Expected \":\"");
      auto ifFalse = parseSwitch(ctx(tok, "?:switch"));
      if (!ifFalse) return nullptr;
      return opCtx.mkAst<AstTernop>("?:", cond, ifTrue, ifFalse);
    }
    else {
      return cond;
    }
  }


  AstExpr *parseOr(YogaContext const &ctx)
  {
    auto lhs = parseAnd(ctx("and"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_OROR)) {
        auto rhs = parseAnd(ctx(tok, "||and"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("||", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseAnd(YogaContext const &ctx)
  {
    auto lhs = parseEquals(ctx("equals"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_ANDAND)) {
        auto rhs = parseEquals(ctx(tok, "&&equals"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("&&", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseEquals(YogaContext const &ctx)
  {
    auto lhs = parseSimilar(ctx("similar"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_EQEQ)) {
        auto rhs = parseSimilar(ctx(tok, "==similar"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("==", lhs, rhs);
      }
      else if (tok->consume(T_BANGEQ)) {
        auto rhs = parseSimilar(ctx(tok, "!=similar"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("!=", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseSimilar(YogaContext const &ctx)
  {
    auto lhs = parseCompare(ctx("compare"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_TILTIL)) {
        auto rhs = parseCompare(ctx(tok, "~~compare"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("~~", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseCompare(YogaContext const &ctx)
  {
    auto lhs = parseSum(ctx("sum"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_LE)) {
        auto rhs = parseSum(ctx(tok, "<=sum"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("<=", lhs, rhs);
      }
      else if (tok->consume(T_LT)) {
        auto rhs = parseSum(ctx(tok, "<sum"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("<", lhs, rhs);
      }
      else if (tok->consume(T_GE)) {
        auto rhs = parseSum(ctx(tok, ">=sum"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>(">=", lhs, rhs);
      }
      else if (tok->consume(T_GT)) {
        auto rhs = parseSum(ctx(tok, ">sum"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>(">=", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseSum(YogaContext const &ctx)
  {
    auto lhs = parseProduct(ctx("product"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_PLUS)) {
        auto rhs = parseProduct(ctx(tok, "+product"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("+", lhs, rhs);
      }
      else if (tok->consume(T_MINUS)) {
        auto rhs = parseProduct(ctx(tok, "-product"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("-", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }


  AstExpr *parseProduct(YogaContext const &ctx)
  {
    auto lhs = parsePower(ctx("power"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_STAR)) {
        auto rhs = parsePower(ctx(tok, "*power"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("*", lhs, rhs);
      }
      else if (tok->consume(T_DIV)) {
        auto rhs = parsePower(ctx(tok, "/power"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("/", lhs, rhs);
      }
      else if (tok->consume(T_MOD)) {
        auto rhs = parsePower(ctx(tok, "%power"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("%", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parsePower(YogaContext const &ctx)
  {
    auto lhs = parseParam(ctx("param"));
    if (!lhs) return nullptr;

    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_HAT)) {
        auto rhs = parseParam(ctx(tok, "^param"));
        if (!rhs) return nullptr;
        lhs = opCtx.mkAst<AstBinop>("^", lhs, rhs);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  AstExpr *parseParam(YogaContext const &ctx)
  {
    auto lhs = parsePrefixed(ctx("prefixed"));
    if (!lhs) return nullptr;
    if (tok->match(T_TILDA)) {
      auto rangeSpec = parseRangeSpec(ctx(tok, "~rangeSpec"));
      if (!rangeSpec) return nullptr;
      return ctx.mkAst<AstParam>(lhs, rangeSpec);
    }
    else {
      return lhs;
    }
  }

  AstExpr *parsePrefixed(YogaContext const &ctx)
  {
    vector<pair<string, YogaContext>> ops;
    while (1) {
      auto opCtx = ctx(tok);
      if (tok->consume(T_PLUS)) {
        if (0) ops.push_back(make_pair("+"s, opCtx)); // Don't even
      }
      else if (tok->consume(T_MINUS)) {
        ops.push_back(make_pair("-", opCtx));
      }
      else if (tok->consume(T_BANG)) {
        ops.push_back(make_pair("!", opCtx));
      }
      else {
        break;
      }
    }
    auto rhs = parseSuffixed(ctx(tok, "suffixed"));
    if (!rhs) return nullptr;

    while (!ops.empty()) {
      auto op = ops.back();

      // As a special case, treat eg -7 as a number with that value, rather than a unary '-' op on the positive number
      if (op.first == "-"s && rhs->asNumber()) {

        /*
          Create a sourceLoc that includes both the number and the unary -.
          This is ends uo as paramSourceLoc in the YogaParamInfo, which is used to
          replace the number in the source text.
        */
        YogaContext combinedCtx = op.second(YogaSourceLoc(
          rhs->sourceLoc.file,
          op.second.lastLoc.start,
          rhs->sourceLoc.end));
        string rhsLiteralNumber;
        if (!rhs->getLiteralNumber(rhsLiteralNumber)) {
          return ctx(rhs).logErrorNull("Not a number");
        }
        rhs = combinedCtx.mkAst<AstNumber>("-"s + rhsLiteralNumber);
      }
      else if (op.first == "+"s && rhs->asNumber()) {
        YogaContext combinedCtx = op.second(YogaSourceLoc(
          rhs->sourceLoc.file,
          op.second.lastLoc.start,
          rhs->sourceLoc.end));
        string rhsLiteralNumber;
        if (!rhs->getLiteralNumber(rhsLiteralNumber)) {
          return ctx(rhs).logErrorNull("Not a number");
        }
        rhs = combinedCtx.mkAst<AstNumber>(rhsLiteralNumber);
      }
      else {
        rhs = op.second.mkAst<AstUnop>(op.first, rhs);
      }
      ops.pop_back();

    }
    return rhs;
  }

  AstExpr *parseSuffixed(YogaContext const &ctx)
  {
    AstExpr * lhs{nullptr};
    if (tok->match(T_LPAR)) {
      lhs = parseParenExpr(ctx("paren"));
      if (!lhs) return nullptr;
    }
    else {
      lhs = parseValue(ctx("value"));
      if (!lhs) return nullptr;
    }

    while (1) {
      if (tok->consume(T_LPAR)) {
        vector<AstExpr *> args;
        if (!parseSepExprs(ctx(tok, "fun("), T_COMMA, T_RPAR, args)) return nullptr;
        auto lhsName = lhs->asName();
        if (!lhsName) {
          ctx.logError("Function is not a literal name"s);
          ctx.logAst("AST", lhs);
          return nullptr;
        }
        lhs = ctx(lhsName).mkAst<AstCall>(lhsName, args);
      }
      else if (tok->consume(T_LBRK)) {
        vector<AstExpr *> args;
        if (!parseSepExprs(ctx(tok, "index["), T_COMMA, T_RBRK, args)) return nullptr;
        lhs = ctx(lhs).mkAst<AstIndex>(lhs, args);
      }
      else if (tok->consume(T_DOT)) {
        auto memberName = parseName(ctx(tok, "name"));
        if (!memberName) return nullptr;
        lhs = ctx(lhs, memberName).mkAst<AstStructref>(lhs, memberName);
      }
      else {
        break;
      }
    }
    return lhs;
  }

  [[nodiscard]] bool parseSepExprs(YogaContext const &ctx, YogaToken sepTok, YogaToken endTok, vector<AstExpr *> &exprs)
  {
    if (tok->consume(endTok)) {
      return true;
    }
    else {
      while (1) {
        auto arg = parseExpr(ctx(tok, "expr"));
        if (!arg) return false;
        exprs.push_back(arg);
        if (tok->consume(sepTok)) {
          continue;
        }
        else {
          break;
        }
      }
      if (!tok->consume(endTok)) {
        ctx(tok).logError("Expected "s + tok->describeTok(endTok));
        return false;
      }
      return true;
    }
  }

  AstCall *parseCall(YogaContext const &ctx)
  {
    auto funcName = parseName(ctx);

    if (!tok->consume(T_LPAR)) {
      return ctx(tok).logErrorNull("Expected \"(\"");
    }
    vector<AstExpr *> args;
    if (!parseSepExprs(ctx(tok, "fun("), T_COMMA, T_RPAR, args)) return nullptr;
    return ctx.mkAst<AstCall>(funcName, args);
  }

  AstName *parseClsName(YogaContext const &ctx)
  {
    string fullName;
    string part;
    if (!tok->consume(T_NAME, part)) return ctx(tok).logErrorNull("Expected clsName");
    ctx.lookup(part).isType = true;

    return parseClsNameSuffix(ctx("clsNameSuffix"), part);
  }

  AstName *parseClsNameSuffix(YogaContext const &ctx, string const &prefix)
  {
    auto fullName = prefix;
    string part;

    while (1) {
      if (tok->consume(T_LBRK)) {
        fullName += "[";
        while (1) {
          if (tok->consume(T_RBRK)) {
            fullName += "]"s;
            break;
          }
          else if (tok->consume(T_NUMBER, part)) {
            fullName += part;
          }
          else if (tok->consume(T_COMMA)) {
            fullName += ","s;
          }
          else {
            auto indexName = parseClsName(ctx(tok, "[]"));
            if (!indexName) return nullptr;

            fullName += indexName->name;
          }
        }
      }
      else {
        break;
      }
    }

    return ctx.mkAst<AstName>(fullName, true);
  }

  AstExpr *parseValue(YogaContext const &ctx)
  {
    if (tok->match(T_NUMBER)) {
      return parseNumber(ctx("number"));
    }
    else if (tok->match(T_LWING)) {
      return parseObject(ctx("object"));
    }
    else if (tok->match(T_LBRK)) {
      return parseArray(ctx("array"));
    }
    else if (tok->match(T_STRING)) {
      return parseString(ctx("string"));
    }
    else if (tok->match(T_TRUE) || tok->match(T_FALSE)) {
      return parseBoolean(ctx("boolean"));
    }
    else if (tok->match(T_NULL)) {
      return parseNull(ctx("null"));
    }
    else if (tok->match(T_DERIV)) {
      return parseDeriv(ctx("deriv"));
    }
    else if (tok->match(T_EXTERN)) {
      return parseExtern(ctx("extern"));
    }
    else if (tok->match(T_NAME)) {
      auto ret = parseName(ctx(tok, "name"));
      if (!ret) return nullptr;
      if (ctx.lookup(ret->name).isType) {
        return parseClsNameSuffix(ctx("clsNameSuffix"), ret->name);
      }
      else {
        return ret;
      }
    }
    else if (tok->match(T_COLOR)) {
      return parseColor(ctx("color"));
    }
    else if (tok->consume(T_DOT)) {
      return ctx.mkAst<AstName>(".", false);
    }
    else {
      return ctx(tok).logErrorNull("Expected value"s);
    }
  }

  AstExpr *parseNumber(YogaContext const &ctx)
  {
    string lit;
    if (!tok->consume(T_NUMBER, lit)) return ctx(tok).logErrorNull("number: Expected number");
    return ctx.mkAst<AstNumber>(lit);
  }

  AstExpr *parseString(YogaContext const &ctx)
  {
    string lit;
    if (!tok->consume(T_STRING, lit)) return ctx(tok).logErrorNull("string: Expected string");
    return ctx.mkAst<AstString>(lit);
  }

  AstExpr *parseBoolean(YogaContext const &ctx)
  {
    if (tok->consume(T_TRUE)) {
      return ctx.mkAst<AstBoolean>(true);
    }
    else if (tok->consume(T_FALSE)) {
      return ctx.mkAst<AstBoolean>(false);
    }
    else {
      return ctx.logErrorNull("Expected boolean");
    }
  }

  AstExpr *parseNull(YogaContext const &ctx)
  {
    if (tok->consume(T_NULL)) {
      return ctx.mkAst<AstNull>();
    }
    else {
      return ctx(tok).logErrorNull("Expected null");
    }
  }

  AstExpr *parseColor(YogaContext const &ctx)
  {
    string colorLit;
    if (!tok->consume(T_COLOR, colorLit)) return ctx(tok).logErrorNull("color: Expected rgba");

    string valueLit;
    if (!tok->consume(T_STRING, valueLit)) return ctx(tok).logErrorNull("color: Expected string");

    if (valueLit.size() == 9 && valueLit[0] == '#') {
      auto r = clamp(strtoul(valueLit.substr(1, 2).c_str(), nullptr, 16)/255.0, 0.0, 1.0);
      auto g = clamp(strtoul(valueLit.substr(3, 2).c_str(), nullptr, 16)/255.0, 0.0, 1.0);
      auto b = clamp(strtoul(valueLit.substr(5, 2).c_str(), nullptr, 16)/255.0, 0.0, 1.0);
      auto a = clamp(strtoul(valueLit.substr(7, 2).c_str(), nullptr, 16)/255.0, 0.0, 1.0);

      return ctx.mkAst<AstCall>(
        ctx.mkAst<AstName>("R[4]", true),
        vector<AstExpr *>{
          ctx.mkAst<AstNumber>(to_string(r)),
          ctx.mkAst<AstNumber>(to_string(g)),
          ctx.mkAst<AstNumber>(to_string(b)),
          ctx.mkAst<AstNumber>(to_string(a))
        });

    }
    return ctx(tok).logErrorNull("Expected rgba\"#XXXXXXXX\"");
  }

  AstExpr *parseArray(YogaContext const &ctx)
  {
    if (!tok->consume(T_LBRK)) return ctx(tok).logErrorNull("array: Expected \"[\"");

    vector<AstExpr *> elems;
    while (1) {
      auto it = parseExpr(ctx(tok, "expr"));
      if (!it) return nullptr;
      elems.push_back(it);
      if (tok->match(T_RBRK)) break;
      if (!tok->consume(T_COMMA)) break;
      if (tok->match(T_RBRK)) break; // trailing comma OK
    }

    if (!tok->consume(T_RBRK)) return ctx(tok).logErrorNull("array: Expected \"]\"");
    return ctx.mkAst<AstArray>(elems);

  }

  AstExtern *parseExtern(YogaContext const &ctx)
  {
    if (!tok->consume(T_EXTERN)) return ctx(tok).logErrorNull("extern: Expected extern");

    string externLabel;
    YogaSourceLoc lastTokenLoc = tok->getSourceLoc();
    while (tok->consume(T_DOT)) {
      string component;
      lastTokenLoc = tok->getSourceLoc();        
      if (!tok->consume(T_NAME, component)) return ctx(tok).logErrorNull("extern: Expected name");
      if (!externLabel.empty()) externLabel += ".";
      externLabel += component;
    }
    auto n = ctx.mkAst<AstExtern>(externLabel);
    n->sourceLoc.end = lastTokenLoc.end;
    return n;
  }

  AstObject *parseObject(YogaContext const &ctx)
  {
    if (!tok->consume(T_LWING)) return ctx(tok).logErrorNull("object: Expected \"{\"");

    vector<AstKeyValue *> kvs;
    AstNode *defaultValue{nullptr};
    if (tok->consume(T_RWING)) {
      return ctx.mkAst<AstObject>(kvs, nullptr, nullptr);
    }
    while (1) {
      if (tok->consume(T_DEFAULT)) {
        if (!tok->consume(T_COLON)) return ctx(tok).logErrorNull("object: Expected \":\"");
        auto value = parseExpr(ctx(tok, "default:expr"));
        if (!value) return nullptr;
        if (defaultValue) return ctx(tok).logErrorNull("object: Multiple defaults");

        defaultValue = value;
      }
      else {
        auto it = parsePair(ctx(tok, "pair"));
        if (!it) return nullptr;
        kvs.push_back(it);
      }
      if (tok->match(T_RWING)) break;
      if (!tok->consume(T_COMMA)) break;
      if (tok->match(T_RWING)) break; // trailing comma OK
    }
    if (!tok->consume(T_RWING)) return ctx(tok).logErrorNull("object: Expected \"}\"");
    return ctx.mkAst<AstObject>(kvs, nullptr, defaultValue);
  }

  AstKeyValue *parsePair(YogaContext const &ctx)
  {
    AstNode * key;
    if (tok->match(T_STRING)) {
      auto stringKey = parseString(ctx(tok, "string:"));
      if (!stringKey) return nullptr;
      key = stringKey;
    }
    else if (tok->match(T_NAME)) {
      auto nameKey = parseDottedName(ctx(tok, "name:"));
      if (!nameKey) return nullptr;
      key = nameKey;
    }
    else {
      return ctx.logErrorNull("pair: Expected key");
    }
    if (!tok->consume(T_COLON)) return ctx(tok).logErrorNull("pair: Expected \":\"");

    auto value = parseExpr(ctx(tok, ":expr"));
    if (!value) return nullptr;
    return ctx.mkAst<AstKeyValue>(key, value);
  }


  AstExpr *parseDeriv(YogaContext const &ctx)
  {
    if (!tok->consume(T_DERIV)) return ctx(tok).logErrorNull("Expected 'deriv'");
    if (!tok->consume(T_LPAR)) return ctx(tok).logErrorNull("deriv: Expected \"(\"");
    auto arg = parseExpr(ctx(tok, "deriv(arg,)"));
    if (!arg) return nullptr;
    if (!tok->consume(T_COMMA)) return ctx(tok).logErrorNull("deriv: Expected \",\"");
    auto wrt = parseExpr(ctx(tok, "deriv(,wrt)"));
    if (!wrt) return nullptr;
    if (!tok->consume(T_RPAR)) return ctx(tok).logErrorNull("deriv: Expected \")\"");

    return ctx(tok).mkAst<AstDeriv>(arg, wrt);
  }

  AstName *parseName(YogaContext const &ctx)
  {
    string name;
    if (!tok->consume(T_NAME, name)) return ctx(tok).logErrorNull("name: Expected name");
    return ctx.mkAst<AstName>(name, false);
  }

  AstName *parseDottedName(YogaContext const &ctx)
  {
    string dottedName;
    while (1) {
      string component;
      if (!tok->consume(T_NAME, component)) return ctx(tok).logErrorNull("name: Expected name");
      dottedName += component;
      if (tok->consume(T_DOT)) {
        dottedName += ".";
        continue;
      }
      else {
        break;
      }
    }
    return ctx(tok).mkAst<AstName>(dottedName, false);
  }

  AstRangeSpec *parseRangeSpec(YogaContext const &ctx)
  {
    if (!tok->consume(T_TILDA)) return ctx(tok).logErrorNull("rangeSpec: Expected \"~\"");

    string dist("normal");
    if (tok->consume(T_PLUS)) {
      dist = "normal"s;
    }
    else if (tok->consume(T_STAR)) {
      dist = "lognormal"s;
    }

    auto fullLoc = tok->getSourceLoc();

    bool negate = false;
    if (tok->consume(T_MINUS)) {
      negate = true;
    }

    if (tok->match(T_NUMBER)) {
      fullLoc.end = tok->getSourceLoc().end;
      auto n = parseNumber(ctx(tok, "~number"));
      if (!n) return nullptr;

      if (negate) {
        string nLiteralNumber;
        if (!n->getLiteralNumber(nLiteralNumber)) {
          return ctx(n).logErrorNull("Not a number");
        }
        n = ctx(fullLoc).mkAst<AstNumber>("-"s + nLiteralNumber);
      }
      return ctx(fullLoc).mkAst<AstRangeSpec>(dist, n);
    }
    else if (tok->match(T_LWING)) {
      return ctx.logErrorNull("WRITEME maybe: rangeSpec with object");
    }
    else {
      return ctx(tok).logErrorNull("rangeSpec: Expected number or object");
    }
  }


  AstExpr *parseParenExpr(YogaContext const &ctx)
  {
    if (!tok->consume(T_LPAR)) return ctx(tok).logErrorNull("parenExpr: Expected \"(\""s);
    auto ret = parseExpr(ctx(tok, "expr"));
    if  (!ret) return nullptr;
    if (!tok->consume(T_RPAR)) return ctx(tok).logErrorNull("parenExpr: Expected \")\""s);
    return ret;
  }


  [[nodiscard]] bool parseStructDef(YogaContext const &ctx)
  {
    if (!tok->consume(T_STRUCT)) return ctx(tok).logError("Expected 'struct'"s);
    auto clsName = parseClsName(ctx(tok, "clsName"));
    if (!clsName) return false;
    if (!tok->consume(T_LWING)) return ctx(tok).logError("struct: Expected \"{\""s);


    vector<AstMemberDecl *> memberDecls;
    set<string> memberNames;
    while (1) {
      if (tok->match(T_NAME)) {
        auto md = parseMemberDecl(ctx(tok, "memberDecl"));
        if (!md) return false;
        for (auto &it : md->members) {
          if (memberNames.count(it.first->name)) {
            return ctx(md->sourceLoc).logError("Duplicate member "s + it.first->name);
          }
          memberNames.insert(it.first->name);
        }
        memberDecls.push_back(md);
      }
      else {
        break;
      }
    }

    vector<AstKeyValue *> options;

    while (1) {
      auto optionCtx = ctx(tok, "option");
      if (tok->consume(T_ELLIPSIS)) {
        options.push_back(optionCtx.mkAst<AstKeyValue>(ctx.mkAst<AstName>("ellipsis", false), ctx.mkAst<AstBoolean>(true)));
      }
      else if (tok->consume(T_LWING)) {
        while (1) {
          auto it = parsePair(optionCtx("{pair"));
          if (!it) return false;
          options.push_back(it);
          if (tok->match(T_RWING)) break;
          if (!tok->consume(T_COMMA)) break;
        }
        if (!tok->consume(T_RWING)) return (bool)optionCtx.logError("options: Expected \"}\""s);
      }
      else {
        break;
      }
    }

    if (!tok->consume(T_RWING)) return ctx(tok).logError("structDecl: Expected \"}\""s);

    auto node = ctx(clsName).mkAst<AstStructDef>(clsName, memberDecls, options);

    if (ctx.reg->verbose >= 5) {
      node->dump(cerr, ""s, 4);
    }
    return ctx.setTopLevel(clsName->name, node);
  }

  AstMemberDecl *parseMemberDecl(YogaContext const &ctx)
  {
    auto clsName = parseClsName(ctx(tok, "clsName"));
    if (!clsName) return nullptr;
    
    AstRangeSpec * rangeSpec{nullptr};
    if (tok->match(T_TILDA)) {
      rangeSpec = parseRangeSpec(ctx(tok, "~rangeSpec"));
      if (!rangeSpec) return nullptr;
    }
    vector<pair<AstName *, AstExpr *>> members;

    while (1) {

      auto memberName = parseName(ctx(tok, "memberName"));
      if (!memberName) return nullptr;

      AstExpr * initVal {nullptr};
      if (tok->consume(T_EQ)) {
        initVal = parseSwitch(ctx("=switch"));
        if (!initVal) return nullptr;
      }

      members.emplace_back(memberName, initVal);

      if (tok->consume(T_COMMA)) {
        continue;
      }
      else {
        break;
      }
    }

    while (tok->match(T_LWING)) {
      return ctx(tok).logErrorNull("WRITEME: parseMemberDecl options"s);
    }

    if (!tok->consume(T_SEMI)) return ctx(tok).logErrorNull("memberDecl: Expected \";\""s);

    return ctx(clsName).mkAst<AstMemberDecl>(clsName, rangeSpec, members);

  }

  AstAnnoCall *parseAnnoCall(YogaContext const &ctx)
  {
    auto call = parseCall(ctx);
    AstObject *options = parseOptionsOrSemi(ctx(tok));
    return ctx.mkAst<AstAnnoCall>(call, options);
  }

  AstObject *parseOptionsOrSemi(YogaContext const &ctx)
  {
    AstObject *options = nullptr;
    if (tok->match(T_LWING)) {
      return parseObject(ctx(tok, "object"));
      if (!options) return nullptr;
    }
    else if (tok->consume(T_SEMI)) {
      vector<AstKeyValue *> kvs;
      return ctx.mkAst<AstObject>(kvs, nullptr, nullptr);
    }
    return ctx(tok).logErrorNull("Expected \";\" or \"{\"");
  }

  AstEnumDecl *parseEnumDecl(YogaContext const &ctx)
  {
    auto memberName = parseName(ctx(tok, "name"));
    if (!memberName) return nullptr;
    vector<AstObject *> memberOptions;
    while (1) {
      if (tok->match(T_SEMI)) break;
      auto it = parseObject(ctx(tok, "options"));
      if (!it) return nullptr;
      memberOptions.push_back(it);
    }
    if (!tok->consume(T_SEMI)) return ctx(tok).logErrorNull("onehot: Expected \";\"");

    return ctx.mkAst<AstEnumDecl>(memberName, memberOptions);
  }

  [[nodiscard]] bool parseOnehotDef(YogaContext const &ctx)
  {
    if (!tok->consume(T_ONEHOT)) return ctx(tok).logError("Expected 'onehot'");
    auto clsName = parseClsName(ctx(tok, "clsName"));
    if (!clsName) return false;
    if (!tok->consume(T_LWING)) return ctx(tok).logError("onehot: Expected \"{\"");
    vector<AstEnumDecl *> enumDecls;
    while (1) {
      if (tok->match(T_RWING)) {
        break;
      }
      auto it = parseEnumDecl(ctx(tok, "{enumDecl"));
      if (!it) return false;
      enumDecls.push_back(it);
    }
    if (!tok->consume(T_RWING)) return ctx(tok).logError("onehot: Expected \"}\"");

    auto ast = ctx.mkAst<AstOnehot>(clsName, enumDecls);
    return ctx.setTopLevel(clsName->name, ast);
  }

  [[nodiscard]] bool parseRequire(YogaContext const &ctx)
  {
    if (!tok->consume(T_REQUIRE)) return ctx(tok).logError("Expected 'require'");
    if (!tok->consume(T_LPAR)) return ctx(tok).logError("require: Expected \"(\"");
    string fn;
    if (!tok->consume(T_STRING, fn)) return ctx(tok).logError("require: Expected filename");
    if (!tok->consume(T_RPAR)) return ctx(tok).logError("require: Expected \")\"");

    auto rnKey = fn + " " + tok->src->fn;
    auto &rnTarget = ctx.reg->sources->requireNameMap[rnKey];
    bool isBuiltin = tok->src->isBuiltin;
    if (rnTarget.empty()) {
      if (ctx.reg->sources->expectOnlyPseudoFiles) {
        return ctx.logError("No requireNameMap for "s + shellEscape(rnKey));
      }
      auto rp = YogaLayout::instance().requirePath(fn, tok->src->fn);
      rnTarget = get<0>(rp);
      if (get<1>(rp)) isBuiltin = true;
    }
    if (!parseFile(ctx, rnTarget, isBuiltin, ParseToplevel)) return false;
    return true;
  }

  [[nodiscard]] bool parseGlobalDef(YogaContext const &ctx)
  {
    auto expr = parseExpr(ctx(tok, "expr"));
    if (!expr) return false;
    if (!tok->consume(T_SEMI)) return ctx(tok).logError("Expected \";\"");

    auto exprBinop = expr->asBinop();
    if (exprBinop && exprBinop->op == "=") {
      auto lhsName = exprBinop->lhs->asName();
      if (lhsName) {
        auto node = ctx.mkAst<AstSimple>(expr);
        return ctx.setTopLevel(lhsName->name, node);
      }
    }
    return ctx.logError("Global expr not a binding");
  }

};
