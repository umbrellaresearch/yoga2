#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"
#include "./pass_base.h"
#include "./effects.h"

#include "./type_impl.h"

enum BranchDir {
  BRANCH_FALSE,
  BRANCH_TRUE,
  BRANCH_DEFAULT,
};


struct VarBinding {
  ExprNode *lv{nullptr};
  ExprNode *rv{nullptr};
  YogaSourceLoc sourceLoc;
};

struct YogaPassComputeExprs : YogaPassBase {
  YogaPassComputeExprs()
  {
    bindingStack.emplace_back();
  }

  list<map<string, VarBinding>> bindingStack;

  vector<pair<BranchDir, ExprNode *>> modulationStack;
  YogaSourceLoc topLevelSourceLoc;
  FunctionEffects *effects{nullptr};
  int localTmpCount{1};

  void pushBindings()
  {
    if (0) cerr << "Push bindings with " + repr(bindingStack.back().size()) + "\n";
    bindingStack.push_back(bindingStack.back());
  }
  void popBindings()
  {
    bindingStack.pop_back();
  }

  VarBinding & getBinding(string const &name)
  {
    return bindingStack.back()[name];
  }


  [[nodiscard]] bool doTopLevel(YogaContext const &ctx, AstFunction *node) override { return true; }
  [[nodiscard]] bool doTopLevel(YogaContext const &ctx, AstSimple *node) override { return true; }


  [[nodiscard]] bool bindName(YogaContext const &ctx, string const &name, ExprNode *lv, ExprNode *rv)
  {
    if (0) {
      cerr << "Bind " + shellEscape(name) + "\n";
      if (lv) cerr << "lv=\n" << *lv;
      if (rv) cerr << "rv=:\n" << *rv;
    }

    if (name == "_" || name == ".") {
      return ctx.logError("Attempt to bind " + shellEscape(name));
    }

    auto &prev = getBinding(name);
    if (prev.lv && lv) {
      ctx.logError("Bind "s + shellEscape(name) + " as lvalue: already bound at "s + repr(prev.sourceLoc));
      ctx.logExpr("bound to", prev.lv);
      return false;
    }
    else {
      prev.lv = lv;
    }
    if (prev.rv && rv) {
      ctx.logError("Bind "s + shellEscape(name) + " as rvalue: already bound at "s + repr(prev.sourceLoc));
      ctx.logExpr("bound to", prev.rv);
      return false;
    }
    else {
      prev.rv = rv;
    }
    prev.sourceLoc = ctx.lastLoc;
    return true;
  }

  ExprNode *calcModulation(YogaContext const &ctx)
  {
    ExprNode *one = ctx(topLevelSourceLoc).mkExpr<ExprConstDouble>(1.0);
    ExprNode *ret = one;
    for (auto &[branchDir, node] : modulationStack) {
      switch (branchDir) {
        case BRANCH_TRUE:
          ret = ctx(node->sourceLoc).mkExpr<ExprMul>(ret, node);
          break;
        case BRANCH_FALSE:
          ret = ctx(node->sourceLoc).mkExpr<ExprMul>(ret, 
            ctx(node->sourceLoc).mkExpr<ExprNot>(node));
          break;
        case BRANCH_DEFAULT:
          ret = ctx(node->sourceLoc).mkExpr<ExprMul>(ret, node);
          break;
        default:
          throw runtime_error("Unknown BranchDir");
      }
    }
    return ret;
  }

  // FUNCTION
  [[nodiscard]] bool preScan(YogaContext const &ctx, AstFunction *node) override
  {
    if (0) cerr << "preScan(AstFunction "s + shellEscape(node->funcName) + "): push bindings\n";
    pushBindings();
    topLevelSourceLoc = node->sourceLoc;
    effects = ctx.mkEffects<FunctionEffects>();
    effects->cppCallable = node->cppCallable;
    effects->cppCallableDebug = node->cppCallableDebug;
    effects->debugLocalsType = ctx.reg->getStruct(ctx, node->funcName + ".d", false);
    effects->debugLocalsType->isDebugLocals = true;

    for (auto &it : node->args) {
      auto t = ctx.getType(it->type->name);
      if (!t) return ctx(it->type).logError("No type named "s + shellEscape(it->type->name));
      ExprNode *inVar = nullptr;
      ExprNode *outVar = nullptr;
      if (it->dirInUp) {
        effects->inArgs.emplace_back(t, it->name->name);
        inVar = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "in."s + it->name->name, YD_IN);
      }
      if (it->dirOutUp) {
        effects->outArgs.emplace_back(t, it->name->name);
        outVar = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "out."s + it->name->name, YD_OUT);
      }
      if (!bindName(ctx(node), it->name->name, outVar, inVar)) return false;
    }

    if (!bindName(
      ctx(node), 
      "dt"s,
      nullptr,
      ctx.mkExpr<ExprVar>(ctx.reg->rType, node->funcName, "dt"s, YD_IN)
      )) return false;

    if (!bindName(
      ctx(node),
      "astonishment"s,
      ctx.mkExpr<ExprVar>(ctx.reg->rType, node->funcName, "astonishment"s, YD_ACCUM),
      nullptr
      )) return false;

    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstFunction *node) override
  {
    if (!effects->cppCallable) {
      for (auto &it : node->args) {
        auto t = ctx.getType(it->type->name);
        if (!t) return ctx(it->type).logError("No type named "s + shellEscape(it->type->name));
        if (it->dirUpOnly) {
          auto outArg = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "out."s + it->name->name, YD_OUT);
          auto inArg = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "in."s + it->name->name, YD_IN);
          if (!addDefaultValues(ctx, outArg, inArg)) return false;
        }
        else if (it->dirOutOnly) {
          auto outArg = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "out."s + it->name->name, YD_OUT);
          if (!addDefaultValues(ctx, outArg, nullptr)) return false;
        }
      }

      for (auto &it : node->args) {
        auto t = ctx.getType(it->type->name);
        if (!t) return ctx(it->type).logError("No type named "s + shellEscape(it->type->name));
        if (it->dirOutOnly) {
          auto outArg = ctx(it->name).mkExpr<ExprVar>(t, node->funcName, "out."s + it->name->name, YD_OUT);
          if (!checkMissingOutputs(ctx, outArg)) return false;
        }
      }
    }

    ctx.lookup(node->funcName).funcEffects = effects;
    return true;
  }

  AssignmentSet *checkProhibitions(YogaContext const &ctx, ExprNode *node)
  {
    if (!node) return nullptr;
    auto &aset = effects->assignments[node->cseKey];
    if (aset.prohibitedByAssignmentTo) {
      return &aset;
    }

    while (node && node->asStructref()) {
      node = node->asStructref()->args[0];
      auto &aset2 = effects->assignments[node->cseKey];
      if (aset2.srcs.size()) {
        return &aset2;
      }
    }
    return nullptr;
  }


  void addParentProhibitions(YogaContext const &ctx, ExprNode *node, ExprNode *cause)
  {
    while (node && node->asStructref()) {
      node = node->asStructref()->args[0];
      auto &aset = effects->assignments[node->cseKey];
      if (!aset.prohibitedByAssignmentTo) {
        aset.prohibitedByAssignmentTo = cause;
      }
    }
  }


  [[nodiscard]] bool addDefaultValues(YogaContext const &ctx, ExprNode *outArg, ExprNode *inArg)
  {
    const int verbose = 0;
    if (inArg && outArg->type != inArg->type) return ctx(outArg).logError("addDefaultUpdates: type mismatch");
    auto t = outArg->type;

    auto &aset = effects->assignments[outArg->cseKey];

    ExprNode *guaranteed = nullptr;
    for (auto &it : aset.srcs) {
      if (it.second && (it.second->cseKey == "cd_eps" || it.second->cseKey == "cd_1")) {
        if (!guaranteed) guaranteed = it.second;
      }
    }

    if (verbose) {
      cerr << "addDefaultUpdates: guaranteed=" + repr(guaranteed) + " prohibited=" + repr(aset.prohibitedByAssignmentTo) + "\n";
      aset.dump(cerr, "  ", 3);
    };

    if (t->asPrimitive()) {
      if (!guaranteed && !aset.prohibitedByAssignmentTo) {
        if (!aset.dst) {  
          aset.dst = outArg;
          aset.mode = AUGMODE_NONE;
        }
        if (inArg) {
          aset.addSrc(inArg, ctx.mkExpr<ExprConstDouble>(numeric_limits<double>::epsilon()));
        }
        else {
          aset.addSrc(ctx.mkExpr<ExprConstZero>(outArg->type), ctx.mkExpr<ExprConstDouble>(numeric_limits<double>::epsilon()));
        }
        if (verbose) {
          cerr << "Added default. Now we have:\n";
          aset.dump(cerr, "  ", 3);
        }
      }
    }

    else if (t->asStruct()) {
      for (auto &mem : t->asStruct()->members) {
        auto subOutArg = ctx.mkExpr<ExprStructref>(outArg, mem->memberName, mem);
        auto subInArg = inArg ? ctx.mkExpr<ExprStructref>(inArg, mem->memberName, mem) : nullptr;
        if (!addDefaultValues(ctx, subOutArg, subInArg)) return false;
      }
    }
    return true;
  }

  [[nodiscard]] bool checkMissingOutputs(YogaContext const &ctx, ExprNode *outArg)
  {
    const int verbose = 0;
    auto t = outArg->type;

    auto &aset = effects->assignments[outArg->cseKey];

    ExprNode *guaranteed = nullptr;
    ExprNode *possible = nullptr;
    for (auto &it : aset.srcs) {
      if (it.second && (it.second->cseKey == "cd_eps" || it.second->cseKey == "cd_1")) {
        if (!guaranteed) guaranteed = it.second;
      }
      if (!possible) possible = it.second;
    }

    if (verbose) {
      cerr << "checkMissingOutputs: guaranteed=" + repr(guaranteed) + " prohibited=" + repr(aset.prohibitedByAssignmentTo) + "\n";
      aset.dump(cerr, "  ", 3);
    };

    if (t->asPrimitive()) {
      if (!possible && !aset.prohibitedByAssignmentTo && !checkProhibitions(ctx, outArg)) {
        string refName;
        if (outArg->getRefName(refName)) {
          ctx(outArg).logWarning("No possible assignment to "+ refName);
        }
        else {
          ctx(outArg).logWarning("No possible assignment to something");
        }
        if (verbose) {
          cerr << "Assignment set:\n";
          aset.dump(cerr, "  ", 3);
        }
      }
    }

    else if (t->asStruct()) {
      for (auto &mem : t->asStruct()->members) {
        auto subOutArg = ctx.mkExpr<ExprStructref>(outArg, mem->memberName, mem);
        if (!checkMissingOutputs(ctx, subOutArg)) return false;
      }
    }
    return true;
  }



  [[nodiscard]] bool endTopLevel(YogaContext const &ctx, AstFunction *node) override
  {
    if (0) cerr << "endTopLevel(AstFunction " + shellEscape(node->funcName) + "): pop bindings\n";

    effects = nullptr;
    popBindings();
    return true;
  }

  // IF
  [[nodiscard]] bool preScan(YogaContext const &ctx, AstIf *node) override
  {
    return true;
  }

  [[nodiscard]] bool postCondScan(YogaContext const &ctx, AstIf *node) override
  {
    auto condRv = node->cond->rv;
    if (!condRv) {
      return ctx(node->cond).logError("no rv for condition");
    }

    if (condRv->type == ctx.reg->boolType) {
      condRv = ctx.mkExpr<ExprConvBoolDouble>(condRv);
    }

    if (condRv->type == ctx.reg->rType) {
      modulationStack.emplace_back(BRANCH_TRUE, condRv);
      return true;
    }

    return ctx.logError("Can't convert condition to R"s);
  }

  [[nodiscard]] bool postIfTrueScan(YogaContext const &ctx, AstIf *node) override
  {
    assert(modulationStack.back().first == BRANCH_TRUE);
    modulationStack.back().first = BRANCH_FALSE;
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstIf *node) override
  {
    assert(modulationStack.back().first == BRANCH_FALSE);
    modulationStack.pop_back();
    return true;
  }

  // DEFAULT
  [[nodiscard]] bool preScan(YogaContext const &ctx, AstDefault *node) override
  {
    modulationStack.emplace_back(BRANCH_DEFAULT, ctx.mkExpr<ExprConstDouble>(numeric_limits<double>::epsilon()));
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstDefault *node) override
  {
    assert(modulationStack.back().first == BRANCH_DEFAULT);
    modulationStack.pop_back();
    return true;
  }

  // BLOCK
  [[nodiscard]] bool preScan(YogaContext const &ctx, AstBlock *node) override
  {
    pushBindings();
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstBlock *node) override
  {
    popBindings();
    node->rv = ctx.mkExpr<ExprConstVoid>();
    return true;
  }

  // TERNOP
  [[nodiscard]] bool postScan(YogaContext const &ctx, AstTernop *node) override
  {
    if (node->op == "?:") {
      auto lhsRv = node->lhs->rv;
      auto mhsRv = node->mhs->rv;
      auto rhsRv = node->rhs->rv;

      if (lhsRv->type == ctx.reg->boolType) {
        lhsRv = ctx.mkExpr<ExprConvBoolDouble>(lhsRv);
      }

      auto notLhsRv = ctx.mkExpr<ExprNot>(lhsRv);

      node->rv = ctx.mkExpr<ExprLinearComb>(vector<ExprNode *> {
        lhsRv,
        mhsRv,
        notLhsRv,
        rhsRv
      });

      return true;
    }
    else {
      return ctx.logError("Unknown ternary operator "s + node->op);
    }
  }

  ExprNode *doAssign(YogaContext const &ctx, string const &op, AstNode *lhs, AstNode *rhs)
  {
    auto rhsRv = rhs->rv;
    if (!rhsRv) {
      if (rhs->lv && rhs->lv->asUnboundVar()) {
        return ctx(rhs->sourceLoc).logErrorNull("Unbound variable " + shellEscape(rhs->lv->asUnboundVar()->name));
      }
      else {
        return ctx(rhs->sourceLoc).logErrorNull("No rvalue for rhs of "s + op);
      }
    }

    auto lhsLv = lhs->lv;
    if (!lhsLv) {
      return ctx(rhs->sourceLoc).logErrorNull("No lvalue for lhs of "s + op);
    }

    auto mode = AUGMODE_NONE;
    if (op == "+=") {
      if (auto lhsLvVar = lhsLv->asVar()) {
        if (lhsLvVar->dir == YD_ACCUM) {
          mode = AUGMODE_PLUSEQ_INPLACE;
        }
        else {
          mode = AUGMODE_PLUSEQ;
        }
      }
      else {
        mode = AUGMODE_PLUSEQ;
      }
    }
    else if (op == "=") {
      mode = AUGMODE_NONE;
    }
    else {
      throw runtime_error("Unhandled augmentation mode op "s + shellEscape(op));
    }

    return doAssign(ctx, mode, lhsLv, rhsRv);
  }

  string debugLocalKey(YogaSourceLoc const &loc, ExprNode *rv, ExprNode *modulation)
  {
    assert(effects);
    auto [line, col] = loc.calcLineNumber();
    auto key = loc.file->fn + ":" + to_string(line) + ":" + rv->cseKey + ":" + modulation->cseKey;
    return key;
  }

  bool debugLocalPossible(ExprNode *rv)
  {
    if (rv->type->isObject()) return false;
    return true;
  }

  ExprNode *doAssign(YogaContext const &ctx, AugMode mode, ExprNode *lhsLv, ExprNode *rhsRv)
  {
    if (mode == AUGMODE_NONE && lhsLv && lhsLv->asUnboundVar()) { // binding to name
      auto lhsName = lhsLv->asUnboundVar()->name;
      if (lhsName == "."s) { // 
        return ctx(lhsLv).logErrorNull("Assignment to ."s);
      }
      else if (lhsName == "_"s) { // assignment to _ is ignored
        return rhsRv;
      }
      else {
        if (!bindName(ctx(lhsLv), lhsName, nullptr, rhsRv)) return nullptr;
        if (effects) {
          auto modulation = ctx.mkExpr<ExprConstDouble>(1.0);
          auto &debugLocalDone = effects->debugLocalDones[debugLocalKey(lhsLv->sourceLoc, rhsRv, modulation)];
          if (!debugLocalDone && debugLocalPossible(rhsRv)) {
            debugLocalDone = true;

            auto localMemberName = lhsName;
            int serial = 1;
            while (effects->debugLocalMemberLocs.find(localMemberName) != effects->debugLocalMemberLocs.end()) {
              localMemberName = lhsName + "." + to_string(serial++);
            }
            effects->debugLocalsType->addMember(ctx, localMemberName, rhsRv->type);
            effects->debugLocalMemberLocs[localMemberName] = lhsLv->sourceLoc;
            effects->debugLocalItems.emplace_back(localMemberName, rhsRv, modulation);
          }
        }
        return rhsRv;
      }
    }
    else if (lhsLv) {
      auto modulation = calcModulation(ctx);

      if (rhsRv->type == ctx.reg->rType && lhsLv->type == ctx.reg->boolType) {
        rhsRv = ctx.mkExpr<ExprConvDoubleBool>(rhsRv);
      }
      else if (rhsRv->type == ctx.reg->boolType && lhsLv->type == ctx.reg->rType) {
        rhsRv = ctx.mkExpr<ExprConvBoolDouble>(rhsRv);
      }

      if (rhsRv->type == lhsLv->type) {
        // good
      }
      else if (mode == AUGMODE_PLUSEQ && lhsLv->type->isSet()) {
        // good
      }
      else {
        return ctx.logErrorNull("Type mismatch in assignment: expected "s + repr_type(lhsLv->type) + 
        " got "s + repr_type(rhsRv->type));
      }

      if (!effects) {
        return ctx(lhsLv).logErrorNull("Assignment outside function");
      }

      auto &assignmentSet = effects->assignments[lhsLv->cseKey];
      if (assignmentSet.prohibitedByAssignmentTo) {
        ctx.logError("Assignment prohibited by previous assignment to member");
        ctx(assignmentSet.prohibitedByAssignmentTo).logNote("Previous assignment is here");
        return nullptr;
      }
      if (assignmentSet.dst == nullptr) {
        assignmentSet.dst = lhsLv;
        assignmentSet.mode = mode;

        // Fail if any previous assignments to containing nodes.
        auto prohibitAset = checkProhibitions(ctx, lhsLv);
        if (prohibitAset) {
          ctx.logError("Assignment prohibited by previous assignment to container");
          ctx(prohibitAset->srcs[0].first).logNote("Previous assignment is here");
          return nullptr;
        }
        // Prohibit any future assignments to containing nodes.
        addParentProhibitions(ctx, lhsLv, lhsLv);        
      }
      else if (assignmentSet.mode == mode) {
        // ok
      }
      else {
        return ctx.logErrorNull("Assignment mode mismatch. Previously used "s + 
          augModeToOp(assignmentSet.mode) + " and now using "s + augModeToOp(mode));
      }
      assignmentSet.srcs.emplace_back(rhsRv, modulation);
      if (0) cerr << "debugLocalItem: "s + lhsLv->sourceLoc.gloss() + "\n";
      auto &debugLocalDone = effects->debugLocalDones[debugLocalKey(lhsLv->sourceLoc, rhsRv, modulation)];
      if (!debugLocalDone) {
        debugLocalDone = true;

        auto localMemberName = lhsLv->debugLocalName;
        int serial = 1;
        while (effects->debugLocalMemberLocs.find(localMemberName) != effects->debugLocalMemberLocs.end()) {
          localMemberName = lhsLv->debugLocalName + "." + to_string(serial++);
        }
        effects->debugLocalsType->addMember(ctx, localMemberName, rhsRv->type);
        effects->debugLocalMemberLocs[localMemberName] = lhsLv->sourceLoc;
        effects->debugLocalItems.emplace_back(localMemberName, rhsRv, modulation);
      }
      return rhsRv;
    }

    return ctx.logErrorNull("WRITEME: assign("s + augModeToOp(mode) + ")"s);
  }

  ExprNode *getRvOrError(YogaContext const &ctx, AstNode *n)
  {
    auto rv = n->rv;
    if (!rv) {
      if (n->lv && n->lv->asUnboundVar()) {
        return ctx(n).logErrorNull("Unbound variable " + shellEscape(n->lv->asUnboundVar()->name));
      }
      else {
        return ctx(n).logErrorNull("No rvalue"s);
      }
    }
    return rv;
  }

  [[nodiscard]] bool callYogaFunction(YogaContext const &ctx, AstFunction *target, AstCall *call)
  {
    if (target->args.size() != call->args.size()) {
      return ctx.logError("Argument mismatch: expected "s + to_string(target->args.size()) + " got "s + to_string(call->args.size()));
    }

    ctx.logCall("Call "s + target->funcName);

    vector<ExprNode *> txRvs;
    vector<ExprNode *> rxLvs;
    vector<YogaType *> rxTypes;
    bool sawDot = false;
    YogaType *returnType = ctx.reg->voidType; // may get changed if there's a dot in the call

    for (auto argi = 0; argi < call->args.size(); argi++) {
      auto &formal = target->args[argi];
      auto &actual = call->args[argi];

      ctx.logCall("  arg " + repr_arg(argi) + 
          "\n    formal:" + repr(formal) + 
          "\n    actual:" + repr(actual));

      auto formalType = ctx.lookup(formal->type->name).type;
      if (formal->dirOutOnly) {
        auto actualLv = actual->lv;
        if (!actualLv) return ctx(actual).logError("No lv for arg "s + repr_arg(argi));

        if (!formalType) {
          return ctx.logError("No type " + shellEscape(formal->type->name));
        }

        if (actualLv->asUnboundVar()) {
          // ok, newly introduced var which we'll assign to
          ctx.logCall("  Unbound actual " + shellEscape(actualLv->asUnboundVar()->name));
          if (actualLv->asUnboundVar()->name == ".") {
            if (sawDot) return ctx.logError("Multiple dots not allowed"s);
            ctx.logCall("  Setting return type to " + repr_type(formalType));
            returnType = formalType;
            actualLv->type = formalType;
            sawDot = true;
          }
          else {
            // Warning: mutating an ExprNode here, which we rarely do
            actualLv->type = formalType;
          }
        }
        if (actualLv->type != formalType) {
          return ctx.logError("Type mismatch for arg " + repr_arg(argi) + 
            ": expected "s + repr_type(formalType) +
            " got "s +  repr_type(actualLv->type));
        }
        rxLvs.push_back(actualLv);
        rxTypes.push_back(actualLv->type);
      }
      else if (formal->dirUpOnly) {
        auto actualLv = actual->lv;
        if (!actualLv) return ctx(actual).logError("No lv for arg "s + repr_arg(argi));

        if (actualLv->type != formalType) {
          return ctx(actual).logError("Type mismatch for arg "s + repr_arg(argi) +
            ": expected " + repr_type(formalType) + 
            " got " + repr_type(actualLv->type));
        }

        rxLvs.push_back(actualLv);
        rxTypes.push_back(actualLv->type);

        auto actualRv = actual->rv;
        if (!actualRv) return ctx(actual).logError("No rv for arg "s + repr_arg(argi));
        txRvs.push_back(actualRv);
      }
      else if (formal->dirInOnly) {
        auto actualRv = actual->rv;
        if (!actualRv) return ctx(actual).logError("No rv for arg "s + repr_arg(argi));
        if (actualRv->type != formalType) {
          return ctx(actual).logError("Type mismatch for arg "s + repr_arg(argi) +
            ": expected " + repr_type(formalType) + 
            " got " + repr_type(actualRv->type));
        }
        txRvs.push_back(actualRv);
      }
      else {
        throw runtime_error("Unknown dir");
      }
    }

    auto callDebugLocalsType = ctx.reg->getStruct(ctx, target->funcName + ".d", false);
    auto debugLocalMemberName = target->funcName;
    int serial = 1;
    while (effects->debugLocalMemberLocs.find(debugLocalMemberName) != effects->debugLocalMemberLocs.end()) {
      debugLocalMemberName = target->funcName + "." + to_string(serial++);
    }
    effects->debugLocalsType->addMember(ctx, debugLocalMemberName, callDebugLocalsType);
    if (0) effects->debugLocalMemberLocs[debugLocalMemberName] = ctx.lastLoc;
    effects->debugCallMap[debugLocalMemberName] = target->funcName;

    auto callv = static_cast<ExprCallYoga *>(ctx.mkExpr<ExprCallYoga>(target->funcName, txRvs, rxTypes, returnType, debugLocalMemberName));

    int outi = 0;
    for (auto &it : rxLvs) {
      if (it->asUnboundVar() && it->asUnboundVar()->name == ".") {
        call->rv = callv->outVals[outi];
      }
      else {
        doAssign(ctx, AUGMODE_NONE, it, callv->outVals[outi]);
      }
      outi++;
    }

    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstBinop *node) override
  {
    if (node->isAssign()) {
      auto outRv = doAssign(ctx, node->op, node->lhs, node->rhs);
      if (!outRv) return false;
      node->rv = outRv;
      return true;
    }
    else {
      auto lhsRv = getRvOrError(ctx, node->lhs);
      if (!lhsRv) return false;
      auto rhsRv = getRvOrError(ctx, node->rhs);
      if (!rhsRv) return false;

      if (node->op == "+"s) {
        auto nodeRv = ctx.mkExpr<ExprAdd>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "-"s) {
        auto nodeRv = ctx.mkExpr<ExprSub>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "*"s) {
        auto nodeRv = ctx.mkExpr<ExprMul>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "/"s) {
        auto nodeRv = ctx.mkExpr<ExprDiv>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "^"s) {
        auto nodeRv = ctx.mkExpr<ExprPow>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "||"s) {
        auto nodeRv = ctx.mkExpr<ExprLogicalOr>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (node->op == "&&"s) {
        auto nodeRv = ctx.mkExpr<ExprLogicalAnd>(lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      if (ExprCompare::knownOp(node->op)) {
        auto nodeRv = ctx.mkExpr<ExprCompare>(node->op, lhsRv, rhsRv);
        if (nodeRv) {
          node->rv = nodeRv;
          return true;
        }
      }
      return ctx.logError("No binop "s + repr_type(lhsRv->type) + " "s + node->op + " "s + repr_type(rhsRv->type));
    }
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstUnop *node) override
  {
    auto argRv = node->arg->rv;
    if (!argRv) {
      return ctx.logError("No rvalue for arg of "s + node->op);
    }
    if (node->op == "+"s) {
      node->rv = argRv;
      return true;
    }
    if (node->op == "-"s) {
      node->rv = ctx.mkExpr<ExprNeg>(argRv);
      return true;
    }
    if (node->op == "!"s) {
      node->rv = ctx.mkExpr<ExprNot>(argRv);
      return true;
    }
    return ctx.logError("No unop "s + node->op + " "s + repr_type(argRv->type));
  }



  [[nodiscard]] bool postScan(YogaContext const &ctx, AstArray *node) override
  {
    vector<ExprNode *> argRvs;
    vector<ExprNode *> argLvs;
    AstNode *missingRv = nullptr;
    AstNode *missingLv = nullptr;
    for (auto &it : node->elems) {
      if (!it->lv && !missingLv) missingLv = it;
      if (!it->rv && !missingRv) missingRv = it;
      argRvs.push_back(it->rv);
      argLvs.push_back(it->lv);
    }
    if (missingLv && missingRv) {
      return ctx(missingRv).logError("Missing value"s);
    }

    if (!missingRv) {
      node->rv = ctx.mkExpr<ExprMkList>(argRvs);
    }
    if (!missingLv) {
      node->lv = ctx.mkExpr<ExprMkList>(argLvs);
    }
    return true;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstCall *node) override
  {
    auto fn = node->literalFuncName;
    vector<ExprNode *> argRvs;
    bool allArgRvsExist = true;
    AstNode *missingRv = nullptr;
    for (auto &it : node->args) {
      if (!it->rv) {
        allArgRvsExist = false;
        if (!missingRv) missingRv = it;
      }
      argRvs.push_back(it->rv);
    }

    if (allArgRvsExist && ExprMinMax::knownOp(fn)) {
      auto nodeRv = ctx.mkExpr<ExprMinMax>(fn, argRvs);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprSum::knownOp(fn) && argRvs.size() == 1) {
      auto nodeRv = ctx.mkExpr<ExprSum>(fn, argRvs[0]);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprClamp::knownOp(fn)) {
      auto nodeRv = ctx.mkExpr<ExprClamp>(fn, argRvs);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprTrig1::knownOp(fn) && argRvs.size() == 1) {
      auto nodeRv = ctx.mkExpr<ExprTrig1>(fn, argRvs[0]);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprTrig2::knownOp(fn) && argRvs.size() == 2) {
      auto nodeRv = ctx.mkExpr<ExprTrig2>(fn, argRvs);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprDot::knownOp(fn) && argRvs.size() == 2) {
      auto nodeRv = ctx.mkExpr<ExprDot>(argRvs[0], argRvs[1]);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprMatrixConstructor::knownOp(fn)) {
      auto nodeRv = ctx.mkExpr<ExprMatrixConstructor>(fn, argRvs);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprMatrixMath::knownOp(fn)) {
      auto nodeRv = ctx.mkExpr<ExprMatrixMath>(fn, argRvs);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprNograd::knownOp(fn) && argRvs.size() == 1) {
      auto nodeRv = ctx.mkExpr<ExprNograd>(argRvs[0]);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    if (allArgRvsExist && ExprConjugate::knownOp(fn) && argRvs.size() == 1) {
      auto nodeRv = ctx.mkExpr<ExprConjugate>(argRvs[0]);
      if (nodeRv) {
        node->rv = nodeRv;
        return true;
      }
    }
    auto fnt = ctx.getType(fn);
    if (fnt) {
      if (allArgRvsExist) {
        if (fnt->isComplex() && argRvs.size() == 2) {
          auto nodeRv = ctx.mkExpr<ExprMkComplex>(argRvs[0], argRvs[1]);
          if (!nodeRv) return false;
          node->rv = nodeRv;
          return true;
        }
        else {
          auto nodeRv = ctx.mkExpr<ExprConstructor>(fn, argRvs);
          if (!nodeRv) return false;
          node->rv = nodeRv;
          return true;
        }
      }
    }
    if (auto target = ctx.lookup(fn).astFunction) {
      return callYogaFunction(ctx, target, node);
    }

    string typeSig;
    for (auto &it : argRvs) {
      if (typeSig.size()) {
        typeSig += ", ";
      }
      if (it) {
        typeSig += it->type->clsName;
      }
      else {
        typeSig += "?";
      }
    }

    if (missingRv) {
      getRvOrError(ctx, missingRv);
    }
    else {
      ctx.logError("No function "s + fn + "("s + typeSig + ")\n"s);
    }
    return false;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstIndex *node) override
  {
    if (!node->lhs->lv && !node->lhs->rv) {
      return ctx.logError("No lvalue or rvalue for indexee"s);
    }

    vector<ExprNode *> argRvs;
    vector<ExprNode *> argLvs;
    bool allArgRvsExist = true;
    argRvs.push_back(node->lhs->rv);
    argLvs.push_back(node->lhs->lv);
    for (auto &it : node->args) {
      if (!it->rv) allArgRvsExist = false;
      argRvs.push_back(it->rv);
      argLvs.push_back(it->rv); // yes, rv for indexes
    }
    if (allArgRvsExist) {
      if (node->lhs->rv && node->lhs->rv->type->asMatrix()) {
        node->rv = ctx.mkExpr<ExprMatrixIndex>(argRvs);
        if (!node->rv) {
          return ctx.logError("Can't create ExprMatrixIndex as rv"s);
        }
      }
      if (node->lhs->lv && node->lhs->lv->type->asMatrix()) {
        node->lv = ctx.mkExpr<ExprMatrixIndex>(argLvs);
        if (!node->lv) {
          return ctx.logError("Can't create ExprMatrixIndex as lv"s);
        }
      }
      return true;
    }

    ctx.logError("Unkown index type"s);
    ctx.logAst("AST", node);
    return false;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstDeriv *node) override
  {
    if (!node->arg->rv) return ctx(node->arg).logError("No rvalue for arg"s);
    if (!node->wrt->rv) return ctx(node->wrt).logError("No rvalue for wrt"s);

    auto derivRv = node->arg->rv->deriv(ctx("deriv"), node->wrt->rv);
    if (!derivRv) {
      return ctx(node->arg).logError("No derivative"s);
    }

    node->rv = derivRv;

    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstName *node) override
  {
    auto bound = bindingStack.back()[node->name];
    if (ctx.reg->verbose >= 99) {
      cerr << "postScan(AstName " + shellEscape(node->name) + "): bindings: lv=\n";
      if (bound.lv) {
        cerr << *bound.lv;
      } else {
        cerr << "  null\n";
      }
      cerr << "rv=\n";
      if (bound.rv) {
        cerr << *bound.rv;
      } else {
        cerr << "  null\n";
      }
    }
    if (bound.rv || bound.lv) {
      if (bound.rv) {
        if (auto v = bound.rv->asVar()) {
          node->rv = ctx(node).mkExpr<ExprVar>(v->type, v->funcName, v->name, v->dir);
        }
        else {
          node->rv = bound.rv;
        }
      }
      if (bound.lv) {
        if (auto v = bound.lv->asVar()) {
          node->lv = ctx(node).mkExpr<ExprVar>(v->type, v->funcName, v->name, v->dir);
        }
        else {
          node->lv = bound.lv;
        }
      }
    }
    else {
      node->lv = ctx(node).mkExpr<ExprUnboundVar>(node->name);
    }
    
    return true;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstParam *node) override
  {
    if (node->lhs) {
      node->rv = node->lhs->rv;
    }
    return true;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstStructref *node) override
  {
    if (node->lhs->rv) {
      auto lhsType = node->lhs->rv->type;
      if (auto lhsTypeStruct = lhsType->asStruct()) {
        auto memberInfo = lhsTypeStruct->getMember(node->literalMemberName);
        if (!memberInfo && lhsTypeStruct->autoCreate && node->expectedType) {
          lhsTypeStruct->addMember(ctx, node->literalMemberName, node->expectedType);
        }
        if (!memberInfo) {
          return ctx.logError("No member "s + node->literalMemberName);
        }
        node->rv = ctx.mkExpr<ExprStructref>(node->lhs->rv, node->literalMemberName, memberInfo);
      }
      else if (lhsType->isObject()) {
        node->rv = ctx.mkExpr<ExprStructref>(node->lhs->rv, node->literalMemberName, nullptr);
      }
      else if (lhsType->isComplex()) {
        if (node->literalMemberName == "r") {
          node->rv = ctx.mkExpr<ExprExtractReal>(node->lhs->rv);
        }
        else if (node->literalMemberName == "i") {
          node->rv = ctx.mkExpr<ExprExtractImag>(node->lhs->rv);
        }
        else {
          return ctx.logError("Use .r or .i to extract real or imag from a complex");
        }
      }
      else {
        return ctx.logError("Not a struct (type is " + repr_type(lhsType) + ")"s);
      }
    }

    if (node->lhs->lv) {
      auto lhsType = node->lhs->lv->type;
      if (auto lhsTypeStruct = lhsType->asStruct()) {
        auto memberInfo = lhsTypeStruct->getMember(node->literalMemberName);
        if (!memberInfo && lhsTypeStruct->autoCreate && node->expectedType) {
          lhsTypeStruct->addMember(ctx, node->literalMemberName, node->expectedType);
        }
        if (!memberInfo) {
          return ctx.logError("No member "s + node->literalMemberName);
        }
        node->lv = ctx.mkExpr<ExprStructref>(node->lhs->lv, node->literalMemberName, memberInfo);
      }
      else if (lhsType->isComplex()) {
        // this is tricky, ignore foo.r and foo.l as lvalue for now
      }
      else if (node->lhs->lv->asUnboundVar()) {
        return ctx(node->lhs->sourceLoc).logError("Unbound variable " + shellEscape(node->lhs->lv->asUnboundVar()->name));
      }
      else {
        return ctx.logError("Not a struct (type is " + repr_type(lhsType) + ")"s);
      }
    }
    return true;
  }



  [[nodiscard]] bool postScan(YogaContext const &ctx, AstString *node) override
  {
    node->rv = ctx.mkExpr<ExprConstString>(node->literalValue);
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstNumber *node) override
  {
    auto paramInfo = node->paramInfo;
    if (paramInfo) {
      node->rv = ctx.mkExpr<ExprParam>(paramInfo);
    }
    else {
      if (node->isImaginary) {
        double v{0.0};
        if (!node->getValue(v)) {
          return ctx(node).logError("Not a number");
        }
        node->rv = ctx.mkExpr<ExprConstComplex>(std::complex(0.0, v));
      }
      else {
        double v{0.0};
        if (!node->getValue(v)) {
          return ctx(node).logError("Not a number");
        }
        node->rv = ctx.mkExpr<ExprConstDouble>(v);
      }
    }
    if (0) {
      cerr << "postNumber: rv=\n" << *node->rv;
    }
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstBoolean *node) override
  {
    if (node->literalValue) {
      node->rv = ctx.mkExpr<ExprConstOne>(ctx.reg->boolType);
    }
    else {
      node->rv = ctx.mkExpr<ExprConstZero>(ctx.reg->boolType);
    }
    return true;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstNull *node) override
  {
    // WRITEME
    return false;
  }


  [[nodiscard]] bool postScan(YogaContext const &ctx, AstObject *node) override
  {
    vector<string> keys;
    bool hasAllRvs = true, hasAllLvs = true;
    vector<ExprNode *> rvs;
    vector<ExprNode *> lvs;
    for (auto &it : node->keyValues) {
      if (it->literalKey.empty()) return ctx(it).logError("Key not literal"s);
      keys.push_back(it->literalKey);
      if (!it->value->rv) hasAllRvs = false;
      rvs.push_back(it->value->rv);
      if (!it->value->lv) hasAllLvs = false;
      lvs.push_back(it->value->lv);
    }
    ExprNode *defaultRv{nullptr};
    if (node->defaultValue && node->defaultValue->rv) {
      defaultRv = node->defaultValue->rv;
    }
    if (hasAllRvs) {
      node->rv = ctx.mkExpr<ExprObject>(keys, rvs, defaultRv);
    }
    if (hasAllLvs) {
      node->lv = ctx.mkExpr<ExprObject>(keys, lvs, nullptr);
    }
    return true;
  }

  [[nodiscard]] bool postScan(YogaContext const &ctx, AstExtern *node) override
  {
    auto paramInfo = node->paramInfo;
    if (paramInfo) {
      node->rv = ctx.mkExpr<ExprParam>(paramInfo);
    }
    return true;
  }

};
