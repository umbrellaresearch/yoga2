#include "./ast.h"
#include "./compilation.h"
#include "./context.h"
#include "./expr.h"
#include "./param.h"
#include "./effects.h"
#include "./expr_impl.h"

ostream & operator <<(ostream &s, ExprNode const &a)
{
  a.dump(s, "    "s, 5);
  return s;
}

ostream & operator <<(ostream &s, vector<ExprNode *> const &a)
{
  for (auto &it : a) {
    if (it) {
      it->dump(s, "    "s, 3);
    } else {
      s << "  node null\n";
    }
  }
  return s;
}

string repr_ref(ExprNode const *a)
{
  if (!a) return "null";
  string refName;
  if (a->getRefName(refName)) {
    return refName;
  }
  return a->cseKey;
}

void ExprNode::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " " << op << "<" << repr_type(type) << "> at " << sourceLoc << "\n";
  if (depth <= 0) {
    s << indent << "  ...\n";
  }
  else {
    for (auto it : args) {
      if (it) {
        it->dump(s, indent + "  "s, depth-1);
      }
      else {
        s << indent + "  null\n";
      }
    }
  }
}


void ExprConstDouble::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> " << literalValue << " at " << sourceLoc << "\n";
}

void ExprConstComplex::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> " << literalValue << " at " << sourceLoc << "\n";
}

void ExprConstZero::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> 0 at " << sourceLoc << "\n";
}

void ExprConstOne::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> 1 at " << sourceLoc << "\n";
}

void ExprConstString::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> " << quoteStringJs(literalValue) << " at " << sourceLoc << "\n";
}

void ExprConstVoid::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " const<" << repr_type(type) << "> at " << sourceLoc << "\n";
}

void ExprParam::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " param<" << repr_type(type) << "> " << 
    " name=" << paramInfo->paramName << 
    " index=" << paramInfo->paramIndex << 
    " at " << sourceLoc << "\n";
}

void ExprParamGrad::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " paramGrad<" << repr_type(type) << "> " << 
    " name=" << paramInfo->paramName << 
    " index=" << paramInfo->paramIndex << 
    " at " << sourceLoc << "\n";
}

void ExprVar::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " var<" << repr_type(type) << "> " << dir << " " << name << " at " << sourceLoc << "\n";
}

void ExprReturnVal::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " return<" << repr_type(type) << "> at " << sourceLoc << "\n";
}

void ExprReturnGrad::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " returnGrad<" << repr_type(type) << "> at " << sourceLoc << "\n";
}

void ExprUnboundVar::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " unboundvar<" << name << " at " << sourceLoc << "\n";
}


void AssignmentSet::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << "assign" << augModeToOp(mode) << "\n";
  if (depth <= 0) {
    s << indent << "  ...\n";
  }
  else if (prohibitedByAssignmentTo) {
    if (dst) {
      s << indent << "  dst:\n";
      dst->dump(s, indent + "    "s, depth - 1);
    }
    s << indent << "  prohibited by assignment to:\n";
    prohibitedByAssignmentTo->dump(s, indent + "    "s, depth - 1);
  }
  else if (dst) {
    s << indent << "  dst:\n";
    dst->dump(s, indent + "    "s, depth - 1);
    s << indent << "  srcs:\n";
    for (auto &[value, modulation] : srcs) {
      s << indent << "    value:\n";
      value->dump(s, indent + "      "s, depth - 1);
      s << indent << "    modulation:\n";
      modulation->dump(s, indent + "      "s, depth - 1);
    }
    if (0 && srcPhi) {
      s << indent << "  srcPhi:\n";
      srcPhi->dump(s, indent + "      "s, depth - 1);
    }
    s << "\n";
  }
}

void ExprPhi::dump(ostream &s, string const &indent, int depth) const
{
  s << indent << cseKey << " phi<" << repr_type(type) << "> at " << sourceLoc << "\n";
  aset.dump(s, indent + "  "s, depth-1);
}
