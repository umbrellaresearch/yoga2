#pragma once
#include "./compilation.h"
#include "./ast.h"
#include "./expr.h"
#include "./annotation.h"

template<typename T, typename ...Args>
T * YogaContext::mkAnnotation(Args&& ...args) const
{
  unique_ptr ptr = make_unique<T>(std::forward<Args>(args)...);
  T *ret = ptr.get();
  reg->annotationPool.emplace_back(std::move(ptr));

  ret->sourceLoc = lastLoc;
  if (ret->sourceLoc.file) {
    reg->annosByFile[ret->sourceLoc.file].push_back(ret);
  }
  return ret;
}
