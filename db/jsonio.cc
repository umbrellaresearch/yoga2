#include "./policy.h"
#include "nlohmann-json/json.hpp"
using json = nlohmann::json;

/*
  nlohmann-json provides a mechanism for creating templates
  to_json(json &j, T value) and from_json(json const &j, T &value)
  and having it automatically do the conversion. But in many cases
  I need access to the YogaContext, so it's easier to just write
  my own methods and call them.
*/

json YogaRef::toJson() const
{
  return json({
    {"fullName", fullName}
  });
}

YogaRef YogaRef::fromJson(YogaContext const &ctx,  json const &j)
{
  if (j.contains("fullName")) {
    return ctx.getRef(j.value("fullName", ""));
  }
  return ctx.getRef("");
}


json PolicyParamVariation::toJson() const
{
  return json({
    {"paramName", paramName},
    {"paramRange", paramRange},
  });
};

PolicyParamVariation PolicyParamVariation::fromJson(YogaContext const &ctx, json const &j)
{
  return PolicyParamVariation(
    j.value("paramName", ""s),
    j.value("paramRange", 0.1)
  );
}



PolicyTerrainSpec PolicyTerrainSpec::fromJson(YogaContext const &ctx, json const &j)
{
  if (j.contains("xVar") && j.contains("yVar") && j.contains("rMetric")) {
    return PolicyTerrainSpec(
      PolicyParamVariation::fromJson(ctx, j.value("xVar", json::object())),
      PolicyParamVariation::fromJson(ctx, j.value("yVar", json::object())),
      PolicyRewardMetric::fromJson(ctx, j.value("rMetric", json::object())),
      j.value("resolution", 9));
  }
  return PolicyTerrainSpec();
}

json PolicyTerrainSpec::toJson() const
{
  return json({
    {"xVar", xVar.toJson()},
    {"yVar", yVar.toJson()},
    {"rMetric", rMetric.toJson()},
    {"resolution", resolution}
  });
}


PolicyRewardMetric PolicyRewardMetric::fromJson(YogaContext const &ctx, json const &j)
{
  return PolicyRewardMetric(
    YogaRef::fromJson(ctx, j.value("ref", json::object())),
    j.value("sampleTime", 1.0),
    j.value("rScale", 1.0));
}

json PolicyRewardMetric::toJson() const
{
  return json({
    {"ref", ref.toJson()},
    {"sampleTime", sampleTime},
    {"rScale", rScale}
  });
}



json PolicySquiggleSpec::toJson() const
{
  return json({
    {"rMetric", rMetric.toJson()},
    {"paramRange", paramRange},
    {"resolution", resolution}
  });
}

PolicySquiggleSpec PolicySquiggleSpec::fromJson(YogaContext const &ctx, json const &j)
{
  return PolicySquiggleSpec(
    PolicyRewardMetric::fromJson(ctx, j.value("rMetric", json::object())),
    j.value("resolution", 33));
}



json PolicyParamSearchSpec::toJson() const
{
  auto psp = json::object();
  for (auto &it : participatingParams) {
    psp[it->paramName] = true;
  }
  return json::object({
    {"participatingParams", psp}
  });
}

PolicyParamSearchSpec PolicyParamSearchSpec::fromJson(YogaContext const &ctx, json const &j)
{
  PolicyParamSearchSpec ret;

  if (j.contains("participatingParams")) {
    for (auto &it : j["participatingParams"].items()) {
      auto param = ctx.reg->paramsByName[it.key()];
      if (param) {
        ret.participatingParams.insert(param);
      }
    }
  }
  return ret;
}
