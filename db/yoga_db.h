#pragma once
/*
  Very basic wrapper on top of an sqlite3 database.

  Use the default instance (returned by `YogaDb::instance()`) unless you need a
  special db for test harnesses or something.
*/

#include "common/std_headers.h"
#include "common/packetbuf.h"
#include "nlohmann-json/json_fwd.hpp"
using json = nlohmann::json;

struct sqlite3;
struct sqlite3_stmt;
struct Trace;
struct ScopeModel;
struct YogaContext;
struct UvMainThreadMutex;
struct YogaCompilation;
struct PacketNetworkEngine;
struct YogaRpcPool;

struct YogaDbTraceInfoRow {
  string name;
  string series;
  string date;
  string script;
  string sortKey;
  string flags;

  YogaDbTraceInfoRow();
  YogaDbTraceInfoRow(string const &_name, string const &_flags = ""s);
  bool parseTraceName(); // convert name to series, data, script, sortKey, flags.
};

ostream & operator <<(ostream &s, const YogaDbTraceInfoRow &a);

struct YogaDbVisInfoRow {
  string name;
  string visible;
  YogaDbVisInfoRow();
  YogaDbVisInfoRow(string const &_name, string const &_visible);
};

ostream & operator <<(ostream &s, const YogaDbVisInfoRow &a);


struct YogaDbLastTraceRow {
  string script;
  string name;
  YogaDbLastTraceRow();
  YogaDbLastTraceRow(string const &_script, string const &_name);
};

ostream & operator <<(ostream &s, const YogaDbLastTraceRow &a);


struct YogaDbExtractRow {
  string script;
  string funcName;
  string traceName;
  R startTs;
  R endTs;
};

ostream & operator <<(ostream &s, const YogaDbExtractRow &a);


struct YogaDbTraceInfoRowNameFlags {
  string name;
  string flags;
};

struct YogaDbLastSeen {
  string name;
  string part;
  string host;
  string dir;
};

DECL_PACKETIO(YogaDbLastSeen);

string canonTraceName(string const &name);

struct YogaDb {
  YogaDb(string const &_fn);

  static YogaDb &instance();

  string fn;
  sqlite3 *db{nullptr};

  sqlite3_stmt *prepare(string const &cmd);
  bool getResultRow(YogaDbTraceInfoRowNameFlags &r, sqlite3_stmt *stmt);
  bool getResultRow(YogaDbTraceInfoRow &r, sqlite3_stmt *stmt);
  bool getResultRow(YogaDbLastSeen &r, sqlite3_stmt *stmt);

  template<typename T>
  bool getAllResults(vector<T> &rows, sqlite3_stmt *stmt);

  bool getMatchingTraceNames(vector<YogaDbTraceInfoRowNameFlags> &ret, string const &nameLike, int startIndex, int maxCount);
  bool getLatestTraceName(string &ret, string const &nameLike);

  bool addTraceInfo(YogaDbTraceInfoRow const &it);
  bool getTraceInfo(YogaDbTraceInfoRow &ret, string const &name);
  vector<YogaDbTraceInfoRow> scanTracesDir();
  bool rebuildTracesTable();
  bool updateTraceFlags(string const &name, string const &flags);

  bool getVisInfo(YogaDbVisInfoRow &ret, string const &name);
  bool addVisInfoByName(YogaDbVisInfoRow const &it);
  bool addLastTraceByScript(YogaDbLastTraceRow const &it);
  bool getLastTraceByScript(YogaDbLastTraceRow &ret, string const &script);

  bool getLastSeen(vector<YogaDbLastSeen> &ret, string const &name);
  bool getLastSeen(vector<YogaDbLastSeen> &ret, string const &name, string const &part);
  bool addLastSeen(YogaDbLastSeen const &it);
  bool addLastSeen(vector<YogaDbLastSeen> const &its);
  void addTrace(Trace *trace);

  static void addLocalLastSeen(vector<YogaDbLastSeen> &lastSeen, Trace *trace);

  vector<YogaDbExtractRow> getExtractsByScriptAndFunc(string const &script, string const &funcName);
  bool addExtract(YogaDbExtractRow const &it);

  bool setupTables();

  map<string, UvMainThreadMutex> workMutexes;

  void ensureTraceLocal(
    string const &traceName, 
    std::function<void(string const &err)> const &cb,
    std::function<void(string const &status)> const &setStatus);

  bool loadTrace(string const &_traceName, 
    shared_ptr<YogaCompilation> reg,
    std::function<void(string const &err, shared_ptr<Trace> const &trace, json const &manifest)> cb,
    std::function<void(string const &status)> const &setStatus);

  void fetchTraceSeq(shared_ptr<YogaRpcPool> pool,
      string const &host,
      string const &traceDir,
      string const &traceName,
      string const &seqName,
      U64 ofs,
      std::function<void(string const &err)> cb);

  void fetchBlobFile(shared_ptr<YogaRpcPool> pool,
      string const &host,
      string const &fn,
      U64 chunkId,
      std::function<void(string const &err)> cb);


};
