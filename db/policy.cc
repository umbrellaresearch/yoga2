#include "./policy.h"
#include "../lib/scope_math.h"
#include "../timeseq/trace.h"
#include "../timeseq/timeseq.h"

PolicyParamVariation::PolicyParamVariation(string _paramName, R _paramRange)
  : paramName(_paramName),
    paramRange(_paramRange),
    power(1.5)
{
}

PolicyParamVariation::PolicyParamVariation()
{
}

bool PolicyParamVariation::isValid() const
{
  return !paramName.empty();
}


static R signpow(R x, R power)
{
  return copysign(pow(abs(x), power), x);
}



PolicyParamVariationBound::PolicyParamVariationBound(
  PolicyParamVariation const &_base,
  R _paramBase, YogaParamInfo *_param, int resolution)
  : PolicyParamVariation(_base),
    paramBase(_paramBase),
    param(_param)
{
  assert(resolution >= 1);
  values.resize(resolution);
  if (values.size() == 1) {
    values[0] = 0.0;
  }
  else {
    for (size_t i = 0; i < values.size(); i++) {
      auto x = signpow(R(i) / R(values.size()-1) * 2.0 - 1.0, power);
      values[i] = x;
    }
  }
}

string PolicyParamVariationBound::getRangeDescForAltRange(R altRange) const
{
  if (param) {
    return param->getRangeDesc(paramBase, altRange);
  }
  return "?";
}

string PolicyParamVariationBound::getRangeDesc() const
{
  return getRangeDescForAltRange(paramRange);
}

shared_ptr<PolicyParamVariationBound> PolicyParamVariation::bound(YogaContext const &ctx, int resolution) const
{
  auto paramBound = ctx.reg->paramsByName[paramName];
  R paramBaseBound = 0.0;
  if (paramBound) {
    paramBaseBound = paramBound->getNormValue(ctx);

    return make_shared<PolicyParamVariationBound>(*this, paramBaseBound, paramBound, resolution);
  }
  return nullptr;
}

shared_ptr<PolicyParamVariationBound> PolicyParamVariation::boundFullRange(YogaContext const &ctx, int resolution) const
{
  auto paramBound = ctx.reg->paramsByName[paramName];
  auto ret = make_shared<PolicyParamVariationBound>(*this, 0.0, paramBound, resolution);
  ret->paramRange = 2.0;
  return ret;
}



PolicyRewardMetric::PolicyRewardMetric()
{
}

PolicyRewardMetric::PolicyRewardMetric(YogaRef const &_ref, R _sampleTime, R _rScale)
  : ref(_ref),
    sampleTime(_sampleTime),
    rScale(_rScale)
{
}

string PolicyRewardMetric::getRangeDescForAltScale(R terrainDatum, R altScale) const
{
  return fmteng(terrainDatum, 3) + 
    " [" + fmteng(terrainDatum - altScale, 2) + 
    " - " + fmteng(terrainDatum + altScale, 2) + "]";
}

string PolicyRewardMetric::getRangeDesc(R terrainDatum) const
{
  return getRangeDescForAltScale(terrainDatum, rScale);
}

PolicyRewardMetric PolicyRewardMetric::bound(YogaContext const &ctx) const
{
  return *this;
}

bool PolicyRewardMetric::isValid() const
{
  return !ref.fullName.empty() && ref.error.empty();
}

vector<R> PolicyRewardMetric::getAltScales() const
{
  vector<R> ret;
  ret.push_back(rScale);
  auto altScale = rScale;
  for (int i=0; i<3; i++) {
    altScale = tidyScaleUp(altScale);
    ret.push_back(altScale);
  }
  altScale = rScale;
  for (int i=0; i<3; i++) {
    altScale = tidyScaleDn(altScale);
    ret.insert(ret.begin(), altScale);
  }
  return ret;
}

R calcMetric(Trace *trace, PolicyRewardMetric const &rMetric)
{
  auto seq = trace->getTimeseq(rMetric.ref.seqName);
  if (seq) {
    auto yv = seq->getEqualBefore(rMetric.sampleTime);
    if (yv.isValid()) {
      YogaValueAccessor<double> seqValue(rMetric.ref);
      return seqValue.rd(yv);
    }
  }
  return 0.0;
}



PolicyTerrainSpec::PolicyTerrainSpec()
{
}

PolicyTerrainSpec::PolicyTerrainSpec(PolicyParamVariation _xVar, PolicyParamVariation _yVar, PolicyRewardMetric _rMetric, int _resolution)
  : policyTerrainEpoch(1),
    xVar(_xVar),
    yVar(_yVar),
    rMetric(_rMetric),
    resolution(_resolution)
{
}

PolicyTerrainSpec::PolicyTerrainSpec(int _policyTerrainEpoch, PolicyParamVariation _xVar, PolicyParamVariation _yVar, PolicyRewardMetric _rMetric, int _resolution)
  : policyTerrainEpoch(_policyTerrainEpoch),
    xVar(_xVar),
    yVar(_yVar),
    rMetric(_rMetric),
    resolution(_resolution)
{
}

bool PolicyTerrainSpec::isValid() const
{
  return rMetric.isValid() && xVar.isValid() && yVar.isValid();
}

void PolicyTerrainSpec::clear()
{
  rMetric = PolicyRewardMetric();
  xVar = PolicyParamVariation();
  yVar = PolicyParamVariation();
  policyTerrainEpoch++;
}


PolicyTerrainMap::PolicyTerrainMap(YogaContext const &ctx, PolicyTerrainSpec const &_spec)
  : spec(_spec),
    paramValueEpoch(ctx.reg->paramValueEpoch),
    rs(spec.resolution * spec.resolution)
{
}

PolicyTerrainMap::~PolicyTerrainMap()
{
}

void PolicyTerrainMap::calcStats()
{
}

R & PolicyTerrainMap::rat(size_t xi, size_t yi)
{
  size_t ri = yi * xVarBound->values.size() + xi;
  return rs.at(ri);
}

void PolicyTerrainMap::animate(bool &uiFlag)
{
  R oldPhaseQuant = round(animatePhase*16.0);
  animatePhase += 1.0/rs.size();
  R newPhaseQuant = round(animatePhase*16.0);
  if (newPhaseQuant != oldPhaseQuant) {
    uiFlag = true;
  }
}



PolicySquiggleSpec::PolicySquiggleSpec()
{
}

PolicySquiggleSpec::PolicySquiggleSpec(int _policySquiggleEpoch, PolicyRewardMetric _rMetric, int _resolution)
  : policySquiggleEpoch(_policySquiggleEpoch),
    rMetric(_rMetric),
    resolution(_resolution)
{
}

PolicySquiggleSpec::PolicySquiggleSpec(PolicyRewardMetric _rMetric, int _resolution)
  : policySquiggleEpoch(1),
    rMetric(_rMetric),
    resolution(_resolution)
{
}

bool PolicySquiggleSpec::isValid() const
{
  return rMetric.isValid();
}


void PolicySquiggleSpec::clear()
{
  rMetric = PolicyRewardMetric();
  policySquiggleEpoch++;
}


PolicySquiggleMap::PolicySquiggleMap(YogaContext const &ctx, PolicySquiggleSpec const &_spec, size_t numVars)
  : spec(_spec),
    paramValueEpoch(ctx.reg->paramValueEpoch),
    varBounds(numVars),
    varsByParamIndex(ctx.reg->paramsByIndex.size(), (size_t)-1),
    rs(numVars * spec.resolution)
{
}

PolicySquiggleMap::~PolicySquiggleMap()
{
}

R & PolicySquiggleMap::rat(size_t varIndex, size_t xi)
{
  return rs.at(varIndex * spec.resolution + xi);
}

void PolicySquiggleMap::calcStats()
{
}

void PolicySquiggleMap::animate(bool &uiFlag)
{
  // WRITEME?
  uiFlag = true;
}



PolicyParamSearchSpec::PolicyParamSearchSpec()
{
}

PolicyParamSearchSpec::~PolicyParamSearchSpec()
{
}


/*
*/



void PolicyGradientSpec::addDesiredChange(
    YogaRef const &ref, R ts, R delta, 
    string const &label, U32 labelColor,
    YogaValue deltaValue)
{
  if (0) cerr << "Desired change " + ref.seqName + ": " + repr(deltaValue) + "\n";
  if (deltaValue.type != ref.seqType) {
    throw runtime_error("setDesiredChange: setting delta=" + repr(deltaValue) + " on sequence of type " + repr_type(ref.seqType));
  }

  PolicyGradientInput newChange;
  newChange.uniqueKey = ref.fullName;
  newChange.ref = ref;
  newChange.label = label;
  newChange.labelColor = labelColor;
  newChange.ts = ts;
  newChange.delta = delta;
  newChange.deltaValue = deltaValue;
  desiredChanges.push_back(newChange);
  gradientEpoch ++;
}


void PolicyGradientSpec::clearDesiredChange(YogaRef const &ref, R tsMin, R tsMax)
{
  for (auto it = desiredChanges.begin(); it != desiredChanges.end();) {
    if (it->uniqueKey == ref.fullName && it->ts >= tsMin && it->ts <= tsMax) {
      it = desiredChanges.erase(it);
      gradientEpoch ++;
    }
    else {
      it++;
    }
  }
}

void PolicyGradientSpec::clear()
{
  desiredChanges.clear();
  gradientEpoch ++;
}

bool PolicyGradientSpec::empty()
{
  return desiredChanges.empty();
}

bool PolicyGradientSpec::hasDesiredChange(YogaRef const &ref, R tsMin, R tsMax)
{
  for (auto it = desiredChanges.begin(); it != desiredChanges.end(); it++) {
    if (it->uniqueKey == ref.fullName && it->ts >= tsMin && it->ts <= tsMax) {
      return true;
    }
  }
  return false;
}
