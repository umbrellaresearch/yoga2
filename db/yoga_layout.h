#pragma once
#include "common/std_headers.h"
#include "common/str_utils.h"
#include "nlohmann-json/json_fwd.hpp"
#include <any>
using json = nlohmann::json;

struct YogaRpcPool;
struct PacketNetworkEngine;


struct YogaLayout {
  string homeDir;
  string runDir;
  string tracesDir;
  string dataDir;

  string tlbcoreDir;
  string tlbcoreImagesBase;

  string yogaDir;
  string yogaImagesDir;
  string yogaBinDir;

  string localHostName;

  string localUser;

  string imguiFontFile(string const &font) const {
    return yogaDir + "/deps/imgui/misc/fonts/"s + font;
  }

  string scopeShaderFile(string const &name) const {
    return yogaDir + "/studio/shaders/"s + name + ".glsl";
  }

  tuple<string, bool> requirePath(string const &requireArg, string const &hostFn = "") const;

  string blobFilename(U64 chunkId) const;

  map<string, string> includeMap;

  bool isLocalhost(string const &host) const;

  vector<string> sshifyArgs(vector<string> execArgs, string const &sshHost) const;

  shared_ptr<YogaRpcPool> mkRpcPool(string const &port) const;
  shared_ptr<PacketNetworkEngine> mkRpcConn(string const &service, string const &host, bool autoStart=true) const;
  shared_ptr<PacketNetworkEngine> mkLocalBotdRpcConn(bool autoStart) const;

  shared_ptr<json> config;

  static YogaLayout &instance();

  static int ignoreUserConfig;

};

ostream & operator <<(ostream &s, const YogaLayout &a);




struct YogaRpcPool {
  YogaRpcPool(YogaLayout const &_layout, string _service);
  ~YogaRpcPool();
  void stopAll();

  YogaLayout const &layout;
  string service;

  map<string, shared_ptr<PacketNetworkEngine>> byHost;

  PacketNetworkEngine *get(string const &host);
  shared_ptr<PacketNetworkEngine> get_shared(string const &host);

  string progress();
};

bool isValidTraceName(string const &a);
bool isValidSeqName(string const &a);
