#include "./blob.h"
#include "common/str_utils.h"

const int verbose = 0;

Blob::Blob(size_t _size)
  : size_(_size),
    data_(new U8[_size])
{
  if (verbose) cerr << "Blob alloc " + repr_ptr(data_) + " " + repr(size_) + "\n";
}

Blob::Blob()
  : size_(0),
    data_(nullptr)
{
}

Blob::Blob(Blob &&other)
  : size_(other.size_),
    data_(other.data_)
{
  other.size_ = 0;
  other.data_ = nullptr;
}

Blob& Blob::operator=(Blob &&other)
{
  if (&other != this) {
    if (verbose) cerr << "Blob overwrite " + repr_ptr(data_) + " " + repr(size_) + 
      " with " + repr_ptr(other.data_) + " " + repr(other.size_) + "\n";
    if (data_) {
      delete [] data_;
    }
    data_ = other.data_;
    size_ = other.size_;
    other.data_ = nullptr;
    other.size_ = 0;
  }
  return *this;
}


Blob::~Blob()
{
  if (data_) {
    if (verbose) cerr << "Blob delete " + repr_ptr(data_) + " " + repr(size_) + "\n";
    delete [] data_;
    data_ = nullptr;
  }
  size_ = 0;
}

void Blob::alloc(size_t _size)
{
  if (data_) {
    if (verbose) cerr << "Blob overwrite " + repr_ptr(data_) + " " + repr(size_) + "\n";
    delete [] data_;
    data_ = nullptr;
  }
 
  size_ = _size;
  data_ = new U8[_size];
  if (verbose) cerr << "Blob alloc " + repr_ptr(data_) + " " + repr(size_) + "\n";
}


void Blob::swap(Blob &other) noexcept
{
  std::swap(size_, other.size_);
  std::swap(data_, other.data_);
}


namespace packetio {
void packet_rd_value(::packet &p, Blob &x)
{
  uint32_t size;
  packetio::packet_rd_value(p, size);
  if (!(size < 0x3fffffff)) throw runtime_error("Unreasonable size "s + to_string(size));
  x.alloc(size);
  p.get_bytes(x.data(), size);
}

void packet_wr_value(::packet &p, Blob const &x)
{
  if (!(x.size() < 0x3fffffff)) throw runtime_error("Unreasonable size "s + to_string(x.size()));
  packetio::packet_wr_value(p, (uint32_t)x.size());
  p.add_bytes(x.data(), x.size());
}

}

