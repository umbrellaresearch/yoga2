#pragma once
#include "common/std_headers.h"
#include "../lib/core_types.h"
#include "../jit/compilation.h"
#include "../jit/value.h"
#include "../jit/runtime.h"

struct PolicyParamVariationBound;
struct ScopeModel;

/*
  Describes a policy parameter that can be varied, but without having chosen a nominal value yet
  PolicyParamVariationBound includes a nominal value (paramBase).
*/
struct PolicyParamVariation {
  PolicyParamVariation();
  PolicyParamVariation(string _paramName, R _paramRange);
  bool isValid() const;

  shared_ptr<PolicyParamVariationBound> bound(YogaContext const &ctx, int resolution) const;
  shared_ptr<PolicyParamVariationBound> boundFullRange(YogaContext const &ctx, int resolution) const;

  string paramName;
  R paramRange{0.0};
  R power{1.5};

  json toJson() const;
  static PolicyParamVariation fromJson(YogaContext const &ctx, json const &j);
};

struct PolicyParamVariationBound : PolicyParamVariation {
  PolicyParamVariationBound(PolicyParamVariation const &_base,
    R _paramBase, YogaParamInfo *_param, int resolution);

  string getRangeDesc() const;
  string getRangeDescForAltRange(R altRange) const;

  R paramBase{0.0};
  YogaParamInfo *param{nullptr};
  vector<R> values;
};

/*
  Describes the metric we're using to evaluate policies.
  It can refer to any variable (specified by a YogaRef) at any point in time (sampleTime)
*/
struct PolicyRewardMetric {
  PolicyRewardMetric();
  PolicyRewardMetric(YogaRef const &_ref, R _sampleTime, R _rScale);
  PolicyRewardMetric bound(YogaContext const &ctx) const;
  string getRangeDescForAltScale(R terrainDatum, R altScale) const;
  string getRangeDesc(R terrainDatum) const;
  bool isValid() const;
  vector<R> getAltScales() const;

  YogaRef ref;
  R sampleTime{0.0};
  R rScale{1.0};

  json toJson() const;
  static PolicyRewardMetric fromJson(YogaContext const &ctx, json const &j);
};

R calcMetric(Trace *trace, PolicyRewardMetric const &rMetric);

/*
  The complete spec for a policy terrain run.
  Make sure to increment policyTerrainEpoch when you change anything in here
  so that ScopeModel::updatePolicyTerrain will know to re-run the policy terrain
  calculation.
*/
struct PolicyTerrainSpec {
  PolicyTerrainSpec();
  PolicyTerrainSpec(int _policyTerrainEpoch, PolicyParamVariation _xVar, PolicyParamVariation _yVar, PolicyRewardMetric _rMetric, int _resolution);
  PolicyTerrainSpec(PolicyParamVariation _xVar, PolicyParamVariation _yVar, PolicyRewardMetric _rMetric, int _resolution);

  bool isValid() const;
  void clear();

  int policyTerrainEpoch{0};
  string sourceFileName;
  PolicyParamVariation xVar, yVar;
  PolicyRewardMetric rMetric;
  int resolution{9};

  json toJson() const;
  static PolicyTerrainSpec fromJson(YogaContext const &ctx, json const &j);
};


/*
  A completed policy terrain run. We capture the spec it was based off,
  and we can compare epochs to know if it's out of date.
  This map is between 2 independent variables (X and Y) and one dependent one (R).
*/
struct PolicyTerrainMap {
  PolicyTerrainMap(YogaContext const &ctx, PolicyTerrainSpec const &_spec);
  ~PolicyTerrainMap();

  R & rat(size_t xi, size_t yi);
  void calcStats();
  void animate(bool &uiFlag);

  PolicyTerrainSpec spec;
  int paramValueEpoch{0};
  shared_ptr<PolicyParamVariationBound> xVarBound, yVarBound;

  // Layout is y-major, x-minor
  vector<R> rs; 

  // When we pick a new param set, we animate sliding the old map to match.
  R animateTargetX{0.0}, animateTargetY{0.0}, animateTargetR{0.0}, animatePhase{0.0};
};


/*
  The complete spec for a set of policy squiggles.
*/
struct PolicySquiggleSpec {
  PolicySquiggleSpec();
  PolicySquiggleSpec(int _policySquiggleEpoch, PolicyRewardMetric _rMetric, int _resolution);
  PolicySquiggleSpec(PolicyRewardMetric _rMetric, int _resolution);

  void clear();

  bool isValid() const;
  int policySquiggleEpoch{0};

  // Only include params from this file
  string sourceFileName;

  PolicyRewardMetric rMetric;
  R paramRange{0.5};
  int resolution{33};

  json toJson() const;
  static PolicySquiggleSpec fromJson(YogaContext const &ctx, json const &j);
};


/*
  The results of a policy squiggle calculation.
  You can see if it's out of date by comparing epochs.
*/
struct PolicySquiggleMap {
  PolicySquiggleMap(YogaContext const &ctx, PolicySquiggleSpec const &_spec, size_t numVars);
  ~PolicySquiggleMap();

  R & rat(size_t varIndex, size_t xi);
  void calcStats();
  void animate(bool &uiFlag);

  // The spec that this map is build around. We can check if this map is out of 
  // date by comparing spec.paramSquiggleEpoch
  PolicySquiggleSpec spec;
  int paramValueEpoch{0};

  // indexed by varIndex [0 <= varIndex < varBounds.size()]
  vector<shared_ptr<PolicyParamVariationBound>> varBounds;

  // maps paramIndex (0 <= paramIndex < reg->paramsByIndex.size()] to varIndex
  // Entries we don't have a variation for are (size_t)-1
  vector<size_t> varsByParamIndex;

  // Use rat to get at this.
  // Layout is varIndex-major, with xi minor.
  vector<R> rs;

};

/*

*/
struct PolicyParamSearchSpec {
  PolicyParamSearchSpec();
  ~PolicyParamSearchSpec();
  set<YogaParamInfo *> participatingParams;
  PolicyRewardMetric rMetric;

  R accelTowardsBest = 0.01;
  R velCoeff = 0.925;
  R initialParamSigma = 1.5;
  R initialVelSigma = 0.05;

  int populationTarget = 50;

  json toJson() const;
  static PolicyParamSearchSpec fromJson(YogaContext const &ctx, json const &j);
};



struct PolicyGradientInput {
  string uniqueKey;
  YogaRef ref;
  string label;
  U32 labelColor;
  R ts{0.0};
  R delta{0.0};
  YogaValue deltaValue;
};

/*
*/
struct PolicyGradientSpec {
  int gradientEpoch{0};
  vector<PolicyGradientInput> desiredChanges;

  void addDesiredChange(
    YogaRef const &ref, R ts, R delta, 
    string const &label, U32 labelColor,
    YogaValue deltaValue);

  void clearDesiredChange(YogaRef const &ref, R tsMin=0.0, R tsMax=1e99);
  void clear();

  bool empty();
  bool hasDesiredChange(YogaRef const &ref, R tsMin=0.0, R tsMax=1e99);

};
