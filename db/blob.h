#pragma once
#include "common/std_headers.h"
#include "common/packetbuf.h"

struct Blob {
  Blob();
  Blob(size_t _size);
  Blob(Blob &&other);
  Blob(Blob const &other) = delete;
  ~Blob();

  Blob& operator=(Blob const &other) = delete;
  Blob& operator=(Blob &&other);

  void alloc(size_t _size);
  U8 *data() const { return data_; }
  size_t size() const { return size_; }
  void swap(Blob &other) noexcept;
  U8 & operator [] (size_t ofs) { return data_[ofs]; }

  size_t size_;
  U8 *data_;
};



DECL_PACKETIO(Blob);


