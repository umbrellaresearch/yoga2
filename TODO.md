# TODO

## Before release

* Make sure that examples/greppy/main.vis.json gets some sensible defaults, after monkeying with the syntax of panelStyles


## Bugs

* clamp with 2 args crashes: Assertion failed: (args.size() == 3), function backprop, file jit/expr_backprop.cc, line 497.

## Open-access robots

* Get greppy running:
  * [~] harness, ropes
  * [x] scene camera
  * [x] scene lights
  * onboard camera?
  * [x] power supply
  * [x] ethernet or wifi?
  * more floor space for greppy1
* [x] nimes, rome should boot on power up

## Demo

* Script, perform, edit, publish

* Policy terrain examples.
  * Do I need a nice way to save them? Can I put them up as a 3d visualization on the web?

* [x] --podcast UI flag

* [x] Be consistent in [o, s, a] var names.

## Renames



## Functionality

* debugLocals
  * [x] emit: Invalid types for *: R, C => C
  * Duplicate names coming from eg ['test.d', 'out.s', 'foo'] and ['test.d', 'out.s.foo']
  * Test that it works on botd without debug versions

* Can botd also skip compiling the .yogaGrad versions?

* Prediction:
  * [x] A function predict(update State s, in Plan plan)
  * [x] It can use polynomials or whatever inside
  * Can be trained by sweeping a range of times.
  * Change visual to look more like runtime, with 'predict', 'render2d', 'render3d' instead of engine.
    * Need object properties too, like duration for predict
  * 
  * Menu item on code annotation to re-randomize. Or set to identity.
  * Also, show its eigenvalues.
  * Panel to show training progress

* Dark mode by default, optimize for that, make light mode be the afterthought.

* Argmin. Something like
```yoga
  xinit = Foo(1,2);
  xbest = argmin(c, x, xinit) {
    c = cost(., f(., x))
  }
```

* [x] Fix `init initAcrobotAgent(agent.state) {};`

* [x] Add timeDilation param in runtime

* [x] Add easily configurable phase plots. Probably with a function that returns a R[2].
Or maybe an R[2] and a color, and also takes age as an input. Then we can do the fadeout
in Yoga.

* Policy terrain:
  * [x] Add menu on parameters in code_drawer to set as X or Y of the terrain
  * [x] Highlight the active parameters
  * [x] Picker for policy terrain
  * [x] Selectable ranges for X and Y axes. Typicaly +/- 0.1 nominal
  * [x] Power law in X,Y scaling
  * [x] When computing PT, simulate only up to the capture time.
  * [x] Show range of PT parameters in the code pane
  * Show coordinates with red & green but a polesitting yoga robot for up.
  * Automatically use a low resolution when updating frequently, and a high resolution when stable.
  * Some nice geological colors?
  * [x] Instead of calculating rCenter from each squiggle/terrain map, use the reference trace
  * Make param sliders bigger (esp. in height)

* Optimized version of update for simulations?

* Policy Squiggles:
  * [x] Only update visible params.
    * [x] PolicySquiggleMap should have its own list of params.
  * [x] Hovering over a param should show a high-res squiggle (this needs the optimization above)

* [x] Support panels with multiple arguments

* [x] How about a menu item to open panels instead of checkboxes? We can close from the panel itself.

* Foreign functions:
  * [x] Needs to work in debug mode

* Compiler
  * [x] Verify function call type checking
  * Bind unary + into parameter value like with unary -

* ALE:
  * [x] Need search operators in screen pixels

* Simulation:
  * When is something simulatable?
    * ScopeModel::liveHost.empty()? 
    * Trace::isAnyExtIO()?
    * EngineRegister::liveOnly?

* Policy search
  * Mark some parameters as searchable

* Cleanup
  * Do I still need the system for creating sequences from the manifest? Doesn't the new EngineRegister take care of all that?
  

* Color code debugLog assignments: yellow/red for true assignments, black for bindings.

* Don't show large debugLog entries unless we hover over the variable, then show as a popup.
  * And highlight the destination variable to show hoverability
  * And if there are many values, see if we can't indicate which is which with a stack trace.
  * Don't show type names in debugLog values

* OpenGL
  * Redo rendering with subclasses of ShaderSetup, which know which Shader they want but also
    have set differnet kinds of uniforms & attributes.
  * 
  
* Visual quality
  * Consider using a custom shader for drawing graphs. A tesselation shader could give good, high-perf results.
    * See https://forum.libcinder.org/topic/smooth-thick-lines-using-geometry-shader
    * https://computeranimations.wordpress.com/2015/03/16/rasterization-of-parametric-curves-using-tessellation-shaders-in-glsl/
    * https://www.khronos.org/opengl/wiki/Tessellation#Patches
  * Also, a custom shader for filled areas like a onehot. Currently they look blotchy.

* App Packaging
  * https://developer.apple.com/library/archive/documentation/CoreFoundation/Conceptual/CFBundles/AccessingaBundlesContents/AccessingaBundlesContents.html#//apple_ref/doc/uid/10000123i-CH104-SW1
  

* [x] visual{} should be more like engine{}, allowing multiple arguments from separate traces. [breaking]

* Cook up a policy to avoid language incompatibilities with what's deployed
on public ports and what's in the public git repo.
Maybe change ports when there's a breaking change?
(An example breaking change was changing the types of RenderLine in 6e8298b622fcae90af70aef8acd0a5023b604ca4)
  * Also, maybe I should be working outside of master.

* [x] Render visualizations should get picked up for debugLog execution.

* Nicer panel for YogaPuck

* Change debugLog mechanism to a large struct, with a separate table of
annotations.

* Test dark mode

* Use native mac menus? Would have to do something like CreateApplicationMeny in src/video/cocoa/SDL_cocoaevents.m

* Nicer "Fetching n/m" from YogaDb. Probably needs to send a special progress datatype to the callback.

* [x] Videos seem backwards unless I set a transform. Should be forwards by default

* [x] Greppy: forward-backward movement seems flipped (by W and S).

* [x] Make camerad a regular daemon on a port.
* [x] Make lightd owned by xinetd
* [x] Make lightd time out

* [x] Make generic user/dir for camerad. Needn't correspond to actual user.
* [x] Use camerad with TcpPipe for remote_camera_daemon too.
* [x] Also for fetching entire blobs

* [x] For an update param, arrange for every element to default to the input.
Should cut a lot of clutter.

* [x] Handle reload with syntax error better.
* [x] Figure out if we should click cmd-R or cmd-L to reload and launch.

* Maybe save camera streams in lo-res as well as hi-res. Stream the lo-res in real time

* [x] Use ui.joy instead of a separate waldo system. Parallel to ui.kbd.

* [x] Eliminate: packet IO in yoga. Then also FunctionEffects::sideEffects.

* After a live session, start loading and add it to a visible loading queue but don't jump to it.
  * Put a cancel button for the load. And a goto once it finishes.
  * Can be a popup panel in lower right

* [x] Code value vis
  * [x] Emit version of code that records values of every expression in a struct with source file:row:col (a YogaSourceLoc)
  * [x] display them
  * [?] For subroutines called multiple times, there'll be multiple values. For now, just
    pack them L-R next to each other in the rendering.
  * [x] Add Trace::rerunOne that only does the current time, and records the samples.
    * [solved] memory will be tricky

* Dex4 drivers:
  * isn't ssc_gyro.c obsolete? Why does it reference an ads8344? Legacy from old analog version?

* Predictor structure:
  * Large dt
  * Prev, zeta => actions, next, astonishment
    * Note how action isn't an input. We can't predict for any action. Action is a function of prev and zeta
    * Zeta can have named kinds of badness, like trip or slip. We need to label these in the training set! Otherwise, zeta can be found by an inner-loop gradient descent.
      * For generated traces, we have the zeta used at the time which should stay fairly
      constant (but not guaranteed.) We may have to regenerate the zeta values from
      the actions if they change a lot. But this can be an exception case. For the
      most part, we'll encourage coders to preserve the effect of existing zeta values
      and add new members to zeta. These will be read as zero when loading old experience
      data 
      * 

* Write before/mid/end as CSV, train with Torch, output model
  * Convert trace to csv. CSV is fast enough. If not, I can (in Python) cache the CSV as Feather or something. But the C++ code doesn't need to know about it.
  * Use names like t0_sense_th1 and t2_sense_th1 (inputs) and t1_sense_th1 (output)
    * Try with dots instead
  * 

* Relations:
  * DB should list traces and time ranges to extract. Also intervals. Under an "extraction" name
  * When run, scan DB and load all those traces. Generate pairs of tuples of YogaValues.

  * Visualize samples in 3D, or with ad-hoc visualizations
    * Might need better yoga syntax for drawing
      * Probably requires tagged sets of any type. Ugh.
    * Probably store in a trace-like format, rather than JSON in SQLite.
      * Schema migration is more important than with traces
  * Start manually coalescing samples into clusters, with a sample + linear adjustment.
    * 2D enough?

* Visualize an engine
  * Plot points: an entire I/O tuple.
  * Arrange them in space by projecting each axis into 3D space, then visualizing the 3D repn.
  * Color: green if a real point passes test. red if any point fails test. blue if a fake point passes test.
  * Visualization needs:
    * A named function
    * A set of test points, extracted from a trace

* Eliminate integer types? Maybe bool too? They're barely used.

* [x] panels should show counterfactual

* Demo projects
  * Mocap suit -> Animated character on screen (no dynamics?)
  * [x] Balance bot
  * Stewart platform ball juggler
  * Inverted double pendulum.
  * More general animated sculpture

* [x] Implement dragGrads

* [x] Show paramGrads in code window

* [x] Have linearOp do something for structs containing bools.

* [x] Make backprop work.
  * [x] test individual grad functions
  * [x] make a test harness to perturb individual inputs and verify changes on the output

* [~] Implement and test complex arithmetic

* [x] Always pass around ExprOut, instead of EmittedValueEntry or value/ptr + type + signed.

* Rolling buffer for timeseqs for long sessions? Do I need it?

* [x] Slidable partition between scope and editor. See https://github.com/ocornut/imgui/issues/125#issuecomment-135775009

* [x] Make color be a U32 in RenderSolid, RenderLine

* [x] Restore consoleSeq.

* Test live audio
  * record some using ALSA driver
  * listen with autoScroll
  * Make sure we can record at 48k (or else do everything at 44.1, but I think 48k is more standard)
  * See how well audio/video are synced
  * Possibly we could also write a traceRender for blobs. Just show envelope, not entire waveform.
  * How does it work live?

* [x] Make left/right arrow scroll at real time

* Does convDoubleBool test if != 0.0?  It should test for > 0.5. How about if?

* [x] Fix flickering video?

* [x] Set video position properly

* [x] Efficiency hack in TraceRenderOnehot: coalesce quads

* [x] HW: move top camera to look down at feet, move whatever's blocking foot camera.

* [x] Send yogaParam updates to botd

* [x] Add counterfactuals!

* [x] Make minUpdateDt do something

* [x] Coordinate fetching video blobs with traceSavedAs

* [x] Zoom in/out timewise

* [x] Autoscaling

* [x] Visualize onehot

* [x] Strip chart of .interval type

* [x] Load remote traces after live

* [x] Load blobs separately after remote traces

* [x] Port mk_cad42_dsp.js to Yoga

* [x] Verify precendence for ^. Eg,
   foo^2 * 3

* [x] Compile all avr32

* Test and push avr32 (keep safe backups)

* [x] Scope: Add exact numbers at cursor

* [x] Scope: add graph for vector/matrix.

* [x] Save scope selector checkbox values in database.

* [x] Make lights work


## Language ergonomics

* s/Polyfit([1-5])/Poly$1/g    [breaking]


## Reliability

* Arrange to `sudo journalctl --rotate --vacuum-size=50M` on orly & nimes periodically

## Documentation

* How to specify visualizations in Yoga. Like for Dex4Kin.

## Optimizations

* Optimize Timeseq::getLast, which is taking nearly half the time when doing a rerun.
  * Maybe make timeseq use a `deque<pair<double, U8 *>>` instead of `map<double, U8 *>`.
  * Should reject out-of-order adds

## Exploratory

* Not very interesting to change SDL to glfw. But it might be interesting to try the docking window version of imgui.

