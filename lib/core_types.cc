#include "common/std_headers.h"
#include "../comm/ssc_io.h"
#include "../comm/ssc_network_engine.h"
#include "../db/yoga_layout.h"
#include "../lib/core_types.h"
#include "common/packetbuf_types.h"
#include "nlohmann-json/json.hpp"

namespace packetio {
  string packet_get_typetag(json const &x)
  {
    return "json:1";
  }
  void packet_wr_value(packet &p, json const &x)
  {
    packetio::packet_wr_value(p, x.dump());
  }
  void packet_rd_value(packet &p, json &x)
  {
    string s;
    packetio::packet_rd_value(p, s);
    x = json::parse(s);
  }
  std::function<void(packet &, json &)> packet_rd_value_compat(json const &x, string const &typetag)
  {
    if (typetag == "json:1") {
      return static_cast<void(*)(packet &, json &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(json);

namespace packetio {
  string packet_get_typetag(TimeseqChange const &x)
  {
    return "TimeseqChange:1";
  }
  void packet_wr_value(packet &p, TimeseqChange const &x)
  {
    packetio::packet_wr_value(p, x.beginTs);
    packetio::packet_wr_value(p, x.endTs);
    packetio::packet_wr_value(p, x.windowFuncName);
    packetio::packet_wr_value(p, x.changeKey);
    packetio::packet_wr_value(p, x.value);
  }
  void packet_rd_value(packet &p, TimeseqChange &x)
  {
    packetio::packet_rd_value(p, x.beginTs);
    packetio::packet_rd_value(p, x.endTs);
    packetio::packet_rd_value(p, x.windowFuncName);
    packetio::packet_rd_value(p, x.changeKey);
    packetio::packet_rd_value(p, x.value);
  }
  std::function<void(packet &, TimeseqChange &)> packet_rd_value_compat(TimeseqChange const &x, string const &typetag)
  {
    if (typetag == "TimeseqChange:1") {
      return static_cast<void(*)(packet &, TimeseqChange &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(TimeseqChange);

namespace packetio {
  string packet_get_typetag(SolverParams const &x)
  {
    return "SolverParams:1";
  }
  void packet_wr_value(packet &p, SolverParams const &x)
  {
    packetio::packet_wr_value(p, x.eps);
    packetio::packet_wr_value(p, x.nsteps);
    packetio::packet_wr_value(p, x.verbose);
  }
  void packet_rd_value(packet &p, SolverParams &x)
  {
    packetio::packet_rd_value(p, x.eps);
    packetio::packet_rd_value(p, x.nsteps);
    packetio::packet_rd_value(p, x.verbose);
  }
  std::function<void(packet &, SolverParams &)> packet_rd_value_compat(SolverParams const &x, string const &typetag)
  {
    if (typetag == "SolverParams:1") {
      return static_cast<void(*)(packet &, SolverParams &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(SolverParams);


namespace packetio {
  string packet_get_typetag(PingPong const &x)
  {
    return "PingPong:1";
  }
  void packet_wr_value(packet &p, PingPong const &x)
  {
    packetio::packet_wr_value(p, x.rxTs);
    packetio::packet_wr_value(p, x.txTs);
  }
  void packet_rd_value(packet &p, PingPong &x)
  {
    packetio::packet_rd_value(p, x.rxTs);
    packetio::packet_rd_value(p, x.txTs);
  }
  std::function<void(packet &, PingPong &)> packet_rd_value_compat(PingPong const &x, string const &typetag)
  {
    if (typetag == "PingPong:1") {
      return static_cast<void(*)(packet &, PingPong &)>(packetio::packet_rd_value);
    }
    return nullptr;
  }
}

INSTANTIATE_PACKETIO(PingPong);

static YogaCodeRegister registerCoreTypes("core_types.cc", R"(
  struct YogaConsoleMsg {
    string message;
  };

)");
