#pragma once
#include "common/packetbuf.h"
#include "nlohmann-json/json_fwd.hpp"
using json = nlohmann::json;


DECL_PACKETIO(json);

struct TimeseqChange {
  R beginTs;
  R endTs;
  string windowFuncName;
  string changeKey;
  R value;
};

DECL_PACKETIO(TimeseqChange);

struct SolverParams {
  R eps;
  int nsteps;
  int verbose;
};

DECL_PACKETIO(SolverParams);

struct PingPong {
  R txTs, rxTs;
};

DECL_PACKETIO(PingPong);

struct YogaConsoleMsg {
  U8 *message;
};
