#include "../test/test_utils.h"
#include "./scope_math.h"


TEST_CASE("quantizeSubsample", "[lib][scope_math]") {
  CHECK(quantizeSubsample(1.0) == 1.0);
  CHECK(quantizeSubsample(0.1) == 1.0);
  CHECK(quantizeSubsample(1.5) == 2.0);
  CHECK(quantizeSubsample(2.5) == 5.0);
  CHECK(quantizeSubsample(25.0) == 50.0);
  CHECK(quantizeSubsample(250.0) == 500.0);
}

TEST_CASE("fmteng", "[lib][scope_math]") {
  CHECK(fmteng(1.75, 3) == "+1.75");
  CHECK(fmteng(0, 3) == "0");
  CHECK(fmteng(1, 3) == "+1.000");
  CHECK(fmteng(50, 3) == "+50.0");
  CHECK(fmteng(500, 3) == "+500");
  CHECK(fmteng(5000, 3) == "+5000");
  CHECK(fmteng(50000, 3) == "+50.0e3");
  CHECK(fmteng(500000, 3) == "+500e3");
  CHECK(fmteng(5000000, 3) == "+5000e3");

  CHECK(fmteng(5e-1, 3) == "+0.500");
  CHECK(fmteng(5e-2, 3) == "+0.0500");
  CHECK(fmteng(5e-3, 3) == "+5.00e-3");
  CHECK(fmteng(5e-4, 3) == "+0.500e-3");
  CHECK(fmteng(5e-5, 3) == "+0.0500e-3");
  CHECK(fmteng(5e-6, 3) == "+5.00e-6");
}


TEST_CASE("softclamp", "[lib][scope_math]") {
  CHECK(softclamp(0.5) == 0.5);
  CHECK(softclamp(-0.5) == -0.5);
  CHECK(softclamp(1.0) == 1.0);
  CHECK(softclamp(2.0) >= 1.5);
  CHECK(softclamp(-1.0) == -1.0);
  CHECK(softclamp(-2.0) <= -1.5);
}
