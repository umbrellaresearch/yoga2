#pragma once
#include "numerical/numerical.h"



// Quantizes to the next smaller interval
double quantizeIntervalDn(double v);
double quantizeIntervalUp(double v);
double quantizeSubsample(double v);

/*
  Return largest nice number >= v.
  Nice means [1, 1.5, 2, 3, 5, 7] * 10^n. 6 steps per decade
*/
double tidyScale(double v);
double tidyScaleUp(double v);
double tidyScaleDn(double v);

const double PIXEL_RATIO = 2.0; // ugh, should be from display

static inline double snap5(double x)
{
  return (round(x * PIXEL_RATIO - 0.5) + 0.5) / PIXEL_RATIO;
}

static inline double snap(double x)
{
  return round(x * PIXEL_RATIO) / PIXEL_RATIO;
}

double compressGrad(double g);
double expandGrad(double g);
double compressPower(double v, double power);
double expandPower(double v, double power);

string fmteng(double v, int prec);
string fmta2(double v);
string fmta3(double v);


// Return the next possible time after ts within the floating point representation
inline double timeEpsAfter(double ts)
{
  return nextafter(ts, ts + 1.0);
}
// Return the next possible time before ts within the floating point representation
inline double timeEpsBefore(double ts)
{
  return nextafter(ts, ts - 1.0);
}

R softclamp(R x);
R softunclamp(R x);
